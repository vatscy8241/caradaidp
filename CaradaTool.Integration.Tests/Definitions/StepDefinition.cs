﻿using CaradaTool.Integration.Tests.Properties;
using Microsoft.VisualBasic.FileIO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace CaradaTool.Integration.Tests.Definitions
{
    /// <summary>
    /// シナリオ定義クラスの抽象クラス
    /// </summary>
    [Binding]
    class StepDefinition : Steps
    {
        #region private変数

        /// <summary>
        /// CARADAID発行結果TSVのヘッダー.
        /// </summary>
        private static string[] CaradaIdRegistHeader = new string[] { "CARADA ID", "パスワード", "UID", "エラーコード", "エラー詳細" };

        /// <summary>
        /// ユーザー情報リセット結果結果TSVファイルのヘッダー.
        /// </summary>
        private static string[] ResetUserInfoHeader = new string[] { "CARADA ID", "パスワード", "リセット種別", "エラーコード", "エラー詳細" };

        /// <summary>
        /// confフォルダのパス.
        /// </summary>
        private static string ConfPath = Path.Combine(TestHelper.TestConfiguration.ExePath, @"..\conf\");

        /// <summary>
        /// inフォルダのパス.
        /// </summary>
        private static string InPath = Path.Combine(TestHelper.TestConfiguration.ExePath, @"..\in\");

        /// <summary>
        /// ツール用プロパティTSVファイルのフォルダパス.
        /// </summary>
        private static string PropPath = TestHelper.TestConfiguration.ExePath;

        /// <summary>
        /// logフォルダのパス.
        /// </summary>
        private static string LogPath = Path.Combine(TestHelper.TestConfiguration.ExePath, @"..\log\");

        /// <summary>
        /// outフォルダのパス.
        /// </summary>
        private static string OutPath = Path.Combine(TestHelper.TestConfiguration.ExePath, @"..\out\");

        /// <summary>
        /// オリジナルのconfフォルダパス.
        /// </summary>
        private static string OrgConfPath = Path.Combine(TestHelper.TestConfiguration.ExePath, @"..\..\..\conf\");

        /// <summary>
        /// オリジナルのinフォルダパス.
        /// </summary>
        private static string OrgInPath = Path.Combine(TestHelper.TestConfiguration.ExePath, @"..\..\..\in\");

        /// <summary>
        /// オリジナルのツール用プロパティTSVファイルのフォルダパス.
        /// </summary>
        private static string OrgPropPath = Path.Combine(TestHelper.TestConfiguration.ExePath, @"..\..\..\");

        /// <summary>
        /// 標準出力を保存するStringBuilder.
        /// </summary>
        private static StringBuilder outputBuilder = new StringBuilder();

        #endregion

        #region シナリオ変数utility

        /// <summary>
        /// データを一時保存し、次ステップ以降に引き回します。
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="value">値</param>
        /// <returns></returns>
        protected virtual void SaveCurrentData(string key, string value)
        {
            ScenarioContext.Current["Current:" + key] = value;
        }

        /// <summary>
        /// 一時保存したデータを再取得します。
        /// </summary>
        /// <param name="key">キー</param>
        /// <returns>値</returns>
        protected virtual object ReceiveCurrentData(string key)
        {
            if (!ScenarioContext.Current.ContainsKey("Current:" + key))
            {
                throw new SpecFlowException(string.Format("指定されたキー「{0}」にはデータは紐づいていません。", key));
            }
            return ScenarioContext.Current["Current:" + key];
        }

        /// <summary>
        /// それまでのステップで一時保存したデータを動的に埋め込みます。
        /// &lt;Current:[key名]&gt; を含まない場合は文字列をそのまま返します。
        /// </summary>
        /// <param name="value">
        ///   <list type="number">
        ///     <item>
        ///       <term>Current</term>
        ///       <description>
        ///         &lt;Current:【*1】&gt;
        ///         【*1】：一時保存した際のキー
        ///       </description>
        ///     </item>
        ///   </list>
        /// </param>
        /// <returns></returns>
        protected virtual string ConvertCurrentData(string value)
        {
            var strValue = value;
            while (true)
            {
                var match = Regex.Match(strValue, @"<Current:([a-zA-Z0-9_-]+)>", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    var key = match.Groups[1].Value;
                    var tempValue = ReceiveCurrentData(key).ToString();
                    strValue = strValue.Replace(match.Captures[0].Value, tempValue);
                }
                else
                {
                    break;
                }
            }
            return strValue;
        }

        /// <summary>
        /// Spec Flow Table中の文字列を置き換える開発者定義のFunc。
        /// &lt;NULL&rt;&lt;%21NULL&gt;は予約済みなので使用禁止。
        /// </summary>
        protected virtual List<Func<string, string>> ConvertSpecialStrings { get; set; }

        /// <summary>
        /// SpecFlowTableをIEnumerable&lt;dynamic&gt;にします。
        /// </summary>
        /// <param name="oldTable">Table</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        protected virtual IEnumerable<dynamic> CreateDynamicSet(Table oldTable)
        {
            var newTable = new Table(oldTable.Header.ToArray());

            var set = oldTable.CreateDynamicSet();

            foreach (var row in set)
            {
                var newRow = new List<string>();

                foreach (var cell in row as IDictionary<string, object>)
                {
                    var s = cell.Value.ToString();
                    ConvertSpecialStrings.ForEach(f => s = f(s));
                    newRow.Add(s);
                }
                newTable.AddRow(newRow.ToArray());
            }

            return newTable.CreateDynamicSet();
        }
        #endregion

        #region Scenario SetUp/TearDown


        /// <summary>
        /// シナリオ実行前処理
        /// </summary>
        [BeforeScenario()]
        protected void BeforeScenario()
        {
            ConvertSpecialStrings = new List<Func<string, string>>();
            ConvertSpecialStrings.Add(ConvertCurrentData);
        }

        /// <summary>
        /// シナリオ完了後処理
        /// </summary>
        [AfterScenario()]
        protected void AfterScenario()
        {
        }

        #endregion

        #region Given Methods

        /// <summary>
        /// ツール実行前準備を行う
        /// </summary>
        [Given(@"ツール実行前準備を行う")]
        public void InitProperties()
        {
            // ログ出力先フォルダを削除する
            if (Directory.Exists(LogPath))
            {
                Directory.Delete(LogPath, true);
            }

            // confフォルダ内を削除する
            var confInfo = new DirectoryInfo(ConfPath);
            foreach (var file in confInfo.GetFiles())
            {
                file.Delete();
            }

            // オリジナルのシークレットファイルをコピーする
            FileSystem.CopyFile(
                Path.Combine(OrgConfPath, Settings.Default.SecretFileName),
                Path.Combine(ConfPath, Settings.Default.SecretFileName));

            // PropertyFileを削除する
            Given(@"「CARADAID登録」ツールのプロパティTSVファイルを削除する");
            Given(@"「ユーザー情報リセット」ツールのプロパティTSVファイルを削除する");

            // オリジナルの設定ファイルをコピーする(CARADAID登録ツール用)
            FileSystem.CopyFile(
                Path.Combine(OrgPropPath, Settings.Default.UserRegistPropFileName),
                Path.Combine(PropPath, Settings.Default.UserRegistPropFileName));

            // オリジナルの設定ファイルをコピーする(ユーザー情報リセットツール用)
            FileSystem.CopyFile(
                Path.Combine(OrgPropPath, Settings.Default.ResetUserInfoPropFileName),
                Path.Combine(PropPath, Settings.Default.ResetUserInfoPropFileName));

            // inフォルダを削除する
            if (Directory.Exists(InPath))
            {
                Directory.Delete(InPath, true);
            }

            // オリジナルの入力ファイルフォルダをコピーする
            FileSystem.CopyDirectory(OrgInPath, InPath);

            // 出力TSVファイル名の検証に使うのでScenarioContextに入力TSVファイル名を保存する
            // シナリオで入力ファイルを用意する場合はExecuteCreateInputFileで上書きされる
            // inフォルダに複数存在する場合でも構わず最初に取得したファイル名を保存しておく
            var inInfo = new DirectoryInfo(InPath);
            foreach (var file in inInfo.GetFiles())
            {
                SaveCurrentData("InputFileName", Path.GetFileName(file.FullName));
                break;
            }
        }

        /// <summary>
        /// inフォルダ内を削除する
        /// </summary>
        [Given(@"inフォルダ内を削除する")]
        public void DeleteInInFolder()
        {
            // inフォルダ内を削除する
            var inInfo = new DirectoryInfo(InPath);
            foreach (var file in inInfo.GetFiles())
            {
                file.Delete();
            }
        }

        /// <summary>
        /// confフォルダ内を削除する
        /// </summary>
        [Given(@"confフォルダ内を削除する")]
        public void DeleteInConfFolder()
        {
            // confフォルダ内を削除する
            var confInfo = new DirectoryInfo(ConfPath);
            foreach (var file in confInfo.GetFiles())
            {
                file.Delete();
            }
        }

        /// <summary>
        /// プロパティTSVファイルを削除する(ツール名でプロパティを指定する版)
        /// </summary>
        [Given(@"「(.*)」ツールのプロパティTSVファイルを削除する")]
        public void DeleteTargetToolPropertyFile(string toolName)
        {
            // PropertyFileを削除する
            File.Delete(Path.Combine(PropPath, GetTargetToolPropertyFileName(toolName)));
        }

        /// <summary>
        /// 空の入力TSVファイルを作成する
        /// </summary>
        /// <param name="fileName">ファイル名</param>
        [Given(@"空の入力TSVファイルを「(.*)」というファイル名で作る")]
        public void TouchInputFile(string fileName)
        {
            Given(@"inフォルダ内を削除する");
            StreamWriter toucher = new StreamWriter(Path.Combine(InPath, fileName));
            toucher.Close();
        }

        /// <summary>
        /// 入力TSVファイルをinフォルダの中身を削除しないで指定ファイル名で新しく作成する
        /// </summary>
        /// <param name="fileName">ファイル名</param>
        /// <param name="table">table</param>
        [Given(@"入力TSVファイルを「(.*)」というファイル名で、inフォルダ内のファイルを残して次のテーブル通りに作る")]
        public void CreateInputFileNotDeleteInFolder(string fileName, TechTalk.SpecFlow.Table table)
        {
            ExecuteCreateInputFile(fileName, table);
        }

        /// <summary>
        /// 入力TSVファイルを指定の名前で作る
        /// </summary>
        /// <param name="fileName">ファイル名</param>
        /// <param name="table">table</param>
        [Given(@"入力TSVファイルを「(.*)」というファイル名で次のテーブル通りに作る")]
        public void CreateInputFile(string fileName, TechTalk.SpecFlow.Table table)
        {
            Given(@"inフォルダ内を削除する");
            ExecuteCreateInputFile(fileName, table);
        }

        /// <summary>
        /// 対象ツールの設定ファイル(シークレット情報TSV・プロパティTSV)ファイルを指定の名前で作る.
        /// </summary>
        /// <param name="toolName">ツール名</param>
        /// <param name="fileName">シークレット情報 or プロパティ</param>
        /// <param name="table">table</param>
        [Given(@"「(.*)」ツールの「(.*)」TSVファイルを次のテーブル通りに作る")]
        public void CreatePropertyFile(string toolName, string fileName, TechTalk.SpecFlow.Table table)
        {
            try
            {
                var filePath = "";
                if ("シークレット情報".Equals(fileName))
                {
                    filePath = Path.Combine(ConfPath, Settings.Default.SecretFileName);
                    Given(@"confフォルダ内を削除する");
                }
                else if ("プロパティ".Equals(fileName))
                {
                    filePath = Path.Combine(PropPath, GetTargetToolPropertyFileName(toolName));
                    Given(@"「" + toolName + "」ツールのプロパティTSVファイルを削除する");
                }
                else
                {
                    throw new SpecFlowException("不正な設定ファイル名が指定されました。");
                }

                var entities = CreateDynamicSet(table);

                using (var sw = new StreamWriter(filePath, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    foreach (var entity in entities)
                    {
                        var line = "";
                        var dic = entity as IDictionary<string, object>;
                        line = dic["key"] + "\t" + dic["value"];
                        sw.WriteLine(line);
                    }
                }
            }
            catch (Exception e)
            {
                throw new SpecFlowException("設定ファイル作成でエラーが発生しました。", e);
            }
        }

        /// <summary>
        /// EXEを実行する(CARADAID発行ツール用)
        /// </summary>
        [Given(@"EXEを実行する")]
        public void ExecExe()
        {
            outputBuilder.Clear();

            var p = new Process();
            p.StartInfo.FileName = Path.Combine(TestHelper.TestConfiguration.ExePath, "CaradaTool.exe");

            // ITが動いてる風に見せる時はこのプロパティをfalseにする.
            p.StartInfo.CreateNoWindow = true;

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;

            // 標準出力受取ハンドラ設定
            p.OutputDataReceived += OutputHandler;

            // プロセス起動
            p.Start();
            p.BeginOutputReadLine();
            p.WaitForExit();
            p.Dispose();

            // 標準出力書き出し
            var output = outputBuilder.ToString();
            output = output.Replace("\r\r\n", "\n");

            // 全結果のベースフォルダーを取得する
            var resultBasePath = Path.Combine(Settings.Default.TestResultBasePath, FeatureContext.Current.FeatureInfo.Title, ScenarioContext.Current.ScenarioInfo.Title, DateTime.Now.ToString("yyyyMMdd_HH_mmss_fff"));
            if (!Directory.Exists(resultBasePath))
            {
                Directory.CreateDirectory(resultBasePath);
            }
            // DOS窓(標準出力)ファイルパス
            var stdOutPath = Path.Combine(resultBasePath, Settings.Default.ResultDosFileName);
            using (var writer = new StreamWriter(stdOutPath, true, Encoding.GetEncoding("Shift_JIS")))
            {
                writer.WriteLine(output);
            }

            // ScenarioContextに標準出力を保存したファイルのパスを保存する
            SaveCurrentData("DosPath", stdOutPath);

            // ログが1つだけか一応確認する
            var files = Directory.GetFiles(LogPath);
            if (files.Length != 1)
            {
                throw new SpecFlowException("EXEが出力したログファイルが特定できないので処理を中止します。");
            }

            // EXEのログファイル名を取得する
            var logfileName = Path.GetFileName(files[0]);


            // ログをTestResultsフォルダへ移動する
            FileSystem.MoveFile(
                Path.Combine(LogPath, logfileName),
                Path.Combine(resultBasePath, logfileName));

            // ScenarioContextに移動したログファイルのパスを保存する
            SaveCurrentData("LogPath", Path.Combine(resultBasePath, logfileName));

            // 結果ファイルが1つだけか一応確認する
            var outFiles = Directory.GetFiles(OutPath);
            if (outFiles.Length > 0)
            {
                if (outFiles.Length > 1)
                {
                    throw new SpecFlowException("EXEが出力した結果ファイルが特定できないので処理を中止します。");
                }

                // 結果ファイル名を取得する
                var outfileName = Path.GetFileName(outFiles[0]);

                // 結果ファイルをTestResultsフォルダへ移動する
                FileSystem.MoveFile(
                    Path.Combine(OutPath, outfileName),
                    Path.Combine(resultBasePath, outfileName));

                // ScenarioContextに移動したログファイルのパスを保存する
                SaveCurrentData("OutPath", Path.Combine(resultBasePath, outfileName));
            }

            // テスト時の入力TSV・JWT用シークレット情報TSV・ツール用プロパティTSVを保存する
            var inInfo = new DirectoryInfo(InPath);
            foreach (var file in inInfo.GetFiles())
            {
                FileSystem.CopyFile(file.FullName, Path.Combine(resultBasePath, @"inputfile\" + Path.GetFileName(file.FullName)));
            }
            var confInfo = new DirectoryInfo(ConfPath);
            foreach (var file in confInfo.GetFiles())
            {
                FileSystem.CopyFile(file.FullName, Path.Combine(resultBasePath, @"inputfile\" + Path.GetFileName(file.FullName)));
            }
            if (File.Exists(Path.Combine(PropPath, Settings.Default.UserRegistPropFileName)))
            {
                FileSystem.CopyFile(Path.Combine(PropPath, Settings.Default.UserRegistPropFileName), Path.Combine(resultBasePath, @"inputfile\" + Settings.Default.UserRegistPropFileName));
            }
        }

        /// <summary>
        /// EXEを実行する(ユーザー情報リセットツール用)
        /// </summary>
        /// <param name="caradaId">CARADA ID</param>
        /// <param name="initialization">初期化フラグ</param>
        /// <param name="resetConfirm">リセット確認</param>
        /// <remarks>初期化フラグを「未入力」とした場合、CARADA ID入力後にプロセスを強制終了している。</remarks>
        [Given(@"CARADAID「(.*)」、初期化フラグ「(.*)」、リセット確認「(.*)」としてユーザー情報リセットツールを実行する")]
        public void ExecResetUserInfoTool(string caradaId, string initialization, string resetConfirm)
        {
            var doExit = false;
            if (initialization.Equals("未入力"))
            {
                doExit = true;
            }
            ExecResetUserInfoTool(caradaId, initialization, resetConfirm, doExit);
        }

        /// <summary>
        /// 何秒待ち
        /// </summary>
        [Given(@"「(.*)」秒待ち")]
        public void WaitSecond(int seconds)
        {
            Thread.Sleep(seconds * 1000);
        }

        #endregion

        #region Then Methods

        /// <summary>
        /// 標準出力ファイルの最新から数えた対象出力回に期待値が出力されていること。
        /// 改行は無視する。
        /// </summary>
        /// <param name="numberofwrote">最後から数えた出力回数</param>
        /// <param name="target">期待値</param>
        [Then(@"DOS窓の最後から「([0-9]+)」回目の出力に「(.*)」を含むこと")]
        public void ThenStdOutDisplayed(int numberofwrote, string target)
        {
            var line = GetTargetToolStdOutLine(ScenarioContext.Current["Current:DosPath"].ToString(), numberofwrote);

            Assert.IsTrue(line.Contains(ConvertCurrentData(target)), "【" + line + "】に「" + ConvertCurrentData(target) + "」を含みません。");
        }

        /// <summary>
        /// ログレベルごとの最新から数えた対象出力回に期待値が出力されていること。
        /// 改行は無視する。
        /// </summary>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="numberofwrote">最後から数えた出力回数</param>
        /// <param name="target">期待値</param>
        [Then(@"ログのログレベル「(.*)」の最後から「([0-9]+)」回目の出力に「(.*)」を含むこと")]
        public void ThenApplicationLogWritten(string loglevel, int numberofwrote, string target)
        {
            var line = GetTargetToolLogLine(ScenarioContext.Current["Current:LogPath"].ToString(), loglevel, numberofwrote);

            Assert.IsTrue(line.Contains(ConvertCurrentData(target)), "【" + line + "】に「" + ConvertCurrentData(target) + "」を含みません。");
        }

        /// <summary>
        /// 出力TSVファイルがfeature指定のテーブル通りになっていること.
        /// </summary>
        /// <param name="table">table</param>
        [Then(@"出力TSVファイルが以下のテーブル通りになっていること")]
        public void ThenOutputTsvFileWritten(TechTalk.SpecFlow.Table table)
        {
            var tsvLines = GetTargetToolOutLines(ScenarioContext.Current["Current:OutPath"].ToString(), CaradaIdRegistHeader);
            TsvMatch(tsvLines, table);
        }

        protected void TsvMatch(List<string[]> tsvLines, TechTalk.SpecFlow.Table table)
        {
            var entities = CreateDynamicSet(table);
            var tableList = new List<string[]>();
            foreach (var entity in entities)
            {
                var dic = entity as IDictionary<string, object>;
                var list = new List<string>();

                foreach (var header in CaradaIdRegistHeader)
                {
                    // Tableのキーにスペースを含むとなぜかtrim後、後続の文字がキャメルされてしまうので対応
                    var key = header;
                    if ("CARADA ID".Equals(header))
                    {
                        key = "CARADAId";
                    }
                    Assert.IsTrue(dic.ContainsKey(key), "CARADAID発行結果TSVの想定結果に無効なヘッダーが含まれています。");
                    var val = "<NULL>".Equals(dic[key].ToString()) ? null : dic[key].ToString();
                    list.Add(val);
                }
                tableList.Add(list.ToArray());
            }
            Assert.IsTrue((tsvLines.Count == tableList.Count), "CARADAID発行結果TSVが想定結果件数と一致しません。");

            for (int i = 0; i < tableList.Count; i++)
            {
                Assert.IsTrue(tableList[i].SequenceEqual(tsvLines[i]), string.Format("{0}行目のCARADAID発行結果TSVが想定結果と一致しません。", (i + 1)));
            }
        }

        /// <summary>
        /// 出力TSVファイルがfeature指定のテーブル通りになっていること.
        /// </summary>
        /// <param name="table">table</param>
        [Then(@"ユーザー情報リセット結果TSVファイルが以下のテーブル通りになっていること")]
        public void ThenOutputResetUserInfoTsvFileWritten(TechTalk.SpecFlow.Table table)
        {
            var tsvLines = GetTargetToolOutLines(ScenarioContext.Current["Current:OutPath"].ToString(), ResetUserInfoHeader);
            ResetUserInfoTsvMatch(tsvLines, table);
        }

        protected void ResetUserInfoTsvMatch(List<string[]> tsvLines, TechTalk.SpecFlow.Table table)
        {
            var entities = CreateDynamicSet(table);
            var tableList = new List<string[]>();
            foreach (var entity in entities)
            {
                var dic = entity as IDictionary<string, object>;
                var list = new List<string>();

                foreach (var header in ResetUserInfoHeader)
                {
                    // Tableのキーにスペースを含むとなぜかtrim後、後続の文字がキャメルされてしまうので対応
                    var key = header;
                    if ("CARADA ID".Equals(header))
                    {
                        key = "CARADAId";
                    }
                    Assert.IsTrue(dic.ContainsKey(key), "ユーザー情報リセット結果TSVファイルの想定結果に無効なヘッダーが含まれています。");
                    var val = "<NULL>".Equals(dic[key].ToString()) ? null : dic[key].ToString();
                    list.Add(val);
                }
                tableList.Add(list.ToArray());
            }
            Assert.IsTrue((tsvLines.Count == tableList.Count), "ユーザー情報リセット結果TSVファイルが想定結果件数と一致しません。");

            for (int i = 0; i < tableList.Count; i++)
            {
                Assert.IsTrue(tableList[i].SequenceEqual(tsvLines[i]), string.Format("{0}行目のユーザー情報リセット結果TSVファイルが想定結果と一致しません。", (i + 1)));
            }
        }

        [Then(@"出力TSVファイルが出力されていないこと")]
        public void ThenOutputTsvFileNotExist()
        {
            var outFiles = Directory.GetFiles(OutPath);
            Assert.IsTrue((outFiles.Length == 0), "出力TSVファイルが存在します。");
        }

        [Then(@"出力TSVファイル名が正しいこと")]
        public void ThenOutputTsvFileNameCorrect()
        {
            // 拡張子を除外した入力TSVファイル名、ログファイル名、出力TSVファイル名を取得
            var inFile = Path.GetFileNameWithoutExtension(
                ScenarioContext.Current["Current:InputFileName"].ToString());
            var logFile = Path.GetFileNameWithoutExtension(
                ScenarioContext.Current["Current:LogPath"].ToString());
            var outFile = Path.GetFileNameWithoutExtension(
                ScenarioContext.Current["Current:OutPath"].ToString());

            // 正しい年月日時分秒ミリ秒かの検証までは行わない(システムが付ける時間なので)
            var pattern = @"^" + inFile + @"結果_\d{17}\.tsv$";
            Assert.IsTrue((Regex.IsMatch(outFile + ".tsv", pattern)), "出力TSVファイル名の前半が入力TSVファイル名に合致しません。");

            // 日時部分がログファイル名の日時部分と合致すること
            Assert.AreEqual(outFile.Split('_')[3], logFile.Split('_')[1], "出力TSVファイル名の日時とログファイル名の日時が異なります。");
        }

        [Then(@"ユーザー情報リセット結果TSVファイル名が正しいこと")]
        public void ThenOutputResetUserInfoTsvFileNameCorrect()
        {
            // 拡張子を除外したログファイル名、出力TSVファイル名を取得
            var logFile = Path.GetFileNameWithoutExtension(
                ScenarioContext.Current["Current:LogPath"].ToString());
            var outFile = Path.GetFileNameWithoutExtension(
                ScenarioContext.Current["Current:OutPath"].ToString());

            // 正しい年月日時分秒ミリ秒かの検証までは行わない(システムが付ける時間なので)
            var pattern = @"^ユーザー情報リセット結果_\d{17}\.tsv$";
            Assert.IsTrue((Regex.IsMatch(outFile + ".tsv", pattern)), "出力TSVファイル名の前半が入力TSVファイル名に合致しません。");

            // 日時部分がログファイル名の日時部分と合致すること
            Assert.AreEqual(outFile.Split('_')[1], logFile.Split('_')[1], "出力TSVファイル名の日時とログファイル名の日時が異なります。");
        }

        #endregion

        #region APIMock

        /// <summary>
        /// MockSettingにモックデータを投入する。
        /// </summary>
        /// <param name="table">table</param>
        [Given(@"APIモックテーブルへ次のデータを登録する")]
        public void GivenApiMockInsert(Table table)
        {
            var mockAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=" + Settings.Default.MockStorageAccount + ";AccountKey=" + Settings.Default.MockStorageAccountKey);
            TableStorageInsert("MockSetting", table, mockAccount);
        }

        /// <summary>
        /// MockSettingからモックデータを削除する。
        /// </summary>
        /// <param name="table">table</param>
        [Given(@"APIモックテーブルから次のデータを削除する")]
        public void GivenApiMockDelete(Table table)
        {
            var mockAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=" + Settings.Default.MockStorageAccount + ";AccountKey=" + Settings.Default.MockStorageAccountKey);
            TableStorageDelete("MockSetting", table, mockAccount);
        }

        #endregion

        #region Table Storage

        /// <summary>
        /// Table Storageへデータを登録する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">table</param>
        /// <param name="account">アカウント</param>
        protected virtual void TableStorageInsert(string tableName, Table table, CloudStorageAccount account)
        {
            var entities = CreateDynamicSet(table);

            var cloudTable = account.CreateCloudTableClient().GetTableReference(tableName);

            foreach (var entity in entities)
            {
                var tableEntity = new DynamicTableEntity();

                var dic = entity as IDictionary<string, object>;

                foreach (var t in (from s in dic
                                   where s.Key != "Timestamp"
                                   select s))
                {
                    if (t.Value.ToString() == "<NULL>")
                    {
                        tableEntity[t.Key] = EntityProperty.CreateEntityPropertyFromObject(null);
                    }
                    else
                    {

                        var value = ConvertTableValueType(t.Value.ToString());
                        if (t.Key == "PartitionKey")
                        {
                            tableEntity.PartitionKey = (string)value;
                        }
                        else if (t.Key == "RowKey")
                        {
                            tableEntity.RowKey = (string)value;
                        }
                        else
                        {
                            if (value.GetType() == typeof(double))
                            {
                                tableEntity[t.Key] = new EntityProperty((double)value);
                            }
                            else if (value.GetType() == typeof(DateTime))
                            {
                                tableEntity[t.Key] = new EntityProperty((DateTime)value);
                            }
                            else if (value.GetType() == typeof(DateTimeOffset))
                            {
                                tableEntity[t.Key] = new EntityProperty((DateTimeOffset)value);
                            }
                            else
                            {
                                tableEntity[t.Key] = new EntityProperty((string)value);
                            }
                        }
                    }
                }

                cloudTable.Execute(TableOperation.InsertOrReplace(tableEntity));
            }
        }

        /// <summary>
        /// Table Storageからキーにマッチするデータを削除する。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">table</param>
        /// <param name="account">アカウント</param>
        protected virtual void TableStorageDelete(string tableName, Table table, CloudStorageAccount account)
        {
            var entities = CreateDynamicSet(table);

            var cloudTable = account.CreateCloudTableClient().GetTableReference(tableName);

            foreach (var row in entities)
            {
                var conditions = (from kv in row as IDictionary<string, object>
                                  select TableQuery.GenerateFilterCondition(kv.Key, QueryComparisons.Equal, (string)ConvertTableValueType(kv.Value.ToString())));

                var where = conditions.Aggregate(conditions.First(), (a, b) => TableQuery.CombineFilters(a, TableOperators.And, b));

                var query = new TableQuery<DynamicTableEntity>() { FilterString = where };
                foreach (var tableEntity in cloudTable.ExecuteQuery(query))
                {
                    tableEntity.ETag = "*";
                    cloudTable.Execute(TableOperation.Delete(tableEntity));
                }
            }
        }

        /// <summary>
        /// 頭に@が付いたvalue値を文字列にして返す。
        /// 頭に#が付いたvalue値をdouble型にして返す。
        /// 頭に*が付いたvalue値をDateTimeOffset型にして返す。この場合、Offset付き文字列でない場合は、実行環境デフォルトのOffsetになります。
        /// それ以外は文字列とする
        /// </summary>
        /// <param name="value">value</param>
        /// <returns>型変換したvalue</returns>
        protected virtual object ConvertTableValueType(string value)
        {
            var strValue = value.ToString();

            if (Regex.IsMatch(strValue, "^@[^@]"))
            {
                return strValue.Substring(1);
            }
            else if (Regex.IsMatch(strValue, "^#[^#]"))
            {
                return double.Parse(strValue.Substring(1));

            }
            else if (Regex.IsMatch(strValue, @"^\*[^*]"))
            {
                return DateTimeOffset.Parse(strValue.Substring(1));
            }
            return strValue.Replace("@@", "@").Replace("##", "#").Replace("**", "*");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// 結果TSVファイルを取得する
        /// </summary>
        /// <param name="outputTsvFullPath">結果TSVファイルのフルパス</param>
        /// <returns>結果TSVファイル</returns>
        protected virtual List<string[]> GetTargetToolOutLines(string outputTsvFullPath, string[] header)
        {
            var lines = new List<string[]>();

            if (string.IsNullOrEmpty(outputTsvFullPath))
            {
                return lines;
            }

            var data = File.ReadAllText(outputTsvFullPath, Encoding.GetEncoding("Shift_JIS"));
            var splitted = data.Split(new[] { "\r\n" }, StringSplitOptions.None);

            Assert.IsTrue(splitted.Length > 0, "結果TSVファイルが空です。");

            foreach (var item in splitted.Select((line, i) => new { line, i }))
            {
                var cols = item.line.Split(new[] { "\t" }, StringSplitOptions.None);

                // Header行
                if (item.i == 0)
                {
                    Assert.IsTrue(header.SequenceEqual(cols), "結果TSVのヘッダーに誤りがあります。");
                    continue;
                }

                var list = new List<string>();

                foreach (var col in cols.Select((v, i) => new { v, i }))
                {
                    // CARADA IDがnullの場合は空行とみなす
                    if ((col.i == 0) && string.IsNullOrEmpty(col.v))
                    {
                        // 末尾の空行は無視
                        if (item.i == (splitted.Length - 1))
                        {
                            break;
                        }
                    }
                    list.Add(col.v);
                }
                if (list.Count > 0)
                {
                    lines.Add(list.ToArray());
                }
            }
            return lines;
        }

        private string[] GetEmptyArray(int length)
        {
            string[] ret = new string[length];
            for (int i = 0; i < length; i++)
            {
                ret[i] = string.Empty;
            }
            return ret;
        }

        /// <summary>
        /// 標準出力ファイルの直前からn回数出力した対象ログレベル行を返す。
        /// </summary>
        /// <param name="stdOutFullPath">標準出力ファイルのフルパス</param>
        /// <param name="numberofwrote">最後から数えた何回目の出力か</param>
        /// <returns>対象行</returns>
        protected virtual string GetTargetToolStdOutLine(string stdOutFullPath, int numberofwrote)
        {
            if (numberofwrote <= 0)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(stdOutFullPath))
            {
                return string.Empty;
            }

            var data = File.ReadAllText(stdOutFullPath, Encoding.GetEncoding("Shift_JIS"));
            var splitted = data.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            var cnt = 0;
            // traditional loop
            for (int i = splitted.Length - 1; i >= 0; i--)
            {
                cnt++;
                if (numberofwrote == cnt)
                {
                    return splitted[i];
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 指定ログファイルの直前からn回数出力した対象ログレベル行を返す。
        /// </summary>
        /// <param name="logFullPath">ログファイルのフルパス</param>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="numberofwrote">最後から数えた何回目の出力か</param>
        /// <returns>対象行</returns>
        protected virtual string GetTargetToolLogLine(string logFullPath, string loglevel, int numberofwrote)
        {
            if (numberofwrote <= 0)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(logFullPath))
            {
                return string.Empty;
            }

            var data = File.ReadAllText(logFullPath, Encoding.GetEncoding("Shift_JIS"));

            var work = "!aaaaaaaaaaa!";
            data = data.Replace("\r\n[", work).Replace("\r\n", "\t").Replace(work, "\r\n[");
            var splitted = data.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            var cnt = 0;
            // traditional loop
            for (int i = splitted.Length - 1; i >= 0; i--)
            {
                if (splitted[i].IndexOf("[" + loglevel + "]", StringComparison.OrdinalIgnoreCase) > 0)
                {
                    cnt++;
                    if (numberofwrote == cnt)
                    {
                        return splitted[i];
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 入力TSVファイルを指定の名前で作る
        /// </summary>
        /// <param name="fileName">ファイル名</param>
        /// <param name="table">table</param>
        private void ExecuteCreateInputFile(string fileName, TechTalk.SpecFlow.Table table)
        {
            try
            {
                var entities = CreateDynamicSet(table);
                var filePath = Path.Combine(InPath, fileName);

                using (var sw = new StreamWriter(filePath, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    var lineCount = 0;
                    foreach (var entity in entities)
                    {
                        var header = "";
                        var line = "";
                        var colCount = 0;
                        foreach (KeyValuePair<string, object> pair in entity)
                        {
                            if (colCount != 0)
                            {
                                header += "\t";
                                line += "\t";
                            }
                            // Tableのキーにスペースを含むとなぜかtrim後、後続の文字がキャメルされてしまうので対応
                            header += ("CARADAId".Equals(pair.Key)) ? "CARADA ID" : pair.Key;
                            line += pair.Value;
                            colCount++;
                        }
                        if (lineCount == 0)
                        {
                            sw.WriteLine(header);
                        }
                        sw.WriteLine(line);
                        lineCount++;
                    }
                }
                // 出力TSVファイル名の検証に使うのでScenarioContextに入力TSVファイル名を保存する
                SaveCurrentData("InputFileName", fileName);
            }
            catch (Exception e)
            {
                throw new SpecFlowException("入力TSVファイル作成でエラーが発生しました。", e);
            }
        }

        /// <summary>
        /// 条件を満たすまで待機する（100ミリ秒毎に確認する）。
        /// タイムアウトした場合は例外をスローする。
        /// </summary>
        /// <param name="predicate">結果がtrueなら終了、falseなら待機</param>
        /// <param name="afterTimeout">タイムアウトした場合に行う処理</param>
        /// <param name="timeoutSeconds">タイムアウト時間（秒） デフォルトは10秒</param>
        private void WaitUntil(Func<bool> predicate, Action afterTimeout = null, int timeoutSeconds = 10)
        {
            var startTime = DateTime.UtcNow;
            while (!predicate())
            {
                if ((DateTime.UtcNow - startTime).TotalSeconds > timeoutSeconds)
                {
                    if (afterTimeout != null)
                    {
                        afterTimeout();
                    }
                    throw new SpecFlowException("待機時間が" + timeoutSeconds + "秒を超えました。");
                }
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// ツール名からツール用プロパティファイル名を取得する.
        /// </summary>
        /// <param name="toolName">ツール名</param>
        /// <returns>ツール用プロパティファイル名</returns>
        private string GetTargetToolPropertyFileName(string toolName)
        {
            switch (toolName)
            {
                case "CARADAID登録":
                    return Settings.Default.UserRegistPropFileName;
                case "ユーザー情報リセット":
                    return Settings.Default.ResetUserInfoPropFileName;
                default:
                    throw new SpecFlowException("不正なツール名が指定されました。");
            }
        }

        /// <summary>
        /// EXEを実行する(ユーザー情報リセットツール用)
        /// </summary>
        /// <param name="caradaId">CARADA ID</param>
        /// <param name="initialization">初期化フラグ</param>
        /// <param name="resetConfirm">リセット確認</param>
        /// <param name="doExit">
        /// CARADA ID入力後、プロセスを強制終了する場合、true</param>
        private void ExecResetUserInfoTool(string caradaId, string initialization, string resetConfirm, bool doExit)
        {
            outputBuilder.Clear();

            var p = new Process();
            p.StartInfo.FileName = Path.Combine(TestHelper.TestConfiguration.ExePath, "CaradaTool.exe");

            // ITが動いてる風に見せる時はこのプロパティをfalseにする.
            p.StartInfo.CreateNoWindow = true;

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.Arguments = "ResetUserInfoApiJob";

            // 標準出力受取ハンドラ設定
            p.OutputDataReceived += OutputHandler;

            // プロセス起動
            p.Start();
            p.BeginOutputReadLine();

            // 待つというよりSleepさせて吐かせる
            Given(@"「1」秒待ち");

            if (!p.HasExited)
            {
                // CARADA ID入力
                p.StandardInput.WriteLine(caradaId);

                Given(@"「1」秒待ち");

                if (doExit)
                {
                    // 入力待ち状態になるので終了させる
                    p.Kill();
                }
            }

            if (!p.HasExited)
            {
                // 利用開始状態リセット確認入力
                p.StandardInput.WriteLine(initialization);

                Given(@"「1」秒待ち");
            }

            if (!p.HasExited)
            {
                // リセット確認入力
                p.StandardInput.WriteLine(resetConfirm);

                Given(@"「1」秒待ち");
            }

            p.WaitForExit();
            p.Dispose();

            // エビデンスを取得する
            GetResetUserInfoEvidence();
        }

        /// <summary>
        /// ユーザー情報リセットツールのエビデンスを取得する.(専用)
        /// </summary>
        private void GetResetUserInfoEvidence()
        {
            // 標準出力書き出し
            var output = outputBuilder.ToString();
            output = output.Replace("\r\r\n", "\n");

            // 全結果のベースフォルダーを取得する
            var resultBasePath = Path.Combine(Settings.Default.TestResultBasePath, FeatureContext.Current.FeatureInfo.Title, ScenarioContext.Current.ScenarioInfo.Title, DateTime.Now.ToString("yyyyMMdd_HH_mmss_fff"));
            if (!Directory.Exists(resultBasePath))
            {
                Directory.CreateDirectory(resultBasePath);
            }
            // DOS窓(標準出力)ファイルパス
            var stdOutPath = Path.Combine(resultBasePath, Settings.Default.ResultDosFileName);
            using (var writer = new StreamWriter(stdOutPath, true, Encoding.GetEncoding("Shift_JIS")))
            {
                writer.WriteLine(output);
            }

            // ScenarioContextに標準出力を保存したファイルのパスを保存する
            SaveCurrentData("DosPath", stdOutPath);

            // ログが1つだけか一応確認する
            var files = Directory.GetFiles(LogPath);
            if (files.Length != 1)
            {
                throw new SpecFlowException("EXEが出力したログファイルが特定できないので処理を中止します。");
            }

            // EXEのログファイル名を取得する
            var logfileName = Path.GetFileName(files[0]);


            // ログをTestResultsフォルダへ移動する
            FileSystem.MoveFile(
                Path.Combine(LogPath, logfileName),
                Path.Combine(resultBasePath, logfileName));

            // ScenarioContextに移動したログファイルのパスを保存する
            SaveCurrentData("LogPath", Path.Combine(resultBasePath, logfileName));

            // 結果ファイルが1つだけか一応確認する
            var outFiles = Directory.GetFiles(OutPath);
            if (outFiles.Length > 0)
            {
                if (outFiles.Length > 1)
                {
                    throw new SpecFlowException("EXEが出力した結果ファイルが特定できないので処理を中止します。");
                }

                // 結果ファイル名を取得する
                var outfileName = Path.GetFileName(outFiles[0]);

                // 結果ファイルをTestResultsフォルダへ移動する
                FileSystem.MoveFile(
                    Path.Combine(OutPath, outfileName),
                    Path.Combine(resultBasePath, outfileName));

                // ScenarioContextに移動したログファイルのパスを保存する
                SaveCurrentData("OutPath", Path.Combine(resultBasePath, outfileName));
            }

            // テスト時のJWT用シークレット情報TSV・ツール用プロパティTSVを保存する
            var confInfo = new DirectoryInfo(ConfPath);
            foreach (var file in confInfo.GetFiles())
            {
                FileSystem.CopyFile(file.FullName, Path.Combine(resultBasePath, @"inputfile\" + Path.GetFileName(file.FullName)));
            }
            if (File.Exists(Path.Combine(PropPath, Settings.Default.ResetUserInfoPropFileName)))
            {
                FileSystem.CopyFile(Path.Combine(PropPath, Settings.Default.ResetUserInfoPropFileName), Path.Combine(resultBasePath, @"inputfile\" + Settings.Default.ResetUserInfoPropFileName));
            }
        }

        /// <summary>
        /// 標準出力を受け取るハンドラ.
        /// </summary>
        /// <param name="o">object</param>
        /// <param name="args">args</param>
        private static void OutputHandler(object o, DataReceivedEventArgs args)
        {
            outputBuilder.AppendLine(args.Data);
        }

        #endregion
    }
}