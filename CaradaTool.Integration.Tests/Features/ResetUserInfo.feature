﻿Feature: ResetUserInfo
    ユーザー情報リセットツールの正常系

Background: 
    CARADA IdPのユーザー情報リセットAPIはAPIモックを使用している。
    CARADA ID入力でエラーとなり、入力待ちの状態でツールを終了したい場合は、初期化フラグを「未入力」とする。
    その後のリセット確認は通らないのでどんな値でも良いが、便宜上「未入力」としている。
    実際に未入力とする場合は空文字を設定する。

Scenario: ユーザー情報リセットツール_正常系_利用開始前状態リセット
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                           | RowKey | PostParametersJson                              | ContentType      | ResponseBody                                                                    |
    | LocalTool%2Fmanagement%2FresetUserInfo | 1      | {"carada_id":"carada_001","initialization":"1"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "initialization" : "1" } |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「Y」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「13」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「12」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「11」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「10」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「9」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「8」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「6」回目の出力に「1」を含むこと
    Then DOS窓の最後から「5」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「4」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「3」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：Y」を含むこと
    Then DOS窓の最後から「2」回目の出力に「リセットしています．．．」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常にリセットされました。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常にリセットされました。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then ユーザー情報リセット結果TSVファイル名が正しいこと
    Then ユーザー情報リセット結果TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | リセット種別       | エラーコード | エラー詳細 |
    | carada_001 | pword001   | 利用開始前リセット |              |            |

Scenario: ユーザー情報リセットツール_正常系_パスワードのみリセット
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                           | RowKey | PostParametersJson                              | ContentType      | ResponseBody                                                                    |
    | LocalTool%2Fmanagement%2FresetUserInfo | 1      | {"carada_id":"carada_001","initialization":"0"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "initialization" : "0" } |
    Given CARADAID「carada_001」、初期化フラグ「2」、リセット確認「y」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「13」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「12」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「11」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「10」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「9」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「8」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「6」回目の出力に「2」を含むこと
    Then DOS窓の最後から「5」回目の出力に「CARADA ID：carada_001、パスワードリセット」を含むこと
    Then DOS窓の最後から「4」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「3」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：y」を含むこと
    Then DOS窓の最後から「2」回目の出力に「リセットしています．．．」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常にリセットされました。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"0"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","initialization":"0"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常にリセットされました。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then ユーザー情報リセット結果TSVファイル名が正しいこと
    Then ユーザー情報リセット結果TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | リセット種別       | エラーコード | エラー詳細 |
    | carada_001 | pword001   | パスワードリセット |              |            |

Scenario: ユーザー情報リセットツール_正常系_利用開始前状態リセット確認で未入力でキャンセル
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given CARADAID「carada_001」、初期化フラグ「」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「8」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「7」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「6」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「5」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「4」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「3」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「2」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_正常系_利用開始前状態リセット確認で1、2以外でキャンセル
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given CARADAID「carada_001」、初期化フラグ「3」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「9」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「8」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「7」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「6」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「5」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「4」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「3」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「2」回目の出力に「3」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_正常系_リセット実行確認で未入力でキャンセル
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「12」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「11」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「10」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「9」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「8」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「6」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「5」回目の出力に「1」を含むこと
    Then DOS窓の最後から「4」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「3」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「2」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_正常系_リセット実行確認でY以外でキャンセル
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「A」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「12」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「11」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「10」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「9」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「8」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「6」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「5」回目の出力に「1」を含むこと
    Then DOS窓の最後から「4」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「3」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「2」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：A」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理をキャンセルしました。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then 出力TSVファイルが出力されていないこと
