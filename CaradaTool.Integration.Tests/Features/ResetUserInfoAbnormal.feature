﻿Feature: ResetUserInfoAbnormal
    ユーザー情報リセットツールの異常系

Background: 
    CARADA IdPのユーザー情報リセットAPIはAPIモックを使用している。
    CARADA ID入力でエラーとなり、入力待ちの状態でツールを終了したい場合は、初期化フラグを「未入力」とする。
    その後のリセット確認は通らないのでどんな値でも良いが、便宜上「未入力」としている。
    実際に未入力とする場合は空文字を設定する。

Scenario: ユーザー情報リセットツール_異常系_JWT用シークレット情報TSVファイルが存在しない
    Given ツール実行前準備を行う
    Given confフォルダ内を削除する
    Given CARADAID「carada_001」、初期化フラグ「未入力」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ユーザー情報リセット結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_異常系_JWT用シークレット情報TSVファイルにissなし
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「シークレット情報」TSVファイルを次のテーブル通りに作る
    | key       | value                                                            |
    | secretKey | qq$][4{9OK4U!Y#4#gL-26rW=seL&madyQP9akdKkO;O:%Y#7?.&UAXYE_[6#8>j |
    Given CARADAID「carada_001」、初期化フラグ「未入力」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：issが存在しません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：issが存在しません。)」を含むこと
    # ユーザー情報リセット結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_異常系_JWT用シークレット情報TSVファイルにsecretKeyなし
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「シークレット情報」TSVファイルを次のテーブル通りに作る
    | key       | value                                                            |
    | iss       | LnLnCommonConsumer                                               |
    Given CARADAID「carada_001」、初期化フラグ「未入力」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：secretKeyが存在しません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：secretKeyが存在しません。)」を含むこと
    # ユーザー情報リセット結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_異常系_ツール用プロパティTSVファイルが存在しない
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールのプロパティTSVファイルを削除する
    Given CARADAID「carada_001」、初期化フラグ「未入力」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：ResetUserInfoProperties.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：ResetUserInfoProperties.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ユーザー情報リセット結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_異常系_ツール用プロパティTSVファイルにUriなし
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value              |
    | client_id | 934926816000000001 |
    Given CARADAID「carada_001」、初期化フラグ「未入力」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：ResetUserInfoProperties.tsv、内容：パラメータ：Uriが存在しません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：ResetUserInfoProperties.tsv、内容：パラメータ：Uriが存在しません。)」を含むこと
    # ユーザー情報リセット結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_異常系_CARADAID未入力
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given CARADAID「」、初期化フラグ「未入力」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「5」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「4」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「3」回目の出力に「リセットする CARADA ID を入力してください。：」を含むこと
    Then DOS窓の最後から「2」回目の出力に「CARADA IDは必須です。」を含むこと
    Then DOS窓の最後から「1」回目の出力に「リセットする CARADA ID を入力してください。：」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    # ユーザー情報リセット結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_異常系_CARADAIDが不正
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given CARADAID「CARADA_001」、初期化フラグ「未入力」、リセット確認「未入力」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「5」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「4」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「3」回目の出力に「リセットする CARADA ID を入力してください。：CARADA_001」を含むこと
    Then DOS窓の最後から「2」回目の出力に「CARADA IDが不正です。」を含むこと
    Then DOS窓の最後から「1」回目の出力に「リセットする CARADA ID を入力してください。：」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    # ユーザー情報リセット結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: ユーザー情報リセットツール_異常系_レスポンスのcarada_idが合致しない
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                           | RowKey | PostParametersJson                              | ContentType      | ResponseBody                                                                    |
    | LocalTool%2Fmanagement%2FresetUserInfo | 1      | {"carada_id":"carada_001","initialization":"1"} | application/json | { "carada_id" : "carada_999", "password" : "pword001", "initialization" : "1" } |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「Y」としてユーザー情報リセットツールを実行する
    # DOS窓の検証(登録は正常に行われるので正常終了)
    Then DOS窓の最後から「13」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「12」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「11」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「10」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「9」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「8」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「6」回目の出力に「1」を含むこと
    Then DOS窓の最後から「5」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「4」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「3」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：Y」を含むこと
    Then DOS窓の最後から「2」回目の出力に「リセットしています．．．」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常にリセットされました。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_999","password":"*****","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常にリセットされました。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証(結果にエラーが出る)
    Then ユーザー情報リセット結果TSVファイル名が正しいこと
    Then ユーザー情報リセット結果TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | リセット種別       | エラーコード | エラー詳細                                       |
    | carada_001 | pword001   | 利用開始前リセット |              | CARADA IDが合致しません。(carada_001,carada_999) |

Scenario: ユーザー情報リセットツール_異常系_APIエラー
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                           | RowKey | PostParametersJson                              | ContentType      | ResponseBody                                                                                                                     | ResponseCode |
    | LocalTool%2Fmanagement%2FresetUserInfo | 1      | {"carada_id":"carada_001","initialization":"1"} | application/json | { "carada_id" : "carada_001", "error" : "invalid_carada_id", "error_description" : "CARADA IDに紐付くユーザーが存在しません。" } | 400          |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「Y」としてユーザー情報リセットツールを実行する
    # DOS窓の検証
    Then DOS窓の最後から「14」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「13」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「12」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「11」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「10」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「9」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「8」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「1」を含むこと
    Then DOS窓の最後から「6」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「5」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「4」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：Y」を含むこと
    Then DOS窓の最後から「3」回目の出力に「リセットしています．．．」を含むこと
    Then DOS窓の最後から「2」回目の出力に「エラーが発生しました。」を含むこと
    Then DOS窓の最後から「1」回目の出力に「CARADA IDに紐付くユーザーが存在しません。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「API Response : {"carada_id":"carada_001","error":"invalid_carada_id","error_description":"CARADA IDに紐付くユーザーが存在しません。"} TryCount:1」を含むこと
    Then ログのログレベル「ERROR」の最後から「2」回目の出力に「エラーが発生しました。」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「CARADA IDに紐付くユーザーが存在しません。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then ユーザー情報リセット結果TSVファイル名が正しいこと
    Then ユーザー情報リセット結果TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | リセット種別 | エラーコード      | エラー詳細                                |
    | carada_001 |            |              | invalid_carada_id | CARADA IDに紐付くユーザーが存在しません。 |

Scenario: ユーザー情報リセットツール_異常系_APIエラー_503_メンテナンス
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                           | RowKey | PostParametersJson                              | ContentType      | ResponseBody                                                                                                        | ResponseCode |
    | LocalTool%2Fmanagement%2FresetUserInfo | 1      | {"carada_id":"carada_001","initialization":"1"} | application/json | { "carada_id" : "carada_001", "error" : "maintenance", "error_description" : "現在サーバーのメンテナンス中です。" } | 503          |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「Y」としてユーザー情報リセットツールを実行する
    Then DOS窓の最後から「14」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「13」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「12」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「11」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「10」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「9」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「8」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「1」を含むこと
    Then DOS窓の最後から「6」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「5」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「4」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：Y」を含むこと
    Then DOS窓の最後から「3」回目の出力に「リセットしています．．．」を含むこと
    Then DOS窓の最後から「2」回目の出力に「エラーが発生しました。」を含むこと
    Then DOS窓の最後から「1」回目の出力に「現在サーバーのメンテナンス中です。」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「API Response : {"carada_id":"carada_001","error":"maintenance","error_description":"現在サーバーのメンテナンス中です。"} TryCount:1」を含むこと
    Then ログのログレベル「ERROR」の最後から「2」回目の出力に「エラーが発生しました。」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「現在サーバーのメンテナンス中です。」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then ユーザー情報リセット結果TSVファイル名が正しいこと
    Then ユーザー情報リセット結果TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | リセット種別 | エラーコード | エラー詳細                         |
    | carada_001 |            |              | maintenance  | 現在サーバーのメンテナンス中です。 |

Scenario: ユーザー情報リセットツール_異常系_リトライ回数オーバー
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                                                                                        |
    | Uri       | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/management/resetUserInfo |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                           | RowKey | PostParametersJson                              | ContentType      | ResponseBody | ResponseCode |
    | LocalTool%2Fmanagement%2FresetUserInfo | 1      | {"carada_id":"carada_001","initialization":"1"} | application/json |              | 504          |
    | LocalTool%2Fmanagement%2FresetUserInfo | 2      | {"carada_id":"carada_001","initialization":"1"} | application/json |              | 504          |
    | LocalTool%2Fmanagement%2FresetUserInfo | 3      | {"carada_id":"carada_001","initialization":"1"} | application/json |              | 504          |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「Y」としてユーザー情報リセットツールを実行する
    Then DOS窓の最後から「14」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「13」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「12」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「11」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「10」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「9」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「8」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「7」回目の出力に「1」を含むこと
    Then DOS窓の最後から「6」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「5」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「4」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：Y」を含むこと
    Then DOS窓の最後から「3」回目の出力に「リセットしています．．．」を含むこと
    Then DOS窓の最後から「2」回目の出力に「エラーが発生しました。」を含むこと
    Then DOS窓の最後から「1」回目の出力に「サーバーエラー」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:2」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「TryCount:2」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:3」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「TryCount:3」を含むこと
    Then ログのログレベル「ERROR」の最後から「2」回目の出力に「エラーが発生しました。」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバーエラー」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then ユーザー情報リセット結果TSVファイル名が正しいこと
    Then ユーザー情報リセット結果TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | リセット種別 | エラーコード | エラー詳細     |
    | carada_001 |            |              | 504          | サーバーエラー |

Scenario: ユーザー情報リセットツール_異常系_想定外エラー
    Given ツール実行前準備を行う
    Given 「ユーザー情報リセット」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                    |
    | Uri       | https://not.reachable.jp |
    Given CARADAID「carada_001」、初期化フラグ「1」、リセット確認「Y」としてユーザー情報リセットツールを実行する
    Then DOS窓の最後から「16」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then DOS窓の最後から「15」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「14」回目の出力に「リセットする CARADA ID を入力してください。：carada_001」を含むこと
    Then DOS窓の最後から「13」回目の出力に「ユーザー情報を利用開始前状態へリセットしますか？」を含むこと
    Then DOS窓の最後から「12」回目の出力に「1を入力：利用開始前状態へリセットする。」を含むこと
    Then DOS窓の最後から「11」回目の出力に「2を入力：パスワードのみリセットする。」を含むこと
    Then DOS窓の最後から「10」回目の出力に「未入力、その他の値を入力：キャンセル。」を含むこと
    Then DOS窓の最後から「9」回目の出力に「1」を含むこと
    Then DOS窓の最後から「8」回目の出力に「CARADA ID：carada_001、利用開始前リセット」を含むこと
    Then DOS窓の最後から「7」回目の出力に「本当にリセットしてよろしいですか？」を含むこと
    Then DOS窓の最後から「6」回目の出力に「(Yを入力：OK。未入力、Y以外を入力：キャンセル。)：Y」を含むこと
    Then DOS窓の最後から「5」回目の出力に「リセットしています．．．」を含むこと
    Then DOS窓の最後から「4」回目の出力に「何らかのエラーが発生しました。詳細をご確認ください。」を含むこと
    Then DOS窓の最後から「3」回目の出力に「1 つ以上のエラーが発生しました。」を含むこと
    # 最後から2行目はWindows7では「An error occurred while sending the request.」(英語)が、
    # Windows10では「この要求の送信中にエラーが発生しました。」(日本語)が出力される現象が起きたが、
    # 本検証は想定外のエラーが検出された時の挙動を確認するのが目的なので、この行の検証は除外する。
    Then DOS窓の最後から「1」回目の出力に「リモート名を解決できませんでした。: 'not.reachable.jp'」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「CARADA IdP ユーザー情報リセットツール」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「API Request : params:{"carada_id":"carada_001","initialization":"1"} TryCount:1」を含むこと
    Then ログのログレベル「ERROR」の最後から「2」回目の出力に「何らかのエラーが発生しました。詳細をご確認ください。」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「1 つ以上のエラーが発生しました。」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「リモート名を解決できませんでした。: 'not.reachable.jp'」を含むこと
    # ユーザー情報リセット結果TSVファイルの検証
    Then 出力TSVファイルが出力されていないこと
