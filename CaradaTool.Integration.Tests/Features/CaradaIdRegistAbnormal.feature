﻿Feature: CaradaIdRegistAbnormal
    CARADAID登録ツールの異常系

Background: 
    CARADA IdPの利用開始前ユーザー登録APIはAPIモックを使用している。

Scenario: CARADAID登録ツール_異常系_CARADAID発行TSVが存在しない
    Given ツール実行前準備を行う
    Given inフォルダ内を削除する
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：CARADAID発行TSV、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：CARADAID発行TSV、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_CARADAID発行TSVが2つ以上
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160229_CARADAID発行.tsv」というファイル名で、inフォルダ内のファイルを残して次のテーブル通りに作る
    | CARADA ID  |
    | carada_999 |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：CARADAID発行TSV、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：CARADAID発行TSV、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_CARADAID発行TSVのファイル名の法人IDが想定外
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「D990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：D990001_20160120_CARADAID発行.tsv、内容：ファイル名の形式が正しくありません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：D990001_20160120_CARADAID発行.tsv、内容：ファイル名の形式が正しくありません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_CARADAID発行TSVのファイル名の日付が不正
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160239_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：C990001_20160239_CARADAID発行.tsv、内容：ファイル名の形式が正しくありません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：C990001_20160239_CARADAID発行.tsv、内容：ファイル名の形式が正しくありません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_CARADAID発行TSVのファイル名の文言が不正
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160229_CARADAid発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：C990001_20160229_CARADAid発行.tsv、内容：ファイル名の形式が正しくありません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：C990001_20160229_CARADAid発行.tsv、内容：ファイル名の形式が正しくありません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_CARADAID発行TSVのカラム数が多い
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  | PASSWORD |
    | carada_001 | PWD001   |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：C990001_20160120_CARADAID発行.tsv、内容：ファイルのカラム数が正しくありません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：C990001_20160120_CARADAID発行.tsv、内容：ファイルのカラム数が正しくありません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_JWT用シークレット情報TSVファイルが存在しない
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given confフォルダ内を削除する
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_JWT用シークレット情報TSVファイルにissなし
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given 「CARADAID登録」ツールの「シークレット情報」TSVファイルを次のテーブル通りに作る
    | key       | value                                                            |
    | secretKey | qq$][4{9OK4U!Y#4#gL-26rW=seL&madyQP9akdKkO;O:%Y#7?.&UAXYE_[6#8>j |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：issが存在しません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：issが存在しません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_JWT用シークレット情報TSVファイルにsecretKeyなし
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given 「CARADAID登録」ツールの「シークレット情報」TSVファイルを次のテーブル通りに作る
    | key       | value                                                            |
    | iss       | LnLnCommonConsumer                                               |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：secretKeyが存在しません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Client.tsv、内容：パラメータ：secretKeyが存在しません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_ツール用プロパティTSVファイルが存在しない
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given 「CARADAID登録」ツールのプロパティTSVファイルを削除する
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Properties.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Properties.tsv、内容：ファイルが存在しないか、読み込めません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_ツール用プロパティTSVファイルにclient_idなし
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given 「CARADAID登録」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key | value                                                                                  |
    | Uri | https://dev-hclnln-apimock.cloudapp.net/ManagementApiMock/LocalTool/Management/AddUser |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Properties.tsv、内容：パラメータ：client_idが存在しません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Properties.tsv、内容：パラメータ：client_idが存在しません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_ツール用プロパティTSVファイルにUriなし
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given 「CARADAID登録」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value              |
    | client_id | 934926816000000001 |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「2」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Properties.tsv、内容：パラメータ：Uriが存在しません。)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：Properties.tsv、内容：パラメータ：Uriが存在しません。)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_異常系_carada_idが不正
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID   |
    | _carada_001 |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：1、登録された件数：0、登録されなかった件数：1)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：1、登録された件数：0、登録されなかった件数：1)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID   | パスワード | UID     | エラーコード      | エラー詳細            |
    | _carada_001 |            |         |                   | CARADA IDが不正です。 |

Scenario: CARADAID登録ツール_異常系_レスポンスのcarada_idが合致しない
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    | carada_002 |
    | carada_003 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                               |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" } |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json | { "carada_id" : "carada_999", "password" : "pword002", "uid" : "uid_002" } |
    | LocalTool%2FManagement%2FAddUser | 3      | {"client_id":"934926816000000001","carada_id":"carada_003"} | application/json | { "carada_id" : "carada_003", "password" : "pword003", "uid" : "uid_003" } |
    Given EXEを実行する
    # DOS窓の検証(登録は正常に行われるので正常終了)
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：3、登録された件数：3)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「12」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「11」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「10」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : {"carada_id":"carada_999","password":"*****","uid":"uid_002"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_003","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_003","password":"*****","uid":"uid_003"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：3、登録された件数：3)」を含むこと
    # CARADAID発行結果TSVの検証(結果にエラーが出る)
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード | エラー詳細                                       |
    | carada_001 | pword001   | uid_001 |              |                                                  |
    | carada_002 | pword002   | uid_002 |              | CARADA IDが合致しません。(carada_002,carada_999) |
    | carada_003 | pword003   | uid_003 |              |                                                  |

Scenario: CARADAID登録ツール_異常系_APIエラー_original_errorあり
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    | carada_002 |
    | carada_003 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                                                                                                                                   | ResponseCode |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" }                                                                                                     |              |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json | { "carada_id" : "carada_002", "password" : "pword002", "uid" : "uid_002" }                                                                                                     |              |
    | LocalTool%2FManagement%2FAddUser | 3      | {"client_id":"934926816000000001","carada_id":"carada_003"} | application/json | { "carada_id" : "carada_002", "error" : "invalid_client_id", "original_error" : "クライアントIDが見つかりませんでした。", "error_description" : "クライアントIDが不正です。" } | 400          |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：3、登録された件数：2、登録されなかった件数：1)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「12」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「11」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「10」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : {"carada_id":"carada_002","password":"*****","uid":"uid_002"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_003","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_002","error":"invalid_client_id","original_error":"クライアントIDが見つかりませんでした。","error_description":"クライアントIDが不正です。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：3、登録された件数：2、登録されなかった件数：1)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード      | エラー詳細                                                        |
    | carada_001 | pword001   | uid_001 |                   |                                                                   |
    | carada_002 | pword002   | uid_002 |                   |                                                                   |
    | carada_003 |            |         | invalid_client_id | クライアントIDが見つかりませんでした。 クライアントIDが不正です。 |

Scenario: CARADAID登録ツール_異常系_APIエラー_original_errorなし_CARADAID必須チェック含む
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    |            |
    | carada_003 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                                                                      | ResponseCode |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" }                                        |              |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_003"} | application/json | { "carada_id" : "carada_002", "error" : "invalid_client_id", "error_description" : "クライアントIDが不正です。" } | 400          |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：3、登録された件数：1、登録されなかった件数：2)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_003","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_002","error":"invalid_client_id","error_description":"クライアントIDが不正です。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：3、登録された件数：1、登録されなかった件数：2)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード      | エラー詳細                 |
    | carada_001 | pword001   | uid_001 |                   |                            |
    |            |            |         |                   | CARADA IDは必須です。      |
    | carada_003 |            |         | invalid_client_id | クライアントIDが不正です。 |

Scenario: CARADAID登録ツール_異常系_APIエラー_503_メンテナンス
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    | carada_002 |
    | carada_003 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                                                                        | ResponseCode |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" }                                           |              |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json | { "carada_id" : "carada_002", "error" : "maintenance", "error_description" : "現在サーバーのメンテナンス中です。" } | 503          |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：2、登録された件数：1、登録されなかった件数：1)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_002","error":"maintenance","error_description":"現在サーバーのメンテナンス中です。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：2、登録された件数：1、登録されなかった件数：1)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード | エラー詳細                         |
    | carada_001 | pword001    | uid_001 |              |                                    |
    | carada_002 |            |         | maintenance  | 現在サーバーのメンテナンス中です。 |

Scenario: CARADAID登録ツール_異常系_リトライ回数オーバー
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    | carada_002 |
    | carada_003 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                              | ResponseCode |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" } |              |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json |                                                                           | 504          |
    | LocalTool%2FManagement%2FAddUser | 3      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json |                                                                           | 504          |
    | LocalTool%2FManagement%2FAddUser | 4      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json |                                                                           | 504          |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：2、登録された件数：1、登録されなかった件数：1)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「13」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「12」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「11」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「10」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:2」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「TryCount:2」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:3」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「TryCount:3」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：2、登録された件数：1、登録されなかった件数：1)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード | エラー詳細     |
    | carada_001 | pword001    | uid_001 |              |                |
    | carada_002 |            |         | 504          | サーバーエラー |

Scenario: CARADAID登録ツール_異常系_想定外エラー
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    Given 「CARADAID登録」ツールの「プロパティ」TSVファイルを次のテーブル通りに作る
    | key       | value                    |
    | client_id | 934926816000000001       |
    | Uri       | https://not.reachable.jp |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「6」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「5」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「4」回目の出力に「何らかのエラーが発生しました。詳細をご確認ください。」を含むこと
    Then DOS窓の最後から「3」回目の出力に「1 つ以上のエラーが発生しました。」を含むこと
    # 最後から2行目はWindows7では「An error occurred while sending the request.」(英語)が、
    # Windows10では「この要求の送信中にエラーが発生しました。」(日本語)が出力される現象が起きたが、
    # 本検証は想定外のエラーが検出された時の挙動を確認するのが目的なので、この行の検証は除外する。
    Then DOS窓の最後から「1」回目の出力に「リモート名を解決できませんでした。: 'not.reachable.jp'」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「ERROR」の最後から「2」回目の出力に「何らかのエラーが発生しました。詳細をご確認ください。」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「1 つ以上のエラーが発生しました。」を含むこと
    Then ログのログレベル「ERROR」の最後から「1」回目の出力に「リモート名を解決できませんでした。: 'not.reachable.jp'」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと
