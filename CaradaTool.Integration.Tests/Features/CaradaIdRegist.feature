﻿Feature: CaradaIdRegist
    CARADAID登録ツールの正常系

Background: 
    CARADA IdPの利用開始前ユーザー登録APIはAPIモックを使用している。

Scenario: CARADAID登録ツール_正常系
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    | carada_002 |
    | carada_003 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                               |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" } |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json | { "carada_id" : "carada_002", "password" : "pword002", "uid" : "uid_002" } |
    | LocalTool%2FManagement%2FAddUser | 3      | {"client_id":"934926816000000001","carada_id":"carada_003"} | application/json | { "carada_id" : "carada_003", "password" : "pword003", "uid" : "uid_003" } |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：3、登録された件数：3)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「12」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「11」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「10」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : {"carada_id":"carada_002","password":"*****","uid":"uid_002"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_003","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_003","password":"*****","uid":"uid_003"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：3、登録された件数：3)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード | エラー詳細 |
    | carada_001 | pword001   | uid_001 |              |            |
    | carada_002 | pword002   | uid_002 |              |            |
    | carada_003 | pword003   | uid_003 |              |            |

Scenario: CARADAID登録ツール_正常系_リトライ回数以内に正常レスポンス
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    | carada_002 |
    | carada_003 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                               | ResponseCode |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" } |              |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json |                                                                            | 504          |
    | LocalTool%2FManagement%2FAddUser | 3      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json |                                                                            | 504          |
    | LocalTool%2FManagement%2FAddUser | 4      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json | { "carada_id" : "carada_002", "password" : "pword002", "uid" : "uid_002" } |              |
    | LocalTool%2FManagement%2FAddUser | 5      | {"client_id":"934926816000000001","carada_id":"carada_003"} | application/json | { "carada_id" : "carada_003", "password" : "pword003", "uid" : "uid_003" } |              |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：3、登録された件数：3)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「16」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「15」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「14」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「13」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「12」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「11」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「10」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:2」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「API Response : StatusCode: 504, ReasonPhrase: 'Gateway Timeout'」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「TryCount:2」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:3」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : {"carada_id":"carada_002","password":"*****","uid":"uid_002"} TryCount:3」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_003","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_003","password":"*****","uid":"uid_003"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：3、登録された件数：3)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード | エラー詳細 |
    | carada_001 | pword001   | uid_001 |              |            |
    | carada_002 | pword002   | uid_002 |              |            |
    | carada_003 | pword003   | uid_003 |              |            |

Scenario: CARADAID登録ツール_正常系_CARADAID発行TSVが空
    Given ツール実行前準備を行う
    Given 空の入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で作る
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：0、登録された件数：0)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：0、登録された件数：0)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_正常系_CARADAID発行TSVがヘッダーのみ
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：0、登録された件数：0)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「正常に処理が終了しました。(入力件数：0、登録された件数：0)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイルが出力されていないこと

Scenario: CARADAID登録ツール_正常系_正常行エラー行混合10件
    Given ツール実行前準備を行う
    Given 入力TSVファイルを「C990001_20160120_CARADAID発行.tsv」というファイル名で次のテーブル通りに作る
    | CARADA ID  |
    | carada_001 |
    | carada_002 |
    | carada_003 |
    | carada_004 |
    | carada_005 |
    | carada_006 |
    | carada_007 |
    | carada_008 |
    | carada_009 |
    | carada_010 |
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                          | ContentType      | ResponseBody                                                                                                                                                                   | ResponseCode |
    | LocalTool%2FManagement%2FAddUser | 1      | {"client_id":"934926816000000001","carada_id":"carada_001"} | application/json | { "carada_id" : "carada_001", "password" : "pword001", "uid" : "uid_001" }                                                                                                     |              |
    | LocalTool%2FManagement%2FAddUser | 2      | {"client_id":"934926816000000001","carada_id":"carada_002"} | application/json | { "carada_id" : "carada_002", "error" : "auth_provider_error", "error_description" : "統合認可側のユーザー作製に失敗しました。" }                                              | 400          |
    | LocalTool%2FManagement%2FAddUser | 3      | {"client_id":"934926816000000001","carada_id":"carada_003"} | application/json | { "carada_id" : "carada_003", "password" : "pword003", "uid" : "uid_003" }                                                                                                     |              |
    | LocalTool%2FManagement%2FAddUser | 4      | {"client_id":"934926816000000001","carada_id":"carada_004"} | application/json | { "carada_id" : "carada_004", "error" : "invalid_client_id", "original_error" : "クライアントIDが見つかりませんでした。", "error_description" : "クライアントIDが不正です。" } | 400          |
    | LocalTool%2FManagement%2FAddUser | 5      | {"client_id":"934926816000000001","carada_id":"carada_005"} | application/json | { "carada_id" : "carada_005", "password" : "pword005", "uid" : "uid_005" }                                                                                                     |              |
    | LocalTool%2FManagement%2FAddUser | 6      | {"client_id":"934926816000000001","carada_id":"carada_006"} | application/json | { "carada_id" : "carada_006", "error" : "registered_carada_id", "error_description" : "CARADA IDが既にユーザー登録済みです。" }                                                | 400          |
    | LocalTool%2FManagement%2FAddUser | 7      | {"client_id":"934926816000000001","carada_id":"carada_007"} | application/json | { "carada_id" : "carada_007", "password" : "pword007", "uid" : "uid_007" }                                                                                                     |              |
    | LocalTool%2FManagement%2FAddUser | 8      | {"client_id":"934926816000000001","carada_id":"carada_008"} | application/json | { "carada_id" : "carada_008", "error" : "invalid_client_id", "error_description" : "クライアントIDが不正です。" }                                                              | 400          |
    | LocalTool%2FManagement%2FAddUser | 9      | {"client_id":"934926816000000001","carada_id":"carada_009"} | application/json | { "carada_id" : "carada_009", "password" : "pword009", "uid" : "uid_009" }                                                                                                     |              |
    | LocalTool%2FManagement%2FAddUser | 10     | {"client_id":"934926816000000001","carada_id":"carada_010"} | application/json | { "carada_id" : "carada_010", "error" : "invalid_carada_id", "error_description" : "CARARA IDが不正です。" }                                                                   | 400          |
    Given EXEを実行する
    # DOS窓の検証
    Then DOS窓の最後から「3」回目の出力に「CARADAID発行ツール」を含むこと
    Then DOS窓の最後から「2」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then DOS窓の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：10、登録された件数：5、登録されなかった件数：5)」を含むこと
    # ログファイルの検証
    Then ログのログレベル「INFO」の最後から「33」回目の出力に「CARADAID発行ツール」を含むこと
    Then ログのログレベル「INFO」の最後から「32」回目の出力に「処理を開始します。Issuser ID：LnLnCommonConsumer」を含むこと
    Then ログのログレベル「INFO」の最後から「31」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「30」回目の出力に「API Request : params:{"carada_id":"carada_001","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「29」回目の出力に「API Response : {"carada_id":"carada_001","password":"*****","uid":"uid_001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「28」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「27」回目の出力に「API Request : params:{"carada_id":"carada_002","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「26」回目の出力に「API Response : {"carada_id":"carada_002","error":"auth_provider_error","error_description":"統合認可側のユーザー作製に失敗しました。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「25」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「24」回目の出力に「API Request : params:{"carada_id":"carada_003","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「23」回目の出力に「API Response : {"carada_id":"carada_003","password":"*****","uid":"uid_003"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「22」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「21」回目の出力に「API Request : params:{"carada_id":"carada_004","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「20」回目の出力に「API Response : {"carada_id":"carada_004","error":"invalid_client_id","original_error":"クライアントIDが見つかりませんでした。","error_description":"クライアントIDが不正です。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「19」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「18」回目の出力に「API Request : params:{"carada_id":"carada_005","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「17」回目の出力に「API Response : {"carada_id":"carada_005","password":"*****","uid":"uid_005"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「16」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「15」回目の出力に「API Request : params:{"carada_id":"carada_006","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「14」回目の出力に「API Response : {"carada_id":"carada_006","error":"registered_carada_id","error_description":"CARADA IDが既にユーザー登録済みです。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「13」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「12」回目の出力に「API Request : params:{"carada_id":"carada_007","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「11」回目の出力に「API Response : {"carada_id":"carada_007","password":"*****","uid":"uid_007"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「10」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「9」回目の出力に「API Request : params:{"carada_id":"carada_008","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「8」回目の出力に「API Response : {"carada_id":"carada_008","error":"invalid_client_id","error_description":"クライアントIDが不正です。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「7」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「6」回目の出力に「API Request : params:{"carada_id":"carada_009","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「5」回目の出力に「API Response : {"carada_id":"carada_009","password":"*****","uid":"uid_009"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「4」回目の出力に「Execute API : http」を含むこと
    Then ログのログレベル「INFO」の最後から「3」回目の出力に「API Request : params:{"carada_id":"carada_010","client_id":"934926816000000001"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「2」回目の出力に「API Response : {"carada_id":"carada_010","error":"invalid_carada_id","error_description":"CARARA IDが不正です。"} TryCount:1」を含むこと
    Then ログのログレベル「INFO」の最後から「1」回目の出力に「処理が終了しましたが、エラーがありました。(入力件数：10、登録された件数：5、登録されなかった件数：5)」を含むこと
    # CARADAID発行結果TSVの検証
    Then 出力TSVファイル名が正しいこと
    Then 出力TSVファイルが以下のテーブル通りになっていること
    | CARADA ID  | パスワード | UID     | エラーコード         | エラー詳細                                                        |
    | carada_001 | pword001   | uid_001 |                      |                                                                   |
    | carada_002 |            |         | auth_provider_error  | 統合認可側のユーザー作製に失敗しました。                          |
    | carada_003 | pword003   | uid_003 |                      |                                                                   |
    | carada_004 |            |         | invalid_client_id    | クライアントIDが見つかりませんでした。 クライアントIDが不正です。 |
    | carada_005 | pword005   | uid_005 |                      |                                                                   |
    | carada_006 |            |         | registered_carada_id | CARADA IDが既にユーザー登録済みです。                             |
    | carada_007 | pword007   | uid_007 |                      |                                                                   |
    | carada_008 |            |         | invalid_client_id    | クライアントIDが不正です。                                        |
    | carada_009 | pword009   | uid_009 |                      |                                                                   |
    | carada_010 |            |         | invalid_carada_id    | CARARA IDが不正です。                                             |
