﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.IO;

namespace MTI.CaradaIdProvider.Web.Tests.EmailServices
{
    /// <summary>
    /// IntegrationTestEmailServiceのテストクラス
    /// </summary>
#pragma warning disable 0618
    [TestClass]
    public class IntegrationTestEmailServiceTest
    {
        /// <summary>
        /// BeforeSendに裏口を作ったテスト用クラス
        /// </summary>
        private class TestIntegrationTestEmailService : IntegrationTestEmailService
        {
            public bool BeforeSendEntrance(IdentityMessage message)
            {
                return BeforeSend(message);
            }
        }

        /// <summary>
        /// テスト対象
        /// </summary>
        private TestIntegrationTestEmailService target;

        [TestInitialize]
        public void Initialize()
        {
            target = new TestIntegrationTestEmailService();
            var mockFactory = new Mock<DbContextFactory>();
            mockFactory.Setup(s => s.NewCaradaIdPDbContext()).Throws(new NotImplementedException());
            var f = ReflectionAccessor.GetField<SendGridEmailService>("DB_CONTEXT_FACTORY");
            f.SetValue(target, mockFactory.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void BeforeSend_異常系_例外発生()
        {
            var message = new IdentityMessage();
            message.Subject = "【CARADA ID】登録完了のお知らせ";
            message.Body = "本文";
            message.Destination = "exception@test.mail.com";
            target.BeforeSendEntrance(message);
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void BeforeSend_正常系_メールアドレスは例外発生用だが件名は例外発生用でないこと()
        {
            target = new TestIntegrationTestEmailService();

            var message = new IdentityMessage();
            message.Subject = "テスト";
            message.Body = "本文";
            message.Destination = "exception@test.mail.com";
            target.BeforeSendEntrance(message);
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void BeforeSend_正常系_件名は例外発生用だがメールアドレスは例外発生用でないこと()
        {
            target = new TestIntegrationTestEmailService();

            var message = new IdentityMessage();
            message.Subject = "【CARADA ID】登録完了のお知らせ";
            message.Body = "本文";
            message.Destination = "aaa@test.mail.com";
            target.BeforeSendEntrance(message);
        }
    }
}
