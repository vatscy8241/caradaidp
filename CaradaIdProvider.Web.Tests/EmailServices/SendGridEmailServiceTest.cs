﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.Tasks;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MTI.CaradaIdProvider.Web.Tests.EmailServices
{
    [TestClass]
    public class SendGridEmailServiceTest
    {
        private class SendGridEmailServiceUTEntrance : SendGridEmailService
        {
            public SendGridEmailServiceUTEntrance() : base()
            {
                var f = ReflectionAccessor.GetField<AsyncEmailService>("processor");
                f.SetValue(this, new SyncBackGroundProcessor());
            }

            public bool BeforeSendEntrance(IdentityMessage message)
            {
                return base.BeforeSend(message);
            }
        }

        private SendGridEmailServiceUTEntrance target;
        private Mock<CaradaIdPDbContext> mockDbContext;
        private static Mock<WebRequest> mockWebRequest;
        private Mock<DbContextFactory> mockFactory;

        /// <summary>
        /// テスト対象が持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetField(string name, Object o)
        {
            FieldInfo field = ReflectionAccessor.GetField<SendGridEmailService>(name);
            field.SetValue(target, o);
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new SendGridEmailServiceUTEntrance();
            mockDbContext = new Mock<CaradaIdPDbContext>();
            mockDbContext.Setup(s => s.Users).Returns(new TestAspNetUsersDbSet());
            mockFactory = new Mock<DbContextFactory>();
            mockFactory.Setup(s => s.NewCaradaIdPDbContext()).Returns(mockDbContext.Object);
            var dbFactory = ReflectionAccessor.GetField<SendGridEmailService>("DB_CONTEXT_FACTORY");
            dbFactory.SetValue(target, mockFactory.Object);
            Func<string, WebRequest> f = u => mockWebRequest.Object;
            SetField("CreateWebRequest", f);
            mockWebRequest = new Mock<WebRequest>();

            SetField("apiUser", "dummyUser");
            SetField("apiKey", "dummyPassword");
        }

        [TestMethod]
        public void CreateWebRequestのもともとのDelegateで生成できること()
        {
            // 本物
            var target = new SendGridEmailService();
            var field = ReflectionAccessor.GetField<SendGridEmailService>("CreateWebRequest");
            var f = (Func<string, WebRequest>)field.GetValue(target);
            var request = f.Invoke("http://localhost/hoge");
            Assert.IsNotNull(request);
            Assert.AreEqual(new Uri("http://localhost/hoge"), request.RequestUri);
        }

        [TestMethod]
        public void BeforeSend_BouncesGetのリクエストで例外が発生した場合()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser" });
            mockWebRequest.Setup(s => s.GetResponse()).Throws(new WebException("dummyの例外"));

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_境界確認_BounceGetのリクエストで101番のステータスコードがレスポンスされた場合()
        {
            // 102以上199以下はない

            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser" });

            var mockHttpWebResponse = new Mock<HttpWebResponse>();
            mockHttpWebResponse.Setup(s => s.StatusCode).Returns(HttpStatusCode.SwitchingProtocols);

            mockWebRequest.Setup(s => s.GetResponse()).Returns(mockHttpWebResponse.Object);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_境界確認_BounceGetのリクエストで300番のステータスコードがレスポンスされた場合()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser" });

            var mockHttpWebResponse = new Mock<HttpWebResponse>();
            mockHttpWebResponse.Setup(s => s.StatusCode).Returns(HttpStatusCode.MultipleChoices);

            mockWebRequest.Setup(s => s.GetResponse()).Returns(mockHttpWebResponse.Object);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_BounceGetのリクエストで対象なし_境界確認の正の下限()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser" });

            var mockHttpWebResponse = new Mock<HttpWebResponse>();
            mockHttpWebResponse.Setup(s => s.StatusCode).Returns(HttpStatusCode.OK);

            mockWebRequest.Setup(s => s.GetResponse()).Returns(mockHttpWebResponse.Object);

            var json = "[]";

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(json));

            mockHttpWebResponse.Setup(s => s.GetResponseStream()).Returns(stream);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_BounceGetのリクエストで対象なし_境界確認の正の上限()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser" });

            var mockHttpWebResponse = new Mock<HttpWebResponse>();
            mockHttpWebResponse.Setup(s => s.StatusCode).Returns(HttpStatusCode.PartialContent);

            mockWebRequest.Setup(s => s.GetResponse()).Returns(mockHttpWebResponse.Object);

            var json = "[]";

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(json));

            mockHttpWebResponse.Setup(s => s.GetResponseStream()).Returns(stream);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_BounceDeleteのリクエストで例外が発生した場合()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser" });

            var mockHttpWebResponse = new Mock<HttpWebResponse>();
            mockHttpWebResponse.Setup(s => s.StatusCode).Returns(HttpStatusCode.PartialContent);

            mockWebRequest.SetupSequence(s => s.GetResponse()).Returns(mockHttpWebResponse.Object).Throws(new WebException("dummyの例外"));
            mockWebRequest.Setup(s => s.GetRequestStream()).Returns(new MemoryStream());

            var json = "[{\"email\":\"a@a.com\"}]";

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(json));

            mockHttpWebResponse.Setup(s => s.GetResponseStream()).Returns(stream);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_BounceDeleteのリクエストで200番台以外のステータスコードがレスポンスされた場合()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser" });

            var mockHttpWebResponse = new Mock<HttpWebResponse>();
            mockHttpWebResponse.SetupSequence(s => s.StatusCode).Returns(HttpStatusCode.PartialContent).Returns(HttpStatusCode.MultipleChoices);

            mockWebRequest.Setup(s => s.GetResponse()).Returns(mockHttpWebResponse.Object);
            mockWebRequest.Setup(s => s.GetRequestStream()).Returns(new MemoryStream());

            var json = "[{\"email\":\"a@a.com\"}]";

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(json));

            mockHttpWebResponse.Setup(s => s.GetResponseStream()).Returns(stream);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_正常_Emailが一致()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser", Email = "a@a.com" });

            var mockHttpWebResponse1 = new Mock<HttpWebResponse>();
            mockHttpWebResponse1.Setup(s => s.StatusCode).Returns(HttpStatusCode.OK);
            var mockHttpWebResponse2 = new Mock<HttpWebResponse>();
            mockHttpWebResponse2.Setup(s => s.StatusCode).Returns(HttpStatusCode.OK);

            mockWebRequest.SetupSequence(s => s.GetResponse()).Returns(mockHttpWebResponse1.Object).Returns(mockHttpWebResponse2.Object);
            mockWebRequest.Setup(s => s.GetRequestStream()).Returns(new MemoryStream());

            var json1 = "[{\"email\":\"a@a.com\"}]";
            var json2 = "{\"message\":\"success\"}";

            var stream1 = new MemoryStream(Encoding.UTF8.GetBytes(json1));
            var stream2 = new MemoryStream(Encoding.UTF8.GetBytes(json2));

            mockHttpWebResponse1.Setup(s => s.GetResponseStream()).Returns(stream1);
            mockHttpWebResponse2.Setup(s => s.GetResponseStream()).Returns(stream2);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse1.VerifyAll();
            mockHttpWebResponse2.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_正常_Emailが不一致()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser { Id = "id", UserName = "testUser", Email = "b@b.com" });

            var mockHttpWebResponse1 = new Mock<HttpWebResponse>();
            mockHttpWebResponse1.Setup(s => s.StatusCode).Returns(HttpStatusCode.OK);
            var mockHttpWebResponse2 = new Mock<HttpWebResponse>();
            mockHttpWebResponse2.Setup(s => s.StatusCode).Returns(HttpStatusCode.OK);

            mockWebRequest.SetupSequence(s => s.GetResponse()).Returns(mockHttpWebResponse1.Object).Returns(mockHttpWebResponse2.Object);
            mockWebRequest.Setup(s => s.GetRequestStream()).Returns(new MemoryStream());

            var json1 = "[{\"email\":\"a@a.com\"}]";
            var json2 = "{\"message\":\"success\"}";

            var stream1 = new MemoryStream(Encoding.UTF8.GetBytes(json1));
            var stream2 = new MemoryStream(Encoding.UTF8.GetBytes(json2));

            mockHttpWebResponse1.Setup(s => s.GetResponseStream()).Returns(stream1);
            mockHttpWebResponse2.Setup(s => s.GetResponseStream()).Returns(stream2);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse1.VerifyAll();
            mockHttpWebResponse2.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void BeforeSend_正常_送信先からUserIdが取れない場合()
        {
            // 違いはログにuserIdが出力されないだけであるがUTではログの確認をしないので、正常終了することのみ確認する

            var mockHttpWebResponse1 = new Mock<HttpWebResponse>();
            mockHttpWebResponse1.Setup(s => s.StatusCode).Returns(HttpStatusCode.OK);
            var mockHttpWebResponse2 = new Mock<HttpWebResponse>();
            mockHttpWebResponse2.Setup(s => s.StatusCode).Returns(HttpStatusCode.OK);

            mockWebRequest.SetupSequence(s => s.GetResponse()).Returns(mockHttpWebResponse1.Object).Returns(mockHttpWebResponse2.Object);
            mockWebRequest.Setup(s => s.GetRequestStream()).Returns(new MemoryStream());

            var json1 = "[{\"email\":\"a@a.com\"}]";
            var json2 = "{\"message\":\"success\"}";

            var stream1 = new MemoryStream(Encoding.UTF8.GetBytes(json1));
            var stream2 = new MemoryStream(Encoding.UTF8.GetBytes(json2));

            mockHttpWebResponse1.Setup(s => s.GetResponseStream()).Returns(stream1);
            mockHttpWebResponse2.Setup(s => s.GetResponseStream()).Returns(stream2);

            var result = target.BeforeSendEntrance(new IdentityMessage() { Destination = "a@a.com" });
            Assert.IsTrue(result);

            mockWebRequest.VerifyAll();
            mockHttpWebResponse1.VerifyAll();
            mockHttpWebResponse2.VerifyAll();
            mockFactory.VerifyAll();
        }

        [TestMethod]
        public void ServerCertificateValidationCallback()
        {
            // カバレッジを通すためだけのテストメソッド
            var methods = typeof(SendGridEmailService).GetRuntimeMethodsWithAnonymous();
            foreach (var method in methods)
            {
                ParameterInfo[] parameters = method.GetParameters();

                if (parameters.Count() == 4 &&
                    parameters[0].ParameterType == typeof(object) &&
                    parameters[1].ParameterType == typeof(X509Certificate) &&
                    parameters[2].ParameterType == typeof(X509Chain) &&
                    parameters[3].ParameterType == typeof(SslPolicyErrors))
                {
                    Assert.IsTrue((Boolean)method.InvokeWithAnonymous(target, new object[] { null, null, null, null }));
                }
            }
        }
    }
}
