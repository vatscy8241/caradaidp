﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.IdentityManagers;

namespace MTI.CaradaIdProvider.Web.Tests.EmailServices
{
    /// <summary>
    /// ApplicationEmailTokenProviderテストクラス。
    /// 継承元クラスのテストは行わないので、結果がtrueになるテストはない。
    /// </summary>
    [TestClass]
    public class ApplicationEmailTokenProviderTest
    {
        private ApplicationEmailTokenProvider<CaradaIdUser> target;
        private UserManager<CaradaIdUser, string> manager;
        private CaradaIdUser user;

        [TestInitialize]
        public void Initialize()
        {
            target = new ApplicationEmailTokenProvider<CaradaIdUser>();
            manager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object).Object;
            user = new CaradaIdUser();
        }

        [TestMethod]
        public void ValidateAsync_認証コードがnull()
        {
            var result = target.ValidateAsync("test", null, manager, user).Result;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateAsync_認証コードが5桁()
        {
            var result = target.ValidateAsync("test", "01234", manager, user).Result;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateAsync_認証コードが7桁()
        {
            var result = target.ValidateAsync("test", "0123456", manager, user).Result;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateAsync_認証コードが7桁_trimすると6桁()
        {
            var result = target.ValidateAsync("test", " 012345", manager, user).Result;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateAsync_認証コードが6桁_前後にスペースあり()
        {
            var result = target.ValidateAsync("test", " 01234", manager, user).Result;
            Assert.IsFalse(result);

            result = target.ValidateAsync("test", "　01234", manager, user).Result;
            Assert.IsFalse(result);

            result = target.ValidateAsync("test", "01234 ", manager, user).Result;
            Assert.IsFalse(result);

            result = target.ValidateAsync("test", "01234　", manager, user).Result;
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateAsync_認証コードが6桁()
        {
            var result = target.ValidateAsync("test", "01234a", manager, user).Result;
            Assert.IsFalse(result);
        }
    }
}