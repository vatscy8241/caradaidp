﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.Tasks;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;

namespace MTI.CaradaIdProvider.Web.Tests.EmailServices
{
    /// <summary>
    /// EmailServiceのテストクラス
    /// </summary>
    [TestClass]
    public class AsyncEmailServiceTest
    {
        private class TestEmailService : AsyncEmailService
        {
            public TestEmailService()
            {
                Client = new SmtpClient();
                Client.PickupDirectoryLocation = path;
                var f = ReflectionAccessor.GetField<AsyncEmailService>("processor");
                f.SetValue(this, new SyncBackGroundProcessor());
            }
        }

        private class TestNeverSendEmailService : TestEmailService
        {
            protected override bool BeforeSend(IdentityMessage message)
            {
                return false;
            }
        }

        private class TestAlwaysCancelEmailService : TestEmailService
        {
            public TestAlwaysCancelEmailService() : base()
            {
                var f = ReflectionAccessor.GetField<AsyncEmailService>("processor");
                f.SetValue(this, new SyncBackGroundProcessor() { CancellationToken = new CancellationToken(true) });
            }
        }

        private class TestAlwaysThrowEmailService : TestEmailService
        {
            protected override bool BeforeSend(IdentityMessage message)
            {
                throw new Exception("dummy");
            }
        }

        private class TestEmailService2 : AsyncEmailService
        {
            protected override bool BeforeSend(IdentityMessage message)
            {
                return false;
            }
        }

        private AsyncEmailService target;

        private static string path;

        private SmtpClient client;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            path = Path.GetFullPath(@"..\..\TestMails");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new TestEmailService();
            var p = ReflectionAccessor.GetProperty<SendGridEmailService>("Client");
            client = new SmtpClient();
            client.PickupDirectoryLocation = path;
            p.SetValue(target, client);
        }

        [TestCleanup]
        public void Cleanup()
        {
            var files = Directory.EnumerateFiles(path, "*.eml", SearchOption.AllDirectories);

            foreach (var currentFile in files)
            {
                File.Delete(currentFile);
            }
        }

        [TestMethod]
        public void SendAsync_送信OK()
        {
            var message = new IdentityMessage();
            message.Subject = "サブジェクト";
            message.Body = "本文";
            message.Destination = "to@localhost";
            target.SendAsync(message).Wait();

            var file = (from f in Directory.EnumerateFiles(path, "*.eml")
                        orderby File.GetLastWriteTime(f) descending
                        select f).First();

            // テスト用にインストールしたパッケージでemlファイルを読む
            var mailMessage = new MailBee.Mime.MailMessage();
            mailMessage.LoadMessage(file);

            Assert.AreEqual("サブジェクト", mailMessage.Subject);
            Assert.AreEqual("本文", mailMessage.BodyPlainText);
            Assert.AreEqual("", mailMessage.BodyHtmlText);
            Assert.AreEqual("to@localhost", mailMessage.To.ToString());
            Assert.AreEqual("from@localhost", mailMessage.From.ToString());
        }

        [TestMethod]
        public void SendAsync_送信NG()
        {
            // 必ずエラーになる
            client.PickupDirectoryLocation = @".\";

            var message = new IdentityMessage();
            message.Subject = "サブジェクト";
            message.Body = "本文";
            message.Destination = "to@localhost";

            new TestAlwaysThrowEmailService().SendAsync(message).Wait();

            Assert.AreEqual(0, Directory.EnumerateFiles(path, "*.eml").Count());
        }

        [TestMethod]
        public void SendAsync_IsCancellationがtrueのときは送信しないこと()
        {
            var message = new IdentityMessage();
            message.Subject = "サブジェクト";
            message.Body = "本文";
            message.Destination = "to@localhost";

            new TestAlwaysCancelEmailService().SendAsync(message).Wait();

            Assert.AreEqual(0, Directory.EnumerateFiles(path, "*.eml").Count());
        }

        [TestMethod]
        public void get_Client_新しいSmtpClientを生成すること()
        {
            // カバレッジ用のテストメソッド
            var p = ReflectionAccessor.GetProperty<SendGridEmailService>("Client");
            p.SetMethod.Invoke(target, new Object[] { null });
            var c = p.GetMethod.Invoke(target, null) as SmtpClient;
            Assert.IsNotNull(c);
        }

        [TestMethod]
        public void SendAsync_BeforeSendでfalseの場合は送信を行わないこと()
        {
            var message = new IdentityMessage();
            message.Subject = "サブジェクト";
            message.Body = "本文";
            message.Destination = "to@localhost";

            new TestNeverSendEmailService().SendAsync(message).Wait();

            Assert.AreEqual(0, Directory.EnumerateFiles(path, "*.eml").Count());
        }
    }
}
