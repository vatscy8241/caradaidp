﻿using Microsoft.Owin;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;

namespace MTI.CaradaIdProvider.Web.Tests
{
    public class MockHttpSession : HttpSessionStateBase
    {
        Dictionary<string, object> m_SessionStorage = new Dictionary<string, object>();

        public override object this[string name]
        {
            get { return m_SessionStorage.ContainsKey(name) ? m_SessionStorage[name] : null; }
            set { m_SessionStorage[name] = value; }
        }

        public override int Count
        {
            get { return m_SessionStorage.Count; }
        }

        public override void Remove(string name)
        {
            m_SessionStorage.Remove(name);
        }

        public override void RemoveAll()
        {
            m_SessionStorage.Clear();
        }

        public override void Add(string name, object value)
        {
            m_SessionStorage[name] = value;
        }
    }

    public static class MvcMockHelpers
    {
        public static Mock<HttpContextBase> MockContext;
        private static Dictionary<string, object> HttpContextBaseItems = new Dictionary<string, object>();
        public static Mock<HttpRequestBase> MockRequest;
        public static Mock<HttpResponseBase> MockResponse;
        public static Mock<HttpServerUtilityBase> MockServer;
        public static Mock<IPrincipal> MockUser;
        public static Mock<IIdentity> MockIdentity;
        public static OwinContext Owin;

        public static HttpContextBase FakeHttpContext()
        {
            HttpContextBaseItems = new Dictionary<string, object>();
            MockContext = new Mock<HttpContextBase>() { CallBase = true };
            MockRequest = new Mock<HttpRequestBase>();
            MockResponse = new Mock<HttpResponseBase>();
            var session = new MockHttpSession();
            MockServer = new Mock<HttpServerUtilityBase>();
            MockUser = new Mock<IPrincipal>();
            MockIdentity = new Mock<IIdentity>();
            Owin = new OwinContext();

            MockContext.Setup(ctx => ctx.Request).Returns(MockRequest.Object);
            MockContext.Setup(ctx => ctx.Response).Returns(MockResponse.Object);
            MockContext.Setup(ctx => ctx.Session).Returns(session);
            MockContext.Setup(ctx => ctx.Server).Returns(MockServer.Object);
            MockContext.Setup(ctx => ctx.User).Returns(MockUser.Object);
            MockUser.Setup(user => user.Identity).Returns(MockIdentity.Object);

            MockContext.SetupGet(ctx => ctx.Items).Returns(HttpContextBaseItems);
            MockContext.Object.Items["owin.Environment"] = Owin.Environment;
            return MockContext.Object;
        }

        public static HttpContextBase FakeHttpContext(string url)
        {
            var context = FakeHttpContext();
            context.Request.SetupRequestUrl(url);
            return context;
        }

        public static void SetFakeControllerContext(this Controller controller)
        {
            var httpContext = FakeHttpContext();
            var context = new ControllerContext(new RequestContext(httpContext, new RouteData()), controller);
            controller.ControllerContext = context;
        }

        static string GetUrlFileName(string url)
        {
            if (url.Contains("?"))
                return url.Substring(0, url.IndexOf("?"));
            else
                return url;
        }

        static NameValueCollection GetQueryStringParameters(string url)
        {
            if (url.Contains("?"))
            {
                var parameters = new NameValueCollection();

                string[] parts = url.Split("?".ToCharArray());
                string[] keys = parts[1].Split("&".ToCharArray());

                foreach (string key in keys)
                {
                    string[] part = key.Split("=".ToCharArray());
                    parameters.Add(part[0], part[1]);
                }

                return parameters;
            }
            else
            {
                return null;
            }
        }

        public static void SetHttpMethodResult(this HttpRequestBase request, string httpMethod)
        {
            Mock.Get(request)
                .Setup(req => req.HttpMethod)
                .Returns(httpMethod);
        }

        public static void SetupRequestUrl(this HttpRequestBase request, string url)
        {
            if (url == null)
                throw new ArgumentNullException("url");

            if (!url.StartsWith("~/"))
                throw new ArgumentException("Sorry, we expect a virtual url starting with \"~/\".");

            var mock = Mock.Get(request);

            mock.Setup(req => req.QueryString)
                .Returns(GetQueryStringParameters(url));
            mock.Setup(req => req.AppRelativeCurrentExecutionFilePath)
                .Returns(GetUrlFileName(url));
            mock.Setup(req => req.PathInfo)
                .Returns(string.Empty);
        }
    }

    public static class MockHttpContextHelper
    {
        public static void SetCurrentOfFakeContext()
        {
            var httpRequest = new HttpRequest("", "http://mti.co.jp/", "");
            using (var stringWriter = new StringWriter())
            {
                var httpResponce = new HttpResponse(stringWriter);
                var httpContext = new HttpContext(httpRequest, httpResponce);

                var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                                                        new HttpStaticObjectsCollection(), 10, true,
                                                        HttpCookieMode.AutoDetect,
                                                        SessionStateMode.InProc, false);

                httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                                            BindingFlags.NonPublic | BindingFlags.Instance,
                                            null, CallingConventions.Standard,
                                            new[] { typeof(HttpSessionStateContainer) },
                                            null)
                                    .Invoke(new object[] { sessionContainer });

                HttpContext.Current = httpContext;
            }
        }

        public static void SetCurrentOfFakeContextWithoutSession()
        {
            var httpRequest = new HttpRequest("", "http://mti.co.jp/", "");
            using (var stringWriter = new StringWriter())
            {
                var httpResponce = new HttpResponse(stringWriter);
                var httpContext = new HttpContext(httpRequest, httpResponce);

                HttpContext.Current = httpContext;
            }
        }
    }
}
