﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Utilities;

namespace MTI.CaradaIdProvider.Web.Tests.Utilities
{
    /// <summary>
    /// DeviceTypeUtilityのテストクラス
    /// </summary>
    [TestClass]
    public class DeviceTypeUtilityTest
    {
        [TestMethod]
        public void GetDisplayModeByUserAgent_正常系_fp()
        {
            {
                var ua = "DoCoMo/2.0 P900i(c100:TB) d2cbot (+http://listing-ads.d2c.ne.jp/crawler)";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("fp", result);
            }
            {
                var ua = "SoftBank/1.0/930SH/SHJ001[/Serial] Browser/NetFront/3.4 Profile/MIDP-2.0 Configuration/CLDC-1.1";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("fp", result);
            }
            {
                var ua = "Vodafone/1.0/V705SH/SHJ001[/Serial] Browser/VF-NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("fp", result);
            }
            {
                var ua = "J-PHONE/3.0/J-SH10";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("fp", result);
            }
            {
                var ua = "KDDI-TS21 UP.Browser/6.0.2.273 (GUI) MMP/1.1";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("fp", result);
            }
            {
                var ua = "DoCoMoSoftBankVodafoneUP.Browser";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("fp", result);
            }
        }

        [TestMethod]
        public void GetDisplayModeByUserAgent_正常系_pc()
        {
            {
                // UserAgent NULL
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(null);
                Assert.AreEqual("pc", result);
            }
            {
                // UserAgent 空文字列
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent("");
                Assert.AreEqual("pc", result);
            }
            /****************************************
             * Smart Phone
             ****************************************/
            {
                // SoftBank 系列だが非対応
                var ua = "SMOT-V980/80.2F.2E. MIB/2.2.1 Profile/MIDP-2.0 Configuration/CLDC-1.1";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Willcom
                var ua = "Mozilla/3.0(WILLCOM;KYOCERA/WX340K/2;3.0.3.11.000000/1/C256) NetFront/3.4";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // emobile
                var ua = "emobile/1.0.0 (H11T; like Gecko; Wireless) NetFront/3.4";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // BlackBerry
                var ua = "Mozilla/5.0 (BlackBerry; U; BlackBerry 9780; ja) AppleWebKit/534.8+ (KHTML, like Gecko) Version/6.0.0.587 Mobile Safari/534.8+";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Nexus
                var ua = "Mozilla/5.0 (Linux; Android 5.1; Nexus 5 Build/LMY47I) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.111 Mobile Safari/537.36";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Android
                var ua = "Mozilla/5.0 (Linux; U; Android 2.3.3; ja-jp; INFOBAR A01 Build/S7142) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // iPhone
                var ua = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML,like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // iPod
                var ua = "Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_1 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Windows Phone
                var ua = "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0; FujitsuToshibaMobileCommun; IS12T; KDDI)";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Firefox OS
                var ua = "Mozilla/5.0 (Mobile; LGL25; rv:32.0) Gecko/32.0 Firefox/32.0";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            /****************************************
             *Tablet
             ****************************************/
            {
                // iPad
                {
                    var ua = "Mozilla/5.0 (iPad; CPU OS 8_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12F69 Safari/600.1.4";
                    var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                    Assert.AreEqual("pc", result);
                }
                {
                    var ua = "Mozilla/5.0 (iPad; U; CPU iPhone OS 4_3_3 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Mobile/8J2";
                    var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                    Assert.AreEqual("pc", result);
                }
            }
            {
                // Android
                var ua = "Mozilla/5.0 (Android; Tablet; rv:36.0) Gecko/36.0 Firefox/36.0";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Windows
                var ua = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Firefox OS
                var ua = "Mozilla/5.0 (Tablet; rv:26.0) Gecko/26.0 Firefox/26.0";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Nexus
                // Nexusはモデルでスマートフォンとタブレットが切り分けできる
                //「Nexus 7」「Nexus 9」「Nexus 10」がタブレット
                var ua = "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 7 Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.111 Safari/537.36";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
            {
                // Kindle
                var ua = "Mozilla/5.0 (X11; U; Linux armv7l like Android; en-us) AppleWebKit/531.2+ (KHTML, like Gecko) Version/5.0 Safari/533.2+ Kindle/3.0+";
                var result = DeviceTypeUtility.GetDisplayModeByUserAgent(ua);
                Assert.AreEqual("pc", result);
            }
        }
    }
}
