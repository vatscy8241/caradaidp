﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Utilities;

namespace MTI.CaradaIdProvider.Web.Tests.Utilities
{
    [TestClass]
    public class DateTimeExtensionsTest
    {
        [TestMethod]
        public void ToUnixTime_1970年1月1日0時0分0秒が0になること()
        {
            Assert.AreEqual((long)0, new DateTime(1970, 1, 1).ToUnixTime());
        }
    }
}
