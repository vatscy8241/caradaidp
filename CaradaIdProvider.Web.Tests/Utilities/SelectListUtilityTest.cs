﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Utilities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Utilities
{
    /// <summary>
    /// SelectListUtilityのテストクラス
    /// </summary>
    [TestClass]
    public class SelectListUtilityTest
    {
        [TestMethod]
        public void ConvertDicToSelectList_正常系_引数のdicあり()
        {
            var dic = new Dictionary<int, string>();
            dic.Add(1, "val1");
            dic.Add(2, "val2");
            dic.Add(3, "val3");
            var list = SelectListUtility.ConvertDicToSelectList(dic);
            var i = 1;
            Assert.AreEqual(dic.Count, list.Count);
            foreach (var item in list)
            {
                Assert.AreEqual(i.ToString(), item.Value);
                Assert.AreEqual("val" + i, item.Text);
                i++;
            }
        }

        [TestMethod]
        public void ConvertDicToSelectList_正常系_引数のdicがnull()
        {
            Assert.AreEqual(0, SelectListUtility.ConvertDicToSelectList(null).Count);
        }

        [TestMethod]
        public void ConvertDicToSelectList_正常系_引数のdicが0()
        {
            Assert.AreEqual(0, SelectListUtility.ConvertDicToSelectList(new Dictionary<int, string>()).Count);
        }

        [TestMethod]
        public void ConvertSelectListToDic_正常系_引数のlistあり()
        {
            var list = new List<SelectListItem>();
            var i = 1;
            var pre = "val";
            var item = new SelectListItem();
            item.Value = i.ToString();
            item.Text = pre + i;
            list.Add(item);
            i++;
            item = new SelectListItem();
            item.Value = i.ToString();
            item.Text = pre + i;
            list.Add(item);
            i++;
            item = new SelectListItem();
            item.Value = i.ToString();
            item.Text = pre + i;
            list.Add(item);
            var dic = SelectListUtility.ConvertSelectListToDic(list);

            var j = 1;
            Assert.AreEqual(dic.Count, list.Count);
            foreach (var pair in dic)
            {
                Assert.AreEqual(j, pair.Key);
                Assert.AreEqual(pre + j, pair.Value);
                j++;
            }
        }

        [TestMethod]
        public void ConvertSelectListToDic_正常系_引数のlistがnull()
        {
            Assert.AreEqual(0, SelectListUtility.ConvertSelectListToDic(null).Count);
        }

        [TestMethod]
        public void ConvertSelectListToDic_正常系_引数のlistが0()
        {
            Assert.AreEqual(0, SelectListUtility.ConvertSelectListToDic(new List<SelectListItem>()).Count);
        }
    }
}
