﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Utilities;
using NLog;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Utilities
{
    [TestClass]
    public class JwtIssuerLayoutRendererTest
    {
        private JwtIssuerLayoutRenderer target;

        [TestMethod]
        public void Append_Issuer設定時_成功()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://example.com", ""), new HttpResponse(new StringWriter()));
            HttpContext.Current.Items["Issuer"] = "TestIssuer";

            target = new JwtIssuerLayoutRenderer();
            var builder = new StringBuilder();
            var method = target.GetType().GetMethod("Append", BindingFlags.NonPublic | BindingFlags.Instance, Type.DefaultBinder, new Type[] { typeof(StringBuilder), typeof(LogEventInfo) }, null);
            method.Invoke(target, new object[] { builder, null });
            Assert.AreEqual<string>("TestIssuer", builder.ToString());
        }

        [TestMethod]
        public void Append_Issuer未設定時_通常()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://example.com", ""), new HttpResponse(new StringWriter()));

            target = new JwtIssuerLayoutRenderer();
            var builder = new StringBuilder();
            var method = target.GetType().GetMethod("Append", BindingFlags.NonPublic | BindingFlags.Instance, Type.DefaultBinder, new Type[] { typeof(StringBuilder), typeof(LogEventInfo) }, null);
            method.Invoke(target, new object[] { builder, null });
            Assert.AreEqual<string>("[IssUnknown]", builder.ToString());
        }

        [TestMethod]
        public void Append_Issuerが空で設定時_Userなし()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://example.com", ""), new HttpResponse(new StringWriter()));
            HttpContext.Current.Items["Issuer"] = "";

            target = new JwtIssuerLayoutRenderer();
            var builder = new StringBuilder();
            var method = target.GetType().GetMethod("Append", BindingFlags.NonPublic | BindingFlags.Instance, Type.DefaultBinder, new Type[] { typeof(StringBuilder), typeof(LogEventInfo) }, null);
            method.Invoke(target, new object[] { builder, null });
            Assert.AreEqual<string>("[IssUnknown]", builder.ToString());
        }
    }
}
