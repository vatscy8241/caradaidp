﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Utilities;
using NLog;
using System;
using System.IO;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Utilities
{
    [TestClass]
    public class UserLayoutRendererTest
    {
        private UserLayoutRenderer target;
        private Mock<IIdentity> mockIdentity;

        [TestMethod]
        public void Append_ログイン中_成功()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://test.com", ""), new HttpResponse(new StringWriter()));

            mockIdentity = new Mock<IIdentity>();
            mockIdentity.Setup(m => m.Name).Returns("TestTaro");
            var claimsIdentity = new ClaimsIdentity(mockIdentity.Object);
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, "TestUserId"));
            var principal = new GenericPrincipal(claimsIdentity, null);
            HttpContext.Current.User = principal;

            target = new UserLayoutRenderer();
            var builder = new StringBuilder();
            var method = target.GetType().GetMethod("Append", BindingFlags.NonPublic | BindingFlags.Instance, Type.DefaultBinder, new Type[] { typeof(StringBuilder), typeof(LogEventInfo) }, null);
            method.Invoke(target, new object[] { builder, null });
            Assert.AreEqual<string>("TestUserId TestTaro", builder.ToString());
            mockIdentity.VerifyAll();
        }

        [TestMethod]
        public void Append_未ログイン_通常()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://test.com", ""), new HttpResponse(new StringWriter()));
            var claimsIdentity = new ClaimsIdentity();
            var principal = new GenericPrincipal(claimsIdentity, null);
            HttpContext.Current.User = principal;

            target = new UserLayoutRenderer();
            var builder = new StringBuilder();
            var method = target.GetType().GetMethod("Append", BindingFlags.NonPublic | BindingFlags.Instance, Type.DefaultBinder, new Type[] { typeof(StringBuilder), typeof(LogEventInfo) }, null);
            method.Invoke(target, new object[] { builder, null });
            Assert.AreEqual<string>("[Not Logged]", builder.ToString());
        }

        [TestMethod]
        public void Append_未ログイン_Userなし()
        {
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://test.com", ""), new HttpResponse(new StringWriter()));

            target = new UserLayoutRenderer();
            var builder = new StringBuilder();
            var method = target.GetType().GetMethod("Append", BindingFlags.NonPublic | BindingFlags.Instance, Type.DefaultBinder, new Type[] { typeof(StringBuilder), typeof(LogEventInfo) }, null);
            method.Invoke(target, new object[] { builder, null });
            Assert.AreEqual<string>("[Not Logged]", builder.ToString());
        }

        [TestMethod]
        public void Append_未ログイン_Currentなし()
        {
            target = new UserLayoutRenderer();
            var builder = new StringBuilder();
            var method = target.GetType().GetMethod("Append", BindingFlags.NonPublic | BindingFlags.Instance, Type.DefaultBinder, new Type[] { typeof(StringBuilder), typeof(LogEventInfo) }, null);
            method.Invoke(target, new object[] { builder, null });
            Assert.AreEqual<string>("[Not Logged]", builder.ToString());
        }
    }
}
