﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Utilities;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace MTI.CaradaIdProvider.Web.Tests.Utilities
{
    [TestClass]
    public class LogUtilitiesTest
    {
        [TestMethod]
        public void GetTokenRequestParametersString_KeyValuePairs_正常系()
        {
            var data = new List<KeyValuePair<string, string[]>>() {
                new KeyValuePair<string, string[]>("aaa", new []{"test"}),
                new KeyValuePair<string, string[]>("bbb", new []{"test1","test2"}),
                new KeyValuePair<string, string[]>("Email", new []{"test@test.com"})
            };

            Assert.AreEqual("aaa=\"test\", bbb=[\"test1\", \"test2\"], Email=\"*****\"", LogUtilities.GetParameterString(data));
        }

        [TestMethod]
        public void GetTokenRequestParametersString_KeyValuePairs_パラメータなし()
        {
            Assert.AreEqual("", LogUtilities.GetParameterString(new List<KeyValuePair<string, string[]>>()));
        }

        [TestMethod]
        public void GetTokenRequestParametersString_NameValueCollection_正常系()
        {
            var data = new NameValueCollection();
            data["aaa"] = "test";
            data["bbb"] = "test1";
            data["Password"] = "pass";

            Assert.AreEqual("aaa=\"test\", bbb=\"test1\", Password=\"*****\"", LogUtilities.GetParameterString(data));
        }

        [TestMethod]
        public void GetTokenRequestParametersString_NameValueCollection_パラメータなし()
        {
            Assert.AreEqual("", LogUtilities.GetParameterString(new NameValueCollection()));
        }

        [TestMethod]
        public void MaskProcessing_マスク対象()
        {
            // マスク対象正規表現「.*Password.*,.*Email,.*Answer」caseを区別しない マスク文字列「*****」で設定されている場合

            var key = "Password";
            var value = "aaa";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "password";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "ConfirmPassword";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "NewPasswordConfirm";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "ResetPassword";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "ConfirmResetPassword";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "Answer";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "answer";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "Email";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "email";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "SubEmail";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));

            key = "subemail";
            Assert.AreEqual("*****", LogUtilities.MaskProcessing(key, value));
        }

        [TestMethod]
        public void MaskProcessing_マスク対象外()
        {
            // マスク対象正規表現「.*Password.*,CaradaId,.*Email」 マスク文字列「*****」で設定されている場合

            var key = "Id";
            var value = "aaa";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            key = "IsKeepLogin";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            key = "VerifyCode";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            key = "CaradaId";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            key = "NewCaradaId";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            key = "NewPwdConfirm";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));
        }

        [TestMethod]
        public void MaskProcessing_nullか空文字の場合()
        {
            // マスク対象正規表現「.*Password,CaradaId,.*Email」 マスク文字列「*****」で設定されている場合

            // キーがない
            string key = null;
            var value = "aaa";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            key = "";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            // キーがマスク対象だが、値がない
            key = "Password";
            value = null;
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

            value = "";
            Assert.AreEqual(value, LogUtilities.MaskProcessing(key, value));

        }

        [TestMethod]
        public void GetParameterString_Json文字列の場合()
        {
            var serializer = new JavaScriptSerializer();
            var obj = new { aaa = "test", bbb = "test1", Password = "pass" };
            var data = serializer.Serialize(obj);

            Assert.AreEqual("aaa=\"test\", bbb=\"test1\", Password=\"*****\"", LogUtilities.GetParameterString(data));
        }

        [TestMethod]
        public void GetParameterString_ParameterObjectの場合()
        {
            var obj = new { aaa = "test", bbb = "test1", Password = "pass" };

            Assert.AreEqual("aaa=\"test\", bbb=\"test1\", Password=\"*****\"", LogUtilities.GetParameterString(obj));
        }
    }
}
