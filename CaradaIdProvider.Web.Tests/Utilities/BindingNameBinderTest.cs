﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Utilities;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Utilities
{
    /// <summary>
    /// BingingNameBinderのテストクラス
    /// オーバーライドしたprotectedメソッドは直接呼び出さない。publicメソッド経由でテストする。
    /// </summary>
    [TestClass]
    public class BindingNameBinderTest
    {
        /// <summary>
        /// テスト用モデル
        /// </summary>
        private class DummyModel
        {
            public string StrA { get; set; }

            [BindingName(Name = "StringB")]
            public string StrB { get; set; }

            [BindingName(Name = "StringC")]
            public string StrC { get; set; }

            public string StrD { get; set; }

            [BindingName(Name = "StringE")]
            [BindingName(Name = "StrE")]
            public string StrE { get; set; }
        }

        /// <summary>
        /// テスト用コントローラ
        /// </summary>
        private class DummyController : Controller
        {
        }

        private BindingNameBinder target;

        private DummyController controller;

        private DummyModel model;

        private ModelBindingContext modelBindingContext;

        [TestInitialize]
        public void Initialize()
        {
            target = new BindingNameBinder();
            controller = new DummyController();
            controller.SetFakeControllerContext();
            model = new DummyModel();
            modelBindingContext = new ModelBindingContext()
            {
                ModelName = "",
                ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(null, typeof(DummyModel))
            };

        }

        [TestMethod]
        public void BindProperty_HTTPGETメソッドの場合()
        {
            MvcMockHelpers.MockRequest.Setup(s => s.HttpMethod).Returns("GET");

            var requestQuery = new NameValueCollection {
                {"StrA", "a"},
                {"StringB", "b"},
                {"StrC", "c"},
                {"StrD", "0"},
                {"StrE", "1"}
            };

            var form = new NameValueCollection {
                {"StrA", "aa"},
                {"StringB", "bb"},
                {"StrC", "cc"},
                {"StrD", "10"},
                {"StrE", "11"}
            };

            modelBindingContext.ValueProvider = new NameValueCollectionValueProvider(requestQuery, null);

            MvcMockHelpers.MockRequest.Setup(s => s.QueryString).Returns(requestQuery);
            MvcMockHelpers.MockRequest.Setup(s => s.Form).Returns(form);

            // RequestQueryからバインドされること
            model = (DummyModel)target.BindModel(controller.ControllerContext, modelBindingContext);

            // BindingNameが付いていない場合はプロパティ名でバインドされること
            Assert.AreEqual("a", model.StrA);
            // BindingName.Nameのパラメータがバインドされること
            Assert.AreEqual("b", model.StrB);
            // BindingNameが付いている場合はプロパティ名ではバインドされないこと
            Assert.IsNull(model.StrC);
            Assert.AreEqual("0", model.StrD);
            // プロパティ名自体を指定がされている場合はプロパティ名でバインドされること
            Assert.AreEqual("1", model.StrE);

            MvcMockHelpers.MockRequest.Verify(v => v.HttpMethod);
            MvcMockHelpers.MockRequest.Verify(v => v.QueryString);
            MvcMockHelpers.MockRequest.Verify(v => v.Form, Moq.Times.Never);
        }

        [TestMethod]
        public void BindProperty_HTTPPOSTメソッドの場合()
        {
            MvcMockHelpers.MockRequest.Setup(s => s.HttpMethod).Returns("POST");

            var requestQuery = new NameValueCollection {
                {"StrA", "a"},
                {"StringB", "b"},
                {"StrC", "c"},
                {"StrD", "0"},
                {"StringE", "1"}
            };

            var form = new NameValueCollection {
                {"StrA", "aa"},
                {"StringB", "bb"},
                {"StrC", "cc"},
                {"StrD", "10"},
                {"StringE", "11"}
            };

            modelBindingContext.ValueProvider = new NameValueCollectionValueProvider(form, null);

            MvcMockHelpers.MockRequest.Setup(s => s.QueryString).Returns(requestQuery);
            MvcMockHelpers.MockRequest.Setup(s => s.Form).Returns(form);

            // formからバインドされること
            model = (DummyModel)target.BindModel(controller.ControllerContext, modelBindingContext);

            // BindingNameが付いていない場合はプロパティ名でバインドされること
            Assert.AreEqual("aa", model.StrA);
            // BindingName.Nameのパラメータがバインドされること
            Assert.AreEqual("bb", model.StrB);
            // BindingNameが付いている場合はプロパティ名ではバインドされないこと
            Assert.IsNull(model.StrC);
            Assert.AreEqual("10", model.StrD);
            // 複数指定されている場合はどちらでもバインドされること
            Assert.AreEqual("11", model.StrE);

            MvcMockHelpers.MockRequest.Verify(v => v.HttpMethod);
            MvcMockHelpers.MockRequest.Verify(v => v.QueryString, Moq.Times.Never);
            MvcMockHelpers.MockRequest.Verify(v => v.Form);
        }

        [TestMethod]
        public void BindProperty_HTTPHEADメソッドの場合()
        {
            MvcMockHelpers.MockRequest.Setup(s => s.HttpMethod).Returns("HEAD");

            var requestQuery = new NameValueCollection {
                {"StrA", "a"},
                {"StringB", "b"},
                {"StrC", "c"},
                {"StrD", "0"},
                {"StrE", "1"}
            };

            var form = new NameValueCollection {
                {"StrA", "aa"},
                {"StringB", "bb"},
                {"StrC", "cc"},
                {"StrD", "10"},
                {"StrE", "11"}
            };

            modelBindingContext.ValueProvider = new NameValueCollectionValueProvider(requestQuery, null);

            MvcMockHelpers.MockRequest.Setup(s => s.QueryString).Returns(requestQuery);
            MvcMockHelpers.MockRequest.Setup(s => s.Form).Returns(form);

            model = (DummyModel)target.BindModel(controller.ControllerContext, modelBindingContext);

            // Baseクラスどおりの動作となること
            Assert.AreEqual("a", model.StrA);
            Assert.IsNull(model.StrB);
            Assert.AreEqual("c", model.StrC);
            Assert.AreEqual("0", model.StrD);
            Assert.AreEqual("1", model.StrE);

            MvcMockHelpers.MockRequest.Verify(v => v.HttpMethod);
            MvcMockHelpers.MockRequest.Verify(v => v.QueryString, Moq.Times.Never);
            MvcMockHelpers.MockRequest.Verify(v => v.Form, Moq.Times.Never);
        }
    }
}
