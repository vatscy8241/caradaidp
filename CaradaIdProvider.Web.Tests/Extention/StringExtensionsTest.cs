﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Extention;
using System;
using System.Text;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers
{
    /// <summary>
    /// StringExtensionのテストクラス。
    /// </summary>
    [TestClass]
    public class StringExtensionsTest
    {
        [TestMethod]
        public void Base64UrlDecode_正常系_Base64Urlエンコード文字列がデコードされる()
        {
            var origin = "あいうえお安以宇衣於aiueo１２３４５一二三四五1234567890";
            var encoded = Encode(origin);

            var result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);

            origin = "あいうえお安以宇衣於aiueo１２３４５一二三四五12345";
            encoded = Encode(origin);

            result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);

            origin = "aiueoあいうえお安以宇衣於12345１２３４５一二三四五";
            encoded = Encode(origin);

            result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);

            origin = "aiue";
            encoded = Encode(origin);

            result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);

            origin = "aiueo";
            encoded = Encode(origin);

            result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);

            origin = "aiueok";
            encoded = Encode(origin);

            result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);

            origin = "aiueoka";
            encoded = Encode(origin);

            result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);

            origin = "aiueokak";
            encoded = Encode(origin);

            result = encoded.Base64UrlDecode();
            Assert.AreEqual(origin, result);
        }

        private string Encode(string plainText)
        {
            var bytes = Encoding.UTF8.GetBytes(plainText);
            var base64 = Convert.ToBase64String(bytes);
            return base64.TrimEnd('=').Replace('+', '-').Replace('/', '_');
        }
    }
}
