﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Net.Http;
using System.ServiceModel.Security.Tokens;
using System.Text;

namespace MTI.CaradaIdProvider.Web.Tests.Logics
{
    /// <summary>
    /// JwtApiRequestのテストクラス
    /// </summary>
    [TestClass]
    public class JwtApiRequestTest
    {
        private JwtApiRequest target;

        private Mock<IWebApiInvoker> invokerMock;

        private byte[] keyBytes;

        [TestInitialize]
        public void TestInitialize()
        {
            target = new JwtApiRequest("AuthProvider", "localhost/hoge");
            invokerMock = new Mock<IWebApiInvoker>();
            var fieldInfo = ReflectionAccessor.GetField<JwtApiRequest>("webApiInvoker");
            fieldInfo.SetValue(target, invokerMock.Object);

            keyBytes = Encoding.UTF8.GetBytes("hogehoge");
            Array.Resize(ref keyBytes, 64);
        }

        [TestMethod]
        public void コンストラクタ_正常系_string_string_IWebApiInvoker()
        {
            IWebApiInvoker invoker = new WebApiInvoker();

            target = new JwtApiRequest("target2", "localhost/hoge2", invoker);

            Assert.IsNotNull(target);
            Assert.AreEqual(invoker, ReflectionAccessor.GetField<JwtApiRequest>("webApiInvoker").GetValue(target));
            Assert.AreEqual("target2", ReflectionAccessor.GetField<JwtApiRequest>("targetName").GetValue(target));
            Assert.AreEqual("localhost/hoge2", ReflectionAccessor.GetField<JwtApiRequest>("url").GetValue(target));
            var apitargetInfo = ((ApiTargetInfo)ReflectionAccessor.GetField<JwtApiRequest>("targetInfo").GetValue(target));
            Assert.AreEqual(4, ((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).Count);
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("AUTHPROVIDER"));
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("TARGET2"));
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("TARGET3"));
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("TARGET4"));
        }

        [TestMethod]
        public void コンストラクタ_正常系_string_string()
        {
            target = new JwtApiRequest("target3", "localhost/hoge3");

            Assert.IsNotNull(target);
            Assert.AreEqual("target3", ReflectionAccessor.GetField<JwtApiRequest>("targetName").GetValue(target));
            Assert.AreEqual("localhost/hoge3", ReflectionAccessor.GetField<JwtApiRequest>("url").GetValue(target));
            var apitargetInfo = ((ApiTargetInfo)ReflectionAccessor.GetField<JwtApiRequest>("targetInfo").GetValue(target));
            Assert.AreEqual(4, ((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).Count);
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("AUTHPROVIDER"));
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("TARGET2"));
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("TARGET3"));
            Assert.IsTrue(((Dictionary<string, ApiTarget>)ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(apitargetInfo)).ContainsKey("TARGET4"));
        }

        [TestMethod]
        public void PostJson_正常系_https_パラメータあり()
        {
            invokerMock.Setup(s => s.PostJson("localhost/hoge", It.IsAny<Dictionary<string, List<string>>>(), It.IsAny<JObject>())).Returns(new HttpResponseMessage())
                .Callback((string s, Dictionary<string, List<string>> h, JObject j) =>
                {
                    // JObjectの検証
                    var idToken = (string)j["token"];
                    Assert.IsNotNull(idToken);
                    Assert.AreEqual(idToken.Split('.').Length, 3);

                    // 署名の検証
                    var validateParameters = new TokenValidationParameters()
                    {
                        IssuerSigningToken = new BinarySecretSecurityToken(keyBytes),
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateActor = false,
                        ValidateLifetime = true,
                        // issuerが正しいこと
                        ValidIssuers = new List<string> { "isshoge1" },
                        LifetimeValidator = (DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters) =>
                        {
                            // 約2分以内であること すくなくともテスト対象で生成した日時よりも進んでいるはずなので<=で検証する
                            Assert.IsTrue(DateTime.UtcNow.AddMinutes(-3) <= notBefore && notBefore <= DateTime.UtcNow.AddMinutes(-2));
                            Assert.IsTrue(DateTime.UtcNow.AddMinutes(3) >= expires && expires <= DateTime.UtcNow.AddMinutes(2));
                            return true;
                        }
                    };

                    SecurityToken outToken;
                    new JwtSecurityTokenHandler().ValidateToken(idToken, validateParameters, out outToken);

                    JwtSecurityToken jwtOutToken = (JwtSecurityToken)outToken;

                    Assert.IsNotNull(jwtOutToken);
                    var parameters = jwtOutToken.Payload["params"] as Dictionary<string, object>;
                    Assert.AreEqual(1, parameters["numparam"]);
                    Assert.AreEqual("a", parameters["strparam"]);
                });


            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter["numparam"] = 1;
            parameter["strparam"] = "a";

            var message = target.PostJson(parameter);

            invokerMock.VerifyAll();
        }

        [TestMethod]
        public void PostJson_正常系_https_パラメータなし()
        {
            invokerMock.Setup(s => s.PostJson("localhost/hoge", It.IsAny<Dictionary<string, List<string>>>(), It.IsAny<JObject>())).Returns(new HttpResponseMessage())
                .Callback((string s, Dictionary<string, List<string>> h, JObject j) =>
                {
                    var idToken = (string)j["token"];

                    Assert.IsNotNull(idToken);
                    Assert.AreEqual(idToken.Split('.').Length, 3);

                    var validateParameters = new TokenValidationParameters()
                    {
                        IssuerSigningToken = new BinarySecretSecurityToken(keyBytes),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateActor = false,
                        ValidateLifetime = false
                    };

                    SecurityToken outToken;
                    new JwtSecurityTokenHandler().ValidateToken(idToken, validateParameters, out outToken);

                    JwtSecurityToken jwtOutToken = (JwtSecurityToken)outToken;

                    Assert.IsNotNull(jwtOutToken);
                    Assert.IsFalse(jwtOutToken.Payload.ContainsKey("params"));

                });

            var message = target.PostJson(null);
            invokerMock.VerifyAll();
        }

        [TestMethod]
        public void GetHmacSha256SigningKey_正常系_キーが63文字の場合()
        {
            var method = ReflectionAccessor.GetMethod<JwtApiRequest>("GetHmacSha256SigningKey");
            target = new JwtApiRequest("target2", "localhost/hoge2");

            SymmetricSecurityKey key = (SymmetricSecurityKey)method.Invoke(target, new object[] { });
            Assert.IsNotNull(key);
            Assert.AreEqual(key.GetSymmetricKey().Length, 64);
        }

        [TestMethod]
        public void GetHmacSha256SigningKey_正常系_キーが64文字の場合()
        {
            var method = ReflectionAccessor.GetMethod<JwtApiRequest>("GetHmacSha256SigningKey");
            target = new JwtApiRequest("target3", "localhost/hoge3");

            SymmetricSecurityKey key = (SymmetricSecurityKey)method.Invoke(target, new object[] { });
            Assert.IsNotNull(key);
            Assert.AreEqual(key.GetSymmetricKey().Length, 64);
        }

        [TestMethod]
        public void GetHmacSha256SigningKey_正常系_キーが64文字を超える場合()
        {
            var method = ReflectionAccessor.GetMethod<JwtApiRequest>("GetHmacSha256SigningKey");
            target = new JwtApiRequest("target4", "localhost/hoge4");

            SymmetricSecurityKey key = (SymmetricSecurityKey)method.Invoke(target, new object[] { });
            Assert.IsNotNull(key);
            Assert.AreEqual(key.GetSymmetricKey().Length, 65);
        }

        [TestMethod]
        public void Dispose_IDisposableフィールドの実体がある()
        {
            target.Dispose();
            // 「disposeされた」をハンドルすることは出来ないようなので、例外が出たりしなければよい
        }

        [TestMethod]
        public void Dispose_IDisposableフィールドがnullの場合()
        {
            ReflectionAccessor.GetField<JwtApiRequest>("webApiInvoker").SetValue(target, null);
            // 例外が出たりしなければよい
            target.Dispose();
        }

    }
}
