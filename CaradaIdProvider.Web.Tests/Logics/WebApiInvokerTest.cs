﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace MTI.CaradaIdProvider.Web.Tests.Logics
{
    /// <summary>
    /// WebApiInvokerのテストクラス
    /// </summary>
    [TestClass]
    public class WebApiInvokerTest
    {
        // APIモックのURL
        private string _mockApiUrl = "https://dev-hclnln-apimock.cloudapp.net/CaradaIdProviderWebApiInvokerTest";

        [TestMethod]
        public void PostJson_正常系_APIをコールできる()
        {
            CreateApiMockSetting();
            var target = new WebApiInvoker();
            var header = new Dictionary<string, List<string>>
            {
                {"headerKey", new List<string>{"headerValue"}}
            };
            var result = target.PostJson(_mockApiUrl, header, new JObject());
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void AddHedderValue_正常系_ヘッダーに値を追加できる()
        {
            var header = new HttpClient().DefaultRequestHeaders;
            var addonHeader = new Dictionary<string, List<string>>
            {
                {"newKey", new List<string>{ "newValue1", "newValue2" }},
                {"User-Agent", new List<string>{ "userAgentValue" }},
                {"Host", new List<string>{ "hostInfo" }}
            };

            var pt = new PrivateType(typeof(WebApiInvoker));
            pt.InvokeStatic("AddHedderValue", header, addonHeader, "https://TestDomain/Test");

            var result = header.GetValues("newKey").ToList();

            Assert.AreEqual("newValue1", result[0]);
            Assert.AreEqual("newValue2", result[1]);
        }

        /// <summary>
        /// APIモックにテストデータを投入します。
        /// </summary>
        private void CreateApiMockSetting()
        {
            var context = new ApiMockStorageContext();
            var tokenTable = context.MockSettingTable;
            tokenTable.InsertOrReplaceEntity(new MockSetting()
            {
                PartitionKey = "CaradaIdProviderWebApiInvokerTest",
                RowKey = "1",
                ContentType = "application/json",
                ResponseBody = "{\"result\":\"OK\"}",
                HeadersJson = "{\"headerKey\":[\"headerValue\"]}"
            });
        }

        [TestMethod]
        public void ServerCertificateValidationCallback_正常系_Coverage向上()
        {
            var methods = typeof(WebApiInvoker).GetRuntimeMethodsWithAnonymous();

            var target = new WebApiInvoker();

            foreach (var method in methods)
            {
                var parameters = method.GetParameters();

                if (parameters.Count() == 4 &&
                    parameters[0].ParameterType == typeof(object) &&
                    parameters[1].ParameterType == typeof(X509Certificate) &&
                    parameters[2].ParameterType == typeof(X509Chain) &&
                    parameters[3].ParameterType == typeof(SslPolicyErrors))
                {
                    Assert.IsTrue((Boolean)method.InvokeWithAnonymous(target, new object[] { null, null, null, null }));
                }
            }
        }

        [TestMethod]
        public void Dispose_IDisposableフィールドの実体がある()
        {
            var target = new WebApiInvoker();
            target.Dispose();
            // 「disposeされた」をハンドルすることは出来ないようなので、例外が出たりしなければよい
        }

        [TestMethod]
        public void Dispose_IDisposableフィールドがnullの場合()
        {
            var target = new WebApiInvoker();
            ReflectionAccessor.GetField<WebApiInvoker>("httpClient").SetValue(target, null);
            // 例外が出たりしなければよい
            target.Dispose();
        }
    }
}
