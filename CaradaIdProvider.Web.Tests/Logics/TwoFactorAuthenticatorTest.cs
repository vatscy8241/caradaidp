﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Models.AuthorizationStorageEntities;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Logics
{
    [TestClass]
    public class TwoFactorAuthenticatorTest
    {
        private TwoFactorAuthenticator target;

        private CloudTable twoFactorLogin;

        [TestInitialize]
        public void Initialize()
        {
            target = new TwoFactorAuthenticator();

            AuthorizationStorageContext.CreateCloudTables();
            twoFactorLogin = AuthorizationStorageContext.TwoFactorLogin;
        }

        private void SetField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<TwoFactorAuthenticator>(name);
            field.SetValue(target, o);
        }

        [TestMethod]
        public void TestCompleteTwoFactorLogin_正常系_認証完了すること()
        {
            var mockIdGenarator = new Mock<IdGenerator>();
            mockIdGenarator.Setup(s => s.GenerateTwoFactorLoginKey()).Returns("TestTwoFactorLogin");
            SetField("idGenerator", mockIdGenarator.Object);

            var entity = twoFactorLogin.RetrieveEntity<TwoFactorLoginEntity>("TestTwoFactorLogin", "-");

            if (entity != null)
            {
                twoFactorLogin.DeleteEntity(entity);
            }

            var ret = target.CompleteTwoFactorLogin("id", "userName");

            entity = twoFactorLogin.RetrieveEntity<TwoFactorLoginEntity>("TestTwoFactorLogin", "-");

            Assert.IsNotNull(entity);
            Assert.AreEqual<string>(entity.UserId, "id");

            Assert.IsNotNull(ret);
            Assert.AreEqual<string>(ret.Name, "TwoFactorLogin");
            Assert.AreEqual<string>(ret.Value, "TestTwoFactorLogin");
            Assert.IsTrue(ret.HttpOnly);
            // 2秒の幅を持たせる
            Assert.IsTrue(ret.Expires >= DateTime.UtcNow.AddYears(10).AddSeconds(-2));
            Assert.AreEqual<string>(ret.Path, "/");
            Assert.IsNull(ret.Domain);

            mockIdGenarator.VerifyAll();
        }

        [TestMethod]
        public void AddTwoFactorLoginCookie_正常系_クッキーが設定されること()
        {
            MvcMockHelpers.FakeHttpContext();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(new HttpCookieCollection());

            CookieModel inputCookie = new CookieModel
            {
                Name = "TwoFactorLogin",
                Value = "storageKey",
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddYears(10),
                Path = "/",
                Secure = true
            };

            target.AddTwoFactorLoginCookie(MvcMockHelpers.MockResponse.Object, inputCookie);

            // Cookieの確認
            var cookie = MvcMockHelpers.MockResponse.Object.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("storageKey", cookie.Value);
            Assert.IsTrue(cookie.Secure);
            Assert.AreEqual("/", cookie.Path);

            MvcMockHelpers.MockResponse.VerifyAll();
        }

        [TestMethod]
        public void AddTwoFactorLoginCookie_異常系_引数がnull()
        {
            MvcMockHelpers.FakeHttpContext();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(new HttpCookieCollection());

            CookieModel inputCookie = new CookieModel
            {
                Name = "TwoFactorLogin",
                Value = "storageKey",
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddYears(10),
                Path = "/",
                Secure = true
            };

            target.AddTwoFactorLoginCookie(null, inputCookie);

            // ResponseからCookies取得が呼ばれないことを確認
            MvcMockHelpers.MockResponse.Verify(mr => mr.Cookies, Times.Never);

            target.AddTwoFactorLoginCookie(MvcMockHelpers.MockResponse.Object, null);

            // ResponseからCookies取得が呼ばれないことを確認
            MvcMockHelpers.MockResponse.Verify(mr => mr.Cookies, Times.Never);
        }
    }
}
