﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Logics;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Logics
{
    /// <summary>
    /// HashConverterTest の概要の説明
    /// </summary>
    [TestClass]
    public class HashConverterTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private HashConverter target;

        [TestInitialize]
        public void TestInitialize()
        {
            target = new HashConverter();
        }

        [TestMethod]
        public void ConvertString_正常系_ハッシュ化されること()
        {
            var resultHash = target.ConvertString("ハッシュ化テスト");
            Assert.IsNotNull(resultHash);
            Assert.AreEqual(resultHash, "157f066d34620daabe06feab3b64b31f75bd5bf9cdb3d74a32e81fadae0ab694");
        }

        [TestMethod]
        public void ConvertString_異常系_引数にNull指定の場合エラーとなる()
        {
            try
            {
                var resultHash = target.ConvertString(null);
                Assert.Fail("例外が発生しない");
            }
            catch (ArgumentNullException e)
            {
                Assert.AreEqual(e.GetType().Name, "ArgumentNullException");
            }
        }
    }
}
