﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using Org.BouncyCastle.Crypto;
using System;
using System.Security.Cryptography;

namespace MTI.CaradaIdProvider.Web.Tests.Logics
{
    /// <summary>
    /// RSAEncryptorのテストクラス。
    /// </summary>
    [TestClass]
    public class RSAEncryptorTest
    {
        /// <summary>
        /// 秘密鍵をロードしたテスト対象
        /// </summary>
        private RSAEncryptor secretTarget;
        /// <summary>
        /// 公開鍵をロードしたテスト対象
        /// </summary>
        private RSAEncryptor publicTarget;

        [TestInitialize]
        public void Initialize()
        {
            secretTarget = new RSAEncryptor("-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAjJKWVDRcpKi7Xnl+WCLoZlsJcnBaWGeUIlYJ4yZYkJiqHzqM\nTfjLSmRSeBfpos9r6kdU8U3UCpCxy3/ILCDevN1udSzf0rMsaDYGmBjrtIDGFYK8\n8/J5Dmir79l2mOeNyAJ06j7cRdyEoyLh8iUuWqR+YIdIk+liwxWeWHx6nWYZBmJi\nMOzH9TLHQfDTejWnWKJZoZ1NWXnxICtXEM3gPtMCJZwbIzU0ecSDK1DOxwjBEX/9\nZw4L105w0ZFAdRmhrCkD5B0ROFXhWLDkxIw3Bqe2Xj9n/gJima90u+ilDGRQU3Qb\n2cVQhRmmoTLymerwH7nN4NKICWz78o/EOjvrkQIDAQABAoIBAQCKOd4+gJqUgyJz\nXK5DBlhVxbD9acM86OT9+nKQfPVbgfLO8Ghkh3+G8C0zz/e6U5H+9hNtkCUwGmXq\nAAWadtvvBSIHcI/KhMrm0jEbwG2NuY6OOleI3WXyKDThjYmUAyUkhl3mtNcujw+R\nxsw8qhjxooxuaLiSUUz/HbPtuMFtITGMzW3oTrSoyH88KZNacsqS5A6wnQDzByxr\nPrA6cdmxeqRBV+dr7QOLYUw7peK1DDAFh8wcikdonevvw3kErevNCbPZQb1sjx4B\n9Mq0fXo04h6TbqwxILKAC8LLt9N8+paNbJqRFxseYCRbJ/UQPqlZbgZ+TSF+qgX7\nD8a2PlFRAoGBAMY6kH6H4nMo9zw9GkPw8AElWqARqx99iBnPqtpWwu+uboEH+ZyB\nHJLcFzcXqXXJDE7kclt5+hniJxmH88htMtWIl3qXCTcc6rSE9m6Le6ajaXdDd+IG\nrXXVCZ2EIN2cGt1WllRsJo02jNKKDfJSvpgL2fe3CeA0gJO9Exf8efiVAoGBALWK\naqs/cuRBPPH8znVtyCJJY3yCnI23LLpJJPeBHMupgmgMoLlOx3ihSuRRti//BPZ4\nKoKT5cv6GR8FtBvpcdI5mBJRHHxJjZtELcXi6IjIad7mevuMjiZ3PxzGRot2aYZn\nwf/UvVy+KyA23BjKoeNXX6bz4+59z/chnRDE8hwNAoGAA0/hMCwHDZOGpfwDX1U8\nPVipbfk9mbYYOw4sNZ+zfBu2/vLqKcJCfvWscaA0laI5UWwhuQONuTb9HkEWQqgI\nbGbu2P6DCeeyyDdcgrZT55HdZj+7Vgx0g35+vaIChpFgJYiX1GtGLZ/WtIHX/8+A\nHbw89RWa1aMq7sz+76ypQQ0CgYEAihs4FPka7XyX4bwiUIFo7HSKQ9GVSdZdJ4a3\nobbOcj2NYJyitPaylxmVQObMw+ytGNfH9ziObEPiUpBjOYy5s2VvRBgCpKbMHJ+c\ndtutbUmSCSmry6xdA/RZxgks57H4i3BpzLdKK8DcTBCQFigpSitRMFCASRHbYTVo\nKLa7PQ0CgYBbVuFjYtTKqLQY/km48E9k45HsUCpBUJax28giE02pHLHnTxCqLwL2\niWzVQ5PwqkGG+wiobiJlI+/DuX7nuvLtgs9gRbhtlpFXnl1d6710U8LGtagkOxe2\nGPST/1yOT6tG9G+9tzdZXqkXFV4vPjpGUCunRC9p1jn20fRSkP+zOw==\n-----END RSA PRIVATE KEY-----");
            publicTarget = new RSAEncryptor("-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjJKWVDRcpKi7Xnl+WCLo\nZlsJcnBaWGeUIlYJ4yZYkJiqHzqMTfjLSmRSeBfpos9r6kdU8U3UCpCxy3/ILCDe\nvN1udSzf0rMsaDYGmBjrtIDGFYK88/J5Dmir79l2mOeNyAJ06j7cRdyEoyLh8iUu\nWqR+YIdIk+liwxWeWHx6nWYZBmJiMOzH9TLHQfDTejWnWKJZoZ1NWXnxICtXEM3g\nPtMCJZwbIzU0ecSDK1DOxwjBEX/9Zw4L105w0ZFAdRmhrCkD5B0ROFXhWLDkxIw3\nBqe2Xj9n/gJima90u+ilDGRQU3Qb2cVQhRmmoTLymerwH7nN4NKICWz78o/EOjvr\nkQIDAQAB\n-----END PUBLIC KEY-----");
        }

        [TestMethod]
        public void コンストラクタ_異常系_サポートしていない形式の場合()
        {
            try
            {
                new RSAEncryptor("-----BEGIN CERTIFICATE REQUEST-----\nMIICmTCCAYECAQAwVDELMAkGA1UEBhMCSlAxDjAMBgNVBAgMBVRva3lvMQ4wDAYD\nVQQHDAVUb2t5bzERMA8GA1UECgwITVRJIEx0ZC4xEjAQBgNVBAMMCWxvY2FsaG9z\ndDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMQl5BDsiGxfemTLeDdY\nrhMQWmgRUyCalJlbPioZh80bmORIcn5QAqoaNkrsR8bkMC8/blfqNRDbOy/BUc6z\nDWIzCvg3oXQ8sDTMhpK9cvm7iBQ6NoJx/j9d3PWadVU27heGgWSKrcULqHCrnqQ/\n+8alTwovuGpK3NRs6RQtRPOYjbcUy3KVi7IVCiX5VVkkS3C7U8HifVoeWTaavMmW\nRM+TYlLYMo6fAAS0aWPDmyZ9C0SiL/HB7i3p6oLdmKnYWw3EoVJS5MNcjxZD+wAq\nYugxRQNtfVivEcoE7UFaVq/bhVGb2l2h6G3/3bLAyUZeD12X9shuBu0vIbSrODnk\n8P0CAwEAAaAAMA0GCSqGSIb3DQEBCwUAA4IBAQAfM9gIHY3PjmwbfzE/gaCRHS6n\nRZd0FrtWnQZEvf7U1EVO8xvBa/Rfl6uqJhK8xuzBoaJWyA/stuP8Lab2la8VJfui\nG2a1dtlwMPVnnh+b+ny6rk3VhOBLnaBzKwgmZ96nvsFxvX6yrkc1uctTDzEUSPIY\nVLJ/xfwzCy+TcpDM02PMhXHxq0Zivlnn+RJB/x9zEL4VHbtOkF2F50I0KxOIM0TR\n7z1ji0wYb0F2NZcnJBVEWCgXRPuP4ixIqbTHiZ+NaJ+WErpT8Ns8te5jUf5gF0xc\n//B4iuwFgSxWgMoG57IAH//kMFodvM9oEyf6XVYiZMsXxyLq9b63F7xRDBuL\n-----END CERTIFICATE REQUEST-----");
                Assert.Fail("例外が発生しない");
            }
            catch (CryptographicException)
            {
            }
        }

        [TestMethod]
        public void Encrypt_Decrypt_正常系_公開鍵で暗号化して秘密鍵で復号できること()
        {
            var plain = "あいうえお";
            var encrypt = publicTarget.Encrypt(plain);

            Assert.AreNotEqual(plain, encrypt);

            Assert.AreEqual(plain, secretTarget.Decrypt(encrypt));

            // 公開鍵では復号できないこと
            try
            {
                publicTarget.Decrypt(encrypt);
                Assert.Fail("例外が発生しない");
            }
            catch (CryptographicException)
            {
            }
        }

        [TestMethod]
        public void Encrypt_Decrypt_正常系_秘密鍵で暗号化して公開鍵で復号できること()
        {
            var plain = "あいうえお";
            var encrypt = secretTarget.Encrypt(plain);

            Assert.AreNotEqual(plain, encrypt);

            Assert.AreEqual(plain, publicTarget.Decrypt(encrypt));

            // 秘密鍵では復号できないこと
            try
            {
                secretTarget.Decrypt(encrypt);
                Assert.Fail("例外が発生しない");
            }
            catch (CryptographicException)
            {
            }
        }

        [TestMethod]
        public void Encrypt_Decrypt_正常系_暗号化したときとは別のインスタンスでも復号できること()
        {
            var plain = "かきくけこ";
            var encrypt = publicTarget.Encrypt(plain);

            Initialize();
            Assert.AreEqual(plain, secretTarget.Decrypt(encrypt));
        }

        [TestMethod]
        public void Encrypt_異常系_暗号化に失敗した場合()
        {
            var mockRsa = new Mock<IAsymmetricBlockCipher>();
            mockRsa.Setup(s => s.Init(true, It.IsAny<ICipherParameters>())).Throws(new Exception("dummy"));

            var field = ReflectionAccessor.GetField<RSAEncryptor>("rsa");
            field.SetValue(secretTarget, mockRsa.Object);

            try
            {
                secretTarget.Encrypt("hoge");
                Assert.Fail("例外が発生しない");
            }
            catch (CryptographicException)
            {
            }

            mockRsa.VerifyAll();
        }

        [TestMethod]
        public void ToRSA_正常系_PublicKeyで呼び出し場合はオブジェクトが変換されること()
        {
            var rsa = publicTarget.ToRSA();
            Assert.IsNotNull(rsa);
        }

        [TestMethod]
        public void ToRSA_正常系_SecretKeyで呼び出し場合はオブジェクトが変換されること()
        {
            var rsa = secretTarget.ToRSA();
            Assert.IsNotNull(rsa);
        }
    }
}
