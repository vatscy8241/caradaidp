﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Logics;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MTI.CaradaIdProvider.Web.Tests.Logics
{
    /// <summary>
    /// GenerateIdUtilのテストクラス。
    /// </summary>
    [TestClass]
    public class IdGeneratorTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private IdGenerator target;

        /// <summary>
        /// テスト対象(正規表現)
        /// </summary>
        private TestModel regexTarget;

        private bool isLastValidate = false;

        [TestInitialize]
        public void Initialize()
        {
            target = new IdGenerator();
            regexTarget = new TestModel();
        }

        [TestMethod]
        public void GenerateSubjectId_正常系_生成されること()
        {
            //32文字ハイフンなしで生成されていることを確認
            var sub = target.GenerateSubjectId();
            Assert.IsNotNull(sub);
            Assert.AreEqual(sub.Length, 32);
            Assert.AreEqual(sub.IndexOf("-"), -1);
        }

        [TestMethod]
        public void GenerateAuthorizationCode_正常系_生成されること()
        {
            //32文字ハイフンなしで生成されていることを確認
            var sub = target.GenerateAuthorizationCode();
            Assert.IsNotNull(sub);
            Assert.AreEqual(sub.Length, 32);
            Assert.AreEqual(sub.IndexOf("-"), -1);
        }

        [TestMethod]
        public void GenerateTwoFactorLoginKey_正常系_生成されること()
        {
            //32文字ハイフンなしで生成されていることを確認
            var sub = target.GenerateTwoFactorLoginKey();
            Assert.IsNotNull(sub);
            Assert.AreEqual(sub.Length, 32);
            Assert.AreEqual(sub.IndexOf("-"), -1);
        }

        [TestMethod]
        public void GenerateRandomPassword_正常系_パスワードが生成されること()
        {
            var regex = new Regex(@"^(?=.*?[a-z])(?=.*?[0-9])[a-z0-9]{1,}$", RegexOptions.Compiled | RegexOptions.ExplicitCapture);

            // 特定回数実行し、パスワードの形式通り作成されていることを確認する。
            for (int i = 0; i < 100; i++)
            {
                var password = target.GenerateRandomPassword();
                Assert.IsNotNull(password);
                Assert.AreEqual(8, password.Length);
                Assert.IsTrue(regex.IsMatch(password));
            }
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_正常系_半角英小文字半角数字1文字ずつ()
        {
            // (ASCII) 0-9 => (int) 48-57
            for (var asciiCodeNum = 48; asciiCodeNum <= 57; asciiCodeNum++)
            {
                var numChar = ((char)asciiCodeNum).ToString();

                // (ASCII) a-z => (int) 97-122
                for (var asciiCodeAlphabet = 97; asciiCodeAlphabet <= 122; asciiCodeAlphabet++)
                {
                    var alphabetChar = ((char)asciiCodeAlphabet).ToString();
                    regexTarget.Password = numChar + alphabetChar;
                    var results = DoValidate();
                    Assert.IsTrue(isLastValidate);

                    regexTarget.Password = alphabetChar + numChar;
                    results = DoValidate();
                    Assert.IsTrue(isLastValidate);
                }
            }
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_正常系_半角英数1文字ずつとASCII全文字()
        {
            // ASCII全文字について1文字ずつ調査する
            // 0123456789  abcdefghijklmnopqrstuvwxyz
            // ASCIIコード
            // 48-57       97-122

            // 後方ASCII
            for (var asciiCode = 48; asciiCode <= 57; asciiCode++)
            {
                regexTarget.Password = "z9" + ((char)asciiCode).ToString();

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }
            for (var asciiCode = 97; asciiCode <= 122; asciiCode++)
            {
                regexTarget.Password = "z9" + ((char)asciiCode).ToString();

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }

            // 中間ASCII
            for (var asciiCode = 48; asciiCode <= 57; asciiCode++)
            {
                regexTarget.Password = "z" + ((char)asciiCode).ToString() + "8";

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }
            for (var asciiCode = 97; asciiCode <= 122; asciiCode++)
            {
                regexTarget.Password = "z" + ((char)asciiCode).ToString() + "8";

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }

            // 前方ASCII
            for (var asciiCode = 48; asciiCode <= 57; asciiCode++)
            {
                regexTarget.Password = ((char)asciiCode).ToString() + "h4";

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }
            for (var asciiCode = 97; asciiCode <= 122; asciiCode++)
            {
                regexTarget.Password = ((char)asciiCode).ToString() + "h4";

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_異常系_半角英小文字のみ()
        {
            regexTarget.Password = "abcdefg";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_異常系_半角数字のみ()
        {
            regexTarget.Password = "12345688";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_異常系_全角数字混入()
        {
            regexTarget.Password = "a12345６7";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = "１a1234567";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = "a123456７";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_異常系_全角英字混入()
        {
            regexTarget.Password = "abcＤ1234";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = "Ａbcd1234";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = "abcd123Ｅ";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_異常系_半角英大文字混入()
        {
            regexTarget.Password = "aBcd1234";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = "Abcd1234";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = "abcd123E";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_異常系_記号混入()
        {
            regexTarget.Password = "abc/1234";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = ".bcd1234";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

            regexTarget.Password = "abcd123_";
            results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
        }

        [TestMethod]
        public void GenerateRandomPassword_正規表現_異常系_半角英小文字数字以外が含まれている()
        {
            // 許諾されていないASCII全文字について調査する（1文字ずつ）
            // !"#$%&'()*+,-./:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`{|}~
            // ASCIIコード
            // 33-126(48-57,97-122を除く)
            for (var asciiCode = 33; asciiCode <= 47; asciiCode++)
            {
                regexTarget.Password = "a123" + ((char)asciiCode).ToString() + "1bd";

                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

                regexTarget.Password = ((char)asciiCode).ToString() + "a1234bd";

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

                regexTarget.Password = "a1234bd" + ((char)asciiCode).ToString();

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
            }

            for (var asciiCode = 58; asciiCode <= 96; asciiCode++)
            {
                regexTarget.Password = "a123" + ((char)asciiCode).ToString() + "1bd";

                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

                regexTarget.Password = ((char)asciiCode).ToString() + "a1234bd";

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

                regexTarget.Password = "a1234bd" + ((char)asciiCode).ToString();

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
            }


            for (var asciiCode = 123; asciiCode <= 126; asciiCode++)
            {
                regexTarget.Password = "a123" + ((char)asciiCode).ToString() + "1bd";

                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

                regexTarget.Password = ((char)asciiCode).ToString() + "a1234bd";

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);

                regexTarget.Password = "a1234bd" + ((char)asciiCode).ToString();

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual("パスワード混在エラー", results[0].ErrorMessage);
            }
        }

        private class TestModel
        {
            [RegularExpression("^(?=.*?[a-z])(?=.*?[0-9])[a-z0-9]{1,}$", ErrorMessage = "パスワード混在エラー")]
            public string Password { get; set; }
        }

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(regexTarget, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(regexTarget, context, results, true);
            return results;
        }
    }
}
