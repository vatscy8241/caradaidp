﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.DataBase;
using System.Runtime.Remoting.Messaging;

namespace MTI.CaradaIdProvider.Web.Tests.DataBase
{
    [TestClass]
    public class GlobalDbConfigurationTest
    {
        private GlobalDbConfiguration target;

        [TestMethod]
        public void Constructor_SuspendExecutionStrategyがfalseのとき()
        {
            GlobalDbConfiguration.SuspendExecutionStrategy = false;
            target = new GlobalDbConfiguration();
            Assert.IsFalse(GlobalDbConfiguration.SuspendExecutionStrategy);
        }

        [TestMethod]
        public void Constructor_SuspendExecutionStrategyがtrueのとき()
        {
            GlobalDbConfiguration.SuspendExecutionStrategy = true;
            target = new GlobalDbConfiguration();
            Assert.IsTrue(GlobalDbConfiguration.SuspendExecutionStrategy);
        }

        [TestMethod]
        public void SuspendExecutionStratetyが未設定のとき()
        {
            CallContext.LogicalSetData("SuspendExecutionStrategy", null);
            Assert.IsFalse(GlobalDbConfiguration.SuspendExecutionStrategy);
        }
    }
}
