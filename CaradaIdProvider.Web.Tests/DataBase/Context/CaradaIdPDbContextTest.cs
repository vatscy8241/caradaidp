﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System.Linq;

namespace MTI.CaradaIdProvider.Web.Tests.DataBase.Context
{
    /// <summary>
    /// DbContextクラスはモックを使用するため、UT対象としない。カバレッジ目的のテストメソッドのみ実行する。
    /// </summary>
    [TestClass]
    public class CaradaIdPDbContextTest
    {
        CaradaIdPDbContext target;

        [TestMethod]
        public void ログ出力のdelegateが機能すること()
        {
            // ほかにメソッドもないしこの絞込みで十分だろう
            target = new CaradaIdPDbContext();
            var method = (from m in typeof(CaradaIdPDbContext).GetRuntimeMethodsWithAnonymous()
                          where m.ReturnType == typeof(void)
                          && m.GetParameters().Count() == 1
                          && m.GetParameters()[0].ParameterType == typeof(string)
                          select m).SingleOrDefault();
            method.InvokeWithAnonymous(target, new string[] { "a" });
        }
    }
}
