﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Providers;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;

namespace MTI.CaradaIdProvider.Web.Tests.Providers
{
    /// <summary>
    /// 認可コードプロバイダのテストクラス
    /// </summary>
    [TestClass]
    public class CaradaIdAuthorizationCodeProviderTest
    {
        private CaradaIdAuthorizationCodeProvider target;
        private Mock<CaradaIdPDbContext> mockDbContext;
        private Mock<IOwinContext> mockOwinContext;
        private Mock<IdGenerator> mockIdGenerator;
        private Mock<IPrincipal> mockPrincipal;
        private Mock<IIdentity> mockIdentity;

        /// <summary>
        /// ダミーのSecureDataFormat
        /// </summary>
        private class DummyTicketDataFormat : ISecureDataFormat<AuthenticationTicket>
        {
            public string Protect(AuthenticationTicket data)
            {
                data.Identity.AddClaim(new System.Security.Claims.Claim("Myprotectionmethod", "true"));
                return data.ToString();
            }

            public AuthenticationTicket Unprotect(string protectedText)
            {
                throw new NotImplementedException();
            }
        }

        [TestInitialize]
        public void Initialize()
        {
            mockDbContext = new Mock<CaradaIdPDbContext>();
            mockDbContext.Setup(m => m.RedirectUriMasters).Returns(new TestRedirectUriMastersDbSet());
            mockDbContext.Setup(m => m.UserAuthCodes).Returns(new TestUserAuthCodesDbSet());
            mockDbContext.Setup(m => m.Users).Returns(new TestAspNetUsersDbSet());
            mockDbContext.Setup(m => m.AuthorizedUsers).Returns(new TestAuthorizedUsersDbSet());

            mockIdGenerator = new Mock<IdGenerator>();
            target = new CaradaIdAuthorizationCodeProvider();

            ReflectionAccessor.GetField<CaradaIdAuthorizationCodeProvider>("idGenerator").SetValue(target, mockIdGenerator.Object);

            mockOwinContext = new Mock<IOwinContext>();
            mockOwinContext.Setup(m => m.Request).Returns(new OwinRequest());
            mockOwinContext.Setup(m => m.Response).Returns(new OwinResponse());

            mockPrincipal = new Mock<IPrincipal>();
            mockIdentity = new Mock<IIdentity>();
            mockIdentity.Setup(m => m.AuthenticationType).Returns("ApplicationCookie");
            mockIdentity.Setup(m => m.Name).Returns("TestTaro");
            mockPrincipal.Setup(m => m.Identity).Returns(mockIdentity.Object);

            var mockDbContextFactory = new Mock<DbContextFactory>();
            mockDbContextFactory.Setup(s => s.NewCaradaIdPDbContext()).Returns(mockDbContext.Object);

            ReflectionAccessor.GetField<CaradaIdAuthorizationCodeProvider>("dbFactory").SetValue(target, mockDbContextFactory.Object);
        }

        [TestMethod]
        public void CreateAsync_正常系()
        {
            // DBデータの登録
            var userData = mockDbContext.Object.Users;
            userData.Add(new CaradaIdUser
            {
                Id = "Id",
                UserName = "TestTaro"
            });
            var redirecUriMasters = mockDbContext.Object.RedirectUriMasters;
            redirecUriMasters.Add(new RedirectUriMasters
            {
                SeqNo = 1,
                ClientId = "AAA",
                RedirectUri = "https://test.com/token"
            });

            mockIdGenerator.Setup(m => m.GenerateAuthorizationCode()).Returns("12345");

            var context = new AuthenticationTokenCreateContext(mockOwinContext.Object, new DummyTicketDataFormat(),
                new AuthenticationTicket(new ClaimsIdentity(mockIdentity.Object),
                    new AuthenticationProperties(
                        new Dictionary<string, string>
                        {
                            {"clientId", "AAA"},
                            {"redirectUri", "https://test.com/token"}
                        })
                    {
                        ExpiresUtc = DateTimeOffset.UtcNow.Add(TimeSpan.FromDays(1))
                    }));

            target.CreateAsync(context);

            // 認可コードが生成されること
            Assert.AreEqual("12345", context.Token);

            // 認可コードの登録の確認
            var userAuthCodes = mockDbContext.Object.UserAuthCodes.Find("12345");
            Assert.IsNotNull(userAuthCodes);
            Assert.AreEqual("12345", userAuthCodes.AuthorizationCode);
            Assert.AreEqual("Id", userAuthCodes.UserId);
            Assert.AreEqual(1, userAuthCodes.RedirectUriSeqNo);

            mockIdGenerator.VerifyAll();
        }

        [TestMethod]
        public void CreateAsync_リダイレクトURI複数の正常系()
        {
            // DBデータの登録
            var userData = mockDbContext.Object.Users;
            userData.Add(new CaradaIdUser
            {
                Id = "Id",
                UserName = "TestTaro"
            });
            var redirecUriMasters = mockDbContext.Object.RedirectUriMasters;
            redirecUriMasters.Add(new RedirectUriMasters
            {
                SeqNo = 1,
                ClientId = "AAA",
                RedirectUri = "https://test.com/token1"
            });
            redirecUriMasters.Add(new RedirectUriMasters
            {
                SeqNo = 2,
                ClientId = "AAA",
                RedirectUri = "https://test.com/token"
            });

            mockIdGenerator.Setup(m => m.GenerateAuthorizationCode()).Returns("12345");

            var context = new AuthenticationTokenCreateContext(mockOwinContext.Object, new DummyTicketDataFormat(),
                new AuthenticationTicket(new ClaimsIdentity(mockIdentity.Object),
                    new AuthenticationProperties(
                        new Dictionary<string, string>
                        {
                            {"clientId", "AAA"},
                            {"redirectUri", "https://test.com/token"}
                        })
                    {
                        ExpiresUtc = DateTimeOffset.UtcNow.Add(TimeSpan.FromDays(1))
                    }));

            target.CreateAsync(context);

            // 認可コードが生成されること
            Assert.AreEqual("12345", context.Token);

            // 認可コードの登録の確認
            var userAuthCodes = mockDbContext.Object.UserAuthCodes.Find("12345");
            Assert.IsNotNull(userAuthCodes);
            Assert.AreEqual("12345", userAuthCodes.AuthorizationCode);
            Assert.AreEqual("Id", userAuthCodes.UserId);
            Assert.AreEqual(2, userAuthCodes.RedirectUriSeqNo);

            mockIdGenerator.VerifyAll();
        }

        [TestMethod]
        public void ReceiveAsync_対象認証コードなし()
        {
            var authorizedUsers = mockDbContext.Object.AuthorizedUsers;
            authorizedUsers.Add(new AuthorizedUsers
            {
                UserId = "aaa",
                ClientId = "bbb",
                Sub = "sub"
            });

            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser
            {
                Id = "aaa",
                UserName = "name"
            });

            var redirectUriMasters = mockDbContext.Object.RedirectUriMasters;
            redirectUriMasters.Add(new RedirectUriMasters
            {
                SeqNo = 1,
                ClientId = "bbb",
                RedirectUri = "https://localhost/test"
            });

            var userAuthCodes = mockDbContext.Object.UserAuthCodes;
            userAuthCodes.Add(new UserAuthCodes
            {
                AuthorizationCode = "code",
                UserId = "aaa",
                ExpireDateUtc = new DateTime(2015, 1, 16, 9, 0, 0),
                RedirectUriSeqNo = 1
            });

            var context = new AuthenticationTokenReceiveContext(
                mockOwinContext.Object,
                new DummyTicketDataFormat(),
                "xxx");

            target.ReceiveAsync(context);

            Assert.IsNull(context.Ticket);
        }

        [TestMethod]
        public void ReceiveAsync_認証コードのcaseが違う()
        {
            var authorizedUsers = mockDbContext.Object.AuthorizedUsers;
            authorizedUsers.Add(new AuthorizedUsers
            {
                UserId = "aaa",
                Sub = "sub"
            });

            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser
            {
                Id = "aaa",
                UserName = "name"
            });

            var redirectUriMasters = mockDbContext.Object.RedirectUriMasters;
            redirectUriMasters.Add(new RedirectUriMasters
            {
                SeqNo = 1,
                ClientId = "bbb",
                RedirectUri = "https://localhost/test"
            });

            var userAuthCodes = mockDbContext.Object.UserAuthCodes;
            userAuthCodes.Add(new UserAuthCodes
            {
                AuthorizationCode = "code",
                UserId = "aaa",
                ExpireDateUtc = new DateTime(2015, 1, 16, 9, 0, 0),
                RedirectUriSeqNo = 1
            });

            var context = new AuthenticationTokenReceiveContext(
                mockOwinContext.Object,
                new DummyTicketDataFormat(),
                "CODE");

            target.ReceiveAsync(context);

            Assert.IsNull(context.Ticket);
        }

        [TestMethod]
        public void ReceiveAsync_正常系_ユーザーの認可なし()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser
            {
                Id = "aaa",
                UserName = "name"
            });

            var redirectUriMasters = mockDbContext.Object.RedirectUriMasters;
            redirectUriMasters.Add(new RedirectUriMasters
            {
                SeqNo = 1,
                ClientId = "bbb",
                RedirectUri = "https://localhost/test"
            });

            var userAuthCodes = mockDbContext.Object.UserAuthCodes;
            userAuthCodes.Add(new UserAuthCodes
            {
                AuthorizationCode = "code",
                UserId = "aaa",
                ExpireDateUtc = new DateTime(2015, 1, 16, 9, 0, 0),
                RedirectUriSeqNo = 1
            });

            var context = new AuthenticationTokenReceiveContext(
                mockOwinContext.Object,
                new DummyTicketDataFormat(),
                "code");

            target.ReceiveAsync(context);

            var ticket = context.Ticket;

            Assert.IsNotNull(ticket);
            Assert.IsNotNull(ticket.Identity);
            Assert.AreEqual("https://localhost/test", ticket.Properties.RedirectUri);
            Assert.AreEqual(DateTime.SpecifyKind(new DateTime(2015, 1, 16, 9, 0, 0), DateTimeKind.Utc), ticket.Properties.ExpiresUtc);

            var dic = ticket.Properties.Dictionary;
            Assert.AreEqual("bbb", dic["client_id"]);
            Assert.AreEqual("https://localhost/test", dic["redirect_uri"]);
            Assert.IsFalse(dic.ContainsKey("sub"));

            // 認証コードが削除されていること
            Assert.IsNull(userAuthCodes.Find("code"));
        }

        [TestMethod]
        public void ReceiveAsync_正常系_ユーザーの認可あり()
        {
            var authorizedUsers = mockDbContext.Object.AuthorizedUsers;
            authorizedUsers.Add(new AuthorizedUsers
            {
                UserId = "aaa",
                ClientId = "bbb",
                Sub = "sub"
            });

            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser
            {
                Id = "aaa",
                UserName = "name"
            });

            var redirectUriMasters = mockDbContext.Object.RedirectUriMasters;
            redirectUriMasters.Add(new RedirectUriMasters
            {
                SeqNo = 1,
                ClientId = "bbb",
                RedirectUri = "https://localhost/test"
            });

            var userAuthCodes = mockDbContext.Object.UserAuthCodes;
            userAuthCodes.Add(new UserAuthCodes
            {
                AuthorizationCode = "code",
                UserId = "aaa",
                ExpireDateUtc = new DateTime(2015, 1, 16, 9, 0, 0),
                RedirectUriSeqNo = 1
            });

            var context = new AuthenticationTokenReceiveContext(
                mockOwinContext.Object,
                new DummyTicketDataFormat(),
                "code");

            target.ReceiveAsync(context);

            var ticket = context.Ticket;

            Assert.IsNotNull(ticket);
            Assert.IsNotNull(ticket.Identity);
            Assert.AreEqual("https://localhost/test", ticket.Properties.RedirectUri);
            Assert.AreEqual(DateTime.SpecifyKind(new DateTime(2015, 1, 16, 9, 0, 0), DateTimeKind.Utc), ticket.Properties.ExpiresUtc);

            var dic = ticket.Properties.Dictionary;
            Assert.AreEqual("bbb", dic["client_id"]);
            Assert.AreEqual("https://localhost/test", dic["redirect_uri"]);
            Assert.AreEqual("sub", dic["sub"]);

            // 認証コードが削除されていること
            Assert.IsNull(userAuthCodes.Find("code"));
        }
    }
}
