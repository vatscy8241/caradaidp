﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.OAuth.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Providers;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Providers
{
    /// <summary>
    /// CaradaIdOAuthServiceProviderのテストクラス.
    /// clientIdの大文字小文字が異なるケースはDB固有の事象のため、UTでは想定分岐に到達しない。
    /// stateありなしは必ず呼び出すprivateメソッド内での分岐のため、1ケースのみ実施。
    /// </summary>
    [TestClass]
    public class CaradaIdOAuthServiceProviderTest
    {
        private CaradaIdOAuthServiceProvider target;
        private Mock<CaradaIdPDbContext> mockDbContext;
        private Mock<OwinRequest> mockOwinRequest;
        private Mock<OwinResponse> mockOwinResponse;
        private Mock<IOwinContext> mockOwinContext;
        private Mock<IPrincipal> mockPrincipal;
        private Mock<IIdentity> mockIdentity;
        private Mock<ApplicationSignInManager> mockSignInManager;
        private Mock<TwoFactorAuthenticator> mockTwoFactorAuthenticator;

        /// <summary>
        /// ダミーの認可コードプロバイダ
        /// </summary>
        private class DummyAuthorizationCodeProvider : AuthenticationTokenProvider
        {
            public override System.Threading.Tasks.Task CreateAsync(AuthenticationTokenCreateContext context)
            {
                context.SetToken("12345");
                return Task.FromResult(0);
            }
        }

        /// <summary>
        /// ダミーのSecureDataFormat
        /// </summary>
        private class DummyTicketDataFormat : ISecureDataFormat<AuthenticationTicket>
        {
            public string Protect(AuthenticationTicket data)
            {
                data.Identity.AddClaim(new System.Security.Claims.Claim("Myprotectionmethod", "true"));
                return data.ToString();
            }

            public AuthenticationTicket Unprotect(string protectedText)
            {
                throw new NotImplementedException();
            }
        }

        private void SetField(string name, object o)
        {
            var field = ReflectionAccessor.GetField<CaradaIdOAuthServiceProvider>(name);
            field.SetValue(target, o);
        }

        [TestInitialize]
        public void Initialize()
        {
            mockDbContext = new Mock<CaradaIdPDbContext>();
            mockDbContext.Setup(m => m.Users).Returns(new TestAspNetUsersDbSet());
            mockDbContext.Setup(m => m.ClientMasters).Returns(new TestClientMastersDbSet());
            mockDbContext.Setup(m => m.RedirectUriMasters).Returns(new TestRedirectUriMastersDbSet());
            mockDbContext.Setup(m => m.UserAuthCodes).Returns(new TestUserAuthCodesDbSet());
            mockDbContext.Setup(m => m.AuthorizedUsers).Returns(new TestAuthorizedUsersDbSet());

            target = new CaradaIdOAuthServiceProvider();

            mockOwinRequest = new Mock<OwinRequest>() { CallBase = true };
            mockOwinResponse = new Mock<OwinResponse>() { CallBase = true };
            mockOwinContext = new Mock<IOwinContext>();
            mockOwinContext.Setup(m => m.Request).Returns(mockOwinRequest.Object);
            mockOwinContext.Setup(m => m.Response).Returns(mockOwinResponse.Object);
            mockSignInManager = new Mock<ApplicationSignInManager>(new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object).Object, new Mock<IAuthenticationManager>().Object);
            mockOwinContext.Setup(m => m.Get<ApplicationSignInManager>(It.IsAny<string>())).Returns(mockSignInManager.Object);

            mockPrincipal = new Mock<IPrincipal>();
            mockIdentity = new Mock<IIdentity>();
            mockIdentity.Setup(m => m.AuthenticationType).Returns("ApplicationCookie");
            mockIdentity.Setup(m => m.Name).Returns("TestTaro");
            mockPrincipal.Setup(m => m.Identity).Returns(mockIdentity.Object);

            MockHttpContextHelper.SetCurrentOfFakeContext();

            mockTwoFactorAuthenticator = new Mock<TwoFactorAuthenticator>();
            SetField("twoFactorAuthenticator", mockTwoFactorAuthenticator.Object);

            var mockDbContextFactory = new Mock<DbContextFactory>();
            mockDbContextFactory.Setup(s => s.NewCaradaIdPDbContext()).Returns(mockDbContext.Object);
            SetField("dbFactory", mockDbContextFactory.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            // 設定ファイルのMaintenanceModeの値を初期値に戻す
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenanceMode"].Value = "0";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }

        [TestMethod]
        public void ValidateClientRedirectUri_clientIdが未設定()
        {
            var context = new OAuthValidateClientRedirectUriContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                null, // clientId
                "a");

            target.ValidateClientRedirectUri(context).Wait();

            // Validate NGであること
            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("invalid redirect_uri", context.Error);
        }

        [TestMethod]
        public void ValidateClientRedirectUri_redirectUriが未設定()
        {
            var context = new OAuthValidateClientRedirectUriContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                "a",
                null); // redirectUri

            target.ValidateClientRedirectUri(context).Wait();

            // Validate NGであること
            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("invalid redirect_uri", context.Error);
        }

        [TestMethod]
        public void ValidateClientRedirectUri_clientIdがマスタに存在しない場合()
        {
            var dbData = mockDbContext.Object.RedirectUriMasters;
            // ClientIdは違うがRedirectUriが同じデータ
            dbData.Add(new RedirectUriMasters()
            {
                SeqNo = 1,
                ClientId = "AAA",
                RedirectUri = "https://localhost/test"
            });

            var context = new OAuthValidateClientRedirectUriContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                "BBB",
                "https://localhost/test");

            target.ValidateClientRedirectUri(context).Wait();

            // Validate NGであること
            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("invalid redirect_uri", context.Error);
        }

        [TestMethod]
        public void ValidateClientRedirectUri_clientIdのcaseが違う()
        {
            var dbData = mockDbContext.Object.RedirectUriMasters;

            dbData.Add(new RedirectUriMasters()
            {
                SeqNo = 1,
                ClientId = "AAA",
                RedirectUri = "https://localhost/test"
            });

            var context = new OAuthValidateClientRedirectUriContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                "aaa",
                "https://localhost/test");

            target.ValidateClientRedirectUri(context).Wait();

            // Validate NGであること
            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("invalid redirect_uri", context.Error);
        }

        [TestMethod]
        public void ValidateClientRedirectUri_redirectUriが違う()
        {
            var dbData = mockDbContext.Object.RedirectUriMasters;

            dbData.Add(new RedirectUriMasters()
            {
                SeqNo = 1,
                ClientId = "AAA",
                RedirectUri = "https://localhost/test"
            });

            var context = new OAuthValidateClientRedirectUriContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                "AAA",
                "https://localhost/test1");

            target.ValidateClientRedirectUri(context).Wait();

            // Validate NGであること
            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("invalid redirect_uri", context.Error);
        }

        [TestMethod]
        public void ValidateClientRedirectUri_正常系()
        {
            var dbData = mockDbContext.Object.RedirectUriMasters;

            dbData.Add(new RedirectUriMasters()
            {
                SeqNo = 1,
                ClientId = "AAA",
                RedirectUri = "https://localhost/test"
            });

            var context = new OAuthValidateClientRedirectUriContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                "AAA",
                "https://localhost/test");

            target.ValidateClientRedirectUri(context).Wait();

            // Validate OKであること
            Assert.IsTrue(context.IsValidated);
            Assert.IsFalse(context.HasError);
        }

        [TestMethod]
        public void ValidateClientRedirectUri_リダイレクトURI複数()
        {
            var dbData = mockDbContext.Object.RedirectUriMasters;

            dbData.Add(new RedirectUriMasters()
            {
                SeqNo = 1,
                ClientId = "AAA",
                RedirectUri = "https://localhost/test1"
            });
            dbData.Add(new RedirectUriMasters()
            {
                SeqNo = 2,
                ClientId = "AAA",
                RedirectUri = "https://localhost/test"
            });

            var context = new OAuthValidateClientRedirectUriContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                "AAA",
                "https://localhost/test");

            target.ValidateClientRedirectUri(context).Wait();

            // Validate OKであること
            Assert.IsTrue(context.IsValidated);
            Assert.IsFalse(context.HasError);
        }

        [TestMethod]
        public void ValidateAuthorizeRequest_scopeが未設定()
        {
            var authorizeRequest = new AuthorizeEndpointRequest(new ReadableStringCollection(
                new Dictionary<string, string[]>() {
                    {"response_type", new string[]{"code"}}
                }));

            var context = new OAuthValidateAuthorizeRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                authorizeRequest,
                null);

            target.ValidateAuthorizeRequest(context).Wait();

            // ヘッダ領域にエラーが追加されていること
            Assert.AreEqual("invalid_request", context.Request.Headers["request_error"]);
        }

        [TestMethod]
        public void ValidateAuthorizeRequest_redirectUriが未設定()
        {
            var authorizeRequest = new AuthorizeEndpointRequest(new ReadableStringCollection(
                new Dictionary<string, string[]>() {
                    {"scope", new string[]{"openid"}}
                }));

            var context = new OAuthValidateAuthorizeRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                authorizeRequest,
                null);

            target.ValidateAuthorizeRequest(context).Wait();

            // ヘッダ領域にエラーが追加されていること
            Assert.AreEqual("invalid_request", context.Request.Headers["request_error"]);
        }

        [TestMethod]
        public void ValidateAuthorizeRequest_scopeが2個以上()
        {
            var authorizeRequest = new AuthorizeEndpointRequest(new ReadableStringCollection(
                new Dictionary<string, string[]>() {
                    {"scope", new string[]{"openid", "openid"}},
                    {"response_type", new string[]{"code"}}
                }));

            var context = new OAuthValidateAuthorizeRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                authorizeRequest,
                null);

            target.ValidateAuthorizeRequest(context).Wait();

            // ヘッダ領域にエラーが追加されていること
            Assert.AreEqual("invalid_scope", context.Request.Headers["request_error"]);
        }

        [TestMethod]
        public void ValidateAuthorizeRequest_scopeがopenidでない()
        {
            var authorizeRequest = new AuthorizeEndpointRequest(new ReadableStringCollection(
                new Dictionary<string, string[]>() {
                    {"scope", new string[]{"profile"}},
                    {"response_type", new string[]{"code"}}
                }));

            var context = new OAuthValidateAuthorizeRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                authorizeRequest,
                null);

            target.ValidateAuthorizeRequest(context).Wait();

            // ヘッダ領域にエラーが追加されていること
            Assert.AreEqual("invalid_scope", context.Request.Headers["request_error"]);
        }

        [TestMethod]
        public void ValidateAuthorizeRequest_response_typeがcodeでない()
        {
            var authorizeRequest = new AuthorizeEndpointRequest(new ReadableStringCollection(
                new Dictionary<string, string[]>() {
                    {"scope", new string[]{"openid"}},
                    {"response_type", new string[]{"token"}}
                }));

            var context = new OAuthValidateAuthorizeRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                authorizeRequest,
                null);

            target.ValidateAuthorizeRequest(context).Wait();

            // ヘッダ領域にエラーが追加されていること
            Assert.AreEqual("unsupported_response_type", context.Request.Headers["request_error"]);
        }

        [TestMethod]
        public void ValidateAuthorizeRequest_正常系()
        {
            var authorizeRequest = new AuthorizeEndpointRequest(new ReadableStringCollection(
                new Dictionary<string, string[]>() {
                    {"scope", new string[]{"openid"}},
                    {"response_type", new string[]{"code"}}
                }));

            var context = new OAuthValidateAuthorizeRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                authorizeRequest,
                null);

            target.ValidateAuthorizeRequest(context).Wait();

            // ヘッダ領域にエラーがないこと
            Assert.IsTrue(context.IsValidated);
            Assert.AreEqual(0, context.Request.Headers.Count);
        }

        [TestMethod]
        public void AuthorizeEndpoint_ValidateAuthorizeRequestでバリデートエラーの場合_stateなし()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}}
                })));

            // ヘッダにエラー追加
            endpointContext.Request.Headers.Add("request_error", new string[] { "invalid_scope" });

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // リダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=invalid_scope", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);
        }

        [TestMethod]
        public void AuthorizeEndpoint_ValidateAuthorizeRequestでバリデートエラーの場合_stateあり()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));

            // ヘッダにエラー追加
            endpointContext.Request.Headers.Add("request_error", new string[] { "invalid_scope" });

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // リダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=invalid_scope&state=aaaaa", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);
        }

        [TestMethod]
        public void AuthorizeEndpoint_MaintenanceModeが1の場合()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizeEndpointPath = new PathString("/oauth2/test")
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenanceMode"].Value = "1";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // エラーリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=temporarily_unavailable&state=aaaaa", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);
        }

        [TestMethod]
        public void AuthorizeEndpoint_MaintenanceModeが2の場合()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizeEndpointPath = new PathString("/oauth2/test")
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}}
                })));
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenanceMode"].Value = "2";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // エラーリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=temporarily_unavailable", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);
        }

        [TestMethod]
        public void AuthorizeEndpoint_MaintenanceModeが0だがメンテナンス中で画面から戻ってきた場合()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizeEndpointPath = new PathString("/oauth2/test")
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));

            HttpContext.Current.Session.Add("AuthorizeEndpointSession", new AuthorizeEndpointSession()
            {
                LoginResult = LoginResult.Maintenance
            });

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // エラーリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=temporarily_unavailable&state=aaaaa", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);
        }

        [TestMethod]
        public void AuthorizeEndpoint_ログインしていない場合()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizeEndpointPath = new PathString("/oauth2/test")
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}}
                })));

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // ログイン画面にリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("/User/LoginView", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);

            // セッションが作成されること
            var session =
                HttpContext.Current.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;

            Assert.IsNotNull(session);
            Assert.AreEqual("AAA", session.ClientId);
            Assert.AreEqual("https://localhost/test", session.RedirectUri);
            Assert.AreEqual("openid", session.Scope);
            Assert.AreEqual("code", session.ResponseType);
            Assert.AreEqual(LoginResult.Unlogin, session.LoginResult);
            Assert.AreEqual("/oauth2/test", session.ReturnUrl);
        }

        [TestMethod]
        public void AuthorizeEndpoint_ログインしていない場合_Cookieなし()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizeEndpointPath = new PathString("/oauth2/test")
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}}
                })));

            // ログインしていない
            mockIdentity.Setup(m => m.IsAuthenticated).Returns(false);
            endpointContext.Request.User = mockPrincipal.Object;

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // ログイン画面にリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("/User/LoginView", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);

            // セッションが作成されること
            var session =
                HttpContext.Current.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;

            Assert.IsNotNull(session);
            Assert.AreEqual("AAA", session.ClientId);
            Assert.AreEqual("https://localhost/test", session.RedirectUri);
            Assert.AreEqual("openid", session.Scope);
            Assert.AreEqual("code", session.ResponseType);
            Assert.AreEqual(LoginResult.Unlogin, session.LoginResult);
            Assert.AreEqual("/oauth2/test", session.ReturnUrl);
        }


        [TestMethod]
        public void AuthorizeEndpoint_ログインしていない場合_セッションあり()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizeEndpointPath = new PathString("/oauth2/test")
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}}
                })));

            // ログインしていない
            mockIdentity.Setup(m => m.IsAuthenticated).Returns(false);
            endpointContext.Request.User = mockPrincipal.Object;

            // 前回同じことをやった前提のセッションを作る
            var session = new AuthorizeEndpointSession
            {
                LoginResult = LoginResult.Unlogin
            };
            HttpContext.Current.Session.Add("AuthorizeEndpointSession", session);

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // ログイン画面にリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("/User/LoginView", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);

            // セッションが作成されること
            session = HttpContext.Current.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;

            Assert.IsNotNull(session);
            Assert.AreEqual("AAA", session.ClientId);
            Assert.AreEqual("https://localhost/test", session.RedirectUri);
            Assert.AreEqual("openid", session.Scope);
            Assert.AreEqual("code", session.ResponseType);
            Assert.AreEqual(LoginResult.Unlogin, session.LoginResult);
            Assert.AreEqual("/oauth2/test", session.ReturnUrl);
        }

        [TestMethod]
        public void AuthorizeEndpoint_サーバーエラーが発生した場合()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));

            // 適当なところで例外を発生させる
            mockIdentity.Setup(m => m.IsAuthenticated).Throws(new NullReferenceException());
            endpointContext.Request.User = mockPrincipal.Object;

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // エラーリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=server_error&state=aaaaa", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);

            mockIdentity.Verify(m => m.IsAuthenticated);
        }

        [TestMethod]
        public void AuthorizeEndpoint_ログインしているが認可されていない場合()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser
            {
                Id = "id1",
                UserName = "TestTaro"
            });
            users.Add(new CaradaIdUser
            {
                Id = "id2",
                UserName = "TestHanako"
            });

            var authorizedUsers = mockDbContext.Object.AuthorizedUsers;
            authorizedUsers.Add(new AuthorizedUsers
            {
                UserId = "id2",
                ClientId = "c1",
                Sub = "sub2"
            });

            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizationCodeExpireTimeSpan = TimeSpan.FromDays(1), // 1日後
                    AuthorizationCodeProvider = new DummyAuthorizationCodeProvider(),
                    AuthorizationCodeFormat = new DummyTicketDataFormat()
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"c1"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));

            // ログイン済み
            mockIdentity.Setup(m => m.IsAuthenticated).Returns(true);
            endpointContext.Request.User = mockPrincipal.Object;

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // unauthorized_clientになること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=unauthorized_client&state=aaaaa", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);

            mockIdentity.Verify(m => m.IsAuthenticated);
        }

        [TestMethod]
        public void AuthorizeEndpoint_ログインしているが認可されていない場合_clientIdのcaseが違う()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser
            {
                Id = "id1",
                UserName = "TestTaro"
            });
            users.Add(new CaradaIdUser
            {
                Id = "id2",
                UserName = "TestHanako"
            });

            var authorizedUsers = mockDbContext.Object.AuthorizedUsers;
            authorizedUsers.Add(new AuthorizedUsers
            {
                UserId = "id1",
                ClientId = "c1",
                Sub = "sub1"
            });

            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizationCodeExpireTimeSpan = TimeSpan.FromDays(1), // 1日後
                    AuthorizationCodeProvider = new DummyAuthorizationCodeProvider(),
                    AuthorizationCodeFormat = new DummyTicketDataFormat()
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"C1"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));

            // ログイン済み
            mockIdentity.Setup(m => m.IsAuthenticated).Returns(true);
            endpointContext.Request.User = mockPrincipal.Object;

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // unauthorized_clientになること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=unauthorized_client&state=aaaaa", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);

            mockIdentity.Verify(m => m.IsAuthenticated);
        }

        [TestMethod]
        public void AuthorizeEndpoint_ログイン済みで認可済みの場合()
        {
            var users = mockDbContext.Object.Users;
            users.Add(new CaradaIdUser
            {
                Id = "id1",
                UserName = "TestTaro"
            });
            users.Add(new CaradaIdUser
            {
                Id = "id2",
                UserName = "TestHanako"
            });

            var authorizedUsers = mockDbContext.Object.AuthorizedUsers;
            authorizedUsers.Add(new AuthorizedUsers
            {
                UserId = "id1",
                ClientId = "c1",
                Sub = "sub1"
            });

            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions()
                {
                    AuthorizationCodeExpireTimeSpan = TimeSpan.FromDays(1), // 1日後
                    AuthorizationCodeProvider = new DummyAuthorizationCodeProvider(),
                    AuthorizationCodeFormat = new DummyTicketDataFormat()
                },
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"c1"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));

            // ログイン済み
            mockIdentity.Setup(m => m.IsAuthenticated).Returns(true);
            endpointContext.Request.User = mockPrincipal.Object;

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // 正常リダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?code=12345&state=aaaaa", endpointContext.Response.Headers["Location"]);

            mockIdentity.VerifyAll();
        }

        [TestMethod]
        public void AuthorizeEndpoint_画面側でエラーの場合()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}},
                        {"state", new string[]{"aaaaa"}}
                })));

            HttpContext.Current.Session.Add("AuthorizeEndpointSession", new AuthorizeEndpointSession()
            {
                LoginResult = LoginResult.Error
            });

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // エラーリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=server_error&state=aaaaa", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);
        }

        [TestMethod]
        public void AuthorizeEndpoint_想定外の場合()
        {
            var endpointContext = new OAuthAuthorizeEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new AuthorizeEndpointRequest(new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"AAA"}},
                        {"redirect_uri", new string[]{"https://localhost/test"}},
                        {"scope", new string[]{"openid"}},
                        {"response_type", new string[]{"code"}}
                })));

            // ログインしていないにもかかわらずログイン結果=Successのセッションが入っている場合
            HttpContext.Current.Session.Add("AuthorizeEndpointSession", new AuthorizeEndpointSession()
            {
                LoginResult = LoginResult.Success
            });

            endpointContext.Request.Set<HttpContextBase>(
                typeof(HttpContextBase).FullName, new HttpContextWrapper(HttpContext.Current));

            target.AuthorizeEndpoint(endpointContext).Wait();

            // 例外となってエラーリダイレクトされること
            Assert.IsTrue(endpointContext.IsRequestCompleted);
            Assert.AreEqual(302, endpointContext.Response.StatusCode);
            Assert.AreEqual("https://localhost/test?error=server_error", endpointContext.Response.Headers["Location"]);
            Assert.AreEqual("application/x-www-form-urlencoded; charset=utf-8", endpointContext.Response.ContentType);
        }

        [TestMethod]
        public void ValidateClientAuthentication_clientIdが設定されていない場合()
        {
            var context = new OAuthValidateClientAuthenticationContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_secret", new string[]{"sss"}}
                    }));

            target.ValidateClientAuthentication(context).Wait();

            Assert.IsFalse(context.IsValidated);
        }

        [TestMethod]
        public void ValidateClientAuthentication_clientSecretが設定されていない場合()
        {
            var context = new OAuthValidateClientAuthenticationContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"aaa"}}
                    }));

            target.ValidateClientAuthentication(context).Wait();

            Assert.IsFalse(context.IsValidated);
        }

        [TestMethod]
        public void ValidateClientAuthentication_組み合わせ不正()
        {
            var mockClientMasters = mockDbContext.Object.ClientMasters;
            mockClientMasters.Add(new ClientMasters
            {
                ClientId = "aaa",
                ClientSecret = "sss"
            });

            var context = new OAuthValidateClientAuthenticationContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"xxx"}},
                        {"client_secret", new string[]{"bbb"}}
                    }));

            target.ValidateClientAuthentication(context).Wait();

            Assert.IsFalse(context.IsValidated);
        }

        [TestMethod]
        public void ValidateClientAuthentication_clienIdのcaseが違う()
        {
            var mockClientMasters = mockDbContext.Object.ClientMasters;
            mockClientMasters.Add(new ClientMasters
            {
                ClientId = "AAA",
                ClientSecret = "sss"
            });

            var context = new OAuthValidateClientAuthenticationContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"aaa"}},
                        {"client_secret", new string[]{"sss"}}
                    }));

            target.ValidateClientAuthentication(context).Wait();

            Assert.IsFalse(context.IsValidated);
        }

        [TestMethod]
        public void ValidateClientAuthentication_clienSecretのcaseが違う()
        {
            var mockClientMasters = mockDbContext.Object.ClientMasters;
            mockClientMasters.Add(new ClientMasters
            {
                ClientId = "aaa",
                ClientSecret = "SSS"
            });

            var context = new OAuthValidateClientAuthenticationContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"aaa"}},
                        {"client_secret", new string[]{"sss"}}
                    }));

            target.ValidateClientAuthentication(context).Wait();

            Assert.IsFalse(context.IsValidated);
        }

        [TestMethod]
        public void ValidateClientAuthentication_正常系()
        {
            var mockClientMasters = mockDbContext.Object.ClientMasters;
            mockClientMasters.Add(new ClientMasters
            {
                ClientId = "aaa",
                ClientSecret = "sss"
            });

            var context = new OAuthValidateClientAuthenticationContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"client_id", new string[]{"aaa"}},
                        {"client_secret", new string[]{"sss"}}
                    }));

            target.ValidateClientAuthentication(context).Wait();

            Assert.IsTrue(context.IsValidated);
        }

        [TestMethod]
        public void GetTokenRequestParametersString_正常系()
        {
            var context = new OAuthValidateClientAuthenticationContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new ReadableStringCollection(
                    new Dictionary<string, string[]>() {
                        {"aaa", new string[]{"test"}},
                        {"bbb", new string[]{"test1","test2"}}
                    }));

            var type = target.GetType();

            var methodInfo = type.GetMethod("GetTokenRequestParametersString", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.AreEqual("aaa=\"test\", bbb=[\"test1\", \"test2\"]", methodInfo.Invoke(target, new object[] { context }) as string);
        }

        [TestMethod]
        public void ValidateTokenRequest_MaintenanceModeが1()
        {
            var context = new OAuthValidateTokenRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new TokenEndpointRequest(
                    new ReadableStringCollection(
                        new Dictionary<string, string[]>() {
                            {"grant_type", new string[]{"authorization_code"}}
                        })),
                null);

            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenanceMode"].Value = "1";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            target.ValidateTokenRequest(context).Wait();

            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("temporarily_unavailable", context.Error);
        }

        [TestMethod]
        public void ValidateTokenRequest_MaintenanceModeが2()
        {
            var context = new OAuthValidateTokenRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new TokenEndpointRequest(
                    new ReadableStringCollection(
                        new Dictionary<string, string[]>() {
                            {"grant_type", new string[]{"authorization_code"}}
                        })),
                null);

            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenanceMode"].Value = "2";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            target.ValidateTokenRequest(context).Wait();

            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("temporarily_unavailable", context.Error);
        }

        [TestMethod]
        public void ValidateTokenRequest_grantTypeがサポート外()
        {
            var context = new OAuthValidateTokenRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new TokenEndpointRequest(
                    new ReadableStringCollection(
                        new Dictionary<string, string[]>() {
                            {"grant_type", new string[]{"password"}}
                        })),
                null);

            target.ValidateTokenRequest(context).Wait();

            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("unsupported_grant_type", context.Error);
        }

        [TestMethod]
        public void ValidateTokenRequest_正常系()
        {
            var context = new OAuthValidateTokenRequestContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new TokenEndpointRequest(
                    new ReadableStringCollection(
                        new Dictionary<string, string[]>() {
                            {"grant_type", new string[]{"authorization_code"}}
                        })),
                null);

            target.ValidateTokenRequest(context).Wait();

            Assert.IsTrue(context.IsValidated);
        }

        [TestMethod]
        public void GrantAuthorizationCode_認可していないユーザーの場合()
        {
            var context = new OAuthGrantAuthorizationCodeContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new AuthenticationTicket(
                    new GenericIdentity(mockIdentity.Name),
                    new AuthenticationProperties()));

            target.GrantAuthorizationCode(context).Wait();

            Assert.IsFalse(context.IsValidated);
            Assert.AreEqual("unauthorized_client", context.Error);
        }

        [TestMethod]
        public void GrantAuthorizationCode_正常系()
        {
            var context = new OAuthGrantAuthorizationCodeContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                new AuthenticationTicket(
                    new GenericIdentity(mockIdentity.Name),
                    new AuthenticationProperties(
                        new Dictionary<string, string>()
                        {
                            {"sub", "sub1"}
                        })));

            target.GrantAuthorizationCode(context).Wait();

            Assert.IsTrue(context.IsValidated);
        }

        [TestMethod]
        public void TokenEndpoint_正常系()
        {
            var context = new OAuthTokenEndpointContext(
                mockOwinContext.Object,
                new OAuthAuthorizationServerOptions(),
                                new AuthenticationTicket(
                    new GenericIdentity(mockIdentity.Name),
                    new AuthenticationProperties(
                        new Dictionary<string, string>()
                        {
                            {"sub", "sub1"}
                        })),
                new TokenEndpointRequest(
                    new ReadableStringCollection(
                        new Dictionary<string, string[]>() {
                            {"client_id", new string[]{"aaa"}}
                        })));

            target.TokenEndpoint(context).Wait();

            Assert.IsTrue(context.IsRequestCompleted);
            // jwtの確認はしない
            Assert.IsNotNull(context.AdditionalResponseParameters["id_token"]);
        }
    }
}
