﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using System;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.Tests.IdentityManagers
{
    /// <summary>
    /// ApplicationUserManagerのテストクラス
    /// </summary>
    [TestClass]
    public class ApplicationUserManagerTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private ApplicationUserManager target;

        private IdentityFactoryOptions<ApplicationUserManager> options;
        private IOwinContext context;


        [TestInitialize]
        public void Initialize()
        {
            options = new IdentityFactoryOptions<ApplicationUserManager>();
            context = new OwinContext();
            context.Set<CaradaIdPDbContext>(new CaradaIdPDbContext());
            target = ApplicationUserManager.Create(options, context);
        }

        [TestMethod]
        public void Create()
        {
            // 設定値の妥当性をUTで求めてもしょうがないので、カバレッジ目的のテストメソッドです。
            var options = new IdentityFactoryOptions<ApplicationUserManager>();

            var mockDataProtectionProvider = new Mock<IDataProtectionProvider>();
            mockDataProtectionProvider.Setup(s => s.Create("ASP.NET Identity")).Returns(new Mock<IDataProtector>().Object);

            options.DataProtectionProvider = mockDataProtectionProvider.Object;
            target = ApplicationUserManager.Create(options, context);
        }

        [TestMethod]
        public void IsInvalidVerifyCode_認証コード有効期限がnull()
        {
            var user = new CaradaIdUser()
            {
                AuthCodeExpireDateUtc = null,
                AuthCodeFailedCount = 0
            };
            Assert.IsTrue(target.IsInvalidVerifyCode(user));
        }

        [TestMethod]
        public void IsInvalidVerifyCode_認証コード有効期限切れ()
        {
            var user = new CaradaIdUser()
            {
                AuthCodeExpireDateUtc = DateTime.UtcNow.AddMilliseconds(-1),
                AuthCodeFailedCount = 0
            };
            Assert.IsTrue(target.IsInvalidVerifyCode(user));
        }

        [TestMethod]
        public void IsInvalidVerifyCode_認証コード失敗回数MAX()
        {
            var user = new CaradaIdUser()
            {
                AuthCodeExpireDateUtc = DateTime.UtcNow.AddSeconds(1),
                AuthCodeFailedCount = 5
            };
            Assert.IsTrue(target.IsInvalidVerifyCode(user));
        }

        [TestMethod]
        public void IsInvalidVerifyCode_認証コード有効()
        {
            var user = new CaradaIdUser()
            {
                AuthCodeExpireDateUtc = DateTime.UtcNow.AddSeconds(1),
                AuthCodeFailedCount = 4
            };
            Assert.IsFalse(target.IsInvalidVerifyCode(user));
        }

        [TestMethod]
        public void IsVerifyCodeInvalidNow_認証コード失敗回数MAXになる()
        {
            var user = new CaradaIdUser()
            {
                AuthCodeExpireDateUtc = DateTime.UtcNow.AddMinutes(1),
                AuthCodeFailedCount = 4
            };
            Assert.IsTrue(target.IsVerifyCodeInvalidNow(user).Result);
        }

        [TestMethod]
        public void IsVerifyCodeInvalidNow_認証コード失敗回数MAXにならない()
        {
            var user = new CaradaIdUser()
            {
                AuthCodeExpireDateUtc = DateTime.UtcNow.AddMinutes(1),
                AuthCodeFailedCount = 3
            };
            Assert.IsFalse(target.IsVerifyCodeInvalidNow(user).Result);
        }

        [TestMethod]
        public void ValidatePassword_ロックアウト中()
        {
            var user = new CaradaIdUser()
            {
                Id = "id"
            };
            var password = "pass";
            var mockTarget = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object) { CallBase = true };
            mockTarget.Setup(s => s.IsLockedOutAsync("id")).ReturnsAsync(true);

            var result = mockTarget.Object.ValidatePassword(user, password).Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(PasswordValidationStatus.AlreadyLockedOut, result);

            mockTarget.VerifyAll();
        }

        [TestMethod]
        public void ValidatePassword_現在のパスワード不一致だがロックアウトはされない()
        {
            var user = new CaradaIdUser()
            {
                Id = "id"
            };
            var password = "pass";
            var mockTarget = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object) { CallBase = true };
            mockTarget.Setup(s => s.IsLockedOutAsync("id")).ReturnsAsync(false);
            mockTarget.Setup(s => s.CheckPasswordAsync(user, password)).ReturnsAsync(false);
            mockTarget.Setup(s => s.AccessFailedAsync("id")).ReturnsAsync(IdentityResult.Success);

            var result = mockTarget.Object.ValidatePassword(user, password).Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(PasswordValidationStatus.Failure, result);

            mockTarget.VerifyAll();
        }

        [TestMethod]
        public void ValidatePassword_現在のパスワード不一致でロックアウト開始()
        {
            var user = new CaradaIdUser()
            {
                Id = "id"
            };
            var password = "passs";
            var mockTarget = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object) { CallBase = true };
            mockTarget.SetupSequence(s => s.IsLockedOutAsync("id"))
                .Returns(Task.FromResult<bool>(false)).Returns(Task.FromResult<bool>(true));
            mockTarget.Setup(s => s.CheckPasswordAsync(user, It.IsNotIn<string>("password"))).ReturnsAsync(false);
            mockTarget.Setup(s => s.AccessFailedAsync("id")).ReturnsAsync(IdentityResult.Success);

            var result = mockTarget.Object.ValidatePassword(user, password).Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(PasswordValidationStatus.LockedOut, result);

            mockTarget.VerifyAll();
        }

        [TestMethod]
        public void ValidatePassword_検証結果正常()
        {
            var user = new CaradaIdUser()
            {
                Id = "id"
            };
            var password = "pass";
            var mockTarget = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object) { CallBase = true };
            mockTarget.Setup(s => s.IsLockedOutAsync("id")).ReturnsAsync(false);
            mockTarget.Setup(s => s.CheckPasswordAsync(user, password)).ReturnsAsync(true);

            var result = mockTarget.Object.ValidatePassword(user, password).Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(PasswordValidationStatus.Success, result);

            mockTarget.VerifyAll();
        }
    }
}
