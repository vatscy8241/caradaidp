﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.Tests.IdentityConfigs
{
    [TestClass]
    public class ApplicationUserStoreTest
    {
        private TestApplicationUserStore target;

        /// <summary>
        /// Test用にGetUserAggregateAsyncを実装したUserStore。
        /// </summary>
        private class TestApplicationUserStore : ApplicationUserStore<CaradaIdUser>
        {
            public TestApplicationUserStore(DbContext context) : base(context) { }

            protected override async Task<CaradaIdUser> GetUserAggregateAsync(Expression<Func<CaradaIdUser, bool>> filter)
            {
                return await TestUsers.FirstOrDefaultAsync(filter);
            }

            public IDbSet<CaradaIdUser> TestUsers { get; set; }
        }

        /// <summary>
        /// SQLServerを用いておらず、大文字小文字の判定を行ってしまうため作成したUserStore。カバレッジ用。
        /// </summary>
        private class TestApplicationUserStore2 : ApplicationUserStore<CaradaIdUser>
        {
            public TestApplicationUserStore2(DbContext context) : base(context) { }

            protected override async Task<CaradaIdUser> GetUserAggregateAsync(Expression<Func<CaradaIdUser, bool>> filter)
            {
                return await TestUsers.FirstOrDefaultAsync();
            }

            public IDbSet<CaradaIdUser> TestUsers { get; set; }
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new TestApplicationUserStore(new Mock<CaradaIdPDbContext>().Object);
        }

        [TestMethod]
        public void FindByNameAsync_正常系_検索されること()
        {
            var users = new TestAspNetUsersDbSet();

            users.Add(new CaradaIdUser
            {
                Id = "id1",
                UserName = "a"
            });
            users.Add(new CaradaIdUser
            {
                Id = "id2",
                UserName = "b"
            });

            target.TestUsers = users;

            var user = target.FindByNameAsync("b").Result;

            Assert.AreEqual<string>("id2", user.Id);
            Assert.AreEqual<string>("b", user.UserName);
        }

        [TestMethod]
        public void FindByNameAsync_正常系_対象なし()
        {
            var users = new TestAspNetUsersDbSet();

            users.Add(new CaradaIdUser
            {
                Id = "id1",
                UserName = "a"
            });
            users.Add(new CaradaIdUser
            {
                Id = "id2",
                UserName = "b"
            });

            target.TestUsers = users;

            var user = target.FindByNameAsync("c").Result;

            Assert.IsNull(user);
        }

        [TestMethod]
        public void FindByNameAsync_正常系_入力値に大文字を含む場合_対象無し()
        {
            var users = new TestAspNetUsersDbSet();

            users.Add(new CaradaIdUser
            {
                Id = "id1",
                UserName = "test1234"
            });

            var target2 = new TestApplicationUserStore2(new Mock<CaradaIdPDbContext>().Object);

            target2.TestUsers = users;

            var user = target2.FindByNameAsync("Test1234").Result;
            Assert.IsNull(user);

            user = target2.FindByNameAsync("tEst1234").Result;
            Assert.IsNull(user);

            user = target2.FindByNameAsync("teSt1234").Result;
            Assert.IsNull(user);

            user = target2.FindByNameAsync("tesT1234").Result;
            Assert.IsNull(user);

            user = target2.FindByNameAsync("TEST1234").Result;
            Assert.IsNull(user);

            user = target2.FindByNameAsync("test1234").Result;
            Assert.IsNotNull(user);
        }

        [TestMethod]
        public void FindByEmailAsync_正常系_検索されること()
        {
            var users = new TestAspNetUsersDbSet();

            users.Add(new CaradaIdUser
            {
                Id = "id1",
                Email = "a"
            });
            users.Add(new CaradaIdUser
            {
                Id = "id2",
                Email = "b"
            });

            target.TestUsers = users;

            var user = target.FindByEmailAsync("b").Result;

            Assert.AreEqual<string>("id2", user.Id);
            Assert.AreEqual<string>("b", user.Email);
        }

        [TestMethod]
        public void FindByEmailAsync_正常系_対象なし()
        {
            var users = new TestAspNetUsersDbSet();

            users.Add(new CaradaIdUser
            {
                Id = "id1",
                Email = "a"
            });
            users.Add(new CaradaIdUser
            {
                Id = "id2",
                Email = "b"
            });

            target.TestUsers = users;

            var user = target.FindByEmailAsync("c").Result;

            Assert.IsNull(user);
        }
    }
}
