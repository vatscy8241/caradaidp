﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage.Table;
using Moq;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Models.AuthorizationStorageEntities;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.IdentityManagers
{
    /// <summary>
    /// ApplicationSignInManagerのテストクラス
    /// </summary>
    [TestClass]
    public class ApplicationSignInManagerTest
    {
        private ApplicationSignInManager target;

        private Mock<ApplicationUserManager> mockUserManager;

        private Mock<IAuthenticationManager> mockAuthenticationManager;

        /// <summary>
        /// Azure Storageの対象テーブル
        /// </summary>
        private CloudTable TwoFactorLogin;

        [TestInitialize]
        public void Initialize()
        {
            MockHttpContextHelper.SetCurrentOfFakeContext();
            mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockAuthenticationManager = new Mock<IAuthenticationManager>();
            target = new ApplicationSignInManager(mockUserManager.Object, mockAuthenticationManager.Object);

            // Azure Storageエミュレータの対象テーブルを初期化
            TwoFactorLogin = AuthorizationStorageContext.TwoFactorLogin;
            TwoFactorLogin.DeleteIfExists();
            TwoFactorLogin.Create();
        }

        [TestMethod]
        public void CreateUserIdentityAsync_生成されること()
        {
            Assert.IsNotNull(target.CreateUserIdentityAsync(new CaradaIdUser()));
        }

        [TestMethod]
        public void Create()
        {
            var context = new OwinContext();
            context.Set<ApplicationUserManager>(mockUserManager.Object);
            ApplicationSignInManager.Create(null, context);
        }

        [TestMethod]
        public void SignInAsync_ユーザーがいない場合()
        {
            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(null);

            var result = target.SignInAsync("caradaid", "password", false, false);

            Assert.AreEqual(result.Result, SignInStatusEx.Failure);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_利用開始前ユーザーの場合()
        {
            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = false });

            var result = target.SignInAsync("caradaid", "password", false, false);

            Assert.AreEqual(result.Result, SignInStatusEx.UseStarting);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_ロックアウト中の場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass")).ReturnsAsync(PasswordValidationStatus.AlreadyLockedOut);

            var result = target.SignInAsync("caradaid", "pass", false, false);
            Assert.AreEqual(result.Result, SignInStatusEx.LockedOut);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_パスワードを間違えている場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass")).ReturnsAsync(PasswordValidationStatus.Failure);

            var result = target.SignInAsync("caradaid", "pass", false, false);
            Assert.AreEqual(result.Result, SignInStatusEx.Failure);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_パスワードを間違い回数がロックアウト判定に到達した場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass")).ReturnsAsync(PasswordValidationStatus.LockedOut);

            var result = target.SignInAsync("caradaid", "pass", false, false);
            Assert.AreEqual(result.Result, SignInStatusEx.LockedOut);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_ユーザーが２要素認証をサポートしない場合()
        {
            // 通常この状況はない
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.GetTwoFactorEnabledAsync("id")).ReturnsAsync(false);
            mockUserManager.Setup(s => s.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie)).ReturnsAsync(new ClaimsIdentity());

            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            var result = target.SignInAsync("caradaid", "password", false, true);
            Assert.AreEqual(result.Result, SignInStatusEx.Success);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_メールアドレスが未登録の場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = false
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Success);

            var result = target.SignInAsync("caradaid", "password", false, true);
            Assert.AreEqual(result.Result, SignInStatusEx.UseStarting);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_二要素認証済みCookieがない場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.GetTwoFactorEnabledAsync("id")).ReturnsAsync(true);

            var result = target.SignInAsync("caradaid", "password", false, true);
            Assert.AreEqual(result.Result, SignInStatusEx.RequiresVerification);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_二要素認証済みCookieはあるがストレージに登録がない場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.GetTwoFactorEnabledAsync("id")).ReturnsAsync(true);

            var cookie = new HttpCookie("TwoFactorLogin", "aaa");
            HttpContext.Current.Request.Cookies.Add(cookie);

            var result = target.SignInAsync("caradaid", "password", false, true);
            Assert.AreEqual(result.Result, SignInStatusEx.RequiresVerification);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_二要素認証済みCookieはあるがストレージに違うユーザーIDが登録されている場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id001",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.GetTwoFactorEnabledAsync("id001")).ReturnsAsync(true);

            var cookie = new HttpCookie("TwoFactorLogin", "aaa");
            HttpContext.Current.Request.Cookies.Add(cookie);

            // Azure StorageにInsert
            var insertEntity = new TwoFactorLoginEntity()
            {
                PartitionKey = "aaa",
                RowKey = "-",
                UserId = "id002"

            };
            TwoFactorLogin.InsertEntity(insertEntity);

            var result = target.SignInAsync("caradaid", "password", false, true);
            Assert.AreEqual(result.Result, SignInStatusEx.RequiresVerification);
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignInAsync_正常系_ログイン完了()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync("caradaid")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.GetTwoFactorEnabledAsync("id")).ReturnsAsync(true);

            //mockUserManager.Setup(s => s.IsEmailConfirmedAsync("id")).ReturnsAsync(true);
            mockUserManager.Setup(s => s.ResetAccessFailedCountAsync("id")).ReturnsAsync(IdentityResult.Success);

            var cookie = new HttpCookie("TwoFactorLogin", "aaa");
            HttpContext.Current.Request.Cookies.Add(cookie);

            // Azure StorageにInsert
            var insertEntity = new TwoFactorLoginEntity()
            {
                PartitionKey = "aaa",
                RowKey = "-",
                UserId = "id"

            };
            TwoFactorLogin.InsertEntity(insertEntity);

            var userIdentity = new ClaimsIdentity();

            mockUserManager.Setup(s => s.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie)).ReturnsAsync(userIdentity);

            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            var result = target.SignInAsync("caradaid", "password", false, true);
            Assert.AreEqual(result.Result, SignInStatusEx.Success);
            Assert.AreSame(HttpContext.Current.User.Identity, userIdentity);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SignOut_正常系_UserIdなし()
        {
            mockAuthenticationManager.Setup(a => a.SignOut(DefaultAuthenticationTypes.ApplicationCookie));
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            var result = target.SignOut();
            mockAuthenticationManager.VerifyAll();
        }

        [TestMethod]
        public void SignOut_正常系_UserIdあり()
        {
            mockAuthenticationManager.Setup(a => a.SignOut(DefaultAuthenticationTypes.ApplicationCookie));
            mockUserManager.Setup(u => u.UpdateSecurityStampAsync("testuserid")).ReturnsAsync(IdentityResult.Success);

            var claims = new List<Claim>();
            claims.Add(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "testuserid"));
            HttpContext.Current.User = new GenericPrincipal(new ClaimsIdentity(claims), null);

            var result = target.SignOut();
            mockAuthenticationManager.VerifyAll();
            mockUserManager.VerifyAll();
        }

    }
}
