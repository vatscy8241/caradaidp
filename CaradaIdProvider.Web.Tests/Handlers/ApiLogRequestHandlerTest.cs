﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Handlers;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.Tests.Handlers
{
    /// <summary>
    /// ApiLogRequestHandlerのテストクラス
    /// </summary>
    [TestClass]
    public class ApiLogRequestHandlerTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private ApiLogRequestHandler target;

        private Mock<ILogger> logger;


        /// <summary>
        /// テスト対象が持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<ApiLogRequestHandler>(name);
            field.SetValue(target, o);
        }

        [TestInitialize]
        public void Initialize()
        {
            logger = new Mock<ILogger>();
        }

        [TestMethod]
        public void SendAsync_正常系_GETでパラメータ空文字()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, "http://test.com");
            httpRequestMessage.Content = new StringContent("");
            var responseContent = new StringContent(@"{'aaa':'bbb', 'ccc':'ddd'}");

            target = new ApiLogRequestHandler()
            {
                InnerHandler = new TestHandler((r, c) =>
                {
                    Assert.AreEqual("", r.Content.ReadAsStringAsync().Result);
                    return TestHandler.Return200(responseContent);
                })
            };

            var expected1 = "[REQUEST]";
            var expected2 = "[RESPONSE] parameters:[aaa=\"bbb\", ccc=\"ddd\"]";
            logger.Setup(r => r.Info(expected1));
            logger.Setup(r => r.Info(expected2));
            SetField("AUDIT_LOG", logger.Object);

            var client = new HttpClient(target);
            var result = client.SendAsync(httpRequestMessage).Result;
            logger.VerifyAll();
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

        }

        [TestMethod]
        public void SendAsync_正常系_GETでパラメータ非JSON()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, "http://test.com");
            httpRequestMessage.Content = new StringContent("username=test");
            var responseContent = new StringContent(@"{'aaa':'bbb', 'ccc':'ddd'}");

            target = new ApiLogRequestHandler()
            {
                InnerHandler = new TestHandler((r, c) =>
                {
                    Assert.AreEqual("username=test", r.Content.ReadAsStringAsync().Result);
                    return TestHandler.Return200(responseContent);
                })
            };

            var expected1 = "[REQUEST] parameters:[username=test]";
            var expected2 = "[RESPONSE] parameters:[aaa=\"bbb\", ccc=\"ddd\"]";
            logger.Setup(r => r.Info(expected1));
            logger.Setup(r => r.Info(expected2));
            SetField("AUDIT_LOG", logger.Object);

            var client = new HttpClient(target);
            var result = client.SendAsync(httpRequestMessage).Result;
            logger.VerifyAll();
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }


        [TestMethod]
        public void SendAsync_正常系_GETでパラメータがクエリー()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, "http://test.com");
            var parameters = new Dictionary<string, string>();
            parameters.Add("username", "test");
            httpRequestMessage.Content = new FormUrlEncodedContent(parameters);
            var responseContent = new StringContent(@"{'aaa':'bbb', 'ccc':'ddd'}");

            target = new ApiLogRequestHandler()
            {
                InnerHandler = new TestHandler((r, c) =>
                {
                    Assert.AreEqual("username=test", r.Content.ReadAsStringAsync().Result);
                    return TestHandler.Return200(responseContent);
                })
            };

            var expected1 = "[REQUEST] parameters:[username=test]";
            var expected2 = "[RESPONSE] parameters:[aaa=\"bbb\", ccc=\"ddd\"]";
            logger.Setup(r => r.Info(expected1));
            logger.Setup(r => r.Info(expected2));
            SetField("AUDIT_LOG", logger.Object);

            var client = new HttpClient(target);
            var result = client.SendAsync(httpRequestMessage).Result;
            logger.VerifyAll();
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestMethod]
        public void SendAsync_正常系_GETでパラメータがJSON()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, "http://test.com");
            httpRequestMessage.Content = new StringContent("{\"username\":\"test\"}");
            var responseContent = new StringContent(@"{'aaa':'bbb', 'ccc':'ddd'}");

            target = new ApiLogRequestHandler()
            {
                InnerHandler = new TestHandler((r, c) =>
                {
                    Assert.AreEqual("{\"username\":\"test\"}", r.Content.ReadAsStringAsync().Result);
                    return TestHandler.Return200(responseContent);
                })
            };

            var expected1 = "[REQUEST] parameters:[username=\"test\"]";
            var expected2 = "[RESPONSE] parameters:[aaa=\"bbb\", ccc=\"ddd\"]";
            logger.Setup(r => r.Info(expected1));
            logger.Setup(r => r.Info(expected2));

            SetField("AUDIT_LOG", logger.Object);

            var client = new HttpClient(target);
            var result = client.SendAsync(httpRequestMessage).Result;
            logger.VerifyAll();
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        // POSTでも処理は変わらないが、念のためGETと同じ内容で1ケースのみ確認する
        [TestMethod]
        public void SendAsync_正常系_POSTでパラメータがJSON()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, "http://test.com");
            httpRequestMessage.Content = new StringContent("{\"username\":\"test\"}");
            var responseContent = new StringContent(@"{'aaa':'bbb', 'ccc':'ddd'}");

            target = new ApiLogRequestHandler()
            {
                InnerHandler = new TestHandler((r, c) =>
                {
                    Assert.AreEqual("{\"username\":\"test\"}", r.Content.ReadAsStringAsync().Result);
                    return TestHandler.Return200(responseContent);
                })
            };

            var expected1 = "[REQUEST] parameters:[username=\"test\"]";
            var expected2 = "[RESPONSE] parameters:[aaa=\"bbb\", ccc=\"ddd\"]";
            logger.Setup(r => r.Info(expected1));
            logger.Setup(r => r.Info(expected2));
            SetField("AUDIT_LOG", logger.Object);

            var client = new HttpClient(target);
            var result = client.SendAsync(httpRequestMessage).Result;
            logger.VerifyAll();
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }
    }

    public class TestHandler : DelegatingHandler
    {
        private readonly Func<HttpRequestMessage,
            CancellationToken, Task<HttpResponseMessage>> _handlerFunc;

        public TestHandler()
        {
            _handlerFunc = (r, c) => Return200(r.Content);
        }

        public TestHandler(Func<HttpRequestMessage,
            CancellationToken, Task<HttpResponseMessage>> handlerFunc)
        {
            _handlerFunc = handlerFunc;
        }

        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return _handlerFunc(request, cancellationToken);
        }

        public static Task<HttpResponseMessage> Return200(HttpContent content)
        {
            var responseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            responseMessage.Content = content;
            return Task.Factory.StartNew(
                () => responseMessage);
        }
    }
}
