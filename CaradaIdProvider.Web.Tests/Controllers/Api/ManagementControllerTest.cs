﻿using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Controllers.Api;
using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api
{
    /// <summary>
    /// 利用開始前ユーザー登録APIのテストクラス
    /// </summary>
    [TestClass]
    public class ManagementControllerTest
    {

        private ManagementController target = new ManagementController();
        private Mock<CaradaIdPDbContext> mockDbContext;
        private Mock<ApplicationUserManager> mockUserManager;
        private Mock<JwtApiRequest> mockJwtApiRequest;

        private readonly HttpRequestMessage httpRequestMessage = new HttpRequestMessage()
        {
            Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
        };

        [TestInitialize]
        public void TestInitialize()
        {
            target = new ManagementController() { Request = httpRequestMessage };
            mockDbContext = new Mock<CaradaIdPDbContext>();
            MockHttpContextHelper.SetCurrentOfFakeContext();
            mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockJwtApiRequest = new Mock<JwtApiRequest>(null, null, null);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            mockUserManager.VerifyAll();
            mockDbContext.VerifyAll();
        }

        /// <summary>
        /// テスト対象が持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<ManagementController>(name);
            field.SetValue(target, o);
        }

        /// <summary>
        /// BaseWebControllerが持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetBaseField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<BaseApiController>(name);
            field.SetValue(target, o);
        }


        [TestMethod]
        public void UserAdd_正常系_正常にユーザー登録が完了する()
        {
            var parameter = new RegistUserAddCondition
            {
                ClientId = "91111ClientTest",
                CaradaId = "testUser"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<RegistUserAddCondition>();
            ReflectionAccessor.GetProperty<JwtBase<RegistUserAddCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.SetupSequence(s => s.FindByNameAsync("testUser")).Returns(Task.FromResult<CaradaIdUser>(null)).Returns(Task.FromResult(newUser));
            mockUserManager.Setup(s => s.CreateAsync(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.AddToRoleAsync(newUser.Id, "User")).ReturnsAsync(IdentityResult.Success);
            SetBaseField("_userManager", mockUserManager.Object);

            var clientMastersMock = new TestClientMastersDbSet();
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "36310e37439547ceb9a138336776c3a2",
                ClientName = "AuthProvider",
                ClientSecret = "test_client_secret",
            });
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "047535b4582d4f87a71c27262f27a258",
                ClientName = "mopita",
                ClientSecret = "test_client_secret",
            });
            mockDbContext.Setup(s => s.ClientMasters).Returns(clientMastersMock);
            mockDbContext.Setup(s => s.AuthorizedUsers).Returns(new TestAuthorizedUsersDbSet());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateSubjectId()).Returns("ID1");
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            mockJwtApiRequest.Setup(s => s.PostJson(It.IsAny<Dictionary<string, object>>())).Returns(new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("{ 'uid' : 'testUid', 'access_token' : 'testToken', 'token_type' : 'bearer' }") });
            SetField("jwtRequest", mockJwtApiRequest.Object);

            var task = target.AddUser(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _uid = (string)resultJtoken["uid"];
            var _caradaId = (string)resultJtoken["carada_id"];
            var _password = (string)resultJtoken["password"];
            Assert.AreEqual("testUid", _uid);
            Assert.AreEqual(_caradaId, "testUser");
            Assert.AreEqual("Pass1234", _password);

            mockIdGenerator.VerifyAll();
            mockJwtApiRequest.VerifyAll();
        }

        [TestMethod]
        public void UserAdd_異常系_指定したCARADA_IDのユーザー情報が存在している場合()
        {
            var parameter = new RegistUserAddCondition
            {
                ClientId = "91111ClientTest",
                CaradaId = "testUser"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<RegistUserAddCondition>();
            ReflectionAccessor.GetProperty<JwtBase<RegistUserAddCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(newUser);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.AddUser(jwt);
            var result = task.Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _error = (string)resultJtoken["error"];
            var _errorDescription = (string)resultJtoken["error_description"];
            Assert.AreEqual(_caradaId, "testUser");
            Assert.AreEqual("registered_carada_id", _error);
            Assert.AreEqual("登録済みのCARADA IDです", _errorDescription);
        }

        [TestMethod]
        public void UserAdd_異常系_統合認可のユーザー登録APIの実行に失敗した場合_妥当なJSON返却時()
        {
            var parameter = new RegistUserAddCondition
            {
                ClientId = "91111ClientTest",
                CaradaId = "testUser"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<RegistUserAddCondition>();
            ReflectionAccessor.GetProperty<JwtBase<RegistUserAddCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.SetupSequence(s => s.FindByNameAsync("testUser")).Returns(Task.FromResult<CaradaIdUser>(null)).Returns(Task.FromResult(newUser));
            mockUserManager.Setup(s => s.CreateAsync(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.AddToRoleAsync(newUser.Id, "User")).ReturnsAsync(IdentityResult.Success);
            SetBaseField("_userManager", mockUserManager.Object);

            var clientMastersMock = new TestClientMastersDbSet();
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "36310e37439547ceb9a138336776c3a2",
                ClientName = "AuthProvider",
                ClientSecret = "test_client_secret",
            });
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "047535b4582d4f87a71c27262f27a258",
                ClientName = "mopita",
                ClientSecret = "test_client_secret",
            });
            mockDbContext.Setup(s => s.ClientMasters).Returns(clientMastersMock);
            mockDbContext.Setup(s => s.AuthorizedUsers).Returns(new TestAuthorizedUsersDbSet());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateSubjectId()).Returns("ID1");
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            mockJwtApiRequest.Setup(s => s.PostJson(It.IsAny<Dictionary<string, object>>()))
                .Returns(new HttpResponseMessage() { StatusCode = HttpStatusCode.BadRequest, Content = new StringContent("{ 'error' : 'auth_platform_error', 'error_description' : '[統合認可]連携プラットフォームとの連携に失敗しました。', 'original_error' : 'originar_error_code' }") });
            SetField("jwtRequest", mockJwtApiRequest.Object);

            var task = target.AddUser(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _error = (string)resultJtoken["error"];
            var _errorDescription = (string)resultJtoken["error_description"];
            var _originalError = (string)resultJtoken["original_error"];
            Assert.AreEqual(parameter.CaradaId, _caradaId);
            Assert.AreEqual("auth_provider_error", _error);
            Assert.AreEqual("統合認可プロバイダのユーザー登録に失敗しました", _errorDescription);
            Assert.IsNotNull("auth_platform_error", _originalError);

            mockIdGenerator.VerifyAll();
            mockJwtApiRequest.VerifyAll();
        }

        [TestMethod]
        public void UserAdd_異常系_統合認可のユーザー登録APiの実行に失敗した場合_非JSON返却時()
        {
            var parameter = new RegistUserAddCondition
            {
                ClientId = "91111ClientTest",
                CaradaId = "testUser"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<RegistUserAddCondition>();
            ReflectionAccessor.GetProperty<JwtBase<RegistUserAddCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.SetupSequence(s => s.FindByNameAsync("testUser")).Returns(Task.FromResult<CaradaIdUser>(null)).Returns(Task.FromResult(newUser));
            mockUserManager.Setup(s => s.CreateAsync(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.AddToRoleAsync(newUser.Id, "User")).ReturnsAsync(IdentityResult.Success);
            SetBaseField("_userManager", mockUserManager.Object);

            var clientMastersMock = new TestClientMastersDbSet();
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "36310e37439547ceb9a138336776c3a2",
                ClientName = "AuthProvider",
                ClientSecret = "test_client_secret",
            });
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "047535b4582d4f87a71c27262f27a258",
                ClientName = "mopita",
                ClientSecret = "test_client_secret",
            });
            mockDbContext.Setup(s => s.ClientMasters).Returns(clientMastersMock);
            mockDbContext.Setup(s => s.AuthorizedUsers).Returns(new TestAuthorizedUsersDbSet());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateSubjectId()).Returns("ID1");
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            mockJwtApiRequest.Setup(s => s.PostJson(It.IsAny<Dictionary<string, object>>()))
                .Returns(new HttpResponseMessage() { StatusCode = HttpStatusCode.BadRequest, Content = new StringContent("@") });
            SetField("jwtRequest", mockJwtApiRequest.Object);

            var task = target.AddUser(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _error = (string)resultJtoken["error"];
            var _errorDescription = (string)resultJtoken["error_description"];
            var _originalError = (string)resultJtoken["original_error"];
            Assert.AreEqual(parameter.CaradaId, _caradaId);
            Assert.AreEqual("auth_provider_error", _error);
            Assert.AreEqual("統合認可プロバイダのユーザー登録に失敗しました", _errorDescription);
            Assert.IsNotNull("", _originalError);

            mockIdGenerator.VerifyAll();
            mockJwtApiRequest.VerifyAll();
        }

        [TestMethod]
        public void UserAdd_異常系_統合認可のユーザー登録APiの実行に失敗した場合_統合認可API情報取得失敗()
        {
            var parameter = new RegistUserAddCondition
            {
                ClientId = "91111ClientTest",
                CaradaId = "testUser"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<RegistUserAddCondition>();
            ReflectionAccessor.GetProperty<JwtBase<RegistUserAddCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.SetupSequence(s => s.FindByNameAsync("testUser")).Returns(Task.FromResult<CaradaIdUser>(null)).Returns(Task.FromResult(newUser));
            mockUserManager.Setup(s => s.CreateAsync(It.IsAny<CaradaIdUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.AddToRoleAsync(newUser.Id, "User")).ReturnsAsync(IdentityResult.Success);
            SetBaseField("_userManager", mockUserManager.Object);

            var clientMastersMock = new TestClientMastersDbSet();
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "36310e37439547ceb9a138336776c3a2",
                ClientName = "AuthProvider",
                ClientSecret = "test_client_secret",
            });
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "047535b4582d4f87a71c27262f27a258",
                ClientName = "mopita",
                ClientSecret = "test_client_secret",
            });
            mockDbContext.Setup(s => s.ClientMasters).Returns(clientMastersMock);
            mockDbContext.Setup(s => s.AuthorizedUsers).Returns(new TestAuthorizedUsersDbSet());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateSubjectId()).Returns("ID1");
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            mockJwtApiRequest.Setup(s => s.PostJson(It.IsAny<Dictionary<string, object>>()))
                .Throws(new Exception());

            SetField("jwtRequest", mockJwtApiRequest.Object);

            var task = target.AddUser(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _error = (string)resultJtoken["error"];
            var _errorDescription = (string)resultJtoken["error_description"];
            var _originalError = (string)resultJtoken["original_error"];
            Assert.AreEqual(parameter.CaradaId, _caradaId);
            Assert.AreEqual("auth_provider_error", _error);
            Assert.AreEqual("統合認可プロバイダのユーザー登録API実行でエラーが発生しました", _errorDescription);
            Assert.IsNotNull("", _originalError);

            mockIdGenerator.VerifyAll();
            mockJwtApiRequest.VerifyAll();
        }

        [TestMethod]
        public void ResetUserInfo_正常系_ユーザー情報がリセットされる_Initializationが1()
        {
            var parameter = new ResetUserCondition
            {
                CaradaId = "testUser",
                Initialization = "1"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<ResetUserCondition>();
            ReflectionAccessor.GetProperty<JwtBase<ResetUserCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(newUser);
            mockUserManager.Setup(s => s.GeneratePasswordResetTokenAsync("testId")).ReturnsAsync("testToken");
            mockUserManager.Setup(s => s.ResetPasswordAsync("testId", "testToken", "Pass1234")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateAsync(newUser)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync("testId")).ReturnsAsync(IdentityResult.Success);

            SetBaseField("_userManager", mockUserManager.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            var task = target.ResetUserInfo(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _password = (string)resultJtoken["password"];
            var _initialization = (string)resultJtoken["initialization"];
            Assert.AreEqual(_caradaId, "testUser");
            Assert.AreEqual("Pass1234", _password);
            Assert.AreEqual("1", _initialization);

            mockIdGenerator.VerifyAll();
        }

        [TestMethod]
        public void ResetUserInfo_正常系_ユーザー情報がリセットされる_Initializationが0()
        {
            var parameter = new ResetUserCondition
            {
                CaradaId = "testUser",
                Initialization = "0"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<ResetUserCondition>();
            ReflectionAccessor.GetProperty<JwtBase<ResetUserCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(newUser);
            mockUserManager.Setup(s => s.GeneratePasswordResetTokenAsync("testId")).ReturnsAsync("testToken");
            mockUserManager.Setup(s => s.ResetPasswordAsync("testId", "testToken", "Pass1234")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateAsync(newUser)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync("testId")).ReturnsAsync(IdentityResult.Success);

            SetBaseField("_userManager", mockUserManager.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            var task = target.ResetUserInfo(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _password = (string)resultJtoken["password"];
            var _initialization = (string)resultJtoken["initialization"];
            Assert.AreEqual(_caradaId, "testUser");
            Assert.AreEqual("Pass1234", _password);
            Assert.AreEqual("0", _initialization);

            mockIdGenerator.VerifyAll();
        }

        [TestMethod]
        public void ResetUserInfo_正常系_ユーザー情報がリセットされる_Initializationが未設定()
        {
            var parameter = new ResetUserCondition
            {
                CaradaId = "testUser"
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<ResetUserCondition>();
            ReflectionAccessor.GetProperty<JwtBase<ResetUserCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(newUser);
            mockUserManager.Setup(s => s.GeneratePasswordResetTokenAsync("testId")).ReturnsAsync("testToken");
            mockUserManager.Setup(s => s.ResetPasswordAsync("testId", "testToken", "Pass1234")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateAsync(newUser)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync("testId")).ReturnsAsync(IdentityResult.Success);

            SetBaseField("_userManager", mockUserManager.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            var task = target.ResetUserInfo(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _password = (string)resultJtoken["password"];
            var _initialization = (string)resultJtoken["initialization"];
            Assert.AreEqual(_caradaId, "testUser");
            Assert.AreEqual("Pass1234", _password);
            Assert.AreEqual("0", _initialization);

            mockIdGenerator.VerifyAll();
        }

        [TestMethod]
        public void ResetUserInfo_正常系_ユーザー情報がリセットされる_Initializationが空文字()
        {
            var parameter = new ResetUserCondition
            {
                CaradaId = "testUser",
                Initialization = ""
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<ResetUserCondition>();
            ReflectionAccessor.GetProperty<JwtBase<ResetUserCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(newUser);
            mockUserManager.Setup(s => s.GeneratePasswordResetTokenAsync("testId")).ReturnsAsync("testToken");
            mockUserManager.Setup(s => s.ResetPasswordAsync("testId", "testToken", "Pass1234")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateAsync(newUser)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync("testId")).ReturnsAsync(IdentityResult.Success);

            SetBaseField("_userManager", mockUserManager.Object);

            var mockIdGenerator = new Mock<IdGenerator>();
            mockIdGenerator.Setup(s => s.GenerateRandomPassword()).Returns("Pass1234");
            SetField("idGenerator", mockIdGenerator.Object);

            var task = target.ResetUserInfo(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _password = (string)resultJtoken["password"];
            var _initialization = (string)resultJtoken["initialization"];
            Assert.AreEqual(_caradaId, "testUser");
            Assert.AreEqual("Pass1234", _password);
            Assert.AreEqual("0", _initialization);

            mockIdGenerator.VerifyAll();
        }

        [TestMethod]
        public void ResetUserInfo_異常系_CaradaIdに紐付くユーザー情報が無い場合()
        {
            var parameter = new ResetUserCondition
            {
                CaradaId = "testUser",
                Initialization = ""
            };

            var newUser = new CaradaIdUser
            {
                UserName = parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0,
                Id = "testId"
            };

            var jwt = new Jwt<ResetUserCondition>();
            ReflectionAccessor.GetProperty<JwtBase<ResetUserCondition>>("Parameter").SetValue(jwt, parameter);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.ResetUserInfo(jwt);
            var result = task.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            var _caradaId = (string)resultJtoken["carada_id"];
            var _error = (string)resultJtoken["error"];
            var _errorDescription = (string)resultJtoken["error_description"];
            Assert.AreEqual(_caradaId, "testUser");
            Assert.AreEqual("invalid_carada_id", _error);
            Assert.AreEqual("CARADA IDに紐付くユーザー情報がありません。", _errorDescription);
        }
    }
}
