﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api.Conditions
{
    [TestClass]
    public class ResetUserConditionTest
    {
        /// <summary>
        /// ResetUserConditionのテストクラス
        /// </summary>
        [TestMethod]
        public void ResetUserCondition_正常系_属性の確認()
        {
            var p = ReflectionAccessor.GetProperty<ResetUserCondition>("CaradaId");
            var a = p.GetCustomAttribute<RequiredAttribute>();
            Assert.IsNotNull(p);
            Assert.AreEqual(a.ErrorMessage, "invalid_carada_id");

            p = ReflectionAccessor.GetProperty<ResetUserCondition>("Initialization");
            a = p.GetCustomAttribute<RequiredAttribute>();
            Assert.IsNotNull(p);
            Assert.IsNull(a);
        }
    }
}
