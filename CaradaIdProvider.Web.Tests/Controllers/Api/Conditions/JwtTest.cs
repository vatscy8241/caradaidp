﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api.Conditions
{
    /// <summary>
    /// JWTの検証のテストクラス。
    /// </summary>
    [TestClass]
    public class JwtTest
    {

        #region private

        private const string _testIssuer = "TestIssuer";
        private const string _testSecretkey = "J>sK;4z^MEpC[F8@V|Eo9YDYwoQ|m09r*#}|45S#EP0={G85(Hn@2MN@Vq>ZoaY_";

        private class AnyAttributeCondition : ConditionBase
        {
            [Required(ErrorMessage = "param1_must_not_be_null")]
            public string Param1 { get; set; }

            public bool Param2 { get; set; }

            [Max(1, ErrorMessage = "param3_must_be_under_1")]
            [Max(2, ErrorMessage = "param3_must_be_under_2")]
            public long? Param3 { get; set; }

            [JsonIgnore]
            public bool IsValidateCalled { get; private set; }

            public override IEnumerable<ValidationResult> Validate(ValidationContext context)
            {
                IsValidateCalled = true;

                if (!Param2)
                {
                    yield return new ValidationResult("param2_must_be_true");
                }
            }

            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
            private class MaxAttribute : ValidationAttribute
            {
                public long Max { get; set; }

                public MaxAttribute(long max)
                {
                    Max = max;
                }

                public override bool IsValid(object value)
                {
                    return (long)value <= Max;
                }
            }
        }

        private long ToUnixTime(DateTime t)
        {
            return (long)t.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }

        private string CreateToken(string iss, long? iat, long? exp, long? nbf, object parameters)
        {
            var credentials = new SigningCredentials
            (
                GetHmacSha256SigningKey(_testSecretkey),
                SecurityAlgorithms.HmacSha256Signature,
                SecurityAlgorithms.Sha256Digest
            );

            var jwtHeader = new JwtHeader(credentials);
            var jwtPayload = new JwtPayload();

            var claims = new List<Claim>();
            if (iss != null)
            {
                claims.Add(new Claim("iss", iss));
            }
            if (iat != null)
            {
                claims.Add(new Claim("iat", iat.ToString(), ClaimValueTypes.Integer64));
            }
            if (exp != null)
            {
                claims.Add(new Claim("exp", exp.ToString(), ClaimValueTypes.Integer64));
            }
            if (nbf != null)
            {
                claims.Add(new Claim("nbf", nbf.ToString(), ClaimValueTypes.Integer64));
            }

            jwtPayload.AddClaims(claims);

            var token = new JwtSecurityToken(jwtHeader, jwtPayload);

            if (parameters != null)
            {
                token.Payload.Add("params", parameters);
            }

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private SymmetricSecurityKey GetHmacSha256SigningKey(string key)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            if (keyBytes.Length < 64)
            {
                Array.Resize(ref keyBytes, 64);
            }

            return new InMemorySymmetricSecurityKey(keyBytes);
        }

        private TestIssuers _dbset = new TestIssuers()
        {
            new Issuers
            {
                IssuerId = _testIssuer,
                SecretKey = _testSecretkey
            }
        };

        public object Settings { get; private set; }

        private static void SetValueToField<T>(T target, string fieldName, object value)
        {
            typeof(T).GetField(fieldName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).SetValue(target, value);
        }

        #endregion

        [TestInitialize]
        public void TestInitialize()
        {
            MockHttpContextHelper.SetCurrentOfFakeContextWithoutSession();
            // HttpContext.Current = httpContext.Object;
        }
        [TestCleanup]
        public void ClieanUp()
        {
        }

        [TestMethod]
        public void Validate_正常系_発行者IDに紐づくシークレットキーが取得され署名検証に成功する()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param1 = "test", param2 = true, param3 = 1 });
            var condition = new Jwt<AnyAttributeCondition>()
            {
                Token = token
            };

            var context = new CaradaIdPDbContext
            {
                Issuers = _dbset
            };
            SetValueToField<Jwt<AnyAttributeCondition>>(condition, "_context", context);

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(0, results.Count);
            Assert.AreEqual<string>("test", condition.Parameter.Param1);
            Assert.IsTrue(condition.Parameter.Param2);
            Assert.AreEqual<long?>(1, condition.Parameter.Param3);
            Assert.IsTrue(condition.Parameter.IsValidateCalled);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_発行者IDに紐づくシークレットキーがない_署名検証エラー()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken("invalid_iss", iat, iat + 120, iat - 120, new { param1 = "test", param2 = true, param3 = 1 });
            var condition = new Jwt<AnyAttributeCondition>()
            {
                Token = token
            };

            var context = new CaradaIdPDbContext
            {
                Issuers = _dbset
            };
            SetValueToField<Jwt<AnyAttributeCondition>>(condition, "_context", context);

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_issuer", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }
    }
}
