﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api.Conditions
{
    [TestClass]
    public class RegistUserAddConditionTest
    {
        /// <summary>
        /// RegistUserAddConditionTestのテストクラス
        /// CaradaIdの形式チェックの正規表現については、NewRegistMockViewModelTestで行っている。
        /// </summary>
        [TestMethod]
        public void RegistUserAddCondition_正常系_属性の確認()
        {
            var p = ReflectionAccessor.GetProperty<RegistUserAddCondition>("ClientId");
            var a = p.GetCustomAttribute<RequiredAttribute>();
            Assert.IsNotNull(p);
            Assert.AreEqual(a.ErrorMessage, "invalid_client_id");

            p = ReflectionAccessor.GetProperty<RegistUserAddCondition>("CaradaId");
            a = p.GetCustomAttribute<RequiredAttribute>();
            var b = p.GetCustomAttribute<RegularExpressionAttribute>();
            Assert.IsNotNull(p);
            Assert.AreEqual(a.ErrorMessage, "invalid_carada_id");
            Assert.AreEqual(b.ErrorMessage, "invalid_carada_id");
        }
    }
}
