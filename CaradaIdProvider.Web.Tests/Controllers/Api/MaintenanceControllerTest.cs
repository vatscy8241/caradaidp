﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api
{
    [TestClass]
    public class MaintenanceControllerTest
    {
        MaintenanceController target = new MaintenanceController();

        private readonly HttpRequestMessage httpRequestMessage = new HttpRequestMessage()
        {
            Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
        };

        [TestInitialize]
        public void TestInitialize()
        {
            target = new MaintenanceController() { Request = httpRequestMessage };
        }

        [TestMethod]
        public void Get_正常系_メンテナンスエラーが返ってくる()
        {
            var result = target.Get();

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.ServiceUnavailable, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            Assert.AreEqual("maintenance", (string)resultJtoken["error"]);
            Assert.AreEqual("現在サーバーのメンテナンス中です。", (string)resultJtoken["error_description"]);
        }

        [TestMethod]
        public void Post_正常系_メンテナンスエラーが返ってくる()
        {
            var result = target.Post();

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.ServiceUnavailable, result.StatusCode);
            var resultJtoken = JToken.Parse(result.Content.ReadAsStringAsync().Result);
            Assert.AreEqual("maintenance", (string)resultJtoken["error"]);
            Assert.AreEqual("現在サーバーのメンテナンス中です。", (string)resultJtoken["error_description"]);
        }
    }
}
