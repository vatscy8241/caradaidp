﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using System.Linq;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api.Jwt
{
    /// <summary>
    /// 管理者用APIのTParameter部が継承する基底Conditionクラスのテストクラス。
    /// </summary>
    [TestClass]
    public class ConditionBaseTest
    {
        #region UT dummy class
        /// <summary>
        /// Disposeが呼ばれたらtrue
        /// </summary>
        private static bool IsCalled = false;
        private class TestA : ConditionBase
        {
        }
        private class TestB : ConditionBase
        {
            private SomeDisposable fieldB = new SomeDisposable();
        }
        private class TestD : ConditionBase
        {
            private NotDisposable fieldD = new NotDisposable();
        }
        private class SomeDisposable : System.IDisposable
        {
            public void Dispose()
            {
                IsCalled = true;
            }
        }
        private interface IDisposable { };
        private class NotDisposable : IDisposable
        {
        }
        #endregion

        private ConditionBase target;

        [TestCleanup]
        public void Cleanup()
        {
            IsCalled = false;
        }

        [TestMethod]
        public void Validate_正常系_サブクラスで実装されていない()
        {
            // カバレッジ用の無意味なテストです
            target = new TestA();
            var result = target.Validate(null);
            Assert.AreEqual(0, result.ToList().Count);
        }

        [TestMethod]
        public void Dispose_正常系_サブクラスにIDisposableなフィールドがない()
        {
            target = new TestA();
            // 例外など起こらなければテストOK
            target.Dispose();
        }

        [TestMethod]
        public void Dispose_正常系_サブクラスにIDisposableなフィールドがある()
        {
            target = new TestB();
            target.Dispose();
            Assert.IsTrue(IsCalled);
        }

        [TestMethod]
        public void Dispose_正常系_サブクラスにIDisposableなフィールドがあるがDisposeはない()
        {
            var target = new TestD();
            target.Dispose();
            Assert.IsFalse(IsCalled);
        }
    }
}
