﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using System.Web.Script.Serialization;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api.Jwt
{
    /// <summary>
    /// JwtValidationFilterBaseAttributeのテストクラス
    /// </summary>
    [TestClass]
    public class JwtValidationFilterBaseAttributeTest
    {
        private class JwtValidationFilterForTest : JwtValidationFilterBaseAttribute
        {
            protected override HttpResponseMessage CreateApplicationResponse(HttpActionContext actionContext, string errorCode)
            {
                object result;

                try
                {
                    result = new
                    {
                        carada_id = ((JwtBase<RegistUserAddCondition>)actionContext.ActionArguments["condition"]).Parameter.CaradaId,
                        error = errorCode,
                        error_description = "JWTのパラメーターが不正です"
                    };
                }
                catch (NullReferenceException)
                {
                    result = new
                    {
                        carada_id = "",
                        error = errorCode,
                        error_description = "JWTが不正です"
                    };
                }

                return actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }

        [TestMethod]
        public void OnActionExecuting_正常系_ModelStateに異常がない_エラーレスポンスは生成されない()
        {
            var httpActionContext = new HttpActionContext();

            var attribute = new JwtValidationFilterForTest();

            attribute.OnActionExecuting(httpActionContext);

            var result = httpActionContext.Response;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void OnActionExecuting_正常系_ActionArgumentsのcondition値がnullでない_エラーレスポンスは生成されない()
        {
            var httpActionContext = new HttpActionContext() { };
            httpActionContext.ActionArguments["condition"] = "Value";

            var attribute = new JwtValidationFilterForTest();

            attribute.OnActionExecuting(httpActionContext);

            var result = httpActionContext.Response;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void OnActionExecuting_異常系_ActionArgumentsのcondition値がnull_エラーレスポンスが生成される()
        {
            var httpActionContext = new HttpActionContext() { };
            httpActionContext.ActionArguments["condition"] = null;

            var attribute = new JwtValidationFilterForTest();

            httpActionContext.ControllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage(HttpMethod.Get, "http://www.abc.ef/"));
            httpActionContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, httpActionContext.ControllerContext.Configuration);

            attribute.OnActionExecuting(httpActionContext);

            var result = httpActionContext.Response;

            var jsonSerializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
            var resultContent = result.Content;
            var resultJsonObject = JToken.Parse(resultContent.ReadAsStringAsync().Result);
            var error = (string)resultJsonObject["error"];

            Assert.IsNotNull(result);
            Assert.AreEqual<string>("jwt_invalid_request", error);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [TestMethod]
        public void OnActionExecuting_正常系_エラーコードが未指定_エラーレスポンスが生成される()
        {
            var httpActionContext = new HttpActionContext();

            var attribute = new JwtValidationFilterForTest();

            httpActionContext.ControllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage(HttpMethod.Get, "http://www.abc.ef/"));
            httpActionContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, httpActionContext.ControllerContext.Configuration);

            httpActionContext.ModelState.AddModelError("key1", "");

            var condition = new Jwt<RegistUserAddCondition>();
            httpActionContext.ActionArguments.Add("condition", condition);

            attribute.OnActionExecuting(httpActionContext);

            var result = httpActionContext.Response;

            var jsonSerializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
            var resultContent = result.Content;
            var resultJsonObject = JToken.Parse(resultContent.ReadAsStringAsync().Result);
            var error = (string)resultJsonObject["error"];

            Assert.IsNotNull(result);
            Assert.AreEqual<string>("jwt_invalid_request", error);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);

        }

        [TestMethod]
        public void OnActionExecuting_正常系_エラーコードが指定されている_それにひもづくエラーレスポンスが生成される()
        {
            var httpActionContext = new HttpActionContext();

            var attribute = new JwtValidationFilterForTest();

            httpActionContext.ControllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage(HttpMethod.Get, "http://www.abc.ef/"));
            httpActionContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, httpActionContext.ControllerContext.Configuration);

            httpActionContext.ModelState.AddModelError("key1", "application_error");

            var condition = new Jwt<RegistUserAddCondition>();
            httpActionContext.ActionArguments.Add("condition", condition);

            attribute.OnActionExecuting(httpActionContext);

            var result = httpActionContext.Response;

            var jsonSerializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
            var resultContent = result.Content;
            var resultJsonObject = JToken.Parse(resultContent.ReadAsStringAsync().Result);
            var error = (string)resultJsonObject["error"];

            Assert.IsNotNull(result);
            Assert.AreEqual<string>("application_error", error);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [TestMethod]
        public void OnActionExecuting_正常系_複数エラーコードが指定されている_辞書順で最初のエラーコードにエラーレスポンスが生成される()
        {
            var httpActionContext = new HttpActionContext();

            var attribute = new JwtValidationFilterForTest();

            httpActionContext.ControllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage(HttpMethod.Get, "http://www.abc.ef/"));
            httpActionContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, httpActionContext.ControllerContext.Configuration);

            httpActionContext.ModelState.AddModelError("key1", "error_d");
            httpActionContext.ModelState.AddModelError("key2", "error_b");
            httpActionContext.ModelState.AddModelError("key3", "error_a");
            httpActionContext.ModelState.AddModelError("key4", "error_c");

            var condition = new Jwt<RegistUserAddCondition>();
            httpActionContext.ActionArguments.Add("condition", condition);

            attribute.OnActionExecuting(httpActionContext);

            var result = httpActionContext.Response;

            var jsonSerializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
            var resultContent = result.Content;
            var resultJsonObject = JToken.Parse(resultContent.ReadAsStringAsync().Result);
            var error = (string)resultJsonObject["error"];

            Assert.IsNotNull(result);
            Assert.AreEqual<string>("error_a", error);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
