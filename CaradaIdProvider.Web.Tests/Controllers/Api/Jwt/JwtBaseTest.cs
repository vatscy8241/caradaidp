﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api.Jwt
{
    /// <summary>
    /// JwtBaseのテストクラス。
    /// </summary>
    [TestClass]
    public class JwtBaseTest
    {

        #region "private"

        private const string _testIssuer = "TestIssuer";
        private const string _testSecretkey = "J>sK;4z^MEpC[F8@V|Eo9YDYwoQ|m09r*#}|45S#EP0={G85(Hn@2MN@Vq>ZoaY_";

        private class JwtForTest<T> : JwtBase<T>
            where T : ConditionBase, new()
        {
            private Func<string, string> _getSecretKey;

            public JwtForTest(Func<string, string> getSecretKey)
            {
                _getSecretKey = getSecretKey;
            }

            protected override string GetSecretKey(string issuer)
            {
                return _getSecretKey(issuer);
            }
        }

        private class NoAttributeCondition : ConditionBase
        {
            public string Param1 { get; set; }
            public int? Param2 { get; set; }

            [JsonIgnore]
            public bool IsValidateCalled { get; private set; }

            public override IEnumerable<ValidationResult> Validate(ValidationContext context)
            {
                IsValidateCalled = true;

                if (string.IsNullOrEmpty(Param1))
                {
                    yield return new ValidationResult("param1_must_not_be_null");
                }

                if (Param2 != null && Param2 < 0)
                {
                    yield return new ValidationResult("param2_must_be_positive_or_null");
                }
            }
        }

        private class AnyAttributeCondition : ConditionBase
        {
            [Required(ErrorMessage = "param1_must_not_be_null")]
            public string Param1 { get; set; }

            public bool Param2 { get; set; }

            [Max(1, ErrorMessage = "param3_must_be_under_1")]
            [Max(2, ErrorMessage = "param3_must_be_under_2")]
            public long? Param3 { get; set; }

            [JsonIgnore]
            public bool IsValidateCalled { get; private set; }

            public override IEnumerable<ValidationResult> Validate(ValidationContext context)
            {
                IsValidateCalled = true;

                if (!Param2)
                {
                    yield return new ValidationResult("param2_must_be_true");
                }
            }

            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
            private class MaxAttribute : ValidationAttribute
            {
                public long Max { get; set; }

                public MaxAttribute(long max)
                {
                    Max = max;
                }

                public override bool IsValid(object value)
                {
                    return (long)value <= Max;
                }
            }
        }

        private long ToUnixTime(DateTime t)
        {
            return (long)t.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }

        private string CreateToken(string iss, long? iat, long? exp, long? nbf, object parameters)
        {
            var credentials = new SigningCredentials
            (
                GetHmacSha256SigningKey(_testSecretkey),
                SecurityAlgorithms.HmacSha256Signature,
                SecurityAlgorithms.Sha256Digest
            );

            var jwtHeader = new JwtHeader(credentials);
            var jwtPayload = new JwtPayload();

            var claims = new List<Claim>();
            if (iss != null)
            {
                claims.Add(new Claim("iss", iss));
            }
            if (iat != null)
            {
                claims.Add(new Claim("iat", iat.ToString(), ClaimValueTypes.Integer64));
            }
            if (exp != null)
            {
                claims.Add(new Claim("exp", exp.ToString(), ClaimValueTypes.Integer64));
            }
            if (nbf != null)
            {
                claims.Add(new Claim("nbf", nbf.ToString(), ClaimValueTypes.Integer64));
            }

            jwtPayload.AddClaims(claims);

            var token = new JwtSecurityToken(jwtHeader, jwtPayload);

            if (parameters != null)
            {
                token.Payload.Add("params", parameters);
            }

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private SymmetricSecurityKey GetHmacSha256SigningKey(string key)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            if (keyBytes.Length < 64)
            {
                Array.Resize(ref keyBytes, 64);
            }

            return new InMemorySymmetricSecurityKey(keyBytes);
        }

        #endregion
        [TestInitialize]
        public void TestInitialize()
        {
            MockHttpContextHelper.SetCurrentOfFakeContextWithoutSession();
        }

        [TestMethod]
        public void Validate_正常系_属性なし_有効なJWTを指定_検証に成功する()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param1 = "test", param2 = 5 });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(0, results.Count);
            Assert.AreEqual<string>("test", condition.Parameter.Param1);
            Assert.AreEqual<int?>(5, condition.Parameter.Param2);
            Assert.IsTrue(condition.Parameter.IsValidateCalled);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_正常系_属性あり_有効なJWTを指定_検証に成功する()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param1 = "test", param2 = true, param3 = 1 });
            var condition = new JwtForTest<AnyAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(0, results.Count);
            Assert.AreEqual<string>("test", condition.Parameter.Param1);
            Assert.IsTrue(condition.Parameter.Param2);
            Assert.AreEqual<long?>(1, condition.Parameter.Param3);
            Assert.IsTrue(condition.Parameter.IsValidateCalled);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_iss未指定_リクエスト不正()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(null, iat, iat + 120, iat - 120, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_request", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_iat未指定_リクエスト不正()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, null, iat + 120, iat - 120, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_request", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_exp未指定_リクエスト不正()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, null, iat - 120, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_request", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_nbf未指定_リクエスト不正()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, null, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_request", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_固有パラメータ未指定_デフォルトの検証が行われる()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, null);
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("param1_must_not_be_null", results[0].ErrorMessage);
            Assert.IsTrue(condition.Parameter.IsValidateCalled);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_固有パラメータ検証失敗_固有エラー()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param2 = -1 });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(2, results.Count);
            Assert.AreEqual<int>(1, results.Count(r => r.ErrorMessage == "param1_must_not_be_null"));
            Assert.AreEqual<int>(1, results.Count(r => r.ErrorMessage == "param2_must_be_positive_or_null"));
            Assert.IsTrue(condition.Parameter.IsValidateCalled);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_シークレットキーが見つからない_署名検証エラー()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => null)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_issuer", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_署名検証に失敗_署名検証エラー()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => "imvalidkey")
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_issuer", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_nbfが未来時_有効期限外()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat + 10, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_life_time", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_expが過去時_有効期限外()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat, iat - 120, new { param1 = "test" });
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_life_time", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_JSON形式不正_リクエスト不正()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(null, iat, iat + 120, iat - 120, new { param1 = "test" });
            var splitToken = token.Split('.');
            splitToken[1] = Convert.ToBase64String(Encoding.UTF8.GetBytes(@"{""iss"":""test""")).Replace('+', '-').Replace('/', '_').TrimEnd('=');
            var condition = new JwtForTest<NoAttributeCondition>(_ => _testSecretkey)
            {
                Token = splitToken.Aggregate((now, next) => now + "." + next)
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<string>("jwt_invalid_request", results[0].ErrorMessage);
            Assert.IsNull(condition.Parameter);
            Assert.IsNull(HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_属性による検証失敗_Validateは呼び出されない()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param2 = false, param3 = 3 });
            var condition = new JwtForTest<AnyAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(3, results.Count);
            Assert.AreEqual<int>(1, results.Count(r => r.ErrorMessage == "param1_must_not_be_null" && r.MemberNames.Single() == "Param1"));
            Assert.AreEqual<int>(1, results.Count(r => r.ErrorMessage == "param3_must_be_under_1" && r.MemberNames.Single() == "Param3"));
            Assert.AreEqual<int>(1, results.Count(r => r.ErrorMessage == "param3_must_be_under_2" && r.MemberNames.Single() == "Param3"));
            Assert.IsNull(condition.Parameter.Param1);
            Assert.IsFalse(condition.Parameter.Param2);
            Assert.AreEqual<long?>(3, condition.Parameter.Param3);
            Assert.IsFalse(condition.Parameter.IsValidateCalled);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }

        [TestMethod]
        public void Validate_異常系_属性による検証成功_Validateが呼び出される()
        {
            var iat = ToUnixTime(DateTime.UtcNow);
            var token = CreateToken(_testIssuer, iat, iat + 120, iat - 120, new { param1 = "test", param2 = false, param3 = 1 });
            var condition = new JwtForTest<AnyAttributeCondition>(_ => _testSecretkey)
            {
                Token = token
            };

            var results = condition.Validate(null).ToList();
            Assert.AreEqual<int>(1, results.Count);
            Assert.AreEqual<int>(1, results.Count(r => r.ErrorMessage == "param2_must_be_true"));
            Assert.AreEqual<string>("test", condition.Parameter.Param1);
            Assert.IsFalse(condition.Parameter.Param2);
            Assert.AreEqual<long?>(1, condition.Parameter.Param3);
            Assert.IsTrue(condition.Parameter.IsValidateCalled);
            Assert.AreEqual(_testIssuer, HttpContext.Current.Items["Issuer"]);
        }
    }
}
