﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Controllers.Api;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System.IO;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Api
{
    /// <summary>
    /// BaseApiControllerのテストクラス
    /// </summary>
    [TestClass]
    public class BaseApiControllerTest
    {
        /// <summary>
        /// BaseWebControllerのメソッドがprotectedなので、継承クラスを作成。
        /// </summary>
        private class BaseApiControllerUTEntrance : BaseApiController
        {
            public BaseApiControllerUTEntrance()
                : base()
            {
            }

            public BaseApiControllerUTEntrance(ApplicationUserManager userManager)
                : base(userManager)
            {
            }
        }

        /// <summary>
        /// テスト対象
        /// </summary>
        private BaseApiControllerUTEntrance target;
        private Mock<ApplicationUserManager> mockUserManager;

        [TestInitialize]
        public void Initialize()
        {
            target = new BaseApiControllerUTEntrance();
            MvcMockHelpers.FakeHttpContext();
            mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://tempuri.org", ""), new HttpResponse(new StringWriter()));
            HttpContext.Current.Items["owin.Environment"] = MvcMockHelpers.Owin.Environment;
        }

        [TestMethod]
        public void UserManager_正常系_nullでないとき()
        {
            target = new BaseApiControllerUTEntrance(mockUserManager.Object);
            Assert.AreEqual(mockUserManager.Object, target.UserManager);
        }

        [TestMethod]
        public void UserManager_正常系_nullのとき_OwinContextから取得されること()
        {
            MvcMockHelpers.Owin.Set<ApplicationUserManager>(mockUserManager.Object);
            Assert.AreEqual(mockUserManager.Object, target.UserManager);
        }

        [TestMethod]
        public void CaradaIdPDb_正常系_nullでないとき()
        {
            var p = ReflectionAccessor.GetProperty<BaseApiController>("CaradaIdPDb");
            var mockUserDb = new Mock<CaradaIdPDbContext>();
            p.SetValue(target, mockUserDb.Object);
            Assert.AreEqual(mockUserDb.Object, target.CaradaIdPDb);
        }

        [TestMethod]
        public void CaradaIdPDb_正常系_nullのとき_OwinContextから取得されること()
        {
            var mockUserDb = new Mock<CaradaIdPDbContext>();
            MvcMockHelpers.Owin.Set<CaradaIdPDbContext>(mockUserDb.Object);
            Assert.AreEqual(mockUserDb.Object, target.CaradaIdPDb);
        }
    }
}
