﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Resource;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System.Configuration;
using System.Web.Mvc;


namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Web
{
    /// <summary>
    /// ErrorControllerのテストクラス。
    /// </summary>
    [TestClass]
    public class ErrorControllerTest
    {
        private const string TmpKey = "testKey";
        private const string TmpVal = "testValue";

        /// <summary>
        /// テスト対象
        /// </summary>
        private ErrorController target;

        [TestInitialize]
        public void Initialize()
        {
            target = new ErrorController();
            target.SetFakeControllerContext();
            target.TempData[TmpKey] = TmpVal;
        }

        [TestCleanup]
        public void Cleanup()
        {
            // 設定ファイルのMaintenanceMode関連の値を初期値に戻す
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenanceMode"].Value = "0";
            settings["MaintenancePeriodMessage"].Value = "";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }

        [TestMethod]
        public void NotFound_正常系()
        {
            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.NotFound() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("NotFound", result.ViewName);
            Assert.AreEqual(GetErrorMessage("NotFound"), result.ViewBag.ERROR_MESSAGE);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void ServerError_正常系()
        {
            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.ServerError() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.ViewName);
            Assert.AreEqual(GetErrorMessage("ServerError"), result.ViewBag.ERROR_MESSAGE);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void Timeout_正常系()
        {
            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.Timeout() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Timeout", result.ViewName);
            Assert.AreEqual(GetErrorMessage("Timeout"), result.ViewBag.ERROR_MESSAGE);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void Maintenance_AuthenticationRequestからの遷移()
        {
            target.Session["AuthorizeEndpointSession"] = new AuthorizeEndpointSession()
            {
                ReturnUrl = "https://test/oauth2/auth",
                ClientId = "testclientid",
                Scope = "openid",
                ResponseType = "code",
                RedirectUri = "https://test/auth/response",
                State = "state001"
            };

            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.Maintenance() as RedirectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("https://test/oauth2/auth?client_id=testclientid&scope=openid&response_type=code&redirect_uri=https%3A%2F%2Ftest%2Fauth%2Fresponse&state=state001",
                result.Url);
            var session = target.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;
            Assert.AreEqual(LoginResult.Maintenance, session.LoginResult);
            Assert.AreEqual(0, target.TempData.Count);
        }

        [TestMethod]
        public void Maintenance_MaintenancePeriodMessageなし()
        {
            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.Maintenance() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Maintenance", result.ViewName);
            Assert.AreEqual(GetErrorMessage("MaintenanceMode"), result.ViewBag.ERROR_MESSAGE);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void Maintenance_MaintenancePeriodMessageあり_MaintenanceModeが0()
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenancePeriodMessage"].Value = "期間";
            settings["MaintenanceMode"].Value = "0";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.Maintenance() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Maintenance", result.ViewName);
            Assert.AreEqual(GetErrorMessage("MaintenanceMode"), result.ViewBag.ERROR_MESSAGE);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void Maintenance_MaintenancePeriodMessageあり_MaintenanceModeが1()
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenancePeriodMessage"].Value = "期間";
            settings["MaintenanceMode"].Value = "1";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.Maintenance() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Maintenance", result.ViewName);
            Assert.AreEqual(GetErrorMessage("MaintenanceModePeriod") + "期間", result.ViewBag.ERROR_MESSAGE);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void Maintenance_MaintenancePeriodMessageあり_MaintenanceModeが2()
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            settings["MaintenancePeriodMessage"].Value = "期間";
            settings["MaintenanceMode"].Value = "2";
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);

            Assert.AreEqual(TmpVal, target.TempData[TmpKey]);
            var result = target.Maintenance() as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Maintenance", result.ViewName);
            Assert.AreEqual(GetErrorMessage("MaintenanceModePeriod") + "期間", result.ViewBag.ERROR_MESSAGE);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void Return_AuthenticationRequestからの遷移()
        {
            target.Session["AuthorizeEndpointSession"] = new AuthorizeEndpointSession()
            {
                ReturnUrl = "https://test/oauth2/auth",
                ClientId = "testclientid",
                Scope = "openid",
                ResponseType = "code",
                RedirectUri = "https://test/auth/response",
                State = "state001"
            };

            var result = target.Return() as RedirectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("https://test/oauth2/auth?client_id=testclientid&scope=openid&response_type=code&redirect_uri=https%3A%2F%2Ftest%2Fauth%2Fresponse&state=state001",
                result.Url);
            var session = target.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;
            Assert.AreEqual(LoginResult.Error, session.LoginResult);
        }

        [TestMethod]
        public void Return_その他からの遷移()
        {
            var result = target.Return() as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.RouteValues.Count);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("UserSettings", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void GetErrorMessage_置換パラメータあり()
        {
            var m = ReflectionAccessor.GetMethod<ErrorController>("GetErrorMessage");

            var message = m.Invoke(target, new object[] { "Required", new string[] { "項目" } }) as string;
            Assert.AreEqual(GetErrorMessage("Required", "項目"), message);
        }

        [TestMethod]
        public void GetErrorMessage_置換パラメータなし_空()
        {
            var m = ReflectionAccessor.GetMethod<ErrorController>("GetErrorMessage");

            var message = m.Invoke(target, new object[] { "DefaultError", new string[] { } }) as string;
            Assert.AreEqual(GetErrorMessage("DefaultError"), message);
        }

        [TestMethod]
        public void GetErrorMessage_置換パラメータなし_null()
        {
            var m = ReflectionAccessor.GetMethod<ErrorController>("GetErrorMessage");

            var message = m.Invoke(target, new object[] { "DefaultError", null }) as string;
            Assert.AreEqual(GetErrorMessage("DefaultError"), message);
        }

        private string GetErrorMessage(string errorCode, params string[] args)
        {
            if (string.IsNullOrEmpty(errorCode))
            {
                return null;
            }
            var errorFormat = ErrorMessages.ResourceManager.GetString(errorCode);
            return string.Format(errorFormat, args);
        }
    }
}
