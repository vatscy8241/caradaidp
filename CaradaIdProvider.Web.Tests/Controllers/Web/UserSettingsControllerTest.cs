﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using MTI.CaradaIdProvider.Web.Resource;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Web
{
    /// <summary>
    /// ユーザー設定コントローラのテストクラス
    /// </summary>
    [TestClass]
    public class UserSettingsControllerTest
    {
        private UserSettingsController target;
        private Mock<CaradaIdPDbContext> mockDbContext;
        private Mock<ApplicationUserManager> mockUserManager;

        [TestInitialize]
        public void TestInitialize()
        {
            target = new UserSettingsController();
            target.SetFakeControllerContext();
            mockDbContext = new Mock<CaradaIdPDbContext>();
            mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            mockUserManager.VerifyAll();
            mockDbContext.VerifyAll();
            MvcMockHelpers.MockIdentity.VerifyAll();
        }

        /// <summary>
        /// テスト対象が持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<UserSettingsController>(name);
            field.SetValue(target, o);
        }

        /// <summary>
        /// BaseWebControllerが持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetBaseField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<BaseWebController>(name);
            field.SetValue(target, o);
        }

        [TestMethod]
        public void Index_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("Index");
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void Index_異常系_ユーザー情報が取得できない()
        {
            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignOut()).Returns(Task.FromResult(0));
            SetBaseField("_signInManager", mockSignInManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testUser");
            try
            {
                Task.Run(() => target.Index()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Index_異常系_ユーザー情報が利用開始前状態()
        {
            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = false });
            SetBaseField("_userManager", mockUserManager.Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignOut()).Returns(Task.FromResult(0)); ;
            SetBaseField("_signInManager", mockSignInManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testUser");
            try
            {
                Task.Run(() => target.Index()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Index_異常系_秘密の質問の回答が取得できない()
        {
            //// 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = true, Id = "userId" });
            SetBaseField("_userManager", mockUserManager.Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignOut()).Returns(Task.FromResult(0)); ;
            SetBaseField("_signInManager", mockSignInManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testUser");
            try
            {
                Task.Run(() => target.Index()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Index_異常系_秘密の質問の回答が2件以上存在する()
        {
            //// 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答1",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答2",
                UserId = "userId"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = true, Id = "userId" });
            SetBaseField("_userManager", mockUserManager.Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignOut()).Returns(Task.FromResult(0)); ;
            SetBaseField("_signInManager", mockSignInManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testUser");
            try
            {
                Task.Run(() => target.Index()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Index_異常系_秘密の質問が取得できない()
        {
            //// 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答1",
                UserId = "userId"
            });
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = true, Id = "userId" });
            SetBaseField("_userManager", mockUserManager.Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignOut()).Returns(Task.FromResult(0)); ;
            SetBaseField("_signInManager", mockSignInManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testUser");
            try
            {
                Task.Run(() => target.Index()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Index_正常系_アカウント管理画面が表示される()
        {
            //// 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答",
                UserId = "userId"
            });
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });

            var user = new CaradaIdUser()
            {
                UserName = "testUser",
                EmailConfirmed = true,
                Email = "test@test.com",
                Id = "userId"
            };

            mockUserManager.Setup(s => s.FindByNameAsync("testUser")).ReturnsAsync(user);
            SetBaseField("_userManager", mockUserManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testUser");

            var task = target.Index();
            var result = task.Result as ViewResult;

            Assert.IsNotNull(result);
            var model = result.Model as UserSettingsViewModel;
            Assert.AreEqual("testUser", model.CaradaId);
            Assert.AreEqual("test@test.com", model.Email);
            Assert.AreEqual("秘密の質問", model.SecurityQuestion);
            Assert.AreEqual("", result.ViewName);

        }

        [TestMethod]
        public void PasswordResetGuide_初期表示_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("PasswordResetGuideView", 0);
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void PasswordResetGuide_初期表示_正常系_パスワード再設定案内画面が表示される()
        {
            var result = target.PasswordResetGuideView() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("PasswordResetGuide", result.ViewName);
        }

        [TestMethod]
        public void PasswordResetGuide_パスワードを再設定する_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("PasswordResetGuide", 1);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void PasswordResetGuide_パスワードを再設定する_異常系_ユーザーが取得できない()
        {
            mockUserManager.Setup(s => s.FindByEmailAsync("test@test.com")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.PasswordResetGuide(new PasswordResetGuideViewModel() { Email = "test@test.com" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("NotRegisteredUser"), target.ModelState["Email"].Errors[0].ErrorMessage);

        }

        [TestMethod]
        public void PasswordResetGuide_パスワードを再設定する_異常系_利用開始前ユーザーが取得できた場合()
        {
            var user = new CaradaIdUser()
            {
                EmailConfirmed = false
            };
            mockUserManager.Setup(s => s.FindByEmailAsync("test@test.com")).ReturnsAsync(user);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.PasswordResetGuide(new PasswordResetGuideViewModel() { Email = "test@test.com" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("NotRegisteredUser"), target.ModelState["Email"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordResetGuide_パスワードを再設定する_正常系_パスワード再設定画面へ遷移()
        {
            var userId = "66f50c73-cd05-4679-9127-08af2e4f9f0e";
            var fakeEmail = "test@test.com";

            var targetUser = new CaradaIdUser()
            {
                Id = userId,
                EmailConfirmed = true,
                AuthCodeExpireDateUtc = DateTime.MinValue,
                AuthCodeFailedCount = 1
            };
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeEmail)).ReturnsAsync(targetUser);
            // 更新されること
            mockUserManager.Setup(s => s.UpdateAsync(targetUser))
                .ReturnsAsync(IdentityResult.Success)
                .Callback((CaradaIdUser u) =>
                {
                    // 有効期限は実行時間からの誤差許容を2秒として判断
                    Assert.IsTrue(u.AuthCodeExpireDateUtc > DateTime.UtcNow.AddMinutes(ApplicationUserManager.VERIFY_CODE_EXPIRE_MINUTES - 2));
                    Assert.IsTrue(u.AuthCodeExpireDateUtc < DateTime.UtcNow.AddMinutes(ApplicationUserManager.VERIFY_CODE_EXPIRE_MINUTES + 2));
                    Assert.AreEqual(0, u.AuthCodeFailedCount);
                });
            // セキュリティタイムスタンプが更新されること
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(userId))
                .ReturnsAsync(IdentityResult.Success).Callback((string a) => Assert.AreEqual(a, targetUser.Id));
            // 認証コードが発行されること
            string code = "testcode";
            mockUserManager.Setup(u => u.GenerateTwoFactorTokenAsync(userId, "PasswordResetSendCodeProvider"))
                .ReturnsAsync(code).Callback((string a, string b) => Assert.AreEqual(a, targetUser.Id));
            // 認証コードメールが送信されること
            mockUserManager.Setup(u => u.NotifyTwoFactorTokenAsync(userId, "PasswordResetSendCodeProvider", code))
                .ReturnsAsync(IdentityResult.Success).Callback((string a, string b, string c) => Assert.AreEqual(a, targetUser.Id));
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.PasswordResetGuide(new PasswordResetGuideViewModel() { Email = fakeEmail });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("PasswordReset", result.ViewName);
            // TempDataに登録されたユーザーIDが正しいこと
            Assert.AreEqual("66f50c73-cd05-4679-9127-08af2e4f9f0e", result.TempData["UserId"]);

            Assert.AreNotEqual(DateTime.MinValue, targetUser.AuthCodeExpireDateUtc);
            Assert.AreEqual(0, targetUser.AuthCodeFailedCount);
        }

        [TestMethod]
        public void PasswordReset_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("PasswordReset");
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void PasswordReset_異常系_ユーザー情報が取れない場合()
        {
            target.TempData["UserId"] = "a";

            mockUserManager.Setup(s => s.FindByIdAsync("a")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.PasswordReset(new PasswordResetViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void PasswordReset_異常系_ユーザーが利用開始前状態の場合()
        {
            target.TempData["UserId"] = "a";

            mockUserManager.Setup(s => s.FindByIdAsync("a")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.PasswordReset(new PasswordResetViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void PasswordReset_異常系_認証コードが既に無効になっている場合()
        {
            target.TempData["UserId"] = "a";

            var user = new CaradaIdUser
            {
                Id = "a",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByIdAsync("a")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(true);
            SetBaseField("_userManager", mockUserManager.Object);

            // 想定どおりのエラーがセットされていること
            var task = target.PasswordReset(new PasswordResetViewModel());
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

        }

        [TestMethod]
        public void PasswordReset_異常系_認証コードは不正で失敗回数許容範囲内の場合()
        {
            target.TempData["UserId"] = "a";

            var user = new CaradaIdUser
            {
                Id = "a",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByIdAsync("a")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("a", "PasswordResetSendCodeProvider", "12345")).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).Returns(Task.FromResult<bool>(false));
            SetBaseField("_userManager", mockUserManager.Object);

            // 想定どおりのエラーがセットされていること
            var task = target.PasswordReset(new PasswordResetViewModel { VerifyCode = "12345" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

        }

        [TestMethod]
        public void PasswordReset_異常系_認証コードは不正で失敗回数が無効化回数に到達した場合()
        {
            target.TempData["UserId"] = "a";

            var user = new CaradaIdUser
            {
                Id = "a",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByIdAsync("a")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("a", "PasswordResetSendCodeProvider", "12345")).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).Returns(Task.FromResult<bool>(true));
            SetBaseField("_userManager", mockUserManager.Object);

            // 想定どおりのエラーがセットされていること
            var task = target.PasswordReset(new PasswordResetViewModel { VerifyCode = "12345" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordReset_正常系_パスワードが変更される()
        {
            target.TempData["UserId"] = "a";

            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "userName"
            };

            mockUserManager.Setup(s => s.FindByIdAsync("a")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("a", "PasswordResetSendCodeProvider", "12345")).ReturnsAsync(true);
            mockUserManager.Setup(s => s.UpdateAsync(It.IsAny<CaradaIdUser>())).ReturnsAsync(IdentityResult.Success)
                .Callback((CaradaIdUser u) =>
                {
                    // 認証コードが初期化されること
                    Assert.IsNull(u.AuthCodeExpireDateUtc);
                    Assert.AreEqual(0, u.AuthCodeFailedCount);
                    // ログイン失敗回数、ロックアウト期間が初期化されること
                    Assert.AreEqual(0, u.AccessFailedCount);
                    Assert.IsNull(u.LockoutEndDateUtc);
                });
            mockUserManager.Setup(s => s.GeneratePasswordResetTokenAsync("a")).ReturnsAsync("token");
            mockUserManager.Setup(s => s.ResetPasswordAsync("a", "token", "newpassword")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync("a")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.SendEmailAsync("a",
                Resource.Mail.PasswordResetCompleteSubject,
                string.Format(Resource.Mail.PasswordResetCompleteBody, "userName", Resource.Mail.Support))).Returns(Task.FromResult<int>(0));
            SetBaseField("_userManager", mockUserManager.Object);

            // ログイン画面に遷移すること
            var task = target.PasswordReset(new PasswordResetViewModel { VerifyCode = "12345", ResetPassword = "newpassword" });
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("LoginView", result.RouteValues["Action"]);
            Assert.AreEqual("User", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void EmailResetView_正常系_初期表示_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("EmailResetView", 0);
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void EmailResetView_正常系_初期表示()
        {
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });

            var user = new CaradaIdUser()
            {
                UserName = "testUser",
                EmailConfirmed = true,
                Email = "test@test.com",
                Id = "userId"
            };
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var result = target.EmailResetView() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailReset", result.ViewName);
            // TempDataの確認
            var tempData = result.TempData["EmailReset"] as EmailResetViewModel;
            Assert.IsNull(tempData.CaradaId);
            Assert.IsNull(tempData.Password);
            Assert.IsNull(tempData.SecurityQuestionSelectId);
            Assert.IsNull(tempData.Answer);
            Assert.IsNull(tempData.Email);
        }

        [TestMethod]
        public void EmailResetView_正常系_初期表示_戻るボタンで遷移してきた場合()
        {
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });

            var user = new CaradaIdUser()
            {
                UserName = "testUser",
                EmailConfirmed = true,
                Email = "test@test.com",
                Id = "userId"
            };
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var EmailResetViewModel = new EmailResetViewModel();
            EmailResetViewModel.CaradaId = "testUser";
            EmailResetViewModel.Password = "testPassword";
            EmailResetViewModel.SecurityQuestionSelectId = 2;
            EmailResetViewModel.Answer = "testAnswer";
            EmailResetViewModel.Email = "test2@test.com";

            target.TempData["EmailReset"] = EmailResetViewModel;

            var result = target.EmailResetView() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailReset", result.ViewName);
            // TempDataの確認
            var tempData = result.TempData["EmailReset"] as EmailResetViewModel;
            Assert.AreEqual(EmailResetViewModel.CaradaId, tempData.CaradaId);
            Assert.IsNull(tempData.Password);
            Assert.IsNull(tempData.SecurityQuestionSelectId);
            Assert.IsNull(tempData.Answer);
            Assert.AreEqual(EmailResetViewModel.Email, tempData.Email);
        }

        [TestMethod]
        public void EmailReset_正常系_メールアドレス再設定_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("EmailReset", 1);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_ユーザーが取得できない場合()
        {
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = "test1234" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("LoginError"), target.ModelState["CaradaId"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_利用開始前ユーザーが取得できた場合()
        {
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = false });
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = "test1234" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("NotProcessedUseStart"), target.ModelState["CaradaId"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_パスワードの妥当性検証_LockedOutの場合()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "userName"
            };

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.LockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["CaradaId"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_パスワードの妥当性検証_AlreadyLockedOutの場合()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "userName"
            };

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.AlreadyLockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["CaradaId"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_パスワードの妥当性検証_Failureの場合()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "userName"
            };
            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.Failure);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("LoginError"), target.ModelState["CaradaId"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_秘密の質問の回答検証_答えがない場合()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "userName"
            };

            // 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.IsLockedOutAsync(user.Id)).ReturnsAsync(false);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifySecurityQuestionAnswer"), target.ModelState["Answer"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_秘密の質問の回答検証_答えが誤っている場合()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "test1234"
            };

            //// 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                // 秘密の答え１
                Answer = "2891b17e23d7ce6de42b2ecf4f8751a33f87eed37563bfab13db2104beab481d",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 2,
                // 秘密の答え２
                Answer = "3ab9b18eb8b29dbd3f1c47e6d19d18a6a39c42c499fc15ca350f1121cee5ad47",
                UserId = "a"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.IsLockedOutAsync(user.Id)).ReturnsAsync(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234", Answer = "秘密の答え１", SecurityQuestionSelectId = 2 });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["Answer"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_メールアドレスの検証_現在のメールアドレスと同じ場合()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "test1234"
            };

            // 秘密の質問の答えを設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                // 秘密の答え１
                Answer = "2891b17e23d7ce6de42b2ecf4f8751a33f87eed37563bfab13db2104beab481d",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 2,
                // 秘密の答え２
                Answer = "3ab9b18eb8b29dbd3f1c47e6d19d18a6a39c42c499fc15ca350f1121cee5ad47",
                UserId = "a"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.FindByEmailAsync("a@a.com")).ReturnsAsync(user);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234", Answer = "秘密の答え２", Email = "a@a.com", SecurityQuestionSelectId = 2 });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual("メールアドレスは現在のメールアドレスとは異なる値を入力してください。", target.ModelState["Email"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_異常系_メールアドレス再設定_メールアドレスの検証_別のユーザーが使用している場合()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "test1234"
            };

            var user2 = new CaradaIdUser
            {
                Id = "a2",
                Email = "a2@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "test2234"
            };

            // 秘密の質問の答えを設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                // 秘密の答え１
                Answer = "2891b17e23d7ce6de42b2ecf4f8751a33f87eed37563bfab13db2104beab481d",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 2,
                // 秘密の答え２
                Answer = "3ab9b18eb8b29dbd3f1c47e6d19d18a6a39c42c499fc15ca350f1121cee5ad47",
                UserId = "a"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.FindByEmailAsync("a@a.com")).ReturnsAsync(user2);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234", Answer = "秘密の答え２", Email = "a@a.com", SecurityQuestionSelectId = 2 });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("RegisteredMail"), target.ModelState["Email"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailReset_正常系_メールアドレス再設定()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "test1234"
            };

            // 秘密の質問の答えを設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                // 秘密の答え１
                Answer = "2891b17e23d7ce6de42b2ecf4f8751a33f87eed37563bfab13db2104beab481d",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 2,
                // 秘密の答え２
                Answer = "3ab9b18eb8b29dbd3f1c47e6d19d18a6a39c42c499fc15ca350f1121cee5ad47",
                UserId = "a"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.FindByEmailAsync("a@a.com")).ReturnsAsync(null);
            // 更新されること
            mockUserManager.Setup(s => s.UpdateAsync(user)).ReturnsAsync(IdentityResult.Success);
            // セキュリティタイムスタンプが更新されること
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(user.Id))
                .ReturnsAsync(IdentityResult.Success).Callback((string a) => Assert.AreEqual(a, user.Id));
            // 認証コードが発行されること
            var code = "testcode";
            mockUserManager.Setup(u => u.GenerateTwoFactorTokenAsync(It.IsAny<string>(), "EmailResetSendCodeProvider"))
                .ReturnsAsync(code).Callback((string a, string b) => Assert.AreEqual(a, user.Id));
            // 認証コードメールが送信されること
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage m) =>
                {
                    // メールの内容が正しいこと
                    Assert.AreEqual("a@a.com", m.Destination);
                    Assert.AreEqual(Mail.EmailResetSendCodeSubject, m.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailResetSendCodeBody.Replace(":SUPPORT:", Mail.Support), code), m.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234", Answer = "秘密の答え２", Email = "a@a.com", SecurityQuestionSelectId = 2 });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailResetVerifyCode", result.ViewName);
            // TempDataに登録されたメールアドレスが正しいこと
            var tempData = result.TempData["EmailReset"] as EmailResetViewModel;
            Assert.AreEqual("a@a.com", tempData.Email);
            Assert.AreEqual("test1234", tempData.CaradaId);

            Assert.AreNotEqual(DateTime.MinValue, user.AuthCodeExpireDateUtc);
            Assert.AreEqual(0, user.AuthCodeFailedCount);

            mockEmailService.VerifyAll();
        }

        [TestMethod]
        public void EmailReset_正常系_メールアドレス再設定_指定したEmailから取得したユーザー情報のEmailConfirmedが0()
        {
            var user = new CaradaIdUser
            {
                Id = "a",
                Email = "b@b.com",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "test1234"
            };

            var user2 = new CaradaIdUser
            {
                Id = "a",
                Email = "a@a.com",
                EmailConfirmed = false,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "test1234"
            };

            // 秘密の質問の答えを設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                // 秘密の答え１
                Answer = "2891b17e23d7ce6de42b2ecf4f8751a33f87eed37563bfab13db2104beab481d",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 2,
                // 秘密の答え２
                Answer = "3ab9b18eb8b29dbd3f1c47e6d19d18a6a39c42c499fc15ca350f1121cee5ad47",
                UserId = "a"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "pass1234")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.FindByEmailAsync("a@a.com")).ReturnsAsync(user2);
            // 更新されること
            mockUserManager.Setup(s => s.UpdateAsync(user)).ReturnsAsync(IdentityResult.Success);
            // セキュリティタイムスタンプが更新されること
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(user.Id))
                .ReturnsAsync(IdentityResult.Success).Callback((string a) => Assert.AreEqual(a, user.Id));
            // 認証コードが発行されること
            var code = "testcode";
            mockUserManager.Setup(u => u.GenerateTwoFactorTokenAsync(It.IsAny<string>(), "EmailResetSendCodeProvider"))
                .ReturnsAsync(code).Callback((string a, string b) => Assert.AreEqual(a, user.Id));
            // 認証コードメールが送信されること
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage m) =>
                {
                    // メールの内容が正しいこと
                    Assert.AreEqual("a@a.com", m.Destination);
                    Assert.AreEqual(Mail.EmailResetSendCodeSubject, m.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailResetSendCodeBody.Replace(":SUPPORT:", Mail.Support), code), m.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailReset(new EmailResetViewModel() { CaradaId = user.UserName, Password = "pass1234", Answer = "秘密の答え２", Email = "a@a.com", SecurityQuestionSelectId = 2 });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailResetVerifyCode", result.ViewName);
            // TempDataに登録されたメールアドレスが正しいこと
            var tempData = result.TempData["EmailReset"] as EmailResetViewModel;
            Assert.AreEqual("a@a.com", tempData.Email);
            Assert.AreEqual("test1234", tempData.CaradaId);

            Assert.AreNotEqual(DateTime.MinValue, user.AuthCodeExpireDateUtc);
            Assert.AreEqual(0, user.AuthCodeFailedCount);

            mockEmailService.VerifyAll();
        }

        [TestMethod]
        public void EmailResetVerifyCode_正常系_重複メアド未登録_メールアドレスが変更される()
        {
            var fakeId = "a";
            var fakeEmail = "a@a.com";
            var fakeOldEmail = "oldmailaddress@exapmle.com";
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            mockEmailResetModel.Object.Email = fakeEmail;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailResetSendCodeProvider", fakeVerifyCode)).ReturnsAsync(true);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeEmail)).ReturnsAsync(null);
            mockUserManager.Setup(s => s.UpdateAsync(It.IsAny<CaradaIdUser>())).ReturnsAsync(IdentityResult.Success)
                .Callback((CaradaIdUser u) =>
                {
                    // Emailアドレスが更新されること
                    Assert.AreEqual(fakeEmail, u.Email);
                    // 認証コードが初期化されること
                    Assert.IsNull(u.AuthCodeExpireDateUtc);
                    Assert.AreEqual(0, u.AuthCodeFailedCount);
                });
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(fakeId)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.SendEmailAsync(fakeId,
                Mail.EmailResetCompleteSubject,
                string.Format(Mail.EmailResetCompleteBody, fakeUserName, fakeEmail, Mail.Support))).Returns(Task.FromResult<int>(0));
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage message) =>
                {
                    Assert.AreEqual(fakeOldEmail, message.Destination);
                    Assert.AreEqual(Mail.EmailResetCompleteSubject, message.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailResetCompleteBody, fakeUserName, fakeEmail, Mail.Support), message.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin(fakeId, fakeUserName)).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "StorageKey" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);

            // ログイン画面に遷移すること
            var task = target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode });
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("LoginView", result.RouteValues["Action"]);
            Assert.AreEqual("User", result.RouteValues["controller"]);

            // Cookieの確認
            var cookie = target.Response.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("StorageKey", cookie.Value);

            mockEmailResetModel.VerifyAll();
            mockEmailService.VerifyAll();
            mockDbContext.VerifyAll();
            mockUserManager.VerifyAll();
            mockTwoFactorLogin.VerifyAll();
        }

        [TestMethod]
        public void EmailResetVerifyCode_正常系_認証前重複メアドあり_メールアドレスが変更される()
        {
            var fakeId = "a";
            var fakeEmail = "a@a.com";
            var fakeOldEmail = "oldmailaddress@exapmle.com";
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            mockEmailResetModel.Object.Email = fakeEmail;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;

            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var duplicatedEmailUser = new CaradaIdUser
            {
                Id = "b",
                Email = fakeEmail,
                EmailConfirmed = false,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "duplicatedEmailUser"
            };
            mockDbContext.Setup(s => s.Users).Returns(new TestAspNetUsersDbSet());
            mockDbContext.Object.Users.Add(user);
            mockDbContext.Object.Users.Add(duplicatedEmailUser);

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailResetSendCodeProvider", fakeVerifyCode)).ReturnsAsync(true);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeEmail)).ReturnsAsync(duplicatedEmailUser);

            mockUserManager.SetupSequence(s => s.UpdateAsync(It.IsAny<CaradaIdUser>()))
                .Returns(Task.FromResult(IdentityResult.Success))
                .Returns(Task.FromResult(IdentityResult.Success));
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(fakeId)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.SendEmailAsync(fakeId,
                Mail.EmailResetCompleteSubject,
                string.Format(Mail.EmailResetCompleteBody, fakeUserName, fakeEmail, Mail.Support))).Returns(Task.FromResult<int>(0));
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage message) =>
                {
                    Assert.AreEqual(fakeOldEmail, message.Destination);
                    Assert.AreEqual(Mail.EmailResetCompleteSubject, message.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailResetCompleteBody, fakeUserName, fakeEmail, Mail.Support), message.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin(fakeId, fakeUserName)).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "StorageKey" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);

            // ログイン画面に遷移すること
            var task = target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode });
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("LoginView", result.RouteValues["Action"]);
            Assert.AreEqual("User", result.RouteValues["controller"]);

            // Emailアドレスが更新されること
            Assert.IsNull(duplicatedEmailUser.Email);
            Assert.AreEqual(fakeEmail, user.Email);
            // 認証コードが初期化されること
            Assert.IsNull(user.AuthCodeExpireDateUtc);
            Assert.AreEqual(0, user.AuthCodeFailedCount);

            // Cookieの確認
            var cookie = target.Response.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("StorageKey", cookie.Value);

            mockEmailResetModel.VerifyAll();
            mockEmailService.VerifyAll();
            mockDbContext.VerifyAll();
            mockUserManager.VerifyAll();
            mockTwoFactorLogin.VerifyAll();
        }

        [TestMethod]
        public void EmailResetVerifyCode_異常系_TempDataなし_システムエラー()
        {
            var fakeVerifyCode = "12345";
            var task = target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode });
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void EmailResetVerifyCode_異常系_ユーザ未登録_システムエラー()
        {
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(null);

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode })).Wait();
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.InnerException.GetType(), typeof(HttpException));
                Assert.AreEqual(e.InnerException.Message, "This user is unregistered or already deleted.");
            }
        }

        [TestMethod]
        public void EmailResetVerifyCode_異常系_認証前ユーザ_システムエラー()
        {
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";
            var fakeId = "a";
            var fakeOldEmail = "oldmailaddress@exapmle.com";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;

            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = false,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode })).Wait();
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.InnerException.GetType(), typeof(HttpException));
                Assert.AreEqual(e.InnerException.Message, "This user is unregistered or already deleted.");
            }
        }

        [TestMethod]
        public void EmailResetVerifyCode_異常系_認証コード無効化済_バリデーションエラー表示()
        {
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";
            var fakeId = "a";
            var fakeOldEmail = "oldmailaddress@exapmle.com";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;

            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockEmailResetModel.VerifyAll();
        }

        [TestMethod]
        public void EmailResetVerifyCode_異常系_認証コード無効化_バリデーションエラー表示()
        {
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";
            var fakeId = "a";
            var fakeOldEmail = "oldmailaddress@exapmle.com";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;

            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailResetSendCodeProvider", fakeVerifyCode)).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).ReturnsAsync(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockEmailResetModel.VerifyAll();
        }

        [TestMethod]
        public void EmailResetVerifyCode_異常系_認証コード入力失敗_バリデーションエラー表示()
        {
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";
            var fakeId = "a";
            var fakeOldEmail = "oldmailaddress@exapmle.com";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;

            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailResetSendCodeProvider", fakeVerifyCode)).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).ReturnsAsync(false);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockEmailResetModel.VerifyAll();
        }

        [TestMethod]
        public void EmailResetVerifyCode_異常系_認証済重複メアドあり_システムエラー()
        {
            var fakeId = "a";
            var fakeEmail = "a@a.com";
            var fakeOldEmail = "oldmailaddress@exapmle.com";
            var fakeUserName = "userName";
            var fakeVerifyCode = "12345";

            var mockEmailResetModel = new Mock<EmailResetViewModel>();
            mockEmailResetModel.Object.CaradaId = fakeUserName;
            mockEmailResetModel.Object.Email = fakeEmail;
            target.TempData["EmailReset"] = mockEmailResetModel.Object;

            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var duplicatedEmailUser = new CaradaIdUser
            {
                Id = "b",
                Email = fakeEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 1,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "duplicatedEmailUser"
            };
            mockDbContext.Setup(s => s.Users).Returns(new TestAspNetUsersDbSet());
            mockDbContext.Object.Users.Add(user);
            mockDbContext.Object.Users.Add(duplicatedEmailUser);

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailResetSendCodeProvider", fakeVerifyCode)).ReturnsAsync(true);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeEmail)).ReturnsAsync(duplicatedEmailUser);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailResetVerifyCode(new EmailResetVerifyCodeViewModel { VerifyCode = fakeVerifyCode })).Wait();
                Assert.Fail();
            }
            catch (Exception e)
            {
                Assert.AreEqual(e.InnerException.GetType(), typeof(HttpException));
                Assert.AreEqual(e.InnerException.Message, "This email address is already confirmed.");
            }

            mockEmailResetModel.VerifyAll();
        }

        [TestMethod]
        public void PasswordChangeView_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("PasswordChangeView", 0);
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void PasswordChangeView_正常系_初期表示()
        {
            var result = target.PasswordChangeView() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("PasswordChange", result.ViewName);
        }

        [TestMethod]
        public void PasswordChange_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("PasswordChange", 1);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void PasswordChange_異常系_ユーザー情報が取得できない()
        {
            mockUserManager.Setup(s => s.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.PasswordChange(new PasswordChangeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void PasswordChange_異常系_ユーザーが利用開始前状態の場合()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = false
            };

            mockUserManager.Setup(s => s.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.PasswordChange(new PasswordChangeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void PasswordChange_異常系_ロックアウト中()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.AlreadyLockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.PasswordChange(new PasswordChangeViewModel() { CurrentPassword = "password" });
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["CurrentPassword"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChange_異常系_現在のパスワード不一致だがロックアウトはされない()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Failure);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.PasswordChange(new PasswordChangeViewModel() { CurrentPassword = "password" });
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("PasswordMismatch"), target.ModelState["CurrentPassword"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChange_異常系_現在のパスワード不一致でロックアウト開始()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.LockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.PasswordChange(new PasswordChangeViewModel() { CurrentPassword = "password" });
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["CurrentPassword"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChange_正常系_パスワードが変更されること()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                Email = "a@a.com",
                EmailConfirmed = true,
                UserName = "testUser"
            };

            mockUserManager.Setup(s => s.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.ResetAccessFailedCountAsync("id")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.GeneratePasswordResetTokenAsync("id")).ReturnsAsync("token");
            mockUserManager.Setup(s => s.ResetPasswordAsync("id", "token", "newpassword")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync("id")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.SendEmailAsync("id",
                Resource.Mail.PasswordChangeCompleteSubject,
                string.Format(Resource.Mail.PasswordChangeCompleteBody, "testUser", Resource.Mail.Support))).Returns(Task.FromResult<int>(0));
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.PasswordChange(new PasswordChangeViewModel()
            {
                CurrentPassword = "password",
                NewPassword = "newpassword"
            });
            var result = task.Result as RedirectToRouteResult;

            // アカウント管理画面に遷移すること
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.RouteValues.Count);
            Assert.AreEqual("Index", result.RouteValues["Action"]);
        }

        [TestMethod]
        public void EmailChangeView_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("EmailChangeView", 0);
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void EmailChangeView_異常系_ユーザーが取得できない場合()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(null);

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailChangeView()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void EmailChangeView_異常系_ユーザーが利用開始登録前の場合()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailChangeView()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void EmailChangeView_正常系_TempDataなし()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = "b@b.com" });

            SetBaseField("_userManager", mockUserManager.Object);

            var result = target.EmailChangeView().Result as ViewResult;
            var model = result.Model as EmailChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("EmailChange", result.ViewName);
            Assert.IsNotNull(model);
            Assert.IsNull(model.NewEmail);
            Assert.IsTrue(target.ModelState.IsValid);

            Assert.IsNull(model.NewEmail);
        }

        [TestMethod]
        public void EmailChangeView_正常系_TempDataあり()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = "b@b.com" });

            SetBaseField("_userManager", mockUserManager.Object);
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = "c@c.com"
            };
            target.TempData["EmailChange"] = emailChangeTemp;

            var result = target.EmailChangeView().Result as ViewResult;
            var model = result.Model as EmailChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("EmailChange", result.ViewName);
            Assert.IsNotNull(model);
            Assert.AreEqual("c@c.com", model.NewEmail);
            Assert.IsTrue(target.ModelState.IsValid);
        }

        [TestMethod]
        public void EmailChange_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("EmailChange", 1);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
        }

        [TestMethod]
        public void EmailChange_異常系_ユーザーが取得できない場合()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(null);

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailChange(new EmailChangeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void EmailChange_異常系_ユーザーが利用開始登録前の場合()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailChange(new EmailChangeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void EmailChange_異常系_現在のメールアドレスと同じ()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = "b@b.com" });

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailChange(new EmailChangeViewModel { NewEmail = "b@b.com" });
            var result = task.Result as ViewResult;
            var model = result.Model as EmailChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(model);

            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual("新しいメールアドレスは現在のメールアドレスとは異なる値を入力してください。",
                target.ModelState["NewEmail"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
        }

        [TestMethod]
        public void EmailChange_異常系_現在のメールアドレスと大文字小文字違い()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("fakeName");
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = "b@b.com" });

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailChange(new EmailChangeViewModel { NewEmail = "B@B.COM" });
            var result = task.Result as ViewResult;
            var model = result.Model as EmailChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(model);

            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual("新しいメールアドレスは現在のメールアドレスとは異なる値を入力してください。",
                target.ModelState["NewEmail"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
        }

        [TestMethod]
        public void EmailChange_異常系_ロックアウト中()
        {
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("fakeName");
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true,
                Email = "b@b.com"
            };
            mockUserManager.Setup(s => s.FindByNameAsync("fakeName")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password")).ReturnsAsync(PasswordValidationStatus.AlreadyLockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var model = new EmailChangeViewModel()
            {
                NewEmail = "aaa@a.com",
                Password = "password"
            };
            var task = target.EmailChange(model);
            var result = task.Result as ViewResult;

            var resultModel = result.Model as EmailChangeViewModel;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(resultModel);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["Password"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
        }

        [TestMethod]
        public void EmailChange_異常系_パスワード不一致だがロックアウトはされない()
        {
            var fakeUserName = "testuser";
            var fakePassword = "password";
            var fakeNewEmail = "aaa@a.com";
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true,
                Email = "b@b.com"
            };
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, fakePassword)).ReturnsAsync(PasswordValidationStatus.Failure);
            SetBaseField("_userManager", mockUserManager.Object);

            var model = new EmailChangeViewModel()
            {
                NewEmail = fakeNewEmail,
                Password = fakePassword
            };
            var task = target.EmailChange(model);
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            var resultModel = result.Model as EmailChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(resultModel);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("PasswordMismatch"), target.ModelState["Password"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
        }

        [TestMethod]
        public void EmailChange_異常系_パスワード不一致でロックアウト開始()
        {
            var fakeUserName = "testuser";
            var fakePassword = "password";
            var fakeNewEmail = "aaa@a.com";
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true,
                Email = "b@b.com"
            };

            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, fakePassword)).ReturnsAsync(PasswordValidationStatus.LockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var model = new EmailChangeViewModel()
            {
                NewEmail = fakeNewEmail,
                Password = fakePassword
            };
            var task = target.EmailChange(model);
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            var resultModel = result.Model as EmailChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(resultModel);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["Password"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
        }

        [TestMethod]
        public void EmailChange_異常系_他の利用開始登録済ユーザーが新しいメールアドレスを使用済み()
        {
            var fakeUserName = "testuser";
            var fakePassword = "password";
            var fakeNewEmail = "aaa@a.com";
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true,
                Email = "b@b.com"
            };
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, fakePassword)).ReturnsAsync(PasswordValidationStatus.Success);
            var otherUser = new CaradaIdUser()
            {
                Id = "id2",
                EmailConfirmed = true
            };

            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(otherUser);
            SetBaseField("_userManager", mockUserManager.Object);

            var model = new EmailChangeViewModel()
            {
                NewEmail = fakeNewEmail,
                Password = fakePassword
            };
            var task = target.EmailChange(model);
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            var resultModel = result.Model as EmailChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(resultModel);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("RegisteredMail"), target.ModelState["NewEmail"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
        }

        [TestMethod]
        public void EmailChange_正常系_新しいメールアドレスを保持する利用開始登録前のユーザーがいない場合()
        {
            var fakeUserName = "testuser";
            var fakePassword = "password";
            var fakeNewEmail = "aaa@a.com";
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true,
                Email = "b@b.com",
            };
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, fakePassword)).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.ResetAccessFailedCountAsync(user.Id)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            var model = new EmailChangeViewModel()
            {
                NewEmail = fakeNewEmail,
                Password = fakePassword
            };

            var task = target.EmailChange(model);
            var result = task.Result as ViewResult;
            // 確認画面のviewであること
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailChangeConfirm", result.ViewName);
            Assert.IsTrue(target.ModelState.IsValid);

            // TempDataが作成されていること
            var tempData = result.TempData["EmailChange"] as EmailChangeViewModel;
            Assert.AreEqual(fakeNewEmail, tempData.NewEmail);
        }

        [TestMethod]
        public void EmailChange_正常系_新しいメールアドレスを保持する利用開始登録前のユーザーがいる場合()
        {
            var fakeUserName = "testuser";
            var fakePassword = "password";
            var fakeNewEmail = "aaa@a.com";
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true,
                Email = "b@b.com"
            };
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, fakePassword)).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.ResetAccessFailedCountAsync(user.Id)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });
            SetBaseField("_userManager", mockUserManager.Object);

            var model = new EmailChangeViewModel()
            {
                NewEmail = fakeNewEmail,
                Password = fakePassword
            };

            var task = target.EmailChange(model);
            var result = task.Result as ViewResult;
            // 確認画面のviewであること
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailChangeConfirm", result.ViewName);
            Assert.IsTrue(target.ModelState.IsValid);

            // TempDataが作成されていること
            var tempData = result.TempData["EmailChange"] as EmailChangeViewModel;
            Assert.AreEqual(fakeNewEmail, tempData.NewEmail);
        }

        [TestMethod]
        public void EmailChangeConfirm_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("EmailChangeConfirm", 0);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
        }

        [TestMethod]
        public void EmailChangeConfirm_異常系_TempDataなし_システムエラー()
        {
            var task = target.EmailChangeConfirm();
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void EmailChangeConfirm_異常系_ユーザーが取得できない場合()
        {
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = "test@test.co.jp"
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(null);

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailChangeConfirm()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void EmailChangeConfirm_異常系_ユーザーが利用開始登録前の場合()
        {
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = "aaa@aaa.co.jp"
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = "bbb@bbb.co.jp" });

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailChangeConfirm()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void EmailChangeConfirm_異常系_メールアドレスに紐づくユーザーが利用開始登録済み()
        {
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = "aaa@aaa.co.jp"
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = "bbb@bbb.co.jp" });
            mockUserManager.Setup(s => s.FindByEmailAsync("aaa@aaa.co.jp")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true });

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.EmailChangeConfirm()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void EmailChangeConfirm_正常系_新しいメールアドレスに紐づくユーザーが利用開始登録前()
        {
            var user = new CaradaIdUser
            {
                Id = "testId",
                Email = "bbb@bbb.co.jp",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "testuser"
            };
            
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = "aaa@aaa.co.jp"
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync("aaa@aaa.co.jp")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });
            // 更新されること
            mockUserManager.Setup(s => s.UpdateAsync(user)).ReturnsAsync(IdentityResult.Success);
            // セキュリティタイムスタンプが更新されること
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(user.Id))
                .ReturnsAsync(IdentityResult.Success).Callback((string a) => Assert.AreEqual(a, user.Id));
            // 認証コードが発行されること
            var code = "testcode";
            mockUserManager.Setup(u => u.GenerateTwoFactorTokenAsync(It.IsAny<string>(), "EmailChangeSendCodeProvider"))
                .ReturnsAsync(code).Callback((string a, string b) => Assert.AreEqual(a, user.Id));
            // 認証コードメールが送信されること
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage m) =>
                {
                    // メールの内容が正しいこと
                    Assert.AreEqual("aaa@aaa.co.jp", m.Destination);
                    Assert.AreEqual(Mail.EmailChangeSendCodeSubject, m.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailChangeSendCodeBody.Replace(":SUPPORT:", Mail.Support), code), m.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailChangeConfirm();
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailChangeVerifyCode", result.ViewName);
            Assert.AreNotEqual(DateTime.MinValue, user.AuthCodeExpireDateUtc);
            Assert.AreEqual(0, user.AuthCodeFailedCount);
            var tempData = result.TempData["EmailChange"] as EmailChangeViewModel;
            Assert.AreEqual("aaa@aaa.co.jp", tempData.NewEmail);

            mockEmailService.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeConfirm_正常系_新しいメールアドレスに紐づくユーザーがいない()
        {
            var user = new CaradaIdUser
            {
                Id = "testId",
                Email = "bbb@bbb.co.jp",
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = "testuser"
            };

            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = "aaa@aaa.co.jp"
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync("aaa@aaa.co.jp")).ReturnsAsync(null);
            // 更新されること
            mockUserManager.Setup(s => s.UpdateAsync(user)).ReturnsAsync(IdentityResult.Success);
            // セキュリティタイムスタンプが更新されること
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(user.Id))
                .ReturnsAsync(IdentityResult.Success).Callback((string a) => Assert.AreEqual(a, user.Id));
            // 認証コードが発行されること
            var code = "testcode";
            mockUserManager.Setup(u => u.GenerateTwoFactorTokenAsync(It.IsAny<string>(), "EmailChangeSendCodeProvider"))
                .ReturnsAsync(code).Callback((string a, string b) => Assert.AreEqual(a, user.Id));
            // 認証コードメールが送信されること
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage m) =>
                {
                    // メールの内容が正しいこと
                    Assert.AreEqual("aaa@aaa.co.jp", m.Destination);
                    Assert.AreEqual(Mail.EmailChangeSendCodeSubject, m.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailChangeSendCodeBody.Replace(":SUPPORT:", Mail.Support), code), m.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailChangeConfirm();
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("EmailChangeVerifyCode", result.ViewName);
            Assert.AreNotEqual(DateTime.MinValue, user.AuthCodeExpireDateUtc);
            Assert.AreEqual(0, user.AuthCodeFailedCount);
            var tempData = result.TempData["EmailChange"] as EmailChangeViewModel;
            Assert.AreEqual("aaa@aaa.co.jp", tempData.NewEmail);

            mockEmailService.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("EmailChangeVerifyCode", 1);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
        }

        [TestMethod]
        public void EmailChangeVerifyCode_正常系_新しいメールアドレスに紐づく利用開始登録済ユーザなし()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeOldEmail = "bbb@bbb.co.jp";
            var fakeId = "testId";
            var fakeUserName = "testuser";
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(null);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailChangeSendCodeProvider", fakeVerifyCode)).ReturnsAsync(true);
            mockUserManager.Setup(s => s.UpdateAsync(It.IsAny<CaradaIdUser>())).ReturnsAsync(IdentityResult.Success)
                .Callback((CaradaIdUser u) =>
                {
                    // Emailアドレスが更新されること
                    Assert.AreEqual(fakeNewEmail, u.Email);
                    // 認証コードが初期化されること
                    Assert.IsNull(u.AuthCodeExpireDateUtc);
                    Assert.AreEqual(0, u.AuthCodeFailedCount);
                });
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(fakeId)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.SendEmailAsync(fakeId,
                Mail.EmailChangeCompleteSubject,
                string.Format(Mail.EmailChangeCompleteBody, fakeUserName, fakeNewEmail, Mail.Support))).Returns(Task.FromResult<int>(0));
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage message) =>
                {
                    Assert.AreEqual(fakeOldEmail, message.Destination);
                    Assert.AreEqual(Mail.EmailChangeCompleteSubject, message.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailChangeCompleteBody, fakeUserName, fakeNewEmail, Mail.Support), message.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            // アカウント管理画面に遷移すること
            var task = target.EmailChangeVerifyCode(mockModel.Object);
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            // TempDataがクリアされること
            Assert.IsNull(target.TempData["EmailChange"]);

            mockModel.VerifyAll();
            mockEmailService.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_正常系_新しいメールアドレスに紐づく利用開始登録前ユーザあり()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeOldEmail = "bbb@bbb.co.jp";
            var fakeId = "testId";
            var fakeUserName = "testuser";
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var duplicatedEmailUser = new CaradaIdUser
            {
                Id = "duplicatedUser",
                Email = fakeOldEmail,
                EmailConfirmed = false,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(duplicatedEmailUser);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailChangeSendCodeProvider", fakeVerifyCode)).ReturnsAsync(true);
            mockUserManager.SetupSequence(s => s.UpdateAsync(It.IsAny<CaradaIdUser>()))
                .Returns(Task.FromResult(IdentityResult.Success))
                .Returns(Task.FromResult(IdentityResult.Success));
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(fakeId)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.SendEmailAsync(fakeId,
                Mail.EmailChangeCompleteSubject,
                string.Format(Mail.EmailChangeCompleteBody, fakeUserName, fakeNewEmail, Mail.Support))).Returns(Task.FromResult<int>(0));
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage message) =>
                {
                    Assert.AreEqual(fakeOldEmail, message.Destination);
                    Assert.AreEqual(Mail.EmailChangeCompleteSubject, message.Subject);
                    Assert.AreEqual(string.Format(Mail.EmailChangeCompleteBody, fakeUserName, fakeNewEmail, Mail.Support), message.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            // アカウント管理画面に遷移すること
            var task = target.EmailChangeVerifyCode(mockModel.Object);
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            // TempDataがクリアされること
            Assert.IsNull(target.TempData["EmailChange"]);

            // Emailアドレスが更新されること
            Assert.IsNull(duplicatedEmailUser.Email);
            Assert.AreEqual(fakeNewEmail, user.Email);
            // 認証コードが初期化されること
            Assert.IsNull(user.AuthCodeExpireDateUtc);
            Assert.AreEqual(0, user.AuthCodeFailedCount);

            mockModel.VerifyAll();
            mockEmailService.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_異常系_TempDataなし_システムエラー()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;

            var task = target.EmailChangeVerifyCode(mockModel.Object);
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);

            mockModel.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_異常系_ユーザ情報なし_システムエラー()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeUserName = "testuser";

            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);
            try
            {
                Task.Run(() => target.EmailChangeVerifyCode(mockModel.Object)).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
                Assert.AreEqual(e.InnerException.Message, "This user is unregistered or already deleted.");
            }
            mockModel.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_異常系_利用開始登録前_システムエラー()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeOldEmail = "bbb@bbb.co.jp";
            var fakeId = "testId";
            var fakeUserName = "testuser";
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = false,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };

            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            SetBaseField("_userManager", mockUserManager.Object);
            try
            {
                Task.Run(() => target.EmailChangeVerifyCode(mockModel.Object)).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
                Assert.AreEqual(e.InnerException.Message, "This user is unregistered or already deleted.");
            }
            mockModel.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_異常系_利用開始登録済と重複_システムエラー()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeOldEmail = "bbb@bbb.co.jp";
            var fakeId = "testId";
            var fakeUserName = "testuser";
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var duplicatedEmailUser = new CaradaIdUser
            {
                Id = "duplicatedUser",
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;
            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(duplicatedEmailUser);
            SetBaseField("_userManager", mockUserManager.Object);
            try
            {
                Task.Run(() => target.EmailChangeVerifyCode(mockModel.Object)).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
                Assert.AreEqual(e.InnerException.Message, "This email address is already confirmed.");
            }
            mockModel.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_異常系_認証コード無効化済_エラー表示()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeOldEmail = "bbb@bbb.co.jp";
            var fakeId = "testId";
            var fakeUserName = "testuser";
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(null);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(true);

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailChangeVerifyCode(mockModel.Object);
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            var resultModel = result.Model as EmailChangeVerifyCodeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(resultModel);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
            mockModel.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_異常系_認証コード無効化_エラー表示()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeOldEmail = "bbb@bbb.co.jp";
            var fakeId = "testId";
            var fakeUserName = "testuser";
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(null);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailChangeSendCodeProvider", fakeVerifyCode)).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).ReturnsAsync(true);

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailChangeVerifyCode(mockModel.Object);
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            var resultModel = result.Model as EmailChangeVerifyCodeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(resultModel);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);
            Assert.AreEqual(1, target.ModelState.Count);
            mockModel.VerifyAll();
        }

        [TestMethod]
        public void EmailChangeVerifyCode_異常系_認証コード入力失敗_エラー表示()
        {
            var fakeVerifyCode = "123456";
            var mockModel = new Mock<EmailChangeVerifyCodeViewModel>();
            mockModel.Object.VerifyCode = fakeVerifyCode;
            var fakeNewEmail = "aaa@aaa.co.jp";
            var fakeOldEmail = "bbb@bbb.co.jp";
            var fakeId = "testId";
            var fakeUserName = "testuser";
            var user = new CaradaIdUser
            {
                Id = fakeId,
                Email = fakeOldEmail,
                EmailConfirmed = true,
                AuthCodeFailedCount = 1,
                AuthCodeExpireDateUtc = DateTime.UtcNow,
                AccessFailedCount = 4,
                LockoutEndDateUtc = DateTime.UtcNow,
                UserName = fakeUserName
            };
            var emailChangeTemp = new EmailChangeViewModel
            {
                NewEmail = fakeNewEmail
            };
            target.TempData["EmailChange"] = emailChangeTemp;

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns(fakeUserName);
            mockUserManager.Setup(s => s.FindByNameAsync(fakeUserName)).ReturnsAsync(user);
            mockUserManager.Setup(s => s.FindByEmailAsync(fakeNewEmail)).ReturnsAsync(null);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync(fakeId, "EmailChangeSendCodeProvider", fakeVerifyCode)).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).ReturnsAsync(false);

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.EmailChangeVerifyCode(mockModel.Object);
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            var resultModel = result.Model as EmailChangeVerifyCodeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(resultModel);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);
            Assert.AreEqual(target.ModelState.Count, 1);
            mockModel.VerifyAll();
        }

        [TestMethod]
        public void SecurityQuestionChangeView_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("SecurityQuestionChangeView", 0);
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void SecurityQuestionChangeView_異常系_ユーザーが取得できない場合()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(null);

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionChangeView()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChangeView_異常系_ユーザーが利用開始登録前の場合()
        {
            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionChangeView()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChangeView_異常系_秘密の質問の回答が取得できない()
        {
            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            // 秘密の質問の回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = true, Id = "userId" });
            SetBaseField("_userManager", mockUserManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testuser");
            try
            {
                Task.Run(() => target.SecurityQuestionChangeView()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChangeView_異常系_秘密の質問の回答が2件以上存在する()
        {
            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            //// 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答1",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答2",
                UserId = "userId"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = true, Id = "userId" });
            SetBaseField("_userManager", mockUserManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testuser");
            try
            {
                Task.Run(() => target.SecurityQuestionChangeView()).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChangeView_正常系_秘密の質問変更画面初期表示()
        {
            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            //// 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答",
                UserId = "userId"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(new CaradaIdUser() { EmailConfirmed = true, Id = "userId" });
            SetBaseField("_userManager", mockUserManager.Object);

            MvcMockHelpers.MockIdentity.Setup(i => i.Name).Returns("testuser");

            var result = target.SecurityQuestionChangeView().Result as ViewResult;
            var model = result.Model as SecurityQuestionChangeViewModel;

            Assert.IsNotNull(result);
            Assert.AreEqual("SecurityQuestionChange", result.ViewName);
            Assert.IsNotNull(model);
            Assert.IsNotNull(model.SecurityQuestionSelectId);
            Assert.IsNull(model.Answer);
            Assert.IsTrue(target.ModelState.IsValid);
        }

        [TestMethod]
        public void SecurityQuestionChange_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserSettingsController>("SecurityQuestionChange", 1);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_選択した秘密の質問が取得できない場合()
        {
            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var task = target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 2
                    });
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SecurityQuestionError"), target.ModelState["SecurityQuestionSelectId"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_ユーザーが取得できない場合()
        {
            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(null);

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 1
                    })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_ユーザーが利用開始登録前の場合()
        {
            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });

            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 1
                    })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_ロックアウト中()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });

            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password01")).ReturnsAsync(PasswordValidationStatus.AlreadyLockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 1,
                        Password = "password01"
                    });
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["Password"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_現在のパスワード不一致だがロックアウトはされない()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });

            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password01")).ReturnsAsync(PasswordValidationStatus.Failure);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 1,
                        Password = "password01"
                    });
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("PasswordMismatch"), target.ModelState["Password"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_現在のパスワード不一致でロックアウト開始()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });

            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password01")).ReturnsAsync(PasswordValidationStatus.LockedOut);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 1,
                        Password = "password01"
                    });
            var result = task.Result as ViewResult;

            // 想定どおりのエラーがセットされていること
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SettingsLockOut"), target.ModelState["Password"].Errors[0].ErrorMessage);
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_秘密の質問の回答が取得できない()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            // 秘密の質問の回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password01")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.ResetAccessFailedCountAsync("id")).ReturnsAsync(IdentityResult.Success);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 1,
                        Password = "password01"
                    })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChange_異常系_秘密の質問の回答が2件以上存在する()
        {
            var user = new CaradaIdUser()
            {
                Id = "id",
                EmailConfirmed = true
            };

            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問",
                DisplayOrder = 1
            });
            // 秘密の質問の回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答1",
                UserId = "userId"
            });
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答2",
                UserId = "userId"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password01")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.ResetAccessFailedCountAsync("id")).ReturnsAsync(IdentityResult.Success);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 1,
                        Password = "password01"
                    })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
        }

        [TestMethod]
        public void SecurityQuestionChange_正常系_秘密の質問変更()
        {
            var user = new CaradaIdUser()
            {
                Id = "userId",
                EmailConfirmed = true
            };

            // 秘密の質問を設定する
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            var masterData = mockDbContext.Object.SecurityQuestionMasters;
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "秘密の質問1",
                DisplayOrder = 1
            });
            masterData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 2,
                Question = "秘密の質問2",
                DisplayOrder = 2
            });
            // 秘密の質問の回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var answerData = mockDbContext.Object.SecurityQuestionAnswers;
            answerData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答",
                UserId = "userId"
            });
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            MvcMockHelpers.MockIdentity.SetupGet(s => s.Name).Returns("testuser");
            mockUserManager.Setup(s => s.FindByNameAsync("testuser")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.ValidatePassword(user, "password01")).ReturnsAsync(PasswordValidationStatus.Success);
            mockUserManager.Setup(s => s.ResetAccessFailedCountAsync("userId")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync("userId")).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateAsync(user)).ReturnsAsync(IdentityResult.Success);
            SetBaseField("_userManager", mockUserManager.Object);

            // アカウント管理画面に遷移すること
            var task = target.SecurityQuestionChange(
                    new SecurityQuestionChangeViewModel
                    {
                        SecurityQuestionSelectId = 2,
                        Password = "password01",
                        Answer = "変更の答え"
                    });
            var result = task.Result as RedirectToRouteResult;

            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.IsTrue(target.ModelState.IsValid);
            // TempDataがクリアされること
            Assert.IsNull(target.TempData["SecurityQuestionChange"]);
        }
    }
}
