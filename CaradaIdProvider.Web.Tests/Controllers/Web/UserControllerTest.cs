﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using MTI.CaradaIdProvider.Web.Resource;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Web
{
    /// <summary>
    /// UserControllerTest
    /// </summary>
    [TestClass]
    public class UserControllerTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private UserController target;
        private Mock<CaradaIdPDbContext> mockDbContext;
        private HttpSessionStateBase httpSession;

        [TestInitialize]
        public void TestInitialize()
        {
            target = new UserController();
            target.SetFakeControllerContext();
            mockDbContext = new Mock<CaradaIdPDbContext>();
            httpSession = new MockHttpSession();
            MockHttpContextHelper.SetCurrentOfFakeContext();
        }

        /// <summary>
        /// テスト対象が持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<UserController>(name);
            field.SetValue(target, o);
        }

        /// <summary>
        /// BaseWebControllerが持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetBaseField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<BaseWebController>(name);
            field.SetValue(target, o);
        }

        [TestMethod]
        public void Login_初期表示_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("LoginView");
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void Login_初期表示_正常系_ログインしていない場合()
        {
            MvcMockHelpers.MockIdentity.Setup(m => m.IsAuthenticated).Returns(false);
            var result = target.LoginView() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as LoginViewModel;
            Assert.IsNull(model);
        }

        [TestMethod]
        public void Login_初期表示_正常系_ログイン済みの場合()
        {
            MvcMockHelpers.MockIdentity.Setup(m => m.IsAuthenticated).Returns(true);

            var result = target.LoginView() as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.AreEqual("UserSettings", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Login_ログイン_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("Login", typeof(LoginViewModel));
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void Login_ログイン_異常系_ユーザーがいない_パスワード誤り()
        {
            var mockManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("b", "c", It.IsAny<bool>(), true)).ReturnsAsync(SignInStatusEx.Failure);

            SetBaseField("_signInManager", mockSignInManager.Object);

            var task = target.Login(new LoginViewModel() { CaradaId = "b", Password = "c" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("LoginError"), target.ModelState["CaradaId"].Errors[0].ErrorMessage);

            mockManager.VerifyAll();
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Login_ログイン_異常系_アカウントロック中()
        {
            var mockManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("name", "a", It.IsAny<bool>(), true)).ReturnsAsync(SignInStatusEx.LockedOut);

            SetBaseField("_signInManager", mockSignInManager.Object);

            var task = target.Login(new LoginViewModel() { CaradaId = "name", Password = "a" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("LockOut"), target.ModelState["CaradaId"].Errors[0].ErrorMessage);

            mockManager.VerifyAll();
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Login_ログイン_正常系_2要素認証が必要()
        {
            var user = new CaradaIdUser()
            {
                Id = "userid001",
                UserName = "TestCaradaId",
                EmailConfirmed = true
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            // ユーザー情報が取得されること
            mockUserManager.Setup(u => u.FindByNameAsync(user.UserName)).ReturnsAsync(user);
            //ユーザー情報が更新されること
            mockUserManager.Setup(u => u.UpdateAsync(It.IsAny<CaradaIdUser>())).ReturnsAsync(IdentityResult.Success);
            // セキュリティタイムスタンプが更新されること
            mockUserManager.Setup(u => u.UpdateSecurityStampAsync(user.Id)).ReturnsAsync(IdentityResult.Success);
            // 認証コードが発行されること 
            string code = "123456";
            mockUserManager.Setup(u => u.GenerateTwoFactorTokenAsync(user.Id, "TwoFactorLoginSendCodeProvider"))
                .ReturnsAsync(code).Callback((string a, string b) => Assert.AreEqual(a, user.Id));
            // 認証コードメールが送信されること
            mockUserManager.Setup(u => u.NotifyTwoFactorTokenAsync(user.Id, "TwoFactorLoginSendCodeProvider", code))
                .ReturnsAsync(IdentityResult.Success).Callback((string a, string b, string c) => Assert.AreEqual(a, user.Id));

            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            // サインインを確認
            mockSignInManager.Setup(s => s.SignInAsync(user.UserName, "c", It.IsAny<bool>(), true)).ReturnsAsync(SignInStatusEx.RequiresVerification);
            // サインアウトを確認
            mockSignInManager.Setup(s => s.SignOut()).Returns(Task.FromResult(0));

            SetBaseField("_userManager", mockUserManager.Object);
            SetBaseField("_signInManager", mockSignInManager.Object);

            var model = new LoginViewModel()
            {
                CaradaId = user.UserName,
                Password = "c",
                IsKeepLogin = false
            };
            var task = target.Login(model);
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("TwoFactorLogin", result.ViewName);

            // TempDataへの登録内容が正しいこと
            Assert.AreEqual(model, result.TempData["TwoFactorLogin"]);

            mockUserManager.VerifyAll();
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Login_ログイン_正常系_初回アクセス_利用開始画面へ()
        {
            var mockManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("name", "a", It.IsAny<bool>(), true)).ReturnsAsync(SignInStatusEx.UseStarting);

            SetBaseField("_signInManager", mockSignInManager.Object);

            var model = new LoginViewModel()
            {
                CaradaId = "name",
                Password = "a",
                IsKeepLogin = false
            };

            var task = target.Login(model);
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("UseStartView", result.RouteValues["action"]);
            Assert.AreEqual("User", result.RouteValues["controller"]);

            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Login_ログイン_正常系_アカウント管理画面に遷移()
        {
            var mockManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("name", "a", true, true)).ReturnsAsync(SignInStatusEx.Success);

            SetBaseField("_signInManager", mockSignInManager.Object);

            var task = target.Login(new LoginViewModel() { CaradaId = "name", Password = "a", IsKeepLogin = true });
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.RouteValues.Count);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("UserSettings", result.RouteValues["controller"]);

            Assert.IsTrue(target.ModelState.IsValid);

            mockManager.VerifyAll();
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Login_ログイン_正常系_AuthenticationRequestから遷移()
        {
            var mockManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            var mockSignInManager = new Mock<ApplicationSignInManager>(mockManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("name", "a", true, true)).ReturnsAsync(SignInStatusEx.Success);

            SetBaseField("_signInManager", mockSignInManager.Object);

            target.Session["AuthorizeEndpointSession"] = new AuthorizeEndpointSession()
            {
                ReturnUrl = "https://test/oauth2/auth",
                ClientId = "testclientid",
                Scope = "openid",
                ResponseType = "code",
                RedirectUri = "https://test/auth/response",
                State = "state001"
            };

            var task = target.Login(new LoginViewModel() { CaradaId = "name", Password = "a", IsKeepLogin = true });
            var result = task.Result as RedirectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("https://test/oauth2/auth?client_id=testclientid&scope=openid&response_type=code&redirect_uri=https%3A%2F%2Ftest%2Fauth%2Fresponse&state=state001",
                result.Url);

            // セッションの確認
            var session = target.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;
            Assert.AreEqual(LoginResult.Success, session.LoginResult);

            Assert.IsTrue(target.ModelState.IsValid);

            mockManager.VerifyAll();
            mockSignInManager.VerifyAll();
        }


        [TestMethod]
        public void UseStart_利用開始画面__正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("UseStartView");
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void UseStart_利用開始画面_異常系_TempDataが取得できない()
        {
            var result = target.UseStartView() as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void UseStart_利用開始画面_正常系_入力情報あり_入力フォーム入力済み()
        {
            var targetEmail = "test1234@example.com";

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = targetEmail
            };

            var result = target.UseStartView() as ViewResult;
            Assert.IsNotNull(result);
            var model = result.Model as UseStartViewModel;
            Assert.IsNotNull(model);
            Assert.AreEqual(model.Email, targetEmail);
        }

        [TestMethod]
        public void UseStart_利用規約に同意して開始_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("UseStart", typeof(UseStartViewModel));
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void UseStart_利用規約に同意して開始_異常系_TempDataが取得できない()
        {
            var task = target.UseStart(new UseStartViewModel());
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void UseStart_利用規約に同意して開始_異常系_ユーザー情報が検索できない()
        {

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStart(new UseStartViewModel() { Email = "12345@example.com" })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStart_利用規約に同意して開始_異常系_利用開始登録済み()
        {
            var targetEmail = "test1234@example.com";

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = targetEmail, EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStart(new UseStartViewModel() { Email = targetEmail })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }

            mockUserManager.VerifyAll();
        }


        [TestMethod]
        public void UseStart_利用規約に同意して開始_異常系_企業ユーザフラグがfalse()
        {
            var targetEmail = "test1234@example.com";

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = targetEmail, EmployeeFlag = false });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStart(new UseStartViewModel() { Email = targetEmail })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStart_利用規約に同意して開始_異常系_同一メアド利用開始登録済み()
        {
            var targetEmail = "test1234@example.com";

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = targetEmail, EmployeeFlag = true });
            mockUserManager.Setup(s => s.FindByEmailAsync(targetEmail)).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = targetEmail, EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.UseStart(new UseStartViewModel { Email = targetEmail });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("RegisteredMail"), target.ModelState["Email"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStart_利用規約に同意して開始_正常系_同一メアド利用開始登録前_既存側はnull更新()
        {
            var targetEmail = "test1234@example.com";
            var loginCaradaId = "test1234";
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = loginCaradaId,
                Password = "test1234",
                IsKeepLogin = false
            };
            var userById = new CaradaIdUser { Id = loginCaradaId, EmailConfirmed = false, Email = targetEmail, EmployeeFlag = true };
            var userByEmail = new CaradaIdUser { Id = "test1111", EmailConfirmed = false, Email = targetEmail, EmployeeFlag = true };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync(loginCaradaId)).ReturnsAsync(userById);
            mockUserManager.Setup(s => s.FindByEmailAsync(targetEmail)).ReturnsAsync(userByEmail);
            mockUserManager.Setup(s => s.UpdateAsync(userByEmail)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateAsync(userById)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(loginCaradaId)).ReturnsAsync(IdentityResult.Success);
            var fakeCode = "123456";
            mockUserManager.Setup(s => s.GenerateTwoFactorTokenAsync(loginCaradaId, "UseStartSendCodeProvider")).ReturnsAsync(fakeCode);
            mockUserManager.Setup(s => s.NotifyTwoFactorTokenAsync(loginCaradaId, "UseStartSendCodeProvider", fakeCode)).ReturnsAsync(IdentityResult.Success);

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.UseStart(new UseStartViewModel { Email = targetEmail });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("UseStartVerifyCode", result.ViewName);
            Assert.IsTrue(target.ModelState.IsValid);

            // model
            Assert.IsNull(result.Model);

            // TempData
            var tempDataLogin = result.TempData["Login"] as LoginViewModel;
            var tempDataUseStart = result.TempData["UseStart"] as UseStartViewModel;
            Assert.AreEqual(tempDataLogin.CaradaId, loginCaradaId);
            Assert.AreEqual(tempDataLogin.Password, "test1234");
            Assert.AreEqual(tempDataLogin.IsKeepLogin, false);
            Assert.AreEqual(tempDataUseStart.Email, targetEmail);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStart_利用規約に同意して開始_正常系_同一メアド登録なし()
        {
            var targetEmail = "test1234@example.com";
            var loginCaradaId = "test1234";
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = loginCaradaId,
                Password = "test1234",
                IsKeepLogin = false
            };
            var userById = new CaradaIdUser { Id = loginCaradaId, EmailConfirmed = false, Email = targetEmail, EmployeeFlag = true };
            var userByEmail = new CaradaIdUser { Id = "test1111", EmailConfirmed = false, Email = targetEmail, EmployeeFlag = true };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync(loginCaradaId)).ReturnsAsync(userById);
            mockUserManager.Setup(s => s.FindByEmailAsync(targetEmail)).ReturnsAsync(null);
            mockUserManager.Setup(s => s.UpdateAsync(userById)).ReturnsAsync(IdentityResult.Success);
            mockUserManager.Setup(s => s.UpdateSecurityStampAsync(loginCaradaId)).ReturnsAsync(IdentityResult.Success);
            var fakeCode = "123456";
            mockUserManager.Setup(s => s.GenerateTwoFactorTokenAsync(loginCaradaId, "UseStartSendCodeProvider")).ReturnsAsync(fakeCode);
            mockUserManager.Setup(s => s.NotifyTwoFactorTokenAsync(loginCaradaId, "UseStartSendCodeProvider", fakeCode)).ReturnsAsync(IdentityResult.Success);

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.UseStart(new UseStartViewModel { Email = targetEmail });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("UseStartVerifyCode", result.ViewName);
            Assert.IsTrue(target.ModelState.IsValid);

            // model
            Assert.IsNull(result.Model);

            // ユーザデータ
            // TempData
            var tempDataLogin = result.TempData["Login"] as LoginViewModel;
            var tempDataUseStart = result.TempData["UseStart"] as UseStartViewModel;
            Assert.AreEqual(tempDataLogin.CaradaId, loginCaradaId);
            Assert.AreEqual(tempDataLogin.Password, "test1234");
            Assert.AreEqual(tempDataLogin.IsKeepLogin, false);
            Assert.AreEqual(tempDataUseStart.Email, targetEmail);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_認証する_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("UseStartVerifyCode");
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_TempData_Loginが無い場合()
        {
            var task = target.UseStartVerifyCode(new UseStartVerifyCodeViewModel() { VerifyCode = "12345" });
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_TempData_UseStartが無い場合()
        {

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            var task = target.UseStartVerifyCode(new UseStartVerifyCodeViewModel() { VerifyCode = "12345" });
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_ユーザー情報が検索できない()
        {

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStartVerifyCode(new UseStartVerifyCodeViewModel() { VerifyCode = "12345" })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_検索したユーザーが既に利用開始登録済み()
        {
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = "test@test.co.jp", EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStartVerifyCode(new UseStartVerifyCodeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_検索したユーザーのEmailがnull()
        {
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = null, EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStartVerifyCode(new UseStartVerifyCodeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_検索したユーザーのEmailの値が異なっている()
        {
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = "test2@test.co.jp", EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStartVerifyCode(new UseStartVerifyCodeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_検索したユーザーの企業ユーザーフラグがfalseに設定されている()
        {
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };


            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = "test@test.co.jp", EmployeeFlag = false });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.UseStartVerifyCode(new UseStartVerifyCodeViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_認証コードが既に無効()
        {
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var user = new CaradaIdUser { Id = "id", EmailConfirmed = false, AuthCodeFailedCount = 5, EmployeeFlag = true, Email = "test@test.co.jp" };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.UseStartVerifyCode(new UseStartVerifyCodeViewModel { VerifyCode = "123456" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_認証コード誤り_認証コード無効()
        {
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var user = new CaradaIdUser { Id = "id", EmailConfirmed = false, AuthCodeFailedCount = 4, EmployeeFlag = true, Email = "test@test.co.jp" };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "UseStartSendCodeProvider", "123456")).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).Returns(Task.FromResult<bool>(true));
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.UseStartVerifyCode(new UseStartVerifyCodeViewModel { VerifyCode = "123456" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_異常系_認証コード誤り_認証コード有効()
        {
            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var user = new CaradaIdUser { Id = "id", EmailConfirmed = false, AuthCodeFailedCount = 3, EmployeeFlag = true, Email = "test@test.co.jp" };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "UseStartSendCodeProvider", "123456")).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).Returns(Task.FromResult<bool>(false));
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.UseStartVerifyCode(new UseStartVerifyCodeViewModel { VerifyCode = "123456" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void UseStartVerifyCode_正常系_正常に認証される()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var user = new CaradaIdUser { Id = "id", EmailConfirmed = false, AuthCodeFailedCount = 3, EmployeeFlag = true, Email = "test@test.co.jp" };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "UseStartSendCodeProvider", "123456")).ReturnsAsync(true);
            mockUserManager.Setup(s => s.UpdateAsync(user)).Returns(Task.FromResult<IdentityResult>(new IdentityResult()));
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.UseStartVerifyCode(new UseStartVerifyCodeViewModel { VerifyCode = "123456" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("SecurityQuestionRegist", result.ViewName);
            Assert.IsTrue(target.ModelState.IsValid);

            mockUserManager.VerifyAll();
            mockDbContext.Verify(d => d.SecurityQuestionMasters);
        }


        [TestMethod]
        public void TwoFactorLogin_正常系_認証する_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("TwoFactorLogin");
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_TempDataが取得できない()
        {
            var task = target.TwoFactorLogin(new TwoFactorLoginViewModel());
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_ユーザー情報が検索できない()
        {
            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.TwoFactorLogin(new TwoFactorLoginViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_取得したユーザー情報が利用開始前ユーザー()
        {
            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.TwoFactorLogin(new TwoFactorLoginViewModel())).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_認証コードが既に無効()
        {
            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };
            var user = new CaradaIdUser
            {
                EmailConfirmed = true
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.TwoFactorLogin(new TwoFactorLoginViewModel { VerifyCode = "123456" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_認証コード誤り_認証コード無効()
        {
            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };
            var user = new CaradaIdUser
            {
                Id = "id",
                EmailConfirmed = true
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "TwoFactorLoginSendCodeProvider", "123456")).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).Returns(Task.FromResult<bool>(true));
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.TwoFactorLogin(new TwoFactorLoginViewModel { VerifyCode = "123456" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeInvalidError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_認証コード誤り_認証コード有効()
        {
            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };
            var user = new CaradaIdUser
            {
                Id = "id",
                EmailConfirmed = true
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "TwoFactorLoginSendCodeProvider", "123456")).ReturnsAsync(false);
            mockUserManager.Setup(s => s.IsVerifyCodeInvalidNow(user)).Returns(Task.FromResult<bool>(false));
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.TwoFactorLogin(new TwoFactorLoginViewModel { VerifyCode = "123456" });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("VerifyCodeError"), target.ModelState["VerifyCode"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_ログインに失敗()
        {
            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };
            var user = new CaradaIdUser
            {
                Id = "id",
                EmailConfirmed = true,
                UserName = "test1234"
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "TwoFactorLoginSendCodeProvider", "123456")).ReturnsAsync(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("test1234", "pass1234", false, false)).ReturnsAsync(SignInStatusEx.Failure);
            SetBaseField("_signInManager", mockSignInManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin("id", "test1234")).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "TwoFactorId001" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);

            try
            {
                Task.Run(() => target.TwoFactorLogin(new TwoFactorLoginViewModel() { VerifyCode = "123456" })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }

            //Cookieの確認
            var cookie = target.Response.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("TwoFactorId001", cookie.Value);

            mockUserManager.VerifyAll();
            mockSignInManager.VerifyAll();
            mockTwoFactorLogin.VerifyAll();
        }

        [TestMethod]
        public void TwoFactorLogin_正常系_正常に2段階認証される()
        {
            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = true
            };
            var user = new CaradaIdUser
            {
                Id = "id",
                EmailConfirmed = true,
                UserName = "test1234"
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "TwoFactorLoginSendCodeProvider", "123456")).ReturnsAsync(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("test1234", "pass1234", true, false)).ReturnsAsync(SignInStatusEx.Success);
            SetBaseField("_signInManager", mockSignInManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin("id", "test1234")).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "TwoFactorId002" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);

            var task = target.TwoFactorLogin(new TwoFactorLoginViewModel { VerifyCode = "123456" });
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.RouteValues.Count);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("UserSettings", result.RouteValues["controller"]);

            //Cookieの確認
            var cookie = target.Response.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("TwoFactorId002", cookie.Value);

            mockUserManager.VerifyAll();
            mockSignInManager.VerifyAll();
            mockTwoFactorLogin.VerifyAll();
        }

        [TestMethod]
        public void TwoFactorLogin_正常系_AuthenticationRequestから遷移の場合()
        {
            target.Session["AuthorizeEndpointSession"] = new AuthorizeEndpointSession()
            {
                ReturnUrl = "https://test/oauth2/auth",
                ClientId = "testclientid",
                Scope = "openid",
                ResponseType = "code",
                RedirectUri = "https://test/auth/response",
                State = "state001"
            };

            target.TempData["TwoFactorLogin"] = new LoginViewModel()
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };
            var user = new CaradaIdUser
            {
                Id = "id",
                EmailConfirmed = true,
                UserName = "test1234"
            };
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.IsInvalidVerifyCode(user)).Returns(false);
            mockUserManager.Setup(s => s.VerifyTwoFactorTokenAsync("id", "TwoFactorLoginSendCodeProvider", "123456")).ReturnsAsync(true);
            SetBaseField("_userManager", mockUserManager.Object);

            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("test1234", "pass1234", false, false)).ReturnsAsync(SignInStatusEx.Success);
            SetBaseField("_signInManager", mockSignInManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin("id", "test1234")).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "TwoFactorId002" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);

            var task = target.TwoFactorLogin(new TwoFactorLoginViewModel { VerifyCode = "123456" });
            var result = task.Result as RedirectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("https://test/oauth2/auth?client_id=testclientid&scope=openid&response_type=code&redirect_uri=https%3A%2F%2Ftest%2Fauth%2Fresponse&state=state001",
                result.Url);
            // セッションの確認
            var session = target.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;
            Assert.AreEqual(LoginResult.Success, session.LoginResult);

            // Cookieの確認
            var cookie = target.Response.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("TwoFactorId002", cookie.Value);

            mockUserManager.VerifyAll();
            mockSignInManager.VerifyAll();
            mockTwoFactorLogin.VerifyAll();
        }

        private void SetSecurityQuestion()
        {
            mockDbContext.Setup(d => d.SecurityQuestionMasters).Returns(new TestSecurityQuestionMasters());
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var dbData = mockDbContext.Object.SecurityQuestionMasters;
            dbData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "表示順2秘密の質問",
                DisplayOrder = 2
            });
            dbData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 2,
                Question = "表示順3秘密の質問",
                DisplayOrder = 3
            });
            dbData.Add(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 3,
                Question = "表示順1秘密の質問",
                DisplayOrder = 1
            });
        }

        [TestMethod]
        public void SecurityQuestionRegist_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("SecurityQuestionRegist", typeof(SecurityQuestionRegistViewModel));
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_該当する秘密の質問がない()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            var task = target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
            {
                SecurityQuestionSelectId = 999,
                Answer = "答え"
            });
            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("SecurityQuestionError"), target.ModelState["SecurityQuestionSelectId"].Errors[0].ErrorMessage);
            mockDbContext.Verify(d => d.SecurityQuestionMasters);
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_TempData_Loginが無い場合()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            var task = target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
            {
                SecurityQuestionSelectId = 1,
                Answer = "答え"
            });
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);

            mockDbContext.Verify(d => d.SecurityQuestionMasters);
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_TempData_UseStartが無い場合()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };

            var task = target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
            {
                SecurityQuestionSelectId = 1,
                Answer = "答え"
            });
            var result = task.Result as RedirectToRouteResult;
            // タイムアウトエラー画面へ遷移すること
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);

            mockDbContext.Verify(d => d.SecurityQuestionMasters);
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_ユーザー情報が検索できない()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(null);
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
                {
                    SecurityQuestionSelectId = 1,
                    Answer = "答え"
                })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
            mockDbContext.Verify(d => d.SecurityQuestionMasters);
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_検索したユーザーが既に利用開始登録済み()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = true, Email = "test@test.co.jp", EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
                {
                    SecurityQuestionSelectId = 1,
                    Answer = "答え"
                })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_検索したユーザーのEmailがnull()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = null, EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
                {
                    SecurityQuestionSelectId = 1,
                    Answer = "答え"
                })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_検索したユーザーのEmailの値が異なっている()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = "test2@test.co.jp", EmployeeFlag = true });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
                {
                    SecurityQuestionSelectId = 1,
                    Answer = "答え"
                })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_検索したユーザーの企業ユーザーフラグがfalseに設定されている()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "test1234",
                IsKeepLogin = false
            };
            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false, Email = "test@test.co.jp", EmployeeFlag = false });
            SetBaseField("_userManager", mockUserManager.Object);

            try
            {
                Task.Run(() => target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
                {
                    SecurityQuestionSelectId = 1,
                    Answer = "答え"
                })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void SecurityQuestionRegist_異常系_ログイン状態処理エラー()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            // 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var dbData = mockDbContext.Object.SecurityQuestionAnswers;
            dbData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答",
                UserId = "id"
            });

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var user = new CaradaIdUser { Id = "id", EmailConfirmed = false, AuthCodeFailedCount = 3, EmployeeFlag = true, Email = "test@test.co.jp", UserName = "test1234" };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.UpdateAsync(user)).Returns(Task.FromResult<IdentityResult>(new IdentityResult()));
            SetBaseField("_userManager", mockUserManager.Object);

            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("test1234", "pass1234", false, false)).ReturnsAsync(SignInStatusEx.Failure);
            SetBaseField("_signInManager", mockSignInManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin("id", "test1234")).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "TwoFactorId002" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);
            try
            {
                Task.Run(() => target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
                {
                    SecurityQuestionSelectId = 1,
                    Answer = "答え"
                })).Wait();
                Assert.Fail("例外が発生しない");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e.InnerException, typeof(HttpException));
            }
            mockUserManager.VerifyAll();
            mockDbContext.Verify(d => d.SecurityQuestionMasters);
            mockTwoFactorLogin.VerifyAll();
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void SecurityQuestionRegist_正常系_正常に認証される()
        {
            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            // 秘密の質問回答を設定する
            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());
            var dbData = mockDbContext.Object.SecurityQuestionAnswers;
            dbData.Add(new SecurityQuestionAnswers()
            {
                SecurityQuestionId = 1,
                Answer = "秘密の回答",
                UserId = "id"
            });

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var user = new CaradaIdUser { Id = "id", EmailConfirmed = false, AuthCodeFailedCount = 3, EmployeeFlag = true, Email = "test@test.co.jp", UserName = "test1234" };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.UpdateAsync(user)).Returns(Task.FromResult<IdentityResult>(new IdentityResult()));
            // メール設定
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage message) =>
                {
                    Assert.AreEqual("test@test.co.jp", message.Destination);
                    Assert.AreEqual(Mail.RegisteredSubject, message.Subject);
                    Assert.AreEqual(string.Format(Mail.RegisteredBody, "test1234", "test@test.co.jp", Mail.Support), message.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;
            SetBaseField("_userManager", mockUserManager.Object);

            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("test1234", "pass1234", false, false)).ReturnsAsync(SignInStatusEx.Success);
            SetBaseField("_signInManager", mockSignInManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin("id", "test1234")).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "TwoFactorId002" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);

            var task = target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
            {
                SecurityQuestionSelectId = 1,
                Answer = "答え"
            });
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.AreEqual("UserSettings", result.RouteValues["controller"]);
            Assert.IsTrue(target.ModelState.IsValid);

            //Cookieの確認
            var cookie = target.Response.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("TwoFactorId002", cookie.Value);

            mockUserManager.VerifyAll();
            mockDbContext.Verify(d => d.SecurityQuestionMasters);
            mockTwoFactorLogin.VerifyAll();
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void SecurityQuestionRegist_正常系_AuthenticationRequestから遷移の場合()
        {
            target.Session["AuthorizeEndpointSession"] = new AuthorizeEndpointSession()
            {
                ReturnUrl = "https://test/oauth2/auth",
                ClientId = "testclientid",
                Scope = "openid",
                ResponseType = "code",
                RedirectUri = "https://test/auth/response",
                State = "state001"
            };

            // 秘密の質問リストを設定する
            SetSecurityQuestion();

            mockDbContext.Setup(d => d.SecurityQuestionAnswers).Returns(new TestSecurityQuestionAnswers());

            target.TempData["Login"] = new LoginViewModel
            {
                CaradaId = "test1234",
                Password = "pass1234",
                IsKeepLogin = false
            };

            target.TempData["UseStart"] = new UseStartViewModel
            {
                Email = "test@test.co.jp"
            };

            var user = new CaradaIdUser { Id = "id", EmailConfirmed = false, AuthCodeFailedCount = 3, EmployeeFlag = true, Email = "test@test.co.jp", UserName = "test1234" };

            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByNameAsync("test1234")).ReturnsAsync(user);
            mockUserManager.Setup(s => s.UpdateAsync(user)).Returns(Task.FromResult<IdentityResult>(new IdentityResult()));
            // メール設定
            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage message) =>
                {
                    Assert.AreEqual("test@test.co.jp", message.Destination);
                    Assert.AreEqual(Mail.RegisteredSubject, message.Subject);
                    Assert.AreEqual(string.Format(Mail.RegisteredBody, "test1234", "test@test.co.jp", Mail.Support), message.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;
            SetBaseField("_userManager", mockUserManager.Object);

            var mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignInAsync("test1234", "pass1234", false, false)).ReturnsAsync(SignInStatusEx.Success);
            SetBaseField("_signInManager", mockSignInManager.Object);

            var mockTwoFactorLogin = new Mock<TwoFactorAuthenticator>() { CallBase = true };
            mockTwoFactorLogin.Setup(s => s.CompleteTwoFactorLogin("id", "test1234")).Returns(
                new CookieModel { Name = "TwoFactorLogin", HttpOnly = true, Expires = new DateTime(2020, 1, 1), Value = "TwoFactorId002" });
            SetField("twoFactorAuthenticator", mockTwoFactorLogin.Object);

            var cookies = new HttpCookieCollection();
            MvcMockHelpers.MockResponse.Setup(mr => mr.Cookies).Returns(cookies);

            var task = target.SecurityQuestionRegist(new SecurityQuestionRegistViewModel
            {
                SecurityQuestionSelectId = 1,
                Answer = "答え"
            });
            var result = task.Result as RedirectResult;
            Assert.IsNotNull(result);
            Assert.IsTrue(target.ModelState.IsValid);
            Assert.AreEqual("https://test/oauth2/auth?client_id=testclientid&scope=openid&response_type=code&redirect_uri=https%3A%2F%2Ftest%2Fauth%2Fresponse&state=state001",
                result.Url);
            // セッションの確認
            var session = target.Session["AuthorizeEndpointSession"] as AuthorizeEndpointSession;
            Assert.AreEqual(LoginResult.Success, session.LoginResult);

            //Cookieの確認
            var cookie = target.Response.Cookies["TwoFactorLogin"];
            Assert.IsNotNull(cookie);
            Assert.IsTrue(cookie.HttpOnly);
            Assert.AreNotEqual<DateTime>(new DateTime(0), cookie.Expires);
            Assert.AreEqual("TwoFactorId002", cookie.Value);

            mockUserManager.VerifyAll();
            mockDbContext.Verify(d => d.SecurityQuestionMasters);
            mockTwoFactorLogin.VerifyAll();
            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void Logout_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("Logout");
            Assert.IsNotNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void Logout_正常系()
        {
            // ログアウト処理が呼び出されること
            var mockSignInManager = new Mock<ApplicationSignInManager>(
                new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object).Object,
                new Mock<IAuthenticationManager>().Object);
            mockSignInManager.Setup(s => s.SignOut()).Returns(Task.FromResult(0));

            target.TempData["test"] = "testTempData";

            SetBaseField("_signInManager", mockSignInManager.Object);
            var task = target.Logout();
            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("LoginView", result.RouteValues["action"]);
            Assert.AreEqual("User", result.RouteValues["controller"]);
            Assert.AreEqual(0, target.TempData.Count);
            Assert.IsNull(target.TempData["test"]);

            mockSignInManager.VerifyAll();
        }

        [TestMethod]
        public void IdNoticeView_初期表示_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("IdNoticeView", 0);
            Assert.IsNotNull(method.GetCustomAttribute<HttpGetAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void IdNoticeView_初期表示_正常系_ID連絡画面が表示される()
        {
            var result = target.IdNoticeView() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("IdNotice", result.ViewName);
        }

        [TestMethod]
        public void IdNotice_CARADA_IDを通知する_正常系_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<UserController>("IdNotice", 1);
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            Assert.IsNull(method.GetCustomAttribute<AuthorizeAttribute>());
        }

        [TestMethod]
        public void IdNotice_CARADA_IDを通知する_異常系_検索結果なし()
        {
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByEmailAsync("a@a.com")).ReturnsAsync(new CaradaIdUser());
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.IdNotice(new IdNoticeViewModel() { Email = "a@a.com" });

            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);

            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("NotRegisteredUser"), target.ModelState["Email"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void IdNotice_CARADA_IDを通知する_異常系_利用開始前のユーザーが検索された()
        {
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockUserManager.Setup(s => s.FindByEmailAsync("a@a.com")).ReturnsAsync(new CaradaIdUser { EmailConfirmed = false });
            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.IdNotice(new IdNoticeViewModel() { Email = "a@a.com" });

            var result = task.Result as ViewResult;
            Assert.IsNotNull(result);

            Assert.AreEqual("", result.ViewName);
            Assert.IsNotNull(result.Model);
            Assert.IsFalse(target.ModelState.IsValid);
            Assert.AreEqual(ErrorMessages.ResourceManager.GetString("NotRegisteredUser"), target.ModelState["Email"].Errors[0].ErrorMessage);

            mockUserManager.VerifyAll();
        }

        [TestMethod]
        public void IdNotice_CARADA_IDを通知する_正常系_CARADAIDが通知される()
        {
            var mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object) { CallBase = true };
            mockUserManager.Setup(s => s.FindByEmailAsync("a@a.com")).ReturnsAsync(new CaradaIdUser { Email = "a@a.com", EmailConfirmed = true, UserName = "test_id" });

            var mockEmailService = new Mock<AsyncEmailService>();
            mockEmailService.Setup(s => s.SendAsync(It.IsAny<IdentityMessage>())).Returns(Task.FromResult(0))
                .Callback((IdentityMessage message) =>
                {
                    Assert.AreEqual("a@a.com", message.Destination);
                    Assert.AreEqual(Mail.IdNoticeSubject, message.Subject);
                    Assert.AreEqual(string.Format(Mail.IdNoticeBody, "test_id", Mail.Support), message.Body);
                });

            mockUserManager.Object.EmailService = mockEmailService.Object;

            SetBaseField("_userManager", mockUserManager.Object);

            var task = target.IdNotice(new IdNoticeViewModel() { Email = "a@a.com" });

            var result = task.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("LoginView", result.RouteValues["Action"]);
            Assert.IsNull(result.RouteValues["controller"]);

            mockUserManager.VerifyAll();
            mockEmailService.VerifyAll();
        }
    }
}
