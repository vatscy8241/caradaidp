﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Web
{
    /// <summary>
    /// BaseWebControllerのテストクラス。
    /// </summary>
    [TestClass]
    public class BaseWebControllerTest
    {
        /// <summary>
        /// BaseWebControllerのメソッドがprotectedなので、継承クラスを作成。
        /// </summary>
        private class BaseWebControllerUTEntrance : BaseWebController
        {
            public BaseWebControllerUTEntrance()
                : base()
            {
            }

            public BaseWebControllerUTEntrance(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
                : base(userManager, signInManager)
            {
            }
        }

        /// <summary>
        /// テスト対象
        /// </summary>
        private BaseWebControllerUTEntrance target;
        private Mock<ApplicationUserManager> mockUserManager;
        private Mock<ApplicationSignInManager> mockSignInManager;

        [TestInitialize]
        public void Initialize()
        {
            target = new BaseWebControllerUTEntrance();
            target.SetFakeControllerContext();
            mockUserManager = new Mock<ApplicationUserManager>(new Mock<IUserStore<CaradaIdUser>>().Object);
            mockSignInManager = new Mock<ApplicationSignInManager>(mockUserManager.Object, new Mock<IAuthenticationManager>().Object);
        }

        [TestMethod]
        public void SignInManager_nullでないとき()
        {
            target = new BaseWebControllerUTEntrance(null, mockSignInManager.Object);
            Assert.AreEqual(mockSignInManager.Object, target.SignInManager);
        }

        [TestMethod]
        public void SignInManager_nullのとき_OwinContextから取得されること()
        {
            MvcMockHelpers.Owin.Set<ApplicationSignInManager>(mockSignInManager.Object);
            Assert.AreEqual(mockSignInManager.Object, target.SignInManager);
        }

        [TestMethod]
        public void UserManager_nullでないとき()
        {
            target = new BaseWebControllerUTEntrance(mockUserManager.Object, null);
            Assert.AreEqual(mockUserManager.Object, target.UserManager);
        }

        [TestMethod]
        public void UserManger_nullのとき_OwinContextから取得されること()
        {
            MvcMockHelpers.Owin.Set<ApplicationUserManager>(mockUserManager.Object);
            Assert.AreEqual(mockUserManager.Object, target.UserManager);
        }

        [TestMethod]
        public void AuthenticationManager_nullでないとき()
        {
            var p = ReflectionAccessor.GetProperty<BaseWebController>("AuthenticationManager");
            var mockAuthenticationManager = new Mock<IAuthenticationManager>();
            p.SetValue(target, mockAuthenticationManager.Object);
            Assert.AreEqual(mockAuthenticationManager.Object, target.AuthenticationManager);
        }

        [TestMethod]
        public void AuthenticationManager_nullのとき_OwinContextから取得されること()
        {
            // MvcMockHelpers.MockContext.Owin.Authenticationで取れるのとは違うインスタンスらしいのでnullでない確認のみ行う
            Assert.IsNotNull(target.AuthenticationManager);
        }

        [TestMethod]
        public void CaradaIdPDb_nullでないとき()
        {
            var p = ReflectionAccessor.GetProperty<BaseWebController>("CaradaIdPDb");
            var mockUserDb = new Mock<CaradaIdPDbContext>();
            p.SetValue(target, mockUserDb.Object);
            Assert.AreEqual(mockUserDb.Object, target.CaradaIdPDb);
        }

        [TestMethod]
        public void CaradaIdPDb_nullのとき_OwinContextから取得されること()
        {
            var mockUserDb = new Mock<CaradaIdPDbContext>();
            MvcMockHelpers.Owin.Set<CaradaIdPDbContext>(mockUserDb.Object);
            Assert.AreEqual(mockUserDb.Object, target.CaradaIdPDb);
        }

        [TestMethod]
        public void RedirectToTimeoutErrorOnNoTempData_正常系_タイムアウトエラー画面へリダイレクト()
        {
            var result = target.RedirectToTimeoutErrorOnNoTempData("testTempDataKey") as RedirectToRouteResult;
            // ログ出力はITで確認するのでUT対象外
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void RedirectToTimeoutErrorOnNoTempData_正常系_tempDataKeyがnullでもタイムアウトエラー画面へリダイレクト()
        {
            var result = target.RedirectToTimeoutErrorOnNoTempData(null) as RedirectToRouteResult;
            // ログ出力はITで確認するのでUT対象外
            Assert.AreEqual("Timeout", result.RouteValues["action"]);
            Assert.AreEqual("Error", result.RouteValues["controller"]);
        }
    }
}
