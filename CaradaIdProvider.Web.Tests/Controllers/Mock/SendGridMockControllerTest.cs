﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Mock;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Mock
{
    /// <summary>
    /// SendGridモックのテストクラス。条件網羅はしない。
    /// </summary>
    [TestClass]
    public class SendGridMockControllerTest
    {
        private SendGridMockController target;

        [TestInitialize]
        public void Initialize()
        {
            target = new SendGridMockController();
            target.SetFakeControllerContext();
            var field = ReflectionAccessor.GetField<SendGridMockController>("timeWait");
            field.SetValue(null, 2000);
        }

        /// <summary>
        /// テスト対象が持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<SendGridMockController>(name);
            field.SetValue(target, o);
        }

        [TestMethod]
        public void BouncesGet_対象なし()
        {
            var result = target.BouncesGet(new SendGridMockController.BouncesGetModel() { Email = "hoge@hoge.com" }) as JsonResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(0, ((object[])result.Data).Length);
        }

        [TestMethod]
        public void BounceGet_対象あり()
        {
            var result = target.BouncesGet(new SendGridMockController.BouncesGetModel() { Email = "bounce@a.com" }) as JsonResult;

            Assert.IsNotNull(result);
            SendGridEmailService.BounceModel[] data = result.Data as SendGridEmailService.BounceModel[];
            Assert.AreEqual(1, data.Length);
            Assert.AreEqual("bounce@a.com", data[0].Email);
        }

        [TestMethod]
        public void BounceGet_エラー()
        {
            var result = target.BouncesGet(new SendGridMockController.BouncesGetModel() { Email = "error@a.com" });
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }

        [TestMethod]
        public void BounceDelete_正常()
        {
            var result = target.BouncesDelete(new SendGridMockController.BoucesDeleteModel() { Email = "bounce@a.com" }) as JsonResult;
            Assert.IsNotNull(result);
            var data = result.Data as SendGridEmailService.ResultModel;
            Assert.AreEqual("success", data.Message);
        }

        [TestMethod]
        public void BouncesDelete_エラー()
        {
            var result = target.BouncesDelete(new SendGridMockController.BoucesDeleteModel() { Email = "deleteerror@a.com" });
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }
    }
}
