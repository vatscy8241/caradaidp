﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Controllers.Mock;
using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Mock
{
    /// <summary>
    /// RCモックのテストクラス。RequestTokenはUT不要とする。
    /// </summary>
    [TestClass]
    public class RpMockControllerTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private RpMockController target;
        private Mock<CaradaIdPDbContext> mockDbContext;

        /// <summary>
        /// BaseWebControllerが持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        private void SetBaseField(string name, Object o)
        {
            var field = ReflectionAccessor.GetField<BaseWebController>(name);
            field.SetValue(target, o);
        }

        [TestInitialize]
        public void Initialize()
        {
            mockDbContext = new Mock<CaradaIdPDbContext>();
            target = new RpMockController();
            target.SetFakeControllerContext();
        }

        [TestMethod]
        public void Auth_正常()
        {
            var clientMastersMock = new TestClientMastersDbSet();
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "test_client",
                ClientName = "AuthProvider",
                ClientSecret = "test_client_secret",
            });
            mockDbContext.Setup(s => s.ClientMasters).Returns(clientMastersMock);
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var mock = Moq.Mock.Get(target.HttpContext.Request);
            // ローカル環境
            mock.Setup(m => m.Url).Returns(new Uri("https://localhost/RpMock/Auth"));

            var result = target.Auth() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("https://localhost/oauth2/auth", result.ViewBag.request_url);
            Assert.AreEqual("test_client", result.ViewBag.client_id);
            Assert.AreEqual("code", result.ViewBag.response_type);
            Assert.AreEqual("https://localhost/RpMock/Token", result.ViewBag.redirect_uri);
            Assert.IsNotNull(result.ViewBag.state);
            string sessionState = target.Session["mockSession"] as string;
            Assert.AreEqual(result.ViewBag.state, sessionState);
            Assert.AreEqual("Auth", result.ViewName);

            // 開発環境
            mock.Setup(m => m.Url).Returns(new Uri("https://devid.carada.jp/RpMock/Auth"));

            result = target.Auth() as ViewResult;
            Assert.IsNotNull(result);
            Assert.AreEqual("https://devid.carada.jp/oauth2/auth", result.ViewBag.request_url);
            Assert.AreEqual("test_client", result.ViewBag.client_id);
            Assert.AreEqual("code", result.ViewBag.response_type);
            Assert.AreEqual("https://devid.carada.jp/RpMock/Token", result.ViewBag.redirect_uri);
            Assert.IsNotNull(result.ViewBag.state);
            sessionState = target.Session["mockSession"] as string;
            Assert.AreEqual(result.ViewBag.state, sessionState);
            Assert.AreEqual("Auth", result.ViewName);

            mock.VerifyAll();
            mockDbContext.VerifyAll();
        }

        [TestMethod]
        public void Token_AuthenticationErrorResopnse()
        {
            target.Session["mockSession"] = "1234567890";
            var result = target.Token(new RpMockModel()
            {
                error = "invalid_request",
                state = "1234567890"
            }) as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNull(target.Session["mockSession"]);
            var model = result.Model as RpMockModel;
            Assert.IsNull(model.code);
            Assert.AreEqual("invalid_request", model.error);
            Assert.AreEqual("1234567890", model.state);
            Assert.AreEqual("AuthError", result.ViewName);

            mockDbContext.VerifyAll();
        }

        [TestMethod]
        public void Token_SuccessfulAuthenticationResopnse()
        {
            var clientMastersMock = new TestClientMastersDbSet();
            clientMastersMock.Add(new ClientMasters()
            {
                ClientId = "test_client",
                ClientName = "AuthProvider",
                ClientSecret = "test_client_secret",
            });
            mockDbContext.Setup(s => s.ClientMasters).Returns(clientMastersMock);
            SetBaseField("_caradaIdPDbContext", mockDbContext.Object);

            var mock = Moq.Mock.Get(target.HttpContext.Request);
            // ローカル環境
            mock.Setup(m => m.Url).Returns(new Uri("https://localhost/RpMock/Token"));

            target.Session["mockSession"] = "1234567890";
            var result = target.Token(new RpMockModel()
            {
                code = "testcode001",
                state = "1234567890"
            }) as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNull(target.Session["mockSession"]);
            var model = result.Model as RpMockModel;
            Assert.AreEqual("testcode001", model.code);
            Assert.AreEqual("1234567890", model.state);
            Assert.IsNull(model.error);
            Assert.AreEqual("authorization_code", model.grant_type);
            Assert.AreEqual("https://localhost/RpMock/Token", model.redirect_uri);
            Assert.AreEqual("test_client", model.client_id);
            Assert.AreEqual("test_client_secret", model.client_secret);
            Assert.AreEqual("Token", result.ViewName);

            mock.VerifyAll();
            mockDbContext.VerifyAll();
        }

        [TestMethod]
        public void RequestToken_属性の確認()
        {
            var method = ReflectionAccessor.GetMethod<RpMockController>("RequestToken");
            Assert.IsNotNull(method.GetCustomAttribute<HttpPostAttribute>());
            Assert.IsNotNull(method.GetCustomAttribute<ValidateAntiForgeryTokenAttribute>());
            mockDbContext.VerifyAll();
        }
    }
}
