﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Helpers;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Helpers
{
    [TestClass]
    public class HtmlHelperExtensionsTest
    {
        private class TestModel
        {
            public bool BoolA { get; set; }
        }

        private Mock<ViewContext> mockViewContext;
        private Mock<IViewDataContainer> mockVDC;

        [TestInitialize]
        public void Initialize()
        {
            // このモックはフレームワーク内部で呼び出されるのでVerifyする観点は不要
            mockVDC = new Mock<IViewDataContainer>();
            mockVDC.Setup(s => s.ViewData).Returns(new ViewDataDictionary(new TestModel()));
            mockViewContext = new Mock<ViewContext>() { CallBase = true };
            mockViewContext.Setup(s => s.ViewData).Returns(mockVDC.Object.ViewData);
            mockViewContext.Setup(s => s.UnobtrusiveJavaScriptEnabled).Returns(false);
        }

        [TestMethod]
        public void CheckBoxFor2_hiddenが除去されていること()
        {
            var a = new HtmlHelper<TestModel>(mockViewContext.Object, mockVDC.Object);

            var ret = a.CheckBoxFor2<TestModel>(model => model.BoolA);

            // ライブラリのバージョンが変わったらattrの順番が変わってきそうなので全体のAssertはしない
            Assert.IsTrue(ret.ToHtmlString().StartsWith(@"<input"));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"type=""checkbox"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"id=""BoolA"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"name=""BoolA"""));
            Assert.IsTrue(ret.ToHtmlString().EndsWith(@"/>"));
            Assert.IsFalse(ret.ToHtmlString().Contains(@"type=""hidden"""));
        }

        [TestMethod]
        public void CheckBoxFor2_object_hiddenが除去されていること()
        {
            var a = new HtmlHelper<TestModel>(mockViewContext.Object, mockVDC.Object);

            var ret = a.CheckBoxFor2<TestModel>(model => model.BoolA, new { @class = "hoge" });

            Assert.IsTrue(ret.ToHtmlString().StartsWith(@"<input"));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"type=""checkbox"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"id=""BoolA"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"name=""BoolA"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"class=""hoge"""));
            Assert.IsTrue(ret.ToHtmlString().EndsWith(@"/>"));
            Assert.IsFalse(ret.ToHtmlString().Contains(@"type=""hidden"""));
        }

        [TestMethod]
        public void CheckBoxFor2_IDictionary_hiddenが除去されていること()
        {
            var a = new HtmlHelper<TestModel>(mockViewContext.Object, mockVDC.Object);

            var ret = a.CheckBoxFor2<TestModel>(model => model.BoolA, new Dictionary<string, object> { { "class", "hoge" } });

            Assert.IsTrue(ret.ToHtmlString().StartsWith(@"<input"));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"type=""checkbox"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"id=""BoolA"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"name=""BoolA"""));
            Assert.IsTrue(ret.ToHtmlString().Contains(@"class=""hoge"""));
            Assert.IsTrue(ret.ToHtmlString().EndsWith(@"/>"));
            Assert.IsFalse(ret.ToHtmlString().Contains(@"type=""hidden"""));
        }
    }
}
