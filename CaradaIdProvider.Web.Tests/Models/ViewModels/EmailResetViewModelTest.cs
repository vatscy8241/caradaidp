﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// EmailResetViewModelテストクラス。
    /// </summary>
    /// <remarks>
    /// メールアドレスの形式チェックについては、EmailAddressJPAttributeTestで網羅しているため省略し、
    /// 正常系と異常系を１つずつのみ確認するものとする。
    /// </remarks>
    [TestClass]
    public class EmailResetViewModelTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private EmailResetViewModel target;

        private bool isLastValidate = false;

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            target = new EmailResetViewModel();
        }

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestMethod]
        public void EmailResetViewModel_バリデーションチェック_異常系_必須チェック_CARADA_ID()
        {
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";
            target.Email = "a@a.com";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.Required, "CARADA ID"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
        }

        [TestMethod]
        public void EmailResetViewModel_バリデーションチェック_異常系_必須チェック__パスワード()
        {
            target.CaradaId = "test1234";
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";
            target.Email = "a@a.com";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.Required, "パスワード"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
        }

        [TestMethod]
        public void EmailResetViewModel_バリデーションチェック_異常系_必須チェック_秘密の質問()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.Answer = "あ";
            target.Email = "a@a.com";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.Required, "秘密の質問"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
        }

        [TestMethod]
        public void EmailResetViewModel_正常系_バリデーションチェック_答え()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Email = "a@a.com";

            // 文字数が1文字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";

            var results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 文字数が50文字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "　、。，．・：；？！゛゜´｀¨＾￣＿ヽヾゝゞ〃仝々〆〇ー―‐／＼～∥｜…‥‘’“”（）〔〕［］｛｝〈"; // 50文字

            results = DoValidate();

            Assert.IsTrue(isLastValidate);
            Assert.AreEqual(target.Answer.Length, 50);

            // 文字数が10文字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "　、。，．かきくけこ"; // 10文字

            results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 全角ひらがな
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あいうえおかきくけこ";

            results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 全角記号許容されるもの
            target.SecurityQuestionSelectId = 1;
            target.Answer = "ー―±×÷＝≠＜＞≦￥";

            results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 全て混在
            target.SecurityQuestionSelectId = 1;
            target.Answer = "　、。，．・：；？！゛゜´｀¨＾￣＿ヽヾゝゞ〃仝々〆ぉおかがきぎくぐけげこごさざしじすずせ【】〉《》";

            results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailResetViewModel_異常系_バリデーションチェック_答えの形式が不正文字()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Email = "a@a.com";

            // 文字数が51文字以上
            target.SecurityQuestionSelectId = 1;
            target.Answer = "ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはひふへほ"; // 51文字

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);
            Assert.AreEqual(target.Answer.Length, 51);


            // ASCII全文字について1文字ずつ調査する
            // !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
            // 0x21-0x7e (intでは 33-126 )
            for (var asciiCode = 33; asciiCode <= 126; asciiCode++)
            {
                target.SecurityQuestionSelectId = 1;
                target.Answer = ((char)asciiCode).ToString();

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);
            }

            // 半角数値（全文字）
            target.SecurityQuestionSelectId = 1;
            target.Answer = "1234567890";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 半角英字（全文字）
            target.SecurityQuestionSelectId = 1;
            target.Answer = "abcdEFGH";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 半角記号（全文字）
            target.SecurityQuestionSelectId = 1;
            target.Answer = "[]:@-^/\\";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 半角数値、半角英字、半角記号混在
            target.SecurityQuestionSelectId = 1;
            target.Answer = "12345abcd[]:@-678^/\\EF90GH";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 全角半角混在
            target.SecurityQuestionSelectId = 1;
            target.Answer = "12345abcd[]:@-678^/\\EF90GH";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 全角カタカナ
            target.SecurityQuestionSelectId = 1;
            target.Answer = "アイウエオカキクケコ";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 全角英字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "Ａｂｃｄｅｆ";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailResetViewModel_バリデーションチェック_異常系_必須チェック_答え()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Email = "a@a.com";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.Required, "答え"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
        }

        [TestMethod]
        public void EmailResetViewModel_形式チェック_正常系_RFC違反のドットを含むアドレス_新しいメールアドレス()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";
            target.Email = "hoge..foo.@docomo.ne.jp";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }
        [TestMethod]
        public void EmailResetViewModel_バリデーションチェック_正常系_境界値_最大文字数以内_新しいメールアドレス()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";
            target.Email = "1234567890123456789012345678901234567890123456789012345678901234@1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678.example.com"; // 255文字

            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
            Assert.AreEqual(target.Email.Length, 255);
        }

        [TestMethod]
        public void EmailResetViewModel_形式チェック_異常系_使用できない文字_新しいメールアドレス()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";
            target.Email = "a[a@b.";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "新しいメールアドレス"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
        }

        [TestMethod]
        public void EmailResetViewModel_バリデーションチェック_異常系_境界値_最大文字数超過_新しいメールアドレス()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";
            target.Email = "1234567890123456789012345678901234567890123456789012345678901234@12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789.example.com"; // 256文字

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.MaxLength, "新しいメールアドレス", "255"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
            Assert.IsTrue(target.Email.Length > 255);
        }

        [TestMethod]
        public void EmailResetViewModel_バリデーションチェック_異常系_必須チェックエラー_新しいメールアドレス()
        {
            target.CaradaId = "test1234";
            target.Password = "pass1234";
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.Required, "新しいメールアドレス"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);

        }
    }
}
