﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using MTI.CaradaIdProvider.Web.Resource;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// EmailChangeViewModelテストクラス。
    /// </summary>
    /// <remarks>
    /// メールアドレスの形式チェックについては、EmailAddressJPAttributeTestで網羅しているため省略し、
    /// 正常系と異常系を１つずつのみ確認するものとする。
    /// </remarks>
    [TestClass]
    public class EmailChangeViewModelTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private EmailChangeViewModel target;

        private bool isLastValidate = false;

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            target = new EmailChangeViewModel();
            // ケース記述を簡略にするため、デフォルト値を仕込んでおく。
            target.NewEmail = "default@example.com";
            target.Password = "defaultpassw0rd";
        }

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestMethod]
        public void EmailChangeViewModel_形式チェック_正常系_RFC違反のドットを含むアドレス()
        {
            target.NewEmail = "hoge..foo.@docomo.ne.jp";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }
        [TestMethod]
        public void EmailChangeViewModel_バリデーションチェック_正常系_境界値_最大文字数以内()
        {
            target.NewEmail = "1234567890123456789012345678901234567890123456789012345678901234@1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678.example.com"; // 255文字
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
            Assert.AreEqual(target.NewEmail.Length, 255);
        }

        [TestMethod]
        public void EmailChangeViewModel_形式チェック_異常系_使用できない文字()
        {
            target.NewEmail = "a[a@b.";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "新しいメールアドレス"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
        }

        [TestMethod]
        public void EmailChangeViewModel_バリデーションチェック_異常系_境界値_最大文字数超過()
        {
            target.NewEmail = "1234567890123456789012345678901234567890123456789012345678901234@12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789.example.com"; // 256文字

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.MaxLength, "新しいメールアドレス", "255"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
            Assert.IsTrue(target.NewEmail.Length > 255);
        }

        [TestMethod]
        public void EmailChangeViewModel_バリデーションチェック_異常系_必須チェックエラー()
        {
            {
                target.NewEmail = "";
                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual(string.Format(ErrorMessages.Required, "新しいメールアドレス"), results[0].ErrorMessage);
                Assert.AreEqual(results.Count, 1);
            }
            {
                target.NewEmail = "test@example.com";
                target.Password = "";
                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual(string.Format(ErrorMessages.Required, "パスワード"), results[0].ErrorMessage);
                Assert.AreEqual(results.Count, 1);
            }
            {
                target.NewEmail = "";
                target.Password = "";
                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual(string.Format(ErrorMessages.Required, "新しいメールアドレス"), results[0].ErrorMessage);
                Assert.AreEqual(string.Format(ErrorMessages.Required, "パスワード"), results[1].ErrorMessage);
                Assert.AreEqual(results.Count, 2);
            }

        }
    }
}
