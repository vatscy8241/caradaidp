﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// PasswordChangeViewModelTestテストクラス
    /// </summary>
    [TestClass]
    public class PasswordChangeViewModelTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private PasswordChangeViewModel target;

        private bool isLastValidate = false;

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestInitialize]
        public void TestInitialize()
        {
            target = new PasswordChangeViewModel();
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_正常系_エラーとならないこと()
        {
            target.CurrentPassword = "1111111a";
            target.NewPassword = "1111111b";
            target.NewPasswordConfirm = "1111111b";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_正常系_最大()
        {
            target.CurrentPassword = "Aa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            target.NewPassword = "Bb000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            target.NewPasswordConfirm = "Bb000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_異常系_現在のパスワードの必須チェックエラー()
        {
            target.NewPassword = "1111111a";
            target.NewPasswordConfirm = "1111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "現在のパスワード"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_異常系_新しいパスワードの必須チェックエラー()
        {
            target.CurrentPassword = "1111111b";
            target.NewPasswordConfirm = "1111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "新しいパスワード"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_異常系_新しいパスワード確認用の必須チェックエラー()
        {
            target.CurrentPassword = "1111111b";
            target.NewPassword = "1111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "新しいパスワード（確認用）"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_異常系_新しいパスワードの桁数が足りない()
        {
            target.CurrentPassword = "1111111b";
            target.NewPassword = "111111a";
            target.NewPasswordConfirm = "111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.MinLength, "新しいパスワード", "8"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_新しいパスワードの桁数が多い()
        {
            target.CurrentPassword = "1111111b";
            target.NewPassword = "a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            target.NewPasswordConfirm = "a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.MaxLength, "新しいパスワード", "128"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_異常系_新しいパスワードと確認用が異なる()
        {
            target.CurrentPassword = "1111111b";
            target.NewPassword = "1111111a";
            target.NewPasswordConfirm = "1111111b";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Compare, "新しいパスワード（確認用）", "新しいパスワード"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordChangeViewModel_バリデーションチェック_異常系_新しいパスワード_使用不可能文字()
        {
            var verifyCode = "1111111b";
            var passWord = "";

            // 数字のみ
            passWord = "01234567";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "新しいパスワード"), results[0].ErrorMessage);

            // 英字のみ
            passWord = "azbcAzbg";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "新しいパスワード"), results[0].ErrorMessage);

            // 記号のみ
            passWord = "!\"#$%&'()=~|-^\\@[;:],./`{+*}<>?_";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "新しいパスワード"), results[0].ErrorMessage);

            // 数字と記号のみ
            passWord = "!\"#$%&'()=1~|-222^\\@[;39:],./9`{+*}<>?_";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "新しいパスワード"), results[0].ErrorMessage);

            // 英字と記号のみ
            passWord = "a!\"#$z%&dd'()=~|-bb^\\@[;:],./`{+d*}<>?_g";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "新しいパスワード"), results[0].ErrorMessage);

            // 全角英字が混入
            passWord = "azＡcAzb1a";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.ChangePasswordChar), results[0].ErrorMessage);

            // 全角数字が混入
            passWord = "01234５91a";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.ChangePasswordChar), results[0].ErrorMessage);

            // 全角記号が混入
            passWord = "!\"#$％&'()=~|-^\\@[;:],./`{+*}<>?_1a";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.ChangePasswordChar), results[0].ErrorMessage);

            // 許容されていない記号(半角スペース)が混入
            passWord = "1a!\"#$ &'()=~|-^\\@[;:],./`{+*}<>?_1a";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.ChangePasswordChar), results[0].ErrorMessage);
        }

        [TestMethod]

        public void PasswordChangeViewModel_バリデーションチェック_正常系_新しいパスワード_許容される()
        {
            var verifyCode = "1111111b";
            var passWord = "";

            // [a-z](後方一致)
            passWord = "0900000z";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](後方一致)
            passWord = "0900000G";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](前方一致)
            passWord = "a0909090";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](前方一致)
            passWord = "A0909090";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](中間一致)
            passWord = "0aaazaa9";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](中間一致)
            passWord = "0AAAZAA9";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z]+\d+[a-z](前後一致)
            passWord = "a0123456z";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z]+\d+[A-Z](前後一致)
            passWord = "a0123456z";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号使用(前方一致)
            passWord = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~aB19Bz";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号使用(後方一致)
            passWord = "aB19Bz!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号使用(中間一致)
            passWord = "aB19Bz!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~aB19Bz";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号連続使用
            passWord = "aB19Bz!!!abz9d";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 使用可能形式を使用
            passWord = "aB19Zz!\"#$%&'D(Z9)4*1+,3-.a9/:;<=>?b@[\\d]^_`{|}~";
            target.CurrentPassword = verifyCode;
            target.NewPassword = passWord;
            target.NewPasswordConfirm = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);
        }
    }
}
