﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// LoginViewModelのテストクラス
    /// </summary>
    [TestClass]
    public class LoginViewModelTest
    {
        private LoginViewModel target;

        private bool isLastValidate = false;

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new LoginViewModel();
        }

        [TestMethod]
        public void ログイン画面_正常系_バリデーションチェック()
        {
            target.CaradaId = "testId";
            target.Password = "password";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void ログイン画面_異常系_バリデーションチェック_CARADAIDの必須エラー()
        {
            target.Password = "password";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "CARADA ID"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void ログイン画面_異常系_バリデーションチェック_パスワードの必須エラー()
        {
            target.CaradaId = "testId";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "パスワード"), results[0].ErrorMessage);
        }
    }
}
