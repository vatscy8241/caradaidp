﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// TwoFactorLoginViewModelテストクラス
    /// </summary>
    [TestClass]
    public class TwoFactorLoginViewModelTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private TwoFactorLoginViewModel target;

        private bool isLastValidate = false;

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new TwoFactorLoginViewModel();
        }

        [TestMethod]
        public void TwoFactorLogin_正常系_バリデーションチェック()
        {
            target.VerifyCode = "123456";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void TwoFactorLogin_異常系_バリデーションチェック_認証コードの必須エラー()
        {
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "認証コード"), results[0].ErrorMessage);
        }
    }
}
