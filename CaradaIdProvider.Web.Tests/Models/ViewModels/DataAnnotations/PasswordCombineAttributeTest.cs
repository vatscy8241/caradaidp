﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels.DataAnnotations
{
    /// <summary>
    /// PasswordCombineAttributeテストクラス
    /// </summary>
    [TestClass]
    public class PasswordCombineAttributeTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private TestModel target;

        private bool isLastValidate = false;

        private class TestModel
        {
            [PasswordCombineAttribute]
            public string Password { get; set; }
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new TestModel();
        }

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestMethod]
        public void PasswordCombine_正常系_半角英数1文字ずつ()
        {
            // (ASCII) 0-9 => (int) 48-57
            for (var asciiCodeNum = 48; asciiCodeNum <= 57; asciiCodeNum++)
            {
                var numChar = ((char)asciiCodeNum).ToString();

                // (ASCII) A-Z => (int) 65-90
                for (var asciiCodeAlphabet = 65; asciiCodeAlphabet <= 90; asciiCodeAlphabet++)
                {
                    var alphabetChar = ((char)asciiCodeAlphabet).ToString();
                    target.Password = numChar + alphabetChar;
                    var results = DoValidate();
                    Assert.IsTrue(isLastValidate);

                    target.Password = alphabetChar + numChar;
                    results = DoValidate();
                    Assert.IsTrue(isLastValidate);
                }
                // (ASCII) a-z => (int) 97-122
                for (var asciiCodeAlphabet = 97; asciiCodeAlphabet <= 122; asciiCodeAlphabet++)
                {
                    var alphabetChar = ((char)asciiCodeAlphabet).ToString();
                    target.Password = numChar + alphabetChar;
                    var results = DoValidate();
                    Assert.IsTrue(isLastValidate);

                    target.Password = alphabetChar + numChar;
                    results = DoValidate();
                    Assert.IsTrue(isLastValidate);
                }
            }
        }

        [TestMethod]
        public void PasswordCombine_正常系_半角英数1文字ずつとASCII全文字()
        {
            // ASCII全文字について1文字ずつ調査する
            // !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
            // 0x21-0x7e (intでは 33-126 )

            // 後方ASCII
            for (var asciiCode = 33; asciiCode <= 126; asciiCode++)
            {
                target.Password = "z9" + ((char)asciiCode).ToString();

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }

            // 中間ASCII
            for (var asciiCode = 33; asciiCode <= 126; asciiCode++)
            {
                target.Password = "z" + ((char)asciiCode).ToString() + "8";

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }

            // 前方ASCII
            for (var asciiCode = 33; asciiCode <= 126; asciiCode++)
            {
                target.Password = ((char)asciiCode).ToString() + "h4";

                var results = DoValidate();

                Assert.IsTrue(isLastValidate);
            }
        }

        [TestMethod]
        public void PasswordCombine_正常系_半角英数1文字ずつと全角英字()
        {
            target.Password = "Ａ1a";
            var results = DoValidate();
            Assert.IsTrue(isLastValidate);

            target.Password = "1aａ";
            results = DoValidate();
            Assert.IsTrue(isLastValidate);

            target.Password = "1ｚa";
            results = DoValidate();
            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordCombine_正常系_半角英数1文字ずつと全角数字()
        {
            target.Password = "７1a";
            var results = DoValidate();
            Assert.IsTrue(isLastValidate);

            target.Password = "1a９";
            results = DoValidate();
            Assert.IsTrue(isLastValidate);

            target.Password = "1５a";
            results = DoValidate();
            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordCombine_正常系_半角英数1文字ずつ_全角記号()
        {
            target.Password = "＃1a";
            var results = DoValidate();
            Assert.IsTrue(isLastValidate);

            target.Password = "1a～";
            results = DoValidate();
            Assert.IsTrue(isLastValidate);

            target.Password = "1＠a";
            results = DoValidate();
            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordCombine_異常系_値がNull()
        {
            target.Password = null;
            var results = DoValidate();
            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordCombine_異常系_半角英字のみ()
        {
            target.Password = "abcDEfg";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "Password"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordCombine_異常系_半角数字のみ()
        {
            target.Password = "12345688";
            var results = DoValidate();
            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "Password"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordCombine_異常系_半角英数が1文字ずつではない()
        {
            // ASCII全文字について調査する（1文字ずつ）
            // !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
            // 0x21-0x7e (intでは 33-126 )
            for (var asciiCode = 33; asciiCode <= 126; asciiCode++)
            {
                target.Password = ((char)asciiCode).ToString();

                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "Password"), results[0].ErrorMessage);
            }

            // ASCII全文字について調査する（同一文字2文字ずつ）
            for (var asciiCode = 33; asciiCode <= 126; asciiCode++)
            {
                target.Password = ((char)asciiCode).ToString() + ((char)asciiCode).ToString();

                var results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "Password"), results[0].ErrorMessage);
            }
        }
    }
}
