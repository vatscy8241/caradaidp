﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels.DataAnnotations
{
    [TestClass]
    public class SecurityAnswerAttributeTest
    {
        private SecurityAnswerAttribute target;

        [TestInitialize]
        public void Initialize()
        {
            target = new SecurityAnswerAttribute();
        }

        [TestMethod]
        public void IsValid_正常系_null()
        {
            Assert.IsTrue(target.IsValid(null));
        }

        [TestMethod]
        public void IsValid_異常系_文字列でない()
        {
            Assert.IsFalse(target.IsValid(1));
        }

        [TestMethod]
        public void IsValid_正常系_空文字()
        {
            Assert.IsTrue(target.IsValid(""));
        }

        [TestMethod]
        public void IsValid_正常系_許容文字1文字()
        {
            // やる意味はないが
            Assert.IsTrue(target.IsValid("あ"));
        }

        [TestMethod]
        public void IsValid_正常系_許容文字だけで構成されている()
        {
            Assert.IsTrue(target.IsValid("　、。，．・：；？！゛゜´｀¨＾￣＿ヽヾゝゞ〃仝々〆〇ー―‐／＼～∥｜…‥‘’“”（）〔〕［］｛｝〈〉《》「」『』【】＋－±×÷＝≠＜＞≦≧∞∴♂♀°′″℃￥＄￠￡％＃＆＊＠§☆★○●◎◇ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをん"));
        }

        [TestMethod]
        public void IsValid_異常系_使用不能文字1文字()
        {
            Assert.IsFalse(target.IsValid("⑩"));
        }

        [TestMethod]
        public void IsValid_異常系_使用不能文字を含む()
        {
            Assert.IsFalse(target.IsValid("あああ㌧㌦ああ"));
        }
    }
}
