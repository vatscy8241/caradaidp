﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Resource;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels.DataAnnotations
{
    /// <summary>
    /// EmailAddressJPAttribute テストクラス。
    /// </summary>
    [TestClass]
    public class EmailAddressJPAttributeTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private TestModel target;

        private bool isLastValidate = false;

        private class TestModel
        {
            [EmailAddressJP]
            public string Email { get; set; }
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new TestModel();
        }

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ローカル部ドット無し()
        {
            target.Email = "hoge@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ローカル部先頭以外ドット連続()
        {
            target.Email = "hoge..huge@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ローカル部先頭以外ドット連続at直前ドット()
        {
            target.Email = "hoge.@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ローカル部quotedstring以外で使える全記号()
        {
            target.Email = "!#$%&'*+-/=?^_`.{|}~@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }
        [TestMethod]
        public void EmailAddressJP_正常系_ローカル部quotedstringで使える全記号()
        {
            // ローカル部: !#$%&'*+-/=?^_`.{|}~()<>[]:;@,. \\\" をクオートしたもの。
            target.Email = "\"!#$%&'*+-/=?^_`.{|}~()<>[]:;@,. \\\\\\\"\"@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_大文字あり()
        {
            target.Email = "hoGe@examPle.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_サブドメインあり()
        {
            target.Email = "hoge@sub-net.example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_サブドメインあり_末尾ドット()
        {
            target.Email = "hoge@sub-net.example.com.";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ホスト部FQDN末尾ルートドットあり()
        {
            target.Email = "hoge@sub-example.com.";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ローカル部at直前ドット連続()
        {
            target.Email = "hoge..@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_複合パターン1()
        {
            target.Email = "000+hoge..boo._+9ho.@sa-sa.co.jp.";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_複合パターン2()
        {
            target.Email = "user+mailbox/department=shipping@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ホスト部で使える記号あり()
        {
            target.Email = "hoge@exampl-_~e.example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ホスト部で使える記号あり_記号重複()
        {
            target.Email = "hoge@exampl----_~e.example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ホスト部_最終ラベル以外のラベル末尾記号()
        {
            target.Email = "hoge@exampl-.example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ホスト部_最終ラベル直前以外の連続ドット()
        {
            target.Email = "hoge@example..example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_ホスト部_最終ラベル先頭末尾以外の記号()
        {
            target.Email = "hoge@example.c--o-m";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_複合パターン3()
        {
            target.Email = "user+mailbox/department=shipping@example.com";
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_正常系_バリデート対象オブジェクト無し()
        {
            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_メール文字列空文字()
        {
            target.Email = "";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_atマークなし()
        {
            target.Email = "example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部は末尾以外に最低ドット１つ必要()
        {
            target.Email = "a@a.";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_末尾にはドット１つのみ許容()
        {
            target.Email = "a@example.invalid..";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部は末尾以外に最低ドット１つ必要2()
        {
            target.Email = "12345678901234567890123456789012345678901234567890123456789012345@invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }
        [TestMethod]
        public void EmailAddressJP_異常系_先頭ドット()
        {
            target.Email = ".hoge@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quoteが閉じていない()
        {
            target.Email = "\"hoge@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_始め小括弧()
        {
            target.Email = "hoge(foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_終わり小括弧()
        {
            target.Email = "hoge)foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_左不等号()
        {
            target.Email = "hoge<foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_右不等号()
        {
            target.Email = "hoge>foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_左大括弧()
        {
            target.Email = "hoge[foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_右大括弧()
        {
            target.Email = "hoge]foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_コロン()
        {
            target.Email = "hoge:foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_セミコロン()
        {
            target.Email = "hoge;foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_atmark()
        {
            target.Email = "hoge@foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_コンマ()
        {
            target.Email = "hoge,foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quotedstring外では使用できない記号_スペース()
        {
            target.Email = "hoge foo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quoteが開始されてない()
        {
            target.Email = "hoge\"@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_quoteがローカル部途中で終わる()
        {
            target.Email = "\"hoge\"boo@example.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部先頭が記号()
        {
            target.Email = "hoge@-example.com";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部最後のラベル直前にドット連続()
        {
            target.Email = "hoge@example..invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ラベル末尾が記号()
        {
            target.Email = "hoge@example-.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_最終ラベル末尾が記号()
        {
            target.Email = "hoge@example-..ne.j-";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_感嘆符()
        {
            target.Email = "hoge@exampl!e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_二重引用符()
        {
            target.Email = "hoge@exampl\"e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_シャープ()
        {
            target.Email = "hoge@exampl#e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_ドル記号()
        {
            target.Email = "hoge@exampl$e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_パーセント()
        {
            target.Email = "hoge@exampl%e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_アンド記号()
        {
            target.Email = "hoge@exampl&e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_一重引用符()
        {
            target.Email = "hoge@exampl'e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_左括弧()
        {
            target.Email = "hoge@exampl(e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_右括弧()
        {
            target.Email = "hoge@exampl)e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_アスタリスク()
        {
            target.Email = "hoge@exampl*e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_プラス記号()
        {
            target.Email = "hoge@exampl+e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_カンマ()
        {
            target.Email = "hoge@exampl,e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_スラッシュ()
        {
            target.Email = "hoge@exampl/e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_コロン()
        {
            target.Email = "hoge@exampl:e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_セミコロン()
        {
            target.Email = "hoge@exampl;e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_左不等号()
        {
            target.Email = "hoge@exampl<e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_等号()
        {
            target.Email = "hoge@exampl=e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_右不等号()
        {
            target.Email = "hoge@exampl>e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_疑問符()
        {
            target.Email = "hoge@exampl?e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_ドメインリテラル以外の左ブラケット()
        {
            target.Email = "hoge@exampl[e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_バックスラッシュ()
        {
            target.Email = "hoge@exampl\\e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_ドメインリテラル以外の右ブラケット()
        {
            target.Email = "hoge@exampl]e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_アクサン()
        {
            target.Email = "hoge@exampl^e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_バッククオート()
        {
            target.Email = "hoge@exampl`e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_左中括弧()
        {
            target.Email = "hoge@exampl{e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_縦線()
        {
            target.Email = "hoge@exampl|e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部使用不可能文字_右中括弧()
        {
            target.Email = "hoge@exampl}e.invalid";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部アドレスリテラルIPv4()
        {
            target.Email = "hoge@[192.51.100.0.1]";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void EmailAddressJP_異常系_ホスト部アドレスリテラルIPv6()
        {
            target.Email = "hoge@[IPv6:2001:db8:ffff:ffff:ffff:ffff:ffff:ffff]";
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "Email"), results[0].ErrorMessage);
        }
    }
}

