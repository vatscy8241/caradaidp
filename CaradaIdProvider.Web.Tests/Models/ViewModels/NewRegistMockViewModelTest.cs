﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers.Mock
{
    /// <summary>
    /// NewRegistMockViewModelテストクラス
    /// </summary>
    [TestClass]
    public class NewRegistMockViewModelTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private NewRegistMockViewModel target;

        private bool isLastValidate = false;

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            target = new NewRegistMockViewModel();
        }

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestMethod]
        public void 新規登録Mock_バリデーションチェック_正常系_境界値_最少文字数()
        {
            target.CaradaId = "a";
            target.Password = "0000000a";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void 新規登録Mock_バリデーションチェック_正常系_境界値_最大()
        {
            var caradaId = "test0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000-0.0_test.co.jp";
            var passWord = "Aa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);
            Assert.AreEqual(caradaId.Length, 128);
            Assert.AreEqual(passWord.Length, 128);
        }

        [TestMethod]
        public void 新規登録Mock_バリデーションチェック_異常系_境界値_CARADA_IDの最大文字数超過エラー()
        {
            var caradaId = "test00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000-0.0_test.co.jp";
            var passWord = "Aa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

            target.CaradaId = caradaId;
            target.Password = passWord;

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(caradaId.Length, 129);
            Assert.AreEqual(passWord.Length, 128);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.MaxLength, "CARADA ID", "128"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void 新規登録Mock_バリデーションチェック_異常系_境界値_パスワードの最小文字数未満エラー()
        {
            var caradaId = "a";
            var passWord = "1234tes";

            target.CaradaId = caradaId;
            target.Password = passWord;

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(caradaId.Length, 1);
            Assert.AreEqual(passWord.Length, 7);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.MinLength, "パスワード", "8"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void 新規登録Mock_バリデーションチェック_異常系_境界値_パスワードの最大文字数超過エラー()
        {
            var caradaId = "a";
            var passWord = "Aa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

            target.CaradaId = caradaId;
            target.Password = passWord;

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(caradaId.Length, 1);
            Assert.AreEqual(passWord.Length, 129);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.MaxLength, "パスワード", "128"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void 新規登録Mock_バリデーションチェック_異常系_CARADA_ID必須エラー()
        {
            var caradaId = "";
            var passWord = "test1234";

            target.CaradaId = caradaId;
            target.Password = passWord;

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "CARADA ID"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void 新規登録Mock_バリデーションチェック_異常系_パスワード必須エラー()
        {
            var caradaId = "test1234";

            target.CaradaId = caradaId;
            target.Password = "";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "パスワード"), results[0].ErrorMessage);
        }

        [TestMethod]

        public void 新規登録Mock_バリデーションチェック_異常系_CARADA_ID_使用不可能文字エラーパターン()
        {
            var passWord = "test1234";

            // 記号で始まるID
            target.CaradaId = "!test1234";
            target.Password = passWord;
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 記号の連続使用(._)
            target.CaradaId = "te._st1234";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 記号の連続使用(-.)
            target.CaradaId = "test1234-.";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 記号の連続使用(--)
            target.CaradaId = "t--est1234";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 許可されていない半角記号
            target.CaradaId = "t/est1234";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 全角英字混入
            target.CaradaId = "teＳt1234";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 全角数字混入
            target.CaradaId = "test12３4";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 半角大文字英字混入
            target.CaradaId = "tEst12３4";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);

            // 半角記号で1文字のみ
            target.CaradaId = ".";
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.CaradaIdChar), results[0].ErrorMessage);
        }

        [TestMethod]

        public void 新規登録Mock_バリデーションチェック_正常系_CARADA_ID_許容される正常系()
        {
            var passWord = "test1234";

            // [a-z](前方一致)
            target.CaradaId = "az0000-0.0_090";
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](後方一致)
            target.CaradaId = "09aza.a_a-aa";
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](中間一致)
            target.CaradaId = "0a.aaaz_a-aa9";
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z]+\d+[a-z](前後一致)
            target.CaradaId = "a19111.11_11-11z";
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 使用可能形式を全て使用
            target.CaradaId = "a.b_aza-ad901.a_";
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]

        public void 新規登録Mock_バリデーションチェック_異常系_パスワード_使用不可能文字エラーパターン()
        {
            var caradaId = "test1234";
            var passWord = "";

            //数字のみ
            passWord = "01234567";
            target.CaradaId = caradaId;
            target.Password = passWord;
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);

            //英字のみ
            passWord = "azbcAzbg";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);

            //記号のみ
            passWord = "!\"#$%&'()=~|-^\\@[;:],./`{+*}< >?_";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);

            //数字と記号のみ
            passWord = "!\"#$%&'()=1~|-222^\\@[;39:],./9`{+*}< >?_";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);

            //英字と記号のみ
            passWord = "a!\"#$z%&dd'()=~|-bb^\\@[;:],./`{+d*}< >?_g";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);

            //全角英字が混入
            passWord = "azＡcAzbg";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);
            //全角数字が混入
            passWord = "01234５91";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);

            //全角記号が混入
            passWord = "!\"#$％&'()=~|-^\\@[;:],./`{+*}< >?_";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);

            //許容されていない記号(半角スペース)が混入
            passWord = "!\"#$ &'()=~|-^\\@[;:],./`{+*}< >?_";
            target.CaradaId = caradaId;
            target.Password = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordFormatError), results[0].ErrorMessage);
        }

        [TestMethod]

        public void 新規登録Mock_バリデーションチェック_正常系_パスワード_許容される正常系()
        {
            var caradaId = "test1234";
            var passWord = "";


            // [a-z](前方一致)
            passWord = "0900000z";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](前方一致)
            passWord = "0900000G";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](後方一致)
            passWord = "a0909090";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](後方一致)
            passWord = "A0909090";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](中間一致)
            passWord = "0aaazaa9";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](中間一致)
            passWord = "0AAAZAA9";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z]+\d+[a-z](前後一致)
            passWord = "a0123456z";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z]+\d+[A-Z](前後一致)
            passWord = "a0123456z";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号使用(前方一致)
            passWord = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~aB19Bz";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号使用(後方一致)
            passWord = "aB19Bz!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号フル使用(中間一致)
            passWord = "aB19Bz!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号連続使用
            passWord = "aB19Bz!!!abz9d";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 使用可能形式を使用
            passWord = "aB19Zz!\"#$%&'D(Z9)4*1+,3-.a9/:;<=>?b@[\\d]^_`{|}~";
            target.CaradaId = caradaId;
            target.Password = passWord;

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }
    }
}
