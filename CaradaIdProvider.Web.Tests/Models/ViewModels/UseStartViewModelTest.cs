﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using MTI.CaradaIdProvider.Web.Resource;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// UseStartViewModelテストクラス。
    /// </summary>
    /// <remarks>
    /// メールアドレスの形式チェックについては、EmailAddressJPAttributeTestで網羅しているため省略し、
    /// 正常系と異常系を１つずつのみ確認するものとする。
    /// </remarks>
    [TestClass]
    public class UseStartViewModelTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private UseStartViewModel target;

        private bool isLastValidate = false;

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            target = new UseStartViewModel();
        }

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestMethod]
        public void UseStartViewModel_形式チェック_正常系_RFC違反のドットを含むアドレス()
        {
            target.Email = "hoge..foo.@docomo.ne.jp";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }
        [TestMethod]
        public void UseStartViewModel_バリデーションチェック_正常系_境界値_最大文字数以内()
        {
            target.Email = "1234567890123456789012345678901234567890123456789012345678901234@1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678.example.com"; // 255文字

            var results = DoValidate();

            Assert.IsTrue(isLastValidate);
            Assert.AreEqual(target.Email.Length, 255);
        }

        [TestMethod]
        public void UseStartViewModel_形式チェック_異常系_使用できない文字()
        {
            target.Email = "a[a@b.";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.EmailFormat, "メールアドレス"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
        }

        [TestMethod]
        public void UseStartViewModel_バリデーションチェック_異常系_境界値_最大文字数超過()
        {
            target.Email = "1234567890123456789012345678901234567890123456789012345678901234@12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789.example.com"; // 256文字

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.MaxLength, "メールアドレス", "255"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);
            Assert.IsTrue(target.Email.Length > 255);
        }

        [TestMethod]
        public void UseStartViewModel_バリデーションチェック_異常系_必須チェックエラー()
        {

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(string.Format(ErrorMessages.Required, "メールアドレス"), results[0].ErrorMessage);
            Assert.AreEqual(results.Count, 1);

        }
    }
}
