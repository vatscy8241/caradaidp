﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// SecurityQuestionRegistViewModelのテストクラス
    /// </summary>
    [TestClass]
    public class SecurityQuestionRegistViewModelTest
    {
        private SecurityQuestionRegistViewModel target;

        private bool isLastValidate = false;

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestInitialize]
        public void Initialize()
        {
            target = new SecurityQuestionRegistViewModel();
        }

        [TestMethod]
        public void 秘密の質問登録画面_正常系_バリデーションチェックOK()
        {
            // 文字数が1文字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あ";

            var results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 文字数が50文字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "　、。，．・：；？！゛゜´｀¨＾￣＿ヽヾゝゞ〃仝々〆〇ー―‐／＼～∥｜…‥‘’“”（）〔〕［］｛｝〈"; // 50文字

            results = DoValidate();

            Assert.IsTrue(isLastValidate);
            Assert.AreEqual(target.Answer.Length, 50);

            // 文字数が10文字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "　、。，．あいうえお"; // 10文字

            results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 全角ひらがな
            target.SecurityQuestionSelectId = 1;
            target.Answer = "あいうえおかきくけこ";

            results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 全角記号許容されるもの
            target.SecurityQuestionSelectId = 1;
            target.Answer = "ー＝￥「：」＠？。￥";

            results = DoValidate();

            Assert.IsTrue(isLastValidate);

            // 全て混在
            target.SecurityQuestionSelectId = 1;
            target.Answer = "　、。，．・：；？！゛゜´｀¨＾￣＿ヽヾゝゞ〃仝々〆ぉおかがきぎくぐけげこごさざしじすずせ【】〉《》";

            results = DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void 秘密の質問登録画面_異常系_バリデーションチェック_秘密の質問の必須エラー()
        {
            target.Answer = "てすとこたえ";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionError, "秘密の質問"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void 秘密の質問登録画面_異常系_バリデーションチェック_答えの必須エラー()
        {
            target.SecurityQuestionSelectId = 1;

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "答え"), results[0].ErrorMessage);

            target.SecurityQuestionSelectId = 1;
            target.Answer = " ";
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "答え"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void 秘密の質問登録画面_異常系_バリデーションチェック_答えの形式が不正文字()
        {
            // 文字数が51文字以上
            target.SecurityQuestionSelectId = 1;
            target.Answer = "ああああああああああああああああああああああああああああああああああああああああああああああああああい"; // 51文字

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);
            Assert.AreEqual(target.Answer.Length, 51);


            // ASCII全文字について1文字ずつ調査する
            // !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
            // 0x21-0x7e (intでは 33-126 )
            for (var asciiCode = 33; asciiCode <= 126; asciiCode++)
            {
                target.SecurityQuestionSelectId = 1;
                target.Answer = ((char)asciiCode).ToString();

                results = DoValidate();

                Assert.IsFalse(isLastValidate);
                Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);
            }

            // 半角数値（全文字）
            target.SecurityQuestionSelectId = 1;
            target.Answer = "1234567890";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 半角英字（全文字）
            target.SecurityQuestionSelectId = 1;
            target.Answer = "abcdEFGH";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 半角記号（全文字）
            target.SecurityQuestionSelectId = 1;
            target.Answer = "[]:@-^/\\";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 全角カタカナ
            target.SecurityQuestionSelectId = 1;
            target.Answer = "アイウエオカキクケコ";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);

            // 全角英字
            target.SecurityQuestionSelectId = 1;
            target.Answer = "Ａｂｃｄｅｆ";

            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.SecurityQuestionAnswerChar, "答え"), results[0].ErrorMessage);
        }
    }
}
