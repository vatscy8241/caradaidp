﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Tests.Models.ViewModels
{
    /// <summary>
    /// PasswordResetViewModelTestテストクラス
    /// </summary>
    [TestClass]
    public class PasswordResetViewModelTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private PasswordResetViewModel target;

        private bool isLastValidate = false;

        private List<ValidationResult> DoValidate()
        {
            var context = new ValidationContext(target, null, null);
            var results = new List<ValidationResult>();
            isLastValidate = Validator.TryValidateObject(target, context, results, true);
            return results;
        }

        [TestInitialize]
        public void TestInitialize()
        {
            target = new PasswordResetViewModel();
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_正常系_エラーとならないこと()
        {
            target.VerifyCode = "12345";
            target.ResetPassword = "1111111a";
            target.ConfirmResetPassword = "1111111a";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_正常系_最大()
        {
            target.VerifyCode = "12345";
            target.ResetPassword = "Aa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            target.ConfirmResetPassword = "Aa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

            DoValidate();

            Assert.IsTrue(isLastValidate);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_異常系_認証コードの必須チェックエラー()
        {
            target.ResetPassword = "1111111a";
            target.ConfirmResetPassword = "1111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "認証コード"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_異常系_再設定パスワードの必須チェックエラー()
        {
            target.VerifyCode = "12345";
            target.ConfirmResetPassword = "1111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "再設定パスワード"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_異常系_再設定パスワード確認用の必須チェックエラー()
        {
            target.VerifyCode = "12345";
            target.ResetPassword = "1111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Required, "再設定パスワード（確認用）"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_異常系_再設定パスワードの桁数が足りない()
        {
            target.VerifyCode = "12345";
            target.ResetPassword = "111111a";
            target.ConfirmResetPassword = "111111a";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.MinLength, "再設定パスワード", "8"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_異常系_再設定パスワードの桁数が多い()
        {
            target.VerifyCode = "12345";
            target.ResetPassword = "a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
            target.ConfirmResetPassword = "a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.MaxLength, "再設定パスワード", "128"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_異常系_再設定パスワードと確認用が異なる()
        {
            target.VerifyCode = "12345";
            target.ResetPassword = "1111111a";
            target.ConfirmResetPassword = "1111111b";

            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.Compare, "再設定パスワード（確認用）", "再設定パスワード"), results[0].ErrorMessage);
        }

        [TestMethod]
        public void PasswordResetViewModel_バリデーションチェック_異常系_再設定パスワード_使用不可能文字()
        {
            var verifyCode = "12345";
            var passWord = "";

            // 数字のみ
            passWord = "01234567";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            var results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "再設定パスワード"), results[0].ErrorMessage);

            // 英字のみ
            passWord = "azbcAzbg";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "再設定パスワード"), results[0].ErrorMessage);

            // 記号のみ
            passWord = "!\"#$%&'()=~|-^\\@[;:],./`{+*}<>?_";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "再設定パスワード"), results[0].ErrorMessage);

            // 数字と記号のみ
            passWord = "!\"#$%&'()=1~|-222^\\@[;39:],./9`{+*}<>?_";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "再設定パスワード"), results[0].ErrorMessage);

            // 英字と記号のみ
            passWord = "a!\"#$z%&dd'()=~|-bb^\\@[;:],./`{+d*}<>?_g";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordCombine, "再設定パスワード"), results[0].ErrorMessage);

            // 全角英字が混入
            passWord = "azＡcAzb1a";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordChar), results[0].ErrorMessage);

            // 全角数字が混入
            passWord = "01234５91a";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordChar), results[0].ErrorMessage);

            // 全角記号が混入
            passWord = "!\"#$％&'()=~|-^\\@[;:],./`{+*}<>?_1a";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordChar), results[0].ErrorMessage);

            // 許容されていない記号(半角スペース)が混入
            passWord = "1a!\"#$ &'()=~|-^\\@[;:],./`{+*}<>?_1a";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            results = DoValidate();

            Assert.IsFalse(isLastValidate);
            Assert.AreEqual(String.Format(Resource.ErrorMessages.PasswordChar), results[0].ErrorMessage);
        }

        [TestMethod]

        public void PasswordResetViewModel_バリデーションチェック_正常系_再設定パスワード_許容される()
        {
            var verifyCode = "12345";
            var passWord = "";

            // [a-z](後方一致)
            passWord = "0900000z";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](後方一致)
            passWord = "0900000G";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](前方一致)
            passWord = "a0909090";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](前方一致)
            passWord = "A0909090";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z](中間一致)
            passWord = "0aaazaa9";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z](中間一致)
            passWord = "0AAAZAA9";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [a-z]+\d+[a-z](前後一致)
            passWord = "a0123456z";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // [A-Z]+\d+[A-Z](前後一致)
            passWord = "a0123456z";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号使用(前方一致)
            passWord = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~aB19Bz";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号使用(後方一致)
            passWord = "aB19Bz!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号フル使用(中間一致)
            passWord = "aB19Bz!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 記号連続使用
            passWord = "aB19Bz!!!abz9d";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);

            // 使用可能形式を使用
            passWord = "aB19Zz!\"#$%&'D(Z9)4*1+,3-.a9/:;<=>?b@[\\d]^_`{|}~";
            target.VerifyCode = verifyCode;
            target.ResetPassword = passWord;
            target.ConfirmResetPassword = passWord;
            DoValidate();

            Assert.IsTrue(isLastValidate);
        }
    }
}
