﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using System;
using System.Collections.Generic;

namespace MTI.CaradaIdProvider.Web.Tests.Models
{
    /// <summary>
    /// ApiTargetInfoのテストクラス
    /// </summary>
    [TestClass]
    public class ApiTargetInfoTest
    {
        private ApiTargetInfo target;

        [TestInitialize]
        public void TestInitialize()
        {
            target = new ApiTargetInfo();
        }

        [TestMethod]
        public void Load_正常系_正しいJsonの場合Dictionaryにloadされること()
        {
            var json = @"[{""targetName"":""name1"",""issuer"":""issuer1"",""secretKey"":""key1""},{""targetName"":""name2"",""issuer"":""issuer2"",""secretKey"":""key2""}]";
            target.Load(json);

            var dic = ReflectionAccessor.GetField<ApiTargetInfo>("targets").GetValue(target) as Dictionary<string, ApiTarget>;

            Assert.IsTrue(dic.ContainsKey("NAME1"));
            Assert.AreEqual("name1", dic["NAME1"].TargetName);
            Assert.AreEqual("issuer1", dic["NAME1"].Issuer);
            Assert.AreEqual("key1", dic["NAME1"].SecretKey);

            Assert.IsTrue(dic.ContainsKey("NAME2"));
            Assert.AreEqual("name2", dic["NAME2"].TargetName);
            Assert.AreEqual("issuer2", dic["NAME2"].Issuer);
            Assert.AreEqual("key2", dic["NAME2"].SecretKey);
        }

        [TestMethod]
        public void Load_異常系_正しくないJsonの場合例外となること()
        {
            try
            {
                target.Load(@"{""targetName"":""aaaa""}");
                Assert.Fail("例外が発生しない");
            }
            catch (Exception) { }
        }

        [TestMethod]
        public void GetApiTarget_正常系_API対象情報を取得できること()
        {
            var json = @"[{""targetName"":""name1"",""issuer"":""issuer1"",""secretKey"":""key1""},{""targetName"":""name2"",""issuer"":""issuer2"",""secretKey"":""key2""}]";
            target.Load(json);

            var info = target.GetApiTarget("name1");

            Assert.IsNotNull(info);
            Assert.AreEqual("name1", info.TargetName);
            Assert.AreEqual("issuer1", info.Issuer);
            Assert.AreEqual("key1", info.SecretKey);
        }

        [TestMethod]
        public void GetApiTarget_正常系_大文字小文字を区別せずAPI対象情報を取得できること()
        {
            var json = @"[{""targetName"":""name1"",""issuer"":""issuer1"",""secretKey"":""key1""},{""targetName"":""name2"",""issuer"":""issuer2"",""secretKey"":""key2""}]";
            target.Load(json);

            var info = target.GetApiTarget("Name2");

            Assert.IsNotNull(info);
            Assert.AreEqual("name2", info.TargetName);
            Assert.AreEqual("issuer2", info.Issuer);
            Assert.AreEqual("key2", info.SecretKey);
        }

        [TestMethod]
        public void GetApiTarget_正常系_対象が存在しない場合nullが返ること()
        {
            var json = @"[{""targetName"":""name1"",""issuer"":""issuer1"",""secretKey"":""key1""},{""targetName"":""name2"",""issuer"":""issuer2"",""secretKey"":""key2""}]";
            target.Load(json);

            Assert.IsNull(target.GetApiTarget("Name3"));
        }

        [TestMethod]
        public void GetApiTarget_正常系_キーを指定しない場合nullが返ること()
        {
            var json = @"[{""targetName"":""name1"",""issuer"":""issuer1"",""secretKey"":""key1""},{""targetName"":""name2"",""issuer"":""issuer2"",""secretKey"":""key2""}]";
            target.Load(json);

            Assert.IsNull(target.GetApiTarget(null));
        }
    }
}
