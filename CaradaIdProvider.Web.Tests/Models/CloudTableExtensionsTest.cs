﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using MTI.CaradaIdProvider.Web.Models;
using System;

namespace MTI.CaradaIdProvider.Web.Tests.Models
{
    /// <summary>
    /// クラウドストレージのUT。
    /// CloudTableをモックにできないのでいったんエミュレータを使用して実装。
    /// 同じ理由でretryのテストができない。
    /// </summary>
    [TestClass]
    public class CloudTableExtensionsTest
    {
        private CloudTable SomeTable;

        public class SomeTableEntity : TableEntity
        {
            public int No { get; set; }
        }

        [TestInitialize]
        public void Initialize()
        {
            // エミュレータから取得
            SomeTable = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudTableClient().GetTableReference("SomeTable");
            SomeTable.CreateIfNotExists();
        }

        [TestCleanup]
        public void Cleanup()
        {
            SomeTable.DeleteIfExists();
        }

        [TestMethod]
        public void InsertEntity_標準テスト()
        {
            var insertEntity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            var result = CloudTableExtensions.InsertEntity(SomeTable, insertEntity);

            // 検索して確認
            var getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.PartitionKey, "pkey");
            Assert.AreEqual(getEntity.RowKey, "rkey");
        }

        [TestMethod]
        public void DeleteEntity_標準テスト()
        {
            ITableEntity entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            var result = CloudTableExtensions.InsertEntity(SomeTable, entity);

            // Delete
            var result2 = CloudTableExtensions.DeleteEntity(SomeTable, entity);

            // 検索されないこと
            SomeTableEntity getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.IsNull(getEntity);
        }

        [TestMethod]
        public void DeleteEntity_削除対象がない場合()
        {
            var entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            try
            {
                var result = CloudTableExtensions.DeleteEntity(SomeTable, entity);
            }
            catch (ArgumentException)
            {
                // ArgumentException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void ReplaceEntity_標準テスト()
        {
            var entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            var result = CloudTableExtensions.InsertEntity(SomeTable, entity);

            entity.No = 2;

            // Replace
            var result2 = CloudTableExtensions.ReplaceEntity(SomeTable, entity);

            // 検索して確認
            var getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.No, 2);
        }

        [TestMethod]
        public void ReplaceEntity_更新対象がない場合()
        {
            var entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };
            try
            {
                // Replace
                var result = CloudTableExtensions.ReplaceEntity(SomeTable, entity);
            }
            catch (ArgumentException)
            {
                // ArgumentException
                Assert.IsTrue(true);
            }
        }

        [TestMethod]
        public void InsertOrReplaceEntity_対象がない場合()
        {
            var entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // InsertOrReplace
            var result = CloudTableExtensions.InsertOrReplaceEntity(SomeTable, entity);

            // 検索して確認
            var getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.PartitionKey, "pkey");
            Assert.AreEqual(getEntity.RowKey, "rkey");
        }

        [TestMethod]
        public void InsertOrReplaceEntity_対象が存在する場合()
        {
            var entity = new SomeTableEntity()
            {
                PartitionKey = "pkey",
                RowKey = "rkey"
            };

            // Insert
            var result = CloudTableExtensions.InsertEntity(SomeTable, entity);

            entity.No = 2;

            // InsertOrReplace
            var result2 = CloudTableExtensions.InsertOrReplaceEntity(SomeTable, entity);

            // 検索して確認
            var getEntity = CloudTableExtensions.RetrieveEntity<SomeTableEntity>(SomeTable, "pkey", "rkey");

            Assert.AreEqual(getEntity.PartitionKey, "pkey");
            Assert.AreEqual(getEntity.RowKey, "rkey");
            Assert.AreEqual(getEntity.No, 2);
        }
    }
}
