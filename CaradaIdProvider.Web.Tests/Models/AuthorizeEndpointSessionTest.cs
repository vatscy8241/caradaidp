﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Models;

namespace MTI.CaradaIdProvider.Web.Tests.Models
{
    /// <summary>
    /// AuthorizeEndpointSessionのテストクラス
    /// </summary>
    [TestClass]
    public class AuthorizeEndpointSessionTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private AuthorizeEndpointSession target;

        [TestMethod]
        public void GetParameterizedReturnUrl_正常最大パラメータ数()
        {
            target = new AuthorizeEndpointSession()
            {
                ReturnUrl = "https://test/oauth2/auth",
                ClientId = "testclientid",
                Scope = "openid",
                ResponseType = "code",
                RedirectUri = "https://test/auth/response",
                State = "state001"
            };

            Assert.AreEqual("https://test/oauth2/auth?client_id=testclientid&scope=openid&response_type=code&redirect_uri=https%3A%2F%2Ftest%2Fauth%2Fresponse&state=state001",
                target.GetParameterizedReturnUrl());
        }

        [TestMethod]
        public void GetParameterizedReturnUrl_正常最小パラメータ数()
        {
            target = new AuthorizeEndpointSession()
            {
                ReturnUrl = "https://test/oauth2/auth",
                ClientId = "testclientid",
                Scope = "openid",
                ResponseType = "code",
                RedirectUri = "https://test/auth/response"
            };

            Assert.AreEqual("https://test/oauth2/auth?client_id=testclientid&scope=openid&response_type=code&redirect_uri=https%3A%2F%2Ftest%2Fauth%2Fresponse",
                target.GetParameterizedReturnUrl());
        }
    }
}
