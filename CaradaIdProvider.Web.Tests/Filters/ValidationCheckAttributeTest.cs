﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Filters;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Filters
{
    [TestClass]
    public class ValidationCheckAttributeTest
    {
        private ValidationCheckAttribute target;

        private class DummyController : Controller { }

        [TestInitialize]
        public void Initialize()
        {
            target = new ValidationCheckAttribute();
        }

        [TestMethod]
        public void OnActionExecuting_バリデーションNG_TempData_ViewDataなし()
        {
            var filterContext = new ActionExecutingContext();
            filterContext.Controller = new DummyController();
            filterContext.Controller.ViewData.ModelState.AddModelError("dummy", "ダミーエラー");

            target.OnActionExecuting(filterContext);

            var result = filterContext.Result as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.TempData.Count);
        }

        [TestMethod]
        public void OnActionExecuting_バリデーションNG_TempData_ViewDataあり()
        {
            var filterContext = new ActionExecutingContext();
            filterContext.Controller = new DummyController();
            filterContext.Controller.ViewData.ModelState.AddModelError("dummy", "ダミーエラー");
            filterContext.Controller.TempData["hoge"] = "hoge";
            filterContext.Controller.ViewData["name"] = "name";

            target.OnActionExecuting(filterContext);

            var result = filterContext.Result as ViewResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.TempData.Count);
            Assert.AreEqual("hoge", result.TempData["hoge"]);
            Assert.AreEqual("name", result.ViewData["name"]);
        }

        [TestMethod]
        public void OnActionExecuting_バリデーションOK()
        {
            var filterContext = new ActionExecutingContext();
            filterContext.Controller = new DummyController();

            target.OnActionExecuting(filterContext);

            Assert.IsNull(filterContext.Result);

        }
    }
}
