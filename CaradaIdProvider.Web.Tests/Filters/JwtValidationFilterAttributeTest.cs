﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using MTI.CaradaIdProvider.Web.Filters;
using MTI.CaradaIdProvider.Web.Tests.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using System.Web.Script.Serialization;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers
{
    /// <summary>
    /// JwtValidationFilterAttributeのテストクラス
    /// </summary>
    [TestClass]
    public class JwtValidationFilterAttributeTest
    {
        private const string _testIssuer = "TestIssuer";
        private const string _testSecretkey = "J>sK;4z^MEpC[F8@V|Eo9YDYwoQ|m09r*#}|45S#EP0={G85(Hn@2MN@Vq>ZoaY_";

        private class JwtValidationFilterForTest : JwtValidationFilterAttribute
        {
            public HttpResponseMessage Delegate(HttpActionContext context, string errorCode)
            {
                return base.CreateApplicationResponse(context, errorCode);
            }
        }

        private class JwtForTest<T> : JwtBase<T>
            where T : ConditionBase, new()
        {
            private Func<string, string> _getSecretKey;

            public JwtForTest(Func<string, string> getSecretKey)
            {
                _getSecretKey = getSecretKey;
            }

            protected override string GetSecretKey(string issuer)
            {
                return _getSecretKey(issuer);
            }
        }

        private class NoAttributeCondition : ConditionBase
        {
            public string Param1 { get; set; }
            public int? Param2 { get; set; }

            [JsonIgnore]
            public bool IsValidateCalled { get; private set; }

            public override IEnumerable<ValidationResult> Validate(ValidationContext context)
            {
                IsValidateCalled = true;

                if (string.IsNullOrEmpty(Param1))
                {
                    yield return new ValidationResult("param1_must_not_be_null");
                }

                if (Param2 != null && Param2 < 0)
                {
                    yield return new ValidationResult("param2_must_be_positive_or_null");
                }
            }
        }

        private string CreateToken(string iss, long? iat, long? exp, long? nbf, object parameters)
        {
            var credentials = new SigningCredentials
            (
                GetHmacSha256SigningKey(_testSecretkey),
                SecurityAlgorithms.HmacSha256Signature,
                SecurityAlgorithms.Sha256Digest
            );

            var jwtHeader = new JwtHeader(credentials);
            var jwtPayload = new JwtPayload();

            var claims = new List<Claim>();
            if (iss != null)
            {
                claims.Add(new Claim("iss", iss));
            }
            if (iat != null)
            {
                claims.Add(new Claim("iat", iat.ToString(), ClaimValueTypes.Integer64));
            }
            if (exp != null)
            {
                claims.Add(new Claim("exp", exp.ToString(), ClaimValueTypes.Integer64));
            }
            if (nbf != null)
            {
                claims.Add(new Claim("nbf", nbf.ToString(), ClaimValueTypes.Integer64));
            }

            jwtPayload.AddClaims(claims);

            var token = new JwtSecurityToken(jwtHeader, jwtPayload);

            if (parameters != null)
            {
                token.Payload.Add("params", parameters);
            }

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private SymmetricSecurityKey GetHmacSha256SigningKey(string key)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            if (keyBytes.Length < 64)
            {
                Array.Resize(ref keyBytes, 64);
            }

            return new InMemorySymmetricSecurityKey(keyBytes);
        }

        private long ToUnixTime(DateTime t)
        {
            return (long)t.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }

        [TestMethod]
        public void CreateApplicationResponse_正常系_エラーレスポンスが生成される_JWTの不正()
        {
            var filter = new JwtValidationFilterForTest();
            var context = new HttpActionContext();
            context.ControllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage(HttpMethod.Get, "http://www.abc.ef/"));
            context.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, context.ControllerContext.Configuration);

            context.Request.Content = new StringContent("test");

            var condition = new Jwt<RegistUserAddCondition>();

            context.ActionArguments.Add("condition", condition);

            var result = filter.Delegate(context, "invalid_client");

            var jsonSerializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
            var resultContent = result.Content;
            var resultJson = JToken.Parse(resultContent.ReadAsStringAsync().Result);
            var resultCaradaId = (string)resultJson["carada_id"];
            var resultError = (string)resultJson["error"];
            var resultErrorDescription = (string)resultJson["error_description"];

            Assert.IsNotNull(result);
            Assert.AreEqual("", resultCaradaId);
            Assert.AreEqual<string>("invalid_client", resultError);
            Assert.AreEqual<string>("JWTが不正です", resultErrorDescription);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [TestMethod]
        public void CreateApplicationResponse_正常系_エラーレスポンスが生成される_JWTパラメータの不正_RegistUserAddCondition()
        {
            var filter = new JwtValidationFilterForTest();
            var context = new HttpActionContext();
            context.ControllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage(HttpMethod.Get, "http://www.abc.ef/"));
            context.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, context.ControllerContext.Configuration);

            context.Request.Content = new StringContent("test");

            var parameter = new RegistUserAddCondition
            {
                ClientId = "91111ClientTest",
                CaradaId = "testUser"
            };

            var jwt = new Jwt<RegistUserAddCondition>();
            ReflectionAccessor.GetProperty<JwtBase<RegistUserAddCondition>>("Parameter").SetValue(jwt, parameter);
            context.ActionArguments.Add("condition", jwt);

            var result = filter.Delegate(context, "invalid_client");

            var jsonSerializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
            var resultContent = result.Content;
            var resultJson = JToken.Parse(resultContent.ReadAsStringAsync().Result);
            var resultCaradaId = (string)resultJson["carada_id"];
            var resultError = (string)resultJson["error"];
            var resultErrorDescription = (string)resultJson["error_description"];

            Assert.IsNotNull(result);
            Assert.AreEqual("testUser", resultCaradaId);
            Assert.AreEqual<string>("invalid_client", resultError);
            Assert.AreEqual<string>("JWTのパラメーターが不正です", resultErrorDescription);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [TestMethod]
        public void CreateApplicationResponse_正常系_エラーレスポンスが生成される_JWTパラメータの不正_ResetUserCondition()
        {
            var filter = new JwtValidationFilterForTest();
            var context = new HttpActionContext();
            context.ControllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage(HttpMethod.Get, "http://www.abc.ef/"));
            context.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, context.ControllerContext.Configuration);

            context.Request.Content = new StringContent("test");

            var parameter = new ResetUserCondition
            {
                CaradaId = "",
                Initialization = "1"
            };

            var jwt = new Jwt<ResetUserCondition>();
            ReflectionAccessor.GetProperty<JwtBase<ResetUserCondition>>("Parameter").SetValue(jwt, parameter);
            context.ActionArguments.Add("condition", jwt);

            var result = filter.Delegate(context, "invalid_carada_id");

            var jsonSerializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
            var resultContent = result.Content;
            var resultJson = JToken.Parse(resultContent.ReadAsStringAsync().Result);
            var resultCaradaId = (string)resultJson["carada_id"];
            var resultError = (string)resultJson["error"];
            var resultErrorDescription = (string)resultJson["error_description"];

            Assert.IsNotNull(result);
            Assert.AreEqual("", resultCaradaId);
            Assert.AreEqual<string>("invalid_carada_id", resultError);
            Assert.AreEqual<string>("JWTのパラメーターが不正です", resultErrorDescription);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
