﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Filters;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Filters
{
    [TestClass]
    public class SessionTimeoutFilterAttributeTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private SessionTimeoutFilterAttribute target;

        private ActionExecutingContext filterContext;
        private Mock<HttpContextBase> httpContext;
        private HttpSessionStateBase httpSession;


        [TestInitialize]
        public void Initialize()
        {
            httpSession = new MockHttpSession();
            httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(f => f.Session).Returns(httpSession);
            filterContext = new ActionExecutingContext();
            filterContext.HttpContext = httpContext.Object;
        }

        [TestCleanup]
        public void ClieanUp()
        {
            httpContext.VerifyAll();
        }

        [TestMethod]
        public void OnActionExecuting_引数なし_セッション有効()
        {
            target = new SessionTimeoutFilterAttribute();
            httpSession.Add("test", "test");

            target.OnActionExecuting(filterContext);
            var result = filterContext.Result as RedirectToRouteResult;
            Assert.IsNull(result);

        }

        [TestMethod]
        public void OnActionExecuting_引数なし_セッション無効()
        {
            target = new SessionTimeoutFilterAttribute();

            target.OnActionExecuting(filterContext);
            var result = filterContext.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            var routeValue = result.RouteValues;
            Assert.IsNotNull(routeValue);
            Assert.AreEqual(2, routeValue.Count);
            Assert.AreEqual("Error", routeValue["controller"]);
            Assert.AreEqual("Timeout", routeValue["action"]);
        }

        [TestMethod]
        public void OnActionExecuting_引数あり_対象セッション名()
        {
            target = new SessionTimeoutFilterAttribute("test");
            httpSession.Add("test", "test");

            target.OnActionExecuting(filterContext);
            var result = filterContext.Result as RedirectToRouteResult;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void OnActionExecuting_引数あり_セッション無効()
        {
            target = new SessionTimeoutFilterAttribute("test");

            target.OnActionExecuting(filterContext);
            var result = filterContext.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            var routeValue = result.RouteValues;
            Assert.IsNotNull(routeValue);
            Assert.AreEqual(2, routeValue.Count);
            Assert.AreEqual("Error", routeValue["controller"]);
            Assert.AreEqual("Timeout", routeValue["action"]);
        }

        [TestMethod]
        public void OnActionExecuting_引数あり_対象外セッション名()
        {
            target = new SessionTimeoutFilterAttribute("test");
            httpSession.Add("test2", "test");

            target.OnActionExecuting(filterContext);
            var result = filterContext.Result as RedirectToRouteResult;
            Assert.IsNotNull(result);
            var routeValue = result.RouteValues;
            Assert.IsNotNull(routeValue);
            Assert.AreEqual(2, routeValue.Count);
            Assert.AreEqual("Error", routeValue["controller"]);
            Assert.AreEqual("Timeout", routeValue["action"]);
        }
    }
}
