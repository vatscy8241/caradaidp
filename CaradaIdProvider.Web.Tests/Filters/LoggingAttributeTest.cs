﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Filters;
using NLog;
using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Filters
{
    /// <summary>
    /// LoggingAttributeのテストクラス
    /// </summary>
    [TestClass]
    public class LoggingAttributeTest
    {
        /// <summary>
        /// テスト対象
        /// </summary>
        private LoggingAttribute target;
        private ActionExecutingContext filterContext;
        private Mock<HttpContextBase> mockHttpContext;
        private Mock<HttpRequestBase> mockRequest;
        private Mock<UnvalidatedRequestValuesBase> mockUnvalidated;
        private Type type;
        private FieldInfo field;
        private Mock<ILogger> log;


        [TestInitialize]
        public void TestInitialize()
        {
            target = new LoggingAttribute();
            mockHttpContext = new Mock<HttpContextBase>();
            filterContext = new ActionExecutingContext();
            mockRequest = new Mock<HttpRequestBase>() { CallBase = true };
            mockUnvalidated = new Mock<UnvalidatedRequestValuesBase>();
            mockHttpContext.Setup(ctx => ctx.Request).Returns(mockRequest.Object);
            mockRequest.Setup(req => req.Unvalidated).Returns(mockUnvalidated.Object);
            filterContext.HttpContext = mockHttpContext.Object;
            type = target.GetType();
            field = type.GetField("LOG", BindingFlags.Static | BindingFlags.NonPublic);
            log = new Mock<ILogger>();
        }

        [TestMethod]
        public void OnActionExecuting_POSTでパラメータありの場合()
        {
            // ログ内容のテスト
            var expected = "[ACCESS] parameters:[aaa=\"bbb\"]";
            log.Setup(l => l.Info(expected));
            field.SetValue(target, log.Object);

            mockRequest.Setup(r => r.HttpMethod).Returns("POST");
            var form = new NameValueCollection();
            form.Add("aaa", "bbb");
            mockRequest.Setup(r => r.Form).Returns(form);
            mockUnvalidated.Setup(u => u.Form).Returns(form);

            target.OnActionExecuting(filterContext);

            mockHttpContext.VerifyAll();
            log.VerifyAll();
            mockRequest.VerifyAll();
            mockUnvalidated.VerifyAll();
        }

        [TestMethod]
        public void OnActionExecuting_POSTでパラメータなしの場合()
        {
            // ログ内容のテスト
            var expected = "[ACCESS]";
            log.Setup(l => l.Info(expected));
            field.SetValue(target, log.Object);

            mockRequest.Setup(r => r.HttpMethod).Returns("POST");
            var form = new NameValueCollection();
            mockRequest.Setup(r => r.Form).Returns(form);
            mockUnvalidated.Setup(u => u.Form).Returns(form);

            target.OnActionExecuting(filterContext);

            mockHttpContext.VerifyAll();
            log.VerifyAll();
            mockRequest.Verify(v => v.HttpMethod);
            mockRequest.Verify(v => v.Form);
            mockRequest.Verify(v => v.Unvalidated, Times.Never);
            mockUnvalidated.Verify(v => v.QueryString, Times.Never);
        }

        [TestMethod]
        public void OnActionExecuting_GETでパラメータありの場合()
        {
            // ログ内容のテスト
            var expected = "[ACCESS] parameters:[aaa=\"bbb\"]";
            log.Setup(l => l.Info(expected));
            field.SetValue(target, log.Object);

            mockRequest.Setup(r => r.HttpMethod).Returns("GET");
            var queryString = new NameValueCollection();
            queryString.Add("aaa", "bbb");
            mockRequest.Setup(r => r.QueryString).Returns(queryString);
            mockUnvalidated.Setup(u => u.QueryString).Returns(queryString);

            target.OnActionExecuting(filterContext);

            mockHttpContext.VerifyAll();
            log.VerifyAll();
            mockRequest.VerifyAll();
            mockUnvalidated.VerifyAll();
        }

        [TestMethod]
        public void OnActionExecuting_GETでパラメータなしの場合()
        {
            // ログ内容のテスト
            var expected = "[ACCESS]";
            log.Setup(l => l.Info(expected));
            field.SetValue(target, log.Object);

            mockRequest.Setup(r => r.HttpMethod).Returns("GET");
            var queryString = new NameValueCollection();
            mockRequest.Setup(r => r.QueryString).Returns(queryString);
            mockUnvalidated.Setup(u => u.QueryString).Returns(queryString);

            target.OnActionExecuting(filterContext);

            mockHttpContext.VerifyAll();
            log.VerifyAll();
            mockRequest.Verify(v => v.HttpMethod);
            mockRequest.Verify(v => v.QueryString);
            mockRequest.Verify(v => v.Unvalidated, Times.Never);
            mockUnvalidated.Verify(v => v.QueryString, Times.Never);
        }
    }
}
