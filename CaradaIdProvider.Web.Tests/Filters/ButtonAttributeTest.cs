﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Filters;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Controllers
{
    /// <summary>
    /// ButtonAttributeのテストクラス。
    /// </summary>
    [TestClass]
    public class ButtonAttributeTest
    {
        private ButtonAttribute target;
        private Mock<IValueProvider> valueProviderMock;

        public class DummyController : Controller
        {
            public DummyController(IValueProvider valueProvider)
            {
                this.ValueProvider = valueProvider;
            }

            public ActionResult Index()
            {
                return null;
            }

            public ActionResult Index2()
            {
                return null;
            }
        }

        [TestInitialize]
        public void Initialize()
        {
            valueProviderMock = new Mock<IValueProvider>();
            target = new ButtonAttribute("Index");
        }

        [TestMethod]
        public void IsValidForRequest_指定のメソッドの場合trueが返ること()
        {
            valueProviderMock.Setup(c => c.GetValue("Index")).Returns(new Mock<ValueProviderResult>().Object);

            var context = new ControllerContext();
            context.Controller = new DummyController(valueProviderMock.Object);

            var methodInfo = typeof(DummyController).GetMethod("Index");

            Assert.IsTrue(target.IsValidForRequest(context, methodInfo));
            valueProviderMock.VerifyAll();
        }

        [TestMethod]
        public void IsValidForRequest_指定のメソッドでない場合falseが返ること()
        {
            valueProviderMock.Setup(c => c.GetValue("Index")).Returns(new Mock<ValueProviderResult>().Object);

            var context = new ControllerContext();
            context.Controller = new DummyController(valueProviderMock.Object);

            var methodInfo = typeof(DummyController).GetMethod("Index2");

            Assert.IsTrue(target.IsValidForRequest(context, methodInfo));
            valueProviderMock.VerifyAll();
        }
    }
}
