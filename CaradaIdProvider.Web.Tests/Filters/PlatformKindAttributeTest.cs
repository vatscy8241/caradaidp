﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaIdProvider.Web.Filters;
using MTI.CaradaIdProvider.Web.Models;
using System;
using System.Collections.Specialized;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Tests.Filters
{
    [TestClass]
    public class PlatformKindAttributeTest
    {
        private PlatformKindAttribute target;
        private ActionExecutingContext actionExecutingContext;
        private ActionExecutedContext actionExecutedContext;

        [TestInitialize]
        public void TestInitialize()
        {
            target = new PlatformKindAttribute();
            var a = MvcMockHelpers.FakeHttpContext();
            actionExecutingContext = new ActionExecutingContext();
            actionExecutedContext = new ActionExecutedContext();
            actionExecutingContext.HttpContext = a;
            actionExecutedContext.HttpContext = a;

        }

        [TestMethod]
        public void OnActionExecuting_正常系_XPlatformKindあり()
        {
            var mockBrowser = new Mock<HttpBrowserCapabilitiesBase>();
            mockBrowser.Setup(s => s.IsMobileDevice).Returns(true);

            MvcMockHelpers.MockRequest.Setup(s => s.Headers).Returns(new NameValueCollection { { "Dummy", "Dummy" }, { "X-PlatformKind", "App_Android" } });
            MvcMockHelpers.MockRequest.Setup(s => s.Browser).Returns(mockBrowser.Object);
            MvcMockHelpers.MockRequest.Setup(s => s.Url).Returns(new Uri("http://localhost/hoge"));

            target.OnActionExecuting(actionExecutingContext);

            var model = actionExecutingContext.HttpContext.Items["Analytics"] as AnalyticsSettingsModel;

            Assert.IsNotNull(model);
            // いずれ変わります
            Assert.AreEqual<string>("UA-99999999-1", model.Google.GoogleAnalyticsId);
            Assert.AreEqual<string>("未ログイン", model.Google.Dimension1);
            Assert.AreEqual<string>("WebView", model.Google.Dimension4);
            Assert.AreEqual<string>("SP", model.Google.Dimension6);
            Assert.AreEqual<string>("App_Android", model.Google.Dimension21);
            Assert.AreEqual<string>("auto", model.Google.Domain);
            Assert.AreEqual<string>("12345", model.UserInsight.Id);
            Assert.AreEqual<string>("http://localhost", model.UserInsight.Domain);

            MvcMockHelpers.MockRequest.VerifyAll();
            mockBrowser.VerifyAll();
        }

        [TestMethod]
        public void OnActionExecuting_正常系_XPlatformKindなし_MobileDevice()
        {
            var mockBrowser = new Mock<HttpBrowserCapabilitiesBase>();
            mockBrowser.Setup(s => s.IsMobileDevice).Returns(true);
            MvcMockHelpers.MockRequest.Setup(s => s.Headers).Returns(new NameValueCollection { });
            MvcMockHelpers.MockRequest.Setup(s => s.Browser).Returns(mockBrowser.Object);
            MvcMockHelpers.MockRequest.Setup(s => s.Url).Returns(new Uri("http://localhost/"));

            target.OnActionExecuting(actionExecutingContext);

            var model = actionExecutingContext.HttpContext.Items["Analytics"] as AnalyticsSettingsModel;

            Assert.IsNotNull(model);
            Assert.AreEqual<string>("UA-99999999-1", model.Google.GoogleAnalyticsId);
            Assert.AreEqual<string>("未ログイン", model.Google.Dimension1);
            Assert.AreEqual<string>("Web", model.Google.Dimension4);
            Assert.AreEqual<string>("SP", model.Google.Dimension6);
            Assert.IsNull(model.Google.Dimension21);
            Assert.AreEqual<string>("12345", model.UserInsight.Id);
            Assert.AreEqual<string>("http://localhost", model.UserInsight.Domain);

            MvcMockHelpers.MockRequest.VerifyAll();
            mockBrowser.VerifyAll();
        }

        [TestMethod]
        public void OnActionExecuting_正常系_XPlatformKindなし_MobileDevice以外()
        {
            var mockBrowser = new Mock<HttpBrowserCapabilitiesBase>();
            mockBrowser.Setup(s => s.IsMobileDevice).Returns(false);
            MvcMockHelpers.MockRequest.Setup(s => s.Headers).Returns(new NameValueCollection { });
            MvcMockHelpers.MockRequest.Setup(s => s.Browser).Returns(mockBrowser.Object);
            MvcMockHelpers.MockRequest.Setup(s => s.Url).Returns(new Uri("http://localhost/hoge"));

            target.OnActionExecuting(actionExecutingContext);

            var model = actionExecutingContext.HttpContext.Items["Analytics"] as AnalyticsSettingsModel;

            Assert.IsNotNull(model);
            Assert.AreEqual<string>("UA-99999999-1", model.Google.GoogleAnalyticsId);
            Assert.AreEqual<string>("未ログイン", model.Google.Dimension1);
            Assert.AreEqual<string>("Web", model.Google.Dimension4);
            Assert.AreEqual<string>("PC", model.Google.Dimension6);
            Assert.AreEqual<string>("12345", model.UserInsight.Id);
            Assert.AreEqual<string>("http://localhost", model.UserInsight.Domain);

            MvcMockHelpers.MockRequest.VerifyAll();
            mockBrowser.VerifyAll();
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void OnActionExecuted_Analyticsが取れない()
        {
            MvcMockHelpers.MockIdentity.Setup(s => s.IsAuthenticated).Returns(true);

            target.OnActionExecuted(actionExecutedContext);
            Assert.Fail("例外が発生しない");
        }

        [TestMethod]
        public void OnActionExecuted_Userが取れない()
        {
            MvcMockHelpers.MockContext.Setup(s => s.User).Returns<IPrincipal>(null);
            var model = new AnalyticsSettingsModel();
            actionExecutedContext.HttpContext.Items.Add("Analytics", model);

            target.OnActionExecuted(actionExecutedContext);
            Assert.AreNotEqual<string>("ログイン", model.Google.Dimension1);

            MvcMockHelpers.MockContext.Verify(v => v.User);
        }

        [TestMethod]
        public void OnActionExecuted_Identityが取れない()
        {
            MvcMockHelpers.MockUser.Setup(s => s.Identity).Returns<IIdentity>(null);
            var model = new AnalyticsSettingsModel();
            actionExecutedContext.HttpContext.Items.Add("Analytics", model);

            target.OnActionExecuted(actionExecutedContext);
            Assert.AreNotEqual<string>("ログイン", model.Google.Dimension1);

            MvcMockHelpers.MockUser.Verify(v => v.Identity);
        }

        [TestMethod]
        public void OnActionExecuted_正常系_未ログイン()
        {
            MvcMockHelpers.MockIdentity.Setup(s => s.IsAuthenticated).Returns(false);
            var model = new AnalyticsSettingsModel();
            actionExecutedContext.HttpContext.Items.Add("Analytics", model);

            target.OnActionExecuted(actionExecutedContext);
            Assert.AreNotEqual<string>("ログイン", model.Google.Dimension1);

            MvcMockHelpers.MockIdentity.VerifyAll();
        }

        [TestMethod]
        public void OnActionExecuted_正常系_ログイン済み()
        {
            MvcMockHelpers.MockIdentity.Setup(s => s.IsAuthenticated).Returns(true);
            var model = new AnalyticsSettingsModel();
            actionExecutedContext.HttpContext.Items.Add("Analytics", model);

            target.OnActionExecuted(actionExecutedContext);
            Assert.AreEqual<string>("ログイン", model.Google.Dimension1);

            MvcMockHelpers.MockIdentity.VerifyAll();
        }
    }
}
