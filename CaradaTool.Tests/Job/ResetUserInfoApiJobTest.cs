﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Job;
using MTI.CaradaTool.Logics;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Tests.Properties;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;

namespace MTI.CaradaTool.Tests.Job
{
    /// <summary>
    /// ResetUserInfoApiJob UTクラス.
    /// </summary>
    [TestClass]
    public class ResetUserInfoApiJobTest
    {
        private ResetUserInfoApiJob target;
        private Mock<JwtSecretInfo> secretInfoMock;
        private Mock<ToolProperties> propInfoMock;
        private Mock<JwtApiRequest> jwtRequestMock;
        private Mock<FileUtil> fileUtilMock;
        private Mock<OutputUtil> outputUtilMock;

        private FileUtil fileUtil = new FileUtil();
        private const string targetDirPath = @"..\in\";

        private static readonly string consoleSetInfilePath = "./consoleSetFile.txt";

        #region テスト前後処理

        [TestInitialize]
        public void TestInitialize()
        {
            secretInfoMock = new Mock<JwtSecretInfo>(Settings.Default.JwtSecretFilePath);
            propInfoMock = new Mock<ToolProperties>(Settings.Default.AppPropertyFilePath);
            jwtRequestMock = new Mock<JwtApiRequest>(secretInfoMock.Object, propInfoMock.Object);
            fileUtilMock = new Mock<FileUtil>();
            outputUtilMock = new Mock<OutputUtil>();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            FileHelper.DeleteDir(targetDirPath);
        }

        #endregion

        #region コンストラクタ

        [TestMethod]
        public void ResetUserInfoApiJob_正常系_コンストラクタ()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);
            Assert.AreEqual(secretInfoMock.Object, ReflectionAccessor.GetField<ResetUserInfoApiJob>("secretInfo").GetValue(target));
            Assert.AreEqual(propInfoMock.Object, ReflectionAccessor.GetField<ResetUserInfoApiJob>("propInfo").GetValue(target));
            Assert.IsNotNull(ReflectionAccessor.GetField<ResetUserInfoApiJob>("jwtRequest").GetValue(target));
        }

        #endregion

        #region CreateOutputTsvFileName

        [TestMethod]
        public void CreateOutputTsvFileName_正常系()
        {

            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            var expected = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("CreateOutputTsvFileName");
            Assert.AreEqual(expected, (string)method.Invoke(target, new object[] { }));
        }

        #endregion

        #region IsValidCaradaId

        [TestMethod]
        public void IsValidCaradaId_正常系_CARADAID128文字形式正常()
        {
            var input = "carada.id-123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_12345678";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("IsValidCaradaId");
            Assert.IsTrue((bool)method.Invoke(target, new object[] { input }));
        }

        [TestMethod]
        public void IsValidCaradaId_異常系_CARADAIDがnull()
        {
            string input = null;
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("IsValidCaradaId");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));
        }

        [TestMethod]
        public void IsValidCaradaId_異常系_CARADAID129文字()
        {
            var input = "carada.id-123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("IsValidCaradaId");

            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));
        }

        [TestMethod]
        public void IsValidCaradaId_異常系_CARADAID形式不正()
        {
            // ここでは不正文字でエラーとなるかに注力する
            // エラーになった時の関数呼び出しなどは上のケースと同じ
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);

            // 大文字英字混入
            var input = "caRada.id-1";
            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("IsValidCaradaId");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 全角英字混入
            input = "carＡada.id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 全角数字混入
            input = "caraada.id-１";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 使っても良い記号以外の記号混入(1)
            input = "caraada&id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 使っても良い記号以外の記号混入(2)
            input = "caraada.id-!";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 使っても良い記号以外の記号混入(3)
            input = "caraada.id#1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 記号から始まる
            input = "_caraada.id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 記号の連続使用
            input = "caraada..id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));
        }

        #endregion

        #region ExecuteApi

        /// <summary>
        /// ExecuteApi(正常系)CARADAID一致
        /// </summary>
        /// <remarks>SuccessProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_正常系_ステータスコード200_CARADAID一致()
        {
            var input = "carada_id-1";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "2");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'0'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input, "2" });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "パスワードリセット", "", "");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        /// <summary>
        /// ExecuteApi(正常系)CARADAID不一致
        /// </summary>
        /// <remarks>SuccessProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_正常系_ステータスコード200_CARADAID不一致()
        {
            var input = "carada_id-1";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-2','password':'hogefuga','initialization':'0'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input, "1" });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var errorDetail = string.Format("CARADA IDが合致しません。({0},{1})", input, "carada_id-2");
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "パスワードリセット", "", errorDetail);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        /// <summary>
        /// ExecuteApi(異常系)APIエラー
        /// </summary>
        /// <remarks>ApiErrorProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード400_APIエラー()
        {
            var input = "carada_id-1";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.BadRequest;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','error':'invalid_client_id','original_error':'統合認可にクライアントが見つかりません。','error_description':'クライアントIDなし'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input, "1" });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "", "", "invalid_client_id", "クライアントIDなし");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        /// <summary>
        /// ExecuteApi(異常系)メンテナンス
        /// </summary>
        /// <remarks>ApiErrorProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード503_メンテナンス()
        {
            var input = "carada_id-1";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.ServiceUnavailable;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','error':'maintenance','error_description':'現在サーバーのメンテナンス中です。'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input, "1" });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "", "", "maintenance", "現在サーバーのメンテナンス中です。");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        /// <summary>
        /// ExecuteApi(異常系)ステータスコード504→504→200
        /// </summary>
        /// <remarks>リトライ確認.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード504_504_200()
        {
            var input = "carada_id-1";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response1 = new HttpResponseMessage();
            response1.StatusCode = HttpStatusCode.GatewayTimeout;

            var response2 = new HttpResponseMessage();
            response2.StatusCode = HttpStatusCode.GatewayTimeout;

            var response3 = new HttpResponseMessage();
            response3.StatusCode = HttpStatusCode.OK;
            response3.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'1'}");

            jwtRequestMock.SetupSequence(s => s.PostJson(param)).Returns(response1).Returns(response2).Returns(response3);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input, "1" });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "2"), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "3"), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), It.IsAny<string>()), Times.Exactly(3));
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response1, "1")), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response2, "2")), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:3"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "利用開始前リセット", "", "");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        /// <summary>
        /// ExecuteApi(異常系)リトライ回数オーバー
        /// </summary>
        /// <remarks>リトライ確認.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_リトライ回数オーバー()
        {
            var input = "carada_id-1";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response1 = new HttpResponseMessage();
            response1.StatusCode = HttpStatusCode.GatewayTimeout;

            var response2 = new HttpResponseMessage();
            response2.StatusCode = HttpStatusCode.GatewayTimeout;

            var response3 = new HttpResponseMessage();
            response3.StatusCode = HttpStatusCode.GatewayTimeout;

            jwtRequestMock.SetupSequence(s => s.PostJson(param)).Returns(response1).Returns(response2).Returns(response3);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            try
            {
                var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("ExecuteApi");
                method.Invoke(target, new object[] { input, "1" });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                // Reflectionを介しているので一段目はこれ
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(ApiException));
                var apie = re.InnerException as ApiException;
                Assert.AreEqual(response3.StatusCode, apie.StatusCode);
                Assert.AreEqual(input, apie.CaradaId);
                Assert.AreEqual("サーバーエラー", apie.Message);

                jwtRequestMock.VerifyAll();

                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "2"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "3"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), It.IsAny<string>()), Times.Exactly(3));
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response1, "1")), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response2, "2")), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response3, "3")), Times.Once);
                outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);
            }
        }

        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード200_400_503_504以外()
        {
            var input = "carada_id-1";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.Forbidden;

            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            try
            {
                var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("ExecuteApi");
                method.Invoke(target, new object[] { input, "1" });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                // Reflectionを介しているので一段目はこれ
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(ApiException));
                var apie = re.InnerException as ApiException;
                Assert.AreEqual(response.StatusCode, apie.StatusCode);
                Assert.AreEqual(input, apie.CaradaId);
                Assert.AreEqual("サーバーエラー", apie.Message);

                jwtRequestMock.VerifyAll();

                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response, "1")), Times.Once);
                outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);
            }
        }

        #endregion

        #region Execute

        [TestMethod]
        public void Execute_正常系_利用開始前リセット()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            var input = "carada_id-1";
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'1'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var consoleSetList = new List<string>();
            consoleSetList.Add(input);
            consoleSetList.Add("1");
            consoleSetList.Add("y");

            CreateConsoleSetInFile(consoleSetList);

            using (var reader = new StreamReader(consoleSetInfilePath))
            {
                Console.SetIn(reader);
                // 実行
                target.Execute();
            }

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "利用開始前リセット", "", "");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        [TestMethod]
        public void Execute_正常系_パスワードリセット()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            var input = "carada_id-1";
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "0");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'0'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var consoleSetList = new List<string>();
            consoleSetList.Add(input);
            consoleSetList.Add("2");
            consoleSetList.Add("y");

            CreateConsoleSetInFile(consoleSetList);

            using (var reader = new StreamReader(consoleSetInfilePath))
            {
                Console.SetIn(reader);

                // 実行
                target.Execute();

            }

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "パスワードリセット", "", "");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        [TestMethod]
        public void Execute_正常系_キャンセル()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            var input = "carada_id-1";
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "0");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'0'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var consoleSetList = new List<string>();
            consoleSetList.Add(input);
            consoleSetList.Add("a");

            CreateConsoleSetInFile(consoleSetList);

            using (var reader = new StreamReader(consoleSetInfilePath))
            {
                Console.SetIn(reader);

                // 実行
                target.Execute();

            }

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, "処理をキャンセルしました。", (System.Exception)null), Times.Once);
        }

        [TestMethod]
        public void Execute_正常系_1度選択しキャンセル()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            var input = "carada_id-1";
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "0");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'0'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var consoleSetList = new List<string>();
            consoleSetList.Add(input);
            consoleSetList.Add("1");
            consoleSetList.Add("a");

            CreateConsoleSetInFile(consoleSetList);

            using (var reader = new StreamReader(consoleSetInfilePath))
            {
                Console.SetIn(reader);

                // 実行
                target.Execute();

            }

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, "処理をキャンセルしました。", (System.Exception)null), Times.Once);
        }

        [TestMethod]
        public void Execute_正常系_APIException()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            var input = "carada_id-1";
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'1'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Throws(new ApiException("サーバーエラー", input, HttpStatusCode.GatewayTimeout));
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var consoleSetList = new List<string>();
            consoleSetList.Add(input);
            consoleSetList.Add("1");
            consoleSetList.Add("y");

            CreateConsoleSetInFile(consoleSetList);

            using (var reader = new StreamReader(consoleSetInfilePath))
            {
                Console.SetIn(reader);
                // 実行
                target.Execute();
            }

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tリセット種別\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "", "", (int)HttpStatusCode.GatewayTimeout, "サーバーエラー");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);
        }

        [TestMethod]
        public void Execute_正常系_API実行で他のException()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "fileUtil", fileUtilMock.Object);
            var input = "carada_id-1";
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("initialization", "1");

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','initialization':'1'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Throws(new ArgumentException("引数不正エラー"));
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("ユーザー情報リセット結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputTsvPath", tsvPath);

            var consoleSetList = new List<string>();
            consoleSetList.Add(input);
            consoleSetList.Add("1");
            consoleSetList.Add("y");

            CreateConsoleSetInFile(consoleSetList);

            using (var reader = new StreamReader(consoleSetInfilePath))
            {
                Console.SetIn(reader);
                try
                {
                    // 実行
                    target.Execute();
                    Assert.Fail("例外が発生しない");
                }
                catch (System.Exception e)
                {
                    Assert.IsInstanceOfType(e, typeof(ArgumentException));
                    var ae = e as ArgumentException;
                    Assert.AreEqual("引数不正エラー", ae.Message);

                    outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
                    outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
                    outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);

                    jwtRequestMock.VerifyAll();
                    fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(It.IsAny<string>()), Times.Never);
                    // API実行で他のExceptionの際にファイルは作成されない
                }
            }
        }

        #endregion

        #region BeforeExecute

        [TestMethod]
        public void BeforeExecute_正常系()
        {
            secretInfoMock.Object.Issuer = "TestIssuer";
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);

            target.BeforeExecute();

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, string.Format("処理を開始します。Issuser ID：{0}", secretInfoMock.Object.Issuer), (System.Exception)null), Times.Once);
        }

        #endregion

        #region AfterExecute

        [TestMethod]
        public void AfterExecute_正常系_正常ログ出力()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "executeResult", true);
            target.AfterExecute();

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, "正常にリセットされました。", (System.Exception)null), Times.Once);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void AfterExecute_正常系_失敗ログ出力無し()
        {
            target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<ResetUserInfoApiJob>(target, "executeResult", false);
            target.AfterExecute();
        }

        #endregion

        #region WriteTsv

        [TestMethod]
        public void WriteTsv_異常系_引数がnullの場合()
        {
            try
            {
                target = new ResetUserInfoApiJob(secretInfoMock.Object, propInfoMock.Object);

                var method = ReflectionAccessor.GetMethod<ResetUserInfoApiJob>("WriteTsv");
                method.Invoke(target, new object[] { null });
            }
            catch (System.Exception e)
            {
                // Reflectionを介しているので一段目はこれ
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var apie = re.InnerException as AppFatalException;
                Assert.AreEqual("CARADAID発行結果TSVが出力できませんでした。", apie.Message);
            }
        }

        #endregion

        #region private method

        private void CreateTestInputFile(string filePath)
        {
            fileUtil.WriteLine(filePath, string.Format("{0}", "CARADA ID"), "Shift_JIS");
            fileUtil.WriteLine(filePath, string.Format("{0}", "carada_id-1"), "Shift_JIS");
        }
        private void CreateTestInputFileAbnormalCol(string filePath)
        {
            fileUtil.WriteLine(filePath, string.Format("{0}\t{1}", "CARADA ID", "CARADA ID TOO"), "Shift_JIS");
        }

        private void CreateConsoleSetInFile(List<string> list)
        {
            var deleteFile = new FileInfo(consoleSetInfilePath);

            // ファイルが存在しているか判断する
            if (deleteFile.Exists)
            {
                // 読み取り専用属性がある場合は、読み取り専用属性を解除する
                if ((deleteFile.Attributes & System.IO.FileAttributes.ReadOnly) == System.IO.FileAttributes.ReadOnly)
                {
                    deleteFile.Attributes = FileAttributes.Normal;
                }

                // ファイルを削除する
                deleteFile.Delete();
            }

            using (var sw = new StreamWriter(@consoleSetInfilePath, true, System.Text.Encoding.GetEncoding("Shift_Jis")))
            {
                foreach (var cm in list)
                {
                    sw.WriteLine(cm);
                }
            }
        }
        #endregion
    }
}
