﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Job;
using MTI.CaradaTool.Logics;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Tests.Properties;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;

namespace MTI.CaradaTool.Tests.Job
{
    /// <summary>
    /// UserRegistApiJob UTクラス.
    /// </summary>
    [TestClass]
    public class UserRegistApiJobTest
    {
        private UserRegistApiJob target;
        private Mock<JwtSecretInfo> secretInfoMock;
        private Mock<ToolProperties> propInfoMock;
        private Mock<JwtApiRequest> jwtRequestMock;
        private Mock<FileUtil> fileUtilMock;
        private Mock<OutputUtil> outputUtilMock;

        private FileUtil fileUtil = new FileUtil();
        private const string targetDirPath = @"..\in\";
        //private string inputFilePath = Path.Combine(targetDirPath, "C990001_20160120_CARADAID発行.tsv");

        #region テスト前後処理

        [TestInitialize]
        public void TestInitialize()
        {
            secretInfoMock = new Mock<JwtSecretInfo>(Settings.Default.JwtSecretFilePath);
            propInfoMock = new Mock<ToolProperties>(Settings.Default.AppPropertyFilePath);
            jwtRequestMock = new Mock<JwtApiRequest>(secretInfoMock.Object, propInfoMock.Object);
            fileUtilMock = new Mock<FileUtil>();
            outputUtilMock = new Mock<OutputUtil>();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            FileHelper.DeleteDir(targetDirPath);
        }

        #endregion

        #region コンストラクタ

        [TestMethod]
        public void UserRegistApiJob_正常系_コンストラクタ()
        {
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);
            Assert.AreEqual(secretInfoMock.Object, ReflectionAccessor.GetField<UserRegistApiJob>("secretInfo").GetValue(target));
            Assert.AreEqual(propInfoMock.Object, ReflectionAccessor.GetField<UserRegistApiJob>("propInfo").GetValue(target));
            Assert.IsNotNull(ReflectionAccessor.GetField<UserRegistApiJob>("jwtRequest").GetValue(target));
        }

        #endregion

        #region GetInputFileNameIfValid

        [TestMethod]
        public void GetInputFileNameIfValid_正常系()
        {
            var expected = "C990001_20160120_CARADAID発行.tsv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(expected);

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0]))).Returns(false);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
            Assert.AreEqual(expected, (string)method.Invoke(target, new object[] { }));
            fileUtilMock.VerifyAll();
        }

        [TestMethod]
        public void GetInputFileNameIfValid_異常系_ファイルが2つ以上()
        {
            var expected = "C990001_20160120_CARADAID発行.tsv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(expected);
            fileList.Add("C990002_20160120_CARADAID発行.tsv");

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0]))).Returns(false);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
                method.Invoke(target, new object[] { });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                // Reflectionを介しているので一段目はこれ
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var afe = re.InnerException as AppFatalException;
                Assert.AreEqual("CARADAID発行TSV", afe.FileName);
                Assert.AreEqual("ファイルが存在しないか、読み込めません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(tsvPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(It.IsAny<string>()), Times.Never);
            }
        }

        [TestMethod]
        public void GetInputFileNameIfValid_異常系_ファイルが0()
        {
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
                method.Invoke(target, new object[] { });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var afe = re.InnerException as AppFatalException;
                Assert.AreEqual("CARADAID発行TSV", afe.FileName);
                Assert.AreEqual("ファイルが存在しないか、読み込めません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(tsvPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(It.IsAny<string>()), Times.Never);
            }
        }

        [TestMethod]
        public void GetInputFileNameIfValid_異常系_ファイルが読み込めない()
        {
            var expected = "C990001_20160120_CARADAID発行.tsv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(expected);

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0]))).Returns(true);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
                method.Invoke(target, new object[] { });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var afe = re.InnerException as AppFatalException;
                Assert.AreEqual(expected, afe.FileName);
                Assert.AreEqual("ファイルが存在しないか、読み込めません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(tsvPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0])), Times.Once);
            }
        }

        [TestMethod]
        public void GetInputFileNameIfValid_異常系_ファイル名形式が合致しない_法人ID()
        {
            var expected = "B990001_20160120_CARADAID発行.tsv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(expected);

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0]))).Returns(false);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
                method.Invoke(target, new object[] { });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var afe = re.InnerException as AppFatalException;
                Assert.AreEqual(expected, afe.FileName);
                Assert.AreEqual("ファイル名の形式が正しくありません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(tsvPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0])), Times.Once);
            }
        }

        [TestMethod]
        public void GetInputFileNameIfValid_異常系_ファイル名形式が合致しない_年月日()
        {
            var expected = "C990001_20160199_CARADAID発行.tsv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(expected);

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0]))).Returns(false);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
                method.Invoke(target, new object[] { });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var afe = re.InnerException as AppFatalException;
                Assert.AreEqual(expected, afe.FileName);
                Assert.AreEqual("ファイル名の形式が正しくありません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(tsvPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0])), Times.Once);
            }
        }

        [TestMethod]
        public void GetInputFileNameIfValid_異常系_ファイル名形式が合致しない_固定文字列()
        {
            var expected = "C990001_20160120_CADADAID発行.tsv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(expected);

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0]))).Returns(false);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
                method.Invoke(target, new object[] { });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var afe = re.InnerException as AppFatalException;
                Assert.AreEqual(expected, afe.FileName);
                Assert.AreEqual("ファイル名の形式が正しくありません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(tsvPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0])), Times.Once);
            }
        }

        [TestMethod]
        public void GetInputFileNameIfValid_異常系_ファイル名形式が合致しない_拡張子()
        {
            var expected = "C990001_20160120_CARADAID発行.csv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(expected);

            var tsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(tsvPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0]))).Returns(false);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("GetInputFileNameIfValid");
                method.Invoke(target, new object[] { });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(AppFatalException));
                var afe = re.InnerException as AppFatalException;
                Assert.AreEqual(expected, afe.FileName);
                Assert.AreEqual("ファイル名の形式が正しくありません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(tsvPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(tsvPath, fileList[0])), Times.Once);
            }
        }

        #endregion

        #region CreateOutputTsvFileName

        [TestMethod]
        public void CreateOutputTsvFileName_正常系()
        {
            var input = "C990001_20160120_CARADAID発行.tsv";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            var expected = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("CreateOutputTsvFileName");
            Assert.AreEqual(expected, (string)method.Invoke(target, new object[] { input }));
        }

        #endregion

        #region IsValidCaradaId

        [TestMethod]
        public void IsValidCaradaId_正常系_CARADAID128文字形式正常()
        {
            var input = "carada.id-123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_12345678";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("IsValidCaradaId");
            Assert.IsTrue((bool)method.Invoke(target, new object[] { input }));
        }

        [TestMethod]
        public void IsValidCaradaId_異常系_CARADAIDがnull_初出力()
        {
            string input = null;
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("IsValidCaradaId");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "\t\t\t\tCARADA IDは必須です。", "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(0, registedCount);
            Assert.AreEqual(1, failedCount);
        }

        [TestMethod]
        public void IsValidCaradaId_異常系_CARADAIDが空_ヘッダなし()
        {
            var input = "";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "registedCount", 1);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("IsValidCaradaId");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Never);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Never);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "\t\t\t\tCARADA IDは必須です。", "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(1, registedCount);
            Assert.AreEqual(1, failedCount);
        }

        [TestMethod]
        public void IsValidCaradaId_異常系_CARADAID129文字()
        {
            var input = "carada.id-123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789_123456789";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("IsValidCaradaId");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "", "", "", "CARADA IDが不正です。");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(0, registedCount);
            Assert.AreEqual(1, failedCount);
        }

        [TestMethod]
        public void IsValidCaradaId_異常系_CARADAID形式不正()
        {
            // ここでは不正文字でエラーとなるかに注力する
            // エラーになった時の関数呼び出しなどは上のケースと同じ
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            // 大文字英字混入
            var input = "caRada.id-1";
            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("IsValidCaradaId");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 全角英字混入
            input = "carＡada.id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 全角数字混入
            input = "caraada.id-１";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 使っても良い記号以外の記号混入(1)
            input = "caraada&id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 使っても良い記号以外の記号混入(2)
            input = "caraada.id-!";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 使っても良い記号以外の記号混入(3)
            input = "caraada.id#1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 記号から始まる
            input = "_caraada.id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));

            // 記号の連続使用
            input = "caraada..id-1";
            Assert.IsFalse((bool)method.Invoke(target, new object[] { input }));
        }

        #endregion

        #region ExecuteApi

        /// <summary>
        /// ExecuteApi(正常系)CARADAID一致
        /// </summary>
        /// <remarks>SuccessProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_正常系_ステータスコード200_CARADAID一致()
        {
            var input = "carada_id-1";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "testuid", "", "");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(1, registedCount);
            Assert.AreEqual(0, failedCount);
        }

        /// <summary>
        /// ExecuteApi(正常系)CARADAID不一致
        /// </summary>
        /// <remarks>SuccessProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_正常系_ステータスコード200_CARADAID不一致()
        {
            var input = "carada_id-1";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'notMatch','password':'hogefuga','uid':'testuid'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var errorDetail = string.Format("CARADA IDが合致しません。({0},{1})", input, "notMatch");
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "testuid", "", errorDetail);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(1, registedCount);
            Assert.AreEqual(0, failedCount);
        }

        /// <summary>
        /// ExecuteApi(異常系)APIエラー
        /// </summary>
        /// <remarks>ApiErrorProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード400_APIエラー()
        {
            var input = "carada_id-1";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.BadRequest;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','error':'invalid_client_id','original_error':'統合認可にクライアントが見つかりません。','error_description':'クライアントIDなし'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "", "", "invalid_client_id", "統合認可にクライアントが見つかりません。 クライアントIDなし");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(0, registedCount);
            Assert.AreEqual(1, failedCount);
        }

        /// <summary>
        /// ExecuteApi(異常系)メンテナンス
        /// </summary>
        /// <remarks>ApiErrorProcのテストも含む.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード503_メンテナンス()
        {
            var input = "carada_id-1";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.ServiceUnavailable;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','error':'maintenance','error_description':'現在サーバーのメンテナンス中です。'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "", "", "maintenance", "現在サーバーのメンテナンス中です。");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(0, registedCount);
            Assert.AreEqual(1, failedCount);
        }

        /// <summary>
        /// ExecuteApi(異常系)ステータスコード504→504→200
        /// </summary>
        /// <remarks>リトライ確認.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード504_504_200()
        {
            var input = "carada_id-1";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response1 = new HttpResponseMessage();
            response1.StatusCode = HttpStatusCode.GatewayTimeout;

            var response2 = new HttpResponseMessage();
            response2.StatusCode = HttpStatusCode.GatewayTimeout;

            var response3 = new HttpResponseMessage();
            response3.StatusCode = HttpStatusCode.OK;
            response3.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");

            jwtRequestMock.SetupSequence(s => s.PostJson(param)).Returns(response1).Returns(response2).Returns(response3);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("ExecuteApi");
            method.Invoke(target, new object[] { input });

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "2"), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "3"), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), It.IsAny<string>()), Times.Exactly(3));
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response1, "1")), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response2, "2")), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:3"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "testuid", "", "");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(1, registedCount);
            Assert.AreEqual(0, failedCount);
        }

        /// <summary>
        /// ExecuteApi(異常系)リトライ回数オーバー
        /// </summary>
        /// <remarks>リトライ確認.</remarks>
        [TestMethod]
        public void ExecuteApi_異常系_リトライ回数オーバー()
        {
            var input = "carada_id-1";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response1 = new HttpResponseMessage();
            response1.StatusCode = HttpStatusCode.GatewayTimeout;

            var response2 = new HttpResponseMessage();
            response2.StatusCode = HttpStatusCode.GatewayTimeout;

            var response3 = new HttpResponseMessage();
            response3.StatusCode = HttpStatusCode.GatewayTimeout;

            jwtRequestMock.SetupSequence(s => s.PostJson(param)).Returns(response1).Returns(response2).Returns(response3);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("ExecuteApi");
                method.Invoke(target, new object[] { input });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                // Reflectionを介しているので一段目はこれ
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(ApiException));
                var apie = re.InnerException as ApiException;
                Assert.AreEqual(response3.StatusCode, apie.StatusCode);
                Assert.AreEqual(input, apie.CaradaId);
                Assert.AreEqual("サーバーエラー", apie.Message);

                jwtRequestMock.VerifyAll();

                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "2"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "3"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), It.IsAny<string>()), Times.Exactly(3));
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response1, "1")), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response2, "2")), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response3, "3")), Times.Once);
                outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);
            }
        }

        [TestMethod]
        public void ExecuteApi_異常系_ステータスコード200_400_503_504以外()
        {
            var input = "carada_id-1";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.Forbidden;

            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            try
            {
                var method = ReflectionAccessor.GetMethod<UserRegistApiJob>("ExecuteApi");
                method.Invoke(target, new object[] { input });
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                // Reflectionを介しているので一段目はこれ
                Assert.IsInstanceOfType(e, typeof(TargetInvocationException));
                var re = e as TargetInvocationException;
                Assert.IsInstanceOfType(re.InnerException, typeof(ApiException));
                var apie = re.InnerException as ApiException;
                Assert.AreEqual(response.StatusCode, apie.StatusCode);
                Assert.AreEqual(input, apie.CaradaId);
                Assert.AreEqual("サーバーエラー", apie.Message);

                jwtRequestMock.VerifyAll();

                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response, "1")), Times.Once);
                outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);
            }
        }

        #endregion

        #region Execute

        [TestMethod]
        public void Execute_正常系()
        {
            var inputFile = "C990001_20160120_CARADAID発行.tsv";

            // TextFieldParser用に正常ファイルを用意する
            fileUtil.CreateDirectoryIfNotExist(targetDirPath);
            CreateTestInputFile(Path.Combine(targetDirPath, inputFile));

            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(inputFile);

            fileUtilMock.Setup(s => s.GetFileNames(targetDirPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0]))).Returns(false);

            var input = "carada_id-1";
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            // 実行
            target.Execute();

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(LogLevelEnum.Info, It.IsAny<JToken>(), "API Response : {0} TryCount:1"), Times.Once);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "hogefuga", "testuid", "", "");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var inputCount = ReflectionAccessor.GetField<UserRegistApiJob>("inputCount").GetValue(target) as int?;
            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(1, inputCount);
            Assert.AreEqual(1, registedCount);
            Assert.AreEqual(0, failedCount);
        }

        [TestMethod]
        public void Execute_正常系_入力ファイルが空()
        {
            var inputFile = "C990001_20160120_CARADAID発行.tsv";

            // TextFieldParser用に空ファイルを用意する
            fileUtil.CreateDirectoryIfNotExist(targetDirPath);
            fileUtil.Touch(Path.Combine(targetDirPath, inputFile));

            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(inputFile);

            fileUtilMock.Setup(s => s.GetFileNames(targetDirPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0]))).Returns(false);

            var input = "carada_id-1";
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            // 実行
            target.Execute();

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Debug,
                "Execute API : {0} params:{1} TryCount:{2}",
                propInfoMock.Object.Uri,
                JsonConvert.SerializeObject(param), "1"), Times.Never);

            jwtRequestMock.Verify(s => s.PostJson(param), Times.Never);
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(It.IsAny<string>()), Times.Never);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Never);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, It.IsAny<string>(), "Shift_JIS"), Times.Never);

            var inputCount = ReflectionAccessor.GetField<UserRegistApiJob>("inputCount").GetValue(target) as int?;
            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(0, inputCount);
            Assert.AreEqual(0, registedCount);
            Assert.AreEqual(0, failedCount);
        }

        [TestMethod]
        public void Execute_異常系_ヘッダーのカラム数が不正()
        {
            var inputFile = "C990001_20160120_CARADAID発行.tsv";

            // TextFieldParser用に不正ファイルを用意する
            fileUtil.CreateDirectoryIfNotExist(targetDirPath);
            CreateTestInputFileAbnormalCol(Path.Combine(targetDirPath, inputFile));

            // GetInputFileNameIfValid
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(inputFile);

            //var inputTsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(targetDirPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0]))).Returns(false);

            var input = "carada_id-1";
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Returns(response);
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            try
            {
                // 実行
                target.Execute();
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual(inputFile, afe.FileName);
                Assert.AreEqual("ファイルのカラム数が正しくありません。", afe.Message);
                fileUtilMock.Verify(s => s.GetFileNames(targetDirPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0])), Times.Once);

                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Debug,
                    "Execute API : {0} params:{1} TryCount:{2}",
                    propInfoMock.Object.Uri,
                    JsonConvert.SerializeObject(param), It.IsAny<string>()), Times.Never);

                jwtRequestMock.Verify(s => s.PostJson(param), Times.Never);
                fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(It.IsAny<string>()), Times.Never);
                fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Never);
                fileUtilMock.Verify(s => s.WriteLine(tsvPath, It.IsAny<string>(), "Shift_JIS"), Times.Never);

                var inputCount = ReflectionAccessor.GetField<UserRegistApiJob>("inputCount").GetValue(target) as int?;
                var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
                var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
                Assert.AreEqual(0, inputCount);
                Assert.AreEqual(0, registedCount);
                Assert.AreEqual(0, failedCount);
            }
        }

        [TestMethod]
        public void Execute_異常系_APIException()
        {
            var inputFile = "C990001_20160120_CARADAID発行.tsv";

            // TextFieldParser用に正常ファイルを用意する
            fileUtil.CreateDirectoryIfNotExist(targetDirPath);
            CreateTestInputFile(Path.Combine(targetDirPath, inputFile));

            // GetInputFileNameIfValid
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(inputFile);

            //var inputTsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(targetDirPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0]))).Returns(false);

            var input = "carada_id-1";
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Throws(new ApiException("サーバーエラー", input, HttpStatusCode.GatewayTimeout));
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            // 実行
            target.Execute();

            fileUtilMock.Verify(s => s.GetFileNames(targetDirPath), Times.Once);
            fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0])), Times.Once);

            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
            outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
            outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);

            jwtRequestMock.VerifyAll();
            fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(tsvRootPath), Times.Once);
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Once);
            var line = string.Format("{0}\t{1}\t{2}\t{3}\t{4}", input, "", "", (int)HttpStatusCode.GatewayTimeout, "サーバーエラー");
            fileUtilMock.Verify(s => s.WriteLine(tsvPath, line, "Shift_JIS"), Times.Once);

            var inputCount = ReflectionAccessor.GetField<UserRegistApiJob>("inputCount").GetValue(target) as int?;
            var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
            var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
            Assert.AreEqual(1, inputCount);
            Assert.AreEqual(0, registedCount);
            Assert.AreEqual(1, failedCount);
        }

        [TestMethod]
        public void Execute_異常系_API実行で他のException()
        {
            var inputFile = "C990001_20160120_CARADAID発行.tsv";

            // TextFieldParser用に正常ファイルを用意する
            fileUtil.CreateDirectoryIfNotExist(targetDirPath);
            CreateTestInputFile(Path.Combine(targetDirPath, inputFile));

            // GetInputFileNameIfValid
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "fileUtil", fileUtilMock.Object);

            var fileList = new List<string>();
            fileList.Add(inputFile);

            //var inputTsvPath = @"..\in\";
            fileUtilMock.Setup(s => s.GetFileNames(targetDirPath)).Returns(fileList);
            fileUtilMock.Setup(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0]))).Returns(false);

            var input = "carada_id-1";
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            // リクエストパラメータ組立
            var param = new Dictionary<string, object>();
            propInfoMock.Object.ClientId = "1234567";
            propInfoMock.Object.Uri = @"https://carada.jp/";
            param.Add("carada_id", input);
            param.Add("client_id", propInfoMock.Object.ClientId);

            // レスポンス組立
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            jwtRequestMock.Setup(s => s.PostJson(param)).Throws(new ArgumentException("引数不正エラー"));
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "jwtRequest", jwtRequestMock.Object);

            var tsvRootPath = @"..\out\";
            var tsvFileName = string.Format("C990001_20160120_CARADAID発行結果_{0}.tsv", OutputUtil.ExecDateTime);
            var tsvPath = Path.Combine(tsvRootPath, tsvFileName);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputTsvPath", tsvPath);

            try
            {
                // 実行
                target.Execute();
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(ArgumentException));
                var ae = e as ArgumentException;
                Assert.AreEqual("引数不正エラー", ae.Message);
                fileUtilMock.Verify(s => s.GetFileNames(targetDirPath), Times.Once);
                fileUtilMock.Verify(s => s.IsFileLocked(Path.Combine(targetDirPath, fileList[0])), Times.Once);

                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfoMock.Object.Uri), Times.Once);
                outputUtilMock.Verify(s => s.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}", JsonConvert.SerializeObject(param), "1"), Times.Once);
                outputUtilMock.Verify(s => s.OutputApiResultWithMask(It.IsAny<LogLevelEnum>(), It.IsAny<JToken>(), It.IsAny<string>()), Times.Never);

                jwtRequestMock.VerifyAll();
                fileUtilMock.Verify(s => s.CreateDirectoryIfNotExist(It.IsAny<string>()), Times.Never);
                fileUtilMock.Verify(s => s.WriteLine(tsvPath, "CARADA ID\tパスワード\tUID\tエラーコード\tエラー詳細", "Shift_JIS"), Times.Never);
                fileUtilMock.Verify(s => s.WriteLine(tsvPath, It.IsAny<string>(), "Shift_JIS"), Times.Never);

                var inputCount = ReflectionAccessor.GetField<UserRegistApiJob>("inputCount").GetValue(target) as int?;
                var registedCount = ReflectionAccessor.GetField<UserRegistApiJob>("registedCount").GetValue(target) as int?;
                var failedCount = ReflectionAccessor.GetField<UserRegistApiJob>("failedCount").GetValue(target) as int?;
                Assert.AreEqual(1, inputCount);
                Assert.AreEqual(0, registedCount);
                Assert.AreEqual(0, failedCount);
            }
        }

        #endregion

        #region BeforeExecute

        [TestMethod]
        public void BeforeExecute_正常系()
        {
            secretInfoMock.Object.Issuer = "TestIssuer";
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);

            target.BeforeExecute();

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, string.Format("処理を開始します。Issuser ID：{0}", secretInfoMock.Object.Issuer), (System.Exception)null), Times.Once);
        }

        #endregion

        #region AfterExecute

        [TestMethod]
        public void AfterExecute_正常系_正常ログ出力()
        {
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);
            int inputCount = 1;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "inputCount", inputCount);
            int registedCount = 1;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "registedCount", registedCount);
            int failedCount = 0;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "failedCount", failedCount);

            target.AfterExecute();

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, "正常に処理が終了しました。(入力件数：{0}、登録された件数：{1})", inputCount.ToString(), registedCount.ToString()), Times.Once);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void AfterExecute_正常系_失敗ログ出力()
        {
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);
            int inputCount = 3;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "inputCount", inputCount);
            int registedCount = 2;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "registedCount", registedCount);
            int failedCount = 1;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "failedCount", failedCount);

            target.AfterExecute();

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, "処理が終了しましたが、エラーがありました。(入力件数：{0}、登録された件数：{1}、登録されなかった件数：{2})", inputCount.ToString(), registedCount.ToString(), failedCount.ToString()), Times.Once);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void AfterExecute_正常系_処理件数0件()
        {
            target = new UserRegistApiJob(secretInfoMock.Object, propInfoMock.Object);

            ReflectionAccessor.SetField<UserRegistApiJob>(target, "outputUtil", outputUtilMock.Object);
            int inputCount = 0;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "inputCount", inputCount);
            int registedCount = 0;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "registedCount", registedCount);
            int failedCount = 0;
            ReflectionAccessor.SetField<UserRegistApiJob>(target, "failedCount", failedCount);

            target.AfterExecute();

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Info, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        #endregion

        #region private method

        private void CreateTestInputFile(string filePath)
        {
            fileUtil.WriteLine(filePath, string.Format("{0}", "CARADA ID"), "Shift_JIS");
            fileUtil.WriteLine(filePath, string.Format("{0}", "carada_id-1"), "Shift_JIS");
        }
        private void CreateTestInputFileAbnormalCol(string filePath)
        {
            fileUtil.WriteLine(filePath, string.Format("{0}\t{1}", "CARADA ID", "CARADA ID TOO"), "Shift_JIS");
        }

        #endregion
    }
}
