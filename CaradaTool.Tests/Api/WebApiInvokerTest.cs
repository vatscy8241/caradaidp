﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Api;
using MTI.CaradaTool.Tests.TestHelpers;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;

namespace MTI.CaradaTool.Tests.Api
{
    /// <summary>
    /// WebApiInvokerテストクラス
    /// </summary>
    [TestClass]
    public class WebApiInvokerTest
    {
        // APIモックのURL
        private string _mockApiUrl = "https://dev-hclnln-apimock.cloudapp.net/ClientTemplateWebApiInvokerTest";

        [TestMethod]
        public void PostJson_正常系_APIをコールできる()
        {
            CreateApiMockSetting();
            var target = new WebApiInvoker();
            var header = new Dictionary<string, List<string>>
            {
                {"headerKey", new List<string>{"headerValue"}}
            };
            var result = target.PostJson(_mockApiUrl, header, new JObject());
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        /// <summary>
        /// APIモックにテストデータを投入します。
        /// </summary>
        private void CreateApiMockSetting()
        {
            var context = new ApiMockStorageContext();
            var tokenTable = context.MockSettingTable;
            tokenTable.InsertOrReplaceEntity(new MockSetting()
            {
                PartitionKey = "ClientTemplateWebApiInvokerTest",
                RowKey = "1",
                ContentType = "application/json",
                ResponseBody = "{\"result\":\"OK\"}",
                HeadersJson = "{\"headerKey\":[\"headerValue\"]}"
            });
        }
    }
}
