﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Net;
using System.Net.Http;

namespace MTI.CaradaTool.Tests.Utils
{
    /// <summary>
    /// OutputUtil UTクラス.
    /// </summary>
    /// <remarks>
    /// Console.WriteLineで出力している内容は確認できなさそうなので、
    /// ログ呼び出しの確認のみ行う.(標準出力の確認はIT、総合テスト等で確認.)
    /// </remarks>
    [TestClass]
    public class OutputUtilTest
    {
        private OutputUtil target;
        private Mock<ILogger> loggerMock;

        [TestInitialize]
        public void TestInitialize()
        {
        }

        [TestCleanup]
        public void TestCleanup()
        {
        }

        [TestMethod]
        public void OutputUtil_正常系_コンストラクタ()
        {
            // 一度作る
            target = new OutputUtil();
            var logger = ReflectionAccessor.GetField<OutputUtil>("logger").GetValue(target) as Logger;
            Assert.IsNotNull(logger);
            var firstExecDateTime = OutputUtil.ExecDateTime;
            Assert.IsNotNull(firstExecDateTime);

            // もう一度作る
            var nextTarget = new OutputUtil();
            var nextLogger = ReflectionAccessor.GetField<OutputUtil>("logger").GetValue(nextTarget) as Logger;
            Assert.AreEqual(logger, nextLogger);
            Assert.AreEqual(firstExecDateTime, OutputUtil.ExecDateTime);
        }

        [TestMethod]
        public void OutputConsoleAndLog_正常系_loglevel_message_eなし()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputConsoleAndLog(LogLevelEnum.Fatal, "hoge");
            loggerMock.Verify(s => s.Fatal("hoge"), Times.Once);
            loggerMock.Verify(s => s.Fatal(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void OutputConsoleAndLog_正常系_loglevel_message_eあり()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputConsoleAndLog(LogLevelEnum.Error, "hoge", new ArgumentException("fuga"));
            loggerMock.Verify(s => s.Error("hoge"), Times.Once);
            loggerMock.Verify(s => s.Error("fuga"), Times.Once);
            loggerMock.Verify(s => s.Error(It.IsAny<string>()), Times.Exactly(2));
        }

        [TestMethod]
        public void OutputLog_正常系()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputLog(LogLevelEnum.Fatal, "fatal");
            target.OutputLog(LogLevelEnum.Error, "error");
            target.OutputLog(LogLevelEnum.Warn, "warn");
            target.OutputLog(LogLevelEnum.Debug, "debug");
            target.OutputLog(LogLevelEnum.Trace, "trace");
            target.OutputLog(LogLevelEnum.Info, "info");
            loggerMock.Verify(s => s.Fatal("fatal"), Times.Once);
            loggerMock.Verify(s => s.Error("error"), Times.Once);
            loggerMock.Verify(s => s.Warn("warn"), Times.Once);
            loggerMock.Verify(s => s.Debug("debug"), Times.Once);
            loggerMock.Verify(s => s.Trace("trace"), Times.Once);
            loggerMock.Verify(s => s.Info("info"), Times.Once);
        }

        [TestMethod]
        public void OutputConsoleAndLog_正常系_loglevel_format_argsなし()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputConsoleAndLog(LogLevelEnum.Warn, "argsなし{0}");
            loggerMock.Verify(s => s.Warn("argsなし{0}"), Times.Once);
            loggerMock.Verify(s => s.Warn(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void OutputConsoleAndLog_正常系_loglevel_format_argsあり()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputConsoleAndLog(LogLevelEnum.Debug, "{0} {1} {2} {3} {4}", "あ", "い", "う", "え", "お");
            loggerMock.Verify(s => s.Debug("あ い う え お"), Times.Once);
            loggerMock.Verify(s => s.Debug(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void OutputLog_正常系_loglevel_format_argsなし()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputLog(LogLevelEnum.Trace, "argsなし{0}");
            loggerMock.Verify(s => s.Trace("argsなし{0}"), Times.Once);
            loggerMock.Verify(s => s.Trace(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void OutputLog_正常系_loglevel_format_argsあり()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputLog(LogLevelEnum.Info, "{0} {1} {2} {3} {4}", "あ", "い", "う", "え", "お");
            loggerMock.Verify(s => s.Info("あ い う え お"), Times.Once);
            loggerMock.Verify(s => s.Info(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void GetAllExceptionMessage_正常系()
        {
            target = new OutputUtil();
            var ae1 = new ArgumentException("ArgumentException1");
            var ae2 = new ArgumentException("ArgumentException2", ae1);
            var ae3 = new ArgumentException("ArgumentException3", ae2);
            var result = target.GetAllExceptionMessage(ae3);
            Assert.AreEqual("ArgumentException3\r\nArgumentException2\r\nArgumentException1", result);
        }

        [TestMethod]
        public void GetAllExceptionMessage_正常系_Exceptionがnull()
        {
            target = new OutputUtil();
            Assert.AreEqual(string.Empty, target.GetAllExceptionMessage(null));
        }

        [TestMethod]
        public void GetAllExceptionMessage_正常系_InnerExceptionなし()
        {
            target = new OutputUtil();
            var ae1 = new ArgumentException("ArgumentException1");
            Assert.AreEqual("ArgumentException1", target.GetAllExceptionMessage(ae1));
        }

        [TestMethod]
        public void OutputApiResultWithMask_正常系_マスク対象あり()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);

            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputApiResultWithMask(LogLevelEnum.Info, apiResult, "output : {0}");
            loggerMock.Verify(s => s.Info("output : {\"carada_id\":\"carada_id-1\",\"password\":\"*****\",\"uid\":\"testuid\"}"), Times.Once);
            loggerMock.Verify(s => s.Info(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void OutputApiResultWithMask_正常系_マスク対象なし()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','uname':'hogefuga','uid':'testuid'}");
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);

            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputApiResultWithMask(LogLevelEnum.Info, apiResult, "output : {0}");
            loggerMock.Verify(s => s.Info("output : {\"carada_id\":\"carada_id-1\",\"uname\":\"hogefuga\",\"uid\":\"testuid\"}"), Times.Once);
            loggerMock.Verify(s => s.Info(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void OutputApiResultWithMask_正常系_マスク対象が複数()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_password':'carada_id-1','password':'hogefuga','password_uid':'testuid'}");
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);

            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputApiResultWithMask(LogLevelEnum.Info, apiResult, "output : {0}");
            loggerMock.Verify(s => s.Info("output : {\"carada_password\":\"*****\",\"password\":\"*****\",\"password_uid\":\"*****\"}"), Times.Once);
            loggerMock.Verify(s => s.Info(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void OutputApiResultWithMask_異常系_JTokenがnull()
        {
            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputApiResultWithMask(LogLevelEnum.Info, null, "output : {0}");
            loggerMock.Verify(s => s.Info(It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void OutputApiResultWithMask_異常系_formatがnull()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(@"{'carada_id':'carada_id-1','password':'hogefuga','uid':'testuid'}");
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);

            target = new OutputUtil();
            loggerMock = new Mock<ILogger>();

            ReflectionAccessor.SetField<OutputUtil>(target, "logger", loggerMock.Object);

            target.OutputApiResultWithMask(LogLevelEnum.Info, apiResult, null);
            loggerMock.Verify(s => s.Info("{\"carada_id\":\"carada_id-1\",\"password\":\"*****\",\"uid\":\"testuid\"}"), Times.Once);
            loggerMock.Verify(s => s.Info(It.IsAny<string>()), Times.Once);
        }
    }
}
