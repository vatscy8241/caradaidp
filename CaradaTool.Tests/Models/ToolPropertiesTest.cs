﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTI.CaradaTool.Tests.Models
{
    /// <summary>
    /// ToolProperties UTクラス.
    /// </summary>
    [TestClass]
    public class ToolPropertiesTest
    {
        private FileUtil fileUtil = new FileUtil();
        private const string parentDirPath = @".\utTmp\";
        private const string targetDirPath = parentDirPath + @"test\";
        private const string fileName = "Properties.tsv";
        private const string resetUserInfoFileName = "ResetUserInfoProperties.tsv";
        private const string filePath = targetDirPath + fileName;
        private const string resetUserInfoFilePath = targetDirPath + resetUserInfoFileName;

        [TestInitialize]
        public void TestInitialize()
        {
            fileUtil.CreateDirectoryIfNotExist(targetDirPath);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            FileHelper.DeleteDir(parentDirPath);
        }

        [TestMethod]
        public void ToolProperties_正常系_コンストラクタとSetProperties_CARADAID登録ツール()
        {
            WriteTestTsv(filePath);

            var target = new ToolProperties(filePath);
            Assert.AreEqual(filePath, ReflectionAccessor.GetField<ToolProperties>("filePath").GetValue(target));

            // PropertiesUtil#GetPropertiesが呼ばれていることの確認
            var properties = ReflectionAccessor.GetField<ToolProperties>("properties").GetValue(target) as Dictionary<string, string>;
            Assert.IsTrue(properties.ContainsKey("client_id"));
            Assert.AreEqual("value1", properties["client_id"]);
            Assert.IsTrue(properties.ContainsKey("Uri"));
            Assert.AreEqual("value2", properties["Uri"]);

            // ValidatePropertyは通過

            // SetMyPropertiesが呼ばれていることの確認
            Assert.AreEqual("value1", target.ClientId);
            Assert.AreEqual("value2", target.Uri);
        }

        [TestMethod]
        public void ToolProperties_正常系_コンストラクタとSetProperties_ユーザー情報リセットツール()
        {
            WriteTestTsvResetUserInfo(resetUserInfoFilePath);

            var target = new ToolProperties(resetUserInfoFilePath);
            Assert.AreEqual(resetUserInfoFilePath, ReflectionAccessor.GetField<ToolProperties>("filePath").GetValue(target));

            // PropertiesUtil#GetPropertiesが呼ばれていることの確認
            var properties = ReflectionAccessor.GetField<ToolProperties>("properties").GetValue(target) as Dictionary<string, string>;
            Assert.IsTrue(properties.ContainsKey("Uri"));
            Assert.AreEqual("value1", properties["Uri"]);

            // ValidatePropertyは通過

            // SetMyPropertiesが呼ばれていることの確認
            Assert.IsNull(target.ClientId);
            Assert.AreEqual("value1", target.Uri);
        }

        [TestMethod]
        public void ToolProperties_正常系_コンストラクタとSetProperties_ValidatePropertyで余計なclient_idあり_ユーザー情報リセットツール()
        {
            WriteTestTsv(resetUserInfoFilePath);

            var target = new ToolProperties(resetUserInfoFilePath);
            Assert.AreEqual(resetUserInfoFilePath, ReflectionAccessor.GetField<ToolProperties>("filePath").GetValue(target));

            // PropertiesUtil#GetPropertiesが呼ばれていることの確認
            var properties = ReflectionAccessor.GetField<ToolProperties>("properties").GetValue(target) as Dictionary<string, string>;
            Assert.IsTrue(properties.ContainsKey("Uri"));
            Assert.AreEqual("value2", properties["Uri"]);

            // ValidatePropertyは通過

            // SetMyPropertiesが呼ばれていることの確認
            Assert.IsNull(target.ClientId);
            Assert.AreEqual("value2", target.Uri);
        }

        [TestMethod]
        public void ToolProperties_異常系_コンストラクタとSetProperties_指定ファイルが存在しない()
        {
            WriteTestTsv(filePath);

            try
            {
                new ToolProperties("not_exist_file.txt");
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual("not_exist_file.txt", afe.FileName);
                Assert.AreEqual("ファイルが存在しないか、読み込めません。", afe.Message);
            }
        }

        [TestMethod]
        public void ToolProperties_異常系_コンストラクタとSetProperties_指定ファイルが開けない()
        {
            WriteTestTsv(filePath);

            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
            {
                try
                {
                    new ToolProperties(filePath);
                    Assert.Fail("例外が発生しない");
                }
                catch (System.Exception e)
                {
                    Assert.IsInstanceOfType(e, typeof(AppFatalException));
                    var afe = e as AppFatalException;
                    Assert.AreEqual(fileName, afe.FileName);
                    Assert.AreEqual("ファイルが存在しないか、読み込めません。", afe.Message);
                }
            }
        }

        [TestMethod]
        public void ToolProperties_異常系_コンストラクタとSetProperties_ValidatePropertyでclient_idなし_CARADAID登録ツール()
        {
            WriteTestTsv(filePath, "hoge");

            try
            {
                new ToolProperties(filePath);
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual(fileName, afe.FileName);
                Assert.AreEqual("パラメータ：client_idが存在しません。", afe.Message);
            }
        }

        [TestMethod]
        public void ToolProperties_異常系_コンストラクタとSetProperties_ValidatePropertyでUriなし_CARADAID登録ツール()
        {
            WriteTestTsv(filePath, "client_id", "fuga");

            try
            {
                new ToolProperties(filePath);
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual(fileName, afe.FileName);
                Assert.AreEqual("パラメータ：Uriが存在しません。", afe.Message);
            }
        }

        [TestMethod]
        public void ToolProperties_異常系_コンストラクタとSetProperties_ValidatePropertyでUriなし_ユーザー情報リセットツール()
        {
            WriteTestTsvResetUserInfo(resetUserInfoFilePath, "fuga");

            try
            {
                new ToolProperties(resetUserInfoFilePath);
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual(resetUserInfoFileName, afe.FileName);
                Assert.AreEqual("パラメータ：Uriが存在しません。", afe.Message);
            }
        }

        [TestMethod]
        public void IsRequiredProperty_正常系_キーが含まれる_CARADAID登録ツール()
        {
            WriteTestTsv(filePath);
            var target = new ToolProperties(filePath);
            var method = ReflectionAccessor.GetMethod<ToolProperties>("IsRequiredProperty");
            Assert.IsTrue((bool)method.Invoke(target, new object[] { "client_id" }));
        }

        [TestMethod]
        public void IsRequiredProperty_正常系_キーが含まれない_CARADAID登録ツール()
        {
            WriteTestTsv(filePath);
            var target = new ToolProperties(filePath);
            var method = ReflectionAccessor.GetMethod<ToolProperties>("IsRequiredProperty");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { "hoge" }));
        }

        [TestMethod]
        public void IsRequiredProperty_正常系_キーが含まれる_ユーザー情報リセットツール()
        {
            WriteTestTsv(resetUserInfoFilePath);
            var target = new ToolProperties(resetUserInfoFilePath);
            var method = ReflectionAccessor.GetMethod<ToolProperties>("IsRequiredProperty");
            Assert.IsTrue((bool)method.Invoke(target, new object[] { "Uri" }));
        }

        [TestMethod]
        public void IsRequiredProperty_正常系_キーが含まれない_ユーザー情報リセットツール()
        {
            WriteTestTsv(resetUserInfoFilePath);
            var target = new ToolProperties(resetUserInfoFilePath);
            var method = ReflectionAccessor.GetMethod<ToolProperties>("IsRequiredProperty");
            Assert.IsFalse((bool)method.Invoke(target, new object[] { "client_id" }));
        }

        private void WriteTestTsv(string path, string key1 = "client_id", string key2 = "Uri")
        {
            using (var sw = new StreamWriter(filePath, true, Encoding.GetEncoding("Shift_JIS")))
            {
                sw.WriteLine(string.Format("{0}\t{1}", key1, "value1"));
                sw.WriteLine(string.Format("{0}\t{1}", key2, "value2"));
            }
            using (var sw = new StreamWriter(resetUserInfoFilePath, true, Encoding.GetEncoding("Shift_JIS")))
            {
                sw.WriteLine(string.Format("{0}\t{1}", key2, "value2"));
            }
        }
        private void WriteTestTsvResetUserInfo(string path, string key1 = "Uri")
        {
            using (var sw = new StreamWriter(resetUserInfoFilePath, true, Encoding.GetEncoding("Shift_JIS")))
            {
                sw.WriteLine(string.Format("{0}\t{1}", key1, "value1"));
            }
        }
    }
}
