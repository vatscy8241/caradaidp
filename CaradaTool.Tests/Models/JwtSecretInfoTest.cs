﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTI.CaradaTool.Tests.Models
{
    /// <summary>
    /// JwtSecretInfo UTクラス.
    /// </summary>
    [TestClass]
    public class JwtSecretInfoTest
    {
        private FileUtil fileUtil = new FileUtil();
        private const string parentDirPath = @".\utTmp\";
        private const string targetDirPath = parentDirPath + @"test\";
        private const string fileName = "test.prop";
        private const string filePath = targetDirPath + fileName;

        [TestInitialize]
        public void TestInitialize()
        {
            fileUtil.CreateDirectoryIfNotExist(targetDirPath);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            FileHelper.DeleteDir(parentDirPath);
        }

        [TestMethod]
        public void JwtSecretInfo_正常系_コンストラクタとSetProperties()
        {
            WriteTestTsv();

            var target = new JwtSecretInfo(filePath);
            Assert.AreEqual(filePath, ReflectionAccessor.GetField<JwtSecretInfo>("filePath").GetValue(target));

            // PropertiesUtil#GetPropertiesが呼ばれていることの確認
            var properties = ReflectionAccessor.GetField<JwtSecretInfo>("properties").GetValue(target) as Dictionary<string, string>;
            Assert.IsTrue(properties.ContainsKey("iss"));
            Assert.AreEqual("value1", properties["iss"]);
            Assert.IsTrue(properties.ContainsKey("secretKey"));
            Assert.AreEqual("value2", properties["secretKey"]);

            // ValidatePropertyは通過

            // SetMyPropertiesが呼ばれていることの確認
            Assert.AreEqual("value1", target.Issuer);
            Assert.AreEqual("value2", target.SecretKey);
        }

        [TestMethod]
        public void JwtSecretInfo_異常系_コンストラクタとSetProperties_指定ファイルが存在しない()
        {
            WriteTestTsv();

            try
            {
                new JwtSecretInfo("not_exist_file.txt");
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual("not_exist_file.txt", afe.FileName);
                Assert.AreEqual("ファイルが存在しないか、読み込めません。", afe.Message);
            }
        }

        [TestMethod]
        public void JwtSecretInfo_異常系_コンストラクタとSetProperties_指定ファイルが開けない()
        {
            WriteTestTsv();

            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
            {
                try
                {
                    new JwtSecretInfo(filePath);
                    Assert.Fail("例外が発生しない");
                }
                catch (System.Exception e)
                {
                    Assert.IsInstanceOfType(e, typeof(AppFatalException));
                    var afe = e as AppFatalException;
                    Assert.AreEqual(fileName, afe.FileName);
                    Assert.AreEqual("ファイルが存在しないか、読み込めません。", afe.Message);
                }
            }
        }

        [TestMethod]
        public void JwtSecretInfo_異常系_コンストラクタとSetProperties_ValidatePropertyでissなし()
        {
            WriteTestTsv("hoge");

            try
            {
                new JwtSecretInfo(filePath);
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual(fileName, afe.FileName);
                Assert.AreEqual("パラメータ：issが存在しません。", afe.Message);
            }
        }

        [TestMethod]
        public void JwtSecretInfo_異常系_コンストラクタとSetProperties_ValidatePropertyでsecretKeyなし()
        {
            WriteTestTsv("iss", "fuga");

            try
            {
                new JwtSecretInfo(filePath);
                Assert.Fail("例外が発生しない");
            }
            catch (System.Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(AppFatalException));
                var afe = e as AppFatalException;
                Assert.AreEqual(fileName, afe.FileName);
                Assert.AreEqual("パラメータ：secretKeyが存在しません。", afe.Message);
            }
        }

        private void WriteTestTsv(string key1 = "iss", string key2 = "secretKey")
        {
            using (var sw = new StreamWriter(filePath, true, Encoding.GetEncoding("Shift_JIS")))
            {
                sw.WriteLine(string.Format("{0}\t{1}", key1, "value1"));
                sw.WriteLine(string.Format("{0}\t{1}", key2, "value2"));
            }
        }
    }
}
