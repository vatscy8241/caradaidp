﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaTool.Api;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Tests.Properties;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Net.Http;
using System.ServiceModel.Security.Tokens;
using System.Text;

namespace MTI.CaradaTool.Logics
{
    /// <summary>
    /// JwtApiRequest UTクラス.
    /// </summary>
    [TestClass]
    public class JwtApiRequestTest
    {
        private JwtApiRequest target;
        private Mock<JwtSecretInfo> secretInfoMock;
        private Mock<ToolProperties> propInfoMock;
        private Mock<OutputUtil> outputUtilMock;

        private Mock<IWebApiInvoker> invokerMock;

        private byte[] keyBytes;

        [TestInitialize]
        public void TestInitialize()
        {
            secretInfoMock = new Mock<JwtSecretInfo>(Settings.Default.JwtSecretFilePath);
            secretInfoMock.Object.Issuer = "isshoge1";
            secretInfoMock.Object.SecretKey = "hogehoge";

            propInfoMock = new Mock<ToolProperties>(Settings.Default.AppPropertyFilePath);
            propInfoMock.Object.Uri = "localhost/hoge";
            propInfoMock.Object.ClientId = "client_id";

            target = new JwtApiRequest(secretInfoMock.Object, propInfoMock.Object);
            invokerMock = new Mock<IWebApiInvoker>();

            ReflectionAccessor.SetField<JwtApiRequest>(target, "webApiInvoker", invokerMock.Object);

            outputUtilMock = new Mock<OutputUtil>();
            ReflectionAccessor.SetField<JwtApiRequest>(target, "outputUtil", outputUtilMock.Object);

            keyBytes = Encoding.UTF8.GetBytes("hogehoge");
            Array.Resize(ref keyBytes, 64);
        }

        [TestMethod]
        public void JwtApiRequestコンストラクタ_正常系_JwtSecretInfo_JwtSecretInfo_IWebApiInvoker()
        {
            var invoker = new WebApiInvoker();

            target = new JwtApiRequest(secretInfoMock.Object, propInfoMock.Object, invoker);

            Assert.IsNotNull(target);
            Assert.AreEqual(secretInfoMock.Object, ReflectionAccessor.GetField<JwtApiRequest>("secretInfo").GetValue(target));
            Assert.AreEqual(propInfoMock.Object, ReflectionAccessor.GetField<JwtApiRequest>("propInfo").GetValue(target));
            Assert.AreEqual(invoker, ReflectionAccessor.GetField<JwtApiRequest>("webApiInvoker").GetValue(target));
        }

        [TestMethod]
        public void JwtApiRequestコンストラクタ_正常系_JwtSecretInfo_ToolProperties()
        {
            target = new JwtApiRequest(secretInfoMock.Object, propInfoMock.Object);

            Assert.IsNotNull(target);
            Assert.AreEqual(secretInfoMock.Object, ReflectionAccessor.GetField<JwtApiRequest>("secretInfo").GetValue(target));
            Assert.AreEqual(propInfoMock.Object, ReflectionAccessor.GetField<JwtApiRequest>("propInfo").GetValue(target));
            Assert.IsInstanceOfType(ReflectionAccessor.GetField<JwtApiRequest>("webApiInvoker").GetValue(target), typeof(WebApiInvoker));
        }

        [TestMethod]
        public void PostJson_正常系_https_パラメータあり()
        {
            invokerMock.Setup(s => s.PostJson("localhost/hoge", It.IsAny<Dictionary<string, List<string>>>(), It.IsAny<JObject>())).Returns(new HttpResponseMessage())
                .Callback((string s, Dictionary<string, List<string>> h, JObject j) =>
                {
                    // JObjectの検証
                    var idToken = (string)j["token"];
                    Assert.IsNotNull(idToken);
                    Assert.AreEqual(idToken.Split('.').Length, 3);

                    // 署名の検証
                    var validateParameters = new TokenValidationParameters()
                    {
                        IssuerSigningToken = new BinarySecretSecurityToken(keyBytes),
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateActor = false,
                        ValidateLifetime = true,
                        // issuerが正しいこと
                        ValidIssuers = new List<string> { "isshoge1" },
                        LifetimeValidator = (DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters) =>
                        {
                            // 約2分以内であること すくなくともテスト対象で生成した日時よりも進んでいるはずなので<=で検証する
                            Assert.IsTrue(DateTime.UtcNow.AddMinutes(-3) <= notBefore && notBefore <= DateTime.UtcNow.AddMinutes(-2));
                            Assert.IsTrue(DateTime.UtcNow.AddMinutes(3) >= expires && expires <= DateTime.UtcNow.AddMinutes(2));
                            return true;
                        }
                    };

                    SecurityToken outToken;
                    new JwtSecurityTokenHandler().ValidateToken(idToken, validateParameters, out outToken);

                    var jwtOutToken = (JwtSecurityToken)outToken;

                    Assert.IsNotNull(jwtOutToken);
                    var parameters = jwtOutToken.Payload["params"] as Dictionary<string, object>;
                    Assert.AreEqual(1, parameters["numparam"]);
                    Assert.AreEqual("a", parameters["strparam"]);
                });


            var parameter = new Dictionary<string, object>();
            parameter["numparam"] = 1;
            parameter["strparam"] = "a";

            var message = target.PostJson(parameter);

            invokerMock.VerifyAll();
        }

        [TestMethod]
        public void PostJson_正常系_https_パラメータなし()
        {
            invokerMock.Setup(s => s.PostJson("localhost/hoge", It.IsAny<Dictionary<string, List<string>>>(), It.IsAny<JObject>())).Returns(new HttpResponseMessage())
                .Callback((string s, Dictionary<string, List<string>> h, JObject j) =>
                {
                    var idToken = (string)j["token"];

                    Assert.IsNotNull(idToken);
                    Assert.AreEqual(idToken.Split('.').Length, 3);

                    var validateParameters = new TokenValidationParameters()
                    {
                        IssuerSigningToken = new BinarySecretSecurityToken(keyBytes),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateActor = false,
                        ValidateLifetime = false
                    };

                    SecurityToken outToken;
                    new JwtSecurityTokenHandler().ValidateToken(idToken, validateParameters, out outToken);

                    var jwtOutToken = (JwtSecurityToken)outToken;

                    Assert.IsNotNull(jwtOutToken);
                    Assert.IsFalse(jwtOutToken.Payload.ContainsKey("params"));

                });

            var message = target.PostJson(null);
            invokerMock.VerifyAll();
        }

        [TestMethod]
        public void GetHmacSha256SigningKey_正常系_キーが63文字の場合()
        {
            var method = ReflectionAccessor.GetMethod<JwtApiRequest>("GetHmacSha256SigningKey");
            secretInfoMock.Object.SecretKey = "123456789012345678901234567890123456789012345678901234567890123";
            target = new JwtApiRequest(secretInfoMock.Object, propInfoMock.Object);

            var key = (SymmetricSecurityKey)method.Invoke(target, new object[] { });
            Assert.IsNotNull(key);
            Assert.AreEqual(key.GetSymmetricKey().Length, 64);
        }

        [TestMethod]
        public void GetHmacSha256SigningKey_正常系_キーが64文字の場合()
        {
            var method = ReflectionAccessor.GetMethod<JwtApiRequest>("GetHmacSha256SigningKey");
            secretInfoMock.Object.SecretKey = "1234567890123456789012345678901234567890123456789012345678901234";
            target = new JwtApiRequest(secretInfoMock.Object, propInfoMock.Object);

            var key = (SymmetricSecurityKey)method.Invoke(target, new object[] { });
            Assert.IsNotNull(key);
            Assert.AreEqual(key.GetSymmetricKey().Length, 64);
        }

        [TestMethod]
        public void GetHmacSha256SigningKey_正常系_キーが64文字を超える場合()
        {
            var method = ReflectionAccessor.GetMethod<JwtApiRequest>("GetHmacSha256SigningKey");
            secretInfoMock.Object.SecretKey = "12345678901234567890123456789012345678901234567890123456789012345";
            target = new JwtApiRequest(secretInfoMock.Object, propInfoMock.Object);

            var key = (SymmetricSecurityKey)method.Invoke(target, new object[] { });
            Assert.IsNotNull(key);
            Assert.AreEqual(key.GetSymmetricKey().Length, 65);
        }
    }
}
