﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using Microsoft.WindowsAzure.Storage.Table;
using System;


namespace MTI.CaradaTool.Tests.TestHelpers
{
    /// <summary>
    /// APIモック用のAzure Table Storage を管理するためのクラスです。
    /// </summary>
    public class ApiMockStorageContext
    {
        private string connectionString;
        private CloudStorageAccount account;
        private CloudTableClient tableClient;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="connectionString">Azure Table Storage への接続文字列</param>
        public ApiMockStorageContext(string connectionString = null)
        {
            int maxRetryCount = 3;
            var maxDelay = TimeSpan.FromSeconds(10);
            this.connectionString = connectionString;
            this.account = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=devhcapimock01;AccountKey=QipzJv7U0ZElLyxtIz3jz9EM4lSaBKpJLb35LMRWzw+PbB9nm4hrd7GB/rXI9+w9TAkrvCV7VECu0lZXqVKkWg==");
            this.tableClient = this.account.CreateCloudTableClient();
            this.tableClient.DefaultRequestOptions.RetryPolicy = new LinearRetry(maxDelay, maxRetryCount);
            this.tableClient.DefaultRequestOptions.ServerTimeout = TimeSpan.FromSeconds(60);
        }

        /// <summary>
        /// CloudStorageAccount を返すプロパティ
        /// </summary>
        public CloudStorageAccount Account
        {
            get
            {
                return this.account;
            }
        }

        /// <summary>
        /// AccessToken テーブルを返すプロパティ
        /// </summary>
        public CloudTable MockSettingTable
        {
            get { return GetCloudTable("MockSetting"); }
        }

        private CloudTable GetCloudTable(string tableName)
        {
            return this.tableClient.GetTableReference(tableName);
        }
    }

    public class MockSetting : TableEntity
    {
        //デフォルト コンストラクタが必要です。
        public MockSetting()
        {

        }

        public string ContentType { get; set; }
        public string GetQueriesJson { get; set; }
        public string ResponseBody { get; set; }
        public string HeadersJson { get; set; }
    }
}
