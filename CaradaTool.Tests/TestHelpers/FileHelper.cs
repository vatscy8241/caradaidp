﻿using System.IO;

namespace MTI.CaradaTool.Tests.TestHelpers
{
    /// <summary>
    /// テスト用ファイルヘルパー.
    /// </summary>
    public class FileHelper
    {
        /// <summary>
        /// 指定ディレクトリを配下含め削除する.
        /// </summary>
        /// <remarks>
        /// UTの連続実行のタイミングで削除出来ないタイミングがあるので、
        /// Exceptionを吐いた時は無視する.
        /// それぞれのUT内で削除処理は行う想定なので、スキップしても問題ないはず.
        /// </remarks>
        /// <param name="path"></param>
        public static void DeleteDir(string path)
        {
            if (Directory.Exists(path))
            {
                try
                {
                    Directory.Delete(path, true);
                }
                catch
                {
                }
            }
        }
    }
}
