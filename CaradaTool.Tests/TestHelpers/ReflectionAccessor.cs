﻿using System;
using System.Linq;
using System.Reflection;

namespace MTI.CaradaTool.Tests.TestHelpers
{
    public static class ReflectionAccessor
    {
        /// <summary>
        /// privateなmethodを取得する
        /// </summary>
        /// <typeparam name="T">取得したいメソッドが実装されたクラス</typeparam>
        /// <param name="methodName">取得したいメソッド名</param>
        /// <returns>メソッド</returns>
        public static MethodInfo GetMethod<T>(string methodName)
        {
            return typeof(T).GetRuntimeMethods().FirstOrDefault(method => method.Name == methodName);
        }

        /// <summary>
        /// privateなmethodを取得する
        /// </summary>
        /// <typeparam name="T">取得したいメソッドが実装されたクラス</typeparam>
        /// <param name="methodName">取得したいメソッド名</param>
        /// <param name="paramCount">パラメータ数</param>
        /// <returns>メソッド</returns>
        public static MethodInfo GetMethod<T>(string methodName, int paramCount)
        {
            return typeof(T).GetRuntimeMethods().FirstOrDefault(method => method.Name == methodName && method.GetParameters().Count() == paramCount);
        }

        /// <summary>
        /// privateなmethodを取得する
        /// </summary>
        /// <typeparam name="T">取得したいメソッドが実装されたクラス</typeparam>
        /// <param name="methodName">取得したいメソッド名</param>
        /// <param name="parameterTypes">パラメータの型配列</param>
        /// <returns>メソッド</returns>
        public static MethodInfo GetMethod<T>(string methodName, params Type[] parameterTypes)
        {
            return typeof(T).GetRuntimeMethod(methodName, parameterTypes);
        }

        /// <summary>
        /// privateなpropertyを取得する
        /// </summary>
        /// <typeparam name="T">取得したいpropertyが実装されたクラス</typeparam>
        /// <param name="propertyName">取得したいproperty名</param>
        /// <returns>property</returns>
        public static PropertyInfo GetProperty<T>(string propertyName)
        {
            return typeof(T).GetRuntimeProperties().FirstOrDefault(prop => prop.Name == propertyName);
        }

        /// <summary>
        /// privateなfieldを取得する
        /// </summary>
        /// <typeparam name="T">取得したいpropertyが実装されたクラス</typeparam>
        /// <param name="fieldName">取得したいproperty名</param>
        /// <returns>property</returns>
        public static FieldInfo GetField<T>(string fieldName)
        {
            return typeof(T).GetRuntimeFields().FirstOrDefault(field => field.Name == fieldName);
        }

        /// <summary>
        /// テスト対象が持つフィールドを置き換える
        /// </summary>
        /// <param name="name">フィールド名</param>
        /// <param name="o">オブジェクト</param>
        public static void SetField<T>(T target, string name, Object o)
        {
            var field = ReflectionAccessor.GetField<T>(name);
            field.SetValue(target, o);
        }
    }
}