﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MTI.CaradaTool;
using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Job;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Tests.Properties;
using MTI.CaradaTool.Tests.TestHelpers;
using MTI.CaradaTool.Utils;
using System;

namespace CaradaTool.Tests
{
    /// <summary>
    /// Program UTクラス.
    /// </summary>
    [TestClass]
    public class ProgramTest
    {
        private Program target = new Program();
        private Mock<JwtSecretInfo> secretInfoMock;
        private Mock<ToolProperties> propInfoMock;
        private Mock<UserRegistApiJob> userRegistApiJobMock;
        private Mock<OutputUtil> outputUtilMock;

        #region テスト前後処理

        [TestInitialize]
        public void TestInitialize()
        {
            secretInfoMock = new Mock<JwtSecretInfo>(Settings.Default.JwtSecretFilePath);
            propInfoMock = new Mock<ToolProperties>(Settings.Default.AppPropertyFilePath);
            userRegistApiJobMock = new Mock<UserRegistApiJob>(secretInfoMock.Object, propInfoMock.Object);
            outputUtilMock = new Mock<OutputUtil>();
        }

        [TestCleanup]
        public void TestCleanup()
        {
        }

        #endregion

        [TestMethod]
        public void Program_正常系()
        {
            ReflectionAccessor.SetField<Program>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<Program>(target, "secretInfo", secretInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "propInfo", propInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "job", userRegistApiJobMock.Object);

            Program.Main(null);
            userRegistApiJobMock.Verify(s => s.BeforeExecute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.Execute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.AfterExecute(), Times.Once);

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<System.Exception>()), Times.Never);
        }

        [TestMethod]
        public void Program_異常系_AppFatalException_FileNameなし()
        {
            var ae = new ArgumentException("引数エラー");
            var afe = new AppFatalException("CARADAID発行結果TSVが出力できませんでした。", ae);
            userRegistApiJobMock.Setup(s => s.Execute()).Throws(afe);
            ReflectionAccessor.SetField<Program>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<Program>(target, "secretInfo", secretInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "propInfo", propInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "job", userRegistApiJobMock.Object);

            Program.Main(null);
            userRegistApiJobMock.Verify(s => s.BeforeExecute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.Execute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.AfterExecute(), Times.Never);

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, afe.Message, ae), Times.Once);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void Program_異常系_AppFatalException_FileNameあり()
        {
            var fileName = "err_file";
            var msg = "ファイルのカラム数が正しくありません。";
            var afe = new AppFatalException(fileName, msg);
            userRegistApiJobMock.Setup(s => s.Execute()).Throws(afe);
            ReflectionAccessor.SetField<Program>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<Program>(target, "secretInfo", secretInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "propInfo", propInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "job", userRegistApiJobMock.Object);

            Program.Main(null);
            userRegistApiJobMock.Verify(s => s.BeforeExecute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.Execute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.AfterExecute(), Times.Never);

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, "ファイル読み込みでエラーが発生しました。処理を中止します。(エラーがあったファイル：{0}、内容：{1})", fileName, msg), Times.Once);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<System.Exception>()), Times.Never);
        }

        [TestMethod]
        public void Program_異常系_Exception()
        {
            var ae = new ArgumentException("引数エラー");
            userRegistApiJobMock.Setup(s => s.Execute()).Throws(ae);
            ReflectionAccessor.SetField<Program>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<Program>(target, "secretInfo", secretInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "propInfo", propInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "job", userRegistApiJobMock.Object);

            Program.Main(null);
            userRegistApiJobMock.Verify(s => s.BeforeExecute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.Execute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.AfterExecute(), Times.Never);

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, "何らかのエラーが発生しました。詳細をご確認ください。", ae), Times.Once);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void Program_正常系_ResetUserInfoApiJobの場合()
        {
            ReflectionAccessor.SetField<Program>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<Program>(target, "secretInfo", secretInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "propInfo", propInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "job", userRegistApiJobMock.Object);

            string[] args = { "ResetUserInfoApiJob" };

            Program.Main(args);
            userRegistApiJobMock.Verify(s => s.BeforeExecute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.Execute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.AfterExecute(), Times.Once);

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<System.Exception>()), Times.Never);
        }

        [TestMethod]
        public void Program_正常系_その他の場合()
        {
            ReflectionAccessor.SetField<Program>(target, "outputUtil", outputUtilMock.Object);
            ReflectionAccessor.SetField<Program>(target, "secretInfo", secretInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "propInfo", propInfoMock.Object);
            ReflectionAccessor.SetField<Program>(target, "job", userRegistApiJobMock.Object);

            string[] args = { "sonota" };

            Program.Main(args);
            userRegistApiJobMock.Verify(s => s.BeforeExecute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.Execute(), Times.Once);
            userRegistApiJobMock.Verify(s => s.AfterExecute(), Times.Once);

            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            outputUtilMock.Verify(s => s.OutputConsoleAndLog(LogLevelEnum.Error, It.IsAny<string>(), It.IsAny<System.Exception>()), Times.Never);
        }
    }
}
