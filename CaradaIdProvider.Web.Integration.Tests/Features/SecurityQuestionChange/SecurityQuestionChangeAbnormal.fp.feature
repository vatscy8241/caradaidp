﻿Feature: SecurityQuestionChangeAbnormal.fp
    秘密の質問変更の異常系(FP版)

Scenario: 秘密の質問変更_FP版_異常系_変更するボタン押下_エラー表示
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=J-PHONE/3.0/J-SH10」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    Then レイアウトがFP用であること
    # 必須チェック
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「答えを入力してください。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードを入力してください。」であること

Scenario: 秘密の質問変更_FP版_異常系_変更するボタン押下_エラー時入力復元
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 P903i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000b」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then IDが「SecurityQuestionSelectId」の入力値は「2」であること
    Then IDが「Answer」の入力値は「てすと」であること
    Then IDが「Password」の入力値は「0000000b」であること
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードに誤りがあります。」であること

Scenario: 秘密の質問変更_FP版_異常系_変更するボタン押下_答えに変な漢字
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 P903i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Answer」に「繫」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionChange」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionSelectId="2"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Answer="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Password="*****"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
