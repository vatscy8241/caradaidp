﻿Feature: SecurityQuestionChangeAbnormal
    秘密の質問変更の異常系

Scenario: 秘密の質問変更_異常系_画面表示_ログインしたユーザー情報が存在しない場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示直前に削除
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_画面表示_ログインしたユーザー情報のEmailConfirmedが0の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示直前に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_画面表示_秘密の質問マスタが取得できない場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示直前に削除
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルを初期化する
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_画面表示_秘密の質問の回答が0件の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面遷移直前に削除
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルを初期化する
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_画面表示_秘密の質問の回答が2件以上取得された場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面遷移直前に削除
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 2                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_変更するボタン押下_バリデーションチェック
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 必須チェック
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    # 秘密の質問IDの必須、形式チェックについては結合テスト仕様書に記載の通りUTで担保する。
    Then IDが「Answer」の入力値は「」であること
    Then IDが「Password」の入力値は「」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えを入力してください。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードを入力してください。」であること
    Given 「1」秒待ち
    # 秘密の質問の答えが不正な場合
    Given IDが「Answer」に「abcdef12345」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then IDが「Answer」の入力値は「abcdef12345」であること
    Then IDが「Password」の入力値は「」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    Given 「1」秒待ち
    # 秘密の質問の答えの文字数上限超過
    Given IDが「Answer」に「あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもゃやゅゆょよらりるれろゎわゐゑを」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then IDが「Answer」の入力値は「あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもゃやゅゆょよらりるれろゎわゐゑを」であること
    Then IDが「Password」の入力値は「0000000a」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    Given 「1」秒待ち
    # 秘密の質問の答えに変な漢字が入っていてもアベンドせずバリデーションエラーになりAccessログも出ることの確認
    Given IDが「Answer」に「繫」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then IDが「Answer」の入力値は「繫」であること
    Then IDが「Password」の入力値は「」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionChange」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionSelectId="1"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Answer="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Password="*****"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Given 「1」秒待ち
    # 選択された秘密の質問IDが、秘密の質問リストに存在しない場合
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Password」に「0000000a」を入力する
    # 変更するボタン押下前に削除
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を選択してください。」であること

Scenario: 秘密の質問変更_異常系_変更するボタン押下_ログインしたユーザー情報が存在しない場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    # 変更するボタン押下前に削除
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_変更するボタン押下_ログインしたユーザーのEmailConfirmedが0の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    # 変更するボタン押下前に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_変更するボタン押下_パスワード不正_ロックアウト_復帰
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 1回目
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000b」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードに誤りがあります。」であること
    Given 「1」秒待ち
    # 2回目
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000c」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードに誤りがあります。」であること
    Given 「1」秒待ち
    # 3回目
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000d」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードに誤りがあります。」であること
    Given 「1」秒待ち
    # 4回目
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000e」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードに誤りがあります。」であること
    Given 「1」秒待ち
    # 5回目
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000f」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト開始」を含むこと
    Given 「1」秒待ち
    # 正常なパスワードを入れてもロックアウト
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト中」を含むこと
    # アカウントロック終了期限に変化がないこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # アカウントロック終了期限を過去にしてアカウントロックを解除する
    Given シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する
    | Field             | Value                   |
    | LockoutEndDateUtc | 2016-02-01 00:00:00.000 |
    Given ユーザーDBの「AspNetUsers」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する
    | Id                                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 |
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 新しい秘密の質問の答えに更新されること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る

Scenario: 秘密の質問変更_異常系_変更するボタン押下_秘密の質問の回答が0件の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    # 変更するボタン押下前に削除
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルを初期化する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_変更するボタン押下_秘密の質問の回答が2件以上取得された場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問変更画面表示
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    # 変更するボタン押下前にログインしているユーザーの秘密の質問の答えを追加(2件目)
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 2                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問変更_異常系_ロック情報の共有を確認する
    #ロック情報の確認のため、各画面を横断したアカウントロックケース
    #①ログイン
    #②メールアドレス変更画面のパスワード失敗
    #③パスワード変更画面のパスワード失敗
    #④秘密の質問変更画面のパスワード失敗
    #⑤ログアウト(ログイン状態だとメールアドレス再設定画面への遷移が直接入力しかない為)
    #⑥メールアドレス再設定のパスワードで失敗
    #⑦メールアドレス再設定の秘密の質問で失敗
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName               | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | SecurityQuestionChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「SecurityQuestionChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 1回目 メールアドレス変更画面のパスワード失敗
    When IDが「EmailChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID メールアドレス変更」を含むこと
    Given IDが「NewEmail」に「b@b.com」を入力する
    Given IDが「Password」に「0000000b」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードに誤りがあります。」であること
    Given 「1」秒待ち
    # 2回目 パスワード変更画面のパスワード失敗
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID アカウント管理」を含むこと
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Given IDが「inputPassword」に「a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「パスワードに誤りがあります。」であること
    Given 「1」秒待ち
    # 3回目 秘密の質問変更画面のパスワード失敗
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID アカウント管理」を含むこと
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000b」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードに誤りがあります。」であること
    Given 「1」秒待ち
    # ログアウト
    Given 「/User/Logout」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # 4回目 メールアドレス再設定画面のパスワードで失敗
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「CARADA ID メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「SecurityQuestionChange」を入力する
    Given IDが「Password」に「0000000b」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ID・パスワードが正しくありません」を含むこと
    Given 「1」秒待ち
    # 5回目 メールアドレス再設定の秘密の質問で失敗
    Given IDが「CaradaId」に「SecurityQuestionChange」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト開始」を含むこと
