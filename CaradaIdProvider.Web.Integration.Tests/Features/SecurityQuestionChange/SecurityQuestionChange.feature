﻿Feature: SecurityQuestionChange
    秘密の質問変更の正常系

Scenario: 秘密の質問変更_正常系_変更するボタン押下
    # ロックアウト期間明けのテストも兼ねる
    Given 現在時刻から「0」分後の時間をCurrentDataにEpochとして設定する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3ef1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <Current:Epoch>   | 1              | 4                 | testid   | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 9999-12-31 23:59:59.999 | 5                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given 文字列「てすと」をSHA-256で暗号化してシナリオ変数Currentへ「SecretAnswer」として設定する
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                 |
    | 4d74efe8-7c62-496e-97cf-3ef1a9a08489 | 1                  | <Current:SecretAnswer> |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    # アカウント管理画面表示
    Then 内容「CARADA ID アカウント管理」を含むこと
    Then IDが「SecurityQuestion」のInnerTextに「テスト質問1」を含むこと
    # 秘密の質問変更画面表示（ログイン後URL直叩き）
    Given 「/UserSettings/SecurityQuestionChangeView」にブラウザでアクセスする
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    Then 仮想URLが「/UserSettings/SecurityQuestionChange」であること
    Then レイアウトがデフォルトであること
    Then UserInsightの仮想URLが「/UserSettings/SecurityQuestionChange」であり、他のパラメータも正しく設定されていること
    Then formの「autocomplete」属性の値が「off」であること
    Then IDが「SecurityQuestionSelectId」の選択値は「テスト質問1」であること
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Then IDが「Answer」の入力値は「」であること
    Then IDが「Password」の入力値は「」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionChangeView」を含むこと
    Given IDが「Answer」に「☆とすて℃」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「CARADA ID アカウント管理」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | LockoutEndDateUtc | AccessFailedCount | UpdateDateUtc |
    | 4d74efe8-7c62-496e-97cf-3ef1a9a08489 | <Current:Epoch>   | 0                 | <!NULL>       |
    # 更新日時とSecurityStampが更新されることはダンプで確認
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 文字列「☆とすて℃」をSHA-256で暗号化してシナリオ変数Currentへ「SecretAnswer」として設定する
    Then ユーザーDBの「SecurityQuestionAnswers」テーブルに次のデータが存在すること
    | UserId                               | SecurityQuestionId | Answer                 |
    | 4d74efe8-7c62-496e-97cf-3ef1a9a08489 | 2                  | <Current:SecretAnswer> |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then IDが「SecurityQuestion」のInnerTextに「テスト質問2」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「SecurityQuestionChange」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「SecurityQuestionSelectId="2"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「Answer="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「Password="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「秘密の質問変更完了」を含むこと

Scenario: 秘密の質問変更_正常系_戻るボタン押下後に再表示
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3ef1a9a08491 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 4                 | testid   | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 9999-12-31 23:59:59.999 | 5                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given 文字列「てすと」をSHA-256で暗号化してシナリオ変数Currentへ「SecretAnswer」として設定する
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                 |
    | 4d74efe8-7c62-496e-97cf-3ef1a9a08491 | 1                  | <Current:SecretAnswer> |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    # アカウント管理画面表示
    Then 内容「CARADA ID アカウント管理」を含むこと
    Then IDが「SecurityQuestion」のInnerTextに「テスト質問1」を含むこと
    # 秘密の質問変更画面表示（リンク）
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    Given IDが「Answer」に「とすて」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Password」に「0000000a」を入力する
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID アカウント管理」を含むこと
    Then IDが「SecurityQuestion」のInnerTextに「テスト質問1」を含むこと
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    Then IDが「Answer」の入力値は「」であること
    Then IDが「SecurityQuestionSelectId」の選択値は「テスト質問1」であること
    Then IDが「Password」の入力値は「」であること

Scenario: 秘密の質問変更_正常系_初期表示_未ログインで直接遷移_ログイン画面遷移
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/UserSettings/SecurityQuestionChangeView」にブラウザでアクセスする
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「parameters:[ReturnUrl="/UserSettings/SecurityQuestionChangeView"]」を含むこと
    Then 内容「CARADA ID ログイン」を含むこと
