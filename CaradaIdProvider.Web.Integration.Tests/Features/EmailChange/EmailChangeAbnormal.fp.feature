﻿Feature: EmailChangeAbnormal.fp
    メールアドレス変更の異常系(FP版)

Scenario: メールアドレス変更_FP版_異常系_バリデーションチェック_入力復元有り
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH06A3(c500;TC;W30H18) 」を設定する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName    | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | EmailChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08487 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    # アカウント管理画面表示
    Given ID「EmailChange」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    # メールアドレス変更画面表示
    When IDが「EmailChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # 必須チェック
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewEmail」のエラーメッセージは「新しいメールアドレスを入力してください。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードを入力してください。」であること
    Given 「1」秒待ち
    Given IDが「NewEmail」に「b@b.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    # 復元チェック
    Given IDが「NewEmail」に「b@b.com」を入力する
    Given IDが「Password」に「」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then IDが「NewEmail」の入力値は「b@b.com」であること
    Then IDが「Password」の入力値は「」であること
    Given 「1」秒待ち
    Given IDが「NewEmail」に「」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then IDが「NewEmail」の入力値は「」であること
    Then IDが「Password」の入力値は「」であること
    Given 「1」秒待ち
    Given IDが「NewEmail」に「b@b.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    # メールアドレス変更確認画面表示
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更確認」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # メールアドレス変更認証画面表示
    When 「name」という属性の値が「Confirm」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更認証」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # 必須チェック
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードを入力してください。」であること
    Given 「1」秒待ち
    # 復元チェック
    Given IDが「VerifyCode」に「123456」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then IDが「VerifyCode」の入力値は「123456」であること
