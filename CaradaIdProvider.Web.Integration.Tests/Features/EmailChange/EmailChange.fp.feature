﻿Feature: EmailChange.fp
    メールアドレス変更の正常系(FP版)

Scenario: メールアドレス変更_FP版_正常系_アドレス重複なし_アカウント管理画面へ遷移
    Given HTTPヘッダーに「User-Agent=KDDI-HI3D UP.Browser/6.2_7.2.7.1.K.2.234 (GUI) MMP/2.0 」を設定する
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName        | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7b62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 4                 | EmailChangeUser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 9999-12-31 23:59:59.999 | 5                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルを初期化する
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7b62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240438ea2a799f7a1512bb5 | -      | 4d74efe8-7b62-496e-97cf-3df1a9a08489 |
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240438ea2a799f7a1512bb5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Given ID「EmailChangeUser」、パスワード「0000000a」のアカウントでFP版にログインする
    # アカウント管理画面表示
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    # メールアドレス変更画面表示（ログイン後URL直叩き）
    Given 「/UserSettings/EmailChangeView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # メールアドレス変更画面表示項目確認(全ケース共通なのでここだけで確認)
    Then 内容「ﾒｰﾙｱﾄﾞﾚｽの変更を行います｡<br>」を含むこと
    Then 内容「新しく設定されるﾒｰﾙｱﾄﾞﾚｽと､本人認証の為に､登録されたﾊﾟｽﾜｰﾄﾞをご入力ください｡<br>」を含むこと
    Then IDが「NewEmail」の入力値は「」であること
    Then IDが「Password」の入力値は「」であること
    # 入力
    # RFC違反だがキャリアメールで許可されているメールアドレス(@直前にドット、ドット連続)
    Given IDが「NewEmail」に「b..@b.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    # 「変更する」ボタン押下→メールアドレス変更確認画面表示
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更確認」を含むこと
    Then レイアウトがFP用であること
    # メールアドレス変更確認画面表示項目確認(全ケース共通なのでここだけで確認)
    Then 内容「入力内容をご確認の上､登録ﾎﾞﾀﾝから完了させてください｡」を含むこと
    Then 内容「b..@b.com」を含むこと
    # ログ確認
    # メールアドレス再設定画面が呼ばれたこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「POST /UserSettings/EmailChange」を含むこと	
    # パラメータ(マスク)チェック
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「NewEmail="*****", Password="*****"」を含むこと
    # 編集ボタンで一旦戻る
    When IDが「Edit」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更」を含むこと
    Then IDが「NewEmail」の入力値は「b..@b.com」であること
    Then IDが「Password」の入力値は「」であること
    Given IDが「NewEmail」に「b.@b.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更確認」を含むこと
    Then 内容「b.@b.com」を含むこと
    When 「name」という属性の値が「Confirm」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更認証」を含むこと
    # メールアドレス変更認証画面表示項目確認(全ケース共通なのでここだけで確認)
    Then レイアウトがFP用であること
    Then IDが「VerifyCode」の入力値は「」であること
    # ログイン失敗回数が初期化されること。認証コード失敗回数が初期化されること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName        | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7b62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailChangeUser | 1            | 2014-12-10 06:59:58.743 | 0                   |
    # SecurityStampが更新されること。認証コード有効期限が初期化されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 認証コード案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    Then 最後に送信されたメールタイトルが「【CARADA ID】メールアドレス変更のご案内」であること
    Then 最後に送信されたメールのあて先が「b.@b.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはメールアドレス変更用の認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 認証コード失敗用に一旦保存して戻って再度発行
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というシナリオ変数へ設定する
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更」を含むこと
    Then レイアウトがFP用であること
    # 戻る押下時、新しいメールアドレスは入力済みの値、パスワード入力値は初期化
    Then IDが「NewEmail」の入力値は「b.@b.com」であること
    Then IDが「Password」の入力値は「」であること
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更確認」を含むこと
    Then レイアウトがFP用であること
    When 「name」という属性の値が「Confirm」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更認証」を含むこと
    Then レイアウトがFP用であること
    # 認証コード失敗回数初期化が見たいので、古い認証コードを入れて一度失敗する(認証コード失敗回数が1に増える)
    Given IDが「VerifyCode」に「<Current:VerifyCode>」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「POST /UserSettings/EmailChangeVerifyCode」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「VerifyCode="<Current:VerifyCode>"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Given 「1」秒待ち
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName        | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7b62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailChangeUser | 1            | 2014-12-10 06:59:58.743 | 1                   |
    # メールアドレス再設定認証画面で入力
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # メールアドレスが更新されること。認証コード失敗回数が初期化されること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email      | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7b62-496e-97cf-3df1a9a08489 | b.@b.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailChangeUser | 1            | 2014-12-10 06:59:58.743 | 0                   |
    # 更新日時とSecurityStampが更新されること。認証コード有効期限が初期化されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # メールアドレス変更完了案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    # 新メールアドレス
    Then 最後に送信された「2」件のメールのどれかのあて先が「b.@b.com」の組でありタイトルが「【CARADA ID】メールアドレス変更完了のお知らせ」であること
    Then 最後に送信された「2」件のメールのどれかのあて先が「b.@b.com」の組であり本文に「メールアドレスの変更が完了いたしました。」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「b.@b.com」の組であり本文に「EmailChangeUser」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「b.@b.com」の組であり本文に「b.@b.com」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「b.@b.com」の組であり本文にお問い合わせ先文言が含まれること
    # 旧メールアドレス
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組でありタイトルが「【CARADA ID】メールアドレス変更完了のお知らせ」であること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「メールアドレスの変更が完了いたしました。」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「EmailChangeUser」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「b.@b.com」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文にお問い合わせ先文言が含まれること
    Then 「Access」ログのログレベル「INFO」の最後から「7」回目の出力に「VerifyCode="<Current:VerifyCode>"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「メールアドレス変更完了」を含むこと