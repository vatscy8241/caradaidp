﻿Feature: AddUser
    管理者用API 利用開始前ユーザー登録APIの正常系

Scenario: AddUser_正常系_利用開始前ユーザーが登録される
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    #Given CARADAのユーザー登録APIへのリクエストパラメータ用にIssuerとSecretKeyを設定する
    Given API固有パラメータをField->Value形式で設定する
    | Field     | Value                          |
    | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
    | carada_id | testuser                       |
    Given JWT形式でリクエストボディを設定する
    Given APIモックテーブルへ次のデータを登録する
    | PartitionKey                     | RowKey | PostParametersJson                                                                                                                                                                                                                                                                                       | ContentType      | ResponseBody                                                                                                | ResponseCode |
    | caradaIdProvider%2FregistUserAdd | 1      | {"client_id":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "platform_id": "HCPF", "provider_type":"7", "profile":{ "common":{"nick_name":"XXXXXXXXXXXXXXXXXXXX", "gender":"2", "birthdate":"19000101", "area_code":"JP-13"},"specific":{ "user_type":"0", "height":"300", "weight":"1", "permission_license":"2" }}} | application/json | { "uid" : "005c8bd57e244db7b40cc9717e1b0cdd", "access_token" : "bbbbbbbbbbbbbbb", "token_type" : "bearer" } | 200          |
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「200」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディのpassword値が簡素なパスワード形式かつ8文字であること
    Then APIレスポンスボディの「uid」の値が「<!NULL>」であること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | UserName | EmailConfirmed | PasswordHash | TwoFactorEnabled | CreateDateUtc | AuthCodeFailedCount | LockoutEnabled | EmployeeFlag | AccessFailedCount |
    | testuser | 0              | <!NULL>      | 1                | <!NULL>       | 0                   | 1              | 1            | 0                 |
    Given DBテーブル「AspNetUsers」のフィールド名「UserName」の値が「testuser」のレコードから、「Id」フィールドの値を取得し、シナリオ変数「UserId」として設定する
    Then CARADA_ID「testuser」のAspNetUserRolesのロール名が「User」であること
    Then ユーザーDBの「AuthorizedUsers」テーブルに次のデータが存在すること
    | UserId           | ClientId                         | Sub     |
    | <Current:UserId> | 36310e37439547ceb9a138336776c3a2 | <!NULL> |
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser" 」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", uid="005c8bd57e244db7b40cc9717e1b0cdd"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「6」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「6」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「5」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「5」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「url:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「token:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「統合認可のユーザー登録API：HTTPステータスコード=OK」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「"uid": "005c8bd57e244db7b40cc9717e1b0cdd"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「統合認可プロバイダのユーザー登録APIの実行成功(Uid=005c8bd57e244db7b40cc9717e1b0cdd, AccessToken=bbbbbbbbbbbbbbb, TokenType=bearer)」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", uid="005c8bd57e244db7b40cc9717e1b0cdd"]」を含むこと
    #ユーザ登録後の初回ログイン確認
    Then ユーザー登録後、CARADA_ID「testuser」のユーザーが利用開始画面へ遷移できること
