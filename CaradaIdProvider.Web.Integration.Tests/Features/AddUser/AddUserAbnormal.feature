﻿Feature: AddUserAbnormal
    管理者用API 利用開始前ユーザー登録APIの異常系

Scenario: AddUser_異常系_JWT検証_リクエストボディ無し
    Given 空のリクエストボディを指定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] リクエストパラメータにJWTのTokenが見つかりません」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_リクエストボディが空オブジェクト
    Given リクエストボディに空のオブジェクトを指定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_リクエストボディがJSON以外
    Given JSON形式でないリクエストボディを指定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「parameters:[{abc]」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「parameters:[{abc]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「Error」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「Error」の最後から「1」回目の出力に「[JWT_Invalid] リクエストパラメータにJWTのTokenが見つかりません」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_リクエストボディに空のtokenを指定する
    Given リクエストボディに空のtokenを指定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token=""]」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token=""]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_iss未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given iss未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Issがnullまたは空です。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_iat未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given iat未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Iatがnullです。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_exp未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given exp未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Expがnullです。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_nbf未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given nbf未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Nbfがnullです。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_無効なissを含むJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    # iss="invalid_iss"としてJWTを設定する
    Given 無効なissを含むJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_issuer」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] DBには未登録のIssuerです。(issuer=invalid_iss)」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_共通でない秘密鍵で署名したJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given 共通でない秘密鍵で署名したJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_issuer」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] SecurityTokenの署名検証に失敗しました。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_nbfに未来時刻を指定したJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given nbfに未来時刻を指定したJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_life_time」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Nbfが未来です。(Nbf=」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_JWT検証_expに現在時刻を指定したJWT形式でリクエストボディを設定する
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given expに現在時刻を指定したJWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_life_time」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Expが過去です。(Exp=」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_パラメータ検証_クライアントIDはNULL
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value    |
        | carada_id | testuser |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「error」の値が「invalid_client_id」であること
    Then APIレスポンスボディの「error_description」の値が「JWTのパラメーターが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="", CaradaId="testuser"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="invalid_client_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="", CaradaId="testuser"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="invalid_client_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「[JWT_Invalid] [Condition] ErrorAttribute=RequiredAttribute, ErrorMessage=invalid_client_id」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_パラメータ検証_クライアントID空文字列
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value    |
        | client_id |          |
        | carada_id | testuser |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「error」の値が「invalid_client_id」であること
    Then APIレスポンスボディの「error_description」の値が「JWTのパラメーターが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="", CaradaId="testuser"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="invalid_client_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="", CaradaId="testuser"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="invalid_client_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「[JWT_Invalid] [Condition] ErrorAttribute=RequiredAttribute, ErrorMessage=invalid_client_id」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_パラメータ検証_CARADA_IDはNULL
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value                          |
        | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「invalid_carada_id」であること
    Then APIレスポンスボディの「error_description」の値が「JWTのパラメーターが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId=""」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId=""」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「[JWT_Invalid] [Condition] ErrorAttribute=RequiredAttribute, ErrorMessage=invalid_carada_id」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_パラメータ検証_CARADA_IDが空文字列
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value                          |
        | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
        | carada_id |                                |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「invalid_carada_id」であること
    Then APIレスポンスボディの「error_description」の値が「JWTのパラメーターが不正です」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId=""」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId=""」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「[JWT_Invalid] [Condition] ErrorAttribute=RequiredAttribute, ErrorMessage=invalid_carada_id」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: AddUser_異常系_ユーザ情報が取得できた場合
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efa8-7c62-496f-97cf-3df1a9a08489 | 1              | <NULL>      | 0                    | 1                | 1              | 0                 | testuser | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value                          |
        | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
        | carada_id | testuser                       |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「error」の値が「registered_carada_id」であること
    Then APIレスポンスボディの「error_description」の値が「登録済みのCARADA IDです」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="registered_carada_id", error_description="登録済みのCARADA IDです"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="registered_carada_id", error_description="登録済みのCARADA IDです"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「登録済みのCARADA IDです。(CARADA ID=testuser)」を含むこと

Scenario: AddUser_異常系_統合認可ユーザ登録APIが成功以外_妥当なJSON返却時
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value                          |
        | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
        | carada_id | testuser                       |
    Given JWT形式でリクエストボディを設定する
    Given APIモックテーブルへ次のデータを登録する
        | PartitionKey                     | RowKey | PostParametersJson                                                                                                                                                                                                                                                                                       | ContentType      | ResponseBody                                                                                                                                  | ResponseCode |
        | caradaIdProvider%2FregistUserAdd | 1      | {"client_id":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "platform_id": "HCPF", "provider_type":"7", "profile":{ "common":{"nick_name":"XXXXXXXXXXXXXXXXXXXX", "gender":"2", "birthdate":"19000101", "area_code":"JP-13"},"specific":{ "user_type":"0", "height":"300", "weight":"1", "permission_license":"2" }}} | application/json | { "error" : "auth_invalid_profile", "error_description" : "[統合認可] 連携先プラットフォームでのプロフィールチェックがエラーとなりました。" } | 400          |
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「error」の値が「auth_provider_error」であること
    Then APIレスポンスボディの「error_description」の値が「統合認可プロバイダのユーザー登録に失敗しました」であること
    Then APIレスポンスボディの「original_error」の値が「auth_invalid_profile」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録に失敗しました", original_error="auth_invalid_profile"]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP -」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「url:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「token:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録に失敗しました", original_error="auth_invalid_profile"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「統合認可プロバイダのユーザー登録APIの実行に失敗しました。 response={ "error" : "auth_invalid_profile", "error_description" : "[統合認可] 連携先プラットフォームでのプロフィールチェックがエラーとなりました。" }」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在しないこと
    | UserName |
    | testuser |
    Then ユーザーDBの「AspNetUserRoles」テーブルに次のデータが存在しないこと
    | UserId  |
    | <!NULL> |
    Then ユーザーDBの「AuthorizedUsers」テーブルに次のデータが存在しないこと
    | UserId  |
    | <!NULL> |

Scenario: AddUser_異常系_統合認可ユーザ登録APIが成功以外_JSONが空で返却時
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value                          |
        | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
        | carada_id | testuser                       |
    Given JWT形式でリクエストボディを設定する
    Given APIモックテーブルへ次のデータを登録する
        | PartitionKey                     | RowKey | PostParametersJson                                                                                                                                                                                                                                                                                       | ContentType      | ResponseBody | ResponseCode |
        | caradaIdProvider%2FregistUserAdd | 1      | {"client_id":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "platform_id": "HCPF", "provider_type":"7", "profile":{ "common":{"nick_name":"XXXXXXXXXXXXXXXXXXXX", "gender":"2", "birthdate":"19000101", "area_code":"JP-13"},"specific":{ "user_type":"0", "height":"300", "weight":"1", "permission_license":"2" }}} | application/json | {}           | 400          |
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「error」の値が「auth_provider_error」であること
    Then APIレスポンスボディの「error_description」の値が「統合認可プロバイダのユーザー登録に失敗しました」であること
    Then APIレスポンスボディの「original_error」の値が「」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録に失敗しました", original_error=""]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP -」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「url:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「token:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録に失敗しました", original_error=""]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「統合認可プロバイダのユーザー登録APIの実行に失敗しました。 response={}」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在しないこと
    | UserName |
    | testuser |
    Then ユーザーDBの「AspNetUserRoles」テーブルに次のデータが存在しないこと
    | UserId  |
    | <!NULL> |
    Then ユーザーDBの「AuthorizedUsers」テーブルに次のデータが存在しないこと
    | UserId  |
    | <!NULL> |

Scenario: AddUser_異常系_統合認可ユーザ登録APIが成功以外_JSON以外で返却時
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
        | Field     | Value                          |
        | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
        | carada_id | testuser                       |
    Given JWT形式でリクエストボディを設定する
    Given APIモックテーブルへ次のデータを登録する
        | PartitionKey                     | RowKey | PostParametersJson                                                                                                                                                                                                                                                                                       | ContentType | ResponseBody                     | ResponseCode |
        | caradaIdProvider%2FregistUserAdd | 1      | {"client_id":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "platform_id": "HCPF", "provider_type":"7", "profile":{ "common":{"nick_name":"XXXXXXXXXXXXXXXXXXXX", "gender":"2", "birthdate":"19000101", "area_code":"JP-13"},"specific":{ "user_type":"0", "height":"300", "weight":"1", "permission_license":"2" }}} | text/html   | <html><body>error!</body></html> | 400          |
    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「error」の値が「auth_provider_error」であること
    Then APIレスポンスボディの「error_description」の値が「統合認可プロバイダのユーザー登録に失敗しました」であること
    Then APIレスポンスボディの「original_error」の値が「」であること
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録に失敗しました", original_error=""]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「4」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP -」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「url:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「token:[」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録に失敗しました", original_error=""]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「統合認可プロバイダのユーザー登録APIの実行に失敗しました。 response=<html><body>error!</body></html>」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在しないこと
    | UserName |
    | testuser |
    Then ユーザーDBの「AspNetUserRoles」テーブルに次のデータが存在しないこと
    | UserId  |
    | <!NULL> |
    Then ユーザーDBの「AuthorizedUsers」テーブルに次のデータが存在しないこと
    | UserId  |
    | <!NULL> |

#Scenario: AddUser_異常系_統合認可ユーザ登録APIが成功以外_統合認可API情報取得失敗
## 自動実行がファイル排他の関係で困難なため、手動でテストする。
## ServiceConfiguration.CI.cscfg の ApiTargetInfo の AuthProviderTest を AuthProviderTestIT へ変更して実行し、実行後は元に戻すこと。
#    Given ユーザーDBの「ClientMasters」テーブルを初期化する
#    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
#    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
#    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
#    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
#    Given ユーザーDBの「Issuers」テーブルを初期化する
#    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
#    | IssuerId  | SecretKey                                                          | Remark                |
#    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
#    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
#    Given JwtのIssuerを「CaradaIdP」として設定する
#    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
#    Given API固有パラメータをField->Value形式で設定する
#        | Field     | Value                          |
#        | client_id | aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa |
#        | carada_id | testuser                       |
#    Given JWT形式でリクエストボディを設定する
#    When API「/api/Management/AddUser」にPOSTでアクセスしたとき
#    Then APIのHTTPステータスコードが「400」であること
#    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
#    Then APIレスポンスボディの「error」の値が「auth_provider_error」であること
#    Then APIレスポンスボディの「error_description」の値が「統合認可プロバイダのユーザー登録API実行でエラーが発生しました」であること
#    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
#    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
#    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
#    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
#    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
#    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録API実行でエラーが発生しました"]」を含むこと
#    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/AddUser」を含むこと
#    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
#    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
#    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] ClientId="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", CaradaId="testuser"」を含むこと
#    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
#    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", error="auth_provider_error", error_description="統合認可プロバイダのユーザー登録API実行でエラーが発生しました"]」を含むこと
#    Then 「Application」ログのログレベル「ERROR」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
#    Then 「Application」ログのログレベル「ERROR」の最後から「2」回目の出力に「サーバエラーが発生しました。」を含むこと
#    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/AddUser」を含むこと
#    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「統合認可プロバイダのユーザー登録API実行でエラーが発生しました」を含むこと
#    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在しないこと
#    | UserName |
#    | testuser |
#    Then ユーザーDBの「AspNetUserRoles」テーブルに次のデータが存在しないこと
#    | UserId  |
#    | <!NULL> |
#    Then ユーザーDBの「AuthorizedUsers」テーブルに次のデータが存在しないこと
#    | UserId  |
#    | <!NULL> |