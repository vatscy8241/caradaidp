﻿Feature: SendGridAbnormal
    モックを使用したSendGridApi実行の異常系

Scenario: SendGrid_異常系_バウンスリスト取得でエラーコードがレスポンスされた場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email       | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | error@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | SendGrid | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「error@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「2」回目の出力に「リモート サーバーがエラーを返しました: (404) 見つかりません」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「SendGrid バウンスリスト取得エラー(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489)」を含むこと
    # apiがエラーであっても送信も失敗するとは限らないので、送信は試行する 届く届かないはテスト範疇でない
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること

Scenario: SendGrid_異常系_バウンスリスト取得でタイムアウトした場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email         | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | timeout@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | SendGrid | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「timeout@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Given 「10」秒待ち
    # 実行するPCによって例外が異なりログ出力メッセージを確定できない（何れの場合でもタイムアウトしたこと自体はログ目視により分かる）ので、メッセージを確認しない
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「SendGrid バウンスリスト取得エラー(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること

Scenario: SendGrid_異常系_バウンスリスト削除でエラーコードがレスポンスされた場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email             | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | deleteerror@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | SendGrid | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「deleteerror@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「2」回目の出力に「リモート サーバーがエラーを返しました: (404) 見つかりません」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「SendGrid バウンスリスト削除エラー(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること

Scenario: SendGrid_異常系_バウンスリスト削除でタイムアウトした場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email               | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | deletetimeout@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | SendGrid | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「deletetimeout@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Given 「10」秒待ち
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「SendGrid バウンスリスト削除エラー(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること