﻿Feature: SendGrid
    モックを利用したSendGridApi実行の正常系
    メール送信にはパスワード再設定を使用する

Scenario: SendGrid_正常系_バウンスリストなし
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | SendGrid | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    # この後にバウンスのログが出ていないこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】パスワード再設定のご案内, 利用開始登録済み:True」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「GET  /SendGridMock/bouncesget」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること

Scenario: SendGrid_正常系_バウンスリストあり
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email        | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | bounce@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | SendGrid | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「bounce@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】パスワード再設定のご案内, 利用開始登録済み:True」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「SendGrid バウンスリスト登録あり(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489)」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SendGrid バウンスリスト削除完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること

# RC環境で暫くの間新規登録Mockを動作させる都合、SendGridMockも有効であるため、本シナリオはコメントアウトする。
# RC環境でSendGridMockを無効とした後に本シナリオを確認する。
#Scenario: SendGrid_正常系_RC環境ではSendGridMockにアクセスすると404エラーが起こること
#    Given インターネットサイト「https://rc-carada-idp-prdip/SendGridMock」にブラウザでアクセスする
#    Then 内容「404」を含むこと
#    Given インターネットサイト「https://rc-carada-idp-prdip/SendGridMock/bouncesget」にブラウザでアクセスする
#    Then 内容「404」を含むこと
#    Given インターネットサイト「https://rc-carada-idp-prdip/SendGridMock/bouncesdelete」にブラウザでアクセスする
#    Then 内容「404」を含むこと