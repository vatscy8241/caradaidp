﻿Feature: PasswordReset
    パスワード再設定の正常系

Scenario: パスワード再設定_正常系_未ログイン_パスワード再設定できること
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | a.@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 1                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2099-12-10 06:59:58.743               | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | 1                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357356a240438ea2a799f7a1512ac1 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08409 |
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357356a240438ea2a799f7a1512ac1」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性が存在しないこと
    Then IDが「advice」のInnerTextに「ドメイン指定受信をご利用の方は、「@carada.jp」 のドメイン解除設定をお願いいたします」を含むこと
    Then 仮想URLが「/UserSettings/PasswordResetGuide」であること
    Then UserInsightの仮想URLが「/UserSettings/PasswordResetGuide」であり、他のパラメータも正しく設定されていること
    Given IDが「Email」に「A.@a.com」を入力する
    # ユーザー情報の更新日時とSecurityStampが更新されることの確認はダンプでする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「パスワード再設定」を含むこと
    Then レイアウトがデフォルトであること
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。届いたメールに記載された認証コードを下記へ入力してください。」を含むこと
    Then 仮想URLが「/UserSettings/PasswordReset」であること
    Then UserInsightの仮想URLが「/UserSettings/PasswordReset」であり、他のパラメータも正しく設定されていること
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA IDパスワード再設定用の認証コードとなります。\r\n下記のコードを入力して、認証を完了させてください。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08409へのメール送信を開始します。件名:【CARADA ID】パスワード再設定のご案内, 利用開始登録済み:True」を含むこと
    #開始時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <!NULL>               | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000b」を入力する
    Given IDが「ConfirmResetPassword」に「0000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then 内容「CARADA ID ログイン」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「ResetPassword="*****", ConfirmResetPassword="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08409へのメール送信を開始します。件名:【CARADA ID】パスワード再設定完了のお知らせ, 利用開始登録済み:True」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード再設定完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08409, CARADA ID=aaa.com)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「あなたのCARADA ID：\r\naaa\.com」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「あなたのパスワード：再設定時にご入力いただいたものです。\r\n（パスワードはセキュリティ確保のため表示しておりません。）」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    #完了時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | LockoutEndDateUtc | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <NULL>            | 0                 | <NULL>                | 0                   |
    Given ID「aaa.com」、パスワード「0000000b」のアカウントでログインする
    Then 内容「アカウント管理」を含むこと
    Then IDが「CaradaId」のInnerTextに「aaa.com」を含むこと

Scenario: パスワード再設定_正常系_ログイン済_パスワード再設定できること
# 通常ログイン中はパスワード再設定案内画面に行く遷移はないが操作自体を拒絶するものではない
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-8c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-8c62-496e-97cf-3df1a9a084b9 | 1                  | 質問の答え |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357356a240438ea2a799f7a1512ab5 | -      | 4d74efe8-8c62-496e-97cf-3df1a9a084b9 |
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357356a240438ea2a799f7a1512ab5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「aaa.com」、パスワード「0000000a」のアカウントでログインする
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。届いたメールに記載された認証コードを下記へ入力してください。」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000b」を入力する
    Given IDが「ConfirmResetPassword」に「0000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    # ログイン中なのでアカウント情報設定画面に遷移する
    Then 内容「アカウント管理」を含むこと
    Then IDが「CaradaId」のInnerTextに「aaa.com」を含むこと
    #TODO ログアウト仕様確認まではログインcookie削除後直接アクセスする
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ID「aaa.com」、パスワード「0000000b」のアカウントでログインする
    Then 内容「アカウント管理」を含むこと
    Then IDが「CaradaId」のInnerTextに「aaa.com」を含むこと

Scenario: パスワード再設定_正常系_パスワード再設定画面戻るボタンで前画面に遷移
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08412 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。届いたメールに記載された認証コードを下記へ入力してください。」を含むこと
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと

Scenario: パスワード再設定_正常系_認証コード無効後に成功
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | 1                  | 質問の答え |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357356a240438ea2a799f7a1512ac5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08413 |
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357356a240438ea2a799f7a1512ac5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    #パスワード再設定案内画面に直接遷移
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。届いたメールに記載された認証コードを下記へ入力してください。」を含むこと
    #認証コードを無効にするまで失敗する
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | <NULL>            | True           | 0                 | <!NULL>                | 1                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | <NULL>            | True           | 0                 | <!NULL>                | 2                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | <NULL>            | True           | 0                 | <!NULL>                | 3                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | <NULL>            | True           | 0                 | <!NULL>                | 4                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | <NULL>            | True           | 0                 | <!NULL>               | 5                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | <NULL>            | True           | 0                 | <!NULL>               | 5                   |
    Given 「1」秒待ち
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。届いたメールに記載された認証コードを下記へ入力してください。」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000b」を入力する
    Given IDが「ConfirmResetPassword」に「0000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード再設定完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08413, CARADA ID=aaa.com)」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08413 | a@a.com | 1              | <NULL>            | True           | 0                 | <NULL>                | 0                   |
    Given 「1」秒待ち
    Given ID「aaa.com」、パスワード「0000000b」のアカウントでログインする
    Then 内容「アカウント管理」を含むこと
    Then IDが「CaradaId」のInnerTextに「aaa.com」を含むこと

Scenario: パスワード再設定_正常系_ロックアウト中にパスワード再設定し即ログイン
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc       | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08414 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | 2099-12-10 06:59:58.743 | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08414 | 1                  | 質問の答え |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357356a240438ea2a799f7a1512ac6 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08414 |
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357356a240438ea2a799f7a1512ac6」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # ロックアウトされている
    Given ID「aaa.com」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「このCARADA IDは一時的にご利用いただけません。しばらく待ってからログインしてください。」であること
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。届いたメールに記載された認証コードを下記へ入力してください。」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000b」を入力する
    Given IDが「ConfirmResetPassword」に「0000000b」を入力する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | LockoutEndDateUtc |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08414 | <NULL>            |
    Then 内容「CARADA ID ログイン」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Given ID「aaa.com」、パスワード「0000000b」のアカウントでログインする
    Then 内容「アカウント管理」を含むこと
    Then IDが「CaradaId」のInnerTextに「aaa.com」を含むこと
