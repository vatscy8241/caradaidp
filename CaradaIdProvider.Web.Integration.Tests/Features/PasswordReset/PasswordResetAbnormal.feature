﻿Feature: PasswordResetAbnormal
    パスワード再設定の異常系

Scenario: パスワード再設定_パスワード再設定画面_異常系_直接アクセスでシステムエラー
    Given 「/UserSettings/PasswordReset」にブラウザでアクセスする
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「HTTP404エラー」を含むこと
     # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと

Scenario: パスワード再設定_パスワード再設定案内画面_ユーザー情報が取得できない
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「このメールアドレスに紐付くユーザー登録がありません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは未登録のものです」を含むこと

Scenario: パスワード再設定_パスワード再設定案内画面_利用開始前ユーザー情報が取得できた場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「このメールアドレスに紐付くユーザー登録がありません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは未登録のものです」を含むこと

Scenario: パスワード再設定_パスワード再設定画面_ユーザー情報が取得できない
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「パスワード再設定」を含むこと
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました」を含むこと

Scenario: パスワード再設定_パスワード再設定画面_利用開始前ユーザー情報が取得できた場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「パスワード再設定」を含むこと
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました」を含むこと

Scenario: パスワード再設定_異常系_バリデーションチェック
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「パスワード再設定案内」を含むこと
    #必須チェック
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスを入力してください。」であること
    Given 「1」秒待ち
    #メールアドレス形式不正
    Given IDが「Email」に「@@@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスの形式に誤りがあります。」であること
    Given 「1」秒待ち
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    #必須チェック
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードを入力してください。」であること
    Then dataValmsgForが「ResetPassword」のエラーメッセージは「再設定パスワードを入力してください。」であること
    Then dataValmsgForが「ConfirmResetPassword」のエラーメッセージは「再設定パスワード（確認用）を入力してください。」であること
    Given 「1」秒待ち
    #再設定パスワード：最小桁数未満 再設定パスワード（確認用）：再設定パスワードと不一致
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「000000a」を入力する
    Given IDが「ConfirmResetPassword」に「000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「ResetPassword」のエラーメッセージは「再設定パスワードは 8 文字以上入力してください。」であること
    Then dataValmsgForが「ConfirmResetPassword」のエラーメッセージは「再設定パスワード（確認用）と再設定パスワードが一致しません。」であること
    Given 「1」秒待ち
    #再設定パスワード：最大桁数超
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0」を入力する
    Given IDが「ConfirmResetPassword」に「aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「ResetPassword」のエラーメッセージは「再設定パスワードは 128 文字以下で入力してください。」であること
    Given 「1」秒待ち
    #再設定パスワード：英数非混在不正
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「00000000」を入力する
    Given IDが「ConfirmResetPassword」に「00000000」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「ResetPassword」のエラーメッセージは「再設定パスワードは英数字を組み合わせて入力してください。」であること
    Given 「1」秒待ち
    #再設定パスワード：入力不可能文字混在不正
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000Ａ0」を入力する
    Given IDが「ConfirmResetPassword」に「0000000Ａ0」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「ResetPassword」のエラーメッセージは「再設定パスワードに設定できるのは、半角英数字、半角記号( !"#$%&'()=~|-^\@[;:],./`{+*}<>?_ )です。それ以外のものはご利用頂けません。」であること
    Given 「1」秒待ち
    #認証コード：一致しない1～4回目
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Given 「1」秒待ち
    #5回目失敗で認証コードが無効になる
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化」を含むこと
    Given 「1」秒待ち
    #認証コードが無効になった後は正常なコード値を受け付けないことを確認
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

Scenario: パスワード再設定_異常系_認証コード有効期限切れ
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    #認証コードの有効期限を過去日に設定
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 0                   |
    #正しい認証コードが無効になっていることを確認
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

Scenario: パスワード再設定_異常系_一度戻り、前回発行された認証コードを入力した場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というシナリオ変数へ設定する
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「パスワード再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「パスワード再設定」を含むこと
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Given IDが「VerifyCode」に「<Current:VerifyCode>」を入力する
    Given IDが「ResetPassword」に「0000000B」を入力する
    Given IDが「ConfirmResetPassword」に「0000000B」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「POST /UserSettings/PasswordReset」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「VerifyCode="<Current:VerifyCode>"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
