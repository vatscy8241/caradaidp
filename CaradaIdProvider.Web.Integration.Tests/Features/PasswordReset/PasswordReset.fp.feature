﻿Feature: PasswordReset.fp
    パスワード再設定の正常系(FP版)

Scenario: パスワード再設定_FP版_正常系_パスワード再設定できること_docomo
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email    | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | a.@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 1                 | testuser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2099-12-10 06:59:58.743 | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | 1                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357356a240438ea2a799f7a1512ac1 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08409 |
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357356a240438ea2a799f7a1512ac1」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    # キャリアをdocomoで確認
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ再設定案内」を含むこと
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性が存在しないこと
    Then 内容「※ﾄﾞﾒｲﾝ指定受信をご利用の方は､｢@carada.jp｣」を含むこと
    Then 内容「のﾄﾞﾒｲﾝ解除設定をお願いいたします｡」を含むこと
    Given IDが「Email」に「A.@a.com」を入力する
    # ユーザー情報の更新日時とSecurityStampが更新されることの確認はダンプでする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「ﾊﾟｽﾜｰﾄﾞ再設定」を含むこと
    Then レイアウトがFP用であること
    Then 内容「ご入力いただいたﾒｰﾙｱﾄﾞﾚｽ宛に認証ｺｰﾄﾞを送信しました｡」を含むこと
    Then 内容「届いたﾒｰﾙに記載された認証ｺｰﾄﾞを下記へ入力してください｡」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA IDパスワード再設定用の認証コードとなります。\r\n下記のコードを入力して、認証を完了させてください。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 開始時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <!NULL>               | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000b」を入力する
    Given IDが「ConfirmResetPassword」に「0000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「ResetPassword="*****", ConfirmResetPassword="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード再設定完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08409, CARADA ID=testuser)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「あなたのCARADA ID：\r\ntestuser」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「あなたのパスワード：再設定時にご入力いただいたものです。\r\n（パスワードはセキュリティ確保のため表示しておりません。）」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 完了時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | LockoutEndDateUtc | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <NULL>            | 0                 | <NULL>                | 0                   |
    Given ID「testuser」、パスワード「0000000b」のアカウントでFP版にログインする
    Then 内容「ｱｶｳﾝﾄ管理」を含むこと
    Then 内容「testuser」を含むこと

Scenario: パスワード再設定_FP版_正常系_パスワード再設定できること_au
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email    | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | a.@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 1                 | testuser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2099-12-10 06:59:58.743 | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | 1                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357356a240438ea2a799f7a1512ac1 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08409 |
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357356a240438ea2a799f7a1512ac1」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    # キャリアをauで確認
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ再設定案内」を含むこと
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性が存在しないこと
    Then 内容「※ﾄﾞﾒｲﾝ指定受信をご利用の方は､｢@carada.jp｣」を含むこと
    Then 内容「のﾄﾞﾒｲﾝ解除設定をお願いいたします｡」を含むこと
    Given IDが「Email」に「A.@a.com」を入力する
    # ユーザー情報の更新日時とSecurityStampが更新されることの確認はダンプでする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「ﾊﾟｽﾜｰﾄﾞ再設定」を含むこと
    Then レイアウトがFP用であること
    Then 内容「ご入力いただいたﾒｰﾙｱﾄﾞﾚｽ宛に認証ｺｰﾄﾞを送信しました｡」を含むこと
    Then 内容「届いたﾒｰﾙに記載された認証ｺｰﾄﾞを下記へ入力してください｡」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA IDパスワード再設定用の認証コードとなります。\r\n下記のコードを入力して、認証を完了させてください。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 開始時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <!NULL>               | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000b」を入力する
    Given IDが「ConfirmResetPassword」に「0000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「ResetPassword="*****", ConfirmResetPassword="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード再設定完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08409, CARADA ID=testuser)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「あなたのCARADA ID：\r\ntestuser」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「あなたのパスワード：再設定時にご入力いただいたものです。\r\n（パスワードはセキュリティ確保のため表示しておりません。）」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 完了時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | LockoutEndDateUtc | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <NULL>            | 0                 | <NULL>                | 0                   |
    Given ID「testuser」、パスワード「0000000b」のアカウントでFP版にログインする
    Then 内容「ｱｶｳﾝﾄ管理」を含むこと
    Then 内容「testuser」を含むこと

Scenario: パスワード再設定_FP版_正常系_パスワード再設定できること_softbank
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email    | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | a.@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 1                 | testuser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2099-12-10 06:59:58.743 | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | 1                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357356a240438ea2a799f7a1512ac1 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08409 |
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357356a240438ea2a799f7a1512ac1」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ再設定案内」を含むこと
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性が存在しないこと
    Then 内容「※ﾄﾞﾒｲﾝ指定受信をご利用の方は､｢@carada.jp｣」を含むこと
    Then 内容「のﾄﾞﾒｲﾝ解除設定をお願いいたします｡」を含むこと
    Given IDが「Email」に「A.@a.com」を入力する
    # ユーザー情報の更新日時とSecurityStampが更新されることの確認はダンプでする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「ﾊﾟｽﾜｰﾄﾞ再設定」を含むこと
    Then レイアウトがFP用であること
    Then 内容「ご入力いただいたﾒｰﾙｱﾄﾞﾚｽ宛に認証ｺｰﾄﾞを送信しました｡」を含むこと
    Then 内容「届いたﾒｰﾙに記載された認証ｺｰﾄﾞを下記へ入力してください｡」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA IDパスワード再設定用の認証コードとなります。\r\n下記のコードを入力して、認証を完了させてください。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 開始時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <!NULL>               | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given IDが「ResetPassword」に「0000000b」を入力する
    Given IDが「ConfirmResetPassword」に「0000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「ResetPassword="*****", ConfirmResetPassword="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード再設定完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08409, CARADA ID=testuser)」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード再設定完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「あなたのCARADA ID：\r\ntestuser」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「あなたのパスワード：再設定時にご入力いただいたものです。\r\n（パスワードはセキュリティ確保のため表示しておりません。）」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 完了時の失敗回数と有効期限のリセットを確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | LockoutEndDateUtc | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08409 | <NULL>            | 0                 | <NULL>                | 0                   |
    Given ID「testuser」、パスワード「0000000b」のアカウントでFP版にログインする
    Then 内容「ｱｶｳﾝﾄ管理」を含むこと
    Then 内容「testuser」を含むこと

Scenario: パスワード再設定_FP版_正常系_戻るボタンでログイン画面に遷移
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ再設定案内」を含むこと
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと

Scenario: パスワード再設定_FP版_正常系_戻るボタンでパスワード再設定画面に遷移
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08412 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ再設定案内」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ再設定」を含むこと
    Then 内容「ご入力いただいたﾒｰﾙｱﾄﾞﾚｽ宛に認証ｺｰﾄﾞを送信しました｡」を含むこと
    Then 内容「届いたﾒｰﾙに記載された認証ｺｰﾄﾞを下記へ入力してください｡」を含むこと
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ再設定案内」を含むこと
