﻿Feature: PasswordResetAbnormal.fp
    パスワード再設定の異常系(FP版)

Scenario: パスワード再設定_FP版_異常系_バリデーションチェック
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testuser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    # キャリアをdocomoで確認
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/UserSettings/PasswordResetGuideView」にブラウザでアクセスする
    Then 内容「ﾊﾟｽﾜｰﾄﾞ再設定案内」を含むこと
    # 必須チェック
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then IDが「Email」の入力値は「」であること
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスを入力してください。」であること
    Given 「1」秒待ち
    # メールアドレス形式不正
    Given IDが「Email」に「@@@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then IDが「Email」の入力値は「@@@a.com」であること
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスの形式に誤りがあります。」であること
    Given 「1」秒待ち
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「ﾊﾟｽﾜｰﾄﾞ再設定」を含むこと
    Then 内容「ご入力いただいたﾒｰﾙｱﾄﾞﾚｽ宛に認証ｺｰﾄﾞを送信しました｡」を含むこと
    Then 内容「届いたﾒｰﾙに記載された認証ｺｰﾄﾞを下記へ入力してください｡」を含むこと
    # 必須チェック
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then IDが「VerifyCode」の入力値は「」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードを入力してください。」であること
    Then IDが「ResetPassword」の入力値は「」であること
    Then dataValmsgForが「ResetPassword」のエラーメッセージは「再設定パスワードを入力してください。」であること
    Then IDが「ConfirmResetPassword」の入力値は「」であること
    Then dataValmsgForが「ConfirmResetPassword」のエラーメッセージは「再設定パスワード（確認用）を入力してください。」であること
    Given 「1」秒待ち
    # 再設定パスワード：最小桁数未満 再設定パスワード（確認用）：再設定パスワードと不一致
    Given IDが「VerifyCode」に「1」を入力する
    Given IDが「ResetPassword」に「000000a」を入力する
    Given IDが「ConfirmResetPassword」に「000000b」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then IDが「VerifyCode」の入力値は「1」であること
    Then IDが「ResetPassword」の入力値は「」であること
    Then dataValmsgForが「ResetPassword」のエラーメッセージは「再設定パスワードは 8 文字以上入力してください。」であること
    Then IDが「ConfirmResetPassword」の入力値は「」であること
    Then dataValmsgForが「ConfirmResetPassword」のエラーメッセージは「再設定パスワード（確認用）と再設定パスワードが一致しません。」であること
