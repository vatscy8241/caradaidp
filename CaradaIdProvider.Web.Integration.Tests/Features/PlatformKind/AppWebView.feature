﻿Feature: AppWebView
    CaradaAppのWebViewで実行されているとき
Background: 
    最低2画面確認すれば十分である

Scenario: X-PlatformKindのリクエストヘッダが設定されている場合
    Given HTTPヘッダーに「X-PlatformKind=App_iOS\tUser-Agent=Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 仮想URLが「/User/Login」であること
    Then UserInsightの仮想URLが「/User/Login」であり、他のパラメータも正しく設定されていること
    Then GoogleAnalyticsのフィールド「trackingId」が「UA-49128370-9」であること
    Then GoogleAnalyticsのフィールド「dimension1」が「未ログイン」であること
    Then GoogleAnalyticsのフィールド「dimension4」が「WebView」であること
    Then GoogleAnalyticsのフィールド「dimension6」が「SP」であること
    Then GoogleAnalyticsのフィールド「dimension21」が「App_iOS」であること
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 仮想URLが「/UserSettings/PasswordResetGuide」であること
    Then GoogleAnalyticsのフィールド「trackingId」が「UA-49128370-9」であること
    Then GoogleAnalyticsのフィールド「dimension1」が「未ログイン」であること
    Then GoogleAnalyticsのフィールド「dimension4」が「WebView」であること
    Then GoogleAnalyticsのフィールド「dimension6」が「SP」であること
    Then GoogleAnalyticsのフィールド「dimension21」が「App_iOS」であること

Scenario: X-PlatformKindリクエストヘッダなしUAがスマートフォン
    Given HTTPヘッダーに「User-Agent=Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 仮想URLが「/User/Login」であること
    Then UserInsightの仮想URLが「/User/Login」であり、他のパラメータも正しく設定されていること
    Then GoogleAnalyticsのフィールド「trackingId」が「UA-49128370-9」であること
    Then GoogleAnalyticsのフィールド「dimension1」が「未ログイン」であること
    Then GoogleAnalyticsのフィールド「dimension4」が「Web」であること
    Then GoogleAnalyticsのフィールド「dimension6」が「SP」であること
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 仮想URLが「/UserSettings/PasswordResetGuide」であること
    Then UserInsightの仮想URLが「/UserSettings/PasswordResetGuide」であり、他のパラメータも正しく設定されていること
    Then GoogleAnalyticsのフィールド「trackingId」が「UA-49128370-9」であること
    Then GoogleAnalyticsのフィールド「dimension1」が「未ログイン」であること
    Then GoogleAnalyticsのフィールド「dimension4」が「Web」であること
    Then GoogleAnalyticsのフィールド「dimension6」が「SP」であること