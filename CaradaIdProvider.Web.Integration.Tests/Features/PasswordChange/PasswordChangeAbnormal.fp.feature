﻿Feature: PasswordChangeAbnormal.fp
    パスワード変更の異常系(FP版)

Scenario: パスワード変更_FP版_異常系_バリデーションチェック
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=KDDI-HI3D UP.Browser/6.2_7.2.7.1.K.2.234 (GUI) MMP/2.0」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性の値が「off」であること
    # 必須チェック
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then IDが「inputPassword」の入力値は「」であること
    Then IDが「inputNewPassword」の入力値は「」であること
    Then IDが「inputNewPasswordConfirm」の入力値は「」であること
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「現在のパスワードを入力してください。」であること
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードを入力してください。」であること
    Then dataValmsgForが「NewPasswordConfirm」のエラーメッセージは「新しいパスワード（確認用）を入力してください。」であること
    Given 「1」秒待ち
    # 新しいパスワードに入力不可能文字が使われている
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「a000000Ａ0」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000Ａ0」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードに設定できるのは、半角英数字、半角記号( !"#$%&'()=~|-^\@[;:],./`{+*}<>?_ )です。それ以外のものはご利用頂けません。」であること
    Given 「1」秒待ち
    # 新しいパスワードと新しいパスワード（確認用）が違う
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「a0000000」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPasswordConfirm」のエラーメッセージは「新しいパスワード（確認用）と新しいパスワードが一致しません。」であること
