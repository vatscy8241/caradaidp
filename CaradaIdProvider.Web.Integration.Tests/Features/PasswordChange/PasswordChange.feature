﻿Feature: PasswordChange
    パスワード変更の正常系

Scenario: パスワード変更_正常系_パスワード変更_再ログイン
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 二段階認証済とするためストレージエミュレータに登録(AuthCodeFailedCountのリセットを避けるため)
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 二段階認証Cookie生成
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240448ea2a799f7a1512ab5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード変更」を含むこと
    Then 仮想URLが「/UserSettings/PasswordChange」であること
    Then UserInsightの仮想URLが「/UserSettings/PasswordChange」であり、他のパラメータも正しく設定されていること
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # パスワード変更画面表示項目確認(全ケース共通なのでここだけで確認)
    Then 内容「現在のパスワードと、新しく設定したいパスワードを入力してください。」を含むこと
    Then タグ名「label」属性名「for」属性値「inputPassword」のテキストが「現在のパスワード」であること
    Then nameが「CurrentPassword」のタグの「placeholder」という属性の値が「現在のパスワード」であること
    Then IDが「inputPassword」の入力値は「」であること
    Then タグ名「label」属性名「for」属性値「inputNewPassword」のテキストが「新しいパスワード」であること
    Then nameが「NewPassword」のタグの「placeholder」という属性の値が「新しいパスワード」であること
    Then IDが「inputNewPassword」の入力値は「」であること
    Then タグ名「label」属性名「for」属性値「inputNewPasswordConfirm」のテキストが「新しいパスワード（確認用）」であること
    Then nameが「NewPasswordConfirm」のタグの「placeholder」という属性の値が「新しいパスワード（確認用）」であること
    Then IDが「inputNewPasswordConfirm」の入力値は「」であること
    # 入力
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「A1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」を入力する
    Given IDが「inputNewPasswordConfirm」に「A1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    # 新しいパスワードに更新されること。更新日時とSecurityStampが更新されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # ログイン失敗回数が0になること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | <!NULL>      | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 1                   |
    # パスワード変更完了案内メールチェック
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード変更完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Then 最後に送信されたメールの本文に「CARADA IDのパスワードの変更手続きが完了いたしました。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「■あなたのCARADA ID：\r\nPasswordChange」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「■あなたのパスワード：ご変更いただいたものです。\r\n（パスワードはセキュリティ確保のため表示しておりません。）」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 正常終了後画面遷移チェック
    Then 内容「CARADA ID アカウント管理」を含むこと
    # ログチェック
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「parameters:」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「CurrentPassword="*****", NewPassword="*****", NewPasswordConfirm="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】パスワード変更完了のお知らせ, 利用開始登録済み:True」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード変更完了」を含むこと
    # ログアウト後再ログインチェック
    Given 「/User/Logout」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「PasswordChange」、パスワード「A1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    Then IDが「CaradaId」のInnerTextに「PasswordChange」を含むこと

Scenario: パスワード変更_正常系_ログインしていない状態で直接アクセス
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/UserSettings/PasswordChangeView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと

Scenario: パスワード変更_正常系_戻るボタン押下
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 「戻る」ボタン押下
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID アカウント管理」を含むこと
