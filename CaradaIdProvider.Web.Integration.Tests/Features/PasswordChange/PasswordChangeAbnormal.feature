﻿Feature: PasswordChangeAbnormal
    パスワード変更の異常系

Scenario: パスワード変更_異常系_バリデーションチェック
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 必須チェック
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「現在のパスワードを入力してください。」であること
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードを入力してください。」であること
    Then dataValmsgForが「NewPasswordConfirm」のエラーメッセージは「新しいパスワード（確認用）を入力してください。」であること
    Given 「1」秒待ち
    # 新しいパスワードの桁数が足りない
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「111111a」を入力する
    Given IDが「inputNewPasswordConfirm」に「111111a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードは 8 文字以上入力してください。」であること
    Given 「1」秒待ち
    # 新しいパスワードの桁数が多い
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「A11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」を入力する
    Given IDが「inputNewPasswordConfirm」に「A11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードは 128 文字以下で入力してください。」であること
    Given 「1」秒待ち
    # 新しいパスワードがアルファベットだけ
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「aaaaaaaa」を入力する
    Given IDが「inputNewPasswordConfirm」に「aaaaaaaa」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードは英数字を組み合わせて入力してください。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Given 「1」秒待ち
    # 新しいパスワードが数字だけ
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「00000000」を入力する
    Given IDが「inputNewPasswordConfirm」に「00000000」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードは英数字を組み合わせて入力してください。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Given 「1」秒待ち
    # 新しいパスワードに入力不可能文字が使われている
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「0000000Ａ0」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000Ａ0」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPassword」のエラーメッセージは「新しいパスワードに設定できるのは、半角英数字、半角記号( !"#$%&'()=~|-^\@[;:],./`{+*}<>?_ )です。それ以外のものはご利用頂けません。」であること
    Given 「1」秒待ち
    # 新しいパスワードと新しいパスワード（確認用）が違う
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「a0000000」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「NewPasswordConfirm」のエラーメッセージは「新しいパスワード（確認用）と新しいパスワードが一致しません。」であること

Scenario: パスワード変更_異常系_ユーザー情報が存在しないでシステムエラー
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 入力
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000a」を入力する
    # 変更するボタン押下前にユーザー情報を削除
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 変更するボタン押下
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: パスワード変更_異常系_ユーザーのEmailConfirmedが0でシステムエラー
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 入力
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000a」を入力する
    # 変更するボタン押下前にユーザー情報のEmailConfirmedを0に更新
    Given シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する
    | Field          | Value |
    | EmailConfirmed | 0     |
    Given ユーザーDBの「AspNetUsers」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する
    | Id                                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    # 変更するボタン押下
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: パスワード変更_異常系_現在のパスワードが正しくない_ロックアウト_復帰_変更成功
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 二段階認証済とするためストレージエミュレータに登録(二段階認証によるレコード変更を避けるため)
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 二段階認証Cookie生成
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240448ea2a799f7a1512ab5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID パスワード変更」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 入力
    Given IDが「inputPassword」に「a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000a」を入力する
    # 失敗1回目
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「パスワードに誤りがあります。」であること
    # ログイン失敗回数のほかは更新されていないこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 1                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「パスワードがログイン中のものと一致しません」を含むこと
    # 失敗2回目
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「パスワードに誤りがあります。」であること
    # 失敗3回目
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「パスワードに誤りがあります。」であること
    # 失敗4回目
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「パスワードに誤りがあります。」であること
    # 失敗5回目
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト開始」を含むこと
    # ログイン失敗回数とアカウントロック終了期限のほかは更新されていないこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <!NULL>           | 1              | 0                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    # アカウントロック終了期限が設定されていること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # アカウントロックされたので、正しいパスワードでもパスワード変更できない
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    Then dataValmsgForが「CurrentPassword」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト中」を含むこと
    # ログイン失敗回数に変化がないこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <!NULL>           | 1              | 0                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    # アカウントロック終了期限に変化がないこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # アカウントロック終了期限を過去にしてアカウントロックを解除する
    Given シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する
    | Field             | Value                   |
    | LockoutEndDateUtc | 2016-02-01 00:00:00.000 |
    Given ユーザーDBの「AspNetUsers」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する
    | Id                                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    # アカウントロック解除後は正しいパスワードでパスワード変更できること
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「0000000a」を入力する
    Given IDが「inputNewPasswordConfirm」に「0000000a」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    # 新しいパスワードに更新されること。更新日時とSecurityStampが更新されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 正常終了後画面遷移チェック
    Then 内容「CARADA ID アカウント管理」を含むこと
    # ログチェック
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「parameters:」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「CurrentPassword="*****", NewPassword="*****", NewPasswordConfirm="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード変更完了」を含むこと
