﻿Feature: PasswordChange.fp
    パスワード変更の正常系(FP版)

Scenario: パスワード変更_FP版_正常系_パスワード変更_再ログイン
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 二段階認証済とするためストレージエミュレータに登録(AuthCodeFailedCountのリセットを避けるため)
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # パスワード変更画面表示項目確認(全ケース共通なのでここだけで確認)
    Then 内容「現在のﾊﾟｽﾜｰﾄﾞと､新しく設定したいﾊﾟｽﾜｰﾄﾞを入力してください｡」を含むこと
    Then IDが「inputPassword」の入力値は「」であること
    Then IDが「inputNewPassword」の入力値は「」であること
    Then IDが「inputNewPasswordConfirm」の入力値は「」であること
    # 入力
    Given IDが「inputPassword」に「0000000a」を入力する
    Given IDが「inputNewPassword」に「A1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」を入力する
    Given IDが「inputNewPasswordConfirm」に「A1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」を入力する
    When 「name」という属性の値が「Change」のボタンをクリックしたとき
    # 新しいパスワードに更新されること。更新日時とSecurityStampが更新されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # ログイン失敗回数が0になること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | <!NULL>      | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 0                   |
    # パスワード変更完了案内メールチェック
    Then 最後に送信されたメールタイトルが「【CARADA ID】パスワード変更完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Then 最後に送信されたメールの本文に「CARADA IDのパスワードの変更手続きが完了いたしました。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「■あなたのCARADA ID：\r\nPasswordChange」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「■あなたのパスワード：ご変更いただいたものです。\r\n（パスワードはセキュリティ確保のため表示しておりません。）」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 正常終了後画面遷移チェック
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # ログチェック
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「parameters:」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「CurrentPassword="*****", NewPassword="*****", NewPasswordConfirm="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「パスワード変更完了」を含むこと
    # ログアウト後再ログインチェック
    Given 「/User/Logout」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「PasswordChange」、パスワード「A1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること

Scenario: パスワード変更_FP版_正常系_戻るボタン押下
    # 初期データ準備
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName       | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | PasswordChange | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # 初期データダンプ
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # Cookie削除
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「PasswordChange」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # パスワード変更画面表示
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # 「戻る」ボタン押下
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
