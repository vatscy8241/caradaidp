﻿Feature: Token Request Normal
    Token Requestをして正常レスポンスがあること

Scenario: TokenRequest_正常系_正常レスポンスがあること
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test_id  | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # モックを使用してAuthentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「test_id」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    Then ユーザーDBの「UserAuthCodes」テーブルに次のデータが存在すること
    | AuthorizationCode | UserId                               | ExpireDateUtc | RedirectUriSeqNo |
    | <!NULL>           | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <!NULL>       | 2                |
    # Token Request
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [OK]」を含むこと
    Then 内容「iss : [https://id.carada.jp]」を含むこと
    Then 内容「sub : [aaa]」を含むこと
    Then 内容「aud : [36310e37439547ceb9a138336776c3a2]」を含むこと
    Then 内容「JWT署名検証OK?：[True]」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Token Request：」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「client_secret="432fdcfc690661f6b7c5a07b0688e873"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「client_id="36310e37439547ceb9a138336776c3a2"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「grant_type="authorization_code"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「redirect_uri="https://localhost:44306/RpMock/Token"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「code=」を含むこと

Scenario: TokenRequest_正常系_正常レスポンスがあること_複数のredirect_uri
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    | 3     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/Dummy        |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test_id  | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # モックを使用してAuthentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「redirect_uri」に「https://localhost:44306/RpMock/Token」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「test_id」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    Then ユーザーDBの「UserAuthCodes」テーブルに次のデータが存在すること
    | AuthorizationCode | UserId                               | ExpireDateUtc | RedirectUriSeqNo |
    | <!NULL>           | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <!NULL>       | 2                |
    # Token Request
    Given IDが「redirect_uri」に「https://localhost:44306/RpMock/Token」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [OK]」を含むこと
    Then 内容「iss : [https://id.carada.jp]」を含むこと
    Then 内容「sub : [aaa]」を含むこと
    Then 内容「aud : [36310e37439547ceb9a138336776c3a2]」を含むこと
    Then 内容「JWT署名検証OK?：[True]」を含むこと
