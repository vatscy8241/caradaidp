﻿Feature: Token Request Abnormal
    Token Requestをしてエラーレスポンスがあること

Scenario: Token Request_異常系_unsupported_grant_typeエラー
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test_id  | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # モックを使用してAuthentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「test_id」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request grant_typなし
    Given IDが「grant_type」に「」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"unsupported_grant_type"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request grant_typeに不正値
    Given IDが「grant_type」に「aaa」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"unsupported_grant_type"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと

Scenario: Token Request_異常系_invalid_grantエラー
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27b258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776b3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27b258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776b3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9b08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test_id  | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9b08489 | 36310e37439547ceb9a138336776b3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9b08489 | 047535b4582d4f87a71c27262f27b258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # モックを使用してAuthentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「test_id」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request codeなし
    Given IDが「code」に「」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_grant"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Token Request：」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「client_secret="432fdcfc690661f6b7c5a07b0688e873"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「client_id="36310e37439547ceb9a138336776b3a2"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「grant_type="authorization_code"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「redirect_uri="https://localhost:44306/RpMock/Token"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「code=""」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request codeに不正値
    Given IDが「code」に「aaa」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_grant"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「code="aaa"」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request redirect_uriになし
    Given IDが「redirect_uri」に「」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_grant"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「redirect_uri=""」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request redirect_uriに不正値
    Given IDが「redirect_uri」に「a」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_grant"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「redirect_uri="a"」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request 別のクライアントのclient_idとclient_secret
    Given IDが「client_id」に「047535b4582d4f87a71c27262f27b258」を入力する
    Given IDが「client_secret」に「f71ad7322bd24eefbdb774ddbf6429d8」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_grant"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「client_secret="f71ad7322bd24eefbdb774ddbf6429d8"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「client_id="047535b4582d4f87a71c27262f27b258"」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request 認可コードの有効期限切れ
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given ユーザーDBの「UserAuthCodes」テーブルへ次のテストデータを投入する
    | AuthorizationCode                | UserId                               | ExpireDateUtc                | RedirectUriSeqNo |
    | c23b7c070beb4dfc8fef4fc171937145 | 4d74efe8-7c62-496e-97cf-3df1a9b08489 | 2015-01-01T00:00:00.000+0900 | 1                |
    Given IDが「code」に「c23b7c070beb4dfc8fef4fc171937145」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_grant"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと

Scenario: Token Request_異常系_unauthorized_clientエラー
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test_id  | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # モックを使用してAuthentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「test_id」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request ユーザーの認可情報なし
    Given ユーザーDBの「AuthorizedUsers」テーブルを初期化する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"unauthorized_client"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと

Scenario: Token Request_異常系_invalid_clientエラー
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test_id  | 1            | 2014-12-10 06:59:58.743 | 0                   | 
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # モックを使用してAuthentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「test_id」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request client_idなし
    Given IDが「client_id」に「」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_client"}]」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request client_idに不正値
    Given IDが「client_id」に「aaa」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_client"}]」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # client_idが大文字小文字違い
    Given IDが「client_id」に「36310E37439547CEB9A138336776C3A2」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_client"}]」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request client_secretになし
    Given IDが「client_secret」に「」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_client"}]」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request client_secretに不正値
    Given IDが「client_secret」に「aaa」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_client"}]」を含むこと
    # 最初から
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    # Token Request client_secretが大文字小文字違い
    Given IDが「client_secret」に「432FDCFC690661F6B7C5A07B0688E873」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_client"}]」を含むこと

Scenario: TokenRequest_異常系_複数のredirect_uri
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                          |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    | 3     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/Dummy2       |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test_id  | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # モックを使用してAuthentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「redirect_uri」に「https://localhost:44306/RpMock/Token」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「test_id」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    # SeqNo:3のredirect_uriを入力
    Given IDが「redirect_uri」に「https://localhost:44306/Dummy」を入力する
    When 「name」という属性の値が「token」のボタンをクリックしたとき
    Then 内容「Token Response」を含むこと
    Then 内容「HTTPステータスコード : [BadRequest]」を含むこと
    Then 内容「ResponseBody : [{"error":"invalid_grant"}]」を含むこと
    Then 内容「iss : []」を含むこと
    Then 内容「sub : []」を含むこと
    Then 内容「aud : []」を含むこと
    Then 内容「JWT署名検証OK?：[]」を含むこと
