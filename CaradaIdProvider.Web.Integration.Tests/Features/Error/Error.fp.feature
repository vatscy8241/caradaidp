﻿Feature: Error.fp
    エラーの正常系(FP版)

Scenario: サーバーエラー画面_FP版_直接アクセス_ログイン済み
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | ErrorUser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「ErrorUser」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # サーバーエラー画面表示
    Given 「/Error/ServerError」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること

Scenario: サーバーエラー画面_FP版_直接アクセス_未ログイン_docomo
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # サーバーエラー画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/Error/ServerError」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: サーバーエラー画面_FP版_直接アクセス_未ログイン_softbank
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # サーバーエラー画面表示
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/Error/ServerError」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: サーバーエラー画面_FP版_直接アクセス_未ログイン_au
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # サーバーエラー画面表示
    # キャリアをauで確認
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/Error/ServerError」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: 404エラー画面_FP版_直接アクセス_ログイン済み
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | ErrorUser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「ErrorUser」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # 404エラー画面表示
    Given 「/Error/NotFound」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること

Scenario: 404エラー画面_FP版_直接アクセス_未ログイン_docomo
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 404エラー画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/Error/NotFound」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: 404エラー画面_FP版_直接アクセス_未ログイン_softbank
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 404エラー画面表示
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/Error/NotFound」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: 404エラー画面_FP版_直接アクセス_未ログイン_au
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # 404エラー画面表示
    # キャリアをauで確認
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/Error/NotFound」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: 404エラー画面_FP版_存在しないURLにアクセス
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # サーバーエラー画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/Error/TestNotFound」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「HTTP404エラー」を含むこと
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: メンテナンス画面_FP版_直接アクセス_ログイン済み
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | ErrorUser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「ErrorUser」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # サーバーエラー画面表示
    Given 「/Error/Maintenance」にブラウザでアクセスする
    Then 内容「CARADA ID ﾒﾝﾃﾅﾝｽ中」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること

Scenario: メンテナンス画面_FP版_直接アクセス_未ログイン_docomo
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # メンテナンス画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/Error/Maintenance」にブラウザでアクセスする
    Then 内容「CARADA ID ﾒﾝﾃﾅﾝｽ中」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること

Scenario: メンテナンス画面_FP版_直接アクセス_未ログイン_softbank
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # メンテナンス画面表示
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/Error/Maintenance」にブラウザでアクセスする
    Then 内容「CARADA ID ﾒﾝﾃﾅﾝｽ中」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること

Scenario: メンテナンス画面_FP版_直接アクセス_未ログイン_au
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # メンテナンス画面表示
    # キャリアをauで確認
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/Error/Maintenance」にブラウザでアクセスする
    Then 内容「CARADA ID ﾒﾝﾃﾅﾝｽ中」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること

Scenario: セッションタイムアウト画面_FP版_直接アクセス_ログイン済み
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 3                 | ErrorUser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 1                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    # アカウント管理画面表示
    Given ID「ErrorUser」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    # サーバーエラー画面表示
    Given 「/Error/Timeout」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「一定時間アクセスがなかったため、タイムアウトしました。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと

Scenario: セッションタイムアウト画面_FP版_直接アクセス_未ログイン_docomo
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # セッションタイムアウト画面表示
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/Error/Timeout」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「一定時間アクセスがなかったため、タイムアウトしました。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: セッションタイムアウト画面_FP版_直接アクセス_未ログイン_softbank
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # セッションタイムアウト画面表示
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/Error/Timeout」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「一定時間アクセスがなかったため、タイムアウトしました。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること

Scenario: セッションタイムアウト画面_FP版_直接アクセス_未ログイン_au
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # セッションタイムアウト画面表示
    # キャリアをauで確認
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/Error/Timeout」にブラウザでアクセスする
    Then 内容「CARADA ID ｴﾗｰ」を含むこと
    Then 内容「一定時間アクセスがなかったため、タイムアウトしました。」を含むこと
    #レイアウト確認
    Then レイアウトがFP用であること
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
