﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace CaradaIdProvider.Web.Integration.Tests.Features.UseStart
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute()]
    public partial class UseStartAbnormal_FpFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "UseStartAbnormal.fp.feature"
#line hidden
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitializeAttribute()]
        public static void FeatureSetup(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext testContext)
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "UseStartAbnormal.fp", "  利用開始処理の異常系(FP版)", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassCleanupAttribute()]
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitializeAttribute()]
        public virtual void TestInitialize()
        {
            if (((TechTalk.SpecFlow.FeatureContext.Current != null) 
                        && (TechTalk.SpecFlow.FeatureContext.Current.FeatureInfo.Title != "UseStartAbnormal.fp")))
            {
                CaradaIdProvider.Web.Integration.Tests.Features.UseStart.UseStartAbnormal_FpFeature.FeatureSetup(null);
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCleanupAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("利用開始_FP版_異常系_開始ボタン押下_エラー表示_入力復元あり")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "UseStartAbnormal.fp")]
        public virtual void 利用開始_FP版_異常系_開始ボタン押下_エラー表示_入力復元あり()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("利用開始_FP版_異常系_開始ボタン押下_エラー表示_入力復元あり", ((string[])(null)));
#line 4
this.ScenarioSetup(scenarioInfo);
#line 6
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table1.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "<NULL>",
                        "0",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "UseStart",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
            table1.AddRow(new string[] {
                        "5d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "aBc@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "92e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "UseStart2",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 7
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table1, "Given ");
#line 11
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 12
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 13
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 14
    testRunner.Given("HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 15
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 16
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 17
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 18
    testRunner.Given("ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 19
    testRunner.Then("内容「CARADA ID 利用開始」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 20
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 22
    testRunner.Given("IDが「Email」に「aBc@a.com」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 23
    testRunner.When("「name」という属性の値が「Start」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 24
    testRunner.Then("内容「CARADA ID 利用開始」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 25
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 26
    testRunner.Then("dataValmsgForが「Email」のエラーメッセージは「既に登録済みのメールアドレスです。」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 27
    testRunner.Then("IDが「Email」の入力値は「aBc@a.com」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 29
    testRunner.Then("「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは別のユーザーが使用済みです」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("利用開始認証_FP版_異常系_認証ボタン押下_エラー表示_入力復元あり")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "UseStartAbnormal.fp")]
        public virtual void 利用開始認証_FP版_異常系_認証ボタン押下_エラー表示_入力復元あり()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("利用開始認証_FP版_異常系_認証ボタン押下_エラー表示_入力復元あり", ((string[])(null)));
#line 31
this.ScenarioSetup(scenarioInfo);
#line 33
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table2.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "<NULL>",
                        "0",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "UseStart",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 34
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table2, "Given ");
#line 37
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 38
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 39
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 40
    testRunner.Given("HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront" +
                    "/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 41
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 42
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 43
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 44
    testRunner.Given("ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 45
    testRunner.Then("内容「CARADA ID 利用開始」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 46
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 47
    testRunner.Given("IDが「Email」に「a@a.com」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 48
    testRunner.When("「name」という属性の値が「Start」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 49
    testRunner.Then("内容「CARADA ID 利用開始認証」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 50
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 52
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table3.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "a@a.com",
                        "0",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "UseStart",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 53
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table3, "Given ");
#line 56
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 57
    testRunner.Given("最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 58
    testRunner.When("「name」という属性の値が「Verify」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 59
    testRunner.Then("内容「CARADA ID 利用開始認証」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 60
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 61
    testRunner.Then("dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 62
    testRunner.Then("内容「ﾒｰﾙｱﾄﾞﾚｽ： a@a.com」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 63
    testRunner.Then("IDが「VerifyCode」の入力値が最後に送信されたメールから取得した認証コードであること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 65
    testRunner.Then("「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("秘密の質問登録_FP版_異常系_登録するボタン押下_エラー表示")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "UseStartAbnormal.fp")]
        public virtual void 秘密の質問登録_FP版_異常系_登録するボタン押下_エラー表示()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("秘密の質問登録_FP版_異常系_登録するボタン押下_エラー表示", ((string[])(null)));
#line 67
this.ScenarioSetup(scenarioInfo);
#line 68
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "AuthCodeFailedCount"});
            table4.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "0",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "UseStart",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "0"});
#line 69
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table4, "Given ");
#line 72
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 73
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 74
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 75
    testRunner.Given("HTTPヘッダーに「User-Agent=Vodafone/1.0/V705SH/SHJ001[/Serial] Browser/VF-NetFront/3.3 " +
                    "Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 76
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 77
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 78
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 79
    testRunner.Given("ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 80
    testRunner.Then("内容「CARADA ID 利用開始」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 81
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 82
    testRunner.Given("IDが「Email」に「b@b.com」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 83
    testRunner.When("「name」という属性の値が「Start」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 84
    testRunner.Then("内容「CARADA ID 利用開始認証」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 85
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 86
    testRunner.Given("最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 87
    testRunner.When("「name」という属性の値が「Verify」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 88
    testRunner.Then("内容「CARADA ID 秘密の質問登録」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 89
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 90
    testRunner.When("「name」という属性の値が「Regist」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 91
    testRunner.Then("内容「CARADA ID 秘密の質問登録」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 92
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 93
    testRunner.Then("IDが「SecurityQuestionSelectId」の入力値は「」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 94
    testRunner.Then("dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を選択してください。」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 95
    testRunner.Then("IDが「Answer」の入力値は「」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 96
    testRunner.Then("dataValmsgForが「Answer」のエラーメッセージは「答えを入力してください。」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("秘密の質問登録_FP版_異常系_登録するボタン押下_エラー時入力復元")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "UseStartAbnormal.fp")]
        public virtual void 秘密の質問登録_FP版_異常系_登録するボタン押下_エラー時入力復元()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("秘密の質問登録_FP版_異常系_登録するボタン押下_エラー時入力復元", ((string[])(null)));
#line 98
this.ScenarioSetup(scenarioInfo);
#line 99
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table5 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "AuthCodeFailedCount"});
            table5.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "0",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "UseStart",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "0"});
#line 100
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table5, "Given ");
#line 103
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table6 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table6.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "3"});
            table6.AddRow(new string[] {
                        "2",
                        "テスト質問2",
                        "2"});
            table6.AddRow(new string[] {
                        "3",
                        "テスト質問3",
                        "1"});
#line 104
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table6, "Given ");
#line 109
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 110
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 111
    testRunner.Given("HTTPヘッダーに「User-Agent=J-PHONE/3.0/J-SH10」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 112
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 113
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 114
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 115
    testRunner.Given("ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 116
    testRunner.Then("内容「CARADA ID 利用開始」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 117
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 118
    testRunner.Given("IDが「Email」に「b@b.com」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 119
    testRunner.When("「name」という属性の値が「Start」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 120
    testRunner.Then("内容「CARADA ID 利用開始認証」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 121
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 122
    testRunner.Given("最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 123
    testRunner.When("「name」という属性の値が「Verify」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 124
    testRunner.Then("内容「CARADA ID 秘密の質問登録」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 125
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 127
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 128
    testRunner.Given("ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 129
    testRunner.Given("IDが「Answer」に「てすと」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 130
    testRunner.Given("IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 131
    testRunner.When("「name」という属性の値が「Regist」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 132
    testRunner.Then("内容「CARADA ID 秘密の質問登録」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 133
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 134
    testRunner.Then("IDが「SecurityQuestionSelectId」の入力値は「2」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 135
    testRunner.Then("IDが「Answer」の入力値は「てすと」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 136
    testRunner.Then("dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を選択してください。」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 138
    testRunner.Then("「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「秘密の質問が不正です」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("秘密の質問登録_FP版_異常系_登録するボタン押下_答えに変な漢字")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "UseStartAbnormal.fp")]
        public virtual void 秘密の質問登録_FP版_異常系_登録するボタン押下_答えに変な漢字()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("秘密の質問登録_FP版_異常系_登録するボタン押下_答えに変な漢字", ((string[])(null)));
#line 140
this.ScenarioSetup(scenarioInfo);
#line 141
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table7 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "AuthCodeFailedCount"});
            table7.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "0",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "UseStart",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "0"});
#line 142
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table7, "Given ");
#line 145
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table8 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table8.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "3"});
            table8.AddRow(new string[] {
                        "2",
                        "テスト質問2",
                        "2"});
            table8.AddRow(new string[] {
                        "3",
                        "テスト質問3",
                        "1"});
#line 146
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table8, "Given ");
#line 151
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 152
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 153
    testRunner.Given("HTTPヘッダーに「User-Agent=J-PHONE/3.0/J-SH10」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 154
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 155
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 156
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 157
    testRunner.Given("ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 158
    testRunner.Then("内容「CARADA ID 利用開始」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 159
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 160
    testRunner.Given("IDが「Email」に「b@b.com」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 161
    testRunner.When("「name」という属性の値が「Start」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 162
    testRunner.Then("内容「CARADA ID 利用開始認証」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 163
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 164
    testRunner.Given("最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 165
    testRunner.When("「name」という属性の値が「Verify」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 166
    testRunner.Then("内容「CARADA ID 秘密の質問登録」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 167
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 168
    testRunner.Given("IDが「Answer」に「繫」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 169
    testRunner.Given("IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 170
    testRunner.When("「name」という属性の値が「Regist」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 171
    testRunner.Then("内容「CARADA ID 秘密の質問登録」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 172
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 173
    testRunner.Then("IDが「SecurityQuestionSelectId」の入力値は「2」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 174
    testRunner.Then("dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 176
    testRunner.Then("「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionRegist」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 177
    testRunner.Then("「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionSelectId=\"2\"」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 178
    testRunner.Then("「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Answer=\"*****\"」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 179
    testRunner.Then("「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
