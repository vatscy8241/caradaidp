﻿Feature: UseStartAbnormal.fp
    利用開始処理の異常系(FP版)

Scenario: 利用開始_FP版_異常系_開始ボタン押下_エラー表示_入力復元あり
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email     | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>    | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    | 5d74efe8-7c62-496e-97cf-3df1a9a08489 | aBc@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 92e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart2 | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then レイアウトがFP用であること
    # 完全一致のメアド
    Given IDが「Email」に「aBc@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始」を含むこと
    Then レイアウトがFP用であること
    Then dataValmsgForが「Email」のエラーメッセージは「既に登録済みのメールアドレスです。」であること
    Then IDが「Email」の入力値は「aBc@a.com」であること
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは別のユーザーが使用済みです」を含むこと

Scenario: 利用開始認証_FP版_異常系_認証ボタン押下_エラー表示_入力復元あり
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then レイアウトがFP用であること
    # 画面表示後にユーザー情報の認証コード有効期限をnullに更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then レイアウトがFP用であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 内容「ﾒｰﾙｱﾄﾞﾚｽ： a@a.com」を含むこと
    Then IDが「VerifyCode」の入力値が最後に送信されたメールから取得した認証コードであること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

Scenario: 秘密の質問登録_FP版_異常系_登録するボタン押下_エラー表示
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given HTTPヘッダーに「User-Agent=Vodafone/1.0/V705SH/SHJ001[/Serial] Browser/VF-NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then レイアウトがFP用であること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    Then レイアウトがFP用であること
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    Then レイアウトがFP用であること
    Then IDが「SecurityQuestionSelectId」の入力値は「」であること
    Then dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を選択してください。」であること
    Then IDが「Answer」の入力値は「」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えを入力してください。」であること

Scenario: 秘密の質問登録_FP版_異常系_登録するボタン押下_エラー時入力復元
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given HTTPヘッダーに「User-Agent=J-PHONE/3.0/J-SH10」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then レイアウトがFP用であること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    Then レイアウトがFP用であること
    # 画面表示後に秘密の質問マスタを削除する
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    Then レイアウトがFP用であること
    Then IDが「SecurityQuestionSelectId」の入力値は「2」であること
    Then IDが「Answer」の入力値は「てすと」であること
    Then dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を選択してください。」であること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「秘密の質問が不正です」を含むこと

Scenario: 秘密の質問登録_FP版_異常系_登録するボタン押下_答えに変な漢字
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given HTTPヘッダーに「User-Agent=J-PHONE/3.0/J-SH10」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「UseStart」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then レイアウトがFP用であること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Answer」に「繫」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    Then レイアウトがFP用であること
    Then IDが「SecurityQuestionSelectId」の入力値は「2」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    # ログチェック
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionRegist」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionSelectId="2"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Answer="*****"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
