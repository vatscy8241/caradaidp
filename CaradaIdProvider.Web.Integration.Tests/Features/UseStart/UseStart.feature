﻿Feature: UseStart
    利用開始処理の正常系

Scenario: 利用開始_正常系_画面が表示されること
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    # 1-2-3 「別画面で利用規約画面が表示されること」の確認は、
    # 静的リンクを押下した時の画面表示確認で、リンク先により内容が変化するので、
    # 意図したリンク先が記述された、別ウィンドウで表示するリンクタグの確認を行う。
    Then 内容「<a href="https://www.carada.jp/terms/platform/" target="_blank">CARADAプラットフォーム利用規約</a>」を含むこと
    Then 内容「<a href="https://www.carada.jp/terms/use/" target="_blank">CARADA利用規約</a>」を含むこと
    Then 内容「<a href="https://www.carada.jp/terms/corpnotice/" target="_blank">CARADA健康経営支援コーポレートパッケージご利用にあたっての同意事項</a>」を含むこと
    Then UserInsightの仮想URLが「/User/UseStart」であり、他のパラメータも正しく設定されていること

Scenario: 利用開始_正常系_開始ボタン押下_設定したメアドに紐付くユーザーなし
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 4                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then formの「autocomplete」属性の値が「off」であること
    # 一般的なメアドのケース
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then formの「autocomplete」属性の値が「off」であること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # AuthCodeExpireDateUtc は現在日時 + 30分で設定されるため、ダンプで確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc  | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <!NULL>                              | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <!NULL>                | 0                   |
    # 認証コード案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    Then 最後に送信されたメールタイトルが「【CARADA ID】認証コードのご案内」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA ID利用開始認証の認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること

Scenario: 利用開始_正常系_開始ボタン押下_設定したメアドに紐付くユーザーあり
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email     | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>    | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 4                   |
    | 5d74efe8-7c62-496e-97cf-3df1a9a08489 | aBc@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 92e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart2 | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 4                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    # 大文字小文字違いのメアド
    Given IDが「Email」に「AbC@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # AuthCodeExpireDateUtc は現在日時 + 30分で設定されるため、ダンプで確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email     | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | AbC@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <!NULL>                              | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <!NULL>                 | 0                   |
    | 5d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>    | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <!NULL>                              | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart2 | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 4                   |
    # 認証コード案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    Then 最後に送信されたメールタイトルが「【CARADA ID】認証コードのご案内」であること
    Then 最後に送信されたメールのあて先が「AbC@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA ID利用開始認証の認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「Email="*****"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】認証コードのご案内, 利用開始登録済み:False」を含むこと

Scenario: 利用開始_正常系_画面表示後に戻るボタン押下
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    # 「戻る」の確認
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと

Scenario: 利用開始認証_正常系_認証ボタン押下
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    # @の直前にドットがあるメアド(RFC違反)
    Given IDが「Email」に「a.@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    # 認証コード有効期限がnullに、認証コード失敗回数が0に初期化されていること、EmailConfirmedは1にならないことの確認
    # 二段階認証Cookieは生成されないことの確認
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email    | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a.@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <!NULL>                              | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                  | 0                   |
    Then Cookie名が「TwoFactorLogin」のCookieが存在しないこと

Scenario: 利用開始認証_正常系_認証ボタン押下_認証コード無効後戻って再開
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then UserInsightの仮想URLが「/User/UseStart」であり、他のパラメータも正しく設定されていること
    # 連続ドットを含むメアド(RFC違反)
    Given IDが「Email」に「a...b@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    # 認証コード失敗回数が5回になるまで失敗を繰り返す
    Given IDが「VerifyCode」に「1」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「1」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a...b@a.com」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    Given IDが「VerifyCode」に「2」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「2」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a...b@a.com」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    Given IDが「VerifyCode」に「3」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「3」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a...b@a.com」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    Given IDが「VerifyCode」に「4」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「4」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a...b@a.com」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    Given IDが「VerifyCode」に「5」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「5」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 内容「メールアドレス：a...b@a.com」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 認証コード有効期限と認証コード失敗回数が更新されていることの確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email       | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a...b@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <!NULL>                              | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <!NULL>                 | 5                   |
    # 認証コード失敗回数が5回到達後に正しいコードを入れてもエラーとなること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 内容「メールアドレス：a...b@a.com」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    # 「戻る」の確認
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID 利用開始」を含むこと
    Then IDが「Email」の入力値は「a...b@a.com」であること
    Then UserInsightの仮想URLが「/User/UseStart」であり、他のパラメータも正しく設定されていること
    # 再度やり直し
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    Then 内容「メールアドレス：a...b@a.com」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID 秘密の質問登録」を含むこと
    Then UserInsightの仮想URLが「/User/SecurityQuestionRegist」であり、他のパラメータも正しく設定されていること
    # 認証コード有効期限がnullに、認証コード失敗回数が0に初期化されていること、EmailConfirmedは1にならないことの確認
    # 二段階認証Cookieは生成されないことの確認
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email       | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a...b@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <!NULL>                              | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                  | 0                   |
    Then Cookie名が「TwoFactorLogin」のCookieが存在しないこと

Scenario: 秘密の質問登録_正常系_登録ボタン押下
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then 仮想URLが「/User/UseStart」であること
    Then UserInsightの仮想URLが「/User/UseStart」であり、他のパラメータも正しく設定されていること
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Then 仮想URLが「/User/UseStartVerifyCode」であること
    Then UserInsightの仮想URLが「/User/UseStartVerifyCode」であり、他のパラメータも正しく設定されていること
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】認証コードのご案内, 利用開始登録済み:False」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Then 仮想URLが「/User/SecurityQuestionRegist」であること
    Then レイアウトがデフォルトであること
    Then UserInsightの仮想URLが「/User/SecurityQuestionRegist」であり、他のパラメータも正しく設定されていること
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Given IDが「Answer」に「＄てすと￡」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「アカウント管理」を含むこと 
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | b@b.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then ユーザーDBの「SecurityQuestionAnswers」テーブルに次のデータが存在すること
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 2                  | 5df3c4a73a29c8a075e70a0509c336f2b705e70ac2e1a3b88ae3afb7f1505993 |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then 最後に送信されたメールタイトルが「【CARADA ID】利用開始手続き完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「b@b.com」の組であること
    Then 最後に送信されたメールの本文に「あなたのCARADA ID：\r\ntestid」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「メールアドレス：\r\nb@b\.com」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    Then Cookie名が「TwoFactorLogin」のCookieが存在すること
    Given Cookie名「TwoFactorLogin」のCookieの値をCurrentのLastCookieValueとして設定する
    Then ストレージの「TwoFactorLogin」に次のデータが存在すること
    | PartitionKey              | RowKey | UserId                               |
    | <Current:LastCookieValue> | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    Given ストレージの「TwoFactorLogin」テーブルのダンプを取る
    # ログチェック
    Then 「Access」ログのログレベル「INFO」の最後から「8」回目の出力に「SecurityQuestionSelectId="2", Answer="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「7」回目の出力に「利用開始成功」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】利用開始手続き完了のお知らせ, 利用開始登録済み:True」を含むこと
    

Scenario: 秘密の質問登録_正常系_登録ボタン押下_ユーザー情報に紐付いた秘密の質問回答情報が既に存在する場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「CARADA ID 利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Then レイアウトがデフォルトであること
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問1」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「アカウント管理」を含むこと 
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | b@b.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then ユーザーDBの「SecurityQuestionAnswers」テーブルに次のデータが存在すること
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then 最後に送信されたメールタイトルが「【CARADA ID】利用開始手続き完了のお知らせ」であること
    Then 最後に送信されたメールのあて先が「b@b.com」の組であること
    Then 最後に送信されたメールの本文に「あなたのCARADA ID：\r\ntestid」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「メールアドレス：\r\nb@b\.com」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    Then Cookie名が「TwoFactorLogin」のCookieが存在すること
    Given Cookie名「TwoFactorLogin」のCookieの値をCurrentのLastCookieValueとして設定する
    Then ストレージの「TwoFactorLogin」に次のデータが存在すること
    | PartitionKey              | RowKey | UserId                               |
    | <Current:LastCookieValue> | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    Given ストレージの「TwoFactorLogin」テーブルのダンプを取る
    # ログチェック
    Then 「Access」ログのログレベル「INFO」の最後から「8」回目の出力に「SecurityQuestionSelectId="1", Answer="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「7」回目の出力に「利用開始成功」を含むこと
