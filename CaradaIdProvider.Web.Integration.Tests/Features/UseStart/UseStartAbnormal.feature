﻿Feature: UseStartAbnormal
    利用開始処理の異常系

Scenario: 利用開始_異常系_直接アクセスでシステムエラー
    Given 「/User/UseStartView」にブラウザでアクセスする
    Then 内容「一定時間アクセスがなかったため、タイムアウトしました。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「There is no TempData[ Login ]」を含むこと
     # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと

Scenario: 利用開始_異常系_開始ボタン押下_メールアドレス設定なし
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始」を含むこと
    Then IDが「Email」の入力値は「」であること
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスを入力してください。」であること

Scenario: 利用開始_異常系_開始ボタン押下_メールアドレス不正でエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「.a@@@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始」を含むこと
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスの形式に誤りがあります。」であること
    Then IDが「Email」の入力値は「.a@@@a.com」であること
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: 利用開始_異常系_開始ボタン押下_ユーザー情報が存在しないでシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    # ログイン後にユーザー情報を削除
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始_異常系_開始ボタン押下_ユーザーのEmailConfirmedが1でシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    # ログイン後にユーザー情報のEmailConfirmedを1に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始_異常系_開始ボタン押下_ユーザーの企業ユーザーフラグが1以外でシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 0            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始_異常系_開始ボタン押下_メアドに紐付くユーザーが既に存在しEmailConfirmedが1
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email     | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName  | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>    | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    | 5d74efe8-7c62-496e-97cf-3df1a9a08489 | aBc@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 92e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart2 | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    # 完全一致のメアド
    Given IDが「Email」に「aBc@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始」を含むこと
    Then dataValmsgForが「Email」のエラーメッセージは「既に登録済みのメールアドレスです。」であること
    Then IDが「Email」の入力値は「aBc@a.com」であること
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは別のユーザーが使用済みです」を含むこと
    # 大文字小文字違いのメアド
    Given IDが「Email」に「AbC@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始」を含むこと
    Then dataValmsgForが「Email」のエラーメッセージは「既に登録済みのメールアドレスです。」であること
    Then IDが「Email」の入力値は「AbC@a.com」であること
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは別のユーザーが使用済みです」を含むこと

Scenario: 利用開始認証_異常系_直接アクセスでシステムエラー
    Given 「/User/UseStartVerifyCode」にブラウザでアクセスする
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「HTTP404エラー」を含むこと
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_認証コード設定なし
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 4                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードを入力してください。」であること

Scenario: 利用開始認証_異常系_認証ボタン押下_ユーザー情報が存在しないでシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 画面表示後にユーザー情報を削除
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_ユーザーのEmailConfirmedが1でシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 画面表示後にユーザー情報のEmailConfirmedを1に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_ユーザー情報のEmailがnullでシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 画面表示後にユーザー情報のEmailをnullに更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_前画面引継のEmail違いでシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 画面表示後にユーザー情報のEmailを利用開始画面で設定された値と異なる値に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | b@b.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_ユーザーの企業ユーザーフラグが1以外でシステムエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 画面表示後にユーザー情報の企業ユーザーフラグを0に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 0            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_認証コード未発行状態でエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 画面表示後にユーザー情報の認証コード有効期限をnullに更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_認証コード失敗回数5回到達でエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 認証コード失敗回数が5回になるまで失敗を繰り返す
    Given IDが「VerifyCode」に「1」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「1」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a@a.com」を含むこと
    Given IDが「VerifyCode」に「2」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「2」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a@a.com」を含むこと
    Given IDが「VerifyCode」に「3」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「3」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a@a.com」を含むこと
    Given IDが「VerifyCode」に「4」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「4」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 内容「メールアドレス：a@a.com」を含むこと
    Given IDが「VerifyCode」に「5」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then IDが「VerifyCode」の入力値は「5」であること
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 内容「メールアドレス：a@a.com」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 認証コード有効期限と認証コード失敗回数が更新されていることの確認
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <!NULL>                              | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <!NULL>                 | 5                   |
    # 認証コード失敗回数が5回到達後に正しいコードを入れてもエラーとなること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 内容「メールアドレス：a@a.com」を含むこと
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「4」回目の出力に「認証コード入力失敗」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「3」回目の出力に「認証コード入力失敗」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「2」回目の出力に「認証コード入力失敗」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「2」回目の出力に「認証コード無効化」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_認証コード有効期限切れでエラー
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    # 画面表示後にユーザー情報の認証コード有効期限を過去に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

Scenario: 利用開始認証_異常系_認証ボタン押下_一度戻り、前回発行された認証コードを入力した場合
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL>  | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | UseStart | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「UseStart」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というシナリオ変数へ設定する
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given IDが「VerifyCode」に「<Current:VerifyCode>」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「POST /User/UseStartVerifyCode」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「VerifyCode="<Current:VerifyCode>"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと

Scenario: 秘密の質問登録_異常系_直接アクセスした場合
    Given 「/User/SecurityQuestionRegist」にブラウザでアクセスする
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「HTTP404エラー」を含むこと
     # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
   
Scenario: 秘密の質問登録_異常系_登録するボタン押下_秘密の質問選択無しの場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Given IDが「Answer」に「てすと」を入力する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Then IDが「SecurityQuestionSelectId」の入力値は「」であること
    Then dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を選択してください。」であること

Scenario: 秘密の質問登録_異常系_登録するボタン押下_バリデーションエラー
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Then IDが「Answer」の入力値は「」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えを入力してください。」であること
    # 秘密の質問の答えが不正な場合
    Given IDが「Answer」に「abcdef12345」を入力する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then IDが「Answer」の入力値は「abcdef12345」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    # 秘密の質問の答えの文字数上限超過
    Given IDが「Answer」に「あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもゃやゅゆょよらりるれろゎわゐゑを」を入力する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then IDが「Answer」の入力値は「あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもゃやゅゆょよらりるれろゎわゐゑを」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    Given 「1」秒待ち
    # 秘密の質問の答えに変な漢字が入っていてもアベンドせずバリデーションエラーになりAccessログも出ることの確認
    Given IDが「Answer」に「繫」を入力する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then IDが「Answer」の入力値は「繫」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えは全角ひらがな50文字以内で入力してください。」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionRegist」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「SecurityQuestionSelectId="2"」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「Answer="*****"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: 秘密の質問登録_異常系_登録するボタン押下_ユーザー情報が存在しない場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問登録_異常系_登録するボタン押下_ユーザー情報のEmailがnullの場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    # 画面表示後にユーザー情報のEmailをnullに更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email  | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <NULL> | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問登録_異常系_登録するボタン押下_ユーザー情報のEmailConfirmedが1の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    # 画面表示後にユーザー情報のEmailConfirmedを1に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | b@b.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問登録_異常系_登録するボタン押下_ユーザー情報のEmailが前画面引継のEmailと異なる場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    # 画面表示後にユーザー情報のEmailを利用開始画面で設定された値と異なる値に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | c@c.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問登録_異常系_登録するボタン押下_ユーザー情報の企業ユーザーフラグが1以外の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    # 画面表示後にユーザー情報の企業ユーザーフラグを0に更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | b@b.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 0            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: 秘密の質問登録_異常系_登録するボタン押下_設定された秘密の質問IDに合致する秘密の質問が取得できない場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    # 画面表示後に秘密の質問マスタを削除する
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then IDが「SecurityQuestionSelectId」の入力値は「2」であること
    Then dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を選択してください。」であること
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「秘密の質問が不正です」を含むこと

Scenario: 秘密の質問登録_異常系_登録するボタン押下_認証成功後にログイン失敗した場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    # 登録ボタン押下前にパスワードをログインできない値に更新
    Given シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する
    | Field         | Value                   |
    | PasswordHash  | a                       |
    Given ユーザーDBの「AspNetUsers」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する
    | Id                                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと