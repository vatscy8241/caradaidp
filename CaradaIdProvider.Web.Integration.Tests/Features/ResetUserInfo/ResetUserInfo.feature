﻿Feature: ResetUserInfo
    管理者用API ユーザー情報リセットAPIの正常系

Background: 
    監査ログは全てアプリケーションログに多重で出力されている。(web.configのルールで)
    そのため、多重出力が正常に動いていることをレスポンスの部分だけで確認すれば十分。

Scenario: ResetUserInfo_正常系_利用開始前ユーザーでinisitalizationが1
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount | UpdateDateUtc           | LockoutEndDateUtc       | Email  | SecurityStamp                        |
    | 4d74efa8-7c62-496f-97cf-3df1a9a08489 | 0              | <NULL>      | 0                    | 1                | 1              | 1                 | testuser | 1            | 2014-12-10 06:59:58.743 | 0                   | 2016-03-01 00:00:00.000 | 2016-03-01 00:00:00.000 | <NULL> | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    #Given CARADAのユーザー登録APIへのリクエストパラメータ用にIssuerとSecretKeyを設定する
    Given API固有パラメータをField->Value形式で設定する
    | Field          | Value    |
    | carada_id      | testuser |
    | initialization | 1        |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「200」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「initialization」の値が「1」であること
    Then APIレスポンスボディのpassword値が簡素なパスワード形式かつ8文字であること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | UserName | EmailConfirmed | PasswordHash | TwoFactorEnabled | CreateDateUtc | AuthCodeFailedCount | LockoutEnabled | EmployeeFlag | AccessFailedCount | LockoutEndDateUtc | UpdateDateUtc | SecurityStamp | Email  |
    | testuser | 0              | <!NULL>      | 1                | <!NULL>       | 0                   | 1              | 1            | 0                 | <NULL>            | <!NULL>       | <!NULL>       | <NULL> |
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="testuser", Initialization="1"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="1"]」を含むこと
    Then 「AUDIT」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="1"]」を含むこと
    ##ユーザー登録後の初回ログイン確認
    Then ユーザー登録後、CARADA_ID「testuser」のユーザーが利用開始画面へ遷移できること

Scenario: ResetUserInfo_正常系_利用開始前ユーザーでinisitalizationがなし
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount | UpdateDateUtc           | LockoutEndDateUtc       | Email  | SecurityStamp                        |
    | 4d74efa8-7c62-496f-97cf-3df1a9a08489 | 0              | <NULL>      | 0                    | 1                | 1              | 1                 | testuser | 1            | 2014-12-10 06:59:58.743 | 0                   | 2016-03-01 00:00:00.000 | 2016-03-01 00:00:00.000 | <NULL> | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    #Given CARADAのユーザー登録APIへのリクエストパラメータ用にIssuerとSecretKeyを設定する
    Given API固有パラメータをField->Value形式で設定する
    | Field          | Value    |
    | carada_id      | testuser |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「200」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「initialization」の値が「0」であること
    Then APIレスポンスボディのpassword値が簡素なパスワード形式かつ8文字であること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | UserName | EmailConfirmed | PasswordHash | TwoFactorEnabled | CreateDateUtc | AuthCodeFailedCount | LockoutEnabled | EmployeeFlag | AccessFailedCount | LockoutEndDateUtc | UpdateDateUtc | SecurityStamp | Email  |
    | testuser | 0              | <!NULL>      | 1                | <!NULL>       | 0                   | 1              | 1            | 0                 | <NULL>            | <!NULL>       | <!NULL>       | <NULL> |
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="testuser", Initialization=""」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="0"]」を含むこと
    Then 「AUDIT」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="0"]」を含むこと
    ##ユーザー登録後の初回ログイン確認
    Then ユーザー登録後、CARADA_ID「testuser」のユーザーが利用開始画面へ遷移できること

Scenario: ResetUserInfo_正常系_利用開始後ユーザーでinisitalizationが1
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount | UpdateDateUtc           | LockoutEndDateUtc       | Email           | SecurityStamp                        |
    | 4d74efa8-7c62-496f-97cf-3df1a9a08489 | 1              | <NULL>      | 0                    | 1                | 1              | 1                 | testuser | 1            | 2014-12-10 06:59:58.743 | 0                   | 2016-03-01 00:00:00.000 | 2016-03-01 00:00:00.000 | test@test.co.jp | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    #Given CARADAのユーザー登録APIへのリクエストパラメータ用にIssuerとSecretKeyを設定する
    Given API固有パラメータをField->Value形式で設定する
    | Field          | Value    |
    | carada_id      | testuser |
    | initialization | 1        |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「200」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「initialization」の値が「1」であること
    Then APIレスポンスボディのpassword値が簡素なパスワード形式かつ8文字であること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | UserName | EmailConfirmed | PasswordHash | TwoFactorEnabled | CreateDateUtc | AuthCodeFailedCount | LockoutEnabled | EmployeeFlag | AccessFailedCount | LockoutEndDateUtc | UpdateDateUtc | SecurityStamp | Email  |
    | testuser | 0              | <!NULL>      | 1                | <!NULL>       | 0                   | 1              | 1            | 0                 | <NULL>            | <!NULL>       | <!NULL>       | <NULL> |
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="testuser", Initialization="1"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="1"]」を含むこと
    Then 「AUDIT」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="1"]」を含むこと
    ##ユーザー登録後の初回ログイン確認
    Then ユーザー登録後、CARADA_ID「testuser」のユーザーが利用開始画面へ遷移できること

Scenario: ResetUserInfo_正常系_利用開始後ユーザーでinisitalizationなし
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount | UpdateDateUtc           | LockoutEndDateUtc       | Email           | SecurityStamp                        |
    | 4d74efa8-7c62-496f-97cf-3df1a9a08489 | 1              | <NULL>      | 0                    | 1                | 1              | 1                 | testuser | 1            | 2014-12-10 06:59:58.743 | 0                   | 2016-03-01 00:00:00.000 | 2016-03-01 00:00:00.000 | test@test.co.jp | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efa8-7c62-496f-97cf-3df1a9a08489 | 1                  | 質問の答え |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    #Given CARADAのユーザー登録APIへのリクエストパラメータ用にIssuerとSecretKeyを設定する
    Given API固有パラメータをField->Value形式で設定する
    | Field          | Value    |
    | carada_id      | testuser |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「200」であること
    Then APIレスポンスボディの「carada_id」の値が「testuser」であること
    Then APIレスポンスボディの「initialization」の値が「0」であること
    Then APIレスポンスボディのpassword値が簡素なパスワード形式かつ8文字であること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | UserName | EmailConfirmed | PasswordHash | TwoFactorEnabled | CreateDateUtc | AuthCodeFailedCount | LockoutEnabled | EmployeeFlag | AccessFailedCount | LockoutEndDateUtc | UpdateDateUtc | SecurityStamp | Email           |
    | testuser | 1              | <!NULL>      | 1                | <!NULL>       | 0                   | 1              | 1            | 0                 | <NULL>            | <!NULL>       | <!NULL>       | test@test.co.jp |
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="testuser", Initialization=""」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="0"]」を含むこと
    Then 「AUDIT」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="testuser", password="*****", initialization="0"]」を含むこと
    ##ユーザー登録後の初回ログイン確認
    Then ユーザー登録後、CARADA_ID「testuser」のユーザーがアカウント画面へ遷移できること