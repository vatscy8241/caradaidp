﻿Feature: ResetUserInfoAbnormal
    管理者用API ユーザー情報リセットAPIの異常系

Scenario: ResetUserInfo_異常系_JWT検証_リクエストボディ無し
    Given 空のリクエストボディを指定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] リクエストパラメータにJWTのTokenが見つかりません」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_リクエストボディが空オブジェクト
    Given リクエストボディに空のオブジェクトを指定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_リクエストボディがJSON以外
    Given JSON形式でないリクエストボディを指定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「parameters:[{abc]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「Error」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「Error」の最後から「1」回目の出力に「[JWT_Invalid] リクエストパラメータにJWTのTokenが見つかりません」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_リクエストボディに空のtokenを指定する
    Given リクエストボディに空のtokenを指定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token=""]」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_iss未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given iss未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Issがnullまたは空です。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_iat未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given iat未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Iatがnullです。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_exp未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given exp未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Expがnullです。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_nbf未指定のJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given nbf未指定のJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_request」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Nbfがnullです。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST]」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_request", error_description="JWTが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_無効なissを含むJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given 無効なissを含むJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_issuer」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] DBには未登録のIssuerです。(issuer=invalid_iss)」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_共通でない秘密鍵で署名したJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given 共通でない秘密鍵で署名したJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_issuer」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] SecurityTokenの署名検証に失敗しました。」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_issuer", error_description="JWTが不正です"]」を含むこと
    
Scenario: ResetUserInfo_異常系_JWT検証_nbfに未来時刻を指定したJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given nbfに未来時刻を指定したJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_life_time」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Nbfが未来です。(Nbf=」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_JWT検証_expに現在時刻を指定したJWT形式でリクエストボディを設定する
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given expに現在時刻を指定したJWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「」であること
    Then APIレスポンスボディの「error」の値が「jwt_invalid_life_time」であること
    Then APIレスポンスボディの「error_description」の値が「JWTが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「[JWT_Invalid] Expが過去です。(Exp=」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="jwt_invalid_life_time", error_description="JWTが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_パラメータ検証_CARADA_IDはNULL
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
    | Field          | Value    |
    | initialization | 1        |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「error」の値が「invalid_carada_id」であること
    Then APIレスポンスボディの「error_description」の値が「JWTのパラメーターが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="", Initialization="1"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「[JWT_Invalid] [Condition] ErrorAttribute=RequiredAttribute, ErrorMessage=invalid_carada_id」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="", Initialization="1"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_パラメータ検証_CARADA_ID空文字列
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given API固有パラメータをField->Value形式で設定する
    | Field          | Value |
    | carada_id      |       |
    | initialization | 1     |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「error」の値が「invalid_carada_id」であること
    Then APIレスポンスボディの「error_description」の値が「JWTのパラメーターが不正です」であること
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="", Initialization="1"」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「2」回目の出力に「[JWT_Invalid] [Condition] ErrorAttribute=RequiredAttribute, ErrorMessage=invalid_carada_id」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「WARN」の最後から「1」回目の出力に「バリデーションチェックエラー」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="", Initialization="1"」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="", error="invalid_carada_id", error_description="JWTのパラメーターが不正です"]」を含むこと

Scenario: ResetUserInfo_異常系_ユーザ情報が取得できない
    Given ユーザーDBの「Issuers」テーブルを初期化する
    Given ユーザーDBの「Issuers」テーブルへ次のテストデータを投入する
    | IssuerId  | SecretKey                                                          | Remark                                 |
    | CaradaIdP | b_+sUo)=c67k^*:K9jFL)(6\|;[VmL{TV]hs$\|v9[4W48VDR{ck0YA6zs?ypvE^wL | 開発テスト用の発行者情報(ローカル・CI) |
    Given JwtのIssuerを「CaradaIdP」として設定する
    Given JwtのSecretKeyを「b_+sUo)=c67k^*:K9jFL)(6|;[VmL{TV]hs$|v9[4W48VDR{ck0YA6zs?ypvE^wL」として設定する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount | UpdateDateUtc           |
    | 4d74efa8-7c62-496f-97cf-3df1a9a08489 | 1              | <NULL>      | 0                    | 1                | 1              | 0                 | testuser | 1            | 2014-12-10 06:59:58.743 | 0                   | 2016-03-01 00:00:00.000 |
    Given API固有パラメータをField->Value形式で設定する
    | Field          | Value |
    | carada_id      | user  |
    | initialization | 1     |
    Given JWT形式でリクエストボディを設定する
    When API「/api/Management/ResetUserInfo」にPOSTでアクセスしたとき
    Then APIのHTTPステータスコードが「400」であること
    Then APIレスポンスボディの「carada_id」の値が「user」であること
    Then APIレスポンスボディの「error」の値が「invalid_carada_id」であること
    Then APIレスポンスボディの「error_description」の値が「CARADA IDに紐付くユーザー情報がありません。」であること
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="user", Initialization="1" 」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="user", error="invalid_carada_id", error_description="CARADA IDに紐付くユーザー情報がありません。"]」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Application」ログのログレベル「ERROR」の最後から「1」回目の出力に「CARADA IDに紐付くユーザー情報がありません。(CARADA ID=user)」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[IssUnknown] - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「3」回目の出力に「[REQUEST] parameters:[token="」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「2」回目の出力に「[REQUEST PARAMS] CaradaId="user", Initialization="1" 」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「CaradaIdP - POST /api/Management/ResetUserInfo」を含むこと
    Then 「Audit」ログのログレベル「INFO」の最後から「1」回目の出力に「[RESPONSE] parameters:[carada_id="user", error="invalid_carada_id", error_description="CARADA IDに紐付くユーザー情報がありません。"]」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | UserName | EmailConfirmed | TwoFactorEnabled | CreateDateUtc | AuthCodeFailedCount | LockoutEnabled | EmployeeFlag | AccessFailedCount | UpdateDateUtc           |
    | testuser | 1              | 1                | <!NULL>       | 0                   | 1              | 1            | 0                 | 2016-03-01 00:00:00.000 |
