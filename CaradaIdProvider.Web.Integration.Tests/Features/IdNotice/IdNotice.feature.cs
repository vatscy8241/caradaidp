﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace CaradaIdProvider.Web.Integration.Tests.Features.IdNotice
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute()]
    public partial class IdNoticeFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "IdNotice.feature"
#line hidden
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitializeAttribute()]
        public static void FeatureSetup(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext testContext)
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "IdNotice", "  ID連絡の正常系", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassCleanupAttribute()]
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitializeAttribute()]
        public virtual void TestInitialize()
        {
            if (((TechTalk.SpecFlow.FeatureContext.Current != null) 
                        && (TechTalk.SpecFlow.FeatureContext.Current.FeatureInfo.Title != "IdNotice")))
            {
                CaradaIdProvider.Web.Integration.Tests.Features.IdNotice.IdNoticeFeature.FeatureSetup(null);
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCleanupAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("ID連絡画面_正常系_未ログイン_メールアドレス入力でID連絡できること")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "IdNotice")]
        public virtual void ID連絡画面_正常系_未ログイン_メールアドレス入力でID連絡できること()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("ID連絡画面_正常系_未ログイン_メールアドレス入力でID連絡できること", ((string[])(null)));
#line 4
this.ScenarioSetup(scenarioInfo);
#line 5
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table1.AddRow(new string[] {
                        "4d74efa8-7c62-496e-97cf-3df1a9a08489",
                        "a.@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "aaa.com",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 6
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table1, "Given ");
#line 9
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 10
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 11
    testRunner.Then("内容「CARADA ID ログイン」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 12
    testRunner.When("IDが「CaradaIdNotice」のリンクをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 13
    testRunner.Then("内容「CARADA ID ID再通知」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 14
    testRunner.Then("レイアウトがデフォルトであること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 15
    testRunner.Then("formの「autocomplete」属性が存在しないこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 16
    testRunner.Then("仮想URLが「/User/IdNotice」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 17
    testRunner.Then("UserInsightの仮想URLが「/User/IdNotice」であり、他のパラメータも正しく設定されていること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 18
    testRunner.Given("IDが「Email」に「a.@a.com」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 19
    testRunner.When("「name」という属性の値が「Notice」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 20
    testRunner.Then("内容「CARADA ID ログイン」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 21
    testRunner.Then("最後に送信されたメールタイトルが「【CARADA ID】ご登録IDのお知らせ」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 22
    testRunner.Then("最後に送信されたメールのあて先が「a.@a.com」の組であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 23
    testRunner.Then("最後に送信されたメールの本文に「お客様のCARADA IDは以下となります。」の正規表現がマッチすること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 24
    testRunner.Then("最後に送信されたメールの本文に「■あなたのCARADA ID：\\r\\naaa.com」の正規表現がマッチすること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 25
    testRunner.Then("「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「UserId:4d74efa8-7c62-496e-97cf-3df1a9a08489へ" +
                    "のメール送信を開始します。件名:【CARADA ID】ご登録IDのお知らせ, 利用開始登録済み:True」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 26
    testRunner.Then("「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ID連絡完了(UserId=4d74efa8-7c62-496e-97cf-3df1a9" +
                    "a08489, CARADA ID=aaa.com)」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("ID連絡画面_正常系_ログイン済_メールアドレス入力でID連絡できること")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "IdNotice")]
        public virtual void ID連絡画面_正常系_ログイン済_メールアドレス入力でID連絡できること()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("ID連絡画面_正常系_ログイン済_メールアドレス入力でID連絡できること", ((string[])(null)));
#line 28
this.ScenarioSetup(scenarioInfo);
#line 30
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table2.AddRow(new string[] {
                        "4d74efa8-7c62-496e-97cf-3df1a9a08410",
                        "a..@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "aaa.com",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 31
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table2, "Given ");
#line 34
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table3.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "3"});
#line 35
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table3, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "UserId",
                        "SecurityQuestionId",
                        "Answer"});
            table4.AddRow(new string[] {
                        "4d74efa8-7c62-496e-97cf-3df1a9a08410",
                        "1",
                        "質問の答え"});
#line 38
    testRunner.Given("ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する", ((string)(null)), table4, "Given ");
#line 41
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 42
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 43
    testRunner.Then("内容「CARADA ID ログイン」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 44
    testRunner.Then("UserInsightの仮想URLが「/User/Login」であり、他のパラメータも正しく設定されていること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 45
    testRunner.Given("ID「aaa.com」、パスワード「0000000a」のアカウントでログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 46
    testRunner.Given("「/User/IdNoticeView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 47
    testRunner.Then("内容「CARADA ID ID再通知」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 48
    testRunner.Then("レイアウトがデフォルトであること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 49
    testRunner.Then("UserInsightの仮想URLが「/User/IdNotice」であり、他のパラメータも正しく設定されていること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 50
    testRunner.Given("IDが「Email」に「A..@a.com」を入力する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 51
    testRunner.When("「name」という属性の値が「Notice」のボタンをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 53
    testRunner.Then("内容「CARADA ID アカウント管理」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 54
    testRunner.Then("UserInsightの仮想URLが「/UserSettings/Index」であり、他のパラメータも正しく設定されていること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 55
    testRunner.Then("最後に送信されたメールタイトルが「【CARADA ID】ご登録IDのお知らせ」であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 56
    testRunner.Then("最後に送信されたメールのあて先が「A..@a.com」の組であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 57
    testRunner.Then("最後に送信されたメールの本文に「お客様のCARADA IDは以下となります。」の正規表現がマッチすること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 58
    testRunner.Then("最後に送信されたメールの本文に「■あなたのCARADA ID：\\r\\naaa.com」の正規表現がマッチすること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 59
    testRunner.Then("「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「ID連絡完了(UserId=4d74efa8-7c62-496e-97cf-3df1a9" +
                    "a08410, CARADA ID=aaa.com)」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("ID連絡画面_正常系_戻るボタン押下後にログイン画面からログインできること")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "IdNotice")]
        public virtual void ID連絡画面_正常系_戻るボタン押下後にログイン画面からログインできること()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("ID連絡画面_正常系_戻るボタン押下後にログイン画面からログインできること", ((string[])(null)));
#line 61
this.ScenarioSetup(scenarioInfo);
#line 62
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table5 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table5.AddRow(new string[] {
                        "4d74efa8-7c62-496e-97cf-3df1a9a0842",
                        "a@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "aaa.com",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 63
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table5, "Given ");
#line 66
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table6 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table6.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "3"});
#line 67
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table6, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table7 = new TechTalk.SpecFlow.Table(new string[] {
                        "UserId",
                        "SecurityQuestionId",
                        "Answer"});
            table7.AddRow(new string[] {
                        "4d74efa8-7c62-496e-97cf-3df1a9a0842",
                        "1",
                        "質問の答え"});
#line 70
    testRunner.Given("ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する", ((string)(null)), table7, "Given ");
#line 73
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 74
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 75
    testRunner.Then("内容「CARADA ID ログイン」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 76
    testRunner.When("IDが「CaradaIdNotice」のリンクをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 77
    testRunner.Then("内容「CARADA ID ID再通知」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 78
    testRunner.Then("レイアウトがデフォルトであること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 79
    testRunner.When("IDが「Back」のリンクをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 80
    testRunner.Then("内容「CARADA ID ログイン」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 81
    testRunner.Given("ID「aaa.com」、パスワード「0000000a」のアカウントでログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 82
    testRunner.Then("内容「CARADA ID アカウント管理」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
