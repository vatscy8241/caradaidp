﻿Feature: IdNotice.fp
    ID連絡の正常系(FP版)

Scenario: ID連絡画面_FP版_正常系_未ログイン_メールアドレス入力でID連絡できること
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email    | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efa8-7c62-496e-97cf-3df1a9a08489 | a.@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given HTTPヘッダーに「User-Agent=J-PHONE/3.0/J-SH10」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「CARADA ID ID連絡」を含むこと
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「Email」に「a.@a.com」を入力する
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】ご登録IDのお知らせ」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「お客様のCARADA IDは以下となります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「■あなたのCARADA ID：\r\naaa.com」の正規表現がマッチすること
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ID連絡完了(UserId=4d74efa8-7c62-496e-97cf-3df1a9a08489, CARADA ID=aaa.com)」を含むこと

Scenario: ID連絡画面_FP版_正常系_画面表示後に戻るボタン押下
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given HTTPヘッダーに「User-Agent=J-PHONE/3.0/J-SH10」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「CARADA ID ID連絡」を含むこと
    Then レイアウトがFP用であること
    # 「戻る」の確認
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
