﻿Feature: IdNoticeAbnormal
    ID連絡の異常系

Scenario: ID連絡画面_異常系_バリデーションチェック
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「CARADA ID ID再通知」を含むこと
    #必須チェック
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスを入力してください。」であること
    Given 「1」秒待ち
    #メールアドレス形式不正
    Given IDが「Email」に「@@@a.com」を入力する
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスの形式に誤りがあります。」であること
    Then レイアウトがデフォルトであること

Scenario: ID連絡画面_異常系_メールアドレス該当なし_エラー表示
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「CARADA ID ID再通知」を含むこと
    Given IDが「Email」に「notice_test@test.com」を入力する
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「このメールアドレスに紐付くユーザー登録がありません。」であること
    Then レイアウトがデフォルトであること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは未登録のものです」を含むこと

Scenario: ID連絡画面_異常系_該当メールアドレスは利用開始前_エラー表示
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efa8-7c62-496e-97cf-3df1a9a08450 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「CARADA ID ID再通知」を含むこと
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「このメールアドレスに紐付くユーザー登録がありません。」であること
    Then レイアウトがデフォルトであること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは未登録のものです」を含むこと
