﻿Feature: IdNoticeAbnormal.fp
    ID連絡の異常系(FP版)

Scenario: ID連絡画面_FP版_異常系_ログイン中_エラー表示_入力復元あり
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efa8-7c62-496e-97cf-3df1a9a08450 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「aaa.com」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID 利用開始」を含むこと
    Then レイアウトがFP用であること
    Given 「/User/IdNoticeView」にブラウザでアクセスする
    Then 内容「CARADA ID ID連絡」を含むこと
    Then レイアウトがFP用であること
    Given IDが「Email」に「a@a.com」を入力する
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    Then 内容「CARADA ID ID連絡」を含むこと
    Then レイアウトがFP用であること
    Then dataValmsgForが「Email」のエラーメッセージは「このメールアドレスに紐付くユーザー登録がありません。」であること
    Then IDが「Email」の入力値は「a@a.com」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは未登録のものです」を含むこと
