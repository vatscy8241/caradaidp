﻿Feature: IdNotice
    ID連絡の正常系

Scenario: ID連絡画面_正常系_未ログイン_メールアドレス入力でID連絡できること
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email    | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efa8-7c62-496e-97cf-3df1a9a08489 | a.@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「CARADA ID ID再通知」を含むこと
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性が存在しないこと
    Then 仮想URLが「/User/IdNotice」であること
    Then UserInsightの仮想URLが「/User/IdNotice」であり、他のパラメータも正しく設定されていること
    Given IDが「Email」に「a.@a.com」を入力する
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】ご登録IDのお知らせ」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「お客様のCARADA IDは以下となります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「■あなたのCARADA ID：\r\naaa.com」の正規表現がマッチすること
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「UserId:4d74efa8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】ご登録IDのお知らせ, 利用開始登録済み:True」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ID連絡完了(UserId=4d74efa8-7c62-496e-97cf-3df1a9a08489, CARADA ID=aaa.com)」を含むこと

Scenario: ID連絡画面_正常系_ログイン済_メールアドレス入力でID連絡できること
    # 通常ログイン中はID連絡案内画面に行く遷移はないが操作自体を拒絶するものではない
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email     | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efa8-7c62-496e-97cf-3df1a9a08410 | a..@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efa8-7c62-496e-97cf-3df1a9a08410 | 1                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then UserInsightの仮想URLが「/User/Login」であり、他のパラメータも正しく設定されていること
    Given ID「aaa.com」、パスワード「0000000a」のアカウントでログインする
    Given 「/User/IdNoticeView」にブラウザでアクセスする
    Then 内容「CARADA ID ID再通知」を含むこと
    Then レイアウトがデフォルトであること
    Then UserInsightの仮想URLが「/User/IdNotice」であり、他のパラメータも正しく設定されていること
    Given IDが「Email」に「A..@a.com」を入力する
    When 「name」という属性の値が「Notice」のボタンをクリックしたとき
    # ログイン中なのでアカウント情報設定画面に遷移する
    Then 内容「CARADA ID アカウント管理」を含むこと
    Then UserInsightの仮想URLが「/UserSettings/Index」であり、他のパラメータも正しく設定されていること
    Then 最後に送信されたメールタイトルが「【CARADA ID】ご登録IDのお知らせ」であること
    Then 最後に送信されたメールのあて先が「A..@a.com」の組であること
    Then 最後に送信されたメールの本文に「お客様のCARADA IDは以下となります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文に「■あなたのCARADA ID：\r\naaa.com」の正規表現がマッチすること
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「ID連絡完了(UserId=4d74efa8-7c62-496e-97cf-3df1a9a08410, CARADA ID=aaa.com)」を含むこと

Scenario: ID連絡画面_正常系_戻るボタン押下後にログイン画面からログインできること
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                  | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efa8-7c62-496e-97cf-3df1a9a0842 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                              | SecurityQuestionId | Answer     |
    | 4d74efa8-7c62-496e-97cf-3df1a9a0842 | 1                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「CARADA ID ID再通知」を含むこと
    Then レイアウトがデフォルトであること
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「aaa.com」、パスワード「0000000a」のアカウントでログインする
    Then 内容「CARADA ID アカウント管理」を含むこと
