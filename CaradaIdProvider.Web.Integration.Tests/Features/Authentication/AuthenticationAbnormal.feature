﻿Feature: AuthenticationRequestAbnormal
    Authentication Requestをしてエラーレスポンスがあること

Scenario: AuthenticationRequest_異常系_invalid redirect_uriエラー
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    | 46310e37439547ceb9a138336776c3a2 | test             | 532fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                          |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    | 3     | 46310e37439547ceb9a138336776c3a2 | https://localhost:12345/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # Authentication Request redirect_uriなし
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「redirect_uri」に「」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「error: invalid redirect_uri」を含むこと
    # Authentication Request client_idが不正
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「client_id」に「aaaa」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「error: invalid redirect_uri」を含むこと
    # Authentication Request client_idとredirect_uriが一致しない
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「redirect_uri」に「https://localhost:12345/RpMock/Token」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「error: invalid redirect_uri」を含むこと
    # Authentication Request client_idが大文字小文字違い
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「client_id」に「047535B4582D4F87A71C27262F27A258」を入力する
    Given IDが「redirect_uri」に「https://localhost:44306/RpMock/Token」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「error: invalid redirect_uri」を含むこと

Scenario: AuthenticationRequest_異常系_invalid_redirect_urlエラー_複数のredirect_url
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                          |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    | 3     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/Dummy        |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「redirect_uri」に「https://localhost:12345/RpMock/Token」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「error: invalid redirect_uri」を含むこと

Scenario: AuthenticationRequest_異常系_パラメータ返却ケース(server_error)以外のエラー
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                          |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 2                  | 質問の答え |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # Authentication Request scopeなし
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「scope」に「」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Authentication Request Error」を含むこと
    Then 内容「invalid_scope」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「Authentication Request：」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「client_id=36310e37439547ceb9a138336776c3a2」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「scope=&」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「response_type=code」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「redirect_uri=https%3A%2F%2Flocalhost%3A44306%2FRpMock%2FToken」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「parameters:」を含むこと
    # Authentication Request scope不正
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「scope」に「a」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Authentication Request Error」を含むこと
    Then 内容「invalid_scope」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「Authentication Request：」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「client_id=36310e37439547ceb9a138336776c3a2」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「scope=a」を含むこと
    # Authentication Request response_typeなし
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「response_type」に「」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Authentication Request Error」を含むこと
    Then 内容「invalid_request」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「response_type=&」を含むこと
    # Authentication Request response_type不正
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「response_type」に「aaaa」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Authentication Request Error」を含むこと
    Then 内容「unsupported_response_type」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「response_type=aaaa」を含むこと
    # Authentication Request 対象ユーザのAuthorizedUsersが取得できない
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then Cookie名が「CaradaIdLogin」の有効期限が設定されていないCookieが存在すること
    Given ユーザーDBの「AuthorizedUsers」テーブルを初期化する
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Authentication Request Error」を含むこと
    Then 内容「unauthorized_client」を含むこと

Scenario: AuthenticationRequest_異常系_パラメータ返却ケース(server_error)のエラー
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                          |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    # EmployeeFlagを0にしてエラーを発生させる
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 0            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # Authentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「Authentication Request Error」を含むこと
    Then 内容「server_error」を含むこと

# RC環境に上がり次第コメントを解除しRC環境の設定に修正し確認
#
#Scenario: RC環境ではRpMockにアクセスすると404エラーが起こること
#    Given インターネットサイト「https://rcid-carada/RpMock」にブラウザでアクセスする
#    Then 内容「404」を含むこと
#    Given インターネットサイト「https://rcid-carada/RpMock/Auth」にブラウザでアクセスする
#    Then 内容「404」を含むこと