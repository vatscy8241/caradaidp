﻿Feature: AuthenticationRequestNormal
    Authentication Requestをして正常レスポンスがあること

Scenario: AuthenticationRequest_正常系_ログイン済み_正常レスポンスがあること_stateあり
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given ユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | Question    | DisplayOrder |
    | テスト質問1 | 1            |
    | テスト質問2 | 2            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 2                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン済み状態にする
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then Cookie名が「CaradaIdLogin」の有効期限が設定されていないCookieが存在すること
    # Authentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    # mockはデフォルトで入力済みだが、本ケースは明示的に入力する
    Given IDが「client_id」に「047535b4582d4f87a71c27262f27a258」を入力する
    Given IDが「scope」に「openid」を入力する
    Given IDが「response_type」に「code」を入力する
    Given IDが「redirect_uri」に「https://localhost:44306/RpMock/Token」を入力する
    Given IDが「state」に「ad20dbabff2c494b828ee20058fc4723」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    Then IDが「receiveState」のInnerTextに「ad20dbabff2c494b828ee20058fc4723」を含むこと
    Then ユーザーDBの「UserAuthCodes」テーブルに次のデータが存在すること
    | AuthorizationCode | UserId                               | ExpireDateUtc | RedirectUriSeqNo |
    | <!NULL>           | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <!NULL>       | 1                |
    Given ユーザーDBの「UserAuthCodes」テーブルのダンプを取る
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「Authentication Request：」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「client_id=047535b4582d4f87a71c27262f27a258」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「scope=openid」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「response_type=code」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「redirect_uri=https%3A%2F%2Flocalhost%3A44306%2FRpMock%2FToken」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「state=ad20dbabff2c494b828ee20058fc4723」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「parameters:」を含むこと

Scenario: AuthenticationRequest_正常系_未ログイン_正常応答すること_stateあり_二段階認証済
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    # 二段階認証済みにする
    Given 「/User/LoginView」に移動しつつ、Cookie名「TwoFactorLogin」値「a」有効期限「2025-12-31 11:11:11.111」のCookieを追加する
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey | RowKey | UserId                               |
    | a            | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # Authentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    # mockはデフォルトで入力済みだが、本ケースは明示的に入力する
    Given IDが「client_id」に「047535b4582d4f87a71c27262f27a258」を入力する
    Given IDが「scope」に「openid」を入力する
    Given IDが「response_type」に「code」を入力する
    Given IDが「redirect_uri」に「https://localhost:44306/RpMock/Token」を入力する
    Given IDが「state」に「ad20dbabff2c494b828ee20058fc4723」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    Then IDが「receiveState」のInnerTextに「ad20dbabff2c494b828ee20058fc4723」を含むこと
    Then ユーザーDBの「UserAuthCodes」テーブルに次のデータが存在すること
    | AuthorizationCode | UserId                               | ExpireDateUtc | RedirectUriSeqNo |
    | <!NULL>           | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <!NULL>       | 1                |
    Given ユーザーDBの「UserAuthCodes」テーブルのダンプを取る

Scenario: AuthenticationRequest_正常系_ログイン済み_正常レスポンスがあること_stateなし
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given ユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | Question    | DisplayOrder |
    | テスト質問1 | 1            |
    | テスト質問2 | 2            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 2                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # ログイン済み状態にする
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then Cookie名が「CaradaIdLogin」の有効期限が設定されていないCookieが存在すること
    # Authentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「state」に「」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    Then 内容「state : []」を含むこと
    Then ユーザーDBの「UserAuthCodes」テーブルに次のデータが存在すること
    | AuthorizationCode | UserId                               | ExpireDateUtc | RedirectUriSeqNo |
    | <!NULL>           | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <!NULL>       | 2                |
    Given ユーザーDBの「UserAuthCodes」テーブルのダンプを取る

Scenario: AuthenticationRequest_正常系_未ログイン_正常レスポンスがあること_stateなし_未二段階認証
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # Authentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「state」に「」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「Successful Authentication Response」を含むこと
    Then 内容「state : []」を含むこと
    Then ユーザーDBの「UserAuthCodes」テーブルに次のデータが存在すること
    | AuthorizationCode | UserId                               | ExpireDateUtc | RedirectUriSeqNo |
    | <!NULL>           | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <!NULL>       | 2                |
    Given ユーザーDBの「UserAuthCodes」テーブルのダンプを取る

Scenario: AuthenticationRequest_正常系_未ログイン_正常レスポンスがあること_利用開始処理
    Given ユーザーDBの「ClientMasters」テーブルを初期化する
    Given ユーザーDBの「ClientMasters」テーブルへ次のテストデータを投入する
    | ClientId                         | ClientName       | ClientSecret                     | AuthorizeConfirmFlag |
    | 047535b4582d4f87a71c27262f27a258 | testA            | f71ad7322bd24eefbdb774ddbf6429d8 | 0                    |
    | 36310e37439547ceb9a138336776c3a2 | AuthProviderTest | 432fdcfc690661f6b7c5a07b0688e873 | 0                    |
    Given IDENTを初期値にしてユーザーDBの「RedirectUriMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「RedirectUriMasters」テーブルへ次のテストデータを投入する
    | SeqNo | ClientId                         | RedirectUri                         |
    | 1     | 047535b4582d4f87a71c27262f27a258 | https://localhost:44306/RpMock/Token |
    | 2     | 36310e37439547ceb9a138336776c3a2 | https://localhost:44306/RpMock/Token |
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AuthorizedUsers」テーブルへ次のテストデータを投入する
    | UserId                               | ClientId                         | Sub |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 36310e37439547ceb9a138336776c3a2 | aaa |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 047535b4582d4f87a71c27262f27a258 | bbb |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given ユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | Question | DisplayOrder      |
    | テスト質問1   | 1            |
    | テスト質問2   | 2            |
    Given ユーザーDBの「UserAuthCodes」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # Authentication Request
    Given 「/RpMock/Auth」にブラウザでアクセスする
    Then 内容「Authentication Requestモック」を含むこと
    Given IDが「state」に「」を入力する
    When 「name」という属性の値が「authentication」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「testid」、パスワード「0000000a」のアカウントでログインする
    Then 内容「利用開始」を含むこと
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Start」のボタンをクリックしたとき
    Then 内容「利用開始認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「秘密の質問登録」を含むこと
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Regist」のボタンをクリックしたとき
    Then 内容「Successful Authentication Response」を含むこと
    Then 内容「state : []」を含むこと
    Then ユーザーDBの「UserAuthCodes」テーブルに次のデータが存在すること
    | AuthorizationCode | UserId                               | ExpireDateUtc | RedirectUriSeqNo |
    | <!NULL>           | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | <!NULL>       | 2                |
    Given ユーザーDBの「UserAuthCodes」テーブルのダンプを取る
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | b@b.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testid   | 1            | 2014-12-10 06:59:58.743 | 0                   |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
