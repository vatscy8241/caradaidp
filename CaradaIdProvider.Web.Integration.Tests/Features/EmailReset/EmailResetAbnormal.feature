﻿Feature: EmailResetAbnormal
    メールアドレス再設定の異常系

Scenario: メールアドレス再設定_異常系_直接アクセスでシステムエラー
    Given 「/UserSettings/EmailResetVerifyCode」にブラウザでアクセスする
    Then 内容「指定されたURLのページは存在しません。」を含むこと
    # ログチェック
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「HTTP404エラー」を含むこと
    # 戻るボタンのチェック
    When 「name」という属性の値が「Return」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと

Scenario: メールアドレス再設定_異常系_バリデーションチェック
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    # CaradaID未入力
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDを入力してください。」であること
    Given 「1」秒待ち
    # パスワード未入力
    Given IDが「CaradaId」に「aaauser」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードを入力してください。」であること
    Given 「1」秒待ち
    # 秘密質問未入力(無し)
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を入力してください。」であること
    Given 「1」秒待ち
    # 答え未入力
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    # 秘密の質問を登録し読み込みなおす
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「答えを入力してください。」であること
    Given 「1」秒待ち
    # メールアドレス未入力
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「テスト答え２」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「新しいメールアドレスを入力してください。」であること
    Given 「1」秒待ち
    # メールアドレスが不正
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「テスト答え２」を入力する
    Given IDが「Email」に「b@@@bcom」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「新しいメールアドレスの形式に誤りがあります。」であること
    Given 「1」秒待ち
    # 認証コード未入力
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    # メールアドレス再設定認証画面に遷移
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードを入力してください。」であること

Scenario: メールアドレス再設定_異常系_バリデーションチェック_CaradaId_Password_不正
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Then 仮想URLが「/UserSettings/EmailReset」であること
    Then UserInsightの仮想URLが「/UserSettings/EmailReset」であり、他のパラメータも正しく設定されていること
    # 1回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000b」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ID・パスワードが正しくありません」を含むこと
    Then 仮想URLが「/UserSettings/EmailReset」であること
    Then UserInsightの仮想URLが「/UserSettings/EmailReset」であり、他のパラメータも正しく設定されていること
    Given 「1」秒待ち
    # 2回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000c」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「c@c.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ID・パスワードが正しくありません」を含むこと
    Given 「1」秒待ち
    # 3回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000d」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「d@d.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ID・パスワードが正しくありません」を含むこと
    Given 「1」秒待ち
    # 4回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000e」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「e@e.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ID・パスワードが正しくありません」を含むこと
    Given 「1」秒待ち
    # CARADA IDとパスワードが正しくない（DBに登録されている組み合わせでない）場合 5回目アカウントロックされる
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000f」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「e@e.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト開始」を含むこと
    Given 「1」秒待ち
    # CARADA IDとパスワードが正しくてもアカウントロックアウト
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト中」を含むこと
    Given 「1」秒待ち
    # アカウントロック終了期限に変化がないこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # アカウントロック終了期限を過去にしてアカウントロックを解除する
    Given シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する
    | Field             | Value                   |
    | LockoutEndDateUtc | 2016-02-01 00:00:00.000 |
    Given ユーザーDBの「AspNetUsers」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する
    | Id                                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 |
    # アカウントロック解除後はメールアドレス再設定できること
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「ログイン」を含むこと
    # 新しいメールアドレスに更新されること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る

 Scenario: メールアドレス再設定_異常系_バリデーションチェック_秘密の質問回答_不正
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    # 1回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすとあ」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「秘密の質問と回答が正しくありません」を含むこと
    Given 「1」秒待ち
    # 2回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすとい」を入力する
    Given IDが「Email」に「c@c.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「秘密の質問と回答が正しくありません」を含むこと
    Given 「1」秒待ち
    # 3回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすとう」を入力する
    Given IDが「Email」に「d@d.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「秘密の質問と回答が正しくありません」を含むこと
    Given 「1」秒待ち
    # 4回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすとえ」を入力する
    Given IDが「Email」に「e@e.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「秘密の質問と回答が正しくありません」を含むこと
    Given 「1」秒待ち
    # 入力された秘密の質問の答えをSHA-256でハッシュ化した値と秘密の質問回答情報の回答が一致しないの場合 5回
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすとあお」を入力する
    Given IDが「Email」に「e@e.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト開始」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「秘密の質問と回答が正しくありません」を含むこと
    Given 「1」秒待ち
    # 入力された秘密の質問の答えをSHA-256でハッシュ化した値と秘密の質問回答情報の回答が一致してもアカウントロック
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト中」を含むこと
    Given 「1」秒待ち
    # アカウントロック終了期限に変化がないこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # アカウントロック終了期限を過去にしてアカウントロックを解除する
    Given シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する
    | Field             | Value                   |
    | LockoutEndDateUtc | 2016-02-01 00:00:00.000 |
    Given ユーザーDBの「AspNetUsers」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する
    | Id                                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 |
    # アカウントロック解除後はメールアドレス再設定できること
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「ログイン」を含むこと
    # 新しいメールアドレスに更新されること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る

 Scenario: メールアドレス再設定_異常系_バリデーションチェック_秘密の質問回答無し
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    # 1回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Given 「1」秒待ち
    # 2回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「c@c.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Given 「1」秒待ち
    # 3回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすとあう」を入力する
    Given IDが「Email」に「d@d.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Given 「1」秒待ち
    # 4回目
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「e@e.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「秘密の質問の答えが正しくありません。」であること
    Given 「1」秒待ち
    # ユーザーに紐付く秘密の質問回答情報が取得できない場合 5回目アカウントロックされる
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「e@e.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Answer」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト開始」を含むこと
    Given 「1」秒待ち
    # ユーザーに紐付く秘密の質問回答情報が取得できてもアカウントロックアウト
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「この機能は一時的にご利用いただけません。しばらく待ってからやり直してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト中」を含むこと
    Given 「1」秒待ち
    # アカウントロック終了期限に変化がないこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # アカウントロック終了期限を過去にしてアカウントロックを解除する
    Given シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する
    | Field             | Value                   |
    | LockoutEndDateUtc | 2016-02-01 00:00:00.000 |
    Given ユーザーDBの「AspNetUsers」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する
    | Id                                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 |
    # アカウントロック解除後はメールアドレス再設定できること
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「ログイン」を含むこと
    # 新しいメールアドレスに更新されること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る

 Scenario: メールアドレス再設定_異常系_再設定画面_ユーザー情報が取得できない場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aAauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b..c@aa.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ID・パスワードが正しくありません」を含むこと

 Scenario: メールアドレス再設定_異常系_再設定画面_CaradaIdから取得したユーザー情報のEmailConfirmedが0の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b..c@aa.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「利用開始処理を行っていません。ログイン画面よりCARADA IDとパスワードで認証してください。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「利用開始前のユーザーです」を含むこと

 Scenario: メールアドレス再設定_異常系_再設定画面_Emailから取得したユーザー情報が自身のユーザー情報の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「A@A.COM」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「メールアドレスは現在のメールアドレスとは異なる値を入力してください。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「現在のメールアドレスと同じメールアドレスが入力されました」を含むこと

 Scenario: メールアドレス再設定_異常系_再設定画面_Emailから取得したユーザー情報のEmailConfirmedが1の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b8 | b@b.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | bbbuser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then dataValmsgForが「Email」のエラーメッセージは「既に登録済みのメールアドレスです。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「メールアドレスは別のユーザーが使用済みです」を含むこと

 Scenario: メールアドレス再設定_異常系_認証画面_ユーザー情報がない場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b..c@df.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    # 画面表示後にユーザー情報を削除
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

 Scenario: メールアドレス再設定_異常系_認証画面_CaradaIdから取得したユーザー情報のEmailConfirmedが0の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b.@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    # 画面表示後にユーザー情報を更新
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

 Scenario: メールアドレス再設定_異常系_認証画面_認証コード不正
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと    
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    # 1回目
    Given IDが「VerifyCode」に「1」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Given 「1」秒待ち
    # 2回目
    Given IDが「VerifyCode」に「2」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Given 「1」秒待ち
    # 3回目
    Given IDが「VerifyCode」に「3」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Given 「1」秒待ち
    # 4回目
    Given IDが「VerifyCode」に「4」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Given 「1」秒待ち
    # 5回目
    Given IDが「VerifyCode」に「5」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化」を含むこと
    Given 「1」秒待ち
    # 正常な認証コードを入れても認証コード無効
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

 Scenario: メールアドレス再設定_異常系_認証画面_認証コード有効期限切れ
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと    
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    # 認証コード有効期限を更新
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2014-12-10 06:59:58.743 | 0                   |
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと

 Scenario: メールアドレス再設定_異常系_認証画面_Emailから取得したユーザー情報のEmailConfirmedが1の場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと    
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    # 再設定のメールアドレスに認証済みの別ユーザーを追加
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b8 | b@b.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | bbbuser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

 Scenario: メールアドレス再設定_異常系_認証画面_一度戻り、前回発行された認証コードを入力した場合
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと    
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というシナリオ変数へ設定する
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問3」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「メールアドレス再設定認証」を含むこと
    Given IDが「VerifyCode」に「<Current:VerifyCode>」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「POST /UserSettings/EmailResetVerifyCode」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「1」回目の出力に「VerifyCode="<Current:VerifyCode>"」を含むこと
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    