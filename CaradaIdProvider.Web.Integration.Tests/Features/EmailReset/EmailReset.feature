﻿Feature: EmailReset
    メールアドレス再設定の正常系

Scenario: メールアドレス再設定_正常系_未ログイン_再設定認証まで実施
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 4                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 9999-12-31 23:59:59.999 | 5                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # メールアドレス再設定画面表示
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「CARADA ID メールアドレス再設定」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    Then 仮想URLが「/UserSettings/EmailReset」であること
    Then UserInsightの仮想URLが「/UserSettings/EmailReset」であり、他のパラメータも正しく設定されていること
    # メールアドレス再設定画面表示項目確認(全ケース共通なのでここだけで確認)
    Then 内容「CARADA IDとパスワードと秘密の質問への回答を入力することによって、メールアドレスを再設定できます。」を含むこと
    Then タグ名「label」属性名「for」属性値「CaradaId」のテキストが「CARADA ID」であること
    Then nameが「CaradaId」のタグの「placeholder」という属性の値が「CARADA ID」であること
    Then IDが「CaradaId」の入力値は「」であること
    Then タグ名「label」属性名「for」属性値「Password」のテキストが「パスワード」であること
    Then nameが「Password」のタグの「placeholder」という属性の値が「パスワード」であること
    Then IDが「Password」の入力値は「」であること
    Then タグ名「label」属性名「for」属性値「SecurityQuestionSelectId」のテキストが「秘密の質問」であること
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Then タグ名「label」属性名「for」属性値「Answer」のテキストが「答え」であること
    Then IDが「SecurityQuestionSelectId」の入力値は「3」であること
    Then nameが「Answer」のタグの「placeholder」という属性の値が「全角ひらがな50文字以内で入力してください。」であること
    Then IDが「Answer」の入力値は「」であること
    Then タグ名「label」属性名「for」属性値「Email」のテキストが「新しいメールアドレス」であること
    Then nameが「Email」のタグの「placeholder」という属性の値が「新しいメールアドレス」であること
    Then IDが「Email」の入力値は「」であること
    Then 内容「ドメイン指定受信をご利用の方は、「@carada.jp」 のドメイン解除設定をお願いいたします。」を含むこと
    # 入力
    Given IDが「CaradaId」に「EmailReset」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問1」を選択する
    Given IDが「Answer」に「てすと」を入力する
    # RFC違反だがキャリアメールで許可されているメアド(..連続)
    Given IDが「Email」に「a..b@a.com」を入力する
    # 「再設定する」ボタン押下→メールアドレス再設定認証画面表示
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証コード確認」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    Then 仮想URLが「/UserSettings/EmailResetVerifyCode」であること
    Then UserInsightの仮想URLが「/UserSettings/EmailResetVerifyCode」であり、他のパラメータも正しく設定されていること
    # メールアドレス再設定認証画面表示項目確認(全ケース共通なのでここだけで確認)
    Then 内容「ご入力いただいたメールアドレス宛に認証コードを送信しました。」を含むこと
    Then 内容「届いたメールに記載された認証コードを下記へ入力してください。」を含むこと
    Then タグ名「label」属性名「for」属性値「VerifyCode」のテキストが「認証コード」であること
    Then nameが「VerifyCode」のタグの「placeholder」という属性の値が「認証コード」であること
    Then IDが「VerifyCode」の入力値は「」であること
    # ログイン失敗回数が初期化されること。認証コード失敗回数が初期化されること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 0                   |
    # SecurityStampが更新されること。認証コード有効期限が初期化されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 認証コード案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    Then 最後に送信されたメールタイトルが「【CARADA ID】メールアドレス再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a..b@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはメールアドレス再設定用の認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # ログ確認
    # メールアドレス再設定認証画面が呼ばれたこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「POST /UserSettings/EmailReset」を含むこと	
    # パラメータ(マスク)チェック
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「CaradaId="EmailReset", Password="*****", SecurityQuestionSelectId="1", Answer="*****", Email="*****"」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「DBに登録されていないユーザーへの送信が要求されました。件名:【CARADA ID】メールアドレス再設定のご案内」を含むこと
    # 認証コード失敗回数初期化が見たいので一度失敗する(認証コード失敗回数が1に増える)
    Given IDが「VerifyCode」に「1」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 1                   |
    # メールアドレス再設定認証画面で入力
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    # メアドが更新されること。認証コード失敗回数が初期化されること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email      | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a..b@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 0                   |
    # 更新日時とSecurityStampが更新されること。認証コード有効期限が初期化されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 二段階認証Cookieが保持されること
    Then Cookie名が「TwoFactorLogin」のCookieが存在すること
    # ストレージに、二段階認証Cookieの値をキーとして、ユーザーIDが登録されること
    Given Cookie名「TwoFactorLogin」のCookieの値をCurrentのLastCookieValueとして設定する
    Then ストレージの「TwoFactorLogin」に次のデータが存在すること
    | PartitionKey              | RowKey | UserId                               |
    | <Current:LastCookieValue> | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    Given ストレージの「TwoFactorLogin」テーブルのダンプを取る
    # ログ確認
    # メールアドレス再設定認証画面が呼ばれたこと
    Then 「Access」ログのログレベル「INFO」の最後から「8」回目の出力に「POST /UserSettings/EmailResetVerifyCode」を含むこと	
    # パラメータチェック
    Then 「Access」ログのログレベル「INFO」の最後から「8」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「8」回目の出力に「VerifyCode="<Current:VerifyCode>"」を含むこと    
    Then 「Access」ログのログレベル「INFO」の最後から「7」回目の出力に「二段階認証完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489, CARADA ID=EmailReset)」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「6」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08489へのメール送信を開始します。件名:【CARADA ID】メールアドレス再設定完了のお知らせ, 利用開始登録済み:True」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「DBに登録されていないユーザーへの送信が要求されました。件名:【CARADA ID】メールアドレス再設定完了のお知らせ」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「メールアドレス再設定完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489, CARADA ID=EmailReset)」を含むこと	
    # メールアドレス再設定完了案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    # 新メアド
    Then 最後に送信された「2」件のメールのどれかのあて先が「a..b@a.com」の組でありタイトルが「【CARADA ID】メールアドレス再設定完了のお知らせ」であること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a..b@a.com」の組であり本文に「メールアドレスの再設定が完了いたしました。」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a..b@a.com」の組であり本文に「EmailReset」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a..b@a.com」の組であり本文に「a..b@a.com」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a..b@a.com」の組であり本文にお問い合わせ先文言が含まれること
    # 旧メアド
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組でありタイトルが「【CARADA ID】メールアドレス再設定完了のお知らせ」であること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「メールアドレスの再設定が完了いたしました。」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「EmailReset」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「a..b@a.com」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文にお問い合わせ先文言が含まれること

Scenario: メールアドレス再設定_正常系_ログイン中_再設定認証まで実施
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「EmailReset」、パスワード「0000000a」のアカウントでログインする
    # アカウント管理画面表示
    Then 内容「CARADA ID アカウント管理」を含むこと
    # メールアドレス再設定画面表示
    Given 「/UserSettings/EmailResetView」にブラウザでアクセスする
    Then 内容「CARADA ID メールアドレス再設定」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 入力
    Given IDが「CaradaId」に「EmailReset」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問1」を選択する
    Given IDが「Answer」に「てすと」を入力する
    # RFC違反だがキャリアメールで許可されているメアド(.@)
    Given IDが「Email」に「a.@a.com」を入力する
    # 「再設定する」ボタン押下→メールアドレス再設定認証画面表示
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証コード確認」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # ログイン失敗回数が初期化されること。認証コード失敗回数が初期化されること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 0                   |
    # SecurityStampが更新されること。認証コード有効期限が初期化されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 認証コード案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    Then 最後に送信されたメールタイトルが「【CARADA ID】メールアドレス再設定のご案内」であること
    Then 最後に送信されたメールのあて先が「a.@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはメールアドレス再設定用の認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # ログ確認
    # メールアドレス再設定認証画面が呼ばれたこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「POST /UserSettings/EmailReset」を含むこと	
    # パラメータ(マスク)チェック
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「CaradaId="EmailReset", Password="*****", SecurityQuestionSelectId="1", Answer="*****", Email="*****"」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「DBに登録されていないユーザーへの送信が要求されました。件名:【CARADA ID】メールアドレス再設定のご案内」を含むこと	
    # メールアドレス再設定認証画面で入力
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    # アカウント管理画面表示(ログイン中なので)
    Then 内容「CARADA ID アカウント管理」を含むこと
    # メアドが更新されること。認証コード失敗回数が初期化されること。
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email    | EmailConfirmed | PasswordHash                                                         | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a.@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 0                   |
    # 更新日時とSecurityStampが更新されること。認証コード有効期限が初期化されること。
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    # 二段階認証Cookieが保持されること
    Then Cookie名が「TwoFactorLogin」のCookieが存在すること
    # ストレージに、二段階認証Cookieの値をキーとして、ユーザーIDが登録されること
    Given Cookie名「TwoFactorLogin」のCookieの値をCurrentのLastCookieValueとして設定する
    Then ストレージの「TwoFactorLogin」に次のデータが存在すること
    | PartitionKey              | RowKey | UserId                               |
    | <Current:LastCookieValue> | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    Given ストレージの「TwoFactorLogin」テーブルのダンプを取る
    # ログ確認
    # メールアドレス再設定認証画面が呼ばれたこと
    Then 「Access」ログのログレベル「INFO」の最後から「10」回目の出力に「POST /UserSettings/EmailResetVerifyCode」を含むこと	
    # パラメータチェック
    Then 「Access」ログのログレベル「INFO」の最後から「10」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「10」回目の出力に「VerifyCode="<Current:VerifyCode>"」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「9」回目の出力に「二段階認証完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489, CARADA ID=EmailReset)」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「メールアドレス再設定完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a08489, CARADA ID=EmailReset)」を含むこと	
    # メールアドレス再設定完了案内メールの確認(文面が変わるとここのチェックも変更になるので注意)
    # 新メアド
    Then 最後に送信された「2」件のメールのどれかのあて先が「a.@a.com」の組でありタイトルが「【CARADA ID】メールアドレス再設定完了のお知らせ」であること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a.@a.com」の組であり本文に「メールアドレスの再設定が完了いたしました。」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a.@a.com」の組であり本文に「EmailReset」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a.@a.com」の組であり本文に「a.@a.com」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a.@a.com」の組であり本文にお問い合わせ先文言が含まれること
    # 旧メアド
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組でありタイトルが「【CARADA ID】メールアドレス再設定完了のお知らせ」であること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「メールアドレスの再設定が完了いたしました。」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「EmailReset」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文に「a.@a.com」の正規表現がマッチすること
    Then 最後に送信された「2」件のメールのどれかのあて先が「a@a.com」の組であり本文にお問い合わせ先文言が含まれること

Scenario: メールアドレス再設定_正常系_未ログイン_画面表示_戻る_ログイン実施
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # メールアドレス再設定画面表示
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「CARADA ID メールアドレス再設定」を含むこと
    # レイアウト確認
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性の値が「off」であること
    # 「戻る」ボタン押下
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    # ログイン画面からログイン実施
    Given ID「EmailReset」、パスワード「0000000a」のアカウントでログインする
    # 画面表示項目確認
    Then 内容「CARADA ID アカウント管理」を含むこと

Scenario: メールアドレス再設定認証_正常系_戻る_再設定画面表示確認
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName   | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 4                 | EmailReset | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 9999-12-31 23:59:59.999 | 5                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # ログイン画面表示
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    # メールアドレス再設定画面表示
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「CARADA ID メールアドレス再設定」を含むこと
    # 入力
    Given IDが「CaradaId」に「EmailReset」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問1」を選択する
    Given IDが「Answer」に「てすと」を入力する
    # 普通のメアド
    Given IDが「Email」に「b@b.com」を入力する
    # 「再設定する」ボタン押下→メールアドレス再設定認証画面表示
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証コード確認」を含むこと
    # 「戻る」ボタン押下→メールアドレス再設定画面表示
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID メールアドレス再設定」を含むこと
    # メールアドレス再設定画面入力値確認
    Then IDが「CaradaId」の入力値は「EmailReset」であること
    Then IDが「Password」の入力値は「」であること
    Then IDが「SecurityQuestionSelectId」のセレクトボックスの順番が「テスト質問3,テスト質問2,テスト質問1」通りであること
    Then IDが「SecurityQuestionSelectId」の入力値は「3」であること
    Then IDが「Answer」の入力値は「」であること
    Then IDが「Email」の入力値は「b@b.com」であること
