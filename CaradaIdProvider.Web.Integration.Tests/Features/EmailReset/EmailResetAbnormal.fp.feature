﻿Feature: EmailResetAbnormal.fp
    メールアドレス再設定の異常系(FP版)

Scenario: メールアドレス再設定_FP版_異常系_未ログイン_エラー表示
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 P903i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ再設定」を含むこと
    Then レイアウトがFP用であること
    # CaradaID未入力
    # パスワード未入力
    # 秘密質問未入力(無し)
    # 答え未入力
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ再設定」を含むこと
    Then レイアウトがFP用であること
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDを入力してください。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードを入力してください。」であること
    Then dataValmsgForが「SecurityQuestionSelectId」のエラーメッセージは「秘密の質問を入力してください。」であること
    Then dataValmsgForが「Answer」のエラーメッセージは「答えを入力してください。」であること

Scenario: メールアドレス再設定_FP版_異常系_ログイン中_エラー_入力復元あり
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaauser  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    | 2                  | テスト質問2   | 2            |
    | 3                  | テスト質問3   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 3                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given ID「aaauser」、パスワード「0000000a」のアカウントでFP版にログインする
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと 
    Then レイアウトがFP用であること
    Given 「/UserSettings/EmailResetView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ再設定」を含むこと
    Then レイアウトがFP用であること
    Given IDが「CaradaId」に「aaauser」を入力する
    Given IDが「Password」に「0000000b」を入力する
    Given IDが「SecurityQuestionSelectId」の「テスト質問2」を選択する
    Given IDが「Answer」に「てすと」を入力する
    Given IDが「Email」に「b@b.com」を入力する
    When 「name」という属性の値が「Reset」のボタンをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ再設定」を含むこと
    Then レイアウトがFP用であること
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then IDが「CaradaId」の入力値は「aaauser」であること
    Then IDが「Password」の入力値は「0000000b」であること
    Then IDが「SecurityQuestionSelectId」の入力値は「2」であること
    Then IDが「Answer」の入力値は「てすと」であること
    Then IDが「Email」の入力値は「b@b.com」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ID・パスワードが正しくありません」を含むこと
    