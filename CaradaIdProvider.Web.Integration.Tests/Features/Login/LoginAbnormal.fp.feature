﻿Feature: LoginAbnormal.fp
    ログイン画面の異常系(FP版)

Scenario: ログイン画面_FP版_異常系_バリデーションチェック
# 網羅はUTで行うため、バリデーション表示の確認のみ行う。
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    # キャリアをdocomoで確認
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then IDが「CaradaId」の入力値は「」であること
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDを入力してください。」であること
    Then IDが「Password」の入力値は「」であること
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードを入力してください。」であること
    #BlockUIで1秒アクセスできないので1秒待っている
    Given 「1」秒待ち
    Given ID「a」、パスワード「p」のアカウントでFP版にログインする
    Then IDが「CaradaId」の入力値は「a」であること
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then IDが「Password」の入力値は「p」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「a@.com」、パスワード「pass」のアカウントでFP版にログインする
    Then IDが「CaradaId」の入力値は「a@.com」であること
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then IDが「Password」の入力値は「pass」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
