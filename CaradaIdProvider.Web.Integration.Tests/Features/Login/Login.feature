﻿Feature: Login
    ログイン画面の正常系

Scenario: ログイン画面_正常系_未ログイン_二段階認証済_アカウント管理画面遷移
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | 質問の答え |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08489 |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240448ea2a799f7a1512ab5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    # アナリティクス関連確認
    Then 仮想URLが「/User/Login」であること
    Then GoogleAnalyticsのフィールド「trackingId」が「UA-49128370-9」であること
    Then GoogleAnalyticsのフィールド「dimension1」が「未ログイン」であること
    Then GoogleAnalyticsのフィールド「dimension4」が「Web」であること
    Then GoogleAnalyticsのフィールド「dimension6」が「PC」であること
    Then UserInsightの仮想URLが「/User/Login」であり、他のパラメータも正しく設定されていること
    Then 内容「CARADA ID ログイン」を含むこと
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「アカウント管理」を含むこと
    # アナリティクス関連確認
    Then 仮想URLが「/UserSettings/Index」であること
    Then GoogleAnalyticsのフィールド「trackingId」が「UA-49128370-9」であること
    Then GoogleAnalyticsのフィールド「dimension1」が「ログイン」であること
    Then GoogleAnalyticsのフィールド「dimension4」が「Web」であること
    Then GoogleAnalyticsのフィールド「dimension6」が「PC」であること
    Then UserInsightの仮想URLが「/UserSettings/Index」であり、他のパラメータも正しく設定されていること
    Then Cookie名が「CaradaIdLogin」の有効期限が設定されていないCookieが存在すること
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「CaradaId="aaa.com", Password="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「ログアウト」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ログイン成功」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る

Scenario: ログイン画面_正常系_未ログイン_初回ログイン画面_利用開始画面遷移
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email  | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0848a | <NULL> | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then レイアウトがデフォルトであること
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「利用開始」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaId="aaa.com", Password="*****",」を含むこと

Scenario: ログイン画面_正常系_二段階認証前_二段階認証画面遷移
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08479 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証コード確認」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「CaradaId="aaa.com", Password="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「ログアウト」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a08479へのメール送信を開始します。件名:【CARADA ID】認証コードのご案内, 利用開始登録済み:True」を含むこと

Scenario: ログイン画面_正常系_未ログイン_二段階認証Cookie失効_二段階認証画面遷移
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08479 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a08479 |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240448ea2a799f7a1512ab5」有効期限「2000-12-31 11:11:11.111」のCookieを追加する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証コード確認」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「CaradaId="aaa.com", Password="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「ログアウト」を含むこと

Scenario: ログイン画面_正常系_未ログイン_二段階認証Cookieユーザ不一致_二段階認証画面遷移
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08471 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a0847a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240448ea2a799f7a1512ab5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証コード確認」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「CaradaId="aaa.com", Password="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「ログアウト」を含むこと

Scenario: ログイン画面_正常系_ログイン済_二段階認証済Cookieなし_アカウント管理画面遷移
#一度ログインさせてから二段階認証Cookieを削除して検証する
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0847a | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0847a | 1                  | 質問の答え |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab5 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a0847a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240448ea2a799f7a1512ab5」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    Given IDが「IsKeepLoginLabel」のチェックボックスをONにする
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「アカウント管理」を含むこと
    Then Cookie名が「CaradaIdLogin」のCookieが存在すること
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    #確認したいのはここから先
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「アカウント管理」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「7」回目の出力に「CaradaId="aaa.com", Password="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「6」回目の出力に「ログアウト」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「ログイン成功」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ログイン済」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る

Scenario: ログイン画面_正常系_パスワード忘れ画面に遷移できること
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「パスワード再設定」を含むこと

Scenario: ログイン画面_正常系_ID連絡の画面に遷移できること
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「ID連絡」を含むこと

Scenario: ログイン画面_正常系_メールアドレス再設定の画面に遷移できること
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「メールアドレス再設定」を含むこと

Scenario: ログイン画面_正常系_アカウントロックアウトがされること
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0848d | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「aaa.com」、パスワード「a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「ログイン失敗」を含むこと
    Given ID「aaa.com」、パスワード「a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Given ID「aaa.com」、パスワード「a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Given ID「aaa.com」、パスワード「a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    #ロックアウトされる
    Given ID「aaa.com」、パスワード「a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「このCARADA IDは一時的にご利用いただけません。しばらく待ってからログインしてください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト開始」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    #ロックアウト中に正しいIDとパスワードでもロックアウトされていること
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「このCARADA IDは一時的にご利用いただけません。しばらく待ってからログインしてください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「アカウントロックアウト中」を含むこと

Scenario: ログイン画面_正常系_ロックアウト後にログイン可能_アカウント管理
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    #ロックアウト終了時刻を直前にしてテストする。
    Given 現在時刻から「-1」分後の時間をCurrentDataにEpochとして設定する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
     | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
     | 4d74efe8-7c62-496e-97cf-3df1a9a0848b | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <Current:Epoch>   | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0848b | 1                  | 質問の答え |
    # 二段階認証済とするためストレージエミュレータに登録
    Given ストレージの「TwoFactorLogin」テーブルを初期化する
    Given ストレージの「TwoFactorLogin」テーブルへ次のテストデータを投入する
    | PartitionKey                     | RowKey | UserId                               |
    | 86357256a240448ea2a799f7a1512ab6 | -      | 4d74efe8-7c62-496e-97cf-3df1a9a0848b |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given 「/」に移動しつつ、Cookie名「TwoFactorLogin」値「86357256a240448ea2a799f7a1512ab6」有効期限「2099-12-31 11:11:11.111」のCookieを追加する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「アカウント管理」を含むこと
    Then Cookie名が「CaradaIdLogin」の有効期限が設定されていないCookieが存在すること
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「CaradaId="aaa.com", Password="*****",」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「3」回目の出力に「ログアウト」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ログイン成功」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る

Scenario: ログイン画面_正常系_ログイン状態保持してログイン済み確認ができること
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0848c | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0848c | 1                  | 質問の答え |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    # チェックボックスはDisplayed=falseなのでClick()できないので、ラベルを押下してチェックしたことにしている
    Given IDが「IsKeepLoginLabel」のチェックボックスをONにする
    Given ID「aaa.com」、パスワード「0000000a」のアカウントでログインする
    Then 内容「アカウント管理」を含むこと 
    Then Cookie名「CaradaIdLogin」の有効期限が現在から「65」日に設定されていること
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「アカウント管理」を含むこと
    Then Cookie名「CaradaIdLogin」の有効期限が現在から「65」日に設定されていること
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ログイン済」を含むこと
