﻿Feature: LoginAbnormal
    ログイン画面の異常系

Scenario: ログイン画面_異常系_バリデーションチェック
#網羅はUTで行うため、バリデーション表示の確認のみ行う。
    #その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given 「/User/LoginView」にブラウザでアクセスする
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDを入力してください。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「パスワードを入力してください。」であること
    #BlockUIで1秒アクセスできないので1秒待っている
    Given 「1」秒待ち
    Given ID「a」、パスワード「p」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「a@.com」、パスワード「pass」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること

Scenario: ログイン画面_異常系_CARADA_ID_大文字小文字判定_小文字で登録時
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | test123  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    #その他のテーブルはカスケードで削除される
    Given 「/User/LoginView」にブラウザでアクセスする
    Given ID「Test123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    #BlockUIで1秒アクセスできないので1秒待っている
    Given 「1」秒待ち
    Given ID「tEst123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「teSt123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「tesT123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「TEST123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること

Scenario: ログイン画面_異常系_CARADA_ID_大文字小文字判定_大文字で登録時
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df2a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | TEST123  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    #その他のテーブルはカスケードで削除される
    Given 「/User/LoginView」にブラウザでアクセスする
    Given ID「Test123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    #BlockUIで1秒アクセスできないので1秒待っている
    Given 「1」秒待ち
    Given ID「tEst123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「teSt123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「tesT123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること
    Given 「1」秒待ち
    Given ID「test123」、パスワード「0000000a」のアカウントでログインする
    Then dataValmsgForが「CaradaId」のエラーメッセージは「CARADA IDまたはパスワードに誤りがあります。」であること
    Then dataValmsgForが「Password」のエラーメッセージは「」であること

#Scenario: ログイン画面_異常系_DB接続エラー_エラー画面遷移
##手動でID:a@a.com パスワード00000000a のユーザを登録後、DBを停止させてテストすること
#    Given 「/User/LoginView」にブラウザでアクセスする
#    Given ID「a@a.com」、パスワード「0000000a」のアカウントでログインする
#    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
#    # ログチェック
#    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと
##手動でDBを再開すること
