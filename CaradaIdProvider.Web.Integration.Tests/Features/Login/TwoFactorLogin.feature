﻿Feature: TwoFactorLogin
    二段階認証の正常系
    ※ログイン画面と一部内容は重複している。

Scenario: 二段階認証画面_正常系_二段階認証Cookieなし_画面表示後認証しアカウント管理画面遷移まで
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 1                  | 質問の答え |
    Given 「/User/LoginView」にブラウザでアクセスする
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Then formの「autocomplete」属性の値が「off」であること
    Then IDが「advice1」のInnerTextに「ドメイン指定受信をご利用の方は、「@carada.jp」のドメイン解除設定をお願いいたします。」を含むこと
    Then IDが「advice2」のInnerTextに「最新のメールに記載された認証コードのみが有効となりますのでご注意ください。」を含むこと
    Then Cookie名が「CaradadLogin」のCookieが存在しないこと
    Then 仮想URLが「/User/TwoFactorLogin」であること
    Then UserInsightの仮想URLが「/User/TwoFactorLogin」であり、他のパラメータも正しく設定されていること
    Then 最後に送信されたメールタイトルが「【CARADA ID】認証コードのご案内」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA IDログインの認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「UserId:4d74efe8-7c62-496e-97cf-3df1a9a084b9へのメール送信を開始します。件名:【CARADA ID】認証コードのご案内, 利用開始登録済み:True」を含むこと
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「アカウント管理」を含むこと
    Then 仮想URLが「/UserSettings/Index」であること
    Then UserInsightの仮想URLが「/UserSettings/Index」であり、他のパラメータも正しく設定されていること
    Then IDが「CaradaId」のInnerTextに「aaa.com」を含むこと
    Then Cookie名が「CaradaIdLogin」のCookieが存在すること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    # 二段階認証の列がクリアされていること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | <NULL>            | True           | 0                 | <NULL>                | 0                   |
    Given Cookie名「TwoFactorLogin」のCookieの値をCurrentのLastCookieValueとして設定する
    Then ストレージの「TwoFactorLogin」に次のデータが存在すること
    | PartitionKey              | RowKey | UserId                               |
    | <Current:LastCookieValue> | -      | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 |
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「<Current:VerifyCode>」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「二段階認証完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a084b9, CARADA ID=aaa.com)」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ストレージの「TwoFactorLogin」テーブルのダンプを取る

Scenario: 二段階認証画面_正常系_認証5回失敗_エラー表示後再発行_アカウント管理画面遷移
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | 2099-12-10 06:59:58.743 | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | 1                  | 質問の答え |
    Given IDが「VerifyCode」に「000000」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | <NULL>            | True           | 0                 | 2099-12-10 06:59:58.743 | 1                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「000001」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | <NULL>            | True           | 0                 | 2099-12-10 06:59:58.743 | 2                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「000000」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | <NULL>            | True           | 0                 | 2099-12-10 06:59:58.743 | 3                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「000001」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「入力された認証コードが一致しません。」であること
    Then 「Access」ログのログレベル「WARN」の最後から「1」回目の出力に「認証コード入力失敗」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | <NULL>            | True           | 0                 | 2099-12-10 06:59:58.743 | 4                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「000000」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | <NULL>            | True           | 0                 | 2099-12-10 06:59:58.743 | 5                   |
    Given 「1」秒待ち
    Given IDが「VerifyCode」に「000001」を入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Then dataValmsgForが「VerifyCode」のエラーメッセージは「認証コードが無効です。もう一度認証コードを取得してください。」であること
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「認証コード無効化済」を含むこと
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc   | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | <NULL>            | True           | 0                 | 2099-12-10 06:59:58.743 | 5                   |

    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ログイン」を含むこと
    Then Cookie名が「CaradaIdLogin」のCookieが存在しないこと
    Then Cookie名が「TwoFactorLogin」のCookieが存在しないこと
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「認証コード」を含むこと
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「アカウント管理」を含むこと
    Then Cookie名が「CaradaIdLogin」のCookieが存在すること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    # 二段階認証の列がクリアされていること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a083c9 | a@a.com | 1              | <NULL>            | True           | 0                 | <NULL>                | 0                   |
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「二段階認証完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a083c9, CARADA ID=aaa.com)」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ストレージの「TwoFactorLogin」テーブルのダンプを取る
    Then Cookie名が「TwoFactorLogin」のCookieが存在すること
