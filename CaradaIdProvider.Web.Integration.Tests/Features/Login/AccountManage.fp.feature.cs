﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace CaradaIdProvider.Web.Integration.Tests.Features.Login
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute()]
    public partial class AccountManage_FpFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "AccountManage.fp.feature"
#line hidden
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitializeAttribute()]
        public static void FeatureSetup(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext testContext)
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "AccountManage.fp", "  アカウント管理画面の正常系(FP版)", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassCleanupAttribute()]
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitializeAttribute()]
        public virtual void TestInitialize()
        {
            if (((TechTalk.SpecFlow.FeatureContext.Current != null) 
                        && (TechTalk.SpecFlow.FeatureContext.Current.FeatureInfo.Title != "AccountManage.fp")))
            {
                CaradaIdProvider.Web.Integration.Tests.Features.Login.AccountManage_FpFeature.FeatureSetup(null);
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCleanupAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("アカウント管理_FP版_正常系_画面が表示されること")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "AccountManage.fp")]
        public virtual void アカウント管理_FP版_正常系_画面が表示されること()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("アカウント管理_FP版_正常系_画面が表示されること", ((string[])(null)));
#line 4
this.ScenarioSetup(scenarioInfo);
#line 5
    testRunner.Given("HTTPヘッダーに「User-Agent=KDDI-HI3D UP.Browser/6.2_7.2.7.1.K.2.234 (GUI) MMP/2.0 」を設定す" +
                    "る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 7
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table1.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "a@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "AccountManage",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 8
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table1, "Given ");
#line 11
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table2.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "1"});
#line 12
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table2, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "UserId",
                        "SecurityQuestionId",
                        "Answer"});
            table3.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "1",
                        "e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a"});
#line 16
    testRunner.Given("ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する", ((string)(null)), table3, "Given ");
#line 19
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 20
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 21
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 22
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 23
    testRunner.Given("ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 25
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 27
    testRunner.Then("内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 28
    testRunner.Then("内容「AccountManage」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 29
    testRunner.Then("内容「a@a.com」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 30
    testRunner.Then("内容「テスト質問1」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("アカウント管理_FP版_正常系_メールアドレス変更ボタン押下")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "AccountManage.fp")]
        public virtual void アカウント管理_FP版_正常系_メールアドレス変更ボタン押下()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("アカウント管理_FP版_正常系_メールアドレス変更ボタン押下", ((string[])(null)));
#line 32
this.ScenarioSetup(scenarioInfo);
#line 33
    testRunner.Given("HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH06A3(c500;TC;W30H18)  」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 35
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table4.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "a@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "AccountManage",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 36
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table4, "Given ");
#line 39
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table5 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table5.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "1"});
#line 40
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table5, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table6 = new TechTalk.SpecFlow.Table(new string[] {
                        "UserId",
                        "SecurityQuestionId",
                        "Answer"});
            table6.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "1",
                        "e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a"});
#line 44
    testRunner.Given("ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する", ((string)(null)), table6, "Given ");
#line 47
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 48
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 49
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 50
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 51
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 52
    testRunner.Given("ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 54
    testRunner.Then("内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 56
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 58
    testRunner.When("IDが「EmailChangeView」のリンクをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 59
    testRunner.Then("内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 61
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("アカウント管理_FP版_正常系_パスワード変更ボタン押下")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "AccountManage.fp")]
        public virtual void アカウント管理_FP版_正常系_パスワード変更ボタン押下()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("アカウント管理_FP版_正常系_パスワード変更ボタン押下", ((string[])(null)));
#line 63
this.ScenarioSetup(scenarioInfo);
#line 64
    testRunner.Given("HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront" +
                    "/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 66
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table7 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table7.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "a@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "AccountManage",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 67
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table7, "Given ");
#line 70
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table8 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table8.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "1"});
#line 71
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table8, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table9 = new TechTalk.SpecFlow.Table(new string[] {
                        "UserId",
                        "SecurityQuestionId",
                        "Answer"});
            table9.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "1",
                        "e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a"});
#line 75
    testRunner.Given("ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する", ((string)(null)), table9, "Given ");
#line 78
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 79
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 80
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 81
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 82
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 83
    testRunner.Given("ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 85
    testRunner.Then("内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 87
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 89
    testRunner.When("IDが「PasswordChangeView」のリンクをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 90
    testRunner.Then("内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ変更」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 92
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("アカウント管理_FP版_正常系_秘密の質問変更ボタン押下")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "AccountManage.fp")]
        public virtual void アカウント管理_FP版_正常系_秘密の質問変更ボタン押下()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("アカウント管理_FP版_正常系_秘密の質問変更ボタン押下", ((string[])(null)));
#line 94
this.ScenarioSetup(scenarioInfo);
#line 95
    testRunner.Given("HTTPヘッダーに「User-Agent=KDDI-HI3D UP.Browser/6.2_7.2.7.1.K.2.234 (GUI) MMP/2.0」を設定する" +
                    "", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 97
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table10 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table10.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "a@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "AccountManage",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 98
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table10, "Given ");
#line 101
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table11 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table11.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "1"});
#line 102
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table11, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table12 = new TechTalk.SpecFlow.Table(new string[] {
                        "UserId",
                        "SecurityQuestionId",
                        "Answer"});
            table12.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08489",
                        "1",
                        "e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a"});
#line 106
    testRunner.Given("ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する", ((string)(null)), table12, "Given ");
#line 109
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 110
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 111
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 112
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 113
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 114
    testRunner.Given("ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 116
    testRunner.Then("内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 118
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 120
    testRunner.When("IDが「SecurityQuestionChangeView」のリンクをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 121
    testRunner.Then("内容「CARADA ID 秘密の質問変更」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 123
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("アカウント管理_FP版_正常系_ログアウトリンク押下")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "AccountManage.fp")]
        public virtual void アカウント管理_FP版_正常系_ログアウトリンク押下()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("アカウント管理_FP版_正常系_ログアウトリンク押下", ((string[])(null)));
#line 125
this.ScenarioSetup(scenarioInfo);
#line 126
    testRunner.Given("HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH06A3(c500;TC;W30H18)」を設定する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 128
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table13 = new TechTalk.SpecFlow.Table(new string[] {
                        "Id",
                        "Email",
                        "EmailConfirmed",
                        "PasswordHash",
                        "SecurityStamp",
                        "PhoneNumber",
                        "PhoneNumberConfirmed",
                        "TwoFactorEnabled",
                        "LockoutEndDateUtc",
                        "LockoutEnabled",
                        "AccessFailedCount",
                        "UserName",
                        "EmployeeFlag",
                        "CreateDateUtc",
                        "UseStartDateUtc",
                        "UpdateDateUtc",
                        "AuthCodeExpireDateUtc",
                        "AuthCodeFailedCount"});
            table13.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08490",
                        "a@a.com",
                        "1",
                        "AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ==",
                        "82e130c2-9623-4f83-b8ba-370a9c5a5fb4",
                        "<NULL>",
                        "0",
                        "1",
                        "<NULL>",
                        "1",
                        "0",
                        "AccountManage",
                        "1",
                        "2014-12-10 06:59:58.743",
                        "2014-12-10 06:59:58.743",
                        "<NULL>",
                        "<NULL>",
                        "0"});
#line 129
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する", ((string)(null)), table13, "Given ");
#line 132
    testRunner.Given("IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line hidden
            TechTalk.SpecFlow.Table table14 = new TechTalk.SpecFlow.Table(new string[] {
                        "SecurityQuestionId",
                        "Question",
                        "DisplayOrder"});
            table14.AddRow(new string[] {
                        "1",
                        "テスト質問1",
                        "1"});
#line 133
    testRunner.Given("IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する", ((string)(null)), table14, "Given ");
#line hidden
            TechTalk.SpecFlow.Table table15 = new TechTalk.SpecFlow.Table(new string[] {
                        "UserId",
                        "SecurityQuestionId",
                        "Answer"});
            table15.AddRow(new string[] {
                        "4d74efe8-7c62-496e-97cf-3df1a9a08490",
                        "1",
                        "e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a"});
#line 137
    testRunner.Given("ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する", ((string)(null)), table15, "Given ");
#line 140
    testRunner.Given("Cookie名「CaradaIdLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 141
    testRunner.Given("Cookie名「TwoFactorLogin」のCookieを削除する", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 143
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 144
    testRunner.Given("「/User/LoginView」にブラウザでアクセスする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 145
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 146
    testRunner.Given("ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 148
    testRunner.Then("内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 150
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 152
    testRunner.When("IDが「Logout」のリンクをクリックしたとき", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 153
    testRunner.Then("内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 155
    testRunner.Then("レイアウトがFP用であること", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 157
    testRunner.Given("ユーザーDBの「AspNetUsers」テーブルのダンプを取る", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 158
    testRunner.Then("「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ログアウト」を含むこと", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
