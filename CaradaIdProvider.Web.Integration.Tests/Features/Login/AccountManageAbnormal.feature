﻿Feature: AccountManageAbnormal
    アカウント管理画面の異常系

Scenario: アカウント管理_異常系_ユーザー情報なし
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでログインする
    # 画面表示項目確認(ユーザー情報があればアカウント管理が表示されることを念の為確認)
    Then 内容「CARADA ID アカウント管理」を含むこと
    # ユーザー情報なし
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    # アカウント管理画面にアクセス
    Given 「/UserSettings/Index」にブラウザでアクセスする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: アカウント管理_異常系_ユーザーのEmailConfirmedが0
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでログインする
    # 画面表示項目確認
    Then 内容「CARADA ID アカウント管理」を含むこと
    # EmailConfirmedを0に変更する
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    # カスケードで削除されるので秘密の質問の回答も再投入(マスタは削除されないので再投入なし)
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    # アカウント管理画面にアクセス
    Given 「/UserSettings/Index」にブラウザでアクセスする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: アカウント管理_異常系_秘密の質問マスタが取得できない
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでログインする
    # 画面表示項目確認(秘密の質問マスタがあればアカウント管理が表示されることを念の為確認)
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問マスタなし
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルを初期化する
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    # アカウント管理画面にアクセス
    Given 「/UserSettings/Index」にブラウザでアクセスする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: アカウント管理_異常系_秘密の質問の回答が取得できない
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでログインする
    # 画面表示項目確認(秘密の質問の回答があればアカウント管理が表示されることを念の為確認)
    Then 内容「CARADA ID アカウント管理」を含むこと
    # 秘密の質問の回答なし
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルを初期化する
    # アカウント管理画面にアクセス
    Given 「/UserSettings/Index」にブラウザでアクセスする
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと

Scenario: アカウント管理_異常系_秘密の質問の回答が2件以上
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    | 2                  | テスト質問2   | 2            |
    # 秘密の質問の回答を2件登録
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 2                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionMasters」テーブルのダンプを取る
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ログイン」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでログインする
    Then 内容「申し訳ございません。何らかの理由でページを正しく表示できませんでした。」を含むこと
    # ログチェック
    Then 「Error」ログのログレベル「ERROR」の最後から「1」回目の出力に「サーバエラーが発生しました。」を含むこと
