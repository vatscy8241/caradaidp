﻿Feature: TwoFactorLogin.fp
    二段階認証の正常系(FP版)

Scenario: 二段階認証画面_FP版_正常系_二段階認証Cookieなし_画面表示後認証しアカウント管理画面遷移まで
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 1                  | 質問の答え |
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 P903i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証ｺｰﾄﾞ確認」を含むこと
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性の値が「off」であること
    # 改行を含む文言は内容「XXX」を含むこと、ではうまく検証出来ないので1行ずつ検証する
    Then 内容「ご入力いただいたﾒｰﾙｱﾄﾞﾚｽ宛に認証ｺｰﾄﾞを」を含むこと
    Then 内容「送信しました｡」を含むこと
    Then 内容「届いたﾒｰﾙに記載された認証ｺｰﾄﾞを下記へ入力」を含むこと
    Then 内容「してください｡」を含むこと
    Then 内容「しばらく経っても認証ｺｰﾄﾞﾒｰﾙが届かない場合は､再度ﾒｰﾙｱﾄﾞﾚｽ入力からやり直してください｡」を含むこと
    Then 内容「※ ﾄﾞﾒｲﾝ指定受信をご利用の方は､｢@carada.jp｣のﾄﾞﾒｲﾝ解除設定をお願いいたします｡」を含むこと
    Then 内容「※ 最新のﾒｰﾙに記載された認証ｺｰﾄﾞのみが有効となりますのでご注意ください｡」を含むこと
    Then 内容「機種変更等で登録中のﾒｰﾙｱﾄﾞﾚｽがご利用になれなくなった方は､こちらからﾒｰﾙｱﾄﾞﾚｽを再設定してください｡」を含むこと
    Then Cookie名が「CaradadLogin」のCookieが存在しないこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】認証コードのご案内」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA IDログインの認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    Given 最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する
    When 「name」という属性の値が「Verify」のボタンをクリックしたとき
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then レイアウトがFP用であること
    Then 内容「aaa.com」を含むこと
    Then Cookie名が「CaradaIdLogin」のCookieが存在すること
    Then ユーザーDBの「AspNetUsers」テーブルに次のデータが存在すること
    # 二段階認証の列がクリアされていること
    | Id                                   | Email   | EmailConfirmed | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | <NULL>            | True           | 0                 | <NULL>                | 0                   |
    Given Cookie名「TwoFactorLogin」のCookieの値をCurrentのLastCookieValueとして設定する
    Then ストレージの「TwoFactorLogin」に次のデータが存在すること
    | PartitionKey              | RowKey | UserId                               |
    | <Current:LastCookieValue> | -      | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 |
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「parameters:」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「5」回目の出力に「<Current:VerifyCode>」を含むこと	
    Then 「Access」ログのログレベル「INFO」の最後から「4」回目の出力に「二段階認証完了(UserId=4d74efe8-7c62-496e-97cf-3df1a9a084b9, CARADA ID=aaa.com)」を含むこと
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given ストレージの「TwoFactorLogin」テーブルのダンプを取る

Scenario: 二段階認証画面_FP版_正常系_画面表示後に戻るボタン押下
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | aaa.com  | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 3            |
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer     |
    | 4d74efe8-7c62-496e-97cf-3df1a9a084b9 | 1                  | 質問の答え |
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 P903i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Given IDが「CaradaId」に「aaa.com」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「CARADA ID 認証ｺｰﾄﾞ確認」を含むこと
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性の値が「off」であること
    # 改行を含む文言は内容「XXX」を含むこと、ではうまく検証出来ないので1行ずつ検証する
    Then 内容「ご入力いただいたﾒｰﾙｱﾄﾞﾚｽ宛に認証ｺｰﾄﾞを」を含むこと
    Then 内容「送信しました｡」を含むこと
    Then 内容「届いたﾒｰﾙに記載された認証ｺｰﾄﾞを下記へ入力」を含むこと
    Then 内容「してください｡」を含むこと
    Then 内容「しばらく経っても認証ｺｰﾄﾞﾒｰﾙが届かない場合は､再度ﾒｰﾙｱﾄﾞﾚｽ入力からやり直してください｡」を含むこと
    Then 内容「※ ﾄﾞﾒｲﾝ指定受信をご利用の方は､｢@carada.jp｣のﾄﾞﾒｲﾝ解除設定をお願いいたします｡」を含むこと
    Then 内容「※ 最新のﾒｰﾙに記載された認証ｺｰﾄﾞのみが有効となりますのでご注意ください｡」を含むこと
    Then 内容「機種変更等で登録中のﾒｰﾙｱﾄﾞﾚｽがご利用になれなくなった方は､こちらからﾒｰﾙｱﾄﾞﾚｽを再設定してください｡」を含むこと
    Then Cookie名が「CaradadLogin」のCookieが存在しないこと
    Then 最後に送信されたメールタイトルが「【CARADA ID】認証コードのご案内」であること
    Then 最後に送信されたメールのあて先が「a@a.com」の組であること
    Then 最後に送信されたメールの本文に「こちらはCARADA IDログインの認証コードとなります。」の正規表現がマッチすること
    Then 最後に送信されたメールの本文にお問い合わせ先文言が含まれること
    # 「戻る」の確認
    When IDが「Back」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
