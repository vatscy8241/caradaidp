﻿Feature: Login.fp
    ログイン画面の正常系(FP版)

Scenario: ログイン画面_FP版_正常系_未ログイン_初回ログイン画面_利用開始画面遷移
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email  | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a0848z | <NULL> | 0              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | testuser | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # キャリアをdocomoで確認
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Then レイアウトがFP用であること
    Then formの「autocomplete」属性が存在しないこと
    Given IDが「CaradaId」に「testuser」を入力する
    Given IDが「Password」に「0000000a」を入力する
    When 「name」という属性の値が「Login」のボタンをクリックしたとき
    Then 内容「利用開始」を含むこと
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「CaradaId="testuser", Password="*****",」を含むこと

Scenario: ログイン画面_FP版_正常系_パスワード再設定画面に遷移できること
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # キャリアをauで確認
    Given HTTPヘッダーに「User-Agent=KDDI-HI31 UP.Browser/6.2.0.5 (GUI) MMP/2.0」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「PasswordResetGuide」のリンクをクリックしたとき
    Then 内容「ﾊﾟｽﾜｰﾄﾞ再設定」を含むこと

Scenario: ログイン画面_FP版_正常系_ID連絡の画面に遷移できること
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # キャリアをsoftbankで確認
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「CaradaIdNotice」のリンクをクリックしたとき
    Then 内容「ID連絡」を含むこと

Scenario: ログイン画面_FP版_正常系_メールアドレス再設定の画面に遷移できること
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    # キャリアをdocomoで確認
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH900i」を設定する
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    When IDが「EmailReset」のリンクをクリックしたとき
    Then 内容「ﾒｰﾙｱﾄﾞﾚｽ再設定」を含むこと
