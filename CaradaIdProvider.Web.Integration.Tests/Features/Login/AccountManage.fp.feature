﻿Feature: AccountManage.fp
    アカウント管理画面の正常系(FP版)

Scenario: アカウント管理_FP版_正常系_画面が表示されること
    Given HTTPヘッダーに「User-Agent=KDDI-HI3D UP.Browser/6.2_7.2.7.1.K.2.234 (GUI) MMP/2.0 」を設定する
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする
    # レイアウト確認
    Then レイアウトがFP用であること
    # 画面表示項目確認
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    Then 内容「AccountManage」を含むこと
    Then 内容「a@a.com」を含むこと
    Then 内容「テスト質問1」を含むこと

Scenario: アカウント管理_FP版_正常系_メールアドレス変更ボタン押下
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH06A3(c500;TC;W30H18)  」を設定する
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする
    # 画面表示項目確認
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # メールアドレスの「変更」ボタン押下
    When IDが「EmailChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾒｰﾙｱﾄﾞﾚｽ変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること

Scenario: アカウント管理_FP版_正常系_パスワード変更ボタン押下
    Given HTTPヘッダーに「User-Agent=SoftBank/1.0/831SH/SHJ003/SN123456789012345 Browser/NetFront/3.5 Profile/MIDP-2.0 Configuration/CLDC-1.1」を設定する
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする
    # 画面表示項目確認
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # パスワードの「変更」ボタン押下
    When IDが「PasswordChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾊﾟｽﾜｰﾄﾞ変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること

Scenario: アカウント管理_FP版_正常系_秘密の質問変更ボタン押下
    Given HTTPヘッダーに「User-Agent=KDDI-HI3D UP.Browser/6.2_7.2.7.1.K.2.234 (GUI) MMP/2.0」を設定する
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08489 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする
    # 画面表示項目確認
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # 秘密の質問の「変更」ボタン押下
    When IDが「SecurityQuestionChangeView」のリンクをクリックしたとき
    Then 内容「CARADA ID 秘密の質問変更」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること

Scenario: アカウント管理_FP版_正常系_ログアウトリンク押下
    Given HTTPヘッダーに「User-Agent=DoCoMo/2.0 SH06A3(c500;TC;W30H18)」を設定する
    # その他のテーブルはカスケードで削除される
    Given ユーザーDBの「AspNetUsers」テーブルを初期化する
    Given ユーザーDBの「AspNetUsers」テーブルへ次のテストデータを投入する
    | Id                                   | Email   | EmailConfirmed | PasswordHash                                                         | SecurityStamp                        | PhoneNumber | PhoneNumberConfirmed | TwoFactorEnabled | LockoutEndDateUtc | LockoutEnabled | AccessFailedCount | UserName      | EmployeeFlag | CreateDateUtc           | UseStartDateUtc         | UpdateDateUtc | AuthCodeExpireDateUtc | AuthCodeFailedCount |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08490 | a@a.com | 1              | AH2WMH21IGwg7J3De4oJFPmcT713XSYWEvYowXz4fuFBPnO7KHD9ElawD1i1GZ7ScQ== | 82e130c2-9623-4f83-b8ba-370a9c5a5fb4 | <NULL>      | 0                    | 1                | <NULL>            | 1              | 0                 | AccountManage | 1            | 2014-12-10 06:59:58.743 | 2014-12-10 06:59:58.743 | <NULL>        | <NULL>                | 0                   |
    Given IDENTを初期値にしてユーザーDBの「SecurityQuestionMasters」テーブルを初期化する
    Given IDENT列を明示的に指定してユーザーDBの「SecurityQuestionMasters」テーブルへ次のテストデータを投入する
    | SecurityQuestionId | Question      | DisplayOrder |
    | 1                  | テスト質問1   | 1            |
    # 「テスト質問1」の答えは「てすと」
    Given ユーザーDBの「SecurityQuestionAnswers」テーブルへ次のテストデータを投入する
    | UserId                               | SecurityQuestionId | Answer                                                           |
    | 4d74efe8-7c62-496e-97cf-3df1a9a08490 | 1                  | e715ed6eb0c8a52d8d016a8cf7d33daa39c874b134e43e6c16237db3ac360e6a |
    Given Cookie名「CaradaIdLogin」のCookieを削除する
    Given Cookie名「TwoFactorLogin」のCookieを削除する
    # SecurityStamp が変化していることの確認用
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Given 「/User/LoginView」にブラウザでアクセスする
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    Given ID「AccountManage」、パスワード「0000000a」のアカウントでFP版にログインする
    # 画面表示項目確認
    Then 内容「CARADA ID ｱｶｳﾝﾄ管理」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # ログアウトリンク押下
    When IDが「Logout」のリンクをクリックしたとき
    Then 内容「CARADA ID ﾛｸﾞｲﾝ」を含むこと
    # レイアウト確認
    Then レイアウトがFP用であること
    # SecurityStamp と が更新されていることを確認
    Given ユーザーDBの「AspNetUsers」テーブルのダンプを取る
    Then 「Access」ログのログレベル「INFO」の最後から「2」回目の出力に「ログアウト」を含むこと