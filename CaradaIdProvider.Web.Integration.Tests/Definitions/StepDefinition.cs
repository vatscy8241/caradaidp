﻿using CaradaIdProvider.Web.Integration.Tests.Properties;
using CaradaIdProvider.Web.Integration.Tests.TestHelper;
using CaradaIdProvider.Web.Test.Integration.TestHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace CaradaIdProvider.Web.Integration.Tests.Definitions
{
    /// <summary>
    /// シナリオ定義クラスの抽象クラス
    /// </summary>
    [Binding]
    class StepDefinition : Steps
    {
        /// <summary>
        /// HOMEのタイトル
        /// </summary>
        private const string HomeTitle = "CARADA ID";

        /// <summary>
        /// HOME以外タイトルに付けるもの
        /// </summary>
        private const string TitleFollow = " | " + HomeTitle;

        /// <summary>
        /// ユーザーDB
        /// </summary>
        private CaradaIdPDbContext userDb = new CaradaIdPDbContext();

        /// <summary>
        /// ブラウザを操作するドライバ
        /// </summary>
        private IWebDriver WebDriver { get; set; }

        /// <summary>
        /// DBダンプデータ
        /// </summary>
        public class DumpRows
        {
            /// <summary>
            /// テーブル名
            /// </summary>
            public string TableName { get; set; }
            /// <summary>
            /// ヘッダ 列名と出力桁数を保持
            /// </summary>
            public Dictionary<string, int> Headers { get; set; }
            /// <summary>
            /// データ
            /// </summary>
            public List<Dictionary<string, string>> DataRows { get; set; }
        };

        /// <summary>
        /// メールに含まれるお問い合わせ先文言
        /// </summary>
        private const string SupportText = @"CARADAコンタクトセンター\r\n +http://mti7.okbiz.okwave.jp/category/show/2282";


        #region シナリオ変数utility
        /// <summary>
        /// データを一時保存し、次ステップ以降に引き回します。
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="value">値</param>
        /// <returns></returns>
        private void SaveCurrentData(string key, string value)
        {
            ScenarioContext.Current["Current:" + key] = value;
        }

        /// <summary>
        /// 一時保存したデータを再取得します。
        /// </summary>
        /// <param name="key">キー</param>
        /// <returns>値</returns>
        private object ReceiveCurrentData(string key)
        {
            if (!ScenarioContext.Current.ContainsKey("Current:" + key))
            {
                throw new SpecFlowException(string.Format("指定されたキー「{0}」にはデータは紐づいていません。", key));
            }
            return ScenarioContext.Current["Current:" + key];
        }

        /// <summary>
        /// それまでのステップで一時保存したデータを動的に埋め込みます。
        /// &lt;Current:[key名]&gt; を含まない場合は文字列をそのまま返します。
        /// </summary>
        /// <param name="value">
        ///   <list type="number">
        ///     <item>
        ///       <term>Current</term>
        ///       <description>
        ///         &lt;Current:【*1】&gt;
        ///         【*1】：一時保存した際のキー
        ///       </description>
        ///     </item>
        ///   </list>
        /// </param>
        /// <returns></returns>
        private string ConvertCurrentData(string value)
        {
            var strValue = value;
            while (true)
            {
                var match = Regex.Match(strValue, @"<Current:([a-zA-Z0-9_-]+)>", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    var key = match.Groups[1].Value;
                    var tempValue = ReceiveCurrentData(key).ToString();
                    strValue = strValue.Replace(match.Captures[0].Value, tempValue);
                }
                else
                {
                    break;
                }
            }
            return strValue;
        }

        /// <summary>
        /// Spec Flow Table中の文字列を置き換える開発者定義のFunc。
        /// &lt;NULL&rt;&lt;%21NULL&gt;は予約済みなので使用禁止。
        /// </summary>
        private List<Func<string, string>> ConvertSpecialStrings { get; set; }

        /// <summary>
        /// SpecFlowTableをIEnumerable&lt;dynamic&gt;にします。
        /// </summary>
        /// <param name="oldTable">Table</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        private IEnumerable<dynamic> CreateDynamicSet(Table oldTable)
        {
            var newTable = new Table(oldTable.Header.ToArray());

            var set = oldTable.CreateDynamicSet();

            foreach (var row in set)
            {
                var newRow = new List<string>();

                foreach (var cell in row as IDictionary<string, object>)
                {
                    var s = cell.Value.ToString();
                    ConvertSpecialStrings.ForEach(f => s = f(s));
                    newRow.Add(s);
                }
                newTable.AddRow(newRow.ToArray());
            }

            return newTable.CreateDynamicSet();
        }

        /// <summary>
        /// 頭に@が付いたvalue値を文字列にして返す。
        /// 頭に#が付いたvalue値をdouble型にして返す。
        /// 頭に*が付いたvalue値をDateTimeOffset型にして返す。この場合、Offset付き文字列でない場合は、実行環境デフォルトのOffsetになります。
        /// それ以外は文字列とする
        /// </summary>
        /// <param name="value">value</param>
        /// <returns>型変換したvalue</returns>
        private object ConvertTableValueType(string value)
        {
            var strValue = value.ToString();

            if (Regex.IsMatch(strValue, "^@[^@]"))
            {
                return strValue.Substring(1);
            }
            else if (Regex.IsMatch(strValue, "^#[^#]"))
            {
                return double.Parse(strValue.Substring(1));

            }
            else if (Regex.IsMatch(strValue, @"^\*[^*]"))
            {
                return DateTimeOffset.Parse(strValue.Substring(1));
            }
            return strValue.Replace("@@", "@").Replace("##", "#").Replace("**", "*");
        }
        #endregion

        #region Scenario SetUp/TearDown

        /// <summary>
        /// シナリオ実行前処理
        /// </summary>
        [BeforeScenario()]
        private void BeforeScenario()
        {
            ConvertSpecialStrings = new List<Func<string, string>>();
            ConvertSpecialStrings.Add(ConvertSpecialDate);
            ConvertSpecialStrings.Add(ConvertCurrentData);

            WebDriver = TestUtil.GetWebDriver();
        }

        /// <summary>
        /// 動的な日付文字列を生成します。
        /// &lt;ThisMonth&gt;,&lt;ThisMonthLast&gt;,&lt;Today&gt;を含まない場合は文字列をそのまま返します。
        /// </summary>
        /// <param name="value">
        ///   <list type="number">
        ///     <item>
        ///     <term>ThisMonth</term>
        ///     <description>
        ///     &lt;ThisMonth【*1】&gt;【*2】dd形式の文字列
        ///     【*1】：当月ではこの部分はなし、翌月では+1 先月では-1と記載します。
        ///     【*2】：区切り文字です。
        ///     例：当月=2015/01の場合
        ///         value=&lt;ThisMonth-4&gt;/02 => 2014/09/02が返る
        ///         value=&lt;ThisMonth&gt;-11 => 2015-01-11が返る
        ///     </description>
        ///     </item>
        ///     <item>
        ///     <term>ThisMonthLast</term>
        ///     <description>
        ///     &lt;ThisMonthLast&gt;
        ///     </description>
        ///     </item>
        ///     <item>
        ///     <term>Today</term>
        ///     <description>
        ///     &lt;Today【*1】&gt;
        ///     【*1】 : 今日はこの部分はなし、明日は+1、昨日は-1と記載します。
        ///     yyyy-MM-dd形式の文字列を返します。
        ///     </description>
        ///     </item>
        ///     <item>
        ///     <term>Today/</term>
        ///     <description>
        ///     &lt;Today【*1】&gt;
        ///     【*1】 : 今日はこの部分はなし、明日は+1、昨日は-1と記載します。
        ///     yyyy/MM/dd形式の文字列を返します。
        ///     </description>
        ///     </item>
        ///   </list>
        /// </param>
        /// <returns>変換したvalue</returns>
        private string ConvertSpecialDate(string value)
        {
            var strValue = value;
            while (true)
            {
                var match = Regex.Match(strValue, @"<Today([+-]\d+)?\>", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    var dt = DateTime.Now;
                    if (match.Groups[1].Length > 0)
                    {
                        dt = dt.AddDays(int.Parse(match.Groups[1].Value));
                    }
                    strValue = strValue.Replace(match.Captures[0].Value, dt.ToString(string.Format("yyyy-MM-dd")));
                }
                else
                {
                    break;
                }
            }
            while (true)
            {
                var match = Regex.Match(strValue, @"<Today/([+-]\d+)?\>", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    var dt = DateTime.Now;
                    if (match.Groups[1].Length > 0)
                    {
                        dt = dt.AddDays(int.Parse(match.Groups[1].Value));
                    }
                    strValue = strValue.Replace(match.Captures[0].Value, dt.ToString(string.Format("yyyy/MM/dd")));
                }
                else
                {
                    break;
                }
            }

            while (true)
            {
                var match = Regex.Match(strValue, @"<ThisMonthLast>", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    var dt = DateTime.Now;
                    dt = new DateTime(dt.Year, dt.Month, 1).AddMonths(1).AddDays(-1);
                    strValue = strValue.Replace(match.Captures[0].Value, dt.ToString(string.Format("yyyy-MM-dd")));
                }
                else
                {
                    break;
                }
            }

            while (true)
            {
                var match = Regex.Match(strValue, @"<ThisMonth([+-]\d+)?\>(.{1})(\d+)", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    var dt = DateTime.Now;
                    if (match.Groups[1].Length > 0)
                    {
                        dt = dt.AddMonths(int.Parse(match.Groups[1].Value));
                    }
                    var delimiter = match.Groups[2].Value;
                    dt = new DateTime(dt.Year, dt.Month, int.Parse(match.Groups[3].Value));
                    strValue = strValue.Replace(match.Captures[0].Value, dt.ToString(string.Format("yyyy{0}MM{0}dd", delimiter)));

                }
                else
                {
                    break;
                }
            }
            return strValue;
        }

        /// <summary>
        /// シナリオ完了後処理
        /// </summary>
        [AfterScenario()]
        private void AfterScenario()
        {
            Given(@"ブラウザーを閉じる");
        }
        #endregion

        #region Given Methods

        /// <summary>
        /// 指定のURIにIEブラウザでアクセスする。
        /// </summary>
        /// <param name="accessUri">アクセスURI</param>
        [Given(@"「(.*)」にブラウザでアクセスする")]
        public void GivenAccessToUri(string accessUri)
        {
            var uri = ConvertAbsoluteUrlToRequestUrl(accessUri);

            WebDriver.Navigate().GoToUrl(uri);

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        [Given(@"インターネットサイト「(.*)」にブラウザでアクセスする")]
        public void GivenAccessToInternetUri(string uri)
        {
            WebDriver.Navigate().GoToUrl(uri);

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// IDからフィールドを検索し、そのテキストにそれまでのステップで一時保存した値を入力する
        /// </summary>
        /// <param name="valueId">入力するテキストのID</param>
        /// <param name="value">入力するテキスト</param>
        [Given(@"IDが「(.*)」に「(.*)」を入力する")]
        public void InputTextById(string valueId, string value)
        {
            WebDriver.FindElement(By.Id(valueId)).Clear();
            WebDriver.FindElement(By.Id(valueId)).SendKeys(ConvertCurrentData(value));
        }

        /// <summary>
        /// IDからフィールドを検索し、その側のドロップダウンに値を選択する
        /// </summary>
        /// <param name="valueId">ドロップダウンのID</param>
        /// <param name="value">選択値</param>
        [Given(@"IDが「(.*)」の「(.*)」を選択する")]
        public void SelectValueById(string valueId, string value)
        {
            var select = new SelectElement(WebDriver.FindElement(By.Id(valueId)));
            select.SelectByText(value);

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// チェックボックスをONにする
        /// </summary>
        /// <param name="valueId">チェックボックスのID</param>
        [Given(@"IDが「(.*)」のチェックボックスをONにする")]
        public void CheckOnById(string valueId)
        {
            WebDriver.FindElement(By.Id(valueId)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// 名前からチェックボックスを検索し、チェックボックスをONにする
        /// </summary>
        /// <param name="value">チェックボックスのvalueの値</param>
        [Given(@"名前が「(.*)」のチェックボックスをONにする")]
        public void CheckOnByValue(string value)
        {
            WebDriver.FindElement(By.Name(value)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        ///  属性の名前と値でボタンをクリックする
        /// </summary>
        /// <param name="attributeName">属性の名前</param>
        /// <param name="attirbuteValue">属性の値</param>
        [Given(@"「(.*)」という属性の値が「(.*)」のボタンをクリックする")]
        public void ClickButtonByParameterValue(string attributeName, string attributeValue)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.XPath("//*[@" + attributeName + "='" + attributeValue + "']")).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// 何秒待ち
        /// </summary>
        [Given(@"「(.*)」秒待ち")]
        public void WaitSecond(int seconds)
        {
            Thread.Sleep(seconds * 1000);
        }

        /// <summary>
        /// ブラウザーを閉じる
        /// </summary>
        [Given(@"ブラウザーを閉じる")]
        private void CloseWebBrowser()
        {
            WebDriver.Close();
            WebDriver.Quit();
        }

        /// <summary>
        /// 画面をリフレッシュする
        /// </summary>
        [Given(@"画面をリフレッシュする")]
        private void RefreshPage()
        {
            WebDriver.Navigate().Refresh();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// ログイン時間
        /// </summary>
        private DateTime loginDateTime;

        /// <summary>
        /// 指定IDとパスワードのアカウントでログインする
        /// </summary>
        /// <param name="id">ID</param>
        /// <param name="password">パスワード</param>
        [Given(@"ID「(.*)」、パスワード「(.*)」のアカウントでログインする")]
        private void LoginUser(string id, string password)
        {
            Given(@"IDが「CaradaId」に「" + id + "」を入力する");
            Given(@"IDが「Password」に「" + password + "」を入力する");
            When(@"valueが「ログイン」のボタンをクリックしたとき");

            // 認証コード入力画面の場合（3秒内に認証コード入力画面と判別できた場合）
            if (isAbleInTime(() => WebDriver.PageSource.Contains("CARADA ID 認証コード確認"), 3))
            {
                Given(@"最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する");
                When(@"「name」という属性の値が「Verify」のボタンをクリックしたとき");
            }

            loginDateTime = DateTime.Now;
        }

        /// <summary>
        /// DBのセッションデータをクリアする
        /// </summary>
        [Given(@"DBのセッションデータをクリアする")]
        private void ClearDBSession()
        {
            var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["SessionDbContext"].ConnectionString);
            try
            {
                sqlConn.Open();
                var cmd = sqlConn.CreateCommand();
                cmd.CommandText = "DELETE FROM dbo.Sessions;";
                cmd.ExecuteNonQuery();
            }
            finally
            {
                sqlConn.Close();
                sqlConn.Dispose();
            }
        }

        /// <summary>
        /// ユーザーDBのデータを初期化する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        [Given(@"ユーザーDBの「(.*)」テーブルを初期化する")]
        public void InitializeUserDb(string tableName)
        {
            DbDelete(tableName, userDb);
        }

        /// <summary>
        /// ID列を1にして初期化する。ID列が存在しなければ例外が発生する。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        [Given(@"IDENTを初期値にしてユーザーDBの「(.*)」テーブルを初期化する")]
        public void InitializeUserDbIdentOne(string tableName)
        {
            DbDelete(tableName, userDb, true);
        }

        /// <summary>
        /// ユーザーDBのテーブルへ次のテストデータを投入する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">挿入するデータ</param>
        [Given(@"ユーザーDBの「(.*)」テーブルへ次のテストデータを投入する")]
        public void InsertUserDb(string tableName, TechTalk.SpecFlow.Table table)
        {
            DbInsert(tableName, table, userDb);
        }

        /// <summary>
        /// ID列を指定してユーザーDBのテーブルへ次のテストデータを投入する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">挿入するデータ</param>
        [Given(@"IDENT列を明示的に指定してユーザーDBの「(.*)」テーブルへ次のテストデータを投入する")]
        public void InsertUserDbIdent(string tableName, TechTalk.SpecFlow.Table table)
        {
            DbInsert(tableName, table, userDb, true);
        }

        /// <summary>
        /// DBエビデンスとして指定テーブルのダンプを取る
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        [Given(@"ユーザーDBの「(.*)」テーブルのダンプを取る")]
        public void GetDumpUserDb(string tableName)
        {
            DumpData(DbAllSelect(tableName, userDb));
        }

        /// <summary>
        /// ストレージのデータを初期化する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        [Given(@"ストレージの「(.*)」テーブルを初期化する")]
        public void InitializeStorageTable(string tableName)
        {
            var cloudTable = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudTableClient().GetTableReference(tableName);
            cloudTable.DeleteIfExists();
            cloudTable.Create();
        }

        /// <summary>
        /// ストレージのテーブルへ次のテストデータを投入する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">挿入するデータ</param>
        [Given(@"ストレージの「(.*)」テーブルへ次のテストデータを投入する")]
        public void InsertDefaultStorageTable(string tableName, TechTalk.SpecFlow.Table table)
        {
            // StorageInsert(tableName, table);
            TableStorageInsert(tableName, table, CloudStorageAccount.DevelopmentStorageAccount);
        }

        /// <summary>
        /// Table Storageへデータを登録する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">table</param>
        /// <param name="account">アカウント</param>
        private void TableStorageInsert(string tableName, Table table, CloudStorageAccount account)
        {
            var entities = CreateDynamicSet(table);

            CloudTable cloudTable = account.CreateCloudTableClient().GetTableReference(tableName);

            foreach (var entity in entities)
            {
                DynamicTableEntity tableEntity = new DynamicTableEntity();

                var dic = entity as IDictionary<string, object>;

                foreach (var t in (from s in dic
                                   where s.Key != "Timestamp"
                                   select s))
                {
                    if (t.Value.ToString() == "<NULL>")
                    {
                        tableEntity[t.Key] = EntityProperty.CreateEntityPropertyFromObject(null);
                    }
                    else
                    {

                        var value = ConvertTableValueType(t.Value.ToString());
                        if (t.Key == "PartitionKey")
                        {
                            tableEntity.PartitionKey = (string)value;
                        }
                        else if (t.Key == "RowKey")
                        {
                            tableEntity.RowKey = (string)value;
                        }
                        else
                        {
                            if (value.GetType() == typeof(double))
                            {
                                tableEntity[t.Key] = new EntityProperty((double)value);
                            }
                            else if (value.GetType() == typeof(DateTime))
                            {
                                tableEntity[t.Key] = new EntityProperty((DateTime)value);
                            }
                            else if (value.GetType() == typeof(DateTimeOffset))
                            {
                                tableEntity[t.Key] = new EntityProperty((DateTimeOffset)value);
                            }
                            else
                            {
                                tableEntity[t.Key] = new EntityProperty((string)value);
                            }
                        }
                    }
                }

                cloudTable.Execute(TableOperation.InsertOrReplace(tableEntity));
            }
        }

        /// <summary>
        /// Table Storageからキーにマッチするデータを削除する。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">table</param>
        /// <param name="account">アカウント</param>
        private void TableStorageDelete(string tableName, Table table, CloudStorageAccount account)
        {
            var entities = CreateDynamicSet(table);

            CloudTable cloudTable = account.CreateCloudTableClient().GetTableReference(tableName);

            foreach (var row in entities)
            {
                var conditions = (from kv in row as IDictionary<string, object>
                                  select TableQuery.GenerateFilterCondition(kv.Key, QueryComparisons.Equal, (string)ConvertTableValueType(kv.Value.ToString())));

                var where = conditions.Aggregate(conditions.First(), (a, b) => TableQuery.CombineFilters(a, TableOperators.And, b));

                TableQuery<DynamicTableEntity> query = new TableQuery<DynamicTableEntity>() { FilterString = where };
                foreach (var tableEntity in cloudTable.ExecuteQuery(query))
                {
                    tableEntity.ETag = "*";
                    cloudTable.Execute(TableOperation.Delete(tableEntity));
                }
            }
        }

        /// <summary>
        /// DBエビデンスとしてストレージの指定テーブルのダンプを取る
        /// </summary>
        /// <param name="tableName">を表す文字列</param>
        [Given(@"ストレージの「(.*)」テーブルのダンプを取る")]
        public void GetDumpDefaultStorage(string tableName)
        {
            DumpData(TableStorageAllSelect(tableName, CloudStorageAccount.DevelopmentStorageAccount));
        }

        /// <summary>
        /// クッキーを削除する
        /// </summary>
        /// <param name="cookieName">Cookie名</param>
        [Given(@"Cookie名「(.*)」のCookieを削除する")]
        public void DeleteCookie(string cookieName)
        {
            WebDriver.Manage().Cookies.DeleteCookieNamed(cookieName);
        }

        /// <summary>
        /// クッキーを追加する
        /// </summary>
        /// <param name="accessUri">移動するURL</param>
        /// <param name="cookieName">クッキー名</param>
        /// <param name="value">値</param>
        /// <param name="datetime">UTCの有効期限</param>
        [Given(@"「(.*)」に移動しつつ、Cookie名「(.*)」値「(.*)」有効期限「(.*)」のCookieを追加する")]
        public void SetCookie(string accessUri, string cookieName, string value, string datetime)
        {
            var uri = ConvertAbsoluteUrlToRequestUrl(accessUri);

            var cookie = new OpenQA.Selenium.Cookie(cookieName, value, null, "/", DateTime.Parse(datetime, CultureInfo.CurrentCulture, DateTimeStyles.AssumeUniversal));

            WebDriver.Navigate().GoToUrl(uri);
            WebDriver.Manage().Cookies.AddCookie(cookie);
        }

        /// <summary>
        /// クッキーの値を取得する
        /// </summary>
        /// <param name="cookieName">Cookie名</param>
        [Given(@"Cookie名「(.*)」のCookieの値をCurrentのLastCookieValueとして設定する")]
        public void GivenSetLastCookieValue(string cookieName)
        {
            SaveCurrentData("LastCookieValue", GetCookie(cookieName).Value);
        }

        /// <summary>
        /// 最後に送信されたメールから認証コードを取得し、入力する。
        /// メールが取れないか認証コードらしき値が見つからないときは空欄とする。
        /// </summary>
        /// <param name="elementId">要素ID</param>
        [Given(@"最後に送信されたメールから取得した認証コードを「(.*)」というIDのオブジェクトに入力する")]
        public void GetVerifyCodeAndInput(string elementId)
        {
            if (elementId == "")
            {
                throw new SpecFlowException("elementIdを指定してください");
            }
            Given(@"最後に送信されたメールから取得した認証コードを「" + elementId + "」というシナリオ変数へ設定する");
            var code = ReceiveCurrentData(elementId) as string;
            Given(@"IDが「" + elementId + "」に「" + code + "」を入力する");
        }


        /// <summary>
        /// 最後に送信されたメールから認証コードを取得し、シナリオ変数へ設定する。
        /// メールが取れないか認証コードらしき値が見つからないときは空文字列とする。
        /// </summary>
        /// <param name="variableName">シナリオ変数Currentのキー名</param>
        [Given(@"最後に送信されたメールから取得した認証コードを「(.*)」というシナリオ変数へ設定する")]
        public void GetVerifyCode(string variableName)
        {
            var code = "";
            var mail = GetLastMail();
            if (mail != null)
            {
                var match = Regex.Match(mail.BodyPlainText, @"\d{6}");
                if (match.Success)
                {
                    code = match.Value;
                    SaveCurrentData(variableName, code);
                }
            }
        }

        /// <summary>
        /// DBエラーにするためにクライアントIDを登録する。
        /// AspNetUsersテーブルに利用開始前状態のデータが1件のみの場合に使用できる。
        /// </summary>
        [Given(@"DBエラーにするためにクライアントID「(.*)」を登録する")]
        public void InsertAuthorizedUsersForDBError(string clientId)
        {
            try
            {
                userDb.Database.Connection.Open();
                var userId = string.Empty;
                using (var cmd = userDb.Database.Connection.CreateCommand())
                {
                    cmd.CommandText = "select Id from AspNetUsers";
                    using (var reader = cmd.ExecuteReader())
                    {
                        Assert.IsTrue(reader.HasRows, "AspNetUsers未登録");
                        reader.Read();
                        userId = reader.GetString(0);
                    }
                }
                userDb.Database.ExecuteSqlCommand("insert into AuthorizedUsers values ('" + userId + "','" + clientId + "','aaa')");
            }
            finally
            {
                userDb.Database.Connection.Close();
            }
        }

        /// <summary>
        /// ユーザーDB更新用に、シナリオ変数へ任意のデータを設定する
        /// </summary>
        /// <remarks>シナリオ内のSpec表オブジェクトとして任意のデータを保持する</remarks>
        /// <param name="table">変数テーブル</param>
        [Given(@"シナリオ変数名PreparedParamsとして以下のField->Valueペアを設定する")]
        public void GivenPrepareUpdateDb(Table table)
        {
            ScenarioContext.Current["PreparedParams"] = table;
        }

        /// <summary>
        /// ユーザーDBのテーブルを任意のデータで更新する
        /// </summary>
        /// <remarks>
        /// 任意のデータでDBを更新する。
        /// ※主キーかどうかのチェックはしていないので、その場合は実行時確実に失敗する。
        /// </remarks>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">更新対象のレコードの条件</param>
        [Given(@"ユーザーDBの「(.*)」テーブルの以下の条件に合うレコードを、シナリオ変数PreparedParamsの値で更新する")]
        public void GivenUpdateDbWithPreparedParams(string tableName, Table table)
        {
            if (!ScenarioContext.Current.ContainsKey("PreparedParams"))
            {
                throw new SpecFlowException("PreparedParamsが未設定です。GivenPrepareUpdateDb()などで先に設定してください。");
            }

            var preparedTable = ScenarioContext.Current["PreparedParams"] as Table;
            var preparedParams = CreateDynamicSet(preparedTable);

            var entities = CreateDynamicSet(table);
            try
            {
                userDb.Database.Connection.Open();
                var currentRow = 0;
                foreach (var entity in entities)
                {
                    currentRow++;
                    var parameters = new List<SqlParameter>();

                    var sql = "UPDATE " + tableName + " SET ";
                    var preparedParamsCount = 0;
                    foreach (var row in preparedParams)
                    {
                        var fieldName = "";
                        foreach (var cell in row as IDictionary<string, object>)
                        {
                            if (cell.Key == "Field")
                            {
                                fieldName = cell.Value.ToString();
                                sql += cell.Value + " = @SET_" + fieldName + " ";
                            }
                            else
                            {
                                parameters.Add(new SqlParameter("@SET_" + fieldName, SqlDbType.NVarChar) { Value = cell.Value.ToString() });
                            }
                            preparedParamsCount++;
                            if ((preparedParamsCount % 2 == 0) && (preparedParamsCount != preparedParams.Count() * 2))
                            {
                                sql += ", ";
                            }
                        }
                    }

                    var sqlWhere = "";
                    foreach (var s in entity as IDictionary<string, object>)
                    {
                        if (sqlWhere == "")
                        {
                            sqlWhere += "WHERE ";
                        }
                        else
                        {
                            sqlWhere += "AND ";
                        }

                        if (s.Value.ToString() == "<NULL>")
                        {
                            sqlWhere += s.Key + " IS NULL ";
                        }
                        else if (s.Value.ToString() == "<!NULL>")
                        {
                            sqlWhere += s.Key + " IS NOT NULL ";
                        }
                        else
                        {
                            sqlWhere += s.Key + " = @" + s.Key + " ";
                            parameters.Add(new SqlParameter("@" + s.Key, SqlDbType.NVarChar) { Value = s.Value.ToString() });
                        }
                    }

                    userDb.Database.ExecuteSqlCommand(sql + sqlWhere, parameters.ToArray());
                    break;
                }
            }
            finally
            {
                userDb.Database.Connection.Close();
            }
        }

        /// <summary>
        /// DBのAspNetUsersテーブルでユーザー名からIdを逆引きしてシナリオ変数に設定する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="fieldName">フィールド名</param>
        /// <param name="fieldValue">期待値</param>
        /// <param name="targetFieldName">取得するフィールド名</param>
        /// <param name="keyName">シナリオ変数Currentのキー名</param>
        [Given("DBテーブル「(.*)」のフィールド名「(.*)」の値が「(.*)」のレコードから、「(.*)」フィールドの値を取得し、シナリオ変数「(.*)」として設定する")]
        public void GivenSetUserIdFromUserName(string tableName, string fieldName, string fieldValue, string targetFieldName, string keyName)
        {
            var result = String.Empty;
            try
            {
                userDb.Database.Connection.Open();

                using (var cmd = userDb.Database.Connection.CreateCommand())
                {
                    var sql = "SELECT " + targetFieldName + " From " + tableName + " WHERE " + fieldName + " = '" + fieldValue + "'";
                    cmd.CommandText = sql;

                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            result = reader.GetValue(0).ToString();
                            break;
                        }
                    }
                }
            }
            finally
            {
                userDb.Database.Connection.Close();
            }

            if (string.IsNullOrEmpty(result))
            {
                throw new SpecFlowException("レコードが取得できませんでした。");
            }

            SaveCurrentData(keyName, result);
        }

        /// <summary>
        /// 文字列をSHA-256として暗号化し、シナリオ変数へ設定する
        /// </summary>
        /// <param name="plainText">暗号化前の文字列</param>
        /// <param name="key">シナリオ変数のキー名</param>
        [Given(@"文字列「(.*)」をSHA-256で暗号化してシナリオ変数Currentへ「(.*)」として設定する")]
        public void GivenSetSHA256Strings(string plainText, string key)
        {
            //  MTI.CaradaIdProvider.Web.Logics.HashConverter ConvertString() から移植
            var signerSha256 = SHA256.Create();
            var hashStringByte = signerSha256.ComputeHash(Encoding.UTF8.GetBytes(plainText));
            var hashString = "";
            foreach (var x in hashStringByte)
            {
                hashString += String.Format("{0:x2}", x);
            }
            SaveCurrentData(key, hashString);
        }

        /// <summary>
        /// HTTPヘッダに値を設定してWebDriverを初期化する。
        /// </summary>
        /// <param name="headersTsv">HTTPヘッダに設定するキーと値(TSV形式)</param>
        /// <remarks>
        /// headersTsvはkey1=value1\tkey2=value2\t・・・と複数指定できます。
        /// </remarks>
        [Given("HTTPヘッダーに「(.*)」を設定する")]
        public void GivenWebDriverWithHttpHeaders(string headersTsv)
        {
            string[] headers = headersTsv.Replace("\\t", "\t").Split('\t');
            Dictionary<string, string> options = headers.ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
            CloseWebBrowser();
            WebDriver = TestUtil.GetWebDriver(true, options);
        }
        #endregion

        #region When Methods

        /// <summary>
        /// IDからリンクを検索して、クリックする。
        /// </summary>
        /// <param name="elementId">要素ID</param>
        [When(@"IDが「(.*)」のリンクをクリックしたとき")]
        public void WhenClickLinkByElementId(string elementId)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.Id(elementId)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        ///  属性の名前と値でリンクをクリックする
        /// </summary>
        /// <param name="attributeName">属性の名前</param>
        /// <param name="attirbuteValue">属性の値</param>
        [When(@"「(.*)」という属性の値が「(.*)」のリンクをクリックしたとき")]
        public void WhenClickLinkByParameterValue(string attributeName, string attributeValue)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.XPath("//a[@" + attributeName + "='" + attributeValue + "']")).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        ///  属性の名前と値でボタンをクリックする
        /// </summary>
        /// <param name="attributeName">属性の名前</param>
        /// <param name="attirbuteValue">属性の値</param>
        [When(@"「(.*)」という属性の値が「(.*)」のボタンをクリックしたとき")]
        public void WhenClickButtonByParameterValue(string attributeName, string attributeValue)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.XPath("//*[@" + attributeName + "='" + attributeValue + "']")).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// valueからリンクを検索して、クリックする。
        /// </summary>
        /// <param name="elementId">要素ID</param>
        [When(@"valueが「(.*)」のリンクをクリックしたとき")]
        public void WhenClickLink(string value)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.LinkText(value)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// valueからボタンを検索して、クリックする。
        /// </summary>
        /// <param name="value">ボタンのvalue</param>
        [When(@"valueが「(.*)」のボタンをクリックしたとき")]
        public void ClickButton(string value)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();
            WebDriver.FindElement(By.XPath("//*[@value='" + value + "']")).Click();
            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// valueからヘッダーリンクを検索して、クリックする。
        /// </summary>
        /// <param name="value">value</param>
        [When(@"valueが「(.*)」のヘッダーリンクをクリックしたとき")]
        public void WhenClickHeaderLink(string value)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.ClassName("tmpl__header-maininset"))
                              .FindElement(By.LinkText(value)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// valueからナビゲーションリンクを検索して、クリックする。
        /// </summary>
        /// <param name="value">value</param>
        [When(@"valueが「(.*)」のナビゲーションリンクをクリックしたとき")]
        public void WhenClickNavigationLink(string value)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.ClassName("tmpl__header-subinset02")).FindElement(By.LinkText(value)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// valueからボディリンクを検索して、クリックする。
        /// </summary>
        /// <param name="value">value</param>
        [When(@"valueが「(.*)」のボディリンクをクリックしたとき")]
        public void WhenClickBodyLink(string value)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.ClassName("content__main")).FindElement(By.LinkText(value)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// valueからフッターリンクを検索して、クリックする。
        /// </summary>
        /// <param name="value">value</param>
        [When(@"valueが「(.*)」のフッターリンクをクリックしたとき")]
        public void WhenClickFooterLink(string value)
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.FindElement(By.ClassName("tmpl__footer-inset")).FindElement(By.LinkText(value)).Click();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// 前に戻る
        /// </summary>
        [When(@"前に戻る")]
        public void WhenGoBack()
        {
            // エビデンスを捕獲する
            CaptureWebPageToFile();

            WebDriver.Navigate().Back();

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        #endregion

        #region Then Methods

        /// <summary>
        /// タイトルが検証文字列と同等であることを検証する。
        /// </summary>
        /// <param name="expected">検証値</param>
        [Then(@"タイトルが「(.*)」であること")]
        public void ThenAssertTitle(string expected)
        {
            if (!expected.Equals(HomeTitle))
            {
                expected += TitleFollow;
            }
            Assert.AreEqual(expected, WebDriver.Title);
        }

        /// <summary>
        /// タイトルが検証文字列で終わることを検証する。タイトル前半にアプリケーション名がつくため作成。
        /// </summary>
        /// <param name="expected">検証値</param>
        [Then(@"タイトルが「(.*)」で終わること")]
        public void ThenAssertTitleEnd(string expected)
        {
            StringAssert.EndsWith(WebDriver.Title, expected);
        }

        /// <summary>
        /// エラーメッセージを検証する。
        /// </summary>
        /// <param name="idFor">ForのID</param>
        /// <param name="expected">エラーメッセージ内容</param>
        [Then(@"Forが「(.*)」のエラーメッセージは「(.*)」であること")]
        public void ThenAssertErrMsg(string idFor, string expected)
        {
            Assert.AreEqual(expected, WebDriver.FindElement(By.XPath("//Span[@For='" + idFor + "']")).Text);
        }

        /// <summary>
        /// エラーメッセージを検証する。
        /// </summary>
        /// <param name="idFor">ForのID</param>
        /// <param name="expected">エラーメッセージ内容</param>
        [Then(@"dataValmsgForが「(.*)」のエラーメッセージは「(.*)」であること")]
        public void ThenAssertErrMsgByDataVal(string idFor, string expected)
        {
            Assert.AreEqual(expected, WebDriver.FindElement(By.XPath("//Span[@data-valmsg-for='" + idFor + "']")).Text);
        }

        /// <summary>
        /// エラーメッセージを検証する。
        /// </summary>
        /// <param name="idFor">ForのID</param>
        /// <param name="expected">エラーメッセージ内容</param>
        [Then(@"統合エラーメッセージは「(.*)」であること")]
        public void ThenAssertErrMsg(string expected)
        {
            Assert.AreEqual(expected, WebDriver.FindElement(By.XPath("//Div[@class='validation-summary-errors']")).Text);
        }

        /// <summary>
        /// IDからフィールドを検索し、そのテキストの入力値を検証する
        /// </summary>
        /// <param name="valueId">入力するテキストのID</param>
        /// <param name="value">検証値</param>
        [Then(@"IDが「(.*)」の入力値は「(.*)」であること")]
        public void ThenAssertInputedTextById(string valueId, string expected)
        {
            var value = WebDriver.FindElement(By.Id(valueId)).GetAttribute("value");
            if (string.IsNullOrEmpty(expected))
            {
                Assert.IsTrue(string.IsNullOrEmpty(value));
            }
            else
            {
                Assert.AreEqual(expected, value);
            }
        }

        /// <summary>
        /// IDからフィールドを検索し、その側のドロップダウンの選択値を検証する
        /// </summary>
        /// <param name="valueId">ドロップダウンのID</param>
        /// <param name="value">検証値</param>
        [Then(@"IDが「(.*)」の選択値は「(.*)」であること")]
        public void ThenAssertSelectedValueById(string valueId, string expected)
        {
            var selectElement = new SelectElement(WebDriver.FindElement(By.Id(valueId)));
            Assert.AreEqual(selectElement.SelectedOption.Text, expected);
        }

        /// <summary>
        /// ラジオボタンの選択値を検証する
        /// </summary>
        /// <param name="name">対象ラジオボタンのname属性</param>
        /// <param name="expected">期待値</param>
        [Then(@"Nameが「(.*)」のラジオボタンの選択値は「(.*)」であること")]
        public void ThenAssertCheckedValueByName(string name, string expected)
        {
            var elemment = WebDriver.FindElement(By.CssSelector("input:checked[type='radio']"));
            Assert.AreEqual(expected, elemment.GetAttribute("value"));
        }


        /// <summary>
        /// 名称からフィールドを検索し、その側の表示値を検証する
        /// </summary>
        /// <param name="valueName">入力するテキストの側のフィールド名</param>
        /// <param name="value">検証値</param>
        [Then(@"名称が「(.*)」の表示値は「(.*)」であること")]
        public void ThenAssertText(string valueName, string expected)
        {
            Assert.IsTrue(WebDriver.PageSource.Contains(valueName));
            Assert.IsTrue(WebDriver.PageSource.Contains(expected));
        }

        /// <summary>
        /// 指定内容を含むこと
        /// ※KAIZENプラットフォーム対応を入れたら画面表示のタイミングが変更されたようなので、
        ///   表示されるまで最大10秒待つように変更（元のソースはコメントアウトしている）
        /// </summary>
        /// <param name="expected">検証内容</param>
        [Then(@"内容「(.*)」を含むこと")]
        public void ThenAssertContent(string expected)
        {
            WaitUntil(() => WebDriver.PageSource.Contains(expected), CaptureWebPageToFile);
            //            StringAssert.Contains(WebDriver.Instance.PageSource, expected);
        }

        /// <summary>
        /// 指定した内容を含まないこと
        /// </summary>
        /// <param name="expected">検証内容</param>
        [Then(@"内容「(.*)」を含まないこと")]
        public void ThenAssertNotContent(string expected)
        {
            Assert.IsFalse(WebDriver.PageSource.Contains(expected));
        }

        /// <summary>
        /// 指定したIPのIP/ホスト名の内容を含むこと
        /// </summary>
        /// <param name="expected">検証内容</param>
        [Then(@"IP「(.*)」のIP/ホスト名の内容を含むこと")]
        public void ThenAssertIpOrHostName(string ipAddress)
        {
            string hostName = null;
            try
            {
                hostName = System.Net.Dns.GetHostEntry(ipAddress).HostName;
            }
            catch
            {
            }
            Assert.IsTrue(WebDriver.PageSource.Contains(string.IsNullOrEmpty(hostName) ? ipAddress : hostName));
        }

        /// <summary>
        /// valueのボタンが存在しないこと。
        /// </summary>
        /// <param name="value">ボタンのvalue</param>
        [Then(@"valueが「(.*)」のボタンが存在しないこと")]
        public void ThenNotExistsButton(string value)
        {
            try
            {
                WebDriver.FindElement(By.XPath("//Input[value='" + value + "']"));
                Assert.Fail();
            }
            catch (NotFoundException) { }

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// valueのボタンが指定した数だけ存在すること
        /// </summary>
        /// <param name="value">ボタンのvalue</param>
        [Then(@"valueが「(.*)」のボタンが「(.*)」個だけ存在すること")]
        public void ThenExistsSeveralButtons(string value, int count)
        {
            Assert.AreEqual(WebDriver.FindElements(By.LinkText(value)).Count, count);

            // エビデンスを捕獲する
            CaptureWebPageToFile();
        }

        /// <summary>
        /// ラジオボタンを検索し、選択値を検証する
        /// </summary>
        /// <param name="value">検証値</param>
        [Then(@"ラジオボタンの選択値は「(.*)」であること")]
        public void ThenAssertCheckedValueByName(string expected)
        {
            Assert.IsTrue(WebDriver.FindElement(By.XPath("//*[text()[contains(.,'" + expected + "')]]")).FindElement(By.TagName("input")).Selected);
        }

        /// <summary>
        /// valueからリンクが存在しないこと
        /// </summary>
        /// <param name="value">value</param>
        [Then(@"valueが「(.*)」のリンクが存在しないこと")]
        public void ThenNoLink(string value)
        {
            try
            {
                WebDriver.FindElement(By.LinkText(value));
                Assert.Fail();
            }
            catch (NotFoundException) { }
        }

        /// <summary>
        /// IDからリンクが存在しないこと
        /// </summary>
        /// <param name="id">ID</param>
        [Then(@"IDが「(.*)」のリンクが存在しないこと")]
        public void ThenNoLinkById(string id)
        {
            try
            {
                WebDriver.FindElement(By.XPath("A[id='" + id + "']"));
                Assert.Fail();
            }
            catch (NotFoundException) { }
        }

        /// <summary>
        /// 外部サイトのタイトルが検証文字列と同等であることを検証する。
        /// </summary>
        /// <param name="expected">検証値</param>
        [Then(@"外部サイトのタイトルが「(.*)」であること")]
        public void ThenAssertOhterSiteTitle(string expected)
        {
            Assert.AreEqual(expected, WebDriver.Title);
        }


        /// <summary>
        /// 外部サイトのタイトルが検証文字列を含んでいることを検証する。
        /// </summary>
        /// <param name="expected">検証値</param>
        [Then(@"外部サイトのタイトルが「(.*)」を含んでいること")]
        public void ThenAssertOhterSiteTitlePart(string expected)
        {
            StringAssert.Contains(WebDriver.Title, expected);
        }

        /// <summary>
        /// タグのinnertextを確認する
        /// </summary>
        /// <param name="elementId">ID</param>
        /// <param name="expected">検証値</param>
        [Then(@"IDが「(.*)」のInnerTextに「(.*)」を含むこと")]
        public void ThenIncludeInnerText(string elementId, string expected)
        {
            StringAssert.Contains(WebDriver.FindElement(By.Id(elementId)).Text, expected);
        }

        /// <summary>
        /// タグが存在しないことを確認する
        /// </summary>
        /// <param name="elementId">ID</param>
        /// <param name="expected">検証値</param>
        [Then(@"IDが「(.*)」のタグが存在しないこと")]
        public void ThenNotExistsTag(string elementId)
        {
            try
            {
                var element = WebDriver.FindElement(By.Id(elementId));
                Assert.Fail("IDに一致するタグが存在する");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(NoSuchElementException));
            }
        }

        /// <summary>
        /// デフォルトレイアウトであることを検証します。
        /// </summary>
        [Then(@"レイアウトがデフォルトであること")]
        public void ThenDefaultLayout()
        {
            {
                // favicon and css
                var targets = WebDriver.FindElements(By.XPath("/html/head/link"));
                Assert.IsTrue(targets.Any(x => x.GetAttribute("rel") == "icon" && x.GetAttribute("href").EndsWith("/Content/shared/img/carada_favicon_16.ico")));
                Assert.IsTrue(targets.Any(x => x.GetAttribute("rel") == "stylesheet" && x.GetAttribute("href").EndsWith("/Content/bootstrap.css")));
                Assert.IsTrue(targets.Any(x => x.GetAttribute("rel") == "stylesheet" && x.GetAttribute("href").EndsWith("/Content/shared/css/carada-common.css")));
            }
            {
                // logo
                var target = WebDriver.FindElements(By.CssSelector("h1 span")).SingleOrDefault();
                Assert.IsNull(target);
                target = WebDriver.FindElements(By.CssSelector("h1 img")).SingleOrDefault();
                StringAssert.EndsWith(target.GetAttribute("src"), "/Content/shared/img/carada_logo.png");
            }
            {
                // footer
                var targets = WebDriver.FindElements(By.CssSelector("footer li a"));
                Assert.IsTrue(targets.Any(x => x.Text == "運営会社" && x.GetAttribute("href").Contains("http://www.mti.co.jp/")));
                Assert.IsTrue(targets.Any(x => x.Text == "プライバシーポリシー" && x.GetAttribute("href").Contains("http://www.mti.co.jp/privacy/")));
                Assert.IsTrue(targets.Any(x => x.Text == "利用規約" && x.GetAttribute("href").Contains("https://www.carada.jp/terms/platform/")));
                Assert.IsTrue(targets.Any(x => x.Text == "お問い合わせ" && x.GetAttribute("href").Contains("http://mti7.okbiz.okwave.jp/category/show/2282")));

            }
            {
                // footer copyright
                var target = WebDriver.FindElement(By.CssSelector("footer small"));
                Assert.AreEqual<string>("©株式会社エムティーアイ", target.Text);
            }
        }

        /// <summary>
        /// FP用レイアウトであることを検証します。
        /// </summary>
        [Then(@"レイアウトがFP用であること")]
        public void ThenFeaturePhoneLayout()
        {
            // favicon and css
            var targetsCss = WebDriver.FindElements(By.XPath("/html/head/link"));
            Assert.IsTrue(targetsCss.Any(x => x.GetAttribute("rel") == "icon" && x.GetAttribute("href").EndsWith("/Content/shared/img/carada_favicon_16.ico")));
            Assert.IsFalse(targetsCss.Any(x => x.GetAttribute("rel") == "stylesheet" && x.GetAttribute("href").EndsWith("/Content/bootstrap.css")));
            Assert.IsFalse(targetsCss.Any(x => x.GetAttribute("rel") == "stylesheet" && x.GetAttribute("href").EndsWith("/Content/shared/css/carada-common.css")));

            // logo
            var targetsLogo = WebDriver.FindElements(By.XPath("/html/body/div/img"));
            Assert.IsTrue(targetsLogo.Any(x => x.GetAttribute("alt") == "CARADA header" && x.GetAttribute("src").EndsWith("/Content/shared/img/logo_100.gif")));

            // footer copyright
            var targetsFooterLink = WebDriver.FindElements(By.XPath("/html/body/div/div/a"));
            Assert.IsTrue(targetsFooterLink.Any(x => x.GetAttribute("href").EndsWith("http://mobile.mti.co.jp/")));
            Assert.AreEqual<string>("株式会社ｴﾑﾃｨｰｱｲ", targetsFooterLink[0].Text);

            var targetsFooter = WebDriver.FindElements(By.XPath("/html/body/div/div/img"));
            Assert.IsTrue(targetsFooter.Any(x => x.GetAttribute("alt") == "CARADA Footer" && x.GetAttribute("src").EndsWith("/Content/shared/img/logo_100_w.gif")));

            // JavaScript
            var targetsHeadJs = WebDriver.FindElements(By.XPath("/html/head/script"));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/jquery-2.1.3.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/jquery.matchHeight-min.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/jquery.blockUI.js")));

            var targetsBodyJs = WebDriver.FindElements(By.XPath("/html/body/script"));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/jquery.validate.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/jquery.validate.unobtrusive.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/bootstrap.min.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/carada-common.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/caradaidp.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/ie-emulation-modes-warning.js")));
            Assert.IsFalse(targetsHeadJs.Any(x => x.GetAttribute("src").EndsWith("/Scripts/ie10-viewport-bug-workaround.js")));
        }

        /// <summary>
        /// 指定したタグ名・属性名・属性値を持つタグのInnerTextを検証する
        /// </summary>
        /// <param name="tagname">タグ名</param>
        /// <param name="attr">属性名</param>
        /// <param name="attrVal">属性値</param>
        /// <param name="expected">InnerTextの期待値</param>
        [Then(@"タグ名「(.*)」属性名「(.*)」属性値「(.*)」のテキストが「(.*)」であること")]
        public void ThenCheckLabelInnerText(string tagname, string attr, string attrVal, string expected)
        {
            var targets = WebDriver.FindElements(By.CssSelector(tagname));
            Assert.IsTrue(targets.Count(x => x.Text == expected && x.GetAttribute(attr).Equals(attrVal)) == 1);
        }

        /// <summary>
        /// ユーザーテーブルデータを検索し、確認する。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">SpecFlow表</param>
        [Then(@"ユーザーDBの「(.*)」テーブルに次のデータが存在すること")]
        public void ThenUserDbAssertData(string tableName, TechTalk.SpecFlow.Table table)
        {
            DbMatch(tableName, table, userDb);
        }

        /// <summary>
        /// ユーザーテーブルデータを検索し、結果がないことを確認する。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">SpecFlow表</param>
        [Then(@"ユーザーDBの「(.*)」テーブルに次のデータが存在しないこと")]
        public void ThenUserDbAssertFalseData(string tableName, TechTalk.SpecFlow.Table table)
        {
            DbMatch(tableName, table, userDb, false);
        }

        /// <summary>
        /// DBのAspNetUserRolesテーブルに登録されているロールが正しいことの検証
        /// </summary>
        [Then("CARADA_ID「(.*)」のAspNetUserRolesのロール名が「(.*)」であること")]
        public void ThenDbRolesIsValid(string userName, string roleName)
        {

            var actual = string.Empty;
            try
            {
                userDb.Database.Connection.Open();
                using (var cmd = userDb.Database.Connection.CreateCommand())
                {
                    var sql = "SELECT AspNetRoles.Name as RoleName From AspNetRoles INNER JOIN AspNetUserRoles ON AspNetUserRoles.RoleId = AspNetRoles.Id INNER JOIN AspNetUsers ON AspNetUserRoles.UserId = AspNetUsers.Id WHERE AspNetUsers.UserName = '" + userName + "'";
                    cmd.CommandText = sql;

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            actual = reader.GetValue(0).ToString();
                        }
                    }
                }
            }
            finally
            {
                userDb.Database.Connection.Close();
            }

            Assert.AreEqual(roleName, actual);
        }

        /// <summary>
        /// Cookieの存在チェックを行う。
        /// </summary>
        /// <param name="cookieName">Cookie名</param>
        [Then(@"Cookie名が「(.*)」のCookieが存在すること")]
        public void ThenAssertCookie(string cookieName)
        {
            Assert.IsNotNull(GetCookie(cookieName));
        }

        /// <summary>
        /// Cookieの存在チェックを行い、有効期限が設定されていることを確認する。
        /// </summary>
        /// <param name="cookieName">Cookie名</param>
        [Then(@"Cookie名が「(.*)」の有効期限が設定されたCookieが存在すること")]
        public void ThenAssertExpiryCookie(string cookieName)
        {
            OpenQA.Selenium.Cookie cookie = GetCookie(cookieName);
            Assert.IsNotNull(cookie);
            Assert.IsNotNull(cookie.Expiry);
        }

        /// <summary>
        /// Cookieの存在チェックを行い、有効期限が設定されていないことを確認する。
        /// </summary>
        /// <param name="cookieName">Cookie名</param>
        [Then(@"Cookie名が「(.*)」の有効期限が設定されていないCookieが存在すること")]
        public void ThenAssertNonExpiryCookie(string cookieName)
        {
            OpenQA.Selenium.Cookie cookie = GetCookie(cookieName);
            Assert.IsNotNull(cookie);
            Assert.IsNull(cookie.Expiry);
        }

        /// <summary>
        /// Cookieの存在チェックを行い、有効期限が正しい設定であることを確認する。
        /// </summary>
        /// <param name="cookieName">Cookie名</param>
        /// <param name="expireDate">有効期限（日）</param>
        [Then(@"Cookie名「(.*)」の有効期限が現在から「(.*)」日に設定されていること")]
        public void ThenAssertExpiryDaysFromNowInCookie(string cookieName, string expireDays)
        {
            OpenQA.Selenium.Cookie cookie = GetCookie(cookieName);
            Assert.IsNotNull(cookie);

            double addDays = 0.0;
            if (!double.TryParse(expireDays, out addDays))
            {
                throw new SpecFlowException("有効期限を半角数字で指定してください");
            }
            Assert.IsNotNull(cookie.Expiry);

            var ts = cookie.Expiry.GetValueOrDefault() - DateTime.UtcNow;
            Assert.AreEqual(ts.Days, TimeSpan.FromDays(addDays).Days);
        }

        [Then(@"Cookie名が「(.*)」のCookieが存在しないこと")]
        public void ThenNotExistsCookie(string cookieName)
        {
            OpenQA.Selenium.Cookie cookie = GetCookie(cookieName);
            Assert.IsNull(cookie);
        }

        /// <summary>
        /// 仮想URLを確認する
        /// </summary>
        /// <param name="expected">期待値</param>
        [Then(@"仮想URLが「(.*)」であること")]
        public void ThenAssertVirtualUrl(string expected)
        {
            // 仮想パスの値がわかればいいので、ga関数の使い方があっているかどうかは判断しない
            var m = Regex.Match(WebDriver.PageSource, @"ga[ \t\n]*\(.*['""]page['""][ \t\n]*:[ \t\n]*['""](.*)['""]", RegexOptions.Multiline);
            Assert.IsTrue(m.Success && m.Groups.Count != 0 && string.Equals(m.Groups[1].Value, expected, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// GAにセットしたフィールドを確認する
        /// </summary>
        /// <param name="field">フィールド名</param>
        /// <param name="expected">期待値</param>
        [Then(@"GoogleAnalyticsのフィールド「(.*)」が「(.*)」であること")]
        public void ThenAssertGAField(string field, string expected)
        {
            var js = (IJavaScriptExecutor)WebDriver;
            var actual = js.ExecuteScript("return ga.getAll()[0].get('" + field + "');").ToString();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// UserInsightにセットしたフィールドを確認する
        /// </summary>
        /// <param name="field">フィールド名</param>
        /// <param name="expected">期待値</param>
        [Then(@"UserInsightのフィールド「(.*)」が「(.*)」であること")]
        public void ThenAssertUserInsightField(string field, string expected)
        {
            var js = (IJavaScriptExecutor)WebDriver;
            var actual = js.ExecuteScript("return _uih['" + field + "'];").ToString();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// UserInsightにセットしたフィールドを確認する
        /// </summary>
        /// <remarks>
        /// UserInsightではURLはhttp:から始まらなければならないが、
        /// ケース記述時の統一性から、ドキュメントルート相対パスでも受け付ける。
        /// どちらにしろ、一致確認はhttp開始のフルパスで行う。
        /// </remarks>
        /// <param name="field">フィールド名</param>
        /// <param name="expected">
        /// 期待値(httpから始まる絶対URL、ドキュメントルートからの相対パス両対応)
        /// </param>
        [Then(@"UserInsightの仮想URLが「(.*)」であり、他のパラメータも正しく設定されていること")]
        public void ThenAssertUserInsight(string expected)
        {
            // lg_id, fb_id, tw_id は空であること
            var keysOfEmptyValue = new string[] { "lg_id", "fb_id", "tw_id" };
            foreach (var key in keysOfEmptyValue)
            {
                ThenAssertUserInsightField(key, "");
            }
            // uigr_1 から uigr_10 までは空であること
            for (var i = 1; i <= 10; i++)
            {
                var key = "uigr_" + i.ToString();
                ThenAssertUserInsightField(key, "");
            }
            // 確認したいURLが相対パスだった場合、フルパスにしてから比較する
            if (!expected.StartsWith("http"))
            {
                expected = ConvertAbsoluteUrlToRequestUrl(expected);
            }
            ThenAssertUserInsightField("url", expected);
            ThenAssertUserInsightField("id", "51935"); // 固定値
        }

        /// <summary>
        /// 送信されたメールのタイトルを確認する
        /// </summary>
        /// <param name="subject">メールタイトル</param>
        [Then(@"最後に送信されたメールタイトルが「(.*)」であること")]
        public void ThenAssertMailSubject(string subject)
        {
            var mail = GetLastMail();
            Assert.IsNotNull(mail);
            Assert.AreEqual(subject, mail.Subject);
        }

        /// <summary>
        /// 送信されたメールのFromを確認する
        /// </summary>
        /// <param name="from">From</param>
        [Then(@"最後に送信されたメールのFromが「(.*)」であること")]
        public void ThenAssertMailFrom(string from)
        {
            var mail = GetLastMail();
            Assert.IsNotNull(mail);
            Assert.AreEqual(from, mail.From.AsString);
        }

        /// <summary>
        /// 送信されたメールのあて先を確認する
        /// </summary>
        /// <param name="to">カンマ区切りのあて先</param>
        [Then(@"最後に送信されたメールのあて先が「(.*)」の組であること")]
        public void ThenAssertMailTo(string to)
        {
            var mail = GetLastMail();
            Assert.IsNotNull(mail);

            foreach (var t in to.Split(','))
            {
                Assert.IsNotNull((from MailBee.Mime.EmailAddress x in mail.To where x.AsString == t select x).First());
            }

            Assert.AreEqual(to.Split(',').Count(), mail.To.Count);
        }

        /// <summary>
        /// 送信されたメールのCCを確認する
        /// </summary>
        /// <param name="cc">カンマ区切りのCC</param>
        [Then(@"最後に送信されたメールのCCが「(.*)」の組であること")]
        public void ThenAssertMailCc(string cc)
        {
            var mail = GetLastMail();
            Assert.IsNotNull(mail);

            foreach (var c in cc.Split(','))
            {
                Assert.IsNotNull((from MailBee.Mime.EmailAddress x in mail.Cc where x.AsString == c select x).First());
            }

            Assert.AreEqual(cc.Split(',').Count(), mail.Cc.Count);
        }

        /// <summary>
        /// 送信されたメールのBCCを確認する
        /// </summary>
        /// <param name="bcc">カンマ区切りのBCC</param>
        [Then(@"最後に送信されたメールのBCCが「(.*)」の組であること")]
        public void ThenAssertMailBcc(string bcc)
        {
            var mail = GetLastMail();
            Assert.IsNotNull(mail);

            foreach (var b in bcc.Split(','))
            {
                Assert.IsNotNull((from MailBee.Mime.EmailAddress x in mail.Bcc where x.AsString == b select x).First());
            }

            Assert.AreEqual(bcc.Split(',').Count(), mail.Bcc.Count);
        }

        /// <summary>
        /// 送信されたメールの本文を確認する
        /// </summary>
        /// <param name="pattern">正規表現</param>
        [Then(@"最後に送信されたメールの本文に「(.*)」の正規表現がマッチすること")]
        public void ThenMatchRegexMailBody(string pattern)
        {
            var mail = GetLastMail();
            Assert.IsNotNull(mail);

            Assert.IsTrue(Regex.IsMatch(mail.BodyPlainText, pattern), "pattern 「" + pattern + "」is not match");
        }

        /// <summary>
        /// 最後に送信されたメールN件のどれかに宛先とタイトルが含まれることを確認する。
        /// or条件のため、このケースを利用する場合、N件分の確認を全部すること。
        /// </summary>
        /// <param name="to">あて先（カンマ区切りで複数設定可能）</param>
        /// <param name="subject">メールタイトル</param>
        [Then(@"最後に送信された「(.*)」件のメールのどれかのあて先が「(.*)」の組でありタイトルが「(.*)」であること")]
        public void ThenAssertLastTwoMailToAndSubject(int mailCount, string to, string subject)
        {
            var mails = GetLastTwoMail(mailCount);
            Assert.IsNotNull(mails, "The number of Email does not match");
            var count = 0;
            foreach (var mail in mails)
            {
                foreach (var t in to.Split(','))
                {
                    if ((from MailBee.Mime.EmailAddress x in mail.To where x.AsString == t select x).FirstOrDefault() != null)
                    {
                        count++;
                    }
                }
                if (count > 0)
                {
                    // 宛先の確認
                    Assert.AreEqual(to.Split(',').Count(), mail.To.Count);
                    Assert.AreEqual(to.Split(',').Count(), count);
                    // 件名の確認
                    Assert.AreEqual(subject, mail.Subject);
                    return;
                }
            }
            Assert.Fail("Email does not match");
        }

        /// <summary>
        /// 最後に送信されたメールN件のどれかに宛先と本文が含まれることを確認する。
        /// or条件のため、このケースを利用する場合、N件分の確認を全部すること。
        /// </summary>
        /// <param name="to">カンマ区切りのあて先</param>
        /// <param name="pattern">正規表現</param>
        [Then(@"最後に送信された「(.*)」件のメールのどれかのあて先が「(.*)」の組であり本文に「(.*)」の正規表現がマッチすること")]
        public void ThenAssertLastTwoMailToAndMailBody(int mailCount, string to, string pattern)
        {
            var mails = GetLastTwoMail(mailCount);
            Assert.IsNotNull(mails, "The number of Email does not match");
            var count = 0;
            foreach (var mail in mails)
            {
                foreach (var t in to.Split(','))
                {
                    if ((from MailBee.Mime.EmailAddress x in mail.To where x.AsString == t select x).FirstOrDefault() != null)
                    {
                        count++;
                    }
                }
                if (count > 0)
                {
                    // 宛先の確認
                    Assert.AreEqual(to.Split(',').Count(), mail.To.Count);
                    Assert.AreEqual(to.Split(',').Count(), count);
                    // 本文の確認
                    Assert.IsTrue(Regex.IsMatch(mail.BodyPlainText, pattern), "pattern 「" + pattern + "」is not match");
                    return;
                }
            }
            Assert.Fail("Email does not match");
        }

        /// <summary>
        /// 送信されたメールの本文にお問い合わせ先文言が含まれることを確認する。
        /// </summary>
        [Then(@"最後に送信されたメールの本文にお問い合わせ先文言が含まれること")]
        public void ThenAssertLastMailIncludeSupportText()
        {
            Then(@"最後に送信されたメールの本文に「" + SupportText + "」の正規表現がマッチすること");
        }

        /// <summary>
        /// 最後に送信されたメールN件のどれかに宛先とお問い合わせ先文言が含まれることを確認する。
        /// or条件のため、このケースを利用する場合、N件分の確認を全部すること。
        /// </summary>
        /// <param name="mailCount">最後に送信されたメールの件数</param>
        /// <param name="to">カンマ区切りのあて先</param>
        [Then(@"最後に送信された「(.*)」件のメールのどれかのあて先が「(.*)」の組であり本文にお問い合わせ先文言が含まれること")]
        public void ThenAssertLastMailsIncludeSupportText(int mailCount, string to)
        {
            Then(@"最後に送信された「" + mailCount + "」件のメールのどれかのあて先が「" + to + "」の組であり本文に「" + SupportText + "」の正規表現がマッチすること");
        }

        /// <summary>
        /// ログレベルごとの最新から数えた対象出力回に期待値が出力されていること。
        /// 改行は無視する。
        /// </summary>
        /// <param name="fileName">ファイル名："Access"か"Error"</param>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="numberofwrote">最後から数えた出力回数</param>
        /// <param name="target">期待値</param>
        [Then(@"「(.*)」ログのログレベル「(.*)」の最後から「([0-9]+)」回目の出力に「(.*)」を含むこと")]
        public void ThenApplicationLogWritten(string fileName, string loglevel, int numberofwrote, string target)
        {
            var line = GetTargetLogLine(fileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log", loglevel, numberofwrote);

            Assert.IsTrue(line.Contains(ConvertCurrentData(target)), "【" + line + "】に「" + ConvertCurrentData(target) + "」を含みません。");
        }

        /// <summary>
        /// セレクトボックスの表示順を確認する。
        /// </summary>
        /// <param name="id">ID</param>
        /// <param name="listNameText">カンマ区切りの表示順の文字列</param>
        [Then("IDが「(.*)」のセレクトボックスの順番が「(.*)」通りであること")]
        public void ThenVerifySelectBoxListOrder(string id, string listText)
        {
            string[] names = listText.Split(',');
            var providers = new List<string>();
            providers.AddRange(names);
            var target = WebDriver.FindElement(By.Id(id));
            var targets = target.FindElements(By.TagName("option"));
            Assert.AreEqual(providers.Count, targets.Count);
            for (var i = 0; i < providers.Count; i++)
            {
                Assert.AreEqual(providers[i], targets[i].Text);
            }
        }

        /// <summary>
        /// シナリオ変数に保存された認証コードが入力値と合致するか検証する。
        /// </summary>
        /// <param name="elementId">要素ID</param>
        [Then(@"IDが「(.*)」の入力値が最後に送信されたメールから取得した認証コードであること")]
        public void ThenInputEqualVerifyCode(string elementId)
        {
            if (elementId == "")
            {
                throw new SpecFlowException("elementIdを指定してください");
            }
            var code = ReceiveCurrentData(elementId) as string;
            Then(@"IDが「" + elementId + "」の入力値は「" + code + "」であること");
        }

        #endregion

        #region ヘッダー

        /// <summary>
        /// valueからヘッダーリンクがある
        /// </summary>
        /// <param name="value">value</param>
        [Then(@"valueが「(.*)」のヘッダーリンクがある")]
        public void ThenCheckHeaderLink(string value)
        {
            Assert.IsNotNull(WebDriver.FindElement(By.XPath("//Div[@class='tmpl__header-maininset']"))
                                               .FindElement(By.LinkText(value)));
        }

        /// <summary>
        /// ストレージのデータを検証する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">table</param>
        [Then(@"ストレージの「(.*)」に次のデータが存在すること")]
        public void ThenStorageExpected(string tableName, TechTalk.SpecFlow.Table table)
        {
            var cloudTable = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudTableClient().GetTableReference(tableName);

            var entities = CreateDynamicSet(table);
            foreach (IDictionary<string, object> entity in entities)
            {
                var partitionKey = entity["PartitionKey"].ToString();
                var rowKey = entity["RowKey"].ToString();
                var saved = cloudTable.ExecuteQuery(new TableQuery().Where(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey),
                        TableOperators.And,
                        TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, rowKey)
                    )
                )).SingleOrDefault();
                if (saved == null)
                {
                    throw new SpecFlowException(string.Format("該当するエンティティがありません。PartitionKey={0}, RowKey={1}", partitionKey, rowKey));
                }

                foreach (var keyValue in entity)
                {
                    var savedValue = string.Empty;
                    if (keyValue.Key == "PartitionKey" || keyValue.Key == "RowKey")
                    {
                        continue;
                    }
                    else if (keyValue.Key == "Timestamp")
                    {
                        savedValue = saved.Timestamp.ToString();
                    }
                    else if (keyValue.Key == "ETag")
                    {
                        savedValue = saved.ETag;
                    }
                    else if (saved.Properties.ContainsKey(keyValue.Key))
                    {
                        savedValue = ConvertEntityPropertyToString(saved.Properties[keyValue.Key]);
                    }
                    else
                    {
                        throw new SpecFlowException(string.Format("ストレージから取得したエンティティにキー「{0}」は含まれていません。", keyValue.Key));
                    }

                    Assert.AreEqual<string>(keyValue.Value.ToString(), savedValue);
                }
            }
        }

        /// <summary>
        /// EntityPropertyをstringに変換します。
        /// </summary>
        /// <param name="property">EntityProperty</param>
        /// <returns>文字列表現</returns>
        private string ConvertEntityPropertyToString(EntityProperty property)
        {
            if (property.PropertyType == EdmType.Binary)
            {
                return property.BinaryValue.ToString();
            }
            else if (property.PropertyType == EdmType.Boolean)
            {
                return property.BooleanValue.ToString();
            }
            else if (property.PropertyType == EdmType.DateTime)
            {
                return property.DateTimeOffsetValue.ToString();
            }
            else if (property.PropertyType == EdmType.Double)
            {
                return property.DoubleValue.ToString();
            }
            else if (property.PropertyType == EdmType.Guid)
            {
                return property.GuidValue.ToString();
            }
            else if (property.PropertyType == EdmType.Int32)
            {
                return property.Int32Value.ToString();
            }
            else if (property.PropertyType == EdmType.Int64)
            {
                return property.Int64Value.ToString();
            }
            else if (property.PropertyType == EdmType.String)
            {
                return property.StringValue;
            }
            else if (property.PropertyAsObject != null)
            {
                return property.PropertyAsObject.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// ストレージのデータを検証する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">table</param>
        [Then(@"ストレージの「(.*)」に次のデータが存在しないこと")]
        public void ThenStorageNotExpected(string tableName, TechTalk.SpecFlow.Table table)
        {
            var cloudTable = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudTableClient().GetTableReference(tableName);

            var currentRow = 0;

            foreach (var row in table.Rows)
            {
                currentRow++;
                var partitionKey = row["PartitionKey"];
                var rowKey = row["RowKey"];

                var values = from n in row
                             where n.Key != "PartitionKey" && n.Key != "RowKey"
                             select n;

                var result = cloudTable.Execute(TableOperation.Retrieve(partitionKey, rowKey));

                Assert.IsNull(result.Result, "{0}行目の条件にマッチする{1}のデータが存在します。", new[] { currentRow.ToString(), tableName });
            }
        }

        /// <summary>
        /// formの属性とその値を検証する。
        /// ※formが複数ある場合には対応していません。
        /// </summary>
        /// <param name="attributeName">属性名</param>
        /// <param name="attributeValue">属性値</param>
        [Then(@"formの「(.*)」属性の値が「(.*)」であること")]
        public void ThenFromAttributeExpected(string attributeName, string attributeValue)
        {
            Assert.IsNotNull(WebDriver.FindElement(By.XPath("//form[@" + attributeName + "='" + attributeValue + "']")));
        }

        /// <summary>
        /// formの属性がないことを検証する。
        /// ※formが複数ある場合には対応していません。
        /// </summary>
        /// <param name="attributeName">属性名</param>
        [Then(@"formの「(.*)」属性が存在しないこと")]
        public void ThenFromAttributeNotExpected(string attributeName)
        {
            try
            {
                WebDriver.FindElement(By.XPath("//form[@" + attributeName + "]"));
                Assert.Fail();
            }
            catch (NoSuchElementException)
            {
            }
        }

        /// <summary>
        ///  対象のタグの属性の名前と値を検証する
        /// </summary>
        /// <param name="name">タグのname</param>
        /// <param name="attributeName">属性の名前</param>
        /// <param name="attirbuteValue">属性の値</param>
        [Then(@"nameが「(.*)」のタグの「(.*)」という属性の値が「(.*)」であること")]
        public void ThenNameTagAttributeExpected(string name, string attributeName, string attributeValue)
        {
            Assert.AreEqual(attributeValue, WebDriver.FindElement(By.Name(name)).GetAttribute(attributeName));
        }

        #endregion

        #region ナビゲーション

        /// <summary>
        /// ナビゲーションはないであること
        /// </summary>
        [Then(@"ナビゲーションはないであること")]
        private void ThenNoNavigation()
        {
            try
            {
                WebDriver.FindElement(By.ClassName("tmpl__header-subinset02"));
                Assert.Fail();
            }
            catch (NotFoundException)
            {

            }
        }

        /// <summary>
        /// valueからナビゲーションリンクがある
        /// </summary>
        /// <param name="value">value</param>
        [Then(@"valueが「(.*)」のナビゲーションリンクがある")]
        public void ThenCheckNavigationLink(string value)
        {
            Assert.IsNotNull(WebDriver.FindElement(By.XPath("//Div[@class='tmpl__header-subinset02']"))
                                               .FindElement(By.LinkText(value)));
        }

        #endregion

        #region フッター

        /// <summary>
        /// valueからフッターリンクがある
        /// </summary>
        /// <param name="value">value</param>
        [Then(@"valueが「(.*)」のフッターリンクがある")]
        public void ThenCheckFooterLink(string value)
        {
            Assert.IsNotNull(WebDriver.FindElement(By.XPath("//Div[@class='tmpl__footer-inset']"))
                                               .FindElement(By.LinkText(value)));
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// 抽象URLからリクエストURLに変換する。
        /// </summary>
        /// <param name="absoluteUrl">抽象URL</param>
        /// <returns>リクエストURL</returns>
        private string ConvertAbsoluteUrlToRequestUrl(string absoluteUrl)
        {
            return System.Configuration.ConfigurationManager.AppSettings["Test.Url.Base"] + absoluteUrl;
        }

        /// <summary>
        /// エビデンスを捕獲する
        /// </summary>
        private void CaptureWebPageToFile()
        {
            // フォルダーを取得する
            var rootPath = Path.Combine(Settings.Default.CapturePath, FeatureContext.Current.FeatureInfo.Title, ScenarioContext.Current.ScenarioInfo.Title, DateTime.Now.ToString("yyyyMMdd_HH"));
            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }

            // 画像ファイル名
            var fileName = Path.Combine(rootPath, DateTime.Now.ToString("mmss_fff") + ".png");
            var now = DateTime.Now;
            while (File.Exists(fileName))
            {
                fileName = Path.Combine(rootPath, DateTime.Now.ToString("mmss_fff") + ".png");
                if (DateTime.Now - now >= TimeSpan.FromSeconds(10))
                {
                    throw new SpecFlowException("キャプチャファイルのファイル名生成に10秒以上かかりました。");
                }
            }
            // ページイメージをファイルに保存する
            now = DateTime.Now;
            Screenshot shot = null;
            do
            {
                shot = ((ITakesScreenshot)WebDriver).GetScreenshot();
                if (DateTime.Now - now >= TimeSpan.FromSeconds(10))
                {
                    throw new SpecFlowException("キャプチャに10秒以上かかりました。");
                }
            } while (shot.AsByteArray.Length == 0);

            // URLの追加
            using (var stream = new MemoryStream(shot.AsByteArray))
            {
                using (var image = Image.FromStream(stream, true))
                {
                    using (var graphics = Graphics.FromImage(image))
                    {
                        using (var font = new Font("ＭＳ ゴシック", 9))
                        {
                            var size = graphics.MeasureString(WebDriver.Url, font, image.Width);
                            var rect = new RectangleF(0, 0, size.Width, size.Height);

                            // 高さは1px余計に取る
                            using (var destImage = new Bitmap(image.Width, (int)size.Height + image.Height + 1))
                            {
                                using (var destGraphics = Graphics.FromImage(destImage))
                                {
                                    destGraphics.DrawImage(image, 0, size.Height);
                                    destGraphics.DrawString(WebDriver.Url, font, Brushes.Black, rect, StringFormat.GenericTypographic);
                                }
                                destImage.Save(fileName, ImageFormat.Png);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// DBエビデンスを取得する
        /// </summary>
        /// <param name="dump">ダンプ行</param>
        private void DumpData(DumpRows dump)
        {
            // フォルダーを取得する
            var rootPath = Path.Combine(Settings.Default.DumpPath, FeatureContext.Current.FeatureInfo.Title, ScenarioContext.Current.ScenarioInfo.Title, DateTime.Now.ToString("yyyyMMdd_HH"));
            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }

            // テキストファイル名
            var fileName = Path.Combine(rootPath, dump.TableName + "_" + DateTime.Now.ToString("mmss_fff") + ".txt");

            using (var f = File.OpenWrite(fileName))
            {
                using (var s = new StreamWriter(f, Encoding.UTF8))
                {
                    var len = 0;
                    foreach (var header in dump.Headers)
                    {
                        s.Write("|" + header.Key.PadRight(header.Value));
                        len += header.Value;
                    }
                    s.WriteLine("|");
                    s.WriteLine("".PadRight(len + dump.Headers.Count, '-') + "-");
                    foreach (var row in dump.DataRows)
                    {
                        foreach (var column in row)
                        {
                            s.Write("|" + column.Value.PadRight(dump.Headers[column.Key]));
                        }
                        s.WriteLine("|");
                    }
                }
            }
        }


        /// <summary>
        /// アクティビティ時間チェック
        /// </summary>
        /// <param name="time">時間</param>
        /// <returns>結果</returns>
        private bool CheckActivityDateTime(DateTime time)
        {
            var time1 = time.AddMinutes(-1);
            return WebDriver.PageSource.Contains(time1.ToString("yyyy年MM月dd日 HH:mm"))
                || WebDriver.PageSource.Contains(time1.ToString("HH:mm"))
                || WebDriver.PageSource.Contains(time.ToString("yyyy年MM月dd日 HH:mm"))
                || WebDriver.PageSource.Contains(time.ToString("HH:mm"));
        }

        /// <summary>
        /// 現在時刻より前の時間をCurrentDataに設定する
        /// </summary>
        /// <param name="minutes">現在時刻より前</param>
        [Given(@"現在時刻から「(.*)」分後の時間をCurrentDataにEpochとして設定する")]
        public void GivenSetEpochBeforeMinutes(string minutes)
        {
            var addMinutes = 0.0;
            if (!double.TryParse(minutes, out addMinutes))
            {
                throw new SpecFlowException("期間は半角数字で指定してください");
            }
            SaveCurrentData("Epoch", DateTime.UtcNow.AddMinutes(addMinutes).ToString("yyyy-MM-dd HH:mm:ss"));
        }

        /// <summary>
        /// DBデータを削除する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="context">DbContext</param>
        /// <param name="reseed">IDENT列を初期値にする場合はtrue</param>
        private void DbDelete(string tableName, DbContext context, bool reseed = false)
        {
            var sql = "DELETE FROM " + tableName + " ";
            if (reseed)
            {
                sql = "DBCC CHECKIDENT (" + tableName + ", RESEED, 0) " + sql;
            }
            context.Database.ExecuteSqlCommand(sql);
        }

        /// <summary>
        /// DBデータを投入する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">featureファイル中の表</param>
        /// <param name="context">DbContext</param>
        private void DbInsert(string tableName, TechTalk.SpecFlow.Table table, DbContext context)
        {
            DbInsert(tableName, table, context, false);
        }

        /// <summary>
        /// SET IDENTITY_INSERTを使用するか選んでDBデータを投入する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">featureファイル中の表</param>
        /// <param name="context">DbContext</param>
        /// <param name="isIdentity">SET IDENTITY_INSERTを使用するかどうか</param>
        private void DbInsert(string tableName, TechTalk.SpecFlow.Table table, DbContext context, bool isIdentity)
        {
            var entities = CreateDynamicSet(table);

            foreach (var entity in entities)
            {
                var sql = "insert into dbo." + tableName + " (:columns) values (:bindcolumns)";
                var columns = new List<string>();
                var parameters = new List<SqlParameter>();
                foreach (var s in entity as IDictionary<string, object>)
                {
                    columns.Add(s.Key);
                    if (s.Value.ToString() == "<NULL>")
                    {
                        parameters.Add(new SqlParameter("@" + s.Key, SqlDbType.NVarChar) { Value = DBNull.Value });
                    }
                    else
                    {
                        parameters.Add(new SqlParameter("@" + s.Key, SqlDbType.NVarChar) { Value = s.Value.ToString() });
                    }
                }

                var columnstr = "";
                var bindstr = "";
                for (var i = 0; i < columns.Count; i++)
                {
                    if (i == 0)
                    {
                        columnstr = columns[i];
                        bindstr = "@" + columns[i];
                    }
                    else
                    {
                        columnstr += "," + columns[i];
                        bindstr += ",@" + columns[i];
                    }
                }
                sql = sql.Replace(":columns", columnstr).Replace(":bindcolumns", bindstr);
                if (isIdentity)
                {
                    sql = "SET IDENTITY_INSERT " + tableName + " ON " + sql;
                }
                context.Database.ExecuteSqlCommand(sql, parameters.ToArray());
            }
        }

        /// <summary>
        /// DB検索を行い、取得できるかAssertを行う。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="table">featureファイル中の表</param>
        /// <param name="context">DbContext</param>
        /// <param name="isMatch">true：検索結果ありならOK, false：検索結果なしならOK</param>
        private void DbMatch(string tableName, TechTalk.SpecFlow.Table table, DbContext context, bool isMatch = true)
        {
            var entities = CreateDynamicSet(table);

            try
            {
                context.Database.Connection.Open();
                var currentRow = 0;
                foreach (var entity in entities)
                {
                    currentRow++;
                    var sql = "select * from " + tableName + " ";

                    var columns = new List<string>();
                    var parameters = new List<SqlParameter>();
                    var sqlWhere = "";
                    foreach (var s in entity as IDictionary<string, object>)
                    {

                        if (sqlWhere == "")
                        {
                            sqlWhere += "WHERE ";
                        }
                        else
                        {
                            sqlWhere += "AND ";
                        }

                        if (s.Value.ToString() == "<NULL>")
                        {
                            sqlWhere += s.Key + " IS NULL ";
                        }
                        else if (s.Value.ToString() == "<!NULL>")
                        {
                            sqlWhere += s.Key + " IS NOT NULL ";
                        }
                        else
                        {
                            sqlWhere += s.Key + " = @" + s.Key + " ";
                            parameters.Add(new SqlParameter("@" + s.Key, SqlDbType.NVarChar) { Value = s.Value.ToString() });
                        }
                    }

                    using (var cmd = context.Database.Connection.CreateCommand())
                    {
                        cmd.CommandText = sql + sqlWhere;
                        cmd.Parameters.AddRange(parameters.ToArray());

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (isMatch)
                            {
                                Assert.IsTrue(reader.HasRows, "{0}行目の条件にマッチする{1}のデータがありません。", new[] { currentRow.ToString(), tableName });
                            }
                            else
                            {
                                Assert.IsFalse(reader.HasRows, "{1}のデータが{0}行目の条件にマッチしています。", new[] { currentRow.ToString(), tableName });
                            }
                        }
                    }
                }
            }
            finally
            {
                context.Database.Connection.Close();
            }
        }

        /// <summary>
        /// テーブルを全検索する
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="context">DbContext</param>
        /// <returns>ダンプ行</returns>
        private DumpRows DbAllSelect(string tableName, DbContext context)
        {
            var ret = new DumpRows() { TableName = tableName };

            try
            {
                context.Database.Connection.Open();

                var sql = "select * from " + tableName;

                using (var cmd = context.Database.Connection.CreateCommand())
                {
                    cmd.CommandText = sql;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var headers = new Dictionary<string, int>();

                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            headers.Add(reader.GetName(i), reader.GetName(i).Length);
                        }

                        var rows = new List<Dictionary<string, string>>();

                        while (reader.Read())
                        {
                            var row = new Dictionary<string, string>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                var o = reader.GetValue(i);
                                if (o == DBNull.Value)
                                {
                                    o = "<NULL>";
                                }
                                var value = o.ToString();

                                if (value.Length > headers[reader.GetName(i)])
                                {
                                    headers[reader.GetName(i)] = value.Length;
                                }

                                row.Add(reader.GetName(i), value);
                            }
                            rows.Add(row);
                        }

                        ret.Headers = headers;
                        ret.DataRows = rows;
                    }

                }
                return ret;

            }
            finally
            {
                context.Database.Connection.Close();
            }
        }

        /// <summary>
        /// Cookieを取得する
        /// </summary>
        /// <param name="cookieName">Cookie名</param>
        /// <returns>Cookie</returns>
        private OpenQA.Selenium.Cookie GetCookie(string cookieName)
        {
            return WebDriver.Manage().Cookies.GetCookieNamed(cookieName);
        }

        /// <summary>
        /// 最後に送信されたメールを取得する
        /// </summary>
        /// <returns>メールメッセージ 存在しない場合はnull</returns>
        private MailBee.Mime.MailMessage GetLastMail()
        {
            var file = (from f in Directory.EnumerateFiles(TestHelper.TestConfiguration.TEST_MAILS_PATH, "*.eml")
                        orderby File.GetCreationTime(f) descending
                        select f).First();

            if (file == null)
            {
                return null;
            }

            var ret = new MailBee.Mime.MailMessage();
            ret.LoadMessage(file);
            return ret;
        }

        /// <summary>
        /// 最後のN件の送信されたメールを取得する
        /// </summary>
        /// <param name="count">件数</param>
        /// <returns>メールメッセージ 存在しない場合かN件未満の場合はnull</returns>
        private List<MailBee.Mime.MailMessage> GetLastTwoMail(int count)
        {
            var files = (from f in Directory.EnumerateFiles(TestHelper.TestConfiguration.TEST_MAILS_PATH, "*.eml")
                         orderby File.GetCreationTime(f) descending
                         select f).Take(count);

            if (files == null || files.Count() != count)
            {
                return null;
            }

            var list = new List<MailBee.Mime.MailMessage>();
            foreach (var file in files)
            {
                var message = new MailBee.Mime.MailMessage();
                message.LoadMessage(file);
                list.Add(message);

            }
            return list;
        }

        /// <summary>
        /// 指定ログファイルの直前からn回数出力した対象ログレベル行を返す。
        /// </summary>
        /// <param name="logFileName">ファイル名</param>
        /// <param name="loglevel">ログレベル</param>
        /// <param name="numberofwrote">最後から数えた何回目の出力か</param>
        /// <returns>対象行</returns>
        private string GetTargetLogLine(string logFileName, string loglevel, int numberofwrote)
        {
            if (numberofwrote <= 0)
            {
                return string.Empty;
            }

            var logFile = (from f in Directory.EnumerateFiles(Path.GetFullPath(Settings.Default.TestLogPath), logFileName + "*")
                           orderby File.GetLastWriteTime(f) descending
                           select f).First();
            if (logFile == null)
            {
                return string.Empty;
            }

            var data = File.ReadAllText(logFile);

            var work = "!aaaaaaaaaaa!";
            data = data.Replace("\r\n[", work).Replace("\r\n", "\t").Replace(work, "\r\n[");
            var splitted = data.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            var cnt = 0;
            // traditional loop
            for (int i = splitted.Length - 1; i >= 0; i--)
            {
                if (splitted[i].IndexOf("[" + loglevel + "]", StringComparison.OrdinalIgnoreCase) > 0)
                {
                    cnt++;
                    if (numberofwrote == cnt)
                    {
                        return splitted[i];
                    }
                }
            }

            // ログファイルが1Mでローテーションされているため、
            // 2回目以前のものが空になってしまうことがある。
            // その場合は（1ケースでログが2Mバイトは超えないはず、という前提のため、やや乱暴だが）
            // 最新から1個前のログファイルだけ追加で調べる。
            var rotateFileName = DateTime.Now.ToString("yyyy-MM-dd") + ".00.log";
            // 対象ログファイルが1個前のログファイルの場合は無限ループを防ぐために再帰呼び出しをせず、空文字を返す
            if (logFileName == rotateFileName)
            {
                return string.Empty;
            }

            return GetTargetLogLine(rotateFileName, loglevel, numberofwrote - cnt);
        }

        /// <summary>
        /// Table Storageを全検索する。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <param name="account">アカウント</param>
        /// <returns>ダンプ行</returns>
        private DumpRows TableStorageAllSelect(string tableName, CloudStorageAccount account)
        {
            var cloudTable = account.CreateCloudTableClient().GetTableReference(tableName);

            var ret = new DumpRows() { TableName = tableName };
            ret.DataRows = new List<Dictionary<string, string>>();

            foreach (var tableEntity in cloudTable.ExecuteQuery(new TableQuery<DynamicTableEntity>()))
            {
                if (ret.Headers == null)
                {
                    ret.Headers = new Dictionary<string, int>();
                    ret.Headers.Add("PartitionKey", "PartitionKey".Length);
                    ret.Headers.Add("RowKey", "RowKey".Length);
                    ret.Headers.Add("Timestamp", "Timestamp".Length);
                    ret.Headers.Add("ETag", "ETag".Length);
                    foreach (var column in tableEntity.Properties)
                    {
                        ret.Headers.Add(column.Key, column.Key.Length);
                    }
                }

                var row = new Dictionary<string, string>();
                if (ret.Headers["PartitionKey"] < tableEntity.PartitionKey.Length)
                {
                    ret.Headers["PartitionKey"] = tableEntity.PartitionKey.Length;
                }
                row.Add("PartitionKey", tableEntity.PartitionKey);
                if (ret.Headers["RowKey"] < tableEntity.RowKey.Length)
                {
                    ret.Headers["RowKey"] = tableEntity.RowKey.Length;
                }
                row.Add("RowKey", tableEntity.RowKey);
                if (ret.Headers["Timestamp"] < tableEntity.Timestamp.ToString().Length)
                {
                    ret.Headers["Timestamp"] = tableEntity.Timestamp.ToString().Length;
                }
                row.Add("Timestamp", tableEntity.Timestamp.ToString());
                if (ret.Headers["ETag"] < tableEntity.ETag.Length)
                {
                    ret.Headers["ETag"] = tableEntity.ETag.Length;
                }
                row.Add("ETag", tableEntity.ETag);

                foreach (var column in tableEntity.Properties)
                {
                    var strValue = ConvertEntityPropertyToString(column.Value);

                    if (ret.Headers[column.Key] < strValue.Length)
                    {
                        ret.Headers[column.Key] = strValue.Length;
                    }
                    row.Add(column.Key, strValue);
                }
                ret.DataRows.Add(row);
            }

            return ret;
        }

        /// <summary>
        /// 条件を満たすまで待機する（100ミリ秒毎に確認する）。
        /// タイムアウトした場合は例外をスローする。
        /// </summary>
        /// <param name="predicate">結果がtrueなら終了、falseなら待機</param>
        /// <param name="afterTimeout">タイムアウトした場合に行う処理</param>
        /// <param name="timeoutSeconds">タイムアウト時間（秒） デフォルトは10秒</param>
        private void WaitUntil(Func<bool> predicate, Action afterTimeout = null, int timeoutSeconds = 10)
        {
            var startTime = DateTime.UtcNow;
            while (!predicate())
            {
                if ((DateTime.UtcNow - startTime).TotalSeconds > timeoutSeconds)
                {
                    if (afterTimeout != null)
                    {
                        afterTimeout();
                    }
                    throw new WebDriverTimeoutException("待機時間が" + timeoutSeconds + "秒を超えました。");
                }
                Thread.Sleep(100);
            }
        }

        /// <summary>
        /// 時間内に条件を満たせるかどうか。
        /// </summary>
        /// <param name="predicate">結果がtrueなら終了、falseなら待機</param>
        /// <param name="timeoutSeconds">タイムアウト時間（秒） デフォルトは10秒</param>
        /// <rereturns>対象時間内に条件を満たせたらtrue、タイムアウトしたらfalse</rereturns>
        private bool isAbleInTime(Func<bool> predicate, int timeoutSeconds = 10)
        {
            var startTime = DateTime.UtcNow;
            while (!predicate())
            {
                if ((DateTime.UtcNow - startTime).TotalSeconds > timeoutSeconds)
                {
                    return false;
                }
                Thread.Sleep(100);
            }
            return true;
        }

        #endregion

        #region API系メソッド
        #region Fields/Properties

        private const string _jsonMimeType = "application/json";

        /// <summary>
        /// リクエストヘッダ
        /// </summary>
        private Dictionary<string, string> RequestHeader
        {
            get
            {
                return GetCurrentValue("RequestHeader", () => new Dictionary<string, string>()) as Dictionary<string, string>;
            }
            set
            {
                ScenarioContext.Current["RequestHeader"] = value;
            }
        }

        /// <summary>
        /// リクエストボディ
        /// </summary>
        private string RequestBody
        {
            get
            {
                return GetCurrentValue("RequestBody") as string;
            }
            set
            {
                ScenarioContext.Current["RequestBody"] = value;
            }
        }

        /// <summary>
        /// レスポンスのHTTPステータスコード
        /// </summary>
        private int? StatusCode
        {
            get
            {
                return GetCurrentValue("StatusCode") as int?;
            }
            set
            {
                ScenarioContext.Current["StatusCode"] = value;
            }
        }

        /// <summary>
        /// レスポンスボディ
        /// </summary>
        private string ResponseBody
        {
            get
            {
                return GetCurrentValue("ResponseBody") as string;
            }
            set
            {
                ScenarioContext.Current["ResponseBody"] = value;
                ScenarioContext.Current.Remove("ResponseBodyJson");
            }
        }

        /// <summary>
        /// レスポンスボディをJSONデシリアライズしたインスタンスを取得します。
        /// </summary>
        private JObject ResponseBodyJson
        {
            get
            {
                return GetCurrentValue("ResponseBodyJson", () => JObject.Parse(ResponseBody)) as JObject;
            }
        }

        /// <summary>
        /// キーを指定してScenarioContext.Currentの値を取得します。
        /// </summary>
        /// <param name="key">キー</param>
        /// <param name="defaultValueSelector">キーが存在しない場合に返す値のセレクタ</param>
        /// <returns>キーに紐づく値</returns>
        private object GetCurrentValue(string key, Func<object> defaultValueSelector = null)
        {
            if (!ScenarioContext.Current.ContainsKey(key))
            {
                ScenarioContext.Current[key] = defaultValueSelector != null ? defaultValueSelector() : null;
            }
            return ScenarioContext.Current[key];
        }

        #endregion

        #region Given

        /// <summary>
        /// 空のリクエストボディを設定します。
        /// </summary>
        [Given(@"空のリクエストボディを指定する")]
        public void SetEmptyOnRequestBody()
        {
            RequestBody = string.Empty;
        }

        /// <summary>
        /// JSON形式でない文字列をリクエストボディに設定します。
        /// </summary>
        [Given(@"JSON形式でないリクエストボディを指定する")]
        public void SetInvalidJsonOnRequestBody()
        {
            RequestBody = @"{abc";
        }

        /// <summary>
        /// 空のオブジェクトをリクエストボディに設定します。
        /// </summary>
        [Given(@"リクエストボディに空のオブジェクトを指定する")]
        public void SetEmptyObjectOnRequestBody()
        {
            RequestBody = "{}";
        }

        /// <summary>
        /// 空のオブジェクトをリクエストボディに設定します。
        /// </summary>
        [Given(@"リクエストボディに空のtokenを指定する")]
        public void SetEmptyTokenOnRequestBody()
        {
            RequestBody = "{ \"token\" : \"\" }";
        }

        /// <summary>
        /// application/x-www-form-urlencodedで送信する場合のAPI用のパラメータをセットします。
        /// 同じNameの場合は、http仕様に基づき配列として処理されます。
        /// </summary>
        /// <param name="table">Name, ValueのTable</param>
        [Given("以下のAPIパラメータをセットする")]
        public void ApiGivenSetParameters(Table table)
        {
            var set = CreateDynamicSet(table);
            var parameters = string.Empty;

            foreach (var row in set)
            {
                if (!string.IsNullOrEmpty(parameters))
                {
                    parameters += "&";
                }
                parameters += Uri.EscapeUriString(row.Name.ToString()) + "=" + Uri.EscapeUriString(row.Value.ToString());
            }
            RequestBody = parameters;
        }

        #endregion

        #region When

        /// <summary>
        /// APIにGETでアクセスします。
        /// </summary>
        /// <param name="url">APIのURL</param>
        [When(@"API「(/.*)」にGETでアクセスしたとき")]
        public void ApiGetRequest(string url)
        {
            ApiRequest(url, client => client.GetAsync(_jsonMimeType).Result);
        }

        /// <summary>
        /// APIに既定のContentTypeでPOSTでアクセスします。
        /// </summary>
        /// <param name="url">APIのURL</param>
        [When(@"API「(/.*)」にPOSTでアクセスしたとき")]
        public void ApiPostRequestWithDefaultContentType(string url)
        {
            ApiPostRequest(url, _jsonMimeType);
        }

        /// <summary>
        /// APIに指定されたContentTypeでPOSTでアクセスします。
        /// </summary>
        /// <param name="url">APIのURL</param>
        /// <param name="contentType">ContentType</param>
        [When(@"「API(/.*)」にContent-Typeを「(.*)」に設定してPOSTでアクセスしたとき")]
        public void ApiPostRequestWithContentType(string url, string contentType)
        {
            ApiPostRequest(url, contentType);
        }

        /// <summary>
        /// APIにPOSTでアクセスします。
        /// </summary>
        /// <param name="url">APIのURL</param>
        /// <param name="contentType">ContentTpye</param>
        private void ApiPostRequest(string url, string contentType)
        {
            ApiRequest(url, client => client.PostAsync(RequestBody, contentType, _jsonMimeType).Result);
        }

        private void ApiRequest(string url, Func<ApiServiceClient, HttpResponseMessage> request)
        {
            using (var client = new ApiServiceClient(url))
            {
                client.Header = RequestHeader;
                var result = request(client);
                StatusCode = (int)result.StatusCode;
                ResponseBody = result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("## StatusCode: {0}", StatusCode);
                Console.WriteLine("## ResponseBody: {0}", ResponseBody);
            }
        }

        #endregion

        #region Then

        /// <summary>
        /// レスポンスの内容が空であることを確認します。
        /// </summary>
        [Then(@"APIレスポンスが空であること")]
        public void ApiCheckResponseEmpty()
        {
            Assert.AreEqual(string.Empty, ResponseBody);
        }

        /// <summary>
        /// HTTPステータスコードが期待通りであることを確認します。
        /// </summary>
        /// <param name="expected"></param>
        [Then(@"APIのHTTPステータスコードが「(\d+)」であること")]
        public void ApiCheckHttpStatusCode(int expected)
        {
            Assert.AreEqual<int?>(expected, StatusCode);
        }

        /// <summary>
        /// errorが期待通りであることを確認します。
        /// </summary>
        /// <param name="errorCode">エラーコードの期待値</param>
        [Then(@"APIエラーコードが「(.*)」であること")]
        public void ApiCheckErrorCode(string errorCode)
        {
            var actual = ResponseBodyJson;
            Assert.AreEqual<string>(errorCode, actual.GetValue("error").ToString());
        }

        /// <summary>
        /// error_descriptionが期待通りであることを確認します。
        /// </summary>
        /// <param name="errorMessage">エラーメッセージの期待値</param>
        [Then(@"APIエラーメッセージが「(.*)」であること")]
        public void ApiCheckErrorMessage(string errorMessage)
        {
            var actual = ResponseBodyJson;
            Assert.AreEqual(errorMessage, actual.GetValue("error_description").ToString());
        }

        /// <summary>
        /// レスポンスボディの指定フィールドの値が期待通りであることを確認します。
        /// (注意) object, array は非対応
        /// </summary>
        /// <param name="key">フィールド名</param>
        /// <param name="expected">期待値</param>
        [Then("APIレスポンスボディの「(.*)」の値が「(.*)」であること")]
        public void ApiCheckResponseParameter(string key, string expected)
        {
            var actual = ResponseBodyJson;
            if (expected == "<NULL>")
            {
                Assert.IsNull(actual.GetValue(key).ToString());

            }
            else if (expected == "<!NULL>")
            {
                Assert.IsNotNull(actual.GetValue(key).ToString());
            }
            else
            {
                Assert.AreEqual(expected, actual.GetValue(key).ToString());
            }
        }

        /// <summary>
        /// レスポンスボディのpasswordの値が、簡素なパスワード形式
        /// （英数小文字と半角数字のみで混在必須な8文字）であることを確認します。
        /// </summary>
        /// <remarks>
        /// パスワード文字列は、システム上妥当なものは
        /// ・半角文字（大小英字・数字・!"#$%&;'()=~|-^\@[;:],./`{+*}<>;?_の記号）
        /// ・8文字以上128文字以内で、英字と数字が混在すること
        /// となっている。
        /// 管理者用APIでは、このルールを簡素化した上記形式で発行されることになった。
        /// </remarks>
        [Then("APIレスポンスボディのpassword値が簡素なパスワード形式かつ8文字であること")]
        public void ApiCheckResponseParameterPassword()
        {
            var actual = ResponseBodyJson.GetValue("password").ToString();
            Assert.AreEqual(8, actual.Length, actual + "は8文字でなければいけません。");

            var _regexValidChar = new Regex(@"^(?=.*?[a-z])(?=.*?\d)(.*){1,}$", RegexOptions.Compiled | RegexOptions.ExplicitCapture);
            Assert.IsTrue(_regexValidChar.Match(actual).Length > 0, actual + "が条件「半角英小文字＋半角数字のみ・混在必須」を満たしていません");
        }

        /// <summary>
        /// ユーザー登録APIの正常系で、発行されたパスワードでログインできることの確認
        /// </summary>
        /// <param name="userName">CARADA ID</param>
        [Then("ユーザー登録後、CARADA_ID「(.*)」のユーザーが利用開始画面へ遷移できること")]
        public void ThenSuccessToUseStart(string userName)
        {
            var password = ResponseBodyJson.GetValue("password").ToString();
            Given("Cookie名「CaradaIdLogin」のCookieを削除する");
            Given("Cookie名「TwoFactorLogin」のCookieを削除する");
            Given("「/User/LoginView」にブラウザでアクセスする");
            Then("内容「CARADA ID ログイン」を含むこと");
            Given("ID「" + userName + "」、パスワード「" + password + "」のアカウントでログインする");
            Then("内容「利用開始」を含むこと");
        }

        /// <summary>
        /// ユーザー情報リセットAPIの正常系で、CARADA IDと再発行されたパスワードでアカウント管理画面が表示されることを確認
        /// </summary>
        /// <param name="userName">CARADA ID</param>
        [Then("ユーザー登録後、CARADA_ID「(.*)」のユーザーがアカウント画面へ遷移できること")]
        public void ThenSuccessToAccountManager(string userName)
        {
            var password = ResponseBodyJson.GetValue("password").ToString();
            Given("Cookie名「CaradaIdLogin」のCookieを削除する");
            Given("Cookie名「TwoFactorLogin」のCookieを削除する");
            Given("「/User/LoginView」にブラウザでアクセスする");
            Then("内容「CARADA ID ログイン」を含むこと");
            Given("ID「" + userName + "」、パスワード「" + password + "」のアカウントでログインする");
            Then("内容「アカウント管理」を含むこと");
        }
        #endregion

        #region JWT
        private string _validIssuer { get; set; }
        private string _validSecretKey { get; set; }

        [Given(@"JwtのIssuerを「(.*)」として設定する")]
        public void GivenSetJwtIssur(string val)
        {
            _validIssuer = val;
        }

        [Given(@"JwtのSecretKeyを「(.*)」として設定する")]
        public void GivenSetJwtSecretKey(string val)
        {
            _validSecretKey = val;
        }

        private string CreateToken(string iss, long? iat, long? exp, long? nbf, object parameters, string secretKey)
        {
            var credentials = new SigningCredentials
            (
                GetHmacSha256SigningKey(secretKey),
                SecurityAlgorithms.HmacSha256Signature,
                SecurityAlgorithms.Sha256Digest
            );

            var jwtHeader = new JwtHeader(credentials);
            var jwtPayload = new JwtPayload();

            var claims = new List<Claim>();
            if (iss != null)
            {
                claims.Add(new Claim("iss", iss));
            }
            if (iat != null)
            {
                claims.Add(new Claim("iat", iat.ToString(), ClaimValueTypes.Integer64));
            }
            if (exp != null)
            {
                claims.Add(new Claim("exp", exp.ToString(), ClaimValueTypes.Integer64));
            }
            if (nbf != null)
            {
                claims.Add(new Claim("nbf", nbf.ToString(), ClaimValueTypes.Integer64));
            }

            jwtPayload.AddClaims(claims);

            var token = new JwtSecurityToken(jwtHeader, jwtPayload);

            if (parameters != null)
            {
                // {"params", "{\"Gender\": \"1\"}"  文字列リテラルではなく、
                // {"params": {"Gender": "1"} のように、値をオブジェクトの入れ子にしたいため、
                // jwtPayload.Addする。
                jwtPayload.Add("params", parameters);
            }

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private SymmetricSecurityKey GetHmacSha256SigningKey(string key)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            if (keyBytes.Length < 64)
            {
                Array.Resize(ref keyBytes, 64);
            }

            return new InMemorySymmetricSecurityKey(keyBytes);
        }


        /// <summary>
        /// JWTを生成し、リクエストボディに設定します。
        /// </summary>
        /// <param name="iss">発行者ID</param>
        /// <param name="iat"></param>
        /// <param name="exp"></param>
        /// <param name="nbf"></param>
        /// <param name="parameters"></param>
        /// <param name="secretKey"></param>
        private void CreateAndSetJwtOnRequestBody(string iss, long? iat, long? exp, long? nbf, object parameters, string secretKey)
        {
            var token = CreateToken(iss, iat, exp, nbf, parameters, secretKey);
            RequestBody = JsonConvert.SerializeObject(new { token });
        }

        /// <summary>
        /// API固有パラメータ
        /// </summary>
        private object ApiParams
        {
            get
            {
                return GetCurrentValue("JwtSpecificParams");
            }
            set
            {
                ScenarioContext.Current["JwtSpecificParams"] = value;
            }
        }

        /// <summary>
        /// JWTのAPI固有パラメータ用の変数を設定する。
        /// </summary>
        /// <remarks>
        /// ネストした構造は表現できません。
        /// またパラメータのキーにFieldは使えません（ヘッダに使っているため）。
        /// </remarks>
        [Given(@"API固有パラメータをField->Value形式で設定する")]
        public void GivenSetJwtParams(Table table)
        {
            // JwtPayload.Add の仕様にあわせて、 Dictionary<String, Object>にする
            var dic = new Dictionary<string, object>();
            var entities = CreateDynamicSet(table);

            foreach (var entity in entities)
            {
                var fieldName = "";
                foreach (var s in entity as IDictionary<string, object>)
                {
                    if (s.Key == "Field")
                    {
                        fieldName = s.Value.ToString();
                    }
                    else
                    {
                        dic.Add(fieldName, s.Value.ToString());
                    }
                }
            }
            ApiParams = dic;
        }

        [Given("JWT形式でリクエストボディを設定する")]
        public void SetJwt()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(_validIssuer, iat, iat + 120, iat - 120, ApiParams, _validSecretKey);
        }

        [Given("iss未指定のJWT形式でリクエストボディを設定する")]
        public void SetJwtWithoutIss()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(null, iat, iat + 120, iat - 120, ApiParams, _validSecretKey);
        }

        [Given("iat未指定のJWT形式でリクエストボディを設定する")]
        public void SetJwtWithoutIat()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(_validIssuer, null, iat + 120, iat - 120, ApiParams, _validSecretKey);
        }

        [Given("exp未指定のJWT形式でリクエストボディを設定する")]
        public void SetJwtWithoutExp()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(_validIssuer, iat, null, iat - 120, ApiParams, _validSecretKey);
        }

        [Given("nbf未指定のJWT形式でリクエストボディを設定する")]
        public void SetJwtWithoutNbf()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(_validIssuer, iat, iat + 120, null, ApiParams, _validSecretKey);
        }

        [Given("無効なissを含むJWT形式でリクエストボディを設定する")]
        public void SetJwtWithInvalidIss()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody("invalid_iss", iat, iat + 120, iat - 120, ApiParams, _validSecretKey);
        }

        [Given("共通でない秘密鍵で署名したJWT形式でリクエストボディを設定する")]
        public void SetJwtWithInvalidSignature()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(_validIssuer, iat, iat + 120, iat - 120, ApiParams, "invalid_secret_key");
        }

        [Given("nbfに未来時刻を指定したJWT形式でリクエストボディを設定する")]
        public void SetJwtWithInvalidNbf()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(_validIssuer, iat, iat + 120, iat + 10, ApiParams, _validSecretKey);
        }

        [Given("expに現在時刻を指定したJWT形式でリクエストボディを設定する")]
        public void SetJwtWithInvalidExp()
        {
            var iat = DateTime.UtcNow.ToUnixTime();
            CreateAndSetJwtOnRequestBody(_validIssuer, iat, iat, iat - 120L, ApiParams, _validSecretKey);
        }

        #endregion
        #endregion

        #region APIMock

        /// <summary>
        /// MockSettingにモックデータを投入する。
        /// </summary>
        /// <param name="table">table</param>
        [Given(@"APIモックテーブルへ次のデータを登録する")]
        public void GivenApiMockInsert(Table table)
        {
            var mockAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=" + Settings.Default.MockStorageAccount + ";AccountKey=" + Settings.Default.MockStorageAccountKey);
            TableStorageInsert("MockSetting", table, mockAccount);
        }

        /// <summary>
        /// MockSettingからモックデータを削除する。
        /// </summary>
        /// <param name="table">table</param>
        [Given(@"APIモックテーブルから次のデータを削除する")]
        public void GivenApiMockDelete(Table table)
        {
            var mockAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=" + Settings.Default.MockStorageAccount + ";AccountKey=" + Settings.Default.MockStorageAccountKey);
            TableStorageDelete("MockSetting", table, mockAccount);
        }

        #endregion

        #region FP

        /// <summary>
        /// 指定IDとパスワードのアカウントでFP版にログインする
        /// </summary>
        /// <param name="id">ID</param>
        /// <param name="password">パスワード</param>
        [Given(@"ID「(.*)」、パスワード「(.*)」のアカウントでFP版にログインする")]
        private void LoginUserFP(string id, string password)
        {
            Given(@"IDが「CaradaId」に「" + id + "」を入力する");
            Given(@"IDが「Password」に「" + password + "」を入力する");
            When(@"「name」という属性の値が「Login」のボタンをクリックしたとき");

            // 認証コード入力画面の場合（3秒内に認証コード入力画面と判別できた場合）
            if (isAbleInTime(() => WebDriver.PageSource.Contains("CARADA ID 認証ｺｰﾄﾞ確認"), 3))
            {
                Given(@"最後に送信されたメールから取得した認証コードを「VerifyCode」というIDのオブジェクトに入力する");
                When(@"「name」という属性の値が「Verify」のボタンをクリックしたとき");
            }

            loginDateTime = DateTime.Now;
        }

        #endregion FP
    }
}