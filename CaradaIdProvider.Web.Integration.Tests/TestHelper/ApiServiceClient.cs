﻿using CaradaIdProvider.Web.Integration.Tests.Properties;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CaradaIdProvider.Web.Test.Integration.TestHelper
{
    /// <summary>
    /// Web APIに HTTP 要求を送信し、HTTP 応答を受信するためのクラスです。
    /// </summary>
    public class ApiServiceClient : IDisposable
    {
        private HttpClient _client;
        private string _url;

        /// <summary>
        /// Httpヘッダ
        /// </summary>
        public Dictionary<string, string> Header { get; set; }

        /// <summary>
        /// Web APIのURLを指定してHttpClientを初期化します。
        /// </summary>
        /// <param name="url">Web APIのURL(ドメイン省略)</param>
        public ApiServiceClient(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback = (se, ce, ch, ss) => true;

            _url = url;
            _client = new HttpClient { BaseAddress = new Uri("https://localhost:" + Settings.Default.SSLIISPortNumberForTestSite) };
            Header = new Dictionary<string, string>();
        }

        /// <summary>
        /// Web APIにGETでアクセスします。
        /// </summary>
        /// <param name="url">APIのURL</param>
        /// <param name="contentType">ContentTpye</param>
        public async Task<HttpResponseMessage> GetAsync(string accept)
        {
            SetHeader();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(accept));
            return await _client.GetAsync(_url);
        }

        /// <summary>
        /// Web APIにPOSTでアクセスします。
        /// </summary>
        /// <param name="url">APIのURL</param>
        /// <param name="contentType">ContentTpye</param>
        public async Task<HttpResponseMessage> PostAsync(string requestBody, string contentType, string accept)
        {
            SetHeader();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(accept));
            return await _client.PostAsync(_url, new StringContent(requestBody, Encoding.UTF8, contentType));
        }

        private void SetHeader()
        {
            foreach (var x in Header)
            {
                _client.DefaultRequestHeaders.Add(x.Key, x.Value);
            }
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}