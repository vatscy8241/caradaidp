﻿using System;
using System.Diagnostics;
using System.IO;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper
{
    /// <summary>
    /// MSBuild用のクラス。
    /// </summary>
    public static class MsBuild
    {
        /// <summary>
        /// DOTNETのホームディレクトリの環境設定パス
        /// </summary>
        private const string MS_DOTNET_HOME_ENVIRONMENT_VARIABLE_NAME = "MS_DOTNET_HOME";

        /// <summary>
        /// MSBuildの実行ファイル名
        /// </summary>
        private const string MS_BUILD_EXE_NAME = "MSBuild.exe";

        /// <summary>
        /// MSBuildを行う。
        /// </summary>
        /// <param name="projectPath">パス</param>
        public static void StartMsBuild(string projectPath)
        {
            var arguments = string.Format(@"""{0}"" /p:Configuration=CI;OutputPath=bin\ /t:Clean;rebuild;TransformWebConfig /p:DefineConstants=""CI;DEBUG""", projectPath);

            var dotnetHome = Environment.GetEnvironmentVariable(MS_DOTNET_HOME_ENVIRONMENT_VARIABLE_NAME, EnvironmentVariableTarget.Machine);

            if (string.IsNullOrEmpty(dotnetHome))
            {
                throw new Exception(string.Format("システム環境変数の「{0}」が指定されていません。", MS_DOTNET_HOME_ENVIRONMENT_VARIABLE_NAME));
            }

            var msBuildRunExecutePath = Path.Combine(dotnetHome, MS_BUILD_EXE_NAME);

            var processStartInfo = new ProcessStartInfo(msBuildRunExecutePath, arguments)
            {
                RedirectStandardOutput = false,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                LoadUserProfile = true,
                WindowStyle = ProcessWindowStyle.Hidden
            };

            using (var process = Process.Start(processStartInfo))
            {
                process.WaitForExit();

                using (var reader = process.StandardError)
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }
    }
}
