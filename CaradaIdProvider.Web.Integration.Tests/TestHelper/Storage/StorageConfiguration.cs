﻿using MTI.CaradaIdProvider.Web.Models;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper.Storage
{
    public static class StorageConfiguration
    {
        /// <summary>
        /// 必要なテーブルを作成します。
        /// </summary>
        public static void Initialize()
        {
            AuthorizationStorageContext.CreateCloudTables();
        }
    }
}
