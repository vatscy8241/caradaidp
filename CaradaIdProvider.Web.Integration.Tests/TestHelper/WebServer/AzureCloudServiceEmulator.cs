﻿using CaradaIdProvider.Web.Integration.Tests.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TechTalk.SpecFlow;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper.WebServer
{
    /// <summary>
    /// AzureEmulatorの設定
    /// </summary>
    internal class AzureEmulatorSettings
    {
        /// <summary>
        /// ビルド後のWebアプリケーションのフルパス
        /// </summary>
        public string ApplicatonFullPath { get; set; }
        /// <summary>
        /// サイト名
        /// </summary>
        public string SiteName { get; set; }
        /// <summary>
        /// Azureプロジェクトのフルパス
        /// </summary>
        public string AzureProjectFullPath { get; set; }
        /// <summary>
        /// EmulatorHome
        /// </summary>
        public string CloudServiceEmulatorHome { get; set; }
        /// <summary>
        /// SdkHome
        /// </summary>
        public string AzureSdkHome { get; set; }

    }

    public interface IAzureCloudServiceEmulator
    {
        void Start();
        void Dispose(string removeRoleName);
    }

    public class AzureCloudServiceEmulatorFactory
    {
        public static IAzureCloudServiceEmulator Create(string siteName, string applicatonFullPath, string azureProjectFullPath)
        {
            var cloudServiceEmulatorHome = Environment.GetEnvironmentVariable(
                "AzureCloudServiceEmulator", EnvironmentVariableTarget.Machine);
            if (string.IsNullOrEmpty(cloudServiceEmulatorHome))
            {
                throw new Exception(string.Format("システム環境変数の「{0}」が指定されていません。",
                    "AzureCloudServiceEmulator"));
            }

            var azureSdkHome = Environment.GetEnvironmentVariable(
                "AzureSdkHome", EnvironmentVariableTarget.Machine);
            if (string.IsNullOrEmpty(azureSdkHome))
            {
                throw new Exception(string.Format("システム環境変数の「{0}」が指定されていません。",
                    "AzureSdkHome"));
            }

            var settings = new AzureEmulatorSettings
            {
                ApplicatonFullPath = applicatonFullPath,
                AzureProjectFullPath = azureProjectFullPath,
                SiteName = siteName,
                CloudServiceEmulatorHome = cloudServiceEmulatorHome,
                AzureSdkHome = azureSdkHome
            };

            var match = Regex.Match(azureSdkHome, @"v(\d+\.\d+)", RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                return new AzureCloudServiceEmulator2_5(settings);
            }

            var version = double.Parse(match.Groups[1].Value);
            // 互換性がないバージョンが出たら新しく型を作ってください
            if (version >= 2.5)
            {
                return new AzureCloudServiceEmulator2_5(settings);
            }
            throw new Exception(string.Format("未知のSdkバージョン {0}", match.Groups[1].Value));
        }
    }

    /// <summary>
    /// AzureEmulator Azure SDK2.5以上に対応
    /// </summary>
    internal class AzureCloudServiceEmulator2_5 : IAzureCloudServiceEmulator
    {
        /// <summary>
        /// csrunコマンド
        /// </summary>
        private readonly string _csrun;
        /// <summary>
        /// cspackコマンド
        /// </summary>
        private readonly string _cspack;
        /// <summary>
        /// csrun /statusのセパレータ
        /// </summary>
        private static readonly string _statusSeparator =
            "==================================================================";
        /// <summary>
        /// デプロイID取得用のセパレータ
        /// </summary>
        private static readonly string _deploymentIdKey = "Deployment-Id:";
        /// <summary>
        /// テスト用のデプロイディレクトリ
        /// </summary>
        private readonly string _testRoot;
        /// <summary>
        /// セッティング
        /// </summary>
        private readonly AzureEmulatorSettings _settings;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="settings"></param>
        public AzureCloudServiceEmulator2_5(AzureEmulatorSettings settings)
        {
            _settings = settings;
            _csrun = _settings.CloudServiceEmulatorHome + @"\csrun.exe";
            _cspack = _settings.AzureSdkHome + @"\bin\cspack.exe";
            var driveLetter = Path.GetPathRoot(AppDomain.CurrentDomain.BaseDirectory);
            _testRoot = driveLetter + Settings.Default.TestRoot;
        }

        /// <summary>
        /// AzureEmulatorを開始する
        /// </summary>
        public void Start()
        {

            // 開始前にロールの初期化を行う
            Dispose(_settings.SiteName);
            // テストフォルダを削除する
            DeleteTestFolder();
            // Developer Fabricの初期化
            ExecuteDevfabricClean();
            ExecuteDevfabricShutodown();

            // パッケージの作成
            ExecuteCspack();
            // デプロイ
            ExecuteCsRun();
        }


        /// <summary>
        /// AzureEmulatorのStatusを取得 csrun /status
        /// </summary>
        /// <returns>csrun /statusの結果リスト。存在しない場合は0件のリストを返す</returns>
        private List<dynamic> GetAzureEmulatorStatus()
        {

            var processInfo = new ProcessStartInfo(_csrun, "/status")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                Verb = "RunAs",
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };


            var process = Process.Start(processInfo);
            var output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            process.Close();

            var tempLines = output.Split(new[] { _statusSeparator }, StringSplitOptions.RemoveEmptyEntries);

            var statusList = new List<dynamic>();
            if (tempLines.Count() <= 1)
            {
                // カウント1件の場合はStatusが存在していない状態なので0件のリストを返す
                return statusList;
            }


            for (var i = 1; i < tempLines.Count(); i++)
            {
                var status = tempLines[i].Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                statusList.Add(new
                {
                    DeploymentId = status[0].Split(new[] { _deploymentIdKey }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(),
                    Roles = status[Array.FindIndex(status, s => s.Trim() == "Roles:") + 1].Trim()
                });
            }

            return statusList;
        }

        /// <summary>
        /// cspackコマンドを実行しEmulatorにデプロイするパッケージを作成
        /// </summary>
        private void ExecuteCspack()
        {

            var processInfo = new ProcessStartInfo(_cspack, CreateCspackArgument())
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                Verb = "RunAs",
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            using (var process = Process.Start(processInfo))
            {
                process.WaitForExit();
                if (process.ExitCode != 0)
                {
                    throw new SpecFlowException(process.StandardError.ReadToEnd());
                }

                process.Close();
            }
        }

        /// <summary>
        /// cspack用のパラメータを作成
        /// </summary>
        /// <returns>cspack用のパラメータ文字列</returns>
        private string CreateCspackArgument()
        {

            var integrationTestRoot = Path.GetDirectoryName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));

            var cdefAbsolutePath = integrationTestRoot + @"\TestHelper\WebServer\EmulatorSettings\ServiceDefinition.csdef";
            var rolePropertycdefAbsolutePathPath = integrationTestRoot + @"\TestHelper\WebServer\EmulatorSettings\roleProperty.txt";
            var rolePhysicalPath = _testRoot + @"\" + _settings.SiteName + @".csx";


            var sitePhysicalDirectoriesParam = @"/sitePhysicalDirectories:" + _settings.SiteName + @";Web;" + _settings.ApplicatonFullPath;
            var outParam = @" /out:" + rolePhysicalPath;
            var roleParam = @"/role:" + _settings.SiteName + @";" + _settings.ApplicatonFullPath + @";bin\" + _settings.SiteName + @".dll";
            var rolePropertiesFileParam = @"/rolePropertiesFile:" + _settings.SiteName + @";" + rolePropertycdefAbsolutePathPath;

            return cdefAbsolutePath + " " + sitePhysicalDirectoriesParam + " " + outParam + " " + roleParam + " " +
                   rolePropertiesFileParam + @" /copyOnly";
        }

        /// <summary>
        /// csrunコマンドを実行する
        /// </summary>
        private void ExecuteCsRun()
        {
            var rolePhysicalPath = _testRoot + @"\" + _settings.SiteName + @".csx";
            var cscfgPath = _settings.AzureProjectFullPath + @"\ServiceConfiguration.CI.cscfg";


            var processInfo = new ProcessStartInfo(_csrun, rolePhysicalPath + " " + cscfgPath + " /useiisexpress")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                Verb = "RunAs",
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            using (var process = Process.Start(processInfo))
            {
                process.WaitForExit();
                if (process.ExitCode != 0)
                {
                    throw new SpecFlowException(process.StandardError.ReadToEnd());
                }

                process.Close();
            }
        }

        /// <summary>
        /// テストフォルダを削除する
        /// </summary>
        private void DeleteTestFolder()
        {
            if (Directory.Exists(_testRoot))
            {
                DirectoryUtils.Delete(_testRoot);
            }
        }

        /// <summary>
        /// csrun /statusの結果リストから、指定のロール名のロールを削除する
        /// </summary>
        /// <param name="removeRoleName">削除対処のロール名</param>
        public void Dispose(string removeRoleName)
        {

            var statusList = GetAzureEmulatorStatus();

            foreach (var status in statusList)
            {
                if (status.Roles != removeRoleName)
                {
                    continue;
                }

                var processInfo = new ProcessStartInfo(_csrun, "/remove:" + status.DeploymentId)
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    LoadUserProfile = true,
                    Verb = "RunAs",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                using (var process = Process.Start(processInfo))
                {
                    process.WaitForExit();
                    if (process.ExitCode != 0)
                    {
                        throw new SpecFlowException(process.StandardError.ReadToEnd());
                    }

                    process.Close();
                }
            }
            DeleteTestFolder();
        }

        /// <summary>
        /// Devloper fabricを初期化
        /// </summary>
        private void ExecuteDevfabricClean()
        {
            var processInfo = new ProcessStartInfo(_csrun, "/devfabric:clean")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                Verb = "RunAs",
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            using (var process = Process.Start(processInfo))
            {
                process.WaitForExit();
                if (process.ExitCode != 0)
                {
                    throw new SpecFlowException(process.StandardError.ReadToEnd());
                }

                process.Close();
            }
        }

        /// <summary>
        /// Developer fabricを終了
        /// </summary>
        private void ExecuteDevfabricShutodown()
        {
            var processInfo = new ProcessStartInfo(_csrun, "/devfabric:shutdown")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                Verb = "RunAs",
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            using (var process = Process.Start(processInfo))
            {
                process.WaitForExit();
                if (process.ExitCode != 0)
                {
                    throw new SpecFlowException(process.StandardError.ReadToEnd());
                }

                process.Close();
            }
        }
    }
}


