﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper.WebServer
{
    public interface IAzureStorageEmulator
    {
        void Start(bool isDataClear = true);
        void Stop(bool isDataClear = true);
        bool IsRunning();
    }

    public class AzureStorageEmulatorFactory
    {
        public static IAzureStorageEmulator Create()
        {
            var azureSdkHome = Environment.GetEnvironmentVariable(
                "AzureSdkHome", EnvironmentVariableTarget.Machine);
            if (string.IsNullOrEmpty(azureSdkHome))
            {
                throw new Exception(string.Format("システム環境変数の「{0}」が指定されていません。",
                    "AzureSdkHome"));
            }

            var storageEmulatorHome = Environment.GetEnvironmentVariable(
                "AzureStorageEmulatorHome", EnvironmentVariableTarget.Machine);
            if (string.IsNullOrEmpty(storageEmulatorHome))
            {
                throw new Exception(string.Format("システム環境変数の「{0}」が指定されていません。",
                    "AzureStorageEmulatorHome"));
            }

            var match = Regex.Match(azureSdkHome, @"v(\d+\.\d+)", RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                return new AzureStorageEmulator2_5(storageEmulatorHome);
            }

            var version = double.Parse(match.Groups[1].Value);
            // 互換性がないバージョンが出たら新しく作ってください
            if (version >= 2.6)
            {
                return new AzureStorageEmulator2_6(storageEmulatorHome);
            }
            else if (version >= 2.5)
            {
                return new AzureStorageEmulator2_5(storageEmulatorHome);
            }
            throw new Exception(string.Format("未知のSdkバージョン {0}", match.Groups[1].Value));
        }
    }

    /// <summary>
    /// Azure StorageEmulator AzureSDK2.5以上対応
    /// </summary>
    internal class AzureStorageEmulator2_5 : IAzureStorageEmulator
    {
        /// <summary>
        /// WAStorageEmulatorコマンド
        /// </summary>
        private readonly string _storageCommand;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public AzureStorageEmulator2_5(string storageEmulatorHome)
        {
            _storageCommand = storageEmulatorHome + @"\WAStorageEmulator";
        }

        /// <summary>
        /// StorageEmulator起動
        /// </summary>
        /// <param name="isDataClear">データ初期化フラグ true:データクリア False:何もしない</param>
        public void Start(bool isDataClear = true)
        {
            ExecuteStart(isDataClear);
        }

        /// <summary>
        /// StorageEmulator終了
        /// </summary>
        /// <param name="isDataClear">データ初期化フラグ true:データクリア False:何もしない</param>
        public void Stop(bool isDataClear = true)
        {
            ExecuteStop(isDataClear);
        }


        /// <summary>
        /// Startコマンド実行
        /// </summary>
        /// <param name="isDataClear">データ初期化フラグ true:データクリア False:何もしない</param>
        private void ExecuteStart(bool isDataClear)
        {
            if (isDataClear)
            {
                ExecuteWaStorageProcess("clear");
            }

            if (IsRunning())
            {
                return;
            }


            ExecuteWaStorageProcess("start");
        }

        /// <summary>
        /// Stopコマンド実行
        /// </summary>
        /// <param name="isDataClear">データ初期化フラグ true:データクリア False:何もしない</param>
        private void ExecuteStop(bool isDataClear)
        {
            if (isDataClear)
            {
                ExecuteWaStorageProcess("clear");
            }

            if (!IsRunning())
            {
                return;
            }


            ExecuteWaStorageProcess("Stop");
        }

        /// <summary>
        /// 起動中かどうか
        /// </summary>
        /// <returns>起動中フラグ true:起動中 false:停止中</returns>
        public bool IsRunning()
        {
            var processInfo = new ProcessStartInfo(_storageCommand, @"status")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                RedirectStandardOutput = true,
                Verb = "RunAs"
            };

            var process = Process.Start(processInfo);
            var output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            var outputs = output.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var status = outputs[1].Split(new[] { "IsRunning: " }, StringSplitOptions.RemoveEmptyEntries);
            process.Close();

            return status[0].Trim() == "True";
        }


        /// <summary>
        /// コマンド実行
        /// </summary>
        /// <param name="param">WAStorageEmulatorコマンドのパラメータ</param>
        private void ExecuteWaStorageProcess(string param)
        {
            var processInfo = new ProcessStartInfo(_storageCommand, param)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                Verb = "RunAs"
            };

            var process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
        }
    }

    /// <summary>
    /// Azure StorageEmulator AzureSDK2.6以上対応 2.5とはexeコマンドが異なる。
    /// </summary>
    internal class AzureStorageEmulator2_6 : IAzureStorageEmulator
    {
        private readonly string _storageCommand;

        public AzureStorageEmulator2_6(string storageEmulatorHome)
        {
            _storageCommand = storageEmulatorHome + @"\AzureStorageEmulator";
        }

        public void Start(bool isDataClear = true)
        {
            if (isDataClear)
            {
                ExecuteStorageProcess("clear");
            }

            if (IsRunning())
            {
                return;
            }


            ExecuteStorageProcess("start");
        }

        public void Stop(bool isDataClear = true)
        {
            if (isDataClear)
            {
                ExecuteStorageProcess("clear");
            }

            if (!IsRunning())
            {
                return;
            }


            ExecuteStorageProcess("Stop");
        }

        public bool IsRunning()
        {
            var processInfo = new ProcessStartInfo(_storageCommand, @"status")
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                RedirectStandardOutput = true,
                Verb = "RunAs"
            };

            var process = Process.Start(processInfo);
            var output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            var outputs = output.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var status = outputs[1].Split(new[] { "IsRunning: " }, StringSplitOptions.RemoveEmptyEntries);
            process.Close();

            return status[0].Trim() == "True";
        }

        private void ExecuteStorageProcess(string param)
        {
            var processInfo = new ProcessStartInfo(_storageCommand, param)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false,
                LoadUserProfile = true,
                Verb = "RunAs"
            };

            var process = Process.Start(processInfo);
            process.WaitForExit();
            process.Close();
        }
    }
}
