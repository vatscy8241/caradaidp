﻿using CaradaIdProvider.Web.Integration.Tests.Properties;
using CaradaIdProvider.Web.Integration.Tests.TestHelper.Storage;
using CaradaIdProvider.Web.Integration.Tests.TestHelper.WebServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper
{
    /// <summary>
    /// テスト全体の設定を行う。
    /// </summary>
    [TestClass]
    public class TestConfiguration
    {

        enum IisCategory
        {
            Site, Mock
        }

        /// <summary>
        /// IIS情報
        /// </summary>
        private static Dictionary<IisCategory, IisExpress> _iisExpresses;
        /// <summary>
        /// AzureEmulator仮
        /// </summary>
        private static IAzureCloudServiceEmulator _emulator;

        /// <summary>
        /// プロジェクトネームのキーの定数
        /// </summary>
        private const string PROJECT_NAME = "projName";

        /// <summary>
        /// ディレクトリネームのキーの定数
        /// </summary>
        private const string DIRECTORY_NAME = "directoryName";

        private const string AZURE_PROJECT_NAME = "azureProjectName";

        /// <summary>
        /// ポートネームのキーの定数
        /// </summary>
        private const string PORT_NAME = "portName";

        /// <summary>
        /// WebプロジェクトのIIS起動の設定情報のDictionary
        /// </summary>
        private static readonly Dictionary<string, string> _webSettingsInfo = new Dictionary<string, string>()
        {
            {PROJECT_NAME, Settings.Default.ProjectName},
            {DIRECTORY_NAME, ".Integrate.Tmp." + Settings.Default.ProjectName},
            {AZURE_PROJECT_NAME, Settings.Default.AzureProjectName}
        };

        /// <summary>
        /// emlファイル保存フォルダ
        /// </summary>
        public static readonly string TEST_MAILS_PATH = Path.GetFullPath(Settings.Default.MailPath);


        public TestContext TestContext { get; set; }

        /// <summary>
        /// 初期化を行う
        /// </summary>
        [AssemblyInitialize]
        public static void Initialize(TestContext testContext)
        {
            Console.WriteLine("テストの初期化処理を行います。");

            // サイトではisDataClear=trueとなっているが、おっかないのでfalseにしておく
            AzureStorageEmulatorFactory.Create().Start(false);
            StorageConfiguration.Initialize();

            StartUpWebServer();

            Console.WriteLine("テストの初期化処理が完了しました。");
        }

        /// <summary>
        /// クリーンアップを行う
        /// </summary>
        [AssemblyCleanup]
        public static void CleanUp()
        {
            Console.WriteLine("テストの終了処理を行います。");


            // AzureStorageEmulator.Stop();

            if (_emulator != null)
            {
                _emulator.Dispose(_webSettingsInfo[PROJECT_NAME]);
            }
            if (_iisExpresses != null)
            {
                ShutdownIisExpressAsync();
            }

            // サイトのTestConfigurationではここでデータ削除処理を行っている

            Console.WriteLine("テストの終了処理が完了しました。");
        }


        /// <summary>
        /// Webサーバーを起動。実装ダサいけど面倒だからこのまま
        /// </summary>
        private static void StartUpWebServer()
        {

            if (Settings.Default.IsExecuteDeploy)
            {
                if (Settings.Default.IsAzureEmulatorValid)
                {
                    _iisExpresses = new Dictionary<IisCategory, IisExpress>
                        {
                            {IisCategory.Site, new IisExpress()}
                        };

                    var tasks = new[]
                    {
                            StartUpAzureEmulator(_webSettingsInfo[PROJECT_NAME], _webSettingsInfo[DIRECTORY_NAME],
                                _webSettingsInfo[AZURE_PROJECT_NAME]),
                        };

                    Task.WaitAll(tasks);
                }
                else
                {
                    _iisExpresses = new Dictionary<IisCategory, IisExpress>
                        {
                            {IisCategory.Site, new IisExpress()}
                        };
                    var tasks = new[]
                    {
                            StartUpIisExpressAsync(IisCategory.Site, _webSettingsInfo[PROJECT_NAME],
                                _webSettingsInfo[DIRECTORY_NAME]),
                        };
                    Task.WaitAll(tasks);
                }


            }
        }


        /// <summary>
        /// AzureEmulatorを起動する。
        /// </summary>
        private static async Task StartUpAzureEmulator(string name, string directoryName, string azureProjectPath)
        {
            await Task.Run(() =>
            {
                ExecuteMsBuild(name, directoryName);
                _emulator = AzureCloudServiceEmulatorFactory.Create(
                    name, GetApplicationPath(directoryName), GetApplicationPath(azureProjectPath));
                _emulator.Start();
            });

        }


        private static void ExecuteMsBuild(string name, string directoryName)
        {
            MsBuild.StartMsBuild(Path.Combine(GetApplicationPath(name), (name + ".csproj")));

            DirectoryUtils.Delete(GetApplicationPath(directoryName));
            DirectoryUtils.Create(GetApplicationPath(directoryName), new[] { FileAttributes.Hidden });
            DirectoryUtils.Copy(GetApplicationPath(name), GetApplicationPath(directoryName), true);
            // Web.configにメールのemlファイルを保存するフォルダを指定する
            MailSetting(Path.Combine(GetApplicationPath(name), @"obj\" + "CI" + @"\TransformWebConfig\transformed\Web.config"));
            File.Copy(Path.Combine(GetApplicationPath(name), @"obj\" + "CI" + @"\TransformWebConfig\transformed\Web.config"), Path.Combine(GetApplicationPath(directoryName), "Web.config"), true);

            Console.WriteLine("テスト対象プロジェクトのビルドを終了しました。");
        }

        /// <summary>
        /// IISExpressを起動する。
        /// </summary>
        private static async Task StartUpIisExpressAsync(IisCategory iisCategory, string name, string directoryName)
        {
            Console.WriteLine("テスト対象プロジェクトのビルドを開始しました。");
            await Task.Run(() =>
            {

                ExecuteMsBuild(name, directoryName);

                Console.WriteLine("IISを起動します。");



                IisExpressSettings settings;
                switch (iisCategory)
                {

                    case IisCategory.Site:
                        settings = Settings.Default.IsSiteSSLValid
                            ? IisExpressSettings.FindSiteSettingByPathForSsl(GetApplicationPath(directoryName))
                            : IisExpressSettings.FindSiteSettingByPathForNoSsl(GetApplicationPath(directoryName));

                        break;
                    default:
                        throw new ArgumentException("iisCategoryエラー");
                }

                _iisExpresses[iisCategory].Start(settings);
            });

            // IISExpress起動までちょっと待つ。
            // FIXME: 起動待ちが出来なかったので、できたら変更したい。
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("IISを起動しました。");
        }

        /// <summary>
        /// IISExpressをシャットダウンする。
        /// </summary>
        private static void ShutdownIisExpressAsync()
        {
            Console.WriteLine("IISを停止します。");

            foreach (KeyValuePair<IisCategory, IisExpress> pair in _iisExpresses)
            {
                pair.Value.Dispose();
            }

            try
            {
                DirectoryUtils.Delete(GetApplicationPath(_webSettingsInfo[DIRECTORY_NAME]));
            }
            catch (Exception e)
            {
                Console.WriteLine("テンポラリファイルの削除に失敗しましたが、無視します。");
                Console.WriteLine(e);
            }

            Console.WriteLine("IISを停止しました。");

        }

        /// <summary>
        /// アプリケーションのパスを取得する。
        /// </summary>
        /// <param name="applicationName">アプリケーション名</param>
        /// <returns>アプリケーションのパス</returns>
        private static string GetApplicationPath(string applicationName)
        {
            var solutionFolder = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory)));
            return Path.Combine(solutionFolder, applicationName);
        }

        /// <summary>
        /// メール一時フォルダを構成ファイルに記述する。相対パスが使えないための処置。
        /// </summary>
        /// <param name="settingPath">構成ファイル</param>
        private static void MailSetting(string settingPath)
        {
            var doc = new System.Xml.XmlDocument();
            doc.Load(settingPath);

            var node = doc["configuration"]["system.net"]["mailSettings"]["smtp"]["specifiedPickupDirectory"];
            node.SetAttribute("pickupDirectoryLocation", TEST_MAILS_PATH);
            doc.Save(settingPath);
            DirectoryUtils.Create(TEST_MAILS_PATH);
        }
    }
}
