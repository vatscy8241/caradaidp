﻿using CaradaIdProvider.Web.Integration.Tests.Properties;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using System.Collections;
using System.Collections.Generic;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper
{
    public static class TestUtil
    {
        /// <summary>
        /// Seleniumテスト用のWebDriverを取得する
        /// </summary>
        /// <param name="isHideCommandPrompt">コンソール非表示フラグ(デフォルトtrue:非表示)</param>
        /// <returns></returns>
        public static IWebDriver GetWebDriver(bool isHideCommandPrompt = true, Dictionary<string, string> options = null)
        {
            switch (Settings.Default.WebDriver)
            {
                case "PhantomJs":
                    return CreatePhantomJsDriver(isHideCommandPrompt, options);
                case "Chrome":
                    return CreateChromeDriver(isHideCommandPrompt);
                default:
                    return CreatePhantomJsDriver(isHideCommandPrompt, options);
            }
        }


        private static IWebDriver CreatePhantomJsDriver(bool isHideCommandPrompt, Dictionary<string, string> options)
        {
            var service = PhantomJSDriverService.CreateDefaultService();
            service.LoadImages = true;
            if (Settings.Default.IsSiteSSLValid)
            {
                service.SslProtocol = "any";
                service.IgnoreSslErrors = true;
                service.WebSecurity = false;
            }

            service.ProxyType = "none";
            service.HideCommandPromptWindow = isHideCommandPrompt;

            var pjsOptions = new PhantomJSOptions();
            if (options != null)
            {
                foreach (KeyValuePair<string, string> pair in options)
                {
                    pjsOptions.AddAdditionalCapability("phantomjs.page.customHeaders." + pair.Key, pair.Value);
                }
            }
            return new PhantomJSDriver(service, pjsOptions);
        }

        private static IWebDriver CreateChromeDriver(bool isHideCommandPrompt)
        {
            var service = ChromeDriverService.CreateDefaultService();

            var options = new ChromeOptions();
            service.HideCommandPromptWindow = isHideCommandPrompt;
            if (!Settings.Default.IsSiteSSLValid)
            {
                return new ChromeDriver(service, options);
            }
            options.AddAdditionalCapability("chrome.switches", new ArrayList() { "--ignore-certificate-errors" }, true);
            options.AddArgument("--test-type");
            return new ChromeDriver(service, options);
        }
    }
}
