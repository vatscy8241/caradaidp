﻿using System.IO;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper
{
    /// <summary>
    /// ディレクトリユーティリティ。
    /// </summary>
    public class DirectoryUtils
    {
        /// <summary>
        /// ディレクトリを作成する。
        /// </summary>
        /// <param name="dirPath">ディレクトリパス</param>
        public static void Create(string dirPath)
        {
            Create(dirPath, null);
        }

        /// <summary>
        /// ディレクトリを作成する。
        /// </summary>
        /// <param name="dirPath">ディレクトリパス</param>
        /// <param name="attributes">ディレクトリ属性</param>
        public static void Create(string dirPath, FileAttributes[] attributes)
        {
            DirectoryInfo dir = new DirectoryInfo(dirPath);

            if (dir.Exists)
            {
                return;
            }

            dir = Directory.CreateDirectory(dirPath);

            if (attributes == null)
            {
                return;
            }

            foreach (FileAttributes attribute in attributes)
            {
                dir.Attributes |= attribute;
            }
        }

        /// <summary>
        /// ディレクトリをコピーする。
        /// </summary>
        /// <param name="sourceDirPath">出力元ディレクトリ</param>
        /// <param name="destDirPath">出力先ディレクトリ</param>
        /// <param name="copySubDirs">サブディレクトリをコピーするかどうか</param>
        public static void Copy(string sourceDirPath, string destDirPath, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirPath);
            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!dir.Exists)
            {
                return;
            }

            if (!Directory.Exists(destDirPath))
            {
                Directory.CreateDirectory(destDirPath);
            }

            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                file.CopyTo(Path.Combine(destDirPath, file.Name), false);
            }

            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    Copy(subdir.FullName, Path.Combine(destDirPath, subdir.Name), copySubDirs);
                }
            }
        }

        /// <summary>
        /// 指定したディレクトリをすべて削除します。
        /// </summary>
        /// <param name="dirPath">削除するディレクトリのパス。</param>
        public static void Delete(string dirPath)
        {
            Delete(new DirectoryInfo(dirPath));
        }

        /// <summary>
        /// 指定したディレクトリをすべて削除します。
        /// </summary>
        /// <param name="directoryInfo">削除するディレクトリの DirectoryInfo。</param>
        public static void Delete(DirectoryInfo directoryInfo)
        {
            if (!directoryInfo.Exists)
            {
                return;
            }

            // すべてのファイルの読み取り専用属性を解除する
            foreach (FileInfo fileInfo in directoryInfo.GetFiles())
            {
                if ((fileInfo.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    fileInfo.Attributes = FileAttributes.Normal;
                }
            }

            // サブディレクトリ内の読み取り専用属性を解除する (再帰)
            foreach (DirectoryInfo subDirectryInfo in directoryInfo.GetDirectories())
            {
                Delete(subDirectryInfo);
            }

            // このディレクトリの読み取り専用属性を解除する
            if ((directoryInfo.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                directoryInfo.Attributes = FileAttributes.Directory;
            }

            // このディレクトリを削除する
            directoryInfo.Delete(true);
        }
    }
}

