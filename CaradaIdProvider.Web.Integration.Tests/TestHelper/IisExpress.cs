﻿using CaradaIdProvider.Web.Integration.Tests.Properties;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;

namespace CaradaIdProvider.Web.Integration.Tests.TestHelper
{

    public class IisExpressSettings
    {
        /// <summary>
        /// デプロイ対象のアプリケーションパス
        /// </summary>
        public string Path { get; private set; }
        /// <summary>
        /// ポート番号
        /// </summary>
        public int Port { get; private set; }
        /// <summary>
        /// SSLを有効にするかどうか
        /// </summary>
        public bool IsSslValid { get; private set; }
        /// <summary>
        /// ログのトレイス設定
        /// </summary>
        public string TraceSetting { get; private set; }


        /// <summary>
        /// インスタンスを生成
        /// テスト用のIISデフォルト設定 SSL無効
        /// </summary>
        /// <param name="path">アプリケーションのパス</param>
        /// <param name="traceSetting">IISのTraceオプション デフォルトi</param>
        /// <returns></returns>
        public static IisExpressSettings FindSiteSettingByPathForNoSsl(string path, string traceSetting = "i")
        {

            return new IisExpressSettings()
            {
                Path = path,
                Port = Settings.Default.NoSSLIISPortNumberForTestSite,
                IsSslValid = false,
                TraceSetting = traceSetting
            };
        }

        /// <summary>
        /// インスタンスを生成
        /// テスト用のIISデフォルト設定 SSL有効
        /// </summary>
        /// <param name="path">アプリケーションのパス</param>
        /// <param name="traceSetting">IISのTraceオプション デフォルトi</param>
        /// <returns></returns>
        public static IisExpressSettings FindSiteSettingByPathForSsl(string path, string traceSetting = "i")
        {
            return new IisExpressSettings()
            {
                Path = path,
                Port = Settings.Default.SSLIISPortNumberForTestSite,
                IsSslValid = true,
                TraceSetting = traceSetting
            };
        }

        /// <summary>
        /// インスタンスを生成
        /// </summary>
        /// <param name="path"></param>
        /// <param name="port"></param>
        /// <param name="isSslValid"></param>
        /// <param name="traceSetting"></param>
        /// <returns></returns>
        public static IisExpressSettings Find(string path, int port, bool isSslValid, string traceSetting)
        {
            return new IisExpressSettings()
            {
                Path = path,
                Port = port,
                IsSslValid = isSslValid,
                TraceSetting = traceSetting
            };
        }
    }



    /// <summary>
    /// IISExpressのプロセス管理クラス。
    /// </summary>
    public class IisExpress : IDisposable
    {
        /// <summary>
        /// 破棄するかどうか
        /// </summary>
        private bool _isDisposed;

        /// <summary>
        /// IISExpressのプロセス
        /// </summary>
        private Process _process;

        /// <summary>
        /// IISExpressの実行ファイル名
        /// </summary>
        private const string IIS_EXPRESS_EXE_NAME = "iisexpress.exe";

        /// <summary>
        /// IISExpressのホームディレクトリの環境変数名
        /// </summary>
        private const string MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME = "MS_IIS_EXPRESS_HOME";


        /// <summary>
        /// 破棄する
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        private static readonly string _iisExpressHome;


        static IisExpress()
        {
            _iisExpressHome = Environment.GetEnvironmentVariable(
                            MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME, EnvironmentVariableTarget.Machine);
            if (string.IsNullOrEmpty(_iisExpressHome))
            {
                throw new Exception(string.Format("システム環境変数の「{0}」が指定されていません。",
                    MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME));
            }
        }

        /// <summary>
        /// IISExpressを起動する
        /// </summary>
        /// <param name="settings">IIS起動オプション</param>
        /// <returns></returns>
        public void Start(IisExpressSettings settings)
        {
            var iisSettingsPath = CopyApplicationHostConfig(settings);
            SetApplicationHostConfig(settings, iisSettingsPath);



            var iisExpressPath = Path.Combine(_iisExpressHome, IIS_EXPRESS_EXE_NAME);

            var arguments = string.Format(CultureInfo.InvariantCulture, @"/config:""{0}"" /trace:{1}", iisSettingsPath, settings.TraceSetting);
            Console.WriteLine(arguments);

            var info = new ProcessStartInfo(iisExpressPath)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                ErrorDialog = true,
                LoadUserProfile = true,
                CreateNoWindow = false,
                UseShellExecute = false,
                Arguments = arguments
            };

            var startThread = new Thread(() => StartIisExpress(info))
            {
                IsBackground = true
            };
            startThread.Start();

        }

        /// <summary>
        /// アプリケーションフォルダにデフォルトのIIS起動設定ファイルをコピーする
        /// </summary>
        /// <param name="settings">IIS起動設定</param>
        /// <returns>ApplicationHost.configのパス</returns>
        private string CopyApplicationHostConfig(IisExpressSettings settings)
        {
            var configPath = Path.Combine(settings.Path, "config");
            DirectoryUtils.Create(configPath);


            var configBasePath = _iisExpressHome + @"\AppServer\applicationhost.config";


            var applicationHostConfigPath = Path.Combine(configPath, @"ApplicationHost.config");
            File.Copy(configBasePath, applicationHostConfigPath);
            return applicationHostConfigPath;
        }


        /// <summary>
        /// ApplicationHostConfigにアプリケーションの設定を書き込む
        /// </summary>
        /// <param name="settings">IIS起動設定</param>
        /// <param name="iisSettingsPath">IISのApplicationHost.configのパス</param>
        private void SetApplicationHostConfig(IisExpressSettings settings, string iisSettingsPath)
        {
            // パスの設定
            var pathSettingArgument =
                string.Format(
                    @"set config -section:system.ApplicationHost/sites /""[name='Development Web Site'].[path='/'].[path='/'].physicalPath:{0}"" /commit:apphost /apphostconfig:""{1}""",
                    settings.Path, iisSettingsPath);
            ExecuteAppcmd(pathSettingArgument);

            // バインディングの設定
            var bindingSettingArgument =
                    string.Format(
                        @"set config -section:system.ApplicationHost/sites /+""[name='Development Web Site'].bindings.[protocol='{0}',bindingInformation=':{1}:localhost']"" /commit:apphost /apphostconfig:""{2}""",
                        settings.IsSslValid ? "https" : "http", settings.Port, iisSettingsPath);
            ExecuteAppcmd(bindingSettingArgument);

            // デフォルトのバインディング設定を削除
            var deleteDefaultSettingArgument =
                string.Format(
                    @"set config -section:system.ApplicationHost/sites /-""[name='Development Web Site'].bindings.[protocol='http',bindingInformation=':8080:localhost']"" /commit:apphost /apphostconfig:""{0}""",
                    iisSettingsPath);
            ExecuteAppcmd(deleteDefaultSettingArgument);

            // ブラウザでディレクトリ見えるように設定(いらないかも)
            var applicationSettingArgument =
                string.Format(
                    @"set config ""Development Web Site"" -section:system.webServer/directoryBrowse /enabled:""True"" /commit:apphost /apphostconfig:""{0}""",
                    iisSettingsPath);
            ExecuteAppcmd(applicationSettingArgument);

        }


        /// <summary>
        /// appcmdコマンドを実行する
        /// </summary>
        /// <param name="arguments">appcmdコマンドの引数</param>
        private void ExecuteAppcmd(string arguments)
        {
            var appcmd = _iisExpressHome + @"\appcmd.exe";


            var processInfor = new ProcessStartInfo(appcmd, arguments)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            var process = Process.Start(processInfor);
            process.WaitForExit();

            if (0 != process.ExitCode)
            {
                throw new Exception("IISのPath設定が失敗");
            }
        }


        /// <summary>
        /// IISExpressを起動する。
        /// </summary>
        /// <param name="directoryPath">ディレクトリパス</param>
        /// <param name="port">ポート番号</param>
        public void Start(string directoryPath, int port)
        {
            var iisExpressHome = Environment.GetEnvironmentVariable(
                MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME, EnvironmentVariableTarget.Machine);
            if (string.IsNullOrEmpty(iisExpressHome))
            {
                throw new Exception(string.Format("システム環境変数の「{0}」が指定されていません。",
                    MS_IIS_EXPRESS_HOME_ENVIRONMENT_VARIABLE_NAME));
            }

            var iisExpressPath = Path.Combine(iisExpressHome, IIS_EXPRESS_EXE_NAME);

            var arguments = string.Format(CultureInfo.InvariantCulture, @"/path:""{0}"" /port:{1} /systray:true /trace:i", directoryPath, port);
            Console.WriteLine(arguments);

            var info = new ProcessStartInfo(iisExpressPath)
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                ErrorDialog = true,
                LoadUserProfile = true,
                CreateNoWindow = false,
                UseShellExecute = false,
                Arguments = arguments
            };

            var startThread = new Thread(() => StartIisExpress(info))
            {
                IsBackground = true
            };
            startThread.Start();
        }

        /// <summary>
        /// 破棄する。
        /// </summary>
        /// <param name="disposing">破棄するかどうか</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposed)
            {
                return;
            }

            if (disposing && _process != null)
            {
                if (_process.HasExited == false)
                {
                    _process.Kill();
                }

                _process.Close();
                _process.Dispose();
            }

            _isDisposed = true;
        }

        /// <summary>
        /// IISExpressを起動する。
        /// </summary>
        /// <param name="info">プロセスの起動情報</param>
        private void StartIisExpress(ProcessStartInfo info)
        {
            try
            {
                _process = Process.Start(info);

                if (_process != null) _process.WaitForExit();

                if (0 != _process.ExitCode) throw new Exception("iis起動エラー");
            }
            catch (Exception)
            {
                Console.WriteLine("IISの初期化中にエラーが発生しました。");
                Dispose();
            }
        }
    }
}
