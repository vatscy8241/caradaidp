﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace MTI.CaradaTool.Exception
{
    /// <summary>
    /// アプリケーション致命例外クラス.
    /// </summary>
    [Serializable]
    public class AppFatalException : System.Exception
    {
        /// <summary>
        /// ファイル名.
        /// </summary>
        public string FileName { get; set; }

        public AppFatalException(string filePath, string message) : base(message)
        {
            this.FileName = Path.GetFileName(filePath);
        }

        public AppFatalException(string message, System.Exception original) : base(message, original)
        {

        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
