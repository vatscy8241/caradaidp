﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace MTI.CaradaTool.Exception
{
    /// <summary>
    /// API例外クラス.
    /// </summary>
    [Serializable]
    public class ApiException : System.Exception
    {
        /// <summary>
        /// CARADA ID.
        /// </summary>
        public string CaradaId { get; set; }

        /// <summary>
        /// HTTPステータスコード.
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// コンストラクタ.
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <param name="caradaId">CARADA ID</param>
        /// <param name="statusCode">HTTPステータスコード</param>
        public ApiException(string message, string caradaId, HttpStatusCode statusCode) : base(message)
        {
            this.CaradaId = caradaId;
            this.StatusCode = statusCode;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
