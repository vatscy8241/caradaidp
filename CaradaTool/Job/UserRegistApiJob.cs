﻿using Microsoft.VisualBasic.FileIO;
using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Logics;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Properties;
using MTI.CaradaTool.Resource;
using MTI.CaradaTool.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace MTI.CaradaTool.Job
{
    /// <summary>
    /// 利用開始前ユーザー登録API呼び出しJOB.
    /// </summary>
    public class UserRegistApiJob : IJob
    {
        /// <summary>
        /// コンソール・ログ出力ユーティリティ.
        /// </summary>
        private OutputUtil outputUtil = new OutputUtil();

        /// <summary>
        /// ファイルユーティリティ
        /// </summary>
        private FileUtil fileUtil = new FileUtil();

        /// <summary>
        /// JWT用シークレット情報.
        /// </summary>
        private JwtSecretInfo secretInfo;

        /// <summary>
        /// ツール用プロパティ.
        /// </summary>
        private ToolProperties propInfo;

        /// <summary>
        /// JWT使用API呼び出しクラス.
        /// </summary>
        private JwtApiRequest jwtRequest;

        /// <summary>
        /// CARADAID発行結果TSVファイルパス.
        /// </summary>
        private string outputTsvPath;

        /// <summary>
        /// CARADA ID入力件数.
        /// </summary>
        private int inputCount;

        /// <summary>
        /// 登録件数.
        /// </summary>
        private int registedCount;

        /// <summary>
        /// 失敗件数.
        /// </summary>
        private int failedCount;

        private static string[] outputTsvHeader = { "CARADA ID", "パスワード", "UID", "エラーコード", "エラー詳細" };

        /// <summary>
        /// コンストラクタ.
        /// </summary>
        /// <param name="secretInfo">JWT用シークレット情報</param>
        /// <param name="propInfo">ツール用プロパティ</param>
        public UserRegistApiJob(JwtSecretInfo secretInfo, ToolProperties propInfo)
        {
            this.secretInfo = secretInfo;
            this.propInfo = propInfo;
            this.jwtRequest = new JwtApiRequest(secretInfo, propInfo);
        }

        /// <summary>
        /// 処理実行.
        /// </summary>
        public virtual void Execute()
        {
            try
            {
                // 入力TSVファイル名を取得する
                var inputFileName = GetInputFileNameIfValid();

                // CARADAID発行結果TSVファイルパス生成
                outputTsvPath = Path.Combine(Settings.Default.ResultTsvPath, CreateOutputTsvFileName(inputFileName));

                using (var parser = GetTsvParser(inputFileName))
                {
                    bool isHeader = true;
                    while (parser.EndOfData == false)
                    {
                        string[] cols = parser.ReadLine().Split('\t');

                        if (isHeader)
                        {
                            // ヘッダーだけ確認して、TSVのカラム数が所定のカラム数でない
                            if (cols.Length != 1)
                            {
                                throw new AppFatalException(inputFileName, ErrorMessage.ERR_TSV_COL_NUM);
                            }
                            // ヘッダーはスキップ
                            isHeader = false;
                            continue;
                        }

                        // 入力件数インクリメント
                        inputCount++;

                        // CARADA ID形式チェック
                        if (!IsValidCaradaId(cols[0]))
                        {
                            continue;
                        }

                        // API実行
                        if (!ExecuteApi(cols[0]))
                        {
                            break;
                        }
                    }
                }

            }
            catch (ApiException apie)
            {
                // エラー行出力して終了
                OutputTsv(new OutputTsvModel
                {
                    CaradaId = apie.CaradaId,
                    ErrCd = ((int)apie.StatusCode).ToString(),
                    ErrDetail = apie.Message
                }, true);
            }
        }

        /// <summary>
        /// API実行.
        /// </summary>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <returns>次の処理に移る場合、true.処理をやめる場合、false.</returns>
        private bool ExecuteApi(string caradaId)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("carada_id", caradaId);
            parameters.Add("client_id", propInfo.ClientId);
            var tryCount = 1;

            outputUtil.OutputLog(LogLevelEnum.Info, "Execute API : {0}", propInfo.Uri);

            while (true)
            {
                outputUtil.OutputLog(LogLevelEnum.Info, "API Request : params:{0} TryCount:{1}",
                    JsonConvert.SerializeObject(parameters), tryCount.ToString());

                // API実行
                var response = jwtRequest.PostJson(parameters);

                // ステータスコード200(OK)の場合
                if (response.IsSuccessStatusCode)
                {
                    SuccessProc(response, caradaId, tryCount);
                    return true;
                }
                // ステータスコード400(Bad Request)の場合
                if (HttpStatusCode.BadRequest.Equals(response.StatusCode))
                {
                    ApiErrorProc(response, caradaId, tryCount);
                    return true;
                }
                // ステータスコード503(Service Unavailable)の場合
                if (HttpStatusCode.ServiceUnavailable.Equals(response.StatusCode))
                {
                    // 処理続行不能
                    // エラー行出力
                    ApiErrorProc(response, caradaId, tryCount);
                    return false;
                }

                // 上記以外は想定したJSONが取得出来ないので、Responseをそのままログ出力
                outputUtil.OutputLog(LogLevelEnum.Info, string.Format("API Response : {0} TryCount:{1}", response, tryCount));

                // ステータスコード504(Gateway Timeout)の場合
                if (HttpStatusCode.GatewayTimeout.Equals(response.StatusCode))
                {
                    // API実行リトライ回数だけリトライする
                    if (Settings.Default.ApiRetryTimes > tryCount)
                    {
                        tryCount++;
                        continue;
                    }
                    // 処理続行不能
                    // エラー行出力
                    throw new ApiException(ErrorMessage.ERR_DETAIL_SERVER_ERR, caradaId, response.StatusCode);
                }
                // 上記以外のステータスコードの場合(処理続行不能)
                // エラー行出力
                throw new ApiException(ErrorMessage.ERR_DETAIL_SERVER_ERR, caradaId, response.StatusCode);

            }
        }

        /// <summary>
        /// API成功時処理.
        /// </summary>
        /// <param name="response">レスポンス</param>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <param name="tryCount">API実行回数</param>
        private void SuccessProc(HttpResponseMessage response, string caradaId, int tryCount)
        {
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);
            outputUtil.OutputApiResultWithMask(LogLevelEnum.Info, apiResult,
                "API Response : {0} TryCount:" + tryCount);

            // インプットとアウトプットのCARADA IDが合致するかチェック
            if (caradaId.Equals((string)apiResult["carada_id"]))
            {
                // 正常行出力
                OutputTsv(new OutputTsvModel
                {
                    CaradaId = caradaId,
                    Password = (string)apiResult["password"],
                    Uid = (string)apiResult["uid"]
                });
            }
            else
            {
                // エラー行出力(例外的)
                OutputTsv(new OutputTsvModel
                {
                    CaradaId = caradaId,
                    Password = (string)apiResult["password"],
                    Uid = (string)apiResult["uid"],
                    ErrDetail = string.Format(ErrorMessage.ERR_DETAIL_CARADA_ID_MISMATCH, caradaId, (string)apiResult["carada_id"])
                });
            }
        }

        /// <summary>
        /// APIエラー時処理.
        /// </summary>
        /// <param name="response">レスポンス</param>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <param name="tryCount">API実行回数</param>
        private void ApiErrorProc(HttpResponseMessage response, string caradaId, int tryCount)
        {
            var apiResult = JToken.Parse(response.Content.ReadAsStringAsync().Result);
            outputUtil.OutputApiResultWithMask(LogLevelEnum.Info, apiResult,
                "API Response : {0} TryCount:" + tryCount);

            var errDetail = ((string)apiResult["original_error"] + " " + (string)apiResult["error_description"]).Trim();
            // エラー行出力
            OutputTsv(new OutputTsvModel
            {
                CaradaId = caradaId,
                ErrCd = (string)apiResult["error"],
                ErrDetail = errDetail,
            }, true);
        }

        /// <summary>
        /// 本処理前に実行する処理.
        /// </summary>
        public virtual void BeforeExecute()
        {
            // 処理開始コンソール表示とログ出力
            outputUtil.OutputConsoleAndLog(LogLevelEnum.Info, string.Format(Message.MSG_JOB_START, secretInfo.Issuer));
        }

        /// <summary>
        /// 本処理後に実行する処理.
        /// </summary>
        public virtual void AfterExecute()
        {
            if (registedCount >= 0)
            {
                if (failedCount > 0)
                {
                    // 失敗ログ
                    outputUtil.OutputConsoleAndLog(
                    LogLevelEnum.Info, ErrorMessage.ERR_FAIL,
                        inputCount.ToString(), registedCount.ToString(), failedCount.ToString());
                }
                else
                {
                    // 正常ログ
                    outputUtil.OutputConsoleAndLog(
                        LogLevelEnum.Info, Message.MSG_JOB_SUCCESS,
                        inputCount.ToString(), registedCount.ToString());
                }
            }
        }

        /// <summary>
        /// エラーがなければ入力TSVファイル名を取得する.
        /// </summary>
        /// <remarks>
        /// エラーがある場合はAppFatalExceptionをthrowする.
        /// </remarks>
        /// <returns>入力TSVファイル名</returns>
        private string GetInputFileNameIfValid()
        {
            // 指定フォルダからファイル一覧を取得する
            var files = fileUtil.GetFileNames(Settings.Default.TsvPath);

            // ファイルが1つでない
            if (files.Count != 1)
            {
                throw new AppFatalException(Settings.Default.InputTsvFileName, ErrorMessage.ERR_FILE_READ);
            }
            // ファイルが読み込めない
            if (fileUtil.IsFileLocked(Path.Combine(Settings.Default.TsvPath, files[0])))
            {
                throw new AppFatalException(files[0], ErrorMessage.ERR_FILE_READ);
            }
            // ファイル名形式が合致しない
            if (!Regex.IsMatch(files[0], Settings.Default.InputTsvFileNameReg))
            {
                throw new AppFatalException(files[0], ErrorMessage.ERR_FILENAME);
            }
            return files[0];
        }

        /// <summary>
        /// CARADAID発行結果TSVファイル名生成.
        /// </summary>
        /// <remarks>
        /// 本関数到達時点で引数のTSVファイルが存在することは保証されている.
        /// </remarks>
        /// <param name="fileName">入力ファイル名.</param>
        /// <returns>CARADAID発行結果TSVファイル名</returns>
        private string CreateOutputTsvFileName(string fileName)
        {
            var sb = new StringBuilder();
            sb.Append(Path.GetFileNameWithoutExtension(fileName));
            sb.Append("結果_");
            sb.Append(OutputUtil.ExecDateTime);
            sb.Append(Path.GetExtension(fileName));
            return sb.ToString();
        }

        /// <summary>
        /// TsvParserを取得する.
        /// </summary>
        /// <remarks>
        /// 本関数到達時点で引数のTSVファイルが存在することは保証されている.
        /// </remarks>
        /// <param name="fileName">TSVファイル名</param>
        /// <returns>TsvParser</returns>
        private TextFieldParser GetTsvParser(string fileName)
        {
            var filePath = Path.Combine(Settings.Default.TsvPath, fileName);
            var parser = new TextFieldParser(filePath, Encoding.GetEncoding(Settings.Default.TsvEncode));
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters("\t");
            return parser;
        }

        /// <summary>
        /// CARADA ID検証.
        /// </summary>
        /// <param name="caradaId">インプットのCARADA ID</param>
        /// <returns>正しいCARADA IDの場合、true. 正しくない値・null・空の場合、false.</returns>
        private bool IsValidCaradaId(string caradaId)
        {
            // CARADA IDが空(または空行)
            if (string.IsNullOrEmpty(caradaId))
            {
                OutputTsv(new OutputTsvModel
                {
                    ErrDetail = ErrorMessage.ERR_DETAIL_CARADA_ID_REQUIRED
                }, true);
                return false;
            }
            // 形式が合致しない
            if ((caradaId.Length > Settings.Default.CaradaIdMaxLength)
                || !Regex.IsMatch(caradaId, Settings.Default.CaradaIdReg))
            {
                // エラー行出力
                OutputTsv(new OutputTsvModel
                {
                    CaradaId = caradaId,
                    ErrDetail = ErrorMessage.ERR_DETAIL_CARADA_ID_FORMAT
                }, true);
                return false;
            }
            return true;
        }

        /// <summary>
        /// CARADAID発行結果TSVファイル出力.
        /// </summary>
        /// <param name="model">CARADAID発行結果TSVモデル</param>
        /// <param name="isError">エラー行出力の時、true.</param>
        /// <param name="isBlank">空行出力の時、true.</param>
        private void OutputTsv(OutputTsvModel model, bool isError = false, bool isBlank = false)
        {
            // まだ1行も書き込まれていなければ、ヘッダーを出力する
            if ((registedCount == 0) && (failedCount == 0))
            {
                // 出力先フォルダが存在しなければ作る
                fileUtil.CreateDirectoryIfNotExist(Settings.Default.ResultTsvPath);

                var header = new OutputTsvModel();
                header.CaradaId = outputTsvHeader[0];
                header.Password = outputTsvHeader[1];
                header.Uid = outputTsvHeader[2];
                header.ErrCd = outputTsvHeader[3];
                header.ErrDetail = outputTsvHeader[4];
                WriteTsv(header);
            }

            // データ行出力
            WriteTsv(model);

            if (!isBlank)
            {
                if (isError)
                {
                    failedCount++;
                }
                else
                {
                    registedCount++;
                }
            }
        }

        /// <summary>
        /// TSVファイル出力.
        /// </summary>
        /// <param name="model">CARADAID発行結果TSVモデル</param>
        private void WriteTsv(OutputTsvModel model)
        {
            try
            {
                fileUtil.WriteLine(outputTsvPath,
                    string.Format("{0}\t{1}\t{2}\t{3}\t{4}", model.CaradaId, model.Password, model.Uid, model.ErrCd, model.ErrDetail),
                    Settings.Default.TsvEncode);
            }
            catch (System.Exception e)
            {
                throw new AppFatalException(ErrorMessage.ERR_TSV_OUTPUT, e);
            }
        }
    }
}
