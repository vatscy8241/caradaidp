﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTI.CaradaTool.Utils
{
    /// <summary>
    /// ファイル取扱ユーティリティクラス.
    /// </summary>
    public class FileUtil
    {
        /// <summary>
        /// 指定パスのディレクトリ内のファイル名のリストを取得する.
        /// </summary>
        /// <param name="dirPath">ディレクトリのパス</param>
        /// <returns>ディレクトリ内のファイル名のリスト.
        /// ディレクトリが存在しないか、ディレクトリ内が空の時は空のリスト.
        /// </returns>
        public virtual List<string> GetFileNames(string dirPath)
        {
            var results = new List<string>();

            // ディレクトリが存在しない場合は空で返す.
            if (Directory.Exists(dirPath))
            {
                foreach (string filePath in Directory.GetFiles(dirPath))
                {
                    results.Add(Path.GetFileName(filePath));
                }
            }
            return results;
        }

        /// <summary>
        /// ファイルがロックされていないかチェックする.
        /// </summary>
        /// <param name="path">対象のファイルパス</param>
        /// <returns>ロックされている場合、true.</returns>
        public virtual bool IsFileLocked(string path)
        {
            FileStream stream = null;

            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch
            {
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
            return false;
        }

        /// <summary>
        /// 指定ディレクトリが存在しなければ作る.
        /// </summary>
        /// <param name="path">ディレクトリのパス</param>
        public virtual void CreateDirectoryIfNotExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// 空のファイルを作る.
        /// </summary>
        /// <param name="path">ファイルのパス</param>
        public virtual void Touch(string path)
        {
            if (!File.Exists(path))
            {
                var toucher = new StreamWriter(path);
                toucher.Close();
            }
        }

        /// <summary>
        /// ファイルに1行書き込む.
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <param name="line">書き込む文字列</param>
        /// <param name="encode">エンコード</param>
        public virtual void WriteLine(string filePath, string line, string encode)
        {
            using (var sw = new StreamWriter(filePath, true, Encoding.GetEncoding(encode)))
            {
                sw.WriteLine(line);
            }
        }
    }
}
