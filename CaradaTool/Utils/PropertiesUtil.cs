﻿using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;
using System.Text;

namespace MTI.CaradaTool.Utils
{
    /// <summary>
    /// プロパティファイル用ユーティリティクラス.
    /// </summary>
    public class PropertiesUtil
    {
        /// <summary>
        /// プロパティファイルの内容を辞書として取得する.
        /// </summary>
        /// <remarks>
        /// key(delimiter)valueで設定されている想定なので、delimiter区切りで2カラム無い場合は辞書には加えない。
        /// </remarks>
        /// <param name="filePath">プロパティファイルのパス</param>
        /// <param name="delimiter">keyとvalueのデリミタ.デフォルトはタブ区切り.</param>
        /// <param name="encode">プロパティファイルの文字コード.デフォルトはShift_JIS.</param>
        /// <returns></returns>
        public virtual Dictionary<string, string> GetProperties(string filePath, string delimiter = "\t", string encode = "Shift_JIS")
        {
            var dic = new Dictionary<string, string>();
            using (var parser = new TextFieldParser(filePath, Encoding.GetEncoding(encode)))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                while (parser.EndOfData == false)
                {
                    string[] cols = parser.ReadFields();
                    if (cols.Length == 2)
                    {
                        dic.Add(cols[0], cols[1]);
                    }
                }
            }
            return dic;
        }
    }
}
