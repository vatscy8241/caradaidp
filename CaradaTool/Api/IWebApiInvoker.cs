﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;

namespace MTI.CaradaTool.Api
{
    /// <summary>
    /// インターフェース
    /// このインターフェースを WebApiInvoker が継承します。
    /// 自動単体テストでモックを利用するためにインターフェースを継承しています。
    /// </summary>
    /// <remarks>UTクラスなし</remarks>
    public interface IWebApiInvoker
    {
        HttpResponseMessage PostJson(string url, Dictionary<string, List<string>> headerValues, JObject parameter);
    }
}
