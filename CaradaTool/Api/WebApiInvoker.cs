using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MTI.CaradaTool.Api
{
    /// <summary>
    /// 統合認可プロバイダと連携プラットフォームの API 呼び出しを行います。
    /// インターフェース IWebApiInvoker を継承している理由は自動単体テストでモック オブジェクトに置き換えが可能とするためです。
    /// </summary>
    public class WebApiInvoker : IWebApiInvoker, IDisposable
    {
        /// <summary>
        /// HttpClient
        /// </summary>
        private HttpClient httpClient = new HttpClient();


        /// <summary>
        /// Web API の POST メソッドを呼び出します。リクエストBody を JSON で送信します。
        /// </summary>
        /// <param name="url">リクエストのURL</param>
        /// <param name="headerValues">リクエスト ヘッダに挿入したい名前、値ペア</param>
        /// <param name="authorizationHeader">認証ヘッダに登録する値</param>
        /// <param name="parameters">送信するパラメータ</param>
        /// <returns>Web API 呼び出しの結果を HttpResponseMessage 型で返します。</returns>
        public virtual HttpResponseMessage PostJson(string url, Dictionary<string, List<string>> headerValues, JObject parameter)
        {
#if DEBUG
            //SSL 証明書のチェックをディセーブルにします。
            ServicePointManager.ServerCertificateValidationCallback = (sndr, certificate, chain, sslPolicyErrors) => true;
#endif
            using (var httpClient = new HttpClient())
            {
                AddHedderValue(httpClient.DefaultRequestHeaders, headerValues, url);

                var responseTask = httpClient.PostAsJsonAsync(url, parameter);
                return responseTask.Result;
            }
        }

        private static void AddHedderValue(HttpRequestHeaders headers, Dictionary<string, List<string>> headerValues, string url)
        {
            if (headerValues != null)
            {
                foreach (string key in headerValues.Keys)
                {
                    //User-Agent の追加はエラーになるため スキップ
                    if (key.ToLower() == "user-agent")
                    {
                        continue;
                    }
                    // HOST対応
                    if (key.ToLower() == "host")
                    {
                        var uri = new Uri(url);
                        headers.Add(key, new List<string>() { uri.Host });
                        continue;
                    }

                    var values = headerValues[key];
                    headers.Add(key, values);
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                httpClient.Dispose();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}