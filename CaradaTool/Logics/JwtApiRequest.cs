﻿using MTI.CaradaTool.Api;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Properties;
using MTI.CaradaTool.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Net.Http;
using System.Security.Claims;
using System.Text;

namespace MTI.CaradaTool.Logics
{
    /// <summary>
    /// JWTを使用したAPIを呼び出すクラス.
    /// </summary>
    public class JwtApiRequest
    {
        private OutputUtil outputUtil = new OutputUtil();

        private JwtSecretInfo secretInfo;
        private ToolProperties propInfo;
        private IWebApiInvoker webApiInvoker;


        public JwtApiRequest(JwtSecretInfo secretInfo, ToolProperties propInfo, IWebApiInvoker webApiInvoker)
        {
            this.secretInfo = secretInfo;
            this.propInfo = propInfo;
            this.webApiInvoker = webApiInvoker;
        }

        public JwtApiRequest(JwtSecretInfo secretInfo, ToolProperties propInfo)
            : this(secretInfo, propInfo, new WebApiInvoker())
        {
        }

        /// <summary>
        /// POSTによってAPIを実行します。管理ツール用APIの規約に従い、JWTのtokenをパラメータとします。
        /// </summary>
        /// <param name="parameters">APIパラメータ</param>
        /// <returns>Response</returns>
        public virtual HttpResponseMessage PostJson(IDictionary<string, object> parameters = null)
        {
            var credentials = new SigningCredentials(
                GetHmacSha256SigningKey(),
                SecurityAlgorithms.HmacSha256Signature,
                SecurityAlgorithms.Sha256Digest);

            var iat = DateTime.UtcNow.ToUnixTime();
            var exp = iat + Settings.Default.AdditionalDefault;
            var nbf = iat - Settings.Default.AdditionalDefault;

            var jwtHeader = new JwtHeader(credentials);
            var jwtPayload = new JwtPayload();

            var claims = new[] {
                new Claim("iss", secretInfo.Issuer),
                new Claim("iat", iat.ToString(), ClaimValueTypes.Integer64),
                new Claim("exp", exp.ToString(), ClaimValueTypes.Integer64),
                new Claim("nbf", nbf.ToString(), ClaimValueTypes.Integer64)
            };

            jwtPayload.AddClaims(claims);

            var token = new JwtSecurityToken(jwtHeader, jwtPayload);

            if (parameters != null)
            {
                token.Payload.Add("params", parameters);
            }

            var tokenJson = new JwtSecurityTokenHandler().WriteToken(token);
            outputUtil.OutputLog(LogLevelEnum.Debug, string.Format("token : {0}", tokenJson));

            var response = webApiInvoker.PostJson(
                propInfo.Uri,
                new Dictionary<string, List<string>> { },
                new JObject
                {
                    {"token", tokenJson}
                });
            return response;
        }

        private SymmetricSecurityKey GetHmacSha256SigningKey()
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(secretInfo.SecretKey);
            if (keyBytes.Length < 64)
            {
                Array.Resize(ref keyBytes, 64);
            }

            return new InMemorySymmetricSecurityKey(keyBytes);
        }
    }
}