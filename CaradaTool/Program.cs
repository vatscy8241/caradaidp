﻿using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Job;
using MTI.CaradaTool.Models;
using MTI.CaradaTool.Properties;
using MTI.CaradaTool.Resource;
using MTI.CaradaTool.Utils;
using System;
using System.IO;

namespace MTI.CaradaTool
{
    /// <summary>
    /// CARADA IdP 企業管理者用ツール メインプログラム.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// コンソール・ログ出力ユーティリティクラス.
        /// </summary>
        private static OutputUtil outputUtil = new OutputUtil();

        /// <summary>
        /// JWT用シークレット情報クラス.
        /// </summary>
        private static JwtSecretInfo secretInfo;

        /// <summary>
        /// ツール用プロパティクラス.
        /// </summary>
        private static ToolProperties propInfo;

        /// <summary>
        /// Jobインターフェース.
        /// </summary>
        private static IJob job;

        /// <summary>
        /// エントリポイント.
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        public static void Main(string[] args)
        {
            try
            {
                // アプリケーション名出力
                outputUtil.OutputConsoleAndLog(LogLevelEnum.Info, GetMyAppName(args));

                if (secretInfo == null)
                {
                    secretInfo = new JwtSecretInfo(Settings.Default.SecretPath);
                }
                if (propInfo == null)
                {
                    propInfo = new ToolProperties(
                        Path.Combine(Settings.Default.PropPath, GetPropertiesFileName(args)));
                }
                // UT用にここでnullチェックしている
                if (job == null)
                {
                    // コマンドライン引数取得(引数は1個しか受け付けない。0個の場合はCARADAID発行ツール起動。)
                    if (args.Length == 1)
                    {
                        switch (args[0])
                        {
                            case "ResetUserInfoApiJob":
                                job = new ResetUserInfoApiJob(secretInfo, propInfo);
                                break;
                            default:
                                break;
                        }
                    }
                    if (job == null)
                    {
                        job = new UserRegistApiJob(secretInfo, propInfo);
                    }
                }

                job.BeforeExecute();
                job.Execute();
                job.AfterExecute();
            }
            catch (AppFatalException afe)
            {
                if (!string.IsNullOrEmpty(afe.FileName))
                {
                    outputUtil.OutputConsoleAndLog(LogLevelEnum.Error, ErrorMessage.ERR_FILE_FATAL, afe.FileName, afe.Message);
                }
                else
                {
                    outputUtil.OutputConsoleAndLog(LogLevelEnum.Error, afe.Message, afe.InnerException);
                }
            }
            catch (System.Exception e)
            {
                outputUtil.OutputConsoleAndLog(LogLevelEnum.Error, ErrorMessage.ERR_UNKNOWN, e);
            }
#if CI
            outputUtil.OutputLog(LogLevelEnum.Debug, "CIで実行完了");
#else
            Console.ReadLine();
#endif
        }

        /// <summary>
        /// アプリケーション名(ツール名)取得.
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        /// <returns>アプリケーション名(ツール名)</returns>
        private static string GetMyAppName(string[] args)
        {
            if ((args != null) && (args.Length == 1))
            {
                switch (args[0])
                {
                    case "ResetUserInfoApiJob":
                        return Settings.Default.ResetUserInfoAppName;
                    default:
                        break;
                }
            }
            return Settings.Default.UserRegistAppName;
        }

        /// <summary>
        /// ツール用プロパティファイル名取得.
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        /// <returns>ツール用プロパティファイル名</returns>
        private static string GetPropertiesFileName(string[] args)
        {
            if ((args != null) && (args.Length == 1))
            {
                switch (args[0])
                {
                    case "ResetUserInfoApiJob":
                        return Settings.Default.ResetUserInfoPropFileName;
                    default:
                        break;
                }
            }
            return Settings.Default.UserRegistPropFileName;
        }
    }
}
