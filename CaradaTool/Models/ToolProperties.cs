﻿using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Properties;
using MTI.CaradaTool.Resource;
using MTI.CaradaTool.Utils;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MTI.CaradaTool.Models
{
    /// <summary>
    /// ツール用プロパティクラス.
    /// </summary>
    public class ToolProperties
    {
        /// <summary>
        /// プロパティの辞書.
        /// </summary>
        protected Dictionary<string, string> properties;

        /// <summary>
        /// プロパティファイルパス.
        /// </summary>
        protected string filePath;

        /// <summary>
        /// ファイルユーティリティクラス.
        /// </summary>
        private FileUtil fileUtil = new FileUtil();

        /// <summary>
        /// プロパティファイルユーティリティクラス.
        /// </summary>
        private PropertiesUtil propertiesUtil = new PropertiesUtil();

        /// <summary>
        /// クライアントID
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// APIのURI
        /// </summary>
        public string Uri { get; set; }

        /// <summary>
        /// CARADAID登録ツールの必須プロパティキー名
        /// </summary>
        private static readonly string[] UserRegistPropKeyName = { Settings.Default.ToolPropertiesClientId, Settings.Default.ToolPropertiesUri };

        /// <summary>
        /// ユーザー情報リセットツールの必須プロパティキー名
        /// </summary>
        private static readonly string[] ResetUserInfoPropKeyName = { Settings.Default.ToolPropertiesUri };

        /// <summary>
        /// コンストラクタ.
        /// </summary>
        /// <remarks>
        /// 指定ファイルにエラーがある場合は例外をthrowする.
        /// </remarks>
        /// <param name="filePath">指定ファイルのパス</param>
        public ToolProperties(string filePath)
        {
            this.filePath = filePath;
            SetProperties();
        }

        /// <summary>
        /// プロパティ設定.
        /// </summary>
        protected void SetProperties()
        {
            // 指定フォルダにファイルが存在しないか、開けない.
            if (!File.Exists(filePath) || fileUtil.IsFileLocked(filePath))
            {
                throw new AppFatalException(filePath, ErrorMessage.ERR_FILE_READ);
            }
            properties = propertiesUtil.GetProperties(filePath);

            // プロパティ妥当性検証
            ValidateProperty();

            // プロパティ設定
            SetMyProperties();
        }

        /// <summary>
        /// プロパティ妥当性検証.
        /// </summary>
        protected void ValidateProperty()
        {
            if (IsRequiredProperty(Settings.Default.ToolPropertiesClientId)
                && !properties.ContainsKey(Settings.Default.ToolPropertiesClientId))
            {
                throw new AppFatalException(filePath, string.Format(ErrorMessage.ERR_TSV_PROP_KEY, Settings.Default.ToolPropertiesClientId));
            }
            if (IsRequiredProperty(Settings.Default.ToolPropertiesUri)
                && !properties.ContainsKey(Settings.Default.ToolPropertiesUri))
            {
                throw new AppFatalException(filePath, string.Format(ErrorMessage.ERR_TSV_PROP_KEY, Settings.Default.ToolPropertiesUri));
            }
        }

        /// <summary>
        /// プロパティ設定.
        /// </summary>
        protected void SetMyProperties()
        {
            if (IsRequiredProperty(Settings.Default.ToolPropertiesClientId))
            {
                ClientId = properties[Settings.Default.ToolPropertiesClientId];
            }
            if (IsRequiredProperty(Settings.Default.ToolPropertiesUri))
            {
                Uri = properties[Settings.Default.ToolPropertiesUri];
            }
        }

        /// <summary>
        /// 指定キーが各ツールのプロパティファイルで必須か？
        /// </summary>
        /// <param name="key">プロパティのキー</param>
        /// <returns>必須の場合、true</returns>
        protected bool IsRequiredProperty(string key)
        {
            var fileName = Path.GetFileName(filePath);
            if (fileName.Equals(Settings.Default.UserRegistPropFileName))
            {
                return UserRegistPropKeyName.Contains(key);
            }
            else if (fileName.Equals(Settings.Default.ResetUserInfoPropFileName))
            {
                return ResetUserInfoPropKeyName.Contains(key);
            }
            return false;
        }
    }
}