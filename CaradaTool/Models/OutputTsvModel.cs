﻿namespace MTI.CaradaTool.Models
{
    /// <summary>
    /// CARADAID発行結果TSVモデル.
    /// </summary>
    /// <remarks>UTクラスなし</remarks>
    public class OutputTsvModel
    {
        /// <summary>
        /// CARADA ID.
        /// </summary>
        public string CaradaId { get; set; } = string.Empty;

        /// <summary>
        /// パスワード.
        /// </summary>
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// UID.
        /// </summary>
        public string Uid { get; set; } = string.Empty;

        /// <summary>
        /// エラーコード.
        /// </summary>
        public string ErrCd { get; set; } = string.Empty;

        /// <summary>
        /// エラー詳細.
        /// </summary>
        public string ErrDetail { get; set; } = string.Empty;
    }
}
