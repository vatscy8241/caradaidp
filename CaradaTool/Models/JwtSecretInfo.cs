﻿using MTI.CaradaTool.Exception;
using MTI.CaradaTool.Properties;
using MTI.CaradaTool.Resource;
using MTI.CaradaTool.Utils;
using System.Collections.Generic;
using System.IO;

namespace MTI.CaradaTool.Models
{
    /// <summary>
    /// JWT用シークレット情報クラス.
    /// </summary>
    public class JwtSecretInfo
    {
        /// <summary>
        /// プロパティの辞書.
        /// </summary>
        protected Dictionary<string, string> properties;

        /// <summary>
        /// プロパティファイルパス.
        /// </summary>
        protected string filePath;

        /// <summary>
        /// ファイルユーティリティクラス.
        /// </summary>
        private FileUtil fileUtil = new FileUtil();

        /// <summary>
        /// プロパティファイルユーティリティクラス.
        /// </summary>
        private PropertiesUtil propertiesUtil = new PropertiesUtil();

        /// <summary>
        /// JWTのissとなる発行者名
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// JWT署名キー
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// コンストラクタ.
        /// </summary>
        /// <remarks>
        /// 指定ファイルにエラーがある場合は例外をthrowする.
        /// </remarks>
        /// <param name="filePath">指定ファイルのパス</param>
        public JwtSecretInfo(string filePath)
        {
            this.filePath = filePath;
            SetProperties();
        }

        /// <summary>
        /// プロパティ設定.
        /// </summary>
        protected void SetProperties()
        {
            // 指定フォルダにファイルが存在しないか、開けない.
            if (!File.Exists(filePath) || fileUtil.IsFileLocked(filePath))
            {
                throw new AppFatalException(filePath, ErrorMessage.ERR_FILE_READ);
            }
            properties = propertiesUtil.GetProperties(filePath);

            // プロパティ妥当性検証
            ValidateProperty();

            // 継承側プロパティ設定
            SetMyProperties();
        }

        /// <summary>
        /// プロパティ妥当性検証.(オーバーライド)
        /// </summary>
        protected void ValidateProperty()
        {
            if (!properties.ContainsKey(Settings.Default.JwtSecretInfoIssuser))
            {
                throw new AppFatalException(filePath, string.Format(ErrorMessage.ERR_TSV_PROP_KEY, Settings.Default.JwtSecretInfoIssuser));
            }
            if (!properties.ContainsKey(Settings.Default.JwtSecretInfoSecretKey))
            {
                throw new AppFatalException(filePath, string.Format(ErrorMessage.ERR_TSV_PROP_KEY, Settings.Default.JwtSecretInfoSecretKey));
            }
        }

        /// <summary>
        /// プロパティ設定.(オーバーライド)
        /// </summary>
        protected void SetMyProperties()
        {
            Issuer = properties[Settings.Default.JwtSecretInfoIssuser];
            SecretKey = properties[Settings.Default.JwtSecretInfoSecretKey];
        }
    }
}