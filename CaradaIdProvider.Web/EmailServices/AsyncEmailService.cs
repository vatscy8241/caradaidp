﻿using Microsoft.AspNet.Identity;
using MTI.CaradaIdProvider.Web.Properties;
using MTI.CaradaIdProvider.Web.Tasks;
using NLog;
using System;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.EmailServices
{
    /// <summary>
    /// 非同期EmailServiceです。
    /// </summary>
    public class AsyncEmailService : IIdentityMessageService
    {
        /// <summary>
        /// ログ
        /// </summary>
        private static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        // テスト用
        private SmtpClient _client;

        /// <summary>
        /// Smtpクライアント
        /// </summary>
        protected SmtpClient Client
        {
            get
            {
                return _client ?? new SmtpClient();
            }
            set { _client = value; }
        }

        /// <summary>
        /// 非同期処理
        /// </summary>
        private IBackGroundProcessor processor = new WebHostingBackGroundProcessor();

        /// <summary>
        /// 完全非同期でメールを送信します。そのため、呼出元でawaitを行っていても、直ちに制御を戻します。
        /// </summary>
        /// <remarks>
        /// エラーが発生した場合、ログに出力しタスクを終了します。
        /// その際、HttpContextから出力している情報はすべて空欄になる点に注意してください。
        /// SmtpClientは都度生成します。
        /// </remarks>
        /// <param name="message">メッセージ</param>
        /// <returns>タスク</returns>
        public virtual Task SendAsync(IdentityMessage message)
        {
            processor.QueueBackGroundWorkItem(c =>
            {
                // サーバーアプリケーションの終了などの場合
                if (c.IsCancellationRequested)
                {
                    LOG.Warn($"メール送信のキャンセルが要求されました。件名:{message.Subject}");
                    return;
                }
                try
                {
                    if (!BeforeSend(message))
                    {
                        LOG.Warn($"送信前処理の結果によりメール送信が取り消されました。件名:{message.Subject}");
                        return;
                    }

                    var mail = new MailMessage();
                    mail.To.Add(new MailAddress(message.Destination));

                    Encoding enc = Encoding.GetEncoding("iso-2022-jp");
                    // http://blogs.technet.com/b/exchangeteamjp/archive/2012/10/05/3524293.aspx
                    // .Net Framework 4.5以降ではencodeを2回施すとのこと
                    mail.Subject = BEncode(BEncode(message.Subject, enc), enc);

                    mail.Body = message.Body;
                    mail.IsBodyHtml = false;

                    Client.Send(mail);
                }
                catch (Exception e)
                {
                    LOG.Error(e, $"メール送信の非同期処理でエラーが発生しました。件名:{message.Subject}");
                }
            });
            return Task.FromResult(0);
        }

        /// <summary>
        /// メッセージヘッダ用にエンコーディングを施す。
        /// </summary>
        /// <param name="str">入力文字列</param>
        /// <param name="encoding">encoding</param>
        /// <returns>エンコード後文字列</returns>
        private static string BEncode(string str, Encoding encoding)
        {
            return string.Format("=?{0}?B?{1}?=", encoding.HeaderName, Convert.ToBase64String(encoding.GetBytes(str)));
        }

        /// <summary>
        /// 送信前処理
        /// 送信後処理については、適宜SendCompletedイベントを使用すること。
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <returns>true：送信を行う false：送信を行わない</returns>
        protected virtual bool BeforeSend(IdentityMessage message)
        {
            return true;
        }
    }
}