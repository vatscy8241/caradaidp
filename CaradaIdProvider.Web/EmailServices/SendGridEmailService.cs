﻿using Microsoft.AspNet.Identity;
using Microsoft.Azure;
using Microsoft.Owin.Infrastructure;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.Properties;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;

namespace MTI.CaradaIdProvider.Web.EmailServices
{
    /// <summary>
    /// SendGrid用のメールサービス
    /// </summary>
    public class SendGridEmailService : AsyncEmailService
    {
        /// <summary>
        /// 対象URL
        /// </summary>
        private string _uri = CloudConfigurationManager.GetSetting("SendGridApiUri");

        private string apiUser;
        private string apiKey;

        /// <summary>
        /// DbContextFactory
        /// </summary>
        private static readonly DbContextFactory DB_CONTEXT_FACTORY = new DbContextFactory();
        /// <summary>
        /// タイムアウト
        /// </summary>
        private const int TIMEOUT = 10000;

        /// <summary>
        /// WebRequestを生成するDelegate
        /// </summary>
        private Func<string, WebRequest> CreateWebRequest = u => WebRequest.Create(u);

        /// <summary>
        /// ログ
        /// </summary>
        protected static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// APIの必須パラメータをWeb.configから取得するコンストラクタ。
        /// </summary>
        public SendGridEmailService()
            : base()
        {
            var config = WebConfigurationManager.OpenWebConfiguration(HostingEnvironment.ApplicationVirtualPath);
            var section = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
            apiUser = section.Smtp.Network.UserName;
            apiKey = section.Smtp.Network.Password;
        }

        /// <summary>
        /// SendGridのバウンスリストを検索し、バウンスリストに対象メッセージが存在すれば削除する。
        /// APIが失敗したとしても送信は試行します。
        /// </summary>
        /// <param name="message">メッセージ</param>
        /// <returns>
        /// 常に「true：送信を行う」
        /// </returns>
        protected override bool BeforeSend(IdentityMessage message)
        {
            var userId = string.Empty;

            using (var caradaIdPDb = DB_CONTEXT_FACTORY.NewCaradaIdPDbContext())
            {
                var entity = (from u in caradaIdPDb.Users
                              where u.Email == message.Destination
                              select new { u.Id, u.EmailConfirmed }).SingleOrDefault();

                if (entity != null)
                {
                    userId = entity.Id;
                    // 利用開始登録前の場合、ログに出力されたUserIdからTraceできないこともあるため出力する
                    LOG.Info($"UserId:{userId}へのメール送信を開始します。件名:{message.Subject}, 利用開始登録済み:{entity.EmailConfirmed}");
                }
                else
                {
                    LOG.Info($"DBに登録されていないユーザーへの送信が要求されました。件名:{message.Subject}");
                }
            }

            var parameters = new Dictionary<string, string>();
            parameters.Add("api_user", apiUser);
            parameters.Add("api_key", apiKey);
            parameters.Add("email", message.Destination);

#if DEBUG
            ServicePointManager.ServerCertificateValidationCallback
                = (sender, certificate, chain, sslPolicyErrors) => { return true; };
#endif
            var bounceGet = RequestGet<List<BounceModel>>("bounces", "get", parameters);
            if (bounceGet == null)
            {
                LOG.Error($"SendGrid バウンスリスト取得エラー(UserId={userId})");
                return true;
            }
            if (bounceGet.Count == 0)
            {
                return true;
            }
            LOG.Info($"SendGrid バウンスリスト登録あり(UserId={userId})");

            var deleteResult = RequestPost<ResultModel>("bounces", "delete", parameters);
            if (deleteResult == null)
            {
                LOG.Error($"SendGrid バウンスリスト削除エラー(UserId={userId})");
                return true;
            }
            LOG.Info($"SendGrid バウンスリスト削除完了(UserId={userId})");

            return true;
        }

        /// <summary>
        /// SendGridのGETメソッドによるAPIを実行する。APIが失敗した場合はエラーログを出力し、nullを返却する。
        /// </summary>
        /// <typeparam name="Json">ResponseJson</typeparam>
        /// <param name="module">module</param>
        /// <param name="action">action</param>
        /// <param name="parameters">パラメータ</param>
        /// <returns>Jsonのオブジェクト</returns>
        private Json RequestGet<Json>(string module, string action, Dictionary<string, string> parameters)
        {
            var uri = string.Format(_uri, module, action);

            foreach (var p in parameters)
            {
                uri = WebUtilities.AddQueryString(uri, p.Key, p.Value);
            }

            // 特にAPIごとにHeaderをどうするとかはドキュメント記載がなかったので送信までしてしまう
            var req = CreateWebRequest(uri);
            req.Method = HttpMethod.Get.Method;
            req.Timeout = TIMEOUT;
            req.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            req.ContentLength = 0;

            try
            {
                var res = req.GetResponse() as HttpWebResponse;

                if (!IsSuccess(res.StatusCode))
                {
                    return default(Json);
                }

                using (var r = new StreamReader(res.GetResponseStream(), Encoding.UTF8))
                {
                    return JsonConvert.DeserializeObject<Json>(r.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                LOG.Error(e);
                return default(Json);
            }
        }

        /// <summary>
        /// SendGridのPOSTメソッドによるAPIを実行する。APIが失敗した場合はエラーログを出力し、nullを返却する。
        /// </summary>
        /// <typeparam name="Json">ResponseJson</typeparam>
        /// <param name="module">module</param>
        /// <param name="action">action</param>
        /// <param name="parameters">パラメータ</param>
        /// <returns>Jsonのオブジェクト</returns>
        private Json RequestPost<Json>(string module, string action, Dictionary<string, string> parameters)
        {
            var uri = string.Format(_uri, module, action);

            var body = string.Empty;
            foreach (var p in parameters)
            {
                if (body != string.Empty)
                {
                    body += "&";
                }
                body += string.Format("{0}={1}", p.Key, HttpUtility.UrlEncode(p.Value, Encoding.UTF8));
            }

            var req = CreateWebRequest(uri);
            req.Method = HttpMethod.Post.Method;
            req.Timeout = TIMEOUT;
            req.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            req.ContentLength = Encoding.ASCII.GetBytes(body).Length;

            var reqStream = req.GetRequestStream();
            using (var sw = new StreamWriter(reqStream))
            {
                sw.Write(body);
            }

            try
            {
                var res = req.GetResponse() as HttpWebResponse;

                if (!IsSuccess(res.StatusCode))
                {
                    return default(Json);
                }

                using (var r = new StreamReader(res.GetResponseStream(), Encoding.UTF8))
                {
                    return JsonConvert.DeserializeObject<Json>(r.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                LOG.Error(e);
                return default(Json);
            }
        }

        private bool IsSuccess(HttpStatusCode code)
        {
            // 2XXは成功
            // 失敗は4XX, 5XXがResponseされるとのことだが成功以外はすべて失敗とする
            // そもそも400番台はここに到達しないで例外になる
            return (code.CompareTo(HttpStatusCode.OK) >= 0 && code.CompareTo(HttpStatusCode.MultipleChoices) < 0);
        }

        /// <summary>
        /// Bounce
        /// </summary>
        public class BounceModel
        {
            /// <summary>
            /// status
            /// </summary>
            public string Status;
            /// <summary>
            /// created
            /// </summary>
            public string Created;
            /// <summary>
            /// reason
            /// </summary>
            public string Reason;
            /// <summary>
            /// email
            /// </summary>
            public string Email;
        }

        /// <summary>
        /// 更新系の結果Json
        /// </summary>
        public class ResultModel
        {
            /// <summary>
            /// message
            /// </summary>
            public string Message;
        }
    }
}