﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.EmailServices
{
    /// <summary>
    /// 二段階認証用Provider。
    /// </summary>
    /// <typeparam name="T">IUser<string></typeparam>
    public class ApplicationEmailTokenProvider<T> : EmailTokenProvider<T> where T : class, IUser<string>
    {
        /// <summary>
        /// 認証コードの妥当性検証をする。
        /// 0始まりの認証コードを正しく判定するため、認証コードの文字数が6文字であることを追加で検証している。
        /// </summary>
        /// <param name="purpose"></param>
        /// <param name="token"></param>
        /// <param name="manager"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public override Task<bool> ValidateAsync(string purpose, string token, UserManager<T, string> manager, T user)
        {
            if (token != null && (token.Length != 6 || token.Trim().Length != 6))
            {
                return Task.FromResult<bool>(false);
            }

            return base.ValidateAsync(purpose, token, manager, user);
        }
    }
}