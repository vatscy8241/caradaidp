﻿using Microsoft.AspNet.Identity;
using MTI.CaradaIdProvider.Web.Tasks;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MTI.CaradaIdProvider.Web.EmailServices
{
    /// <summary>
    /// 結合テスト用のEmailService。
    /// 結合テストでのみ使用される。
    /// </summary>
    public class IntegrationTestEmailService : SendGridEmailService
    {
        [Obsolete("結合テストでのみ使用が許可されています。")]
        public IntegrationTestEmailService() : base()
        {
            // processorがprotectedになっていないのはテスト以外では触るべきでないからです
            var field = typeof(AsyncEmailService).GetRuntimeFields().FirstOrDefault(f => f.Name == "processor");
            field.SetValue(this, new SyncBackGroundProcessor());
        }

        /// <summary>
        /// 特定のメールアドレスの場合、常に例外をthrowするBeforeSendです。
        /// </summary>
        /// <param name="message">メール情報</param>
        /// <returns>例外の発生しないメールアドレスの場合はSendGridEmailService.BeforeSend()の結果</returns>
        protected override bool BeforeSend(IdentityMessage message)
        {
            // 特定の宛先で特定の件名の場合に例外をthrowする
            if (message.Destination == "exception@test.mail.com" && message.Subject == "【CARADA ID】登録完了のお知らせ")
            {
                throw new IOException("例外発生用のメールアドレス");
            }
            return base.BeforeSend(message);
        }
    }
}