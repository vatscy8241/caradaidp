﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Xml.Linq;
using System.Xml.XPath;

namespace MTI.CaradaIdProvider.Web.Helpers
{
    /// <summary>
    /// Hiddenを出力しないCheckBoxヘルパー。
    /// Modelの該当Propertyはnull非許容のboolとすること。テーブルの中の1要素であるような使用法のCheckBoxには使用できない。
    /// </summary>
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// 指定された式で表されるオブジェクトの各プロパティについて、チェック ボックスの input 要素を返します。
        /// </summary>
        /// <typeparam name="TModel">モデルの型。</typeparam>
        /// <param name="htmlHelper">このメソッドによって拡張される HTML ヘルパー インスタンス。</param>
        /// <param name="expression">表示するプロパティを格納しているオブジェクトを識別する式。</param>
        /// <exception cref="System.ArgumentNullException">expression パラメーターが null です。   </exception>
        /// <returns>指定された式で表されるオブジェクトの各プロパティで、type 属性が "checkbox" に設定されている HTML の input 要素。</returns>
        public static MvcHtmlString CheckBoxFor2<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression)
        {
            return CheckBoxFor2(htmlHelper, expression, null);
        }

        /// <summary>
        /// 指定された式で表されるオブジェクトの各プロパティについて、指定された HTML 属性を使用して、チェック ボックスの input 要素を返します。
        /// </summary>
        /// <typeparam name="TModel">モデルの型。</typeparam>
        /// <param name="htmlHelper">このメソッドによって拡張される HTML ヘルパー インスタンス。</param>
        /// <param name="expression">表示するプロパティを格納しているオブジェクトを識別する式。</param>
        /// <param name="htmlAttributes">この要素に設定する HTML 属性を格納するオブジェクト。</param>
        /// <exception cref="System.ArgumentNullException">expression パラメーターが null です。   </exception>
        /// <returns>指定された式で表されるオブジェクトの各プロパティで、type 属性が "checkbox" に設定されている HTML の input 要素。</returns>
        public static MvcHtmlString CheckBoxFor2<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, object htmlAttributes)
        {
            return CheckBoxFor2(htmlHelper, expression, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }

        /// <summary>
        /// 指定された式で表されるオブジェクトの各プロパティについて、指定された HTML 属性を使用して、チェック ボックスの input 要素を返します。
        /// </summary>
        /// <typeparam name="TModel">モデルの型。</typeparam>
        /// <param name="htmlHelper">このメソッドによって拡張される HTML ヘルパー インスタンス。</param>
        /// <param name="expression">表示するプロパティを格納しているオブジェクトを識別する式。</param>
        /// <param name="htmlAttributes">この要素に設定する HTML 属性を格納しているディクショナリ。</param>
        /// <exception cref="System.ArgumentNullException">expression パラメーターが null です。   </exception>
        /// <returns>指定された式で表されるオブジェクトの各プロパティで、type 属性が "checkbox" に設定されている HTML の input 要素。</returns>
        public static MvcHtmlString CheckBoxFor2<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, IDictionary<string, object> htmlAttributes)
        {
            var mvcHtmlString = InputExtensions.CheckBoxFor<TModel>(htmlHelper, expression, htmlAttributes);
            var elements = XElement.Parse("<?xml version='1.0'?><root>" + mvcHtmlString.ToHtmlString() + "</root>").XPathSelectElements("//input[@type!='hidden']");

            return MvcHtmlString.Create(elements.Select(x => x.ToString()).SingleOrDefault());
        }
    }
}