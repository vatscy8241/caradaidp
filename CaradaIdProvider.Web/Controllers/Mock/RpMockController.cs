﻿using Microsoft.Azure;
using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.Logics;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.IdentityModel;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Controllers.Mock
{
    /// <summary>
    /// テスト用のRPのモック。
    /// RouteConfigにて、デバッグ以外では無効になるように設定済み。
    /// </summary>
    public class RpMockController : BaseWebController
    {
        // 文字コード
        private static Encoding ENC = Encoding.UTF8;

        /// <summary>
        /// rsa
        /// </summary>
        private RSAEncryptor rsa = new RSAEncryptor(CloudConfigurationManager.GetSetting("PublicKey"));

        /// <summary>
        /// Authentication Request用モック。
        /// </summary>
        /// <returns>View</returns>
        public ActionResult Auth()
        {
            // クライアント情報を取得
            var clientData = (from cm in CaradaIdPDb.ClientMasters
                              where cm.ClientName == Properties.Settings.Default.AuthProviderClientName
                              select new { ClientId = cm.ClientId }).First();

            // Authentication Requestで送信するURLを設定
            var scheme = HttpContext.Request.Url.Scheme;
            var domain = HttpContext.Request.Url.Authority;
            ViewBag.request_url = scheme + "://" + domain + "/oauth2/auth";
            // client_id
            ViewBag.client_id = clientData.ClientId;
            // scope。固定値
            ViewBag.scope = "openid";
            // response_type。固定値
            ViewBag.response_type = "code";
            // redirect_uri
            ViewBag.redirect_uri = scheme + "://" + domain + "/RpMock/Token";
            // state。ランダム生成
            var state = Guid.NewGuid().ToString("N");
            ViewBag.state = state;

            Session["mockSession"] = state;

            return View("Auth");
        }

        /// <summary>
        /// Token Request用モック。
        /// Authentication Responseのredirect先を兼ねている。
        /// </summary>
        /// <param name="model">(RpMockModel</param>
        /// <returns>View</returns>
        public ActionResult Token(RpMockModel model)
        {
            // Authentication Requestで送信したものを取得。
            if (Session["mockSession"] != null)
            {
                model.request_state = Session["mockSession"] as String;
                Session.Remove("mockSession");
            }
            // Authentication Responseがエラーの場合はエラー用の画面を表示
            if (model.error != null)
            {
                return View("AuthError", model);

            }

            // Token Requestで送信するデフォルト値を設定
            var clientData = (from cm in CaradaIdPDb.ClientMasters
                              where cm.ClientName == Properties.Settings.Default.AuthProviderClientName
                              select new { ClientId = cm.ClientId, ClientSecret = cm.ClientSecret }).First();
            // grant_type。固定値
            model.grant_type = "authorization_code";
            // code : Authentication Responseの値をそのまま利用
            // redirect_uri
            var scheme = HttpContext.Request.Url.Scheme;
            var domain = HttpContext.Request.Url.Authority;
            model.redirect_uri = scheme + "://" + domain + "/RpMock/Token";
            // client_id
            model.client_id = clientData.ClientId;
            // client_secret
            model.client_secret = clientData.ClientSecret;
            return View("Token", model);
        }

        /// <summary>
        /// Token Requestをサーバー間通信として実行するモック。
        /// </summary>
        /// <param name="model">RpMockModel</param>
        /// <returns>Token Response結果のView</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RequestToken(RpMockModel model)
        {
            var scheme = HttpContext.Request.Url.Scheme;
            var domain = HttpContext.Request.Url.Authority;
            var url = scheme + "://" + domain + "/oauth2/token";

            // POST送信する文字列を作成
            var ht = new Hashtable();
            ht["grant_type"] = HttpUtility.UrlEncode(model.grant_type, ENC);
            ht["code"] = HttpUtility.UrlEncode(model.code, ENC);
            ht["redirect_uri"] = HttpUtility.UrlEncode(model.redirect_uri, ENC);
            ht["client_id"] = HttpUtility.UrlEncode(model.client_id, ENC);
            ht["client_secret"] = HttpUtility.UrlEncode(model.client_secret, ENC);

            var param = "";
            foreach (var k in ht.Keys)
            {
                param += String.Format("{0}={1}&", k, ht[k]);
            }
            byte[] postDataBytes = Encoding.ASCII.GetBytes(param);

            ServicePointManager.ServerCertificateValidationCallback
                = (sender, certificate, chain, sslPolicyErrors) => { return true; };

            // リクエストの作成
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            req.ContentLength = postDataBytes.Length;

            // 送信するデータを書き込む
            var reqStream = req.GetRequestStream();
            using (var sw = new StreamWriter(reqStream))
            {
                sw.Write(param);
            }

            // 受信して表示
            try
            {
                var res = (HttpWebResponse)req.GetResponse();
                var resStream = res.GetResponseStream();
                ViewBag.code = res.StatusCode;
                using (var sr = new StreamReader(resStream, ENC))
                {
                    string json = sr.ReadToEnd();
                    ViewBag.responseBody = json;
                    // Response BodyのJSON
                    var result = JsonConvert.DeserializeObject<TokenResponseJson>(json);
                    ViewBag.access_token = result.access_token;
                    ViewBag.token_type = result.token_type;
                    ViewBag.expires_in = result.expires_in;
                    ViewBag.id_token = result.id_token;

                    if (!string.IsNullOrEmpty(ViewBag.id_token))
                    {
                        // Token IDを可視化
                        // "."で連結されているので分離
                        string[] jwt = result.id_token.Split('.');
                        // JWTヘッダ部分
                        ViewBag.jwt_head = Base64UrlToUTF8(jwt[0]);
                        // JWT本文
                        var jwtBody = Base64UrlToUTF8(jwt[1]);
                        ViewBag.jwt_body = jwtBody;
                        var payloadJson = JsonConvert.DeserializeObject<IdTokenPayloadJson>(jwtBody);
                        ViewBag.iss = payloadJson.iss;
                        ViewBag.sub = payloadJson.sub;
                        ViewBag.aud = payloadJson.aud;
                        ViewBag.exp = payloadJson.exp;
                        ViewBag.iat = payloadJson.iat;

                        // 時間をエポック秒から変換
                        var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                        var jstZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time");

                        var expUtc = epoch.AddSeconds(payloadJson.exp);
                        var expJst = TimeZoneInfo.ConvertTimeFromUtc(expUtc, jstZoneInfo);
                        var iatUtc = epoch.AddSeconds(payloadJson.iat);
                        var iatJst = TimeZoneInfo.ConvertTimeFromUtc(iatUtc, jstZoneInfo);
                        ViewBag.exp_utc = expUtc;
                        ViewBag.exp_jst = expJst;
                        ViewBag.iat_utc = iatUtc;
                        ViewBag.iat_jst = iatJst;
                        var utcNow = DateTime.UtcNow;
                        ViewBag.now_utc = utcNow;
                        ViewBag.now_jst = TimeZoneInfo.ConvertTimeFromUtc(utcNow, jstZoneInfo);

                        // JWT署名
                        ViewBag.jwt_signature = jwt[2];

                        // 署名検証
                        ViewBag.jwt_verify = VerifySignature(result.id_token);
                    }
                }
            }
            catch (WebException ex)
            {
                //HTTPプロトコルエラーかどうか調べる
                if (ex.Status == System.Net.WebExceptionStatus.ProtocolError)
                {
                    // HTTPステータスコードとResponseBodyを取得
                    var errorRes = (HttpWebResponse)ex.Response;
                    ViewBag.code = errorRes.StatusCode;
                    var resStream = errorRes.GetResponseStream();
                    using (var sr = new StreamReader(resStream, ENC))
                    {
                        ViewBag.responseBody = sr.ReadToEnd();
                    }
                }
                else
                {
                    ViewBag.code = "Exception発生";
                    ViewBag.responseBody = ex.ToString();
                }
            }

            return View("TokenResponse");
        }

        private string Base64UrlToUTF8(string target)
        {
            var base64str = target.Replace('-', '+').Replace('_', '/');
            var lastBytes = base64str.Length % 4;
            if (lastBytes > 0)
            {
                base64str = base64str.PadRight(base64str.Length + 4 - lastBytes, '=');
            }

            return Encoding.UTF8.GetString(Convert.FromBase64String(base64str));
        }

        private bool VerifySignature(string idToken)
        {
            var parameters = new TokenValidationParameters()
            {
                IssuerSigningToken = new RsaSecurityToken(rsa.ToRSA()),
                ValidateActor = false,
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false
            };

            SecurityToken outToken;
            try
            {
                new JwtSecurityTokenHandler().ValidateToken(idToken, parameters, out outToken);
            }
            catch (SignatureVerificationFailedException)
            {
                return false;
            }
            return outToken != null;
        }
    }

    /// <summary>
    /// RPモック用Model。
    /// Authentication RequestのResponseとToken Requestの送信内容を決めるのに使用される。
    /// </summary>
    public class RpMockModel
    {
        /// <summary>
        /// 認可コード。正常終了時のみ返る
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// state
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// エラー。エラー時のみ返る
        /// </summary>
        public string error { get; set; }

        /// <summary>
        /// リクエストしたstate。セッションから取得してセットする。
        /// </summary>
        public string request_state { get; set; }

        /// <summary>
        /// grant_type
        /// </summary>
        public string grant_type { get; set; }

        /// <summary>
        /// redirect_uri
        /// </summary>
        public string redirect_uri { get; set; }

        /// <summary>
        /// client_id
        /// </summary>
        public string client_id { get; set; }

        /// <summary>
        /// client_secret
        /// </summary>
        public string client_secret { get; set; }
    }

    /// <summary>
    /// Token ResponseのResponseBosyのJSON
    /// </summary>
    public class TokenResponseJson
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string id_token { get; set; }
    }

    /// <summary>
    /// ID TokenのPayloadのJSON
    /// </summary>
    public class IdTokenPayloadJson
    {
        public string iss { get; set; }
        public string sub { get; set; }
        public string aud { get; set; }
        public int exp { get; set; }
        public int iat { get; set; }
    }
}