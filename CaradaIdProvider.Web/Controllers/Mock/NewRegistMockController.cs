﻿using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Controllers.Mock
{
    /// <summary>
    /// 利用開始前ユーザー登録用のモック。
    /// RouteConfigにて、デバッグ以外では無効になるように設定済み。
    /// </summary>
    public class NewRegistMockController : BaseWebController
    {
        /// <summary>
        /// IDジェネレーター
        /// </summary>
        private IdGenerator idGenerator = new IdGenerator();

        /// <summary>
        /// 利用開始前ユーザー登録モック画面 表示アクション
        /// </summary>
        /// <returns>画面ViewResult</returns>
        [HttpGet]
        public ActionResult Index(string status = null)
        {
            var model = new NewRegistMockViewModel();
            if (status == "success")
            {
                ViewBag.Message = "登録しました。";
            }
            else if (status == "error")
            {
                ViewBag.Message = "登録に失敗しました。";
            }
            else if (status == "duplicate")
            {
                ViewBag.Message = "IDが重複しています。";
            }
            else if (status == "nothing")
            {
                ViewBag.Message = "IDに紐付くユーザーが存在しません。";
            }
            return View(model);
        }

        /// <summary>
        /// 利用開始前ユーザー登録モック画面 登録アクション
        /// </summary>
        /// <returns>画面ViewResult</returns>
        [HttpPost]
        public async Task<ActionResult> Index([Bind(Include = "CaradaId, Password, ProcessId")] NewRegistMockViewModel model)
        {
            // ユーザー情報を取得
            var user = await UserManager.FindByNameAsync(model.CaradaId);

            // 新規登録の場合
            if (model.ProcessId == 1)
            {
                if (user != null)
                {
                    ModelState.AddModelError("UserAlreadyExists", "そのIDは既に登録されています。");
                    return RedirectToAction("Index", "NewRegistMock", new { status = "duplicate" });
                }
            }
            // 削除して新規登録の場合
            else
            {
                // ユーザーが存在しない場合は、操作ミスのためエラー
                if (user == null)
                {
                    ModelState.AddModelError("UserAlreadyExists", "そのIDに紐付くユーザーは存在しません");
                    return RedirectToAction("Index", "NewRegistMock", new { status = "nothing" });
                }

                // 現在ログイン中のユーザーを取得
                var userByLogin = await UserManager.FindByNameAsync(User.Identity.Name);

                // 対象のユーザーがログイン中のユーザーと一致する場合にログアウト
                if (userByLogin != null && user == userByLogin)
                {
                    await SignInManager.SignOut();
                }

                // ユーザーが存在する場合は、ユーザー情報を削除する
                await UserManager.DeleteAsync(user);
            }

            // ユーザー情報登録処理
            var newUser = new CaradaIdUser
            {
                UserName = model.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0
            };

            var result = await UserManager.CreateAsync(newUser, model.Password);

            if (!result.Succeeded)
            {
                // 表示アクションへ戻る
                return RedirectToAction("Index", "NewRegistMock", new { status = "error" });
            }

            // ユーザー情報を取得
            user = await UserManager.FindByNameAsync(model.CaradaId);

            // ユーザーロールを登録
            await UserManager.AddToRoleAsync(user.Id, Properties.Settings.Default.UserRole);

            // 統合認可プロバイダーの認可情報を登録
            var clientData = CaradaIdPDb.ClientMasters.First(o => o.ClientName == Properties.Settings.Default.AuthProviderClientName);
            CaradaIdPDb.AuthorizedUsers.Add(new AuthorizedUsers
            {
                UserId = user.Id,
                ClientId = clientData.ClientId,
                Sub = idGenerator.GenerateSubjectId()
            });

            CaradaIdPDb.SaveChanges();

            // 表示アクションへ戻る
            return RedirectToAction("Index", "NewRegistMock", new { status = "success" });
        }
    }
}