﻿using MTI.CaradaIdProvider.Web.Controllers.Web;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Controllers.Mock
{
    /// <summary>
    /// SendGrid Web Apiのモック
    /// 事実上CI専用
    /// </summary>
    public class SendGridMockController : BaseWebController
    {
        private readonly List<string> alwaysBounces = new List<string> { "bounce@a.com" };
        private readonly List<string> alwaysNotFound = new List<string> { "error@a.com" };
        private readonly List<string> alwaysTimeout = new List<string> { "timeout@a.com" };
        private readonly List<string> deleteNotFound = new List<string> { "deleteerror@a.com" };
        private readonly List<string> deleteTimeout = new List<string> { "deletetimeout@a.com" };

        private static int timeWait = 15000;

        /// <summary>
        /// Bounces Get APIのモック
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>ダミーのJson</returns>
        [ActionName("bouncesget")]
        [HttpGet]
        public ActionResult BouncesGet(BouncesGetModel model)
        {
            if (alwaysBounces.Contains(model.Email) || deleteNotFound.Contains(model.Email) || deleteTimeout.Contains(model.Email))
            {
                // bounceリストを返す
                return Json(new[] {
                    new SendGridEmailService.BounceModel {
                        Status = "1.1.1",
                        Created = DateTime.UtcNow.ToLongTimeString(),
                        Reason = "Dummy Reason",
                        Email = model.Email
                    }
                }, JsonRequestBehavior.AllowGet);
            }

            if (alwaysNotFound.Contains(model.Email))
            {
                // 404
                return HttpNotFound();
            }

            if (alwaysTimeout.Contains(model.Email))
            {
                var waitStart = DateTime.Now;

                // 15秒
                while (DateTime.Now - waitStart < TimeSpan.FromSeconds(timeWait)) ; 
            }

            // 空の配列
            return Json(new object[] { }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Bounces Delete APIのモック
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>ダミーのJson</returns>
        [ActionName("bouncesdelete")]
        [HttpPost]
        public ActionResult BouncesDelete(BoucesDeleteModel model)
        {
            if (alwaysNotFound.Contains(model.Email) || deleteNotFound.Contains(model.Email))
            {
                return HttpNotFound();
            }

            if (alwaysTimeout.Contains(model.Email) || deleteTimeout.Contains(model.Email))
            {
                var waitStart = DateTime.Now;
                while (DateTime.Now - waitStart < TimeSpan.FromSeconds(timeWait)) ; 
            }

            return Json(new SendGridEmailService.ResultModel() { Message = "success" });
        }

        [ModelBinder(typeof(BindingNameBinder))]
        public class BouncesGetModel
        {
            [BindingName(Name = "api_user")]
            public string ApiUser { get; set; }
            [BindingName(Name = "api_key")]
            public string ApiKey { get; set; }

            public string Date { get; set; }

            public string Days { get; set; }
            [BindingName(Name = "start_date")]
            public string StartDate { get; set; }
            [BindingName(Name = "end_date")]
            public string EndDate { get; set; }

            public string Limit { get; set; }

            public string Offset { get; set; }

            public string Type { get; set; }

            public string Email { get; set; }
        }

        [ModelBinder(typeof(BindingNameBinder))]
        public class BoucesDeleteModel
        {
            [BindingName(Name = "api_user")]
            public string ApiUser { get; set; }
            [BindingName(Name = "api_key")]
            public string ApiKey { get; set; }
            [BindingName(Name = "start_date")]
            public string StartDate { get; set; }
            [BindingName(Name = "end_date")]
            public string EndDate { get; set; }

            public string Type { get; set; }

            public string Email { get; set; }
            [BindingName(Name = "delete_all")]
            public string DeleteAll { get; set; }
        }
    }
}