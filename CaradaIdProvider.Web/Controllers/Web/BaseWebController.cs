﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.Filters;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Controllers.Web
{
    [LoggingAttribute]
    [PlatformKindAttribute]
    [ValidationCheckAttribute]
    /// <summary>
    /// Web系のベースコントローラクラス。
    /// </summary>
    public abstract class BaseWebController : Controller
    {
        /// <summary>
        /// ApplicationSignInManager
        /// </summary>
        private ApplicationSignInManager _signInManager;

        /// <summary>
        /// ApplicationUserManager
        /// </summary>
        private ApplicationUserManager _userManager;

        /// <summary>
        /// AuthenticationManager
        /// </summary>
        private IAuthenticationManager _authManager;

        /// <summary>
        /// CaradaIdPDbContext
        /// </summary>
        private CaradaIdPDbContext _caradaIdPDbContext;

        /// <summary>
        /// ログ
        /// </summary>
        protected static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public BaseWebController()
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="userManager">ApplicationUserManager</param>
        /// <param name="signInManager">ApplicationSignInManager</param>
        public BaseWebController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        /// <summary>
        /// SignInManager
        /// </summary>
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        /// <summary>
        /// UserManager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// AuthenticationManager
        /// </summary>
        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return _authManager ?? HttpContext.GetOwinContext().Authentication;
            }
            private set
            {
                _authManager = value;
            }
        }

        /// <summary>
        /// CaradaIdPDbContext
        /// </summary>
        public CaradaIdPDbContext CaradaIdPDb
        {
            get
            {
                return _caradaIdPDbContext ?? HttpContext.GetOwinContext().Get<CaradaIdPDbContext>();
            }
            private set
            {
                _caradaIdPDbContext = value;
            }
        }

        /// <summary>
        /// TempDataが取得出来なかった時、タイムアウトエラー画面へ遷移する
        /// </summary>
        /// <param name="tempDataKey">TempDataのキー</param>
        /// <returns></returns>
        public ActionResult RedirectToTimeoutErrorOnNoTempData(string tempDataKey)
        {
            LOG.Error(string.Format("There is no TempData[ {0} ]", tempDataKey));
            return RedirectToAction("Timeout", "Error");
        }
    }
}