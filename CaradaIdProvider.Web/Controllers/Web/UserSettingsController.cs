﻿using Microsoft.AspNet.Identity;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using MTI.CaradaIdProvider.Web.Resource;
using MTI.CaradaIdProvider.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Controllers.Web
{
    /// <summary>
    /// ユーザー設定コントローラー
    /// </summary>
    public class UserSettingsController : BaseWebController
    {
        /// <summary>
        /// メールアドレス再設定画面のTempData名
        /// </summary>
        private const string EmailResetTemp = "EmailReset";

        /// <summary>
        /// メールアドレス変更画面のTempData名
        /// </summary>
        private const string EmailChangeTemp = "EmailChange";

        /// <summary>
        /// 秘密の質問変更画面のTempData名
        /// </summary>
        private const string SecurityQuestionChangeTemp = "SecurityQuestionChange";

        /// <summary>
        /// ユーザーIDのTempData名
        /// </summary>
        private const string UserIdTemp = "UserId";

        /// <summary>
        /// ハッシュコンバーター
        /// </summary>
        private HashConverter hashConverter = new HashConverter();

        /// <summary>
        /// 二段階認証
        /// </summary>
        private TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();

        /// <summary>
        /// アカウント管理画面を初期表示する。
        /// </summary>
        /// <returns>アカウント管理画面のViewResult</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Index()
        {
            // ※戻るボタンで戻った時用の処理。
            TempData.Clear();

            // ユーザー情報取得
            var user = await GetRegisteredSignInUser(true);

            // 秘密の質問の回答を取得
            var selectAnswer = (from c in CaradaIdPDb.SecurityQuestionAnswers
                                where c.UserId == user.Id
                                select c).ToList();
            if (selectAnswer.Count != 1)
            {
                // ログイン状態が不正になる場合があるので、サインアウトしておく
                await SignInManager.SignOut();
                throw new HttpException("SecurityQuestionAnswers must unique per user.");
            }

            var securityQuestionId = selectAnswer[0].SecurityQuestionId;

            // 秘密の質問を取得
            var question = (from c in CaradaIdPDb.SecurityQuestionMasters
                            where c.SecurityQuestionId == securityQuestionId
                            select c).FirstOrDefault();
            if (question == null)
            {
                // ログイン状態が不正になる場合があるので、サインアウトしておく
                await SignInManager.SignOut();
                throw new HttpException("Such SecurityQuestion does not exitsts.");
            }

            var userSettingsModel = new UserSettingsViewModel()
            {
                CaradaId = user.UserName,
                Email = user.Email,
                SecurityQuestion = question.Question
            };

            return View(userSettingsModel);
        }

        /// <summary>
        /// メールアドレス再設定画面 表示アクション
        /// </summary>
        /// <returns>画面ViewResult</returns>
        [HttpGet]
        public ActionResult EmailResetView()
        {
            // 入力情報があれば取得
            var emailResetViewModel = TempData[EmailResetTemp] as EmailResetViewModel;
            if (emailResetViewModel == null)
            {
                emailResetViewModel = new EmailResetViewModel();
            }
            else
            {
                emailResetViewModel.SecurityQuestionSelectId = null;
                emailResetViewModel.Password = null;
                emailResetViewModel.Answer = null;
            }

            emailResetViewModel.SerializeItems = SelectListUtility.ConvertSelectListToDic(GetSecurityQuestion());

            TempData[EmailResetTemp] = emailResetViewModel;

            return View("EmailReset", emailResetViewModel);
        }

        /// <summary>
        /// 秘密の質問リストを取得する
        /// </summary>
        /// <returns>秘密の質問リスト</returns>
        private List<SelectListItem> GetSecurityQuestion()
        {
            var query = (from c in CaradaIdPDb.SecurityQuestionMasters
                         orderby c.DisplayOrder
                         select c).ToList();
            return query.Select(t => new SelectListItem
            {
                Text = t.Question,
                Value = t.SecurityQuestionId.ToString()
            })
            .ToList();
        }

        /// <summary>
        /// ユーザに紐づいた秘密の質問の回答を取得する
        /// </summary>
        /// <param name="userId">ユーザID</param>
        /// <returns>秘密の質問の回答</returns>
        private SecurityQuestionAnswers GetSecurityAnswer(string userId)
        {
            var selectAnswer = (from c in CaradaIdPDb.SecurityQuestionAnswers
                                where c.UserId == userId
                                select c).ToList();
            // Note: 基本的に0になることはユーザ削除時しかない。
            // また、現在の仕様では複数の紐づけは認めていない。
            if (selectAnswer.Count != 1)
            {
                throw new HttpException("SecurityQuestionAnswers must unique per user.");
            }
            return selectAnswer[0];
        }

        /// <summary>
        /// メールアドレス再設定画面 再設定アクション
        /// </summary>
        /// <param name="model">EmailResetViewModel</param>
        /// <returns>エラー：メールアドレス再設定画面のViewResult, 正常：メールアドレス再設定認証画面のViewResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EmailReset([Bind(Include = "CaradaId,Password,SecurityQuestionSelectId,Answer,Email")] EmailResetViewModel model)
        {
            var userByCaradaId = await UserManager.FindByNameAsync(model.CaradaId);

            // ユーザー情報が取得できないか利用開始登録されていない場合、画面を再表示する。
            if (userByCaradaId == null)
            {
                LOG.Warn("ID・パスワードが正しくありません");
                ModelState.AddModelError("CaradaId", ErrorMessages.LoginError);
                return View(model);
            }
            else if (!userByCaradaId.EmailConfirmed)
            {
                LOG.Warn("利用開始前のユーザーです");
                ModelState.AddModelError("CaradaId", ErrorMessages.NotProcessedUseStart);

                return View(model);
            }

            // パスワードの妥当性検証（ログイン成功時失敗回数更新はしない）
            if (!await ValidatePasswordNotCountReset(userByCaradaId, model.Password, "CaradaId"))
            {
                return View(model);
            }

            // 秘密の質問の回答を検索
            var selectAnswer = (from c in CaradaIdPDb.SecurityQuestionAnswers
                                where c.UserId == userByCaradaId.Id && c.SecurityQuestionId == model.SecurityQuestionSelectId
                                select c).FirstOrDefault();

            if (selectAnswer == null || selectAnswer.Answer != hashConverter.ConvertString(model.Answer))
            {
                // 秘密の質問回答誤り 失敗回数のカウントアップをする
                await UserManager.AccessFailedAsync(userByCaradaId.Id);

                LOG.Warn("秘密の質問と回答が正しくありません");

                if (await UserManager.IsLockedOutAsync(userByCaradaId.Id))
                {
                    // ロックされる失敗回数に到達した時点でロック中とする
                    LOG.Error("アカウントロックアウト開始");
                    ModelState.AddModelError("Answer", ErrorMessages.SettingsLockOut);
                    return View(model);
                }

                ModelState.AddModelError("Answer", ErrorMessages.VerifySecurityQuestionAnswer);
                return View(model);
            }

            // ログイン失敗回数をリセットする
            await UserManager.ResetAccessFailedCountAsync(userByCaradaId.Id);

            // メールアドレスの確認
            var userByEmail = await UserManager.FindByEmailAsync(model.Email);

            if (userByEmail != null)
            {

                // CARADA IDとメールアドレスのユーザー情報が一致する場合
                if (userByCaradaId == userByEmail)
                {
                    // メールアドレス変更不要エラー
                    LOG.Warn("現在のメールアドレスと同じメールアドレスが入力されました");
                    ModelState.AddModelError("Email", string.Format(ErrorMessages.MustNotSame, "メールアドレス", "現在のメールアドレス"));
                    return View(model);
                }

                if (userByEmail.EmailConfirmed)
                {
                    // 登録済みメールアドレスエラー
                    LOG.Warn("メールアドレスは別のユーザーが使用済みです");
                    ModelState.AddModelError("Email", ErrorMessages.RegisteredMail);
                    return View(model);
                }
            }

            // ユーザ情報の更新
            userByCaradaId.AuthCodeFailedCount = 0;
            userByCaradaId.AuthCodeExpireDateUtc = DateTime.UtcNow.AddMinutes(ApplicationUserManager.VERIFY_CODE_EXPIRE_MINUTES);
            await UserManager.UpdateAsync(userByCaradaId);

            // セキュリティタイムスタンプ更新
            await UserManager.UpdateSecurityStampAsync(userByCaradaId.Id);

            // 認証コード生成
            var code = await UserManager.GenerateTwoFactorTokenAsync(userByCaradaId.Id, Properties.Settings.Default.EmailResetSendCodeProvider);

            // メールオブジェクト作成
            var message = new IdentityMessage
            {
                Body = string.Format(Mail.EmailResetSendCodeBody.Replace(":SUPPORT:", Mail.Support), code),
                Subject = Mail.EmailResetSendCodeSubject,
                Destination = model.Email
            };

            // 認証コード案内メールを送信
            await UserManager.EmailService.SendAsync(message);

            // 入力フォームの値をTempDataに入れる。
            TempData[EmailResetTemp] = model;

            // メールアドレス再設定認証画面に遷移する。
            return View("EmailResetVerifyCode");
        }

        /// <summary>
        /// メールアドレス再設定認証画面 認証するアクション
        ///  認証コードが妥当な場合、新旧メールアドレスへ完了メール送信し、ログイン画面へ遷移する。
        /// </summary>
        /// <param name="model">EmailResetVerifyCodeViewModel</param>
        /// <returns>エラー：メールアドレス再設定認証画面のViewResult, 正常：ログイン画面へのRedirectToRouteResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EmailResetVerifyCode([Bind(Include = "VerifyCode")] EmailResetVerifyCodeViewModel model)
        {
            // TempDataから入力情報を取得
            var tempData = TempData[EmailResetTemp] as EmailResetViewModel;
            if (tempData == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(EmailResetTemp);
            }
            TempData.Keep(EmailResetTemp);

            var userByCaradaId = await UserManager.FindByNameAsync(tempData.CaradaId);
            // ユーザー情報が取得できないか認証済みでない場合エラー
            if (userByCaradaId == null || !userByCaradaId.EmailConfirmed)
            {
                throw new HttpException("This user is unregistered or already deleted.");
            }

            // 認証コード無効化チェック
            if (UserManager.IsInvalidVerifyCode(userByCaradaId))
            {
                LOG.Error("認証コード無効化済");
                ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                return View(model);
            }

            // 認証コードの検証
            if (!await UserManager.VerifyTwoFactorTokenAsync(userByCaradaId.Id,
                Properties.Settings.Default.EmailResetSendCodeProvider, model.VerifyCode))
            {
                if (await UserManager.IsVerifyCodeInvalidNow(userByCaradaId))
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                    LOG.Error("認証コード無効化");
                }
                else
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeError);
                    LOG.Warn("認証コード入力失敗");
                }

                return View(model);
            }

            // メールアドレスに紐づくユーザ情報の取得
            var userByEmail = await UserManager.FindByEmailAsync(tempData.Email);
            if (userByEmail != null)
            {
                if (userByEmail.EmailConfirmed)
                {
                    throw new HttpException("This email address is already confirmed.");
                }
                else
                {
                    // メールアドレスに紐付くユーザー情報のEmailをnullにして更新
                    userByEmail.Email = null;
                    await UserManager.UpdateAsync(userByEmail);
                }
            }
            // 新しいメールアドレスへ更新する
            var oldEmail = userByCaradaId.Email;
            userByCaradaId.Email = tempData.Email;
            userByCaradaId.UpdateDateUtc = DateTime.UtcNow;
            userByCaradaId.AuthCodeFailedCount = 0;
            userByCaradaId.AuthCodeExpireDateUtc = null;

            await UserManager.UpdateAsync(userByCaradaId);
            // セキュリティスタンプを更新する
            await UserManager.UpdateSecurityStampAsync(userByCaradaId.Id);
            // 二段階認証完了処理
            twoFactorAuthenticator.AddTwoFactorLoginCookie(Response, twoFactorAuthenticator.CompleteTwoFactorLogin(userByCaradaId.Id, userByCaradaId.UserName));
            // 新メールアドレスに完了メール送信
            await UserManager.SendEmailAsync(userByCaradaId.Id, Mail.EmailResetCompleteSubject,
                string.Format(Mail.EmailResetCompleteBody, userByCaradaId.UserName, userByCaradaId.Email, Mail.Support));
            // 旧メールアドレスに完了メールを送信
            var message = new IdentityMessage
            {
                Body = string.Format(Mail.EmailResetCompleteBody, userByCaradaId.UserName, userByCaradaId.Email, Mail.Support),
                Subject = Mail.EmailResetCompleteSubject,
                Destination = oldEmail
            };
            await UserManager.EmailService.SendAsync(message);

            LOG.Info(string.Format("メールアドレス再設定完了(UserId={0}, CARADA ID={1})", userByCaradaId.Id, userByCaradaId.UserName));

            TempData.Clear();

            // ログイン画面へ
            return RedirectToAction("LoginView", "User");
        }

        /// <summary>
        /// パスワード再設定案内画面 表示アクション
        /// </summary>
        /// <returns>パスワード再設定案内画面のViewResult</returns>
        [HttpGet]
        public ActionResult PasswordResetGuideView()
        {
            return View("PasswordResetGuide");
        }

        /// <summary>
        /// パスワード再設定案内画面 パスワードを再設定するアクション
        /// </summary>
        /// <param name="model">PasswordResetGuideViewModel</param>
        /// <returns>エラー：パスワード再設定案内画面のViewResult, 正常：パスワード再設定画面のViewResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PasswordResetGuide([Bind(Include = "Email")] PasswordResetGuideViewModel model)
        {
            // ユーザー情報取得
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null || !user.EmailConfirmed)
            {
                ModelState.AddModelError("Email", ErrorMessages.NotRegisteredUser);
                LOG.Warn("メールアドレスは未登録のものです");
                return View(model);
            }
            // 失敗回数と有効期限をリセット
            user.AuthCodeFailedCount = 0;
            user.AuthCodeExpireDateUtc = DateTime.UtcNow.AddMinutes(ApplicationUserManager.VERIFY_CODE_EXPIRE_MINUTES);
            await UserManager.UpdateAsync(user);

            // セキュリティタイムスタンプ更新
            await UserManager.UpdateSecurityStampAsync(user.Id);

            // 認証コード発行
            var code = await UserManager.GenerateTwoFactorTokenAsync(user.Id,
                Properties.Settings.Default.PasswordResetSendCodeProvider);

            // 認証コードメール送信
            await UserManager.NotifyTwoFactorTokenAsync(user.Id, Properties.Settings.Default.PasswordResetSendCodeProvider, code);

            // ユーザーIDの保持
            TempData[UserIdTemp] = user.Id;

            // パスワード再設定画面
            return View("PasswordReset");
        }

        /// <summary>
        /// パスワード再設定画面 再設定するアクション。
        /// パスワード再設定をするユーザーは何度かパスワードを誤っている前提であり、ログイン失敗回数とロックアウト期間を初期化する。
        /// </summary>
        /// <param name="model">PasswordResetViewModel</param>
        /// <returns>エラー：パスワード再設定画面のViewResult, 正常：ログイン画面へのRedirectToRouteResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PasswordReset([Bind(Include = "VerifyCode,ResetPassword,ConfirmResetPassword")] PasswordResetViewModel model)
        {
            // エラーで画面再表示の際にTempDataが消えないようにする。
            TempData.Keep(UserIdTemp);

            // TempDataからユーザーIDを取得し、ユーザー情報を検索する
            var userId = TempData[UserIdTemp] as string;
            var user = await UserManager.FindByIdAsync(userId);
            if (user == null || !user.EmailConfirmed)
            {
                // ここでユーザーがとれないことは起こりえないためシステムエラーとする
                throw new HttpException("User is not exists, that claims password reset.");
            }

            // 認証コードが無効かどうかはフレームワークでは制御しないので、業務ロジックでコード検証前に判定する必要がある
            if (UserManager.IsInvalidVerifyCode(user))
            {
                LOG.Error("認証コード無効化済");
                ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                return View(model);
            }

            // 認証コードの検証 パスワード失敗回数の加算はトランザクション管理しない
            if (!await UserManager.VerifyTwoFactorTokenAsync(userId,
                Properties.Settings.Default.PasswordResetSendCodeProvider, model.VerifyCode))
            {
                if (await UserManager.IsVerifyCodeInvalidNow(user))
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                    LOG.Error("認証コード無効化");
                }
                else
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeError);
                    LOG.Warn("認証コード入力失敗");
                }

                return View(model);
            }

            // 認証コード初期化
            user.AuthCodeFailedCount = 0;
            user.AuthCodeExpireDateUtc = null;
            // ログイン失敗回数とロックアウト期間の初期化
            user.AccessFailedCount = 0;
            user.LockoutEndDateUtc = null;
            // 更新日時
            user.UpdateDateUtc = DateTime.UtcNow;
            await UserManager.UpdateAsync(user);
            // パスワードリセットのAPIには専用のtokenの生成を必要とする
            var token = await UserManager.GeneratePasswordResetTokenAsync(userId);
            await UserManager.ResetPasswordAsync(userId, token, model.ResetPassword);
            // ユーザー情報を更新したのでセキュリティスタンプを更新する
            await UserManager.UpdateSecurityStampAsync(userId);

            // 完了メール送信
            await UserManager.SendEmailAsync(userId, Mail.PasswordResetCompleteSubject,
                string.Format(Mail.PasswordResetCompleteBody, user.UserName, Mail.Support));

            LOG.Info(string.Format("パスワード再設定完了(UserId={0}, CARADA ID={1})", userId, user.UserName));

            TempData.Remove(UserIdTemp);
            // ログイン画面へ
            return RedirectToAction("LoginView", "User");
        }

        /// <summary>
        /// メールアドレス変更画面 表示アクション
        /// </summary>
        /// <returns>画面ViewResult</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> EmailChangeView()
        {
            var user = await GetRegisteredSignInUser();

            var model = new EmailChangeViewModel();
            var tempEmail = TempData[EmailChangeTemp] as EmailChangeViewModel;
            if (tempEmail != null && !string.IsNullOrEmpty(tempEmail.NewEmail))
            {
                model.NewEmail = tempEmail.NewEmail;
            }
            model.Email = user.Email;
            TempData[EmailChangeTemp] = model;

            return View("EmailChange", model);
        }

        /// <summary>
        /// メールアドレス変更入力画面 変更するアクション
        /// </summary>
        /// <param name="model">EmailChangeViewModel</param>
        /// <returns>エラー：メールアドレス変更画面のViewResult, 正常：メールアドレス変更確認画面のViewResult</returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EmailChange([Bind(Include = "NewEmail,Password")] EmailChangeViewModel model)
        {
            var user = await GetRegisteredSignInUser();
            model.Email = user.Email;
            TempData[EmailChangeTemp] = model;

            // 現在のメールアドレスと同じかチェック
            if (model.NewEmail.Equals(user.Email, StringComparison.CurrentCultureIgnoreCase))
            {
                LOG.Warn("現在のメールアドレスと同じメールアドレスが入力されました");
                ModelState.AddModelError("NewEmail", string.Format(ErrorMessages.MustNotSame, "新しいメールアドレス", "現在のメールアドレス"));
                return View(model);
            }

            // パスワードの妥当性検証
            if (!await ValidatePassword(user, model.Password, "Password"))
            {
                return View(model);
            }

            // 既に使用されているメールアドレスかチェック
            var userByNewEmail = await UserManager.FindByEmailAsync(model.NewEmail);
            if (userByNewEmail != null && userByNewEmail.EmailConfirmed)
            {
                LOG.Warn("メールアドレスは別のユーザーが使用済みです");
                ModelState.AddModelError("NewEmail", ErrorMessages.RegisteredMail);
                return View(model);
            }

            ViewBag.NewEmail = model.NewEmail;
            return View("EmailChangeConfirm");
        }

        /// <summary>
        /// メールアドレス変更確認画面 登録するアクション。
        /// </summary>
        /// <returns>エラー：メールアドレス変更確認画面のViewResult, 正常：メールアドレス変更認証画面へのViewResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> EmailChangeConfirm()
        {
            // TempDataから新しいメールアドレスを取得
            var tempEmailChange = TempData[EmailChangeTemp] as EmailChangeViewModel;

            if (tempEmailChange == null)
            {
                // TempDataから新しいメールアドレスが取得できない場合エラー
                return RedirectToTimeoutErrorOnNoTempData(EmailChangeTemp);
            }

            // ログイン中のユーザー情報取得
            var user = await GetRegisteredSignInUser();

            // エラーに備えTempDataを保持する。
            TempData.Keep(EmailChangeTemp);

            // メールアドレスに紐づくユーザー情報の取得
            var userByEmail = await UserManager.FindByEmailAsync(tempEmailChange.NewEmail);
            if (userByEmail != null && userByEmail.EmailConfirmed)
            {
                throw new HttpException("This email address is already confirmed.");
            }

            // 失敗回数と有効期限をリセット
            user.AuthCodeFailedCount = 0;
            user.AuthCodeExpireDateUtc = DateTime.UtcNow.AddMinutes(ApplicationUserManager.VERIFY_CODE_EXPIRE_MINUTES);
            await UserManager.UpdateAsync(user);

            // セキュリティタイムスタンプ更新
            await UserManager.UpdateSecurityStampAsync(user.Id);

            // 認証コード発行
            var code = await UserManager.GenerateTwoFactorTokenAsync(user.Id,
                Properties.Settings.Default.EmailChangeSendCodeProvider);

            // メールオブジェクト作成
            var message = new IdentityMessage
            {
                Body = string.Format(Mail.EmailChangeSendCodeBody.Replace(":SUPPORT:", Mail.Support), code),
                Subject = Mail.EmailChangeSendCodeSubject,
                Destination = tempEmailChange.NewEmail
            };

            // 認証コード案内メールを送信
            await UserManager.EmailService.SendAsync(message);

            // メールアドレス変更認証画面
            return View("EmailChangeVerifyCode");
        }

        /// <summary>
        /// メールアドレス変更認証画面 認証するアクション
        ///  認証コードが妥当な場合、新旧メールアドレスへ完了メール送信し、アカウント管理画面へ遷移する。
        /// </summary>
        /// <param name="model">EmailChangeVerifyCodeViewModel</param>
        /// <returns>エラー：メールアドレス変更認証画面のViewResult, 正常：アカウント管理画面へのRedirectToRouteResult</returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EmailChangeVerifyCode([Bind(Include = "VerifyCode")] EmailChangeVerifyCodeViewModel model)
        {
            // TempDataから新しいメールアドレスを取得
            var tempMail = TempData[EmailChangeTemp] as EmailChangeViewModel;
            if (tempMail == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(EmailChangeTemp);
            }
            TempData.Keep(EmailChangeTemp);

            // ログイン中のユーザー情報取得
            var userByCaradaId = await GetRegisteredSignInUser();

            // メールアドレスに紐づくユーザ情報の取得
            var userByEmail = await UserManager.FindByEmailAsync(tempMail.NewEmail);
            if (userByEmail != null)
            {
                if (userByEmail.EmailConfirmed)
                {
                    throw new HttpException("This email address is already confirmed.");
                }
                else
                {
                    // メールアドレスに紐付くユーザー情報のEmailをnullにして更新
                    userByEmail.Email = null;
                    await UserManager.UpdateAsync(userByEmail);
                }
            }

            // 認証コード無効化チェック
            if (UserManager.IsInvalidVerifyCode(userByCaradaId))
            {
                LOG.Error("認証コード無効化済");
                ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                return View(model);
            }

            // 認証コードの検証
            if (!await UserManager.VerifyTwoFactorTokenAsync(userByCaradaId.Id,
                Properties.Settings.Default.EmailChangeSendCodeProvider, model.VerifyCode))
            {
                if (await UserManager.IsVerifyCodeInvalidNow(userByCaradaId))
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                    LOG.Error("認証コード無効化");
                }
                else
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeError);
                    LOG.Warn("認証コード入力失敗");
                }

                return View(model);
            }

            // 新しいメールアドレスへ更新する
            var oldEmail = userByCaradaId.Email;
            userByCaradaId.Email = tempMail.NewEmail;
            userByCaradaId.UpdateDateUtc = DateTime.UtcNow;
            userByCaradaId.AuthCodeFailedCount = 0;
            userByCaradaId.AuthCodeExpireDateUtc = null;

            await UserManager.UpdateAsync(userByCaradaId);

            // セキュリティスタンプを更新する
            await UserManager.UpdateSecurityStampAsync(userByCaradaId.Id);

            // 新メールアドレスに完了メール送信
            await UserManager.SendEmailAsync(userByCaradaId.Id, Mail.EmailChangeCompleteSubject,
                string.Format(Mail.EmailChangeCompleteBody, userByCaradaId.UserName, userByCaradaId.Email, Mail.Support));

            // 旧メールアドレスに完了メールを送信
            var message = new IdentityMessage
            {
                Body = string.Format(Mail.EmailChangeCompleteBody, userByCaradaId.UserName, userByCaradaId.Email, Mail.Support),
                Subject = Mail.EmailChangeCompleteSubject,
                Destination = oldEmail
            };
            await UserManager.EmailService.SendAsync(message);

            LOG.Info("メールアドレス変更完了");

            // TempDataをクリアする
            TempData.Clear();

            // アカウント管理画面へ遷移
            return RedirectToAction("Index");
        }

        /// <summary>
        /// パスワード変更画面 表示アクション
        /// </summary>
        /// <returns>画面ViewResult</returns>
        [HttpGet]
        [Authorize]
        public ActionResult PasswordChangeView()
        {
            return View("PasswordChange");
        }

        /// <summary>
        /// パスワード変更画面 変更するアクション。
        /// </summary>
        /// <param name="model">PasswordChangeViewModel</param>
        /// <returns>エラー：パスワード変更画面のViewResult, 正常：アカウント管理画面へのRedirectToRouteResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> PasswordChange([Bind(Include = "CurrentPassword,NewPassword,NewPasswordConfirm")] PasswordChangeViewModel model)
        {
            // ログイン中のユーザー情報取得
            var user = await GetRegisteredSignInUser();

            // パスワードの妥当性検証
            if (!await ValidatePassword(user, model.CurrentPassword, "CurrentPassword"))
            {
                return View(model);
            }

            // パスワードリセットのAPIには専用のtokenの生成を必要とする
            var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            await UserManager.ResetPasswordAsync(user.Id, token, model.NewPassword);
            // ユーザー情報更新
            user.UpdateDateUtc = DateTime.UtcNow;
            await UserManager.UpdateAsync(user);
            // ユーザー情報を更新したのでセキュリティスタンプを更新する
            await UserManager.UpdateSecurityStampAsync(user.Id);

            // 完了メール送信
            await UserManager.SendEmailAsync(user.Id, Mail.PasswordChangeCompleteSubject,
                string.Format(Mail.PasswordChangeCompleteBody, user.UserName, Mail.Support));

            LOG.Info("パスワード変更完了");

            // アカウント管理画面へ遷移
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 秘密の質問変更画面 表示アクション
        /// </summary>
        /// <returns>画面ViewResult</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> SecurityQuestionChangeView()
        {
            // ユーザー情報取得
            var user = await GetRegisteredSignInUser();

            // 秘密の質問リスト取得
            TempData[SecurityQuestionChangeTemp] = SelectListUtility.ConvertSelectListToDic(GetSecurityQuestion());

            // 秘密の質問の回答取得
            var selectAnswer = GetSecurityAnswer(user.Id);

            var model = new SecurityQuestionChangeViewModel()
            {
                SecurityQuestionSelectId = selectAnswer.SecurityQuestionId
            };

            return View("SecurityQuestionChange", model);
        }

        /// <summary>
        /// 秘密の質問変更画面 変更するアクション。
        /// </summary>
        /// <param name="model">SecurityQuestionChangeViewModel</param>
        /// <returns>エラー：秘密の質問変更画面のViewResult, 正常：アカウント管理画面へのRedirectToRouteResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> SecurityQuestionChange([Bind(Include = "SecurityQuestionSelectId,Answer,Password")] SecurityQuestionChangeViewModel model)
        {
            // エラーに備え秘密の質問リストを保持する。
            // TempData.Keep(SecurityQuestionChangeTemp);

            // 秘密の質問を検索する
            var query = (from c in CaradaIdPDb.SecurityQuestionMasters
                         where c.SecurityQuestionId == model.SecurityQuestionSelectId
                         select c).SingleOrDefault();
            if (query == null)
            {
                // 該当する秘密の質問がない
                LOG.Error(string.Format("秘密の質問が不正です"));
                ModelState.AddModelError("SecurityQuestionSelectId", ErrorMessages.SecurityQuestionError);
                return View(model);
            }

            // ログイン中のユーザー情報取得
            var user = await GetRegisteredSignInUser();

            // パスワードの妥当性検証
            if (!await ValidatePassword(user, model.Password, "Password"))
            {
                return View(model);
            }

            // 秘密の質問の回答取得
            var selectAnswer = GetSecurityAnswer(user.Id);

            // 秘密の質問の回答情報を更新する
            CaradaIdPDb.SecurityQuestionAnswers.Remove(selectAnswer);
            CaradaIdPDb.SecurityQuestionAnswers.Add(
                new SecurityQuestionAnswers()
                {
                    UserId = selectAnswer.UserId,
                    SecurityQuestionId = (int)model.SecurityQuestionSelectId,
                    Answer = hashConverter.ConvertString(model.Answer)
                });
            CaradaIdPDb.SaveChanges();

            // ユーザー情報更新
            user.UpdateDateUtc = DateTime.UtcNow;
            await UserManager.UpdateAsync(user);
            // ユーザー情報を更新したのでセキュリティスタンプを更新する
            await UserManager.UpdateSecurityStampAsync(user.Id);

            LOG.Info("秘密の質問変更完了");

            // アカウント管理画面へ遷移
            return RedirectToAction("Index");
        }

        /// <summary>
        /// ログイン中のユーザ情報を取得する
        /// </summary>
        /// <param name="forceSignOutBeforeError">False(default): エラー時に強制サインアウトしない、True: する。</param>
        /// <returns>CaradaIdUserのエンティティ</returns>
        private async Task<CaradaIdUser> GetRegisteredSignInUser(bool forceSignOutBeforeError = false)
        {
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            // ユーザー情報が取得できないか利用開始登録されていない場合エラー
            if (user == null || !user.EmailConfirmed)
            {
                if (forceSignOutBeforeError)
                {
                    // ログイン状態が不正になる場合があるので、サインアウトしておく
                    await SignInManager.SignOut();
                }
                throw new HttpException("This user is unregistered or already deleted.");
            }
            return user;
        }

        /// <summary>
        /// パスワードを検証する（ログイン成功時失敗回数更新あり）
        /// </summary>
        /// <param name="user">検証対象のCaradaIdUserエンティティ</param>
        /// <param name="password">パスワード（平文）</param>
        /// <param name="addModelErrorPropertyName">エラー時に付与するViewModelの対象プロパティ名</param>
        /// <returns></returns>
        private async Task<bool> ValidatePassword(CaradaIdUser user, string password, string addModelErrorPropertyName)
        {
            var passwordValidationResult = await UserManager.ValidatePassword(user, password);
            switch (passwordValidationResult)
            {
                case PasswordValidationStatus.Success:
                    // ログイン失敗回数をリセットする
                    await UserManager.ResetAccessFailedCountAsync(user.Id);
                    return true;
                case PasswordValidationStatus.LockedOut:
                    LOG.Error("アカウントロックアウト開始");
                    ModelState.AddModelError(addModelErrorPropertyName, ErrorMessages.SettingsLockOut);
                    break;
                case PasswordValidationStatus.AlreadyLockedOut:
                    LOG.Error("アカウントロックアウト中");
                    ModelState.AddModelError(addModelErrorPropertyName, ErrorMessages.SettingsLockOut);
                    break;
                case PasswordValidationStatus.Failure:
                default:
                    LOG.Warn("パスワードがログイン中のものと一致しません");
                    ModelState.AddModelError(addModelErrorPropertyName, ErrorMessages.PasswordMismatch);
                    break;
            }
            return false;
        }

        /// <summary>
        /// パスワードを検証する（ログイン成功時失敗回数更新なし）
        /// </summary>
        /// <param name="user">検証対象のCaradaIdUserエンティティ</param>
        /// <param name="password">パスワード（平文）</param>
        /// <param name="addModelErrorPropertyName">エラー時に付与するViewModelの対象プロパティ名</param>
        /// <returns></returns>
        private async Task<bool> ValidatePasswordNotCountReset(CaradaIdUser user, string password, string addModelErrorPropertyName)
        {
            var passwordValidationResult = await UserManager.ValidatePassword(user, password);
            switch (passwordValidationResult)
            {
                case PasswordValidationStatus.Success:
                    return true;
                case PasswordValidationStatus.LockedOut:
                    LOG.Error("アカウントロックアウト開始");
                    ModelState.AddModelError(addModelErrorPropertyName, ErrorMessages.SettingsLockOut);
                    break;
                case PasswordValidationStatus.AlreadyLockedOut:
                    LOG.Error("アカウントロックアウト中");
                    ModelState.AddModelError(addModelErrorPropertyName, ErrorMessages.SettingsLockOut);
                    break;
                case PasswordValidationStatus.Failure:
                default:
                    LOG.Warn("ID・パスワードが正しくありません");
                    ModelState.AddModelError(addModelErrorPropertyName, ErrorMessages.LoginError);
                    break;
            }
            return false;
        }
    }
}