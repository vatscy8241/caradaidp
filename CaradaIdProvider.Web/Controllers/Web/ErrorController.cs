﻿using Microsoft.Azure;
using MTI.CaradaIdProvider.Web.Filters;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Resource;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Controllers.Web
{
    [LoggingAttribute]
    [PlatformKindAttribute]
    [ValidationCheckAttribute]
    /// <summary>
    /// エラー画面コントローラクラス。
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// 404エラー
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult NotFound()
        {
            BeforeAction();
            ViewBag.ERROR_MESSAGE = GetErrorMessage("NotFound");
            return View("NotFound");
        }

        /// <summary>
        /// サーバエラー(404エラー以外の場合。catchされない例外も含む)
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult ServerError()
        {
            BeforeAction();
            ViewBag.ERROR_MESSAGE = GetErrorMessage("ServerError");
            return View("Index");
        }

        /// <summary>
        /// セッションタイムアウト時
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Timeout()
        {
            BeforeAction();
            ViewBag.ERROR_MESSAGE = GetErrorMessage("Timeout");
            return View("Timeout");
        }

        /// <summary>
        /// メンテナンス中
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Maintenance()
        {
            BeforeAction();
            // Authorization Endpointから遷移してきた場合の処理
            var authorizationEndpointSession = Session[Properties.Settings.Default.AuthorizeEndpointSession] as AuthorizeEndpointSession;
            if (authorizationEndpointSession != null)
            {
                // ログイン結果をメンテナンス中に変更
                authorizationEndpointSession.LoginResult = LoginResult.Maintenance;
                return new RedirectResult(authorizationEndpointSession.GetParameterizedReturnUrl());
            }

            // MaintenancePeriodMessageはnull 或いは MaintenanceModeは0の場合、サービス停止期間非表示
            var maintenancePeriodMessage = CloudConfigurationManager.GetSetting("MaintenancePeriodMessage");
            if (string.IsNullOrEmpty(maintenancePeriodMessage) || CloudConfigurationManager.GetSetting("MaintenanceMode") == "0")
            {
                ViewBag.ERROR_MESSAGE = ErrorMessages.MaintenanceMode;
            }
            else
            {
                ViewBag.ERROR_MESSAGE = ErrorMessages.MaintenanceModePeriod + maintenancePeriodMessage;
            }
            return View("Maintenance");
        }

        /// <summary>
        /// 戻るボタン押下時
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Return()
        {
            // Authorization Endpointから遷移してきた場合の処理
            var authorizationEndpointSession = Session[Properties.Settings.Default.AuthorizeEndpointSession] as AuthorizeEndpointSession;
            if (authorizationEndpointSession != null)
            {
                // ログイン結果をエラーに変更
                authorizationEndpointSession.LoginResult = LoginResult.Error;
                return new RedirectResult(authorizationEndpointSession.GetParameterizedReturnUrl());
            }
            // それ以外の場合、アカウント情報設定画面を表示
            return RedirectToAction("Index", "UserSettings");
        }

        /// <summary>
        /// エラーメッセージを取得する
        /// </summary>
        /// <param name="errorCode">エラーコード</param>
        /// <param name="args">エラー文言の引数</param>
        /// <returns>エラーメッセージ</returns>
        private string GetErrorMessage(string errorCode, params string[] args)
        {
            var errorFormat = ErrorMessages.ResourceManager.GetString(errorCode);
            if (args == null || args.Length == 0)
            {
                return errorFormat;
            }
            return string.Format(errorFormat, args);
        }

        /// <summary>
        /// アクション初期処理
        /// </summary>
        private void BeforeAction()
        {
            TempData.Clear();
        }
    }
}