﻿using Microsoft.AspNet.Identity;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Models.ViewModels;
using MTI.CaradaIdProvider.Web.Resource;
using MTI.CaradaIdProvider.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Controllers.Web
{
    /// <summary>
    /// ユーザーコントローラー。
    /// </summary>
    public class UserController : BaseWebController
    {
        /// <summary>
        /// ログイン画面のTempData名
        /// </summary>
        private const string LoginTemp = "Login";

        /// <summary>
        /// 利用開始画面のTempData名
        /// </summary>
        private const string UseStartTemp = "UseStart";

        /// <summary>
        /// 二段階認証のTempData名
        /// </summary>
        private const string TwoFactorLoginTemp = "TwoFactorLogin";

        /// <summary>
        /// 秘密の質問登録のTempData名
        /// </summary>
        private const string SecurityQuestionTemp = "SecurityQuestion";

        /// <summary>
        /// IDジェネレーター
        /// </summary>
        private IdGenerator idGenerator = new IdGenerator();

        /// <summary>
        /// ハッシュコンバーター
        /// </summary>
        private HashConverter hashConverter = new HashConverter();

        /// <summary>
        /// 二段階認証
        /// </summary>
        private TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();

        /// <summary>
        /// ログイン画面初期表示アクション
        /// </summary>
        /// <remarks>
        /// ログイン済みかログイン状態をキープするときは設定画面に遷移する。
        /// 同様の判定をEndpointでも行っているので、未ログインの場合のみEndpointからリダイレクトされる。
        /// 初期表示するパラメータは、セキュリティを考慮してCARADA IDのみ許容する。
        /// </remarks>
        /// <param name="caradaId">初期表示するCARADA ID</param>
        /// <returns>コメント参照</returns>
        [HttpGet]
        public ActionResult LoginView()
        {
            // ログイン済み確認
            if (User.Identity.IsAuthenticated)
            {
                LOG.Info("ログイン済");
                return RedirectToAction("Index", "UserSettings");
            }

            return View("Login");
        }

        /// <summary>
        /// ログインアクション
        /// </summary>
        /// <remarks>
        /// メールアドレス確認フラグがfalseの場合、利用開始画面に遷移する。
        /// 二段階認証が必要な場合は、二段階認証画面に遷移する。
        /// 不要な場合は、下記分岐となる。<br>
        /// 直接URLを起動しログイン成功した場合は設定画面に遷移する。
        /// Endpointからリダイレクトしログイン成功した場合はEndpointにOKを返却する。
        /// </remarks>
        /// <param name="model">model</param>
        /// <returns>コメント参照</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login([Bind(Include = "CaradaId,Password,IsKeepLogin")] LoginViewModel model)
        {
            // サインインマネージャを使用したログインチャレンジ
            var result = await SignInManager.SignInAsync(model.CaradaId, model.Password, model.IsKeepLogin, true);

            switch (result)
            {
                case SignInStatusEx.Success:
                    return GoToReturnUrl();

                case SignInStatusEx.UseStarting:
                    TempData[LoginTemp] = model;
                    return RedirectToAction("UseStartView", "User");

                case SignInStatusEx.LockedOut:
                    ModelState.AddModelError("CaradaId", ErrorMessages.ResourceManager.GetString("LockOut"));
                    return View(model);

                case SignInStatusEx.RequiresVerification:
                    await SignInManager.SignOut();
                    // ユーザー情報が存在することは保証済み
                    var user = await UserManager.FindByNameAsync(model.CaradaId);
                    // 認証コード関連情報の初期化
                    user.AuthCodeFailedCount = 0;
                    user.AuthCodeExpireDateUtc = DateTime.UtcNow.AddMinutes(ApplicationUserManager.VERIFY_CODE_EXPIRE_MINUTES);
                    await UserManager.UpdateAsync(user);
                    // セキュリティタイムスタンプを更新しての認証コード送信
                    await UserManager.UpdateSecurityStampAsync(user.Id);
                    var code = await UserManager.GenerateTwoFactorTokenAsync(user.Id, Properties.Settings.Default.TwoFactorLoginSendCodeProvider);
                    await UserManager.NotifyTwoFactorTokenAsync(user.Id, Properties.Settings.Default.TwoFactorLoginSendCodeProvider, code);
                    TempData[TwoFactorLoginTemp] = model;
                    return View("TwoFactorLogin");

                case SignInStatusEx.Failure:
                default:
                    ModelState.AddModelError("CaradaId", ErrorMessages.ResourceManager.GetString("LoginError"));
                    return View(model);
            }
        }

        /// <summary>
        /// 戻り先URLに遷移する
        /// </summary>
        /// <returns>AuthenticationRequestの場合：EndpointへのRedirect, 以外：アカウント情報設定画面へのRedirect</returns>
        private ActionResult GoToReturnUrl()
        {
            // 明示的にTempDataを全てクリアする
            TempData.Clear();

            // Authorization Endpointから遷移してきた場合の処理
            var authorizationEndpointSession = Session[Properties.Settings.Default.AuthorizeEndpointSession] as AuthorizeEndpointSession;
            if (authorizationEndpointSession != null)
            {
                // ログイン結果を成功に変更
                authorizationEndpointSession.LoginResult = LoginResult.Success;
                return new RedirectResult(authorizationEndpointSession.GetParameterizedReturnUrl());
            }
            // それ以外の場合、アカウント情報設定画面を表示
            return RedirectToAction("Index", "UserSettings");
        }

        /// <summary>
        /// ID連絡画面 表示アクション
        /// </summary>
        /// <returns>画面ViewResult</returns>
        [HttpGet]
        public ActionResult IdNoticeView()
        {
            return View("IdNotice");
        }

        /// <summary>
        /// ID連絡画面 CARADA IDを通知するアクション
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>エラー：画面ViewResult, 正常：ログイン画面へのRedirect</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> IdNotice([Bind(Include = "Email")] IdNoticeViewModel model)
        {
            var user = UserManager.FindByEmail(model.Email);

            if (user == null || !user.EmailConfirmed)
            {
                ModelState.AddModelError("Email", ErrorMessages.NotRegisteredUser);
                LOG.Warn(string.Format("メールアドレスは未登録のものです"));
                return View(model);
            }

            var message = new IdentityMessage
            {
                // 入力されたメールアドレスへCARADA IDを本文として送信
                Destination = model.Email,
                Subject = Mail.IdNoticeSubject,
                Body = string.Format(Mail.IdNoticeBody, user.UserName, Mail.Support)
            };

            await UserManager.EmailService.SendAsync(message);
            LOG.Info(string.Format("ID連絡完了(UserId={0}, CARADA ID={1})", user.Id, user.UserName));

            return RedirectToAction("LoginView");
        }

        /// <summary>
        /// 利用開始画面表示アクション
        /// </summary>
        /// <remarks>
        /// 初回利用時のメールアドレス登録を行う。
        /// </remarks>
        /// <returns>利用開始画面</returns>
        [HttpGet]
        public ActionResult UseStartView()
        {
            var model = new UseStartViewModel();
            var loginViewModel = TempData[LoginTemp] as LoginViewModel;
            if (loginViewModel == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(LoginTemp);
            }
            TempData.Keep(LoginTemp);

            var tempModel = TempData[UseStartTemp] as UseStartViewModel;
            if (tempModel != null && tempModel.Email != null)
            {
                model.Email = tempModel.Email;
            }
            return View("UseStart", model);
        }

        /// <summary>
        /// 利用規約に同意して開始アクション
        /// </summary>
        /// <param name="model">利用開始画面ビューモデル</param>
        /// <returns>利用開始認証画面</returns>
        [HttpPost]
        public async Task<ActionResult> UseStart(UseStartViewModel model)
        {
            var loginViewModel = TempData[LoginTemp] as LoginViewModel;
            if (loginViewModel == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(LoginTemp);
            }
            TempData.Keep(LoginTemp);

            var userByCaradaId = await UserManager.FindByNameAsync(loginViewModel.CaradaId);

            // 以下のいずれかの場合はシステムエラー。
            // ・ユーザー情報が取得できない
            // ・EmailConfirmedがtrue
            // ・企業ユーザフラグが1以外
            if (userByCaradaId == null || userByCaradaId.EmailConfirmed || !userByCaradaId.EmployeeFlag)
            {
                throw new HttpException("This user is unregistered or already deleted or non-enterprise user.");
            }

            // メールアドレスに紐づくユーザ情報の取得
            var userByEmail = await UserManager.FindByEmailAsync(model.Email);
            if (userByEmail != null)
            {
                if (userByEmail.EmailConfirmed)
                {
                    LOG.Warn("メールアドレスは別のユーザーが使用済みです");
                    ModelState.AddModelError("Email", ErrorMessages.RegisteredMail);
                    return View(model);
                }

                if (userByCaradaId.Id != userByEmail.Id)
                {
                    // メールアドレスに紐付くユーザー情報のEmailをnullにして更新
                    userByEmail.Email = null;
                    await UserManager.UpdateAsync(userByEmail);
                }
            }

            // ユーザ情報の更新
            userByCaradaId.Email = model.Email;
            userByCaradaId.AuthCodeFailedCount = 0;
            userByCaradaId.AuthCodeExpireDateUtc = DateTime.UtcNow.AddMinutes(ApplicationUserManager.VERIFY_CODE_EXPIRE_MINUTES);
            await UserManager.UpdateAsync(userByCaradaId);

            // TempData にメールアドレスを追加(保障されていないメールアドレスでの利用開始を防止）
            TempData[UseStartTemp] = model;

            // セキュリティタイムスタンプ更新
            await UserManager.UpdateSecurityStampAsync(userByCaradaId.Id);

            // 認証コード生成
            var code = await UserManager.GenerateTwoFactorTokenAsync(
                userByCaradaId.Id, Properties.Settings.Default.UseStartSendCodeProvider);

            // 認証コード案内メール送信
            await UserManager.NotifyTwoFactorTokenAsync(userByCaradaId.Id, Properties.Settings.Default.UseStartSendCodeProvider, code);

            // 利用開始認証画面を表示
            return View("UseStartVerifyCode");
        }

        /// <summary>
        /// 利用開始認証画面 認証するアクション
        /// </summary>
        /// <param name="model">UseStartVerifyCodeViewModel</param>
        /// <returns>エラー：画面ViewResult, 正常：秘密の質問登録画面ViewResult</returns>
        [HttpPost]
        public async Task<ActionResult> UseStartVerifyCode([Bind(Include = "VerifyCode")] UseStartVerifyCodeViewModel model)
        {
            // ログイン情報のTempDataが取得できない場合はエラー
            var loginViewModel = TempData[LoginTemp] as LoginViewModel;
            if (loginViewModel == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(LoginTemp);
            }
            TempData.Keep(LoginTemp);

            // メールアドレス入力のTempDataが取得できない場合はエラー
            var userStartViewModel = TempData[UseStartTemp] as UseStartViewModel;
            if (userStartViewModel == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(UseStartTemp);
            }
            TempData.Keep(UseStartTemp);

            // ユーザー情報を取得
            var userByCaradaId = await UserManager.FindByNameAsync(loginViewModel.CaradaId);


            // 以下の場合はエラー
            // ・ユーザー情報が取得できない
            // ・EmailConfirmedがtrue
            // ・Emailがnull
            // ・EmailとTempDataの入力メールアドレスが違う
            // ・企業ユーザーフラグが1以外
            if (userByCaradaId == null || userByCaradaId.EmailConfirmed || userByCaradaId.Email == null || userByCaradaId.Email != userStartViewModel.Email || userByCaradaId.EmployeeFlag == false)
            {
                throw new HttpException("This user is unregistered or already deleted or non-enterprise user or different email compared to the start of registration.");
            }

            // 既に認証コードが無効になっているかを検証
            if (UserManager.IsInvalidVerifyCode(userByCaradaId))
            {
                LOG.Error("認証コード無効化済");
                ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                return View(model);
            }

            // 認証コードの妥当性検証
            if (!await UserManager.VerifyTwoFactorTokenAsync(userByCaradaId.Id, Properties.Settings.Default.UseStartSendCodeProvider, model.VerifyCode))
            {
                if (await UserManager.IsVerifyCodeInvalidNow(userByCaradaId))
                {
                    // 認証コード無効
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                    LOG.Error("認証コード無効化");
                }
                else
                {
                    // 認証失敗（認証コードはまだ有効）
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeError);
                    LOG.Warn("認証コード入力失敗");
                }

                return View(model);
            }

            // 認証コード初期化
            userByCaradaId.AuthCodeFailedCount = 0;
            userByCaradaId.AuthCodeExpireDateUtc = null;
            await UserManager.UpdateAsync(userByCaradaId);

            // 秘密の質問リストの取得
            TempData[SecurityQuestionTemp] = SelectListUtility.ConvertSelectListToDic(GetSecurityQuestion());

            // 秘密の質問登録画面を表示する
            return View("SecurityQuestionRegist");
        }

        /// <summary>
        /// 秘密の質問リストを取得する
        /// </summary>
        /// <returns>秘密の質問リスト</returns>
        private List<SelectListItem> GetSecurityQuestion()
        {
            var query = (from c in CaradaIdPDb.SecurityQuestionMasters
                         orderby c.DisplayOrder
                         select c).ToList();
            return query.Select(t => new SelectListItem
            {
                Text = t.Question,
                Value = t.SecurityQuestionId.ToString()
            })
            .ToList();
        }

        /// <summary>
        /// 秘密の質問登録画面 登録するアクション
        /// </summary>
        /// <returns>RPのredirect_uriもしくはアカウント管理画面</returns>
        [HttpPost]
        public async Task<ActionResult> SecurityQuestionRegist([Bind(Include = "SecurityQuestion,Answer,SecurityQuestionSelectId")] SecurityQuestionRegistViewModel model)
        {
            // エラーに備え秘密の質問リストを保持する。
            TempData.Keep(SecurityQuestionTemp);

            // 秘密の質問を検索する
            var query = (from c in CaradaIdPDb.SecurityQuestionMasters
                         where c.SecurityQuestionId == model.SecurityQuestionSelectId
                         select c).SingleOrDefault();
            if (query == null)
            {
                // 該当する秘密の質問がない
                LOG.Error(string.Format("秘密の質問が不正です"));
                ModelState.AddModelError("SecurityQuestionSelectId", ErrorMessages.SecurityQuestionError);
                return View(model);
            }

            // TempDataが取得できない場合はエラー
            var loginViewModel = TempData[LoginTemp] as LoginViewModel;
            if (loginViewModel == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(LoginTemp);
            }
            TempData.Keep(LoginTemp);
            var userStartViewModel = TempData[UseStartTemp] as UseStartViewModel;
            if (userStartViewModel == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(UseStartTemp);
            }
            TempData.Keep(UseStartTemp);

            // ユーザー情報の取得
            var userByCaradaId = await UserManager.FindByNameAsync(loginViewModel.CaradaId);

            // 以下の場合はエラー
            // ・ユーザー情報が取得できない
            // ・EmailConfirmedがtrue
            // ・Emailがnull
            // ・EmailとTempDataの入力メールアドレスが違う
            // ・企業ユーザーフラグが1以外
            if (userByCaradaId == null || userByCaradaId.EmailConfirmed || userByCaradaId.Email == null
                || userByCaradaId.EmployeeFlag == false || userByCaradaId.Email != userStartViewModel.Email)
            {
                throw new HttpException("This user is unregistered or already deleted or non-enterprise user or different email compared to the start of registration.");
            }

            // 秘密の質問の回答を検索
            var selectAnswer = (from c in CaradaIdPDb.SecurityQuestionAnswers
                                where c.UserId == userByCaradaId.Id
                                select c).FirstOrDefault();

            // 秘密の質問の回答が存在した場合、削除する
            if (selectAnswer != null)
            {
                CaradaIdPDb.SecurityQuestionAnswers.Remove(selectAnswer);
                CaradaIdPDb.SaveChanges();
            }

            // ユーザーごとの秘密の質問の回答情報を登録する
            CaradaIdPDb.SecurityQuestionAnswers.Add(
                new SecurityQuestionAnswers()
                {
                    UserId = userByCaradaId.Id,
                    SecurityQuestionId = (int)model.SecurityQuestionSelectId,
                    Answer = hashConverter.ConvertString(model.Answer)
                });
            CaradaIdPDb.SaveChanges();

            // ユーザ情報の更新
            // ・EmailConfirmed を true
            // ・UseStartDateUtc を UTCの現在日時
            userByCaradaId.EmailConfirmed = true;
            userByCaradaId.UseStartDateUtc = DateTime.UtcNow;
            await UserManager.UpdateAsync(userByCaradaId);

            LOG.Info("利用開始成功");

            // 二段階認証完了処理
            twoFactorAuthenticator.AddTwoFactorLoginCookie(Response, twoFactorAuthenticator.CompleteTwoFactorLogin(userByCaradaId.Id, userByCaradaId.UserName));

            // ログイン処理
            if ((await SignInManager.SignInAsync(loginViewModel.CaradaId, loginViewModel.Password, loginViewModel.IsKeepLogin, false)) != SignInStatusEx.Success)
            {
                throw new HttpException("Login Error at SecurityQuestionRegist.");
            }

            // 利用開始メール送信 失敗してもエラーログ出力で処理を進める
            var message = new IdentityMessage
            {
                // 入力されたメールアドレスへCARADA IDを本文として送信
                Destination = userByCaradaId.Email,
                Subject = Mail.RegisteredSubject,
                Body = string.Format(Mail.RegisteredBody, userByCaradaId.UserName, userByCaradaId.Email, Mail.Support)
            };
            await UserManager.EmailService.SendAsync(message);

            return GoToReturnUrl();
        }

        /// <summary>
        /// 二段階認証画面 認証するアクション
        /// </summary>
        /// <returns>エラー：画面ViewResult, 正常：ログインを参照</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> TwoFactorLogin([Bind(Include = "VerifyCode")] TwoFactorLoginViewModel model)
        {

            // TempDataが取得できない場合はエラー
            var loginViewModel = TempData[TwoFactorLoginTemp] as LoginViewModel;
            if (loginViewModel == null)
            {
                return RedirectToTimeoutErrorOnNoTempData(TwoFactorLoginTemp);
            }
            TempData.Keep(TwoFactorLoginTemp);

            var user = await UserManager.FindByNameAsync(loginViewModel.CaradaId);

            // ユーザー情報が取得できないか利用開始されていない場合エラー
            if (user == null || !user.EmailConfirmed)
            {
                throw new HttpException("This user is unregistered or already deleted.");
            }

            // 認証コードが無効かどうかはフレームワークでは制御しないので、業務ロジックでコード検証前に判定する必要がある
            if (UserManager.IsInvalidVerifyCode(user))
            {
                LOG.Error("認証コード無効化済");
                ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                return View(model);
            }

            // 認証コードの検証 パスワード失敗回数の加算はトランザクション管理しない
            if (!await UserManager.VerifyTwoFactorTokenAsync(user.Id,
                Properties.Settings.Default.TwoFactorLoginSendCodeProvider, model.VerifyCode))
            {
                if (await UserManager.IsVerifyCodeInvalidNow(user))
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeInvalidError);
                    LOG.Error("認証コード無効化");
                }
                else
                {
                    ModelState.AddModelError("VerifyCode", ErrorMessages.VerifyCodeError);
                    LOG.Warn("認証コード入力失敗");
                }
                return View(model);
            }

            // 認証コード初期化
            user.AuthCodeFailedCount = 0;
            user.AuthCodeExpireDateUtc = null;
            await UserManager.UpdateAsync(user);

            // 二段階認証完了処理
            twoFactorAuthenticator.AddTwoFactorLoginCookie(Response, twoFactorAuthenticator.CompleteTwoFactorLogin(user.Id, user.UserName));

            await UserManager.ResetAccessFailedCountAsync(user.Id);

            if ((await SignInManager.SignInAsync(loginViewModel.CaradaId, loginViewModel.Password, loginViewModel.IsKeepLogin, false)) != SignInStatusEx.Success)
            {
                throw new HttpException("Login Error at Two Factor Login.");
            }

            return GoToReturnUrl();
        }

        /// <summary>
        /// ログアウトを行い、ログイン画面に遷移する
        /// </summary>
        /// <returns>ログイン画面のViewResult</returns>
        [Authorize]
        public async Task<ActionResult> Logout()
        {
            await SignInManager.SignOut();

            // TempDataをクリアする
            TempData.Clear();

            // ログイン画面を初期表示
            return RedirectToAction("LoginView", "User");
        }

    }
}