﻿using Microsoft.Azure;
using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Filters;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Properties;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MTI.CaradaIdProvider.Web.Controllers.Api
{
    [JwtValidationFilter]
    public class ManagementController : BaseApiController
    {
        /// <summary>
        /// IDジェネレーター
        /// </summary>
        private IdGenerator idGenerator = new IdGenerator();

        private static readonly string JwtTarget = Settings.Default.AuthProviderClientName;

        private static readonly string AuthProviderManagementAddUserUrl = CloudConfigurationManager.GetSetting(Settings.Default.AuthProviderManagementAddUserUrl);

        private JwtApiRequest jwtRequest = new JwtApiRequest(JwtTarget, AuthProviderManagementAddUserUrl, new WebApiInvoker());

        /// <summary>
        /// 利用開始前ユーザー登録API
        /// </summary>
        /// <param name="condition">JWT形式のユーザー登録情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public async Task<HttpResponseMessage> AddUser([FromBody]Jwt<RegistUserAddCondition> condition)
        {
            // CARADA IDの登録済みチェック
            var user = await UserManager.FindByNameAsync(condition.Parameter.CaradaId);

            if (user != null)
            {
                var errorResult = new
                {
                    carada_id = condition.Parameter.CaradaId,
                    error = "registered_carada_id",
                    error_description = "登録済みのCARADA IDです"
                };
                APP_LOG.Error(string.Format("登録済みのCARADA IDです。(CARADA ID={0})", condition.Parameter.CaradaId));
                return Request.CreateResponse(HttpStatusCode.BadRequest, errorResult);
            }

            // ユーザー情報登録処理
            var newUser = new CaradaIdUser
            {
                UserName = condition.Parameter.CaradaId,
                EmailConfirmed = false,
                TwoFactorEnabled = true,
                CreateDateUtc = DateTime.UtcNow,
                AuthCodeFailedCount = 0,
                LockoutEnabled = true,
                EmployeeFlag = true,
                AccessFailedCount = 0
            };

            // パスワード生成
            var password = idGenerator.GenerateRandomPassword();

            // ユーザー登録
            await UserManager.CreateAsync(newUser, password);

            // CARADA IDでユーザー情報を検索
            user = await UserManager.FindByNameAsync(condition.Parameter.CaradaId);

            // ユーザーロールを登録
            await UserManager.AddToRoleAsync(user.Id, Properties.Settings.Default.UserRole);

            // 統合認可プロバイダーの認可情報を登録
            var clientData = CaradaIdPDb.ClientMasters.First(o => o.ClientName == Properties.Settings.Default.AuthProviderClientName);

            // 統合認可用の中間IDを生成
            var sub = idGenerator.GenerateSubjectId();

            // 統合認可用の中間ID情報を登録
            CaradaIdPDb.AuthorizedUsers.Add(new AuthorizedUsers
            {
                UserId = user.Id,
                ClientId = clientData.ClientId,
                Sub = sub
            });

            CaradaIdPDb.SaveChanges();

            // 統合認可のユーザー登録API実行
            var apiResponse = new HttpResponseMessage();
            try
            {
                apiResponse = RegistUser(condition.Parameter.ClientId, sub);
            }
            catch (Exception e)
            {
                APP_LOG.Error(e, "サーバエラーが発生しました。");

                // API実行に失敗した場合、登録したユーザー情報を削除
                await UserManager.DeleteAsync(user);
                APP_LOG.Error("統合認可プロバイダのユーザー登録API実行でエラーが発生しました");

                return Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    carada_id = condition.Parameter.CaradaId,
                    error = "auth_provider_error",
                    error_description = "統合認可プロバイダのユーザー登録API実行でエラーが発生しました",
                });
            }

            // API実行結果の確認
            if (!apiResponse.IsSuccessStatusCode)
            {
                // API実行に失敗した場合、登録したユーザー情報を削除
                await UserManager.DeleteAsync(user);

                var error = apiResponse.Content.ReadAsStringAsync().Result;

                APP_LOG.Error("統合認可プロバイダのユーザー登録APIの実行に失敗しました。 response=" + error);

                var original_error = string.Empty;
                if (!string.IsNullOrEmpty(error))
                {
                    try
                    {
                        var retrunError = JToken.Parse(error);
                        original_error = (string)retrunError["error"];
                    }
                    catch (Exception)
                    {
                    }
                }

                var errorResult = new
                {
                    carada_id = condition.Parameter.CaradaId,
                    error = "auth_provider_error",
                    error_description = "統合認可プロバイダのユーザー登録に失敗しました",
                    original_error = original_error
                };

                // 統合認可API情報取得に失敗してエラーとなった場合は一律 BadRequest にする。
                return Request.CreateResponse(HttpStatusCode.BadRequest, errorResult);
            }

            var apiResult = JToken.Parse(apiResponse.Content.ReadAsStringAsync().Result);
            APP_LOG.Info(string.Format("統合認可のユーザー登録API：HTTPステータスコード={0}, 実行結果={1})", apiResponse.StatusCode, apiResult));

            var apiUid = (string)apiResult["uid"];
            var apiAccessToken = (string)apiResult["access_token"];
            var apitokenType = (string)apiResult["token_type"];

            APP_LOG.Info("統合認可プロバイダのユーザー登録APIの実行成功(Uid={0}, AccessToken={1}, TokenType={2})", apiUid, apiAccessToken, apitokenType);

            // 結果を返す
            var result = new { carada_id = condition.Parameter.CaradaId, password = password, uid = apiUid };

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// ユーザー情報リセットAPI
        /// </summary>
        /// <param name="condition">JWT形式のユーザー登録情報</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public async Task<HttpResponseMessage> ResetUserInfo([FromBody]Jwt<ResetUserCondition> condition)
        {
            // CARADA IDの登録済みチェック
            var user = await UserManager.FindByNameAsync(condition.Parameter.CaradaId);

            if (user == null)
            {
                var errorResult = new
                {
                    carada_id = condition.Parameter.CaradaId,
                    error = "invalid_carada_id",
                    error_description = "CARADA IDに紐付くユーザー情報がありません。"
                };
                APP_LOG.Error(string.Format("CARADA IDに紐付くユーザー情報がありません。(CARADA ID={0})", condition.Parameter.CaradaId));
                return Request.CreateResponse(HttpStatusCode.BadRequest, errorResult);
            }

            // ログイン失敗回数・ロックアウト期間初期化
            user.AccessFailedCount = 0;
            user.LockoutEndDateUtc = null;
            // 更新日時
            user.UpdateDateUtc = DateTime.UtcNow;

            // params:initializationが"1"の場合、利用開始前に初期化
            if (condition.Parameter.Initialization == "1")
            {
                user.Email = null;
                user.EmailConfirmed = false;
            }

            // ユーザー情報更新
            await UserManager.UpdateAsync(user);

            // パスワード生成
            var generatePassword = idGenerator.GenerateRandomPassword();

            // パスワードリセットのAPIには専用のtokenの生成を必要とする
            var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

            // パスワード再設定
            await UserManager.ResetPasswordAsync(user.Id, token, generatePassword);

            // ユーザー情報を更新したのでセキュリティスタンプを更新する
            await UserManager.UpdateSecurityStampAsync(user.Id);

            var result = new
            {
                carada_id = condition.Parameter.CaradaId,
                password = generatePassword,
                initialization = (condition.Parameter.Initialization == "1" ? "1" : "0")
            };

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 統合認可のユーザー登録APIへ、JWTヘッダありでユーザー登録を行う。
        /// </summary>
        /// <remarks>プロフィール部分はCARADA側指定の固定値。</remarks>
        /// <param name="clientId">CARADAを示す統合認可のクライアントID</param>
        /// <param name="sub">統合認可用中間ID</param>
        /// <returns>HttpResponseMessage</returns>
        private HttpResponseMessage RegistUser(string clientId, string sub)
        {
            var profile = new Dictionary<string, object>();

            var commonProfile = new Dictionary<string, string>();
            commonProfile.Add("nick_name", "XXXXXXXXXXXXXXXXXXXX");
            commonProfile.Add("gender", "2");
            commonProfile.Add("birthdate", "19000101");
            commonProfile.Add("area_code", "JP-13");

            var specificProfile = new Dictionary<string, string>();
            specificProfile.Add("user_type", "0");
            specificProfile.Add("height", "300");
            specificProfile.Add("weight", "1");
            specificProfile.Add("permission_license", "2");

            profile.Add("common", commonProfile);
            profile.Add("specific", specificProfile);

            var parameters = new Dictionary<string, object>();
            parameters.Add("client_id", clientId);
            parameters.Add("platform_id", "HCPF");
            parameters.Add("provider_type", "7");
            parameters.Add("sub", sub);
            parameters.Add("profile", profile);

            var response = jwtRequest.PostJson(parameters);

            return response;
        }
    }
}