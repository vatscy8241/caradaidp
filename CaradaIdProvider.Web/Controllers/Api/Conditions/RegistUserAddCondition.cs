﻿using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Controllers.Api.Conditions
{
    /// <summary>
    /// 利用開始前ユーザー登録API用Condition
    /// </summary>
    public class RegistUserAddCondition : ConditionBase
    {
        /// <summary>
        /// クライアントID
        /// </summary>
        [Required(ErrorMessage = "invalid_client_id")]
        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required(ErrorMessage = "invalid_carada_id")]
        [JsonProperty("carada_id")]
        [RegularExpression(@"^(?=[0-9a-z])([-_.]?[a-z0-9])*[-_.]?$", ErrorMessage = "invalid_carada_id")]
        public string CaradaId { get; set; }
    }
}