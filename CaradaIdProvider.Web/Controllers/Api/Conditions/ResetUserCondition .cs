﻿using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Controllers.Api.Conditions
{
    /// <summary>
    /// ユーザー情報リセットAPI用Condition
    /// </summary>
    public class ResetUserCondition : ConditionBase
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required(ErrorMessage = "invalid_carada_id")]
        [JsonProperty("carada_id")]
        public string CaradaId { get; set; }

        /// <summary>
        /// 初期化フラグ
        /// </summary>
        [JsonProperty("initialization")]
        public string Initialization { get; set; }
    }
}