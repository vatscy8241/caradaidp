﻿using Microsoft.AspNet.Identity.Owin;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System.Web;
using System.Web.Http;

namespace MTI.CaradaIdProvider.Web.Controllers.Api
{
    /// <summary>
    /// API系のベースコントローラクラス。
    /// </summary>
    public abstract class BaseApiController : ApiController
    {
        /// <summary>
        /// ApplicationUserManager
        /// </summary>
        private ApplicationUserManager _userManager;

        /// <summary>
        /// CaradaIdPDbContext
        /// </summary>
        private CaradaIdPDbContext _caradaIdPDbContext;

        /// <summary>
        /// アプリケーションログ
        /// </summary>
        protected static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// 監査ログ
        /// </summary>
        protected static readonly ILogger AUDIT_LOG = LogManager.GetLogger(Settings.Default.LogTargetAudit);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public BaseApiController()
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="userManager">ApplicationUserManager</param>
        public BaseApiController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        /// <summary>
        /// UserManager
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// CaradaIdPDbContext
        /// </summary>
        public CaradaIdPDbContext CaradaIdPDb
        {
            get
            {
                return _caradaIdPDbContext ?? HttpContext.Current.GetOwinContext().Get<CaradaIdPDbContext>();
            }
            private set
            {
                _caradaIdPDbContext = value;
            }
        }
    }
}