﻿using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MTI.CaradaIdProvider.Web.Controllers.Api.Jwt
{
    /// <summary>
    /// JWTが不正な場合にエラーレスポンスを作成します。
    /// このクラスを継承して CreateApplicationResponse メソッドを実装してください。
    /// </summary>
    public abstract class JwtValidationFilterBaseAttribute : ActionFilterAttribute
    {
        protected static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// エラーコードからエラーレスポンスを生成します。
        /// </summary>
        /// <param name="actionContext">実行するアクションに関する情報</param>
        /// <param name="errorCode">エラーコード</param>
        /// <returns>エラーレスポンス</returns>
        protected abstract HttpResponseMessage CreateApplicationResponse(HttpActionContext actionContext, string errorCode);

        /// <summary>
        /// JWTが不正な場合にエラーレスポンスを作成し設定します。
        /// JWTの検証後に呼び出されます。
        /// </summary>
        /// <param name="actionContext">実行するアクションに関する情報</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ActionArguments.Any(arg => arg.Value == null))
            {
                APP_LOG.Error("[JWT_Invalid] リクエストパラメータにJWTのTokenが見つかりません");
                actionContext.Response = CreateApplicationResponse(actionContext, JwtErrorCodes.JwtInvalidRequest);
            }
            else if (!actionContext.ModelState.IsValid)
            {
                var errorCode = PickUpErrorCode(actionContext);
                actionContext.Response = CreateApplicationResponse(actionContext, errorCode);
            }
        }

        #region private

        /// <summary>
        /// HttpActionContextからエラーコードを抽出します。
        /// 複数エラーが発生している場合はその中から1つエラーコードを抽出します。
        /// </summary>
        /// <param name="actionContext">実行するアクションに関する情報</param>
        /// <returns>エラーコード</returns>
        private string PickUpErrorCode(HttpActionContext actionContext)
        {
            var errorCodes = GetErrorMessages(actionContext).ToList();

            if (errorCodes.Contains(string.Empty))
            {
                errorCodes.RemoveAll(e => e == string.Empty);
                errorCodes.Add(JwtErrorCodes.JwtInvalidRequest);
            }

            errorCodes.Sort();
            return errorCodes.First();
        }

        /// <summary>
        /// アクション情報からエラーメッセージを全て取得します。
        /// </summary>
        /// <param name="actionContext">実行するアクションに関する情報</param>
        /// <returns>エラーコード</returns>
        private IEnumerable<string> GetErrorMessages(HttpActionContext actionContext)
        {
            var modelErrorCollections = actionContext.ModelState.Select(e => e.Value.Errors);
            return modelErrorCollections.SelectMany(ms => ms.Select(m => m.ErrorMessage));
        }

        #endregion
    }
}