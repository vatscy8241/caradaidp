﻿using MTI.CaradaIdProvider.Web.Extention;
using MTI.CaradaIdProvider.Web.Properties;
using MTI.CaradaIdProvider.Web.Utilities;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Security.Tokens;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Controllers.Api.Jwt
{
    /// <summary>
    /// JWTの検証と固有パラメータのバインド・検証を行います。
    /// このクラスを継承して GetSecretKey メソッドを実装してください。
    /// </summary>
    /// <typeparam name="TParameter">固有パラメータオブジェクト</typeparam>
    public abstract class JwtBase<TParameter> : ConditionBase
        where TParameter : ConditionBase, new()
    {
        private static readonly ILogger AUDIT_LOG = LogManager.GetLogger(Settings.Default.LogTargetAudit);
        private static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// 発行者IDに対するシークレットキーを取得します。
        /// 見つからない場合にはnullを返します。
        /// Validateメソッド内で呼ばれ、キーが見つからないまたはキーが不正の場合には jwt_invalid_issuer エラーが発生します。
        /// </summary>
        /// <param name="issuer">発行者ID</param>
        /// <returns>シークレットキー</returns>
        protected abstract string GetSecretKey(string issuer);

        /// <summary>
        /// JWS文字列
        /// </summary>
        [Required(ErrorMessage = JwtErrorCodes.JwtInvalidRequest)]
        public string Token { get; set; }

        /// <summary>
        /// APIの固有パラメータ
        /// </summary>
        [JsonIgnore]
        public TParameter Parameter { get; private set; }

        /// <summary>
        /// JWTの検証とパラメータチェックを行います。
        /// モデルバインディング時（アクション実行前）に呼び出されます。
        /// </summary>
        /// <param name="context">検証チェックの実行コンテキスト</param>
        /// <returns>検証結果</returns>
        public override sealed IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            var payload = DeserializeJws();
            if (payload == null || !payload.HasRequiredParameters())
            {
                return new[] { new ValidationResult(JwtErrorCodes.JwtInvalidRequest) };
            }

            var key = GetSecretKey(payload.Iss);
            if (string.IsNullOrEmpty(key) || !ValidateSignature(key))
            {
                return new[] { new ValidationResult(JwtErrorCodes.JwtInvalidIssuer) };
            }
            // ログ出力などのため、IssuerをItemsに登録する。
            HttpContext.Current.Items[Settings.Default.LogLayoutUserNameIss] = payload.Iss;

            if (!payload.IsLifetimeValid())
            {
                return new[] { new ValidationResult(JwtErrorCodes.JwtInvalidLifeTime) };
            }

            Parameter = payload.Params;
            AUDIT_LOG.Info("[REQUEST PARAMS] " + LogUtilities.GetParameterString(Parameter));
            return payload.ValidateSpecificParams(context);
        }

        #region private

        /// <summary>
        /// JWSのデシリアライズを行います。
        /// </summary>
        /// <returns>JWT Payload</returns>
        private JwtPayload DeserializeJws()
        {
            try
            {
                var payloadBase64Url = Token.Split('.')[1];
                var payloadJson = payloadBase64Url.Base64UrlDecode();
                return JsonConvert.DeserializeObject<JwtPayload>(payloadJson);
            }
            catch
            {
                APP_LOG.Error("[JWT_Invalid] JWTのデシリアライズに失敗しました。");
                return null;
            }
        }

        /// <summary>
        /// 署名検証を行います。
        /// JWTのパラメータチェックなどはここでは行いません。
        /// </summary>
        /// <param name="key">シークレットキー</param>
        /// <returns>署名が有効の場合はtrue、無効の場合はfalse。</returns>
        private bool ValidateSignature(string key)
        {
            var parameters = new TokenValidationParameters
            {
                RequireSignedTokens = true,
                IssuerSigningToken = GenerateHmacSha256SigningToken(key),
                RequireExpirationTime = false,
                ValidateLifetime = false,
                ValidateIssuer = false,
                ValidateActor = false,
                ValidateAudience = false
            };

            try
            {
                SecurityToken token;
                new JwtSecurityTokenHandler().ValidateToken(Token, parameters, out token);
                return true;
            }
            catch
            {
                APP_LOG.Error("[JWT_Invalid] SecurityTokenの署名検証に失敗しました。");
                return false;
            }
        }

        /// <summary>
        /// シークレットキーを指定してSecurityTokenを生成します。
        /// </summary>
        /// <param name="key">シークレットキー</param>
        /// <returns>トークン</returns>
        private SecurityToken GenerateHmacSha256SigningToken(string key)
        {
            var keyBytes = Encoding.UTF8.GetBytes(key);
            if (keyBytes.Length < 64)
            {
                Array.Resize(ref keyBytes, 64);
            }

            return new BinarySecretSecurityToken(keyBytes);
        }

        private class JwtPayload
        {
            public string Iss { get; set; }
            public long? Iat { get; set; }
            public long? Exp { get; set; }
            public long? Nbf { get; set; }
            public TParameter Params { get; set; }

            /// <summary>
            /// Paramsがnullにならないようにします。
            /// </summary>
            public JwtPayload()
            {
                Params = new TParameter();
            }

            /// <summary>
            /// 必須パラメータが全て存在するかどうかを判断します。
            /// </summary>
            /// <returns>全て存在すればtrue、それ以外はfalse。</returns>
            public bool HasRequiredParameters()
            {
                if (string.IsNullOrEmpty(Iss))
                {
                    APP_LOG.Error("[JWT_Invalid] Issがnullまたは空です。");
                }
                if (Iat == null)
                {
                    APP_LOG.Error("[JWT_Invalid] Iatがnullです。");
                }
                if (Exp == null)
                {
                    APP_LOG.Error("[JWT_Invalid] Expがnullです。");
                }
                if (Nbf == null)
                {
                    APP_LOG.Error("[JWT_Invalid] Nbfがnullです。");
                }
                return !string.IsNullOrEmpty(Iss) && Iat != null && Exp != null && Nbf != null;
            }

            /// <summary>
            /// トークンが有効期限内かどうかを判断します。
            /// </summary>
            /// <returns>有効期限内であればtrue、そうでなければfalse。</returns>
            public bool IsLifetimeValid()
            {
                var utcNow = DateTime.UtcNow.ToUnixTime();
                if (Nbf >= utcNow)
                {
                    var dateTimeString = FromUnixTime(Nbf).ToString("s");
                    var utcString = FromUnixTime(utcNow).ToString("s");
                    APP_LOG.Error($"[JWT_Invalid] Nbfが未来です。(Nbf={dateTimeString}, utcNow={utcString})");
                }
                if (utcNow >= Exp)
                {
                    var dateTimeString = FromUnixTime(Exp).ToString("s");
                    var utcString = FromUnixTime(utcNow).ToString("s");
                    APP_LOG.Error($"[JWT_Invalid] Expが過去です。(Exp={dateTimeString}, utcNow={utcString})");
                }
                return Nbf < utcNow && utcNow < Exp;
            }

            /// <summary>
            /// 固有パラメータの検証を行います。
            /// 最初に各プロパティのValidationAttributeによる検証を行い、
            /// 問題が無ければValidateメソッドを呼び出します。
            /// </summary>
            /// <param name="context">検証チェックの実行コンテキスト</param>
            /// <returns>検証結果</returns>
            public IEnumerable<ValidationResult> ValidateSpecificParams(ValidationContext context)
            {
                var results = ValidateParamsProperties().ToList();
                return results.Count > 0 ? results : Params.Validate(context);
            }

            /// <summary>
            /// 全てのプロパティのバリデーション属性を検証します。
            /// </summary>
            /// <returns>検証結果</returns>
            private IEnumerable<ValidationResult> ValidateParamsProperties()
            {
                foreach (var property in typeof(TParameter).GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var value = property.GetValue(Params);
                    foreach (var validator in (ValidationAttribute[])Attribute.GetCustomAttributes(property, typeof(ValidationAttribute)))
                    {
                        if (!validator.IsValid(value))
                        {
                            APP_LOG.Warn($"[JWT_Invalid] [Condition] ErrorAttribute={validator.GetType().Name.ToString()}, ErrorMessage={validator.ErrorMessage}");
                            yield return new ValidationResult(validator.ErrorMessage, new[] { property.Name });
                        }
                    }
                }
            }

            private static DateTime FromUnixTime(long? unixTime)
            {
                var UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return UnixEpoch.AddSeconds((long)unixTime).ToLocalTime();
            }

        }

        #endregion

    }
}