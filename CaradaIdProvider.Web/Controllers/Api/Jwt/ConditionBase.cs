﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace MTI.CaradaIdProvider.Web.Controllers.Api.Jwt
{
    /// <summary>
    /// 管理者用APIのTParameter部が継承する基底Conditionクラスです。
    /// </summary>
    public abstract class ConditionBase : IValidatableObject, IDisposable
    {
        /// <summary>
        /// TParameterを検証します。
        /// </summary>
        /// <param name="validationContext">検証コンテキスト</param>
        /// <returns>
        /// 失敗した検証の情報を保持するコレクション
        /// </returns>
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new ValidationResult[] { };
        }

        #region dispose
        /// <summary>
        /// アンマネージ リソースの解放およびリセットに関連付けられているアプリケーション定義のタスクを実行します。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// このクラスの実体が持つIDisposableなフィールドに対してはファイナライザでDispose()を実行するため、個別に実装する必要はありません。
        /// 個別に実装する場合は、各フィールドのDispose()が自動で呼び出されることを考慮してください。
        /// </summary>
        /// <param name="disposing">マネージ リソースとアンマネージ リソースの両方を解放する場合は true。アンマネージ リソースだけを解放する場合は false。</param>
        protected virtual void Dispose(bool disposing)
        {
            var hasDisposableFields = (from f in this.GetType().GetRuntimeFields()
                                       where f.FieldType.GetInterface("IDisposable") != null
                                       select f);

            foreach (var field in hasDisposableFields)
            {
                // public voidのDisposeをチェーンする
                var disposeMethod = field.FieldType.GetMethod("Dispose");
                if (disposeMethod != null)
                {
                    var o = field.GetValue(this);
                    if (o != null)
                    {
                        disposeMethod.Invoke(o, new object[] { });
                    }
                }
            }
        }

        ~ConditionBase()
        {
            Dispose(false);
        }
        #endregion
    }
}