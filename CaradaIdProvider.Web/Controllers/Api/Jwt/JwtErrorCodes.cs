﻿
namespace MTI.CaradaIdProvider.Web.Controllers.Api.Jwt
{
    /// <summary>
    /// JWT検証時のエラーコードです。
    /// </summary>
    public static class JwtErrorCodes
    {
        public const string JwtInvalidRequest = "jwt_invalid_request";
        public const string JwtInvalidIssuer = "jwt_invalid_issuer";
        public const string JwtInvalidLifeTime = "jwt_invalid_life_time";
    }
}