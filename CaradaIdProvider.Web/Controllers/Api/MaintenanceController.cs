﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MTI.CaradaIdProvider.Web.Controllers.Api
{
    public class MaintenanceController : BaseApiController
    {
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return MaintenanceResult;
        }

        [HttpPost]
        public HttpResponseMessage Post()
        {
            return MaintenanceResult;
        }

        private HttpResponseMessage MaintenanceResult
        {
            get
            {

                return Request.CreateResponse(HttpStatusCode.ServiceUnavailable, new
                {
                    error = "maintenance",
                    error_description = "現在サーバーのメンテナンス中です。"
                });
            }
        }
    }
}