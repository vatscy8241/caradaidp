﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MTI.CaradaIdProvider.Web.Logics
{
    /// <summary>
    /// RSAで暗号化, 復号化を行うクラス。
    /// IDTokenの署名復号での使用を想定している。
    /// .NetではRSA鍵テキスト方式がOpenSSLと異なるため、Bouncy Castleパッケージを使用する。
    /// </summary>
    public class RSAEncryptor
    {
        private IAsymmetricBlockCipher rsa;
        private AsymmetricKeyParameter keyParameter;

        /// <summary>
        /// コンストラクタ。鍵データが公開鍵か秘密鍵かは自動で判定する。
        /// </summary>
        /// <param name="key">OpenSSLの.pem形式にエクスポートされた鍵データ</param>
        public RSAEncryptor(string key)
        {
            using (var r = new StringReader(key))
            {
                // PemReaderというのはIDisposableじゃなかった。
                var secretReader = new PemReader(r);

                var readObject = secretReader.ReadObject();
                if (readObject is AsymmetricCipherKeyPair)
                {
                    // 秘密鍵が入力された場合
                    keyParameter = ((AsymmetricCipherKeyPair)readObject).Private;
                }
                else if (readObject is AsymmetricKeyParameter)
                {
                    // 公開鍵が入力された場合
                    keyParameter = (AsymmetricKeyParameter)readObject;
                }
                else
                {
                    throw new CryptographicException("Unsupported format.");
                }
                rsa = new Pkcs1Encoding(new RsaEngine());
            }
        }

        /// <summary>
        /// 暗号化を行う。文字列はUTF8として扱う。
        /// </summary>
        /// <param name="plainString">平文</param>
        /// <returns>
        /// Base64エンコーディングした暗号化文
        /// </returns>
        /// <exception cref="System.Security.Cryptography.CryptographicException">暗号化に失敗した場合</exception>                
        public string Encrypt(string plainString)
        {
            try
            {
                rsa.Init(true, keyParameter);
                byte[] bytes = Encoding.UTF8.GetBytes(plainString);
                return Convert.ToBase64String(rsa.ProcessBlock(bytes, 0, bytes.Length));
            }
            catch (Exception e)
            {
                throw new CryptographicException("Encrypt error.", e);
            }
        }

        /// <summary>
        /// 復号化を行う。
        /// </summary>
        /// <param name="encryptedString">Base64エンコーディングした暗号文</param>
        /// <returns>
        /// 平文
        /// </returns>
        /// <exception cref="System.Security.Cryptography.CryptographicException">復号化に失敗した場合</exception>
        public string Decrypt(string encryptedString)
        {
            try
            {
                rsa.Init(false, keyParameter);
                byte[] bytes = Convert.FromBase64String(encryptedString);
                return Encoding.UTF8.GetString(rsa.ProcessBlock(bytes, 0, bytes.Length));
            }
            catch (Exception e)
            {
                throw new CryptographicException("Decrypt error.", e);
            }
        }

        /// <summary>
        /// System.Security.Cryptography.RSAに変換する。
        /// </summary>
        /// <returns>RSAオブジェクト</returns>
        public RSA ToRSA()
        {
            if (!keyParameter.IsPrivate)
            {
                return DotNetUtilities.ToRSA((RsaKeyParameters)keyParameter);
            }
            return DotNetUtilities.ToRSA((RsaPrivateCrtKeyParameters)keyParameter);
        }
    }
}
