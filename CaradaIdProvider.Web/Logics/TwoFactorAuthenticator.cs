﻿using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Models.AuthorizationStorageEntities;
using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Logics
{
    /// <summary>
    /// 二段階認証のデータ管理を行います。
    /// </summary>
    public class TwoFactorAuthenticator
    {
        /// <summary>
        /// ログ
        /// </summary>
        protected static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// IdGenerator
        /// </summary>
        private IdGenerator idGenerator = new IdGenerator();

        /// <summary>
        /// 二段階認証を行いCookie情報を返却します。
        /// CookieはSecure属性、Httponly属性、有効期限10年とする。
        /// </summary>
        /// <param name="userId">ユーザーID(PKとなるID)</param>
        /// <param name="userName">ユーザー名（CARADA ID）</param>
        /// <returns>Cookie情報</returns>
        public virtual CookieModel CompleteTwoFactorLogin(string userId, string userName)
        {
            var storageKey = idGenerator.GenerateTwoFactorLoginKey();
            AuthorizationStorageContext.TwoFactorLogin.InsertEntity(new TwoFactorLoginEntity()
            {
                PartitionKey = storageKey,
                RowKey = Settings.Default.RowKeyValue,
                UserId = userId
            });

            LOG.Info(string.Format("二段階認証完了(UserId={0}, CARADA ID={1})", userId, userName));

            return new CookieModel
            {
                Name = Properties.Settings.Default.TwoFactorLoginCookieName,
                Value = storageKey,
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddYears(Settings.Default.TwoFactorLoginCookieExpireYear),
                Path = "/",
                Secure = true
            };
        }

        /// <summary>
        /// 二段階認証処理から受け取ったCookie情報を用いてResponseにCookieを登録する。
        /// </summary>
        /// <param name="response">HttpResponseBase</param>
        /// <param name="cookie">CookieModel</param>
        public virtual void AddTwoFactorLoginCookie(HttpResponseBase response, CookieModel cookie)
        {
            // 引数がnullの時は何もしない
            if (response == null || cookie == null)
            {
                return;
            }

            response.Cookies.Add(new HttpCookie(cookie.Name, cookie.Value)
            {
                Secure = cookie.Secure,
                HttpOnly = cookie.HttpOnly,
                Expires = cookie.Expires,
                Path = cookie.Path
            });
        }
    }
}