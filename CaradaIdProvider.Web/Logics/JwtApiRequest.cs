﻿using Microsoft.Azure;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Properties;
using MTI.CaradaIdProvider.Web.Utilities;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Net.Http;
using System.Security.Claims;
using System.Text;

namespace MTI.CaradaIdProvider.Web.Logics
{
    /// <summary>
    /// JWTを使用した各システムのAPIを呼び出すクラスです。
    /// </summary>
    public class JwtApiRequest : IDisposable
    {
        private string targetName;
        private string url;
        private IWebApiInvoker webApiInvoker;

        private ApiTargetInfo targetInfo = new ApiTargetInfo();

        protected static readonly ILogger APP_LOG = LogManager.GetLogger(Settings.Default.LogTargetApiApplication);

        /// <summary>
        /// JWTのtarget,issuer,secretKeyを提供する設定文字列（JSON）
        /// </summary>
        private string apiTargetInfo = CloudConfigurationManager.GetSetting(Settings.Default.ApiTargetInfo);

        public JwtApiRequest(string targetName, string targetUrl, IWebApiInvoker webApiInvoker)
        {
            this.targetName = targetName;
            this.url = targetUrl;
            this.webApiInvoker = webApiInvoker;
            targetInfo.Load(apiTargetInfo);
        }

        public JwtApiRequest(string targetName, string targetUrl)
            : this(targetName, targetUrl, new WebApiInvoker())
        {
        }

        /// <summary>
        /// POSTによってAPIを実行します。JWTのtokenをパラメータとします。
        /// </summary>
        /// <param name="parameters">APIパラメータ</param>
        /// <returns>Response</returns>
        public virtual HttpResponseMessage PostJson(IDictionary<string, object> parameters = null)
        {
            var credentials = new SigningCredentials(
                GetHmacSha256SigningKey(),
                SecurityAlgorithms.HmacSha256Signature,
                SecurityAlgorithms.Sha256Digest);

            var iat = DateTime.UtcNow.ToUnixTime();
            var exp = iat + 120;
            var nbf = iat - 120;

            var jwtHeader = new JwtHeader(credentials);
            var jwtPayload = new JwtPayload();

            var claims = new[] {
                new Claim("iss", targetInfo.GetApiTarget(targetName).Issuer),
                new Claim("iat", iat.ToString(), ClaimValueTypes.Integer64),
                new Claim("exp", exp.ToString(), ClaimValueTypes.Integer64),
                new Claim("nbf", nbf.ToString(), ClaimValueTypes.Integer64)
            };

            jwtPayload.AddClaims(claims);

            var token = new JwtSecurityToken(jwtHeader, jwtPayload);

            if (parameters != null)
            {
                token.Payload.Add("params", parameters);
            }
            var encodedToken = new JwtSecurityTokenHandler().WriteToken(token);
            APP_LOG.Info($"url:[{url}], token:[{encodedToken}]");
            // 設定値 0 は HTTP を表します。
            HttpResponseMessage response = webApiInvoker.PostJson(
                url,
                new Dictionary<string, List<string>> { },
                new JObject
                {
                    {"token", encodedToken}
                });
            return response;
        }

        private SymmetricSecurityKey GetHmacSha256SigningKey()
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(targetInfo.GetApiTarget(targetName).SecretKey);
            if (keyBytes.Length < 64)
            {
                Array.Resize(ref keyBytes, 64);
            }

            return new InMemorySymmetricSecurityKey(keyBytes);
        }

        /// <summary>
        /// アンマネージ リソースの解放およびリセットに関連付けられているアプリケーション定義のタスクを実行します。
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// アンマネージ リソースを解放します。オプションでマネージ リソースも破棄します。
        /// </summary>
        /// <param name="disposing">
        ///     マネージ リソースとアンマネージ リソースの両方を解放する場合は true。
        ///     アンマネージ リソースだけを解放する場合は false。
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (webApiInvoker != null)
                {
                    webApiInvoker.Dispose();
                }
            }
        }
    }
}