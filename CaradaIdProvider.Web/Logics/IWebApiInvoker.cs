﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace MTI.CaradaIdProvider.Web.Logics
{
    /// <summary>
    /// インターフェース
    /// このインターフェースを WebApiInvoker が継承します。
    /// 自動単体テストでモックを利用するためにインターフェースを継承しています。
    /// </summary>
    public interface IWebApiInvoker : IDisposable
    {
        HttpResponseMessage PostJson(string url, Dictionary<string, List<string>> headerValues, JObject parameter);
    }
}
