﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Logics
{
    /// <summary>
    /// ハッシュ化用クラス
    /// </summary>
    public class HashConverter
    {
        /// <summary>
        /// 引数の文字列をハッシュ化した文字列へ変換する
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public string ConvertString(string target)
        {
            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            var signerSha256 = SHA256.Create();
            var hashStringByte = signerSha256.ComputeHash(Encoding.UTF8.GetBytes(target));
            var hashString = "";
            foreach (byte x in hashStringByte)
            {
                hashString += String.Format("{0:x2}", x);
            }

            return hashString;
        }
    }
}