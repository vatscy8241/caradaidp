﻿using MTI.CaradaIdProvider.Web.Properties;
using MTI.CaradaIdProvider.Web.Utilities;
using NLog;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Filters
{
    /// <summary>
    /// ログフィルタークラス
    /// </summary>
    public class LoggingAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// ログ
        /// </summary>
        private static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);
        /// <summary>
        /// アクション実行前にアクセスログを出力する
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            // POSTの場合のパラメータありのログ出力
            if (request.HttpMethod == "POST" && request.Form.Count != 0)
            {
                LOG.Info("[ACCESS] parameters:[" + LogUtilities.GetParameterString(request.Unvalidated.Form) + "]");
                return;
            }
            // GETの場合のパラメータありのログ出力
            if (request.HttpMethod == "GET" && request.QueryString.Count != 0)
            {
                LOG.Info("[ACCESS] parameters:[" + LogUtilities.GetParameterString(request.Unvalidated.QueryString) + "]");
                return;
            }
            // パラメータなしのログ出力
            LOG.Info("[ACCESS]");
        }
    }
}