﻿using System.Reflection;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Filters
{
    /// <summary>
    /// submitしたボタンを決定するアノテーション。
    /// </summary>
    public class ButtonAttribute : ActionMethodSelectorAttribute
    {
        /// <summary>
        /// ボタン名
        /// </summary>
        public string ButtonName { get; set; }
        /// <summary>
        /// ボタン名を受け取るコンストラクタ
        /// </summary>
        /// <param name="buttonName">ボタン名</param>
        public ButtonAttribute(string buttonName)
        {
            ButtonName = buttonName;
        }

        /// <summary>
        /// 押されたボタンとボタン名が同じならばtrueを返す。
        /// </summary>
        /// <param name="context">ControllerContext</param>
        /// <param name="methodInfo">メソッド情報</param>
        /// <returns>確認結果</returns>
        public override bool IsValidForRequest(ControllerContext context, MethodInfo methodInfo)
        {
            return context.Controller.ValueProvider.GetValue(ButtonName) != null;
        }
    }
}