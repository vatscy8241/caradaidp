﻿using MTI.CaradaIdProvider.Web.Controllers.Api.Conditions;
using MTI.CaradaIdProvider.Web.Controllers.Api.Jwt;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace MTI.CaradaIdProvider.Web.Filters
{
    /// <summary>
    /// JWTが不正な場合にエラーレスポンスを作成します。
    /// </summary>
    public class JwtValidationFilterAttribute : JwtValidationFilterBaseAttribute
    {
        /// <summary>
        /// エラーコードからエラーレスポンスを生成します。
        /// </summary>
        /// <param param name="context">実行するアクションに関する情報</param>
        /// <param name="errorCode">エラーコード</param>
        /// <returns>エラーレスポンス</returns>
        protected override HttpResponseMessage CreateApplicationResponse(HttpActionContext context, string errorCode)
        {
            object result;

            try
            {
                var condition = context.ActionArguments["condition"];
                if (condition is JwtBase<RegistUserAddCondition>)
                {
                    result = new
                    {
                        carada_id = ((JwtBase<RegistUserAddCondition>)condition).Parameter.CaradaId,
                        error = errorCode,
                        error_description = "JWTのパラメーターが不正です"
                    };
                }
                else
                {
                    result = new
                    {
                        carada_id = ((JwtBase<ResetUserCondition>)condition).Parameter.CaradaId,
                        error = errorCode,
                        error_description = "JWTのパラメーターが不正です"
                    };
                }
            }
            catch (NullReferenceException)
            {
                result = new
                {
                    carada_id = "",
                    error = errorCode,
                    error_description = "JWTが不正です"
                };
            }

            APP_LOG.Warn("バリデーションチェックエラー");
            return context.Request.CreateResponse(HttpStatusCode.BadRequest, result);
        }
    }
}