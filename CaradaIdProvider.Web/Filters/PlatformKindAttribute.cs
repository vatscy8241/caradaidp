﻿using MTI.CaradaIdProvider.Web.Models;
using System.Linq;
using System.Web.Mvc;


namespace MTI.CaradaIdProvider.Web.Filters
{
    /// <summary>
    /// どこから来たかを判断して、情報を保持するフィルターです。
    /// </summary>
    public class PlatformKindAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// CARADA IDアプリのWebViewから来たときは対応する情報を保持します。
        /// </summary>
        /// <param name="filterContext">filterContext</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var httpContext = filterContext.HttpContext;

            var model = new AnalyticsSettingsModel();

            if (httpContext.Request.Headers.AllKeys.Any(s => s == "X-PlatformKind"))
            {
                httpContext.Items["PlatformKind"] = PlatformKind.App;
                model.Google.Dimension4 = "WebView";
                model.Google.Dimension21 = httpContext.Request.Headers.Get("X-PlatformKind");
            }
            else
            {
                httpContext.Items["PlatformKind"] = PlatformKind.Web;
                model.Google.Dimension4 = "Web";
            }

            if (httpContext.Request.Browser.IsMobileDevice)
            {
                model.Google.Dimension6 = "SP";
            }
            else
            {
                model.Google.Dimension6 = "PC";
            }

#if CI
            // CI時にGAPのCookie以外の有効期限も変更されてしまうためドメインを変更する。
            model.Google.Domain = "ciid.carada.jp";
#endif
            // ログイン画面の場合、絶対パスが/のみになるため
            if (httpContext.Request.Url.AbsolutePath == "/")
            {
                model.UserInsight.Domain = httpContext.Request.Url.AbsoluteUri.Substring(0, httpContext.Request.Url.AbsoluteUri.Length - 1);
            }
            else
            {
                model.UserInsight.Domain = httpContext.Request.Url.AbsoluteUri.Replace(httpContext.Request.Url.AbsolutePath, "");
            }
            httpContext.Items["Analytics"] = model;
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Action実行後の状態によって、Analytics情報に保持する値を決定します。
        /// </summary>
        /// <param name="filterContext">filterContext</param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var model = httpContext.Items["Analytics"] as AnalyticsSettingsModel;

            // ログイン状態
            var user = httpContext.User;
            if (user != null && user.Identity != null && user.Identity.IsAuthenticated)
            {
                model.Google.Dimension1 = "ログイン";
            }

            base.OnActionExecuted(filterContext);
        }
    }
}