﻿using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Filters
{
    /// <summary>
    /// バリデーションチェックのフィルタクラス
    /// </summary>
    public class ValidationCheckAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// ログ
        /// </summary>
        protected static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// アクション実行前にバリデーションチェック結果を確認し、エラーの場合は同じViewを表示する。
        /// 実際にはRequest送信前にJQueryでも行われるため、ほとんどの場合はエラーとならない。
        /// </summary>
        /// <param name="filterContext">filterContext</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var viewData = filterContext.Controller.ViewData;

            // Controller, ViewDataにnullはない
            if (!viewData.ModelState.IsValid)
            {
                LOG.Warn("バリデーションチェックエラー");

                var tempData = filterContext.Controller.TempData;
                // tempDataにnullはない
                if (tempData.Count > 0)
                {
                    tempData.Keep();
                }

                filterContext.Result = new ViewResult
                {
                    ViewData = viewData,
                    TempData = tempData
                };
            }

            base.OnActionExecuting(filterContext);
        }
    }
}