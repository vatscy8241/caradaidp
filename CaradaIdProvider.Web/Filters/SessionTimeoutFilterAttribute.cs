﻿using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System.Web.Mvc;
using System.Web.Routing;

namespace MTI.CaradaIdProvider.Web.Filters
{
    /// <summary>
    /// セッションタイムアウトフィルター
    /// </summary>
    public class SessionTimeoutFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// ログ
        /// </summary>
        private static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// セッションのキー名
        /// </summary>
        public string SessionKeyName { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SessionTimeoutFilterAttribute()
        {
        }

        /// <summary>
        /// セッション名を受け取るコンストラクタ
        /// </summary>
        /// <param name="sessionKeyName"></param>
        public SessionTimeoutFilterAttribute(string sessionKeyName)
        {
            SessionKeyName = sessionKeyName;
        }

        /// <summary>
        /// アクション実行前の処理
        /// </summary>
        /// <param name="filterContext">ActionExecutingContext</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = filterContext.HttpContext;
            // セッションがない場合。指定されたセッション名がない場合
            if (context.Session == null || context.Session.Count == 0
                || (SessionKeyName != null && context.Session[SessionKeyName] == null))
            {
                LOG.Warn("セッションタイムアウトエラー");

                // セッションタイムアウトエラー画面を表示する
                filterContext.Result = new RedirectToRouteResult(
                     new RouteValueDictionary { { "controller", "Error" }, { "action", "Timeout" } });
            }

            // 処理続き
            base.OnActionExecuting(filterContext);
        }
    }
}