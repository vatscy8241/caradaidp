﻿using System;
using System.Text;

namespace MTI.CaradaIdProvider.Web.Extention
{
    /// <summary>
    /// System.String 型の拡張メソッドを提供します。
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Base64 URL エンコードされた文字列をUTF-8文字列にデコードします。
        /// </summary>
        /// <param name="encoded">Base64 URL エンコードされた文字列</param>
        /// <returns>デコード結果</returns>
        public static string Base64UrlDecode(this string encoded)
        {
            var base64 = Base64UrlToBase64(encoded);
            var decbuff = Convert.FromBase64String(base64);
            return Encoding.UTF8.GetString(decbuff);
        }

        private static string Base64UrlToBase64(string input)
        {
            var output = input.Replace('-', '+').Replace('_', '/');
            var lastBytes = output.Length % 4;
            if (lastBytes > 0)
            {
                output = output.PadRight(output.Length + 4 - lastBytes, '=');
            }
            return output;
        }
    }
}