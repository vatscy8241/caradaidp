REM Script to configuration of maintenance mode

REM Skip this script execution in the case of local emulator or maintenance mode off
IF "%CONFIG%" == "Local" goto SkipScript

REM Set Variable Parameter --------------------------
REM  web_match_string:
REM    specify Web page URL string of the maintenance mode (regular expression).
REM    "" (The two double-quotes) If you do not set.
REM  api_match_string:
REM    specify API URL string of the maintenance mode (regular expression).
REM    "" (The two double-quotes) If you do not set.
REM  web_excluded_string:
REM    specify excluded Web page URL string of the maintenance mode (regular expression).
REM    format separator space and enclosed single quotes.
REM    Null If you do not set.
REM  api_excluded_string:
REM    specify excluded API URL string of the maintenance mode (regular expression).
REM    format separator space and enclosed single quotes.
REM    Null If you do not set.
REM  web_redirect_string:
REM    Specifying the destination redirect url string for web page (regular expression).
REM    format separator space and enclosed single quotes.
REM    Null If you do not set.
REM  api_rewrite_string:
REM    Specifying the destination rewrite url string for API (regular expression).
REM    Null If you do not set.
set web_match_string="^(?!.*api/).*"
set api_match_string="^(?=.*api/).*"
set web_excluded_string='^/oauth2/' '^/Error/' '^/Content' '^/bundles'
set api_excluded_string='/api/Maintenance/Get'
set web_redirect_string="/Error/Maintenance"
set api_rewrite_string="/api/Maintenance/Get"
REM -------------------------------------------------

REM Set Constant Parameter --------------------------
set maintenance_log_path="C:\Resources\Directory\ApplicationLog\MaintenanceModeLog.txt"
set maintenance_excluded_ip=%MAINTENANCEIP% %MAINTENANCEIP2%
set appcmd_file_path="%windir%\system32\inetsrv\appcmd.exe"
REM -------------------------------------------------

echo Start of the script ---------------------------- >> %maintenance_log_path%
echo [START] %date% %time% Script to configuration of maintenance mode. >> %maintenance_log_path%

REM For Debug Information
echo [INFO] maintenance_log_path    : %maintenance_log_path% >> %maintenance_log_path%
echo [INFO] web_match_string        : %web_match_string% >> %maintenance_log_path%
echo [INFO] api_match_string        : %api_match_string% >> %maintenance_log_path%
echo [INFO] web_excluded_string     : %web_excluded_string% >> %maintenance_log_path%
echo [INFO] api_excluded_string     : %api_excluded_string% >> %maintenance_log_path%
echo [INFO] web_redirect_string     : %web_redirect_string% >> %maintenance_log_path%
echo [INFO] api_rewrite_string      : %api_rewrite_string% >> %maintenance_log_path%
echo [INFO] maintenance_excluded_ip : %maintenance_excluded_ip% >> %maintenance_log_path%
echo [INFO] appcmd_file_path        : %appcmd_file_path% >> %maintenance_log_path%
echo;>> %maintenance_log_path%

REM Configuration of maintenance mode
echo [INFO] The start of the configuration of maintenance mode. MODE:"%MAINTENANCEMODE%" >> %maintenance_log_path%
echo [INFO] Start remove of maintenance mode setting of existing. >> %maintenance_log_path%
%appcmd_file_path% list config /section:system.webServer/rewrite/globalRules | findstr name="""Maintenance_Mode_Web"""
IF NOT ERRORLEVEL 1 (
  echo [INFO] Setting of existing Web page >> %maintenance_log_path%
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /-"[name='Maintenance_Mode_Web',stopProcessing='True']" /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto HandleError )
)
%appcmd_file_path% list config /section:system.webServer/rewrite/globalRules | findstr name="""Maintenance_Mode_Api"""
IF NOT ERRORLEVEL 1 (
  echo [INFO] Setting of existing Api >> %maintenance_log_path%
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /-"[name='Maintenance_Mode_Api',stopProcessing='True']" /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto HandleError )
)
echo [INFO] End remove of maintenance mode setting of existing. >> %maintenance_log_path%
echo;>> %maintenance_log_path%

IF "%MAINTENANCEMODE%" == "0" goto MaintenanceModeOff

IF NOT %web_match_string% == "" (
  echo [INFO] Start Maintenance Mode setting for web page. >> %maintenance_log_path%
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Web',stopProcessing='True']" /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto ConfigurationForWebHandleError )
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /[name='Maintenance_Mode_Web',stopProcessing='True'].match.url:%web_match_string% /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto ConfigurationForWebHandleError )
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /[name='Maintenance_Mode_Web',stopProcessing='True'].action.type:"Redirect" /[name='Maintenance_Mode_Web',stopProcessing='True'].action.url:%web_redirect_string% /[name='Maintenance_Mode_Web',stopProcessing='True'].action.redirectType:"Temporary" /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto ConfigurationForWebHandleError )
  FOR %%i IN (%web_excluded_string%) DO (
    echo [INFO] Excluded String:%%i >> %maintenance_log_path%
    %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Web',stopProcessing='True'].conditions.[input='{URL}',pattern=%%i,negate='true']" /commit:apphost >> %maintenance_log_path%
    IF ERRORLEVEL 1 ( goto ConfigurationForWebHandleError )
  )
  IF "%MAINTENANCEMODE%" == "1" (
    echo [INFO] Setting the specified ip address to be excluded from the maintenance mode for web page. >> %maintenance_log_path%
    FOR %%i IN (%maintenance_excluded_ip%) DO (
      echo [INFO] Excluded IP-Address:%%i >> %maintenance_log_path%
      %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Web',stopProcessing='True'].conditions.[input='{REMOTE_ADDR}',pattern=%%i,negate='true']" /commit:apphost >> %maintenance_log_path%
      IF ERRORLEVEL 1 ( goto ConfigurationForWebHandleError )
      %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Web',stopProcessing='True'].conditions.[input='{HTTP_X-FORWARDED-FOR2}',pattern=%%i,negate='true']" /commit:apphost >> %maintenance_log_path%
      IF ERRORLEVEL 1 ( goto ConfigurationForWebHandleError )
    )
  ) ELSE (
    echo [INFO] Setting all addresses should not be excluded from the maintenance mode for web page. >> %maintenance_log_path%
  )
)
echo;>> %maintenance_log_path%

IF NOT %api_match_string% == "" (
  echo [INFO] Start Maintenance Mode setting for api. >> %maintenance_log_path%
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Api',stopProcessing='True']" /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto ConfigurationForApiHandleError )
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /[name='Maintenance_Mode_Api',stopProcessing='True'].match.url:%api_match_string% /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto ConfigurationForApiHandleError )
  %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /[name='Maintenance_Mode_Api',stopProcessing='True'].action.type:"Rewrite" /[name='Maintenance_Mode_Api',stopProcessing='True'].action.url:%api_rewrite_string% /commit:apphost >> %maintenance_log_path%
  IF ERRORLEVEL 1 ( goto ConfigurationForApiHandleError )
  FOR %%i IN (%api_excluded_string%) DO (
    echo [INFO] Excluded String:%%i >> %maintenance_log_path%
    %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Api',stopProcessing='True'].conditions.[input='{URL}',pattern=%%i,negate='true']" /commit:apphost >> %maintenance_log_path%
    IF ERRORLEVEL 1 ( goto ConfigurationForApiHandleError )
  )
  IF "%MAINTENANCEMODE%" == "1" (
    echo [INFO] Setting the specified ip address to be excluded from the maintenance mode for api. >> %maintenance_log_path%
    FOR %%i IN (%maintenance_excluded_ip%) DO (
      echo [INFO] Excluded IP-Address:%%i >> %maintenance_log_path%
      %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Api',stopProcessing='True'].conditions.[input='{REMOTE_ADDR}',pattern=%%i,negate='true']" /commit:apphost >> %maintenance_log_path%
      IF ERRORLEVEL 1 ( goto ConfigurationForApiHandleError )
      %appcmd_file_path% set config -section:system.webServer/rewrite/globalRules /+"[name='Maintenance_Mode_Api',stopProcessing='True'].conditions.[input='{HTTP_X-FORWARDED-FOR2}',pattern=%%i,negate='true']" /commit:apphost >> %maintenance_log_path%
      IF ERRORLEVEL 1 ( goto ConfigurationForApiHandleError )
    )
  ) ELSE (
    echo [INFO] Setting all addresses should not be excluded from the maintenance mode for api. >> %maintenance_log_path%
  )
)
echo [INFO] Successful Configuration of maintenance mode. >> %maintenance_log_path%
echo;>> %maintenance_log_path%

REM End: Script Successful Status
echo [COMPLETED] %date% %time% Script to configuration of maintenance mode. >> %maintenance_log_path%
echo End of the script ------------------------------ >> %maintenance_log_path%
echo;>> %maintenance_log_path%
echo;>> %maintenance_log_path%
exit 0

REM End: Script Successful Status (Maintenance Mode Off)
:MaintenanceModeOff
echo [INFO] Off set the maintenance mode. >> %maintenance_log_path%
echo [COMPLETED] %date% %time% Script to configuration of maintenance mode. >> %maintenance_log_path%
echo End of the script ------------------------------ >> %maintenance_log_path%
echo;>> %maintenance_log_path%
echo;>> %maintenance_log_path%
exit 0

REM End: Configuration Of Maintenance Mode For Web Page Error Status
:ConfigurationForWebHandleError
echo [ERROR] Failed Configuration of maintenance mode for web page. >> %maintenance_log_path%
goto HandleError

REM End: Configuration Of Maintenance Mode For API Error Status
:ConfigurationForApiHandleError
echo [ERROR] Failed Configuration of maintenance mode for api. >> %maintenance_log_path%
goto HandleError

REM End: Script Error Status
:HandleError
echo [FAILED] %date% %time% Script to configuration of maintenance mode. >> %maintenance_log_path%
echo End of the script ------------------------------ >> %maintenance_log_path%
echo;>> %maintenance_log_path%
echo;>> %maintenance_log_path%
exit 1

REM End: Skip this script execution
:SkipScript
exit 0
