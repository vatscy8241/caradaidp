﻿Start /w pkgmgr.exe /iu:IIS-ApplicationInit

if "%EMULATED%"=="true" exit 0

rem create custom-log folder
MD C:\Resources\Directory\IISAdvancedLogging
ICACLS C:\Resources\Directory\IISAdvancedLogging /GRANT Administrators:(CI)(OI)F
ICACLS C:\Resources\Directory\IISAdvancedLogging /GRANT Users:(CI)(OI)F
MD C:\Resources\Directory\ApplicationLog
ICACLS C:\Resources\Directory\ApplicationLog /GRANT Administrators:(CI)(OI)F
ICACLS C:\Resources\Directory\ApplicationLog /GRANT Users:(CI)(OI)F
MD C:\Resources\Directory\ManagementLog
ICACLS C:\Resources\Directory\ManagementLog /GRANT Administrators:(CI)(OI)F
ICACLS C:\Resources\Directory\ManagementLog /GRANT Users:(CI)(OI)F

if "%CONFIG%" == "Release" goto exec

PowerShell Install-WindowsFeature -Name Web-IP-Security
%windir%\system32\inetsrv\AppCmd.exe unlock config -section:system.webServer/security/ipSecurity

if not exist %ROLEROOT%\sitesroot\0\robots.txt (
    ECHO User-Agent: * > %ROLEROOT%\sitesroot\0\robots.txt
    ECHO Disallow: / >> %ROLEROOT%\sitesroot\0\robots.txt
)
exit 0

:exec
UCSAzureStart.exe
