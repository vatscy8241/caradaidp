﻿using System;

namespace MTI.CaradaIdProvider.Web.Models
{
    /// <summary>
    /// MVCとOwinで異なるCookieの型を抽象化するためのモデル。
    /// </summary>
    public class CookieModel
    {
        /// <summary>
        /// Cookie名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 値
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// ドメイン
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// HttpOnly属性
        /// </summary>
        public bool HttpOnly { get; set; }

        /// <summary>
        /// 有効期限
        /// </summary>
        public DateTime Expires { get; set; }

        /// <summary>
        /// Secure属性
        /// </summary>
        public bool Secure { get; set; }

        /// <summary>
        /// Path
        /// </summary>
        public string Path { get; set; }
    }
}