﻿namespace MTI.CaradaIdProvider.Web.Models
{
    /// <summary>
    /// CARADA IDをブラウジングしている端末の状態種別を表します。
    /// </summary>
    public enum PlatformKind
    {
        /// <summary>
        /// PCやスマートフォンのブラウザアプリケーションでブラウジングしています。
        /// </summary>
        Web = 0,
        /// <summary>
        /// CARADA IDアプリ内のWebViewでブラウジングしています。
        /// </summary>
        App = 1
    }
}