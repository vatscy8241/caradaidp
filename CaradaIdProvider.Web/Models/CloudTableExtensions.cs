﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Microsoft.WindowsAzure.Storage.Table;
using MTI.CaradaIdProvider.Web.Properties;
using System;

namespace MTI.CaradaIdProvider.Web.Models
{
    /// <summary>
    /// Azure Table Storageへの操作ユーティリティ
    /// ・単一Entityの挿入、更新、削除、検索
    /// ・条件より複数のEntityの検索
    /// ・リトライ処理（自動）
    /// </summary>
    public static class CloudTableExtensions
    {
        /// <summary>
        /// Settingから取得したインターバルでリトライするポリシー。
        /// </summary>
        private static RetryPolicy retryPolicy = new RetryPolicy<StorageTransientErrorDetectionStrategy>(
            new FixedInterval(Settings.Default.TableStorageErrorRetryCount,
                TimeSpan.FromSeconds(Settings.Default.TableStorageErrorRetryIntervalSeconds)));

        /// <summary>
        /// テーブルに行を挿入する。
        /// </summary>
        /// <param name="cloudTable">テーブル</param>
        /// <param name="entity">Entity</param>
        /// <returns>result of Table Operation</returns>
        public static TableResult InsertEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            var insertOperation = TableOperation.Insert(entity);
            return ExecuteCloudTableWithRetry(cloudTable, insertOperation);
        }

        /// <summary>
        /// テーブルから行を削除する。
        /// </summary>
        /// <param name="cloudTable">テーブル</param>
        /// <param name="entity">Entity</param>
        /// <returns>result of Table Operations</returns>
        public static TableResult DeleteEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            var deleteOperation = TableOperation.Delete(entity);
            return ExecuteCloudTableWithRetry(cloudTable, deleteOperation);
        }

        /// <summary>
        /// テーブルの行を取得する。
        /// </summary>
        /// <typeparam name="T">Entityのタイプ</typeparam>
        /// <param name="cloudTable">テーブル</param>
        /// <param name="partitionKey">パーティションキー</param>
        /// <param name="rowKey">行キー</param>
        /// <returns>Entity</returns>
        public static T RetrieveEntity<T>(this CloudTable cloudTable, string partitionKey, string rowKey) where T : ITableEntity
        {
            var retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
            var retrievedResult = ExecuteCloudTableWithRetry(cloudTable, retrieveOperation);
            return (T)retrievedResult.Result;
        }

        /// <summary>
        /// テーブルの行を更新する。カラム単位の更新不可。
        /// </summary>
        /// <param name="cloudTable">テーブル</param>
        /// <param name="entity">Entity</param>
        /// <returns>result of Table Operations</returns>
        public static TableResult ReplaceEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            var updateOperation = TableOperation.Replace(entity);
            return ExecuteCloudTableWithRetry(cloudTable, updateOperation);
        }

        /// <summary>
        /// キーに該当するテーブルの行が存在すれば更新、なければ挿入する。
        /// </summary>
        /// <param name="cloudTable">テーブル</param>
        /// <param name="entity">Entity</param>
        /// <returns>result of Table Operations</returns>
        public static TableResult InsertOrReplaceEntity(this CloudTable cloudTable, ITableEntity entity)
        {
            var insertOperation = TableOperation.InsertOrReplace(entity);
            return ExecuteCloudTableWithRetry(cloudTable, insertOperation);
        }

        /// <summary>
        /// 指定のオペレーションを実行する。失敗した場合、Settingsに定義したリトライを行う。
        /// </summary>
        /// <param name="cloudTable">テーブル</param>
        /// <param name="tableOperation">オペレーション</param>
        /// <returns>result of Table Operations</returns>
        private static TableResult ExecuteCloudTableWithRetry(CloudTable cloudTable, TableOperation tableOperation)
        {
            TableResult tableResult = null;
            retryPolicy.ExecuteAction(
                        () =>
                        {
                            tableResult = cloudTable.Execute(tableOperation);
                        });
            return tableResult;
        }
    }
}
