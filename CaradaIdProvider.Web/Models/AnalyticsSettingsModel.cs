﻿using Microsoft.Azure;

namespace MTI.CaradaIdProvider.Web.Models
{
    /// <summary>
    /// トラッキング設定のモデルです。
    /// </summary>
    public class AnalyticsSettingsModel
    {
        /// <summary>
        /// Google Analyticsの設定です。
        /// </summary>
        public GoogleAnalyticsModel Google { get; set; } = new GoogleAnalyticsModel
        {
            GoogleAnalyticsId = CloudConfigurationManager.GetSetting("GoogleAnalyticsId"),
            Dimension1 = "未ログイン",
            Domain = "auto"
        };

        /// <summary>
        /// User Insightの設定です。
        /// </summary>
        public UserInsightModel UserInsight { get; set; } = new UserInsightModel
        {
            Id = CloudConfigurationManager.GetSetting("UserInsightId")
        };

        /// <summary>
        /// Google Analytics設定のモデルです。
        /// </summary>
        public class GoogleAnalyticsModel
        {
            /// <summary>
            /// Google Analytics Id
            /// </summary>
            public string GoogleAnalyticsId { get; set; }

            /// <summary>
            /// Dimension1
            /// </summary>
            public string Dimension1 { get; set; }

            /// <summary>
            /// Dimension4
            /// </summary>
            public string Dimension4 { get; set; }

            /// <summary>
            /// Dimension6
            /// </summary>
            public string Dimension6 { get; set; }

            /// <summary>
            /// Dimension21
            /// </summary>
            public string Dimension21 { get; set; }

            /// <summary>
            /// Domain
            /// </summary>
            public string Domain { get; set; }
        }

        /// <summary>
        /// User Insight設定のモデルです。
        /// </summary>
        public class UserInsightModel
        {
            /// <summary>
            /// User Insight Id
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// Domain
            /// </summary>
            public string Domain { get; set; } = string.Empty;
        }
    }
}