﻿using MTI.CaradaIdProvider.Web.Resource;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// 利用開始前ユーザー登録モック画面のモデル
    /// </summary>
    public class NewRegistMockViewModel
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MaxLength")]
        [RegularExpression(@"^(?=[0-9a-z])([-_.]?[a-z0-9])*[-_.]?$", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "CaradaIdChar")]
        [DataType(DataType.Text)]
        [Display(Name = "CARADA ID")]
        public string CaradaId { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MinLength(8, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MinLength")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MaxLength")]
        [RegularExpression(@"^(?=.*[0-9])(?=.*[a-zA-Z])[0-9a-zA-Z!-/:-@\[-\`\{-\~]{1,}$", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "PasswordFormatError")]
        [DataType(DataType.Password)]
        [Display(Name = "パスワード")]
        public string Password { get; set; }

        /// <summary>
        /// 処理の区分(1:新規登録、2:削除して新規登録)
        /// </summary>
        public int ProcessId { get; set; }
    }
}