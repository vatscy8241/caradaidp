﻿using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// 秘密の質問登録画面のモデル
    /// </summary>
    [Serializable]
    public class SecurityQuestionRegistViewModel
    {
        /// <summary>
        /// 秘密の質問選択ID
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "SecurityQuestionError")]
        [RegularExpression(@"^[0-9]+$", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "NumberError")]
        [Display(Name = "秘密の質問")]
        public int? SecurityQuestionSelectId { get; set; }

        /// <summary>
        /// 秘密の質問リスト
        /// </summary>
        public List<SelectListItem> SecurityQuestion { get; set; }

        /// <summary>
        /// 答え
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MaxLength(50, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "SecurityQuestionAnswerChar")]
        [SecurityAnswer(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "SecurityQuestionAnswerChar")]
        [Display(Name = "答え")]
        public string Answer { get; set; }
    }
}