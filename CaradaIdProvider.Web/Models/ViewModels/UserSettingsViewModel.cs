﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// アカウント管理画面のモデル
    /// </summary>
    public class UserSettingsViewModel
    {
        /// <summary>
        /// CARADA ID
        /// 表示用。
        /// </summary>
        [Display(Name = "CARADA ID")]
        public string CaradaId { get; set; }

        /// <summary>
        /// メールアドレス
        /// 表示用。
        /// </summary>
        [Display(Name = "メールアドレス")]
        public string Email { get; set; }

        /// <summary>
        /// 秘密の質問
        /// 表示用。
        /// </summary>
        [Display(Name = "秘密の質問")]
        public string SecurityQuestion { get; set; }
    }
}