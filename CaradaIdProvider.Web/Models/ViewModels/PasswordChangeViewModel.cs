﻿using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// パスワード変更画面のモデル
    /// </summary>
    [Serializable]
    public class PasswordChangeViewModel
    {
        /// <summary>
        /// 現在のパスワード
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "現在のパスワード")]
        public string CurrentPassword { get; set; }

        /// <summary>
        /// 新パスワード
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MinLength(8, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MinLength")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MaxLength")]
        [PasswordCombine(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "PasswordCombine", ErrorMessage = null)]
        [RegularExpression(@"^[0-9a-zA-Z!-/:-@\[-\`\{-\~]{1,}$", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "ChangePasswordChar")]
        [DataType(DataType.Password)]
        [Display(Name = "新しいパスワード")]
        public string NewPassword { get; set; }

        /// <summary>
        /// 新パスワード（確認用）
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Compare")]
        [DataType(DataType.Password)]
        [Display(Name = "新しいパスワード（確認用）")]
        public string NewPasswordConfirm { get; set; }
    }
}