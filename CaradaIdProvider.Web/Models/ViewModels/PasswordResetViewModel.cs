﻿using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// パスワード再設定入力画面のモデル
    /// </summary>
    [Serializable]
    public class PasswordResetViewModel
    {
        /// <summary>
        /// 認証コード
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "認証コード")]
        public string VerifyCode { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MinLength(8, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MinLength")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MaxLength")]
        [PasswordCombine(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "PasswordCombine", ErrorMessage = null)]
        [RegularExpression(@"^[0-9a-zA-Z!-/:-@\[-\`\{-\~]{1,}$", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "PasswordChar")]
        [DataType(DataType.Password)]
        [Display(Name = "再設定パスワード")]
        public string ResetPassword { get; set; }

        /// <summary>
        /// 再設定パスワード（確認用）
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [System.ComponentModel.DataAnnotations.Compare("ResetPassword", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Compare")]
        [DataType(DataType.Password)]
        [Display(Name = "再設定パスワード（確認用）")]
        public string ConfirmResetPassword { get; set; }
    }
}