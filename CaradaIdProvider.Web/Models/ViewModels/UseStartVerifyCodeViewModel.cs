﻿using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// 利用開始認証画面のモデル
    /// </summary>
    [Serializable]
    public class UseStartVerifyCodeViewModel
    {
        /// <summary>
        /// 認証コード
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "認証コード")]
        public string VerifyCode { get; set; }

    }
}