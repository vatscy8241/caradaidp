﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations
{
    /// <summary>
    /// Requestパラメータとプロパティ名が異なる場合にバインドする属性。
    /// 元のプロパティ名でもバインドする場合は複数のエイリアスとして、プロパティ名どおりのエイリアスを付与すること。
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class BindingNameAttribute : Attribute
    {
        /// <summary>
        /// パラメータ名のエイリアス
        /// </summary>
        public string Name { get; set; }
    }
}