﻿using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations
{
    /// <summary>
    /// 半角英字と半角数字が両方使われていない場合
    /// </summary>
    /// <remarks>System.ComponentModel.DataAnnotations.RegularExpressionAttribute を拡張したもの。</remarks>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class PasswordCombineAttribute : DataTypeAttribute
    {
        private static Regex _regex = new Regex(@"^(?=.*?[a-zA-Z])(?=.*?\d)(.*){1,}$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PasswordCombineAttribute()
        : base(DataType.Password)
        {
            ErrorMessage = string.Format(ErrorMessages.PasswordCombine, "Password");
        }

        /// <summary>
        /// 妥当性判定
        /// </summary>
        /// <param name="value">評価対象文字列</param>
        /// <returns>True: パスワード形式として妥当、False:パスワード形式ではない</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var valueAsString = value as string;
            return valueAsString != null && _regex.Match(valueAsString).Length > 0;
        }
    }
}
