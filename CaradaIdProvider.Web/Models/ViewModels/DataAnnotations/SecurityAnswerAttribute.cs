﻿using MTI.CaradaIdProvider.Web.Properties;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations
{
    /// <summary>
    /// 秘密の質問の答えのAttributeです。
    /// </summary>
    public class SecurityAnswerAttribute : ValidationAttribute
    {
        /// <summary>
        /// 許容する文字セットかどうかを確認します。
        /// </summary>
        /// <param name="value">パラメータvalue</param>
        /// <returns>判定結果</returns>
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var str = value as string;
            return str != null && str.All(a => Settings.Default.AllowSecurityAnswer.Contains(a));
        }
    }
}