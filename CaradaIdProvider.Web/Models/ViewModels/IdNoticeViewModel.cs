﻿using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// ID連絡画面のモデル
    /// </summary>
    [Serializable]
    public class IdNoticeViewModel
    {
        /// <summary>
        /// メールアドレス
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MaxLength(255, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MaxLength")]
        [EmailAddressJP(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "EmailFormat", ErrorMessage = null)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "メールアドレス")]
        public string Email { get; set; }

    }
}