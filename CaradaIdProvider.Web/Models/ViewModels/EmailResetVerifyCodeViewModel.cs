﻿using MTI.CaradaIdProvider.Web.Resource;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// メールアドレス再設定認証画面のモデル
    /// </summary>
    public class EmailResetVerifyCodeViewModel
    {
        /// <summary>
        /// 認証コード
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "認証コード")]
        public string VerifyCode { get; set; }
    }
}