﻿using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// ログイン画面のモデル
    /// </summary>
    [Serializable]
    public class LoginViewModel
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [Display(Name = "CARADA ID")]
        public string CaradaId { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "パスワード")]
        public string Password { get; set; }

        /// <summary>
        /// ログイン状態を保持するチェックボックス
        /// </summary>
        [Display(Name = "ログイン状態を保持する")]
        public bool IsKeepLogin { get; set; }
    }
}