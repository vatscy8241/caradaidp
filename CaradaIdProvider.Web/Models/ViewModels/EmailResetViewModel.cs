﻿using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// メールアドレス再設定画面のモデル
    /// </summary>
    [Serializable]
    public class EmailResetViewModel
    {
        /// <summary>
        /// CARADA ID
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [Display(Name = "CARADA ID")]
        public string CaradaId { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "パスワード")]
        public string Password { get; set; }

        /// <summary>
        /// 秘密の質問選択ID
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [RegularExpression(@"^[0-9]+$", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "NumberError")]
        [Display(Name = "秘密の質問")]
        public int? SecurityQuestionSelectId { get; set; }

        /// <summary>
        /// 答え
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MaxLength(50, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "SecurityQuestionAnswerChar")]
        [SecurityAnswer(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "SecurityQuestionAnswerChar")]
        [Display(Name = "答え")]
        public string Answer { get; set; }

        /// <summary>
        /// 新しいメールアドレス
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MaxLength(255, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MaxLength")]
        [EmailAddressJP(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "EmailFormat", ErrorMessage = null)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "新しいメールアドレス")]
        public string Email { get; set; }

        /// <summary>
        /// SelectListItemのキーと値の辞書
        /// </summary>
        /// <remarks>
        /// SelectListItemがSerializableでないため、TempDataへの保持用として定義する.
        /// 単なるGetter,SetterなのでUTなし.
        /// </remarks>
        public Dictionary<int, string> SerializeItems { get; set; }
    }
}