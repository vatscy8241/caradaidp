﻿using MTI.CaradaIdProvider.Web.Resource;
using System.ComponentModel.DataAnnotations;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// メールアドレス変更認証画面のモデル
    /// </summary>
    public class EmailChangeVerifyCodeViewModel
    {
        /// <summary>
        /// 認証コード
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "認証コード")]
        public string VerifyCode { get; set; }
    }
}