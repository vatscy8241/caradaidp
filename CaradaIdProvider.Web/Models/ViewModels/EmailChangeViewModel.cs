﻿using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Models.ViewModels
{
    /// <summary>
    /// メールアドレス変更入力画面のモデル
    /// </summary>
    [Serializable]
    public class EmailChangeViewModel
    {
        /// <summary>
        /// 現在のメールアドレス
        /// 表示用。
        /// </summary>
        [Display(Name = "現在のメールアドレス")]
        public string Email { get; set; }

        /// <summary>
        /// 新しいメールアドレス
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [MaxLength(255, ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "MaxLength")]
        [EmailAddressJP(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "EmailFormat", ErrorMessage = null)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "新しいメールアドレス")]
        public string NewEmail { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [AllowHtml]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "Required")]
        [DataType(DataType.Password)]
        [Display(Name = "パスワード")]
        public string Password { get; set; }
    }
}