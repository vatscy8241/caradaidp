﻿using Microsoft.Owin.Infrastructure;
using System;
using System.Text;

namespace MTI.CaradaIdProvider.Web.Models
{
    /// <summary>
    /// ログイン結果に関する列挙型
    /// </summary>
    public enum LoginResult
    {
        /// <summary>
        /// 未ログイン
        /// </summary>
        Unlogin,
        /// <summary>
        /// 成功
        /// </summary>
        Success,
        /// <summary>
        /// キャンセル
        /// </summary>
        Cancel,
        /// <summary>
        /// 失敗
        /// </summary>
        Error,
        /// <summary>
        /// メンテナンス中
        /// </summary>
        Maintenance
    }

    /// <summary>
    /// AuthenticationRequestのEndpoint⇔画面間の引継ぎセッション。
    /// </summary>
    [Serializable]
    public class AuthorizeEndpointSession
    {
        /// <summary>
        /// client_id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// redirect_uri
        /// </summary>
        public string RedirectUri { get; set; }

        /// <summary>
        /// scope
        /// </summary>
        public string Scope { get; set; }

        /// <summary>
        /// response_type
        /// </summary>
        public string ResponseType { get; set; }

        /// <summary>
        /// state
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// ログイン結果
        /// </summary>
        public LoginResult LoginResult { get; set; }

        /// <summary>
        /// 戻り先のEndpointUrl
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// 文字コード
        /// </summary>
        private static Encoding ENC = Encoding.UTF8;

        /// <summary>
        /// パラメータ付戻り先URLを生成
        /// </summary>
        /// <returns></returns>
        public string GetParameterizedReturnUrl()
        {
            var location = WebUtilities.AddQueryString(ReturnUrl, "client_id", ClientId);
            location = WebUtilities.AddQueryString(location, "scope", Scope);
            location = WebUtilities.AddQueryString(location, "response_type", ResponseType);
            location = WebUtilities.AddQueryString(location, "redirect_uri", RedirectUri);
            if (State != null)
            {
                location = WebUtilities.AddQueryString(location, "state", State);
            }
            return location;
        }
    }
}