﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace MTI.CaradaIdProvider.Web.Models
{
    /// <summary>
    /// Azure Storageの接続設定
    /// </summary>
    public class AuthorizationStorageContext
    {
        /// <summary>
        /// 接続文字列
        /// </summary>
        private static readonly string CONNECTION_STRING = CloudConfigurationManager.GetSetting("AuthorizationStorageConnectionString");

        /// <summary>
        /// アカウント
        /// </summary>
        public static CloudStorageAccount Account { get { return CloudStorageAccount.Parse(CONNECTION_STRING); } }

        /// <summary>
        /// TwoFactorLoginテーブル
        /// </summary>
        public static CloudTable TwoFactorLogin { get { return GetCloudTable("TwoFactorLogin"); } }

        /// <summary>
        /// テーブルの初期化。
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <returns>テーブル</returns>
        private static CloudTable GetCloudTable(string tableName)
        {
            var tableClient = Account.CreateCloudTableClient();
            return tableClient.GetTableReference(tableName);
        }

        /// <summary>
        /// Azure Table を作成します。
        /// </summary>
        public static void CreateCloudTables()
        {
            TwoFactorLogin.CreateIfNotExists();
        }
    }
}