﻿using Microsoft.WindowsAzure.Storage.Table;

namespace MTI.CaradaIdProvider.Web.Models.AuthorizationStorageEntities
{
    /// <summary>
    /// 二段階認証情報テーブルのEntity。
    /// PartitionKeyは二段階認証Cookieの値(UUID)、RowKeyは"-"(ハイフン)固定となる。
    /// </summary>
    public class TwoFactorLoginEntity : TableEntity
    {
        /// <summary>
        /// ユーザーID
        /// </summary>
        public string UserId { get; set; }
    }
}