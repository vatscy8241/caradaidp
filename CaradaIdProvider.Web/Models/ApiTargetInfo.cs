﻿using MTI.CaradaIdProvider.Web.Properties;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace MTI.CaradaIdProvider.Web.Models
{
    /// <summary>
    /// API実行対象の情報クラスです。
    /// </summary>
    public class ApiTarget
    {
        /// <summary>
        /// 実行対象名
        /// </summary>
        public string TargetName { get; set; }
        /// <summary>
        /// JWTのissとなる発行者名
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// この実行対象のJWT署名キー
        /// </summary>
        public string SecretKey { get; set; }
    }

    /// <summary>
    /// API実行対象Jsonの処理クラスです。
    /// </summary>
    public class ApiTargetInfo
    {
        private Dictionary<string, ApiTarget> targets = new Dictionary<string, ApiTarget>();

        /// <summary>
        /// API実行対象のjson設定を読み込みます。
        /// </summary>
        /// <param name="json">設定json</param>
        public void Load(string json)
        {
            var array = JArray.Parse(json);

            targets.Clear();

            foreach (JToken token in array)
            {
                var target = new ApiTarget
                {
                    TargetName = (string)token[Settings.Default.ApiTargetInfoTargetName],
                    Issuer = (string)token[Settings.Default.ApiTargetInfoTargetIssuer],
                    SecretKey = (string)token[Settings.Default.ApiTargetInfoSecretKey]
                };

                targets.Add(target.TargetName.ToUpper(), target);
            }
        }

        /// <summary>
        /// 対象名からAPI実行対象情報を取得します。大文字小文字は区別しません。
        /// 見つからない場合はnullを返します。
        /// </summary>
        /// <param name="targetName">API実行対象名</param>
        /// <returns>API実行対象情報</returns>
        public ApiTarget GetApiTarget(string targetName)
        {
            if (string.IsNullOrEmpty(targetName))
            {
                return null;
            }

            if (!targets.ContainsKey(targetName.ToUpper()))
            {
                return null;
            }

            return targets[targetName.ToUpper()];
        }
    }
}