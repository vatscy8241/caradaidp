﻿namespace MTI.CaradaIdProvider.Web.IdentityManagers
{
    /// <summary>
    /// ログイン検証ステータス
    /// </summary>
    public enum SignInStatusEx
    {
        Success,                // 成功
        UseStarting,            // 利用開始
        LockedOut,              // ロックアウト
        RequiresVerification,   // 2段階認証
        Failure                 // 失敗
    }
}