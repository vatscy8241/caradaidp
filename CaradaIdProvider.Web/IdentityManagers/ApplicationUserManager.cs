﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Azure;
using Microsoft.Owin;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.EmailServices;
using MTI.CaradaIdProvider.Web.Properties;
using MTI.CaradaIdProvider.Web.Resource;
using System;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.IdentityManagers
{
    /// <summary>
    /// このアプリケーションで使用されるアプリケーション ユーザー マネージャーを設定します。UserManager は ASP.NET Identity の中で定義されており、このアプリケーションで使用されます。
    /// </summary>
    public class ApplicationUserManager : UserManager<CaradaIdUser>
    {
        public ApplicationUserManager(IUserStore<CaradaIdUser> store)
            : base(store)
        {
        }

        /// <summary>
        /// 認証コードの有効時間(分)
        /// すべての認証コードで統一の設定とする。
        /// </summary>
        public const int VERIFY_CODE_EXPIRE_MINUTES = 5;

        /// <summary>
        /// 認証コードの失敗とみなす回数
        /// この回数に達すると認証コードが無効扱いになる。
        /// すべての認証コードで統一の設定とする。
        /// </summary>
        public const int VERIFY_CODE_FAILED_COUNT = 5;

        /// <summary>
        /// アカウントのロックアウト時間(分)
        /// </summary>
        public const int ACCOUNT_LOCKOUT_MINUTES = 30;

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore<CaradaIdUser>(context.Get<CaradaIdPDbContext>()));

            // -------------- バリデータ関係 -------------
            manager.UserValidator = new UserValidator<CaradaIdUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false, // ユーザー名は英数字のみか
            };

            // -------------- メールサービス関係 -------------
            // 二段階認証用
            manager.RegisterTwoFactorProvider(Settings.Default.TwoFactorLoginSendCodeProvider, new ApplicationEmailTokenProvider<CaradaIdUser>
            {
                Subject = Mail.TwoFactorLoginSendCodeSubject,
                BodyFormat = Mail.TwoFactorLoginSendCodeBody.Replace(":SUPPORT:", Mail.Support)
            });

            // 利用開始認証用
            manager.RegisterTwoFactorProvider(Settings.Default.UseStartSendCodeProvider, new ApplicationEmailTokenProvider<CaradaIdUser>
            {
                Subject = Mail.UseStartSendCodeSubject,
                BodyFormat = Mail.UseStartSendCodeBody.Replace(":SUPPORT:", Mail.Support)
            });
            // パスワード再設定用
            manager.RegisterTwoFactorProvider(Settings.Default.PasswordResetSendCodeProvider, new ApplicationEmailTokenProvider<CaradaIdUser>
            {
                Subject = Mail.PasswordResetSendCodeSubject,
                BodyFormat = Mail.PasswordResetSendCodeBody.Replace(":SUPPORT:", Mail.Support)
            });
            // メールアドレス再設定用
            manager.RegisterTwoFactorProvider(Settings.Default.EmailResetSendCodeProvider, new ApplicationEmailTokenProvider<CaradaIdUser>
            {
                // メールアドレス再設定はこの機能を用いての送信を行わないが、ほかにあわせてEmailTokenProviderを登録する
                Subject = Mail.EmailResetSendCodeSubject,
                BodyFormat = Mail.EmailResetSendCodeBody.Replace(":SUPPORT", Mail.Support)
            });
            // メールアドレス変更用
            manager.RegisterTwoFactorProvider(Settings.Default.EmailChangeSendCodeProvider, new ApplicationEmailTokenProvider<CaradaIdUser>
            {
                // メールアドレス再設定はこの機能を用いての送信を行わないが、ほかにあわせてEmailTokenProviderを登録する
                Subject = Mail.EmailChangeSendCodeSubject,
                BodyFormat = Mail.EmailChangeSendCodeBody.Replace(":SUPPORT", Mail.Support)
            });
            // メールサービス
            manager.EmailService = Activator.CreateInstance(Type.GetType(CloudConfigurationManager.GetSetting("EmailService"))) as AsyncEmailService;

            // ユーザー ロックアウトの既定値を設定します。
            manager.UserLockoutEnabledByDefault = false; // 新規作成時にロックアウトするかどうか
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(ACCOUNT_LOCKOUT_MINUTES); // ロックアウト時間
            manager.MaxFailedAccessAttemptsBeforeLockout = VERIFY_CODE_FAILED_COUNT; // ロックアウトされる失敗回数

            // フレームワークのロックアウト機構を使う。
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<CaradaIdUser>(
                        dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }

        /// <summary>
        /// パスワードの検証をする。
        /// </summary>
        /// <param name="user">ユーザー情報</param>
        /// <param name="password">パスワード</param>
        /// <returns>結果ステータスのenum</returns>
        public virtual async Task<PasswordValidationStatus> ValidatePassword(CaradaIdUser user, string password)
        {
            // ロックアウト中か確認する
            if (await IsLockedOutAsync(user.Id))
            {
                return PasswordValidationStatus.AlreadyLockedOut;
            }

            // パスワードの妥当性検証
            if (!await CheckPasswordAsync(user, password))
            {
                // パスワード誤り 失敗回数のカウントアップをする
                await AccessFailedAsync(user.Id);
                if (await IsLockedOutAsync(user.Id))
                {
                    // ロックされる失敗回数に到達した時点でロック中とする
                    return PasswordValidationStatus.LockedOut;
                }
                return PasswordValidationStatus.Failure;
            }
            return PasswordValidationStatus.Success;
        }

        /// <summary>
        /// 認証コードが無効かどうかを判断する。
        /// 認証コードを発行していないときは有効期限がnullであり、そのときに呼び出した場合は不正としてtrueを返却する。
        /// </summary>
        /// <param name="user">CaradaIdUser</param>
        /// <returns>認証コードが有効期限切れか失敗回数が上限に達しているか有効期限がnull：true。そうでない：false</returns>
        public virtual bool IsInvalidVerifyCode(CaradaIdUser user)
        {
            // 認証コードが発行されていなければ不正
            if (user.AuthCodeExpireDateUtc == null)
            {
                return true;
            }
            // 有効期限よりも後ならば不正
            if (DateTime.UtcNow.CompareTo(user.AuthCodeExpireDateUtc) > 0)
            {
                return true;
            }
            // 指定回数以上間違えていれば不正
            if (user.AuthCodeFailedCount >= VERIFY_CODE_FAILED_COUNT)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 利用開始時の認証コードの失敗回数をカウントアップし、認証コードが無効になったかどうかをチェックする
        /// </summary>
        /// <param name="user">CaradaIdUser</param>
        /// <returns>認証コード無効：true、認証コード有効：false</returns>
        public virtual async Task<bool> IsVerifyCodeInvalidNow(CaradaIdUser user)
        {
            // 認証失敗回数をカウントアップ
            user.AuthCodeFailedCount++;
            await UpdateAsync(user);

            return IsInvalidVerifyCode(user);
        }
    }

    /// <summary>
    /// パスワード検証ステータス
    /// </summary>
    public enum PasswordValidationStatus
    {
        Success = 0, // 成功
        Failure = 1, // 失敗
        LockedOut = 2, //ロックアウトされた
        AlreadyLockedOut = 3, //既にロックアウト中
    }
}