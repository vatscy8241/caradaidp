﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Models.AuthorizationStorageEntities;
using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace MTI.CaradaIdProvider.Web.IdentityManagers
{
    /// <summary>
    /// このアプリケーションで使用されるアプリケーション サインイン マネージャーを構成します。
    /// </summary>
    public class ApplicationSignInManager : SignInManager<CaradaIdUser, string>
    {
        /// <summary>
        /// ログ
        /// </summary>
        protected static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="userManager">userManager</param>
        /// <param name="authenticationManager">authenticationManager</param>
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }
        /// <summary>
        /// UserIdentityを生成する。
        /// </summary>
        /// <param name="user">ユーザー情報</param>
        /// <returns>UserIdentity</returns>
        public override Task<ClaimsIdentity> CreateUserIdentityAsync(CaradaIdUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }
        /// <summary>
        /// サインインマネージャを生成する。業務部品からの呼び出しは不要。
        /// </summary>
        /// <param name="options">options</param>
        /// <param name="context">context</param>
        /// <returns>サインインマネージャ</returns>
        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        /// <summary>
        /// UserManagerをApplicationUserManagerとして使用したいので上書きしている。
        /// </summary>
        public new ApplicationUserManager UserManager
        {
            get
            {
                return (ApplicationUserManager)base.UserManager;
            }
        }

        /// <summary>
        /// カスタマイズしたSignInメソッド。
        /// </summary>
        /// <param name="caradaId">CARADA ID</param>
        /// <param name="password">パスワード</param>
        /// <param name="isKeepLogin">ログイン永続化フラグ</param>
        /// <param name="isRequiredTwoFactorLogin">二段階認証のチェックが必要かどうか/param>
        /// <returns>結果のenum</returns>
        public virtual async Task<SignInStatusEx> SignInAsync(string caradaId, string password, bool isKeepLogin, bool isRequiredTwoFactorLogin)
        {
            var user = await UserManager.FindByNameAsync(caradaId);

            // ユーザーがない
            if (user == null)
            {
                return SignInStatusEx.Failure;
            }
            // パスワードの妥当性検証
            var passwordValidationResult = await UserManager.ValidatePassword(user, password);
            switch (passwordValidationResult)
            {
                case PasswordValidationStatus.Success:
                    // 成功なので後続処理を行う
                    break;
                case PasswordValidationStatus.LockedOut:
                    LOG.Error("アカウントロックアウト開始");
                    return SignInStatusEx.LockedOut;
                case PasswordValidationStatus.AlreadyLockedOut:
                    LOG.Error("アカウントロックアウト中");
                    return SignInStatusEx.LockedOut;
                case PasswordValidationStatus.Failure:
                default:
                    LOG.Warn("ログイン失敗");
                    return SignInStatusEx.Failure;
            }

            // メールアドレス登録の確認
            if (!user.EmailConfirmed)
            {
                return SignInStatusEx.UseStarting;
            }

            // 2要素認証が必要かチェックする（必要かどうかはユーザー情報に明示的にセットする）
            if (isRequiredTwoFactorLogin && await UserManager.GetTwoFactorEnabledAsync(user.Id))
            {
                var cookie = HttpContext.Current.Request.Cookies[Properties.Settings.Default.TwoFactorLoginCookieName];
                if (cookie == null)
                {
                    return SignInStatusEx.RequiresVerification;
                }
                var entity = AuthorizationStorageContext.TwoFactorLogin
                    .RetrieveEntity<TwoFactorLoginEntity>(cookie.Value, Properties.Settings.Default.RowKeyValue);
                if (entity == null || entity.UserId != user.Id)
                {
                    return SignInStatusEx.RequiresVerification;
                }
            }

            // 失敗回数を初期化
            await UserManager.ResetAccessFailedCountAsync(user.Id);
            // サインアウトしてサインイン
            await SignOut();
            // HttpContextのユーザー情報を紐付ける
            HttpContext.Current.User = new GenericPrincipal(await CreateUserIdentityAsync(user),
                new string[] { Properties.Settings.Default.UserRole });
            await base.SignInAsync(user, isKeepLogin, false);
            LOG.Info("ログイン成功");
            return SignInStatusEx.Success;
        }

        /// <summary>
        /// SignOutメソッド。SignInメソッドから必ず呼び出すため、業務ロジックでサインイン前に呼び出す必要なし。
        /// </summary>
        public virtual async Task SignOut()
        {
            LOG.Info("ログアウト");
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            // 同じCookie値でのログインを防ぐためSecurityStampを更新する。
            var userId = HttpContext.Current.User.Identity.GetUserId();
            if (userId != null)
            {
                await UserManager.UpdateSecurityStampAsync(userId);
            }

            // HTTPContextのユーザー情報の紐付けを削除する
            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
        }
    }
}