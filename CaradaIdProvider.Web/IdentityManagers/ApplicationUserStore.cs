﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.IdentityManagers
{
    /// <summary>
    /// このアプリケーション用のUserStoreです。
    /// そのままでは検索条件に不備がある点を置き換えています。
    /// </summary>
    /// <typeparam name="TUser">IdentityUser</typeparam>
    public class ApplicationUserStore<TUser> : UserStore<TUser> where TUser : IdentityUser
    {
        /// <summary>
        /// コンストラクタ。
        /// </summary>
        /// <param name="context">IdentityDbContext</param>
        public ApplicationUserStore(DbContext context) : base(context)
        {
        }

        /// <summary>
        /// ユーザー名でユーザーを検索します。
        /// baseのUserStoreではToUpper()しており、非インデックス検索になってしまうため、明示的に等値検索します。
        /// CARADA IDのため大文字小文字は区別する。
        /// </summary>
        /// <param name="userName">ユーザー名</param>
        /// <returns>ユーザー情報</returns>
        public override Task<TUser> FindByNameAsync(string userName)
        {
            var user = GetUserAggregateAsync(u => u.UserName == userName);
            var result = user.Result;

            if (result != null && !result.UserName.Equals(userName))
            {
                return Task.FromResult<TUser>(null);
            }
            return user;
        }

        /// <summary>
        /// Emailでユーザーを検索します。
        /// baseのUserStoreではToUpper()しており、非インデックス検索になってしまうため、明示的に等値検索します。
        /// 大文字小文字を区別するかしないかはDBMSの設定によるものとします。
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>ユーザー情報</returns>
        public override Task<TUser> FindByEmailAsync(string email)
        {
            return GetUserAggregateAsync(u => u.Email == email);
        }
    }
}