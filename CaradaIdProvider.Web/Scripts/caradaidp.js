﻿; $(function () {
    // iPhone / iPad safari用
    $(window).on("pageshow", function (e) {
        if (e.originalEvent.persisted) {
            $.unblockUI();
        }
    });

    var blockOption = {
        message: null,
        overlayCSS: {
            opacity: 0.0
        }
    };

    $("input:submit,input:button,button:submit").click(function () {
        $.blockUI(blockOption);
    });

    $("a").click(function () {
        $.blockUI(blockOption);
        if (this.target !== "" || this.href.lastIndexOf("mailto:", 0) === 0) {
            setTimeout($.unblockUI, 1000);
        }
    });
});

// unblock
$.validator.unobtrusive.options = {
    errorPlacement: function (error, element) {
        if (error.text().length != 0) {
            $.unblockUI();
        }
    }
};