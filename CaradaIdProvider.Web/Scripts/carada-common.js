$(function(){
  $('.match-height').matchHeight();
});
$(function(){
  $(window).on('load resize',function(){
    var maskW = $(".video-thum").width();
    var maskH = $(".video-thum").height();
    $(".video-mask").css({"width":maskW});
    $(".video-mask").css({"height":maskH});
    var arrowW = $(window).width();
    $(".arrow-top").css({"border-left":arrowW/2+"px solid transparent","border-right":arrowW/2+"px solid transparent"});
    $(".arrow-bottom").css({"border-left":arrowW/2+"px solid transparent","border-right":arrowW/2+"px solid transparent"});
    var H = $(window).height();
    $(".modal-body").css({"max-height":H-120+"px"});
    var current_scrollY;
    $("[data-toggle='modal']").on( 'click', function(){
      current_scrollY = $( window ).scrollTop(); 
      $('body').css( {
        top: -1 * current_scrollY
      });        
    });
    $(".modal").on("shown", function(){
        $("body").addClass("modal-open");
    });
    $(".modal").on("hidden", function(){
        $("body").removeClass("modal-open");
    });
  });
});
$(function(){
  $('.accordion a').click(function(){
    $(this).children().removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
    $('a[aria-expanded=true]').children().removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
  });
  $('.scroll-spy a').click(function(){
      var speed = 500;
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $("html, body").animate({scrollTop:position-100}, speed, "swing");
      return false;
  });
  $(window).load(function(){
    if($('#check').attr("checked")){
      action1();
    }
    else {
      action2();
    };
  });
  $('#check').click(function(){
    if(this.checked){
      action1();
    }
    else {
      action2();
    };
  });
  function action1() {
    $('#btn').removeClass("disabled");
  };
  function action2() {
    $('#btn').addClass("disabled");
  };
  if ( navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 
  || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0 ) {
    $('.app-start').css({"display":"block"});
  } else {
    $('.app-start').css({"display":"none"});
  }
});
