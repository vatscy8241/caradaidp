using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.Web.Administration;

namespace MTI.CaradaIdProvider.Web
{
    public class WebRole : RoleEntryPoint
    {
        public override void Run()
        {
            // クラウドサービスもしくはエミュレーターで実行しているかをチェック
            if (RoleEnvironment.IsAvailable)
            {
                // エミュレーターで実行していない場合のみオートスタートの設定を実施
                if (!RoleEnvironment.IsEmulated)
                {
                    using (var serverManager = new ServerManager())
                    {
                        var mainSite = serverManager.Sites[RoleEnvironment.CurrentRoleInstance.Id + "_Web"];
                        var mainApplication = mainSite.Applications["/"];
                        mainApplication["preloadEnabled"] = true;

                        var mainApplicationPool = serverManager.ApplicationPools[mainApplication.ApplicationPoolName];
                        mainApplicationPool["startMode"] = "AlwaysRunning";

                        serverManager.CommitChanges();
                    }
                }
            }
            base.Run();
        }

        public override bool OnStart()
        {
            if (RoleEnvironment.IsAvailable && !RoleEnvironment.IsEmulated)
            {
                RoleEnvironment.Changing += OnRoleEnvironmentChanging;
            }

            return base.OnStart();
        }

        /// Force restart of the instance.
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnRoleEnvironmentChanging(object sender, RoleEnvironmentChangingEventArgs e)
        {
            if (e.Changes.Any(o => o is RoleEnvironmentTopologyChange))
            {
                e.Cancel = false;
            }
            else if (e.Changes.Any(o => o is RoleEnvironmentChange))
            {
                e.Cancel = true;
            }
        }
    }
}
