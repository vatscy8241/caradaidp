﻿using MTI.CaradaIdProvider.Web.Properties;
using MTI.CaradaIdProvider.Web.Utilities;
using NLog;
using System;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;

namespace MTI.CaradaIdProvider.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// ログ
        /// </summary>
        protected static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        protected void Application_Start()
        {
            // SP/FP版レイアウトの切り替え設定
            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode(DeviceTypeUtility.DISPLAY_MODE_FP)
            {
                // DisplayModeはAnalyticsFilterで設定したものを取得します
                ContextCondition = (context => DeviceTypeUtility.DISPLAY_MODE_FP.Equals(context.Items["DisplayMode"]))
            });

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // X-AspNetMvc-Version ヘッダを出力しないようにする。
            MvcHandler.DisableMvcResponseHeader = true;
        }

        /// <summary>
        /// アプリケーションエラー
        /// </summary>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            HttpException httpException = exception as HttpException;

            if (httpException != null && httpException.GetHttpCode() == 404)
            {
                // ログ出力
                LOG.Warn(string.Format("HTTP404エラーが発生しました。(url={0})", HttpContext.Current.Request.Url.ToString()));
            }
            else
            {
                // ログ出力
                LOG.Error(exception, "サーバエラーが発生しました。");
            }
        }

        /// <summary>
        /// リクエスト処理前処理
        /// FP端末の場合にShift_JISでリクエストとレスポンスを処理する
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">EventArgs</param>
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var application = sender as HttpApplication;
            var request = application.Context.Request;
            var response = application.Context.Response;
            var displayMode = DeviceTypeUtility.GetDisplayModeByUserAgent(request.UserAgent);
            application.Context.Items["DisplayMode"] = displayMode;
            if (displayMode == DeviceTypeUtility.DISPLAY_MODE_FP)
            {
                request.ContentEncoding = Encoding.GetEncoding("Shift_JIS");
                response.ContentEncoding = Encoding.GetEncoding("Shift_JIS");
            }
        }
    }
}
