﻿using Microsoft.Azure;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.Logics;
using MTI.CaradaIdProvider.Web.Models;
using MTI.CaradaIdProvider.Web.Properties;
using MTI.CaradaIdProvider.Web.Utilities;
using NLog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Providers
{
    /// <summary>
    /// CARADA Open ID Connectプロバイダ。
    /// このオブジェクトはシングルトンです。
    /// </summary>
    public class CaradaIdOAuthServiceProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// scopeの固定文字列
        /// </summary>
        private const string SCOPE_OPENID = "openid";
        /// <summary>
        /// response_typeの固定文字列
        /// </summary>
        private const string RESPONSE_TYPE_CODE = "code";
        /// <summary>
        /// エラー情報をヘッダに保持するキー
        /// </summary>
        private const string REQUEST_ERROR_HEADER = "request_error";
        /// <summary>
        /// レスポンスの際のcontentType
        /// </summary>
        private const string CONTENT_TYPE = "application/x-www-form-urlencoded; charset=utf-8";
        /// <summary>
        /// RSA暗号化ロジックにて生成したRSAオブジェクト
        /// </summary>
        private RsaSecurityKey rsaSecurityKey;
        /// <summary>
        /// DbContextFactory
        /// </summary>
        private DbContextFactory dbFactory = new DbContextFactory();
        /// <summary>
        /// 二段階認証
        /// </summary>
        private TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();
        /// <summary>
        /// ログ
        /// </summary>
        protected static readonly ILogger LOG = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// コンストラクタ。
        /// </summary>
        public CaradaIdOAuthServiceProvider()
            : base()
        {
            rsaSecurityKey = new RsaSecurityKey(
                new RSAEncryptor(CloudConfigurationManager.GetSetting(Settings.Default.SignatureKey)).ToRSA());
        }

        #region AuthenticationRequest 呼び出される順番に記載
        /// <summary>
        /// AuthenticationRequestにおいて、正しいClientIdとRedirectUriの組であるかどうかを確認する。
        /// 正しくない場合HttpStatus:400でエラーがブラウザにレスポンスされる。
        /// フレームワークの仕様上、ResponseType未設定の場合も同じエラーとなる。
        /// </summary>
        /// <param name="context">Validate用Context</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override async Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            LOG.Info("Authentication Request： [ " + context.Request.QueryString + "]");

            context.Validated();

            if (context.ClientId == null || context.RedirectUri == null)
            {
                context.Rejected();
            }
            else
            {
                using (var caradaIdPDb = dbFactory.NewCaradaIdPDbContext())
                {
                    var clientId = (from r in caradaIdPDb.RedirectUriMasters
                                    where r.ClientId == context.ClientId && r.RedirectUri == context.RedirectUri
                                    select r.ClientId).SingleOrDefault();

                    if (string.IsNullOrEmpty(clientId) || !clientId.Equals(context.ClientId))
                    {
                        // clientIdについては大文字小文字を区別する（DBにcollateは設定していない）
                        context.Rejected();
                    }
                }
            }

            if (!context.IsValidated)
            {
                context.SetError("invalid redirect_uri");
            }

            await Task.FromResult(0);
        }

        /// <summary>
        /// AuthenticationRequestが正しいリクエスト形式であるかを確認する。エラーの場合はRedirectUriにリダイレクト送信する。
        /// SetError()は呼び出さない。ここでValidateがNGの場合、入力されたstateを含めずにリダイレクトしてしまいOAuth2仕様不正となるため、
        /// ヘッダにエラー情報を保持し、ValidateはOK扱いとする。
        /// </summary>
        /// <param name="context">Validate用Context</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override async Task ValidateAuthorizeRequest(OAuthValidateAuthorizeRequestContext context)
        {
            context.Validated();

            // client_idはチェック済み

            if (context.AuthorizeRequest.Scope.Count == 0 || context.AuthorizeRequest.ResponseType == null)
            {
                // 必須チェック response_type未設定の場合は実際にはこのメソッドは呼び出されない
                context.Request.Headers.Add(REQUEST_ERROR_HEADER, new string[] { "invalid_request" });
            }
            else if (context.AuthorizeRequest.Scope.Count > 1 || context.AuthorizeRequest.Scope.SingleOrDefault() != SCOPE_OPENID)
            {
                // scopeがあっていないか2個以上指定された場合
                context.Request.Headers.Add(REQUEST_ERROR_HEADER, new string[] { "invalid_scope" });
            }
            else if (context.AuthorizeRequest.ResponseType != RESPONSE_TYPE_CODE)
            {
                // response_typeがあっていない 事実上implicitモード（tokenを指定された）のときだけこの分岐になる
                context.Request.Headers.Add(REQUEST_ERROR_HEADER, new string[] { "unsupported_response_type" });
            }

            await Task.FromResult(0);
        }

        /// <summary>
        /// AuthenticationRequestのEndpoint実装。
        /// ログインしていない場合はログイン画面にリダイレクトする。
        /// ログインしている場合は認可コードを生成して正常応答をリダイレクトする。
        /// エラーの場合はエラー応答をリダイレクトする。
        /// </summary>
        /// <param name="context">Endpoint用Context</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override async Task AuthorizeEndpoint(OAuthAuthorizeEndpointContext context)
        {
            context.Response.ContentType = CONTENT_TYPE;
            context.RequestCompleted();
            
            // Microsoft.Owin.Host.SystemWebによりHttpContextBaseが保持されている
            var httpContext = context.Request.Get<HttpContextBase>(typeof(HttpContextBase).FullName);

            try
            {
                // Validateエラーの場合
                if (context.Request.Headers.ContainsKey(REQUEST_ERROR_HEADER))
                {
                    RedirectAuthorizeResponse(context, new Dictionary<string, string> { { "error", context.Request.Headers[REQUEST_ERROR_HEADER] } });
                    return;
                }

                using (var caradaIdPDb = dbFactory.NewCaradaIdPDbContext())
                {

                    // ログイン済み、またはログイン永続化を行っている場合（このときは当然セッションはない）
                    if (context.Request.User != null && context.Request.User.Identity.IsAuthenticated)
                    {
                        // 認可済みかどうか確認する
                        var authorizedUser = (from a in caradaIdPDb.AuthorizedUsers
                                              join u in caradaIdPDb.Users on a.UserId equals u.Id
                                              where a.ClientId == context.AuthorizeRequest.ClientId
                                                    && u.UserName == context.Request.User.Identity.Name
                                              select a).SingleOrDefault();

                        if (authorizedUser == null || !authorizedUser.ClientId.Equals(context.AuthorizeRequest.ClientId))
                        {
                            // 認可されていない場合
                            RedirectAuthorizeResponse(context, new Dictionary<string, string> { { "error", "unauthorized_client" } });
                            return;
                        }

                        // 認可コードContextを作る
                        var authorizeCodeContext = new AuthenticationTokenCreateContext(context.OwinContext, context.Options.AuthorizationCodeFormat,
                            new AuthenticationTicket(new ClaimsIdentity(context.Request.User.Identity),
                                new AuthenticationProperties(
                                    new Dictionary<string, string>
                                {
                                    {"clientId", context.AuthorizeRequest.ClientId},
                                    {"redirectUri", context.AuthorizeRequest.RedirectUri},
                                })
                                {
                                    ExpiresUtc = DateTimeOffset.UtcNow.Add(context.Options.AuthorizationCodeExpireTimeSpan)
                                }));
                        // 認可コード生成
                        await context.Options.AuthorizationCodeProvider.CreateAsync(authorizeCodeContext);

                        // 正常応答
                        RedirectAuthorizeResponse(context, new Dictionary<string, string> { { "code", Uri.EscapeDataString(authorizeCodeContext.Token) } });
                        return;
                    }
                }

                // 画面との引継ぎセッションを取得
                var session = httpContext.Session[Settings.Default.AuthorizeEndpointSession] as AuthorizeEndpointSession;

                // 現在メンテナンス中、もしくは画面側でメンテナンス中だった場合（Token RequestではRPのIPをチェックする意味がないので、Authentication Requestでも行わない）
                if (CloudConfigurationManager.GetSetting("MaintenanceMode") != "0" || (session != null && session.LoginResult == LoginResult.Maintenance))
                {
                    RedirectAuthorizeResponse(context, new Dictionary<string, string> { { "error", "temporarily_unavailable" } });
                    return;
                }

                // 未ログインの場合
                if (session == null || session.LoginResult == LoginResult.Unlogin)
                {
                    var loginLocation = "/User/LoginView";

                    // セッションに入れる
                    httpContext.Session[Settings.Default.AuthorizeEndpointSession] = new AuthorizeEndpointSession
                    {
                        ClientId = context.AuthorizeRequest.ClientId,
                        RedirectUri = context.AuthorizeRequest.RedirectUri,
                        Scope = context.AuthorizeRequest.Scope.SingleOrDefault(),
                        ResponseType = context.AuthorizeRequest.ResponseType,
                        State = context.AuthorizeRequest.State,
                        LoginResult = LoginResult.Unlogin,
                        ReturnUrl = context.Options.AuthorizeEndpointPath.Value
                    };

                    // ログイン画面へ遷移
                    context.Response.Redirect(loginLocation);
                    return;
                }

                // 画面側でサーバーエラーの場合
                if (session.LoginResult == LoginResult.Error)
                {
                    RedirectAuthorizeResponse(context, new Dictionary<string, string> { { "error", "server_error" } });
                    return;
                }

                // 想定外のパターン
                throw new SystemException("Unexpected request pattern.");
            }
            catch (Exception e)
            {
                LOG.Error(e, "サーバエラーが発生しました。");
                // ここでの例外はサーバーエラーを返す
                RedirectAuthorizeResponse(context, new Dictionary<string, string> { { "error", "server_error" } });
            }
        }

        /// <summary>
        /// AuthenticationRequestに対して、認可リダイレクトレスポンスをcontextに指定する。
        /// リクエストパラメータにstateが含まれる場合はリダイレクトレスポンスに同値を付加する。
        /// 画面への引継ぎセッションはここで削除する。
        /// </summary>
        /// <param name="context">context</param>
        /// <param name="queries">リダイレクトレスポンスに付加するパラメータ</param>
        private void RedirectAuthorizeResponse(OAuthAuthorizeEndpointContext context, Dictionary<string, string> queries)
        {
            (context.Request.Get<HttpContextBase>(typeof(HttpContextBase).FullName)).Session.Remove(Settings.Default.AuthorizeEndpointSession);
            var location = WebUtilities.AddQueryString(context.AuthorizeRequest.RedirectUri, queries);
            if (context.AuthorizeRequest.State != null)
            {
                location = WebUtilities.AddQueryString(location, "state", context.AuthorizeRequest.State);
            }
            context.Response.Redirect(location);
        }
        #endregion
        #region TokenRequest 呼び出される順番に記載
        /// <summary>
        /// TokenRequestにおいて、clientIdとclientSecretの組が正しいか確認する。
        /// ValidateTokenRequestよりも先に呼び出される。
        /// </summary>
        /// <param name="context">Validate用Context</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            LOG.Info("Token Request：" + GetTokenRequestParametersString(context));

            context.Rejected();

            using (var caradaIdPDb = dbFactory.NewCaradaIdPDbContext())
            {
                string clientId;
                string clientSecret;

                if (context.TryGetFormCredentials(out clientId, out clientSecret))
                {
                    var clientMasters = (from c in caradaIdPDb.ClientMasters
                                         where c.ClientId == clientId && c.ClientSecret == clientSecret
                                         select c).SingleOrDefault();

                    if (clientMasters != null && clientMasters.ClientId.Equals(clientId) && clientMasters.ClientSecret.Equals(clientSecret))
                    {
                        context.Validated(clientId);
                    }
                }
            }

            await Task.FromResult(0);
        }

        /// <summary>
        /// Token Requestのパラメータを文字列で取得する
        /// </summary>
        /// <param name="context">OAuthValidateClientAuthenticationContext</param>
        /// <returns>Token Requestのパラメータ文字列</returns>
        private string GetTokenRequestParametersString(OAuthValidateClientAuthenticationContext context)
        {
            return LogUtilities.GetParameterString(context.Parameters);
        }

        /// <summary>
        /// TokenRequestが正しいRequest形式であるかどうか確認する。
        /// エラーの場合は定められたjsonでレスポンスする。
        /// 別のメソッドで既に行われているチェック、フレームワークが行うチェックについては確認しない。
        /// </summary>
        /// <param name="context">Validate用Context</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override async Task ValidateTokenRequest(OAuthValidateTokenRequestContext context)
        {
            context.Validated();

            // 必須チェックはHandlerにて事実上行われている

            // メンテナンス中チェック（対象のIPはRPなので、IPのチェックはせず。メンテナンスモードOFFでないことでチェックする）
            if (CloudConfigurationManager.GetSetting("MaintenanceMode") != "0")
            {
                context.SetError("temporarily_unavailable");
                return;
            }

            // grant_type
            if (!context.TokenRequest.IsAuthorizationCodeGrantType)
            {
                context.SetError("unsupported_grant_type");
                return;
            }

            await Task.FromResult(0);
        }

        /// <summary>
        /// ValidateTokenRequestが正常であった場合に呼び出されるメソッド。
        /// 認可コードをReceiveした際に設定したチケットを取得できる。
        /// </summary>
        /// <param name="context">Context</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override async Task GrantAuthorizationCode(OAuthGrantAuthorizationCodeContext context)
        {
            context.Validated();

            var ticket = context.Ticket;
            // そのユーザーが認可しているか
            if (!ticket.Properties.Dictionary.ContainsKey("sub"))
            {
                context.SetError("unauthorized_client");
                return;
            }

            await Task.FromResult(0);
        }

        /// <summary>
        /// TokenRequestのEndpoint実装。
        /// json形式でパラメータをレスポンスする。id_token以外はフレームワークによって自動でレスポンスされる。
        /// </summary>
        /// <param name="context">TokenEndpoint用context</param>
        /// <returns>
        /// Task to enable asynchronous execution
        /// </returns>
        public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            context.RequestCompleted();

            // id_token
            context.AdditionalResponseParameters["id_token"] = MakeIdToken(context);

            await Task.FromResult(0);
        }

        /// <summary>
        /// id_tokenを作成する。TokenEndpointメソッドから呼び出される想定。
        /// </summary>
        /// <param name="context">TokenEndpoint用context</param>
        /// <returns>id_token</returns>
        protected virtual string MakeIdToken(OAuthTokenEndpointContext context)
        {
            var credentials = new SigningCredentials(
                rsaSecurityKey,
                SecurityAlgorithms.RsaSha256Signature,
                SecurityAlgorithms.RsaSha256Signature);

            var issuedTime = DateTime.UtcNow;
            var expiresTime = issuedTime.AddMinutes(60);

            var token = new JwtSecurityToken(
                issuer: Settings.Default.ISS,
                audience: context.TokenEndpointRequest.ClientId,
                claims: new[]
                    {
                        new Claim("sub", context.Properties.Dictionary["sub"]),
                        new Claim("iat", issuedTime.ToUnixTime().ToString(), ClaimValueTypes.Integer64)
                    },
                expires: expiresTime,
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        #endregion
    }
}