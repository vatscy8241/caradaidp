﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Logics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.Providers
{
    /// <summary>
    /// 認可コードプロバイダ。
    /// このオブジェクトはシングルトンです。
    /// </summary>
    public class CaradaIdAuthorizationCodeProvider : AuthenticationTokenProvider
    {
        /// <summary>
        /// DbContextFactory
        /// </summary>
        private DbContextFactory dbFactory = new DbContextFactory();
        /// <summary>
        /// ID生成ロジック
        /// </summary>
        private IdGenerator idGenerator = new IdGenerator();

        /// <summary>
        /// 認可コードを生成する。
        /// </summary>
        /// <param name="context">認可コード生成Context</param>
        /// <returns>Task to enable asynchronous execution</returns>
        public override Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            using (var caradaIdPDb = dbFactory.NewCaradaIdPDbContext())
            {
                var userId = (from u in caradaIdPDb.Users
                              where u.UserName == context.Ticket.Identity.Name
                              select u.Id).SingleOrDefault();
                var clientId = context.Ticket.Properties.Dictionary["clientId"];
                var redirectUri = context.Ticket.Properties.Dictionary["redirectUri"];
                var redirectUriSeqNo = (from r in caradaIdPDb.RedirectUriMasters
                                        where r.ClientId == clientId && r.RedirectUri == redirectUri
                                        select r.SeqNo).SingleOrDefault();
                // 生成
                var code = idGenerator.GenerateAuthorizationCode();

                // DB登録
                caradaIdPDb.UserAuthCodes.Add(new UserAuthCodes()
                {
                    UserId = userId,
                    AuthorizationCode = code,
                    ExpireDateUtc = context.Ticket.Properties.ExpiresUtc.Value.DateTime,
                    RedirectUriSeqNo = redirectUriSeqNo
                });

                caradaIdPDb.SaveChanges();

                context.SetToken(code);
            }
            return Task.FromResult(0);
        }

        /// <summary>
        /// 認可コードを受信する。ValidateTokenRequestよりも前に呼び出される。
        /// 認可コードから取得できる情報のうち、clientId, expiresUtc, redirectUriをTicketにセットすることで、
        /// フレームワークによってTicketの内容からエラーが判断される。エラーの場合はすべてinvalid_grantになる。
        /// このメソッドが呼び出された段階で、正常であってもエラーであってもこの認可コードは使用不能になる。
        /// </summary>
        /// <param name="context">認可コード受信context</param>
        /// <returns>Task to enable asynchronous execution</returns>
        public override Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            using (var caradaIdPDb = dbFactory.NewCaradaIdPDbContext())
            {
                // 認可コードから検索
                var authCodes = (from ua in caradaIdPDb.UserAuthCodes
                                 join u in caradaIdPDb.Users on ua.UserId equals u.Id
                                 join r in caradaIdPDb.RedirectUriMasters on ua.RedirectUriSeqNo equals r.SeqNo
                                 join au in caradaIdPDb.AuthorizedUsers on new { ua.UserId, r.ClientId } equals new { au.UserId, au.ClientId } into gj
                                 from subauth in gj.DefaultIfEmpty()
                                 where ua.AuthorizationCode == context.Token
                                 select new
                                 {
                                     UserAuthCodes = ua,
                                     Users = u,
                                     RedirectUriMasters = r,
                                     Sub = subauth != null ? subauth.Sub : null
                                 }).SingleOrDefault();

                if (authCodes != null && authCodes.UserAuthCodes.AuthorizationCode.Equals(context.Token))
                {
                    var dictionary = new Dictionary<string, string> {
                          {"client_id", authCodes.RedirectUriMasters.ClientId},
                          // redirect_uriはticket自体のプロパティのほかにDictionaryにも入れなくては検証してくれないらしい
                          {"redirect_uri", authCodes.RedirectUriMasters.RedirectUri}
                    };
                    if (authCodes.Sub != null)
                    {
                        dictionary["sub"] = authCodes.Sub;
                    }

                    // フレームワークに返却するチケットを構築する
                    var ticket = new AuthenticationTicket(
                        new GenericIdentity(authCodes.Users.UserName),
                        new AuthenticationProperties(dictionary)
                        {
                            ExpiresUtc = DateTime.SpecifyKind(authCodes.UserAuthCodes.ExpireDateUtc, DateTimeKind.Utc),
                            RedirectUri = authCodes.RedirectUriMasters.RedirectUri
                        });

                    context.SetTicket(ticket);

                    // 認可コードの使用
                    caradaIdPDb.UserAuthCodes.Remove(caradaIdPDb.UserAuthCodes.Find(context.Token));
                    caradaIdPDb.SaveChanges();
                }
            }

            return Task.FromResult(0);
        }
    }
}