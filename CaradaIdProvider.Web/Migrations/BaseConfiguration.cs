﻿using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using System.Data.Entity.Migrations;
using System.Linq;

namespace MTI.CaradaIdProvider.Web.Migrations
{
    /// <summary>
    /// Configurationのベースクラス。
    /// </summary>
    public abstract class BaseConfiguration : DbMigrationsConfiguration<CaradaIdPDbContext>
    {
        /// <summary>
        /// 各環境共通の設定を行うコンストラクタ。
        /// </summary>
        public BaseConfiguration()
        {
            AutomaticMigrationsEnabled = false;
            CommandTimeout = 300;
        }

        /// <summary>
        /// RedirectUriMastersに対象データが未登録の場合に登録する。
        /// 主キーがIdentityなのでAddOrUpdateが使用できないため
        /// </summary>
        /// <param name="context">CaradaIdPDbContext</param>
        /// <param name="data">RedirectUriMasters</param>

        protected void AddRedirectUriMasters(CaradaIdPDbContext context, RedirectUriMasters data)
        {
            var registered = (from r in context.RedirectUriMasters
                              where r.ClientId == data.ClientId && r.RedirectUri == data.RedirectUri
                              select r).SingleOrDefault();
            if (registered != null)
            {
                return;
            }
            context.RedirectUriMasters.Add(data);
        }

        /// <summary>
        /// 秘密の質問マスタ登録
        /// </summary>
        /// <param name="context">CaradaIdPDbContext</param>
        protected void AddSecurityQuestionMasters(CaradaIdPDbContext context)
        {
            // 秘密の質問マスタ
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 1,
                Question = "初めての職場の部署名は？",
                DisplayOrder = 1
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 2,
                Question = "中学生時代に所属していた部活は？",
                DisplayOrder = 2
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 3,
                Question = "子どもの頃の夢は？",
                DisplayOrder = 3
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 4,
                Question = "定年後に住みたい場所は？",
                DisplayOrder = 4
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 5,
                Question = "好きな映画のタイトルは？",
                DisplayOrder = 5
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 6,
                Question = "好きなスポーツチームは？",
                DisplayOrder = 6
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 7,
                Question = "初めて作った料理は？",
                DisplayOrder = 7
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 8,
                Question = "小学生時代の担任の先生の名前は？",
                DisplayOrder = 8
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 9,
                Question = "好きな歌手またはバンドの名前は？",
                DisplayOrder = 9
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 10,
                Question = "初めて買ったCDのタイトルは？",
                DisplayOrder = 10
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 11,
                Question = "おふくろの味といえば？",
                DisplayOrder = 11
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 12,
                Question = "座右の銘は？",
                DisplayOrder = 12
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 13,
                Question = "初めて飼ったペットの名前は？",
                DisplayOrder = 13
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 14,
                Question = "子どもの頃のあだ名は？",
                DisplayOrder = 14
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 15,
                Question = "子どもの頃に住んでいた町の名前は？",
                DisplayOrder = 15
            });
            context.SecurityQuestionMasters.AddOrUpdate(new SecurityQuestionMasters()
            {
                SecurityQuestionId = 16,
                Question = "好きな漫画のタイトルは？",
                DisplayOrder = 16
            });
        }
    }
}