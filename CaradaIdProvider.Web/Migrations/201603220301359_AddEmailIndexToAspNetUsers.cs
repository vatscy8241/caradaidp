namespace MTI.CaradaIdProvider.Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddEmailIndexToAspNetUsers : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.AspNetUsers", "Email", unique: false, name: "EmailIndex");
        }

        public override void Down()
        {
            DropIndex("dbo.AspNetUsers", name: "EmailIndex");
        }
    }
}
