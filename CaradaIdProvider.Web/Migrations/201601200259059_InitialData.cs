namespace MTI.CaradaIdProvider.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuthorizedUsers",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClientId = c.String(nullable: false, maxLength: 32),
                        Sub = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => new { t.UserId, t.ClientId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.ClientMasters", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ClientId)
                .Index(t => t.Sub, unique: true);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        EmployeeFlag = c.Boolean(nullable: false),
                        CreateDateUtc = c.DateTime(nullable: false),
                        UseStartDateUtc = c.DateTime(),
                        UpdateDateUtc = c.DateTime(),
                        AuthCodeExpireDateUtc = c.DateTime(),
                        AuthCodeFailedCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ClientMasters",
                c => new
                    {
                        ClientId = c.String(nullable: false, maxLength: 32),
                        ClientName = c.String(nullable: false, maxLength: 100),
                        ClientSecret = c.String(nullable: false, maxLength: 256),
                        AuthorizeConfirmFlag = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ClientId)
                .Index(t => t.ClientName, unique: true);
            
            CreateTable(
                "dbo.RedirectUriMasters",
                c => new
                    {
                        SeqNo = c.Long(nullable: false, identity: true),
                        ClientId = c.String(nullable: false, maxLength: 32),
                        RedirectUri = c.String(nullable: false, maxLength: 300),
                    })
                .PrimaryKey(t => t.SeqNo)
                .ForeignKey("dbo.ClientMasters", t => t.ClientId, cascadeDelete: true)
                .Index(t => new { t.ClientId, t.RedirectUri }, unique: true);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SecurityQuestionAnswers",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        SecurityQuestionId = c.Int(nullable: false),
                        Answer = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => new { t.UserId, t.SecurityQuestionId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.SecurityQuestionMasters", t => t.SecurityQuestionId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SecurityQuestionId);
            
            CreateTable(
                "dbo.SecurityQuestionMasters",
                c => new
                    {
                        SecurityQuestionId = c.Int(nullable: false, identity: true),
                        Question = c.String(nullable: false, maxLength: 128),
                        DisplayOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SecurityQuestionId)
                .Index(t => t.DisplayOrder);
            
            CreateTable(
                "dbo.UserAuthCodes",
                c => new
                    {
                        AuthorizationCode = c.String(nullable: false, maxLength: 32),
                        UserId = c.String(nullable: false, maxLength: 128),
                        RedirectUriSeqNo = c.Long(nullable: false),
                        ExpireDateUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AuthorizationCode)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.RedirectUriMasters", t => t.RedirectUriSeqNo, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RedirectUriSeqNo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserAuthCodes", "RedirectUriSeqNo", "dbo.RedirectUriMasters");
            DropForeignKey("dbo.UserAuthCodes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SecurityQuestionAnswers", "SecurityQuestionId", "dbo.SecurityQuestionMasters");
            DropForeignKey("dbo.SecurityQuestionAnswers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RedirectUriMasters", "ClientId", "dbo.ClientMasters");
            DropForeignKey("dbo.AuthorizedUsers", "ClientId", "dbo.ClientMasters");
            DropForeignKey("dbo.AuthorizedUsers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserAuthCodes", new[] { "RedirectUriSeqNo" });
            DropIndex("dbo.UserAuthCodes", new[] { "UserId" });
            DropIndex("dbo.SecurityQuestionMasters", new[] { "DisplayOrder" });
            DropIndex("dbo.SecurityQuestionAnswers", new[] { "SecurityQuestionId" });
            DropIndex("dbo.SecurityQuestionAnswers", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.RedirectUriMasters", new[] { "ClientId", "RedirectUri" });
            DropIndex("dbo.ClientMasters", new[] { "ClientName" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AuthorizedUsers", new[] { "Sub" });
            DropIndex("dbo.AuthorizedUsers", new[] { "ClientId" });
            DropIndex("dbo.AuthorizedUsers", new[] { "UserId" });
            DropTable("dbo.UserAuthCodes");
            DropTable("dbo.SecurityQuestionMasters");
            DropTable("dbo.SecurityQuestionAnswers");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.RedirectUriMasters");
            DropTable("dbo.ClientMasters");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AuthorizedUsers");
        }
    }
}
