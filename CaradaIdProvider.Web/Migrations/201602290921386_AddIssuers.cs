namespace MTI.CaradaIdProvider.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIssuers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Issuers",
                c => new
                    {
                        IssuerId = c.String(nullable: false, maxLength: 32),
                        SecretKey = c.String(nullable: false, maxLength: 64),
                        Remark = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.IssuerId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Issuers");
        }
    }
}
