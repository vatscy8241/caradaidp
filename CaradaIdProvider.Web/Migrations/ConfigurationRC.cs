﻿using Microsoft.AspNet.Identity.EntityFramework;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using System.Data.Entity.Migrations;

namespace MTI.CaradaIdProvider.Web.Migrations
{
    /// <summary>
    /// RC環境用
    /// </summary>
    internal sealed class ConfigurationRC : BaseConfiguration
    {
        /// <summary>
        /// コンテキストにデータを追加してシードする
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(CaradaIdPDbContext context)
        {
            // クライアントマスタ
            context.ClientMasters.AddOrUpdate(new ClientMasters()
            {
                ClientId = "a23f6519ebc84cdb85f2df24392f909c",
                ClientName = "AuthProvider",
                ClientSecret = "88184dab434145c8b25d94819bdb346f",
                AuthorizeConfirmFlag = false
            });

            // リダイレクトURIマスタ
            // Azure SQL Databaseで「DBCC CHECKIDENT」が無効なため、登録されていない場合のみ登録としている。
            // 更新、削除したい場合は手動で削除してから行うこと
            RedirectUriMasters redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "a23f6519ebc84cdb85f2df24392f909c",
                RedirectUri = "https://rc.hc-auth.com/ja-JP/web/authenticate/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "a23f6519ebc84cdb85f2df24392f909c",
                RedirectUri = "https://rc.hc-auth.com/ja-JP/web/userLogin/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "a23f6519ebc84cdb85f2df24392f909c",
                RedirectUri = "https://rc.hc-auth.com/ja-JP/web/account/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "a23f6519ebc84cdb85f2df24392f909c",
                RedirectUri = "https://rc.hc-auth.com/ja-JP/web/luna/authenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            // ロール
            context.Roles.AddOrUpdate(new IdentityRole()
            {
                Id = "d2fa14c7-28d0-4b29-af74-594296dab454", //ロールID。ASP.NET Identityが自動生成するものを固定値にした。
                Name = Properties.Settings.Default.UserRole
            });

            // 秘密の質問マスタ
            AddSecurityQuestionMasters(context);

            // JWT発行者情報
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminRc01",
                SecretKey = "x%l@$aP*#cs[%MGV!4O7;etDy?=td3_-O6j0vh_+_Bb-yrErx7gBZkFq%Q&}(8XA",
                Remark = "RC環境用の発行者情報01"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminRc02",
                SecretKey = "1v2N-SIF#9>lU3[6O@x]8881oe*9;zA)E26.}X;s1}#uTxSSy$2Jm@l$X%;y}u)3",
                Remark = "RC環境用の発行者情報02"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminRc03",
                SecretKey = "fVYu=Djr%K;YxKw!9^1lXY|$IZ}gdqCcM;nLrk{W@Lfpi(Zt{0/IC8bkRGRSy$_J",
                Remark = "RC環境用の発行者情報03"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminRc04",
                SecretKey = "d6^G[BqjE4:W:3H-7*7#2U)+>G%MFrCGj*qGfhWNA-iv*S)7HJs:_w!iOetK-Inu",
                Remark = "RC環境用の発行者情報04"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminRc05",
                SecretKey = "LLyT7/aE2vBT@)!*McrcUG{C*^+_^)DRR;!)uWY+]pZyZi_)y2&is$$kc*lKc)jZ",
                Remark = "RC環境用の発行者情報05"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaIdPTestRc",
                SecretKey = "B.&u/d]pv:GD!IgevG]-%7VD#LU;OTrlCTdf!OP#Fefq}^PHsj$ETQD72y%CFdbB",
                Remark = "開発テスト用の発行者情報(RC環境)"
            });
        }
    }
}