﻿using Microsoft.AspNet.Identity.EntityFramework;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using System.Data.Entity.Migrations;

namespace MTI.CaradaIdProvider.Web.Migrations
{
    /// <summary>
    /// 本番環境用
    /// </summary>
    internal sealed class ConfigurationRelease : BaseConfiguration
    {
        /// <summary>
        /// コンテキストにデータを追加してシードする
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(CaradaIdPDbContext context)
        {
            // クライアントマスタ
            context.ClientMasters.AddOrUpdate(new ClientMasters()
            {
                ClientId = "3a9996f6bc7e4963949b3f3ccaf95b1a",
                ClientName = "AuthProvider",
                ClientSecret = "0eec1cc0cc0e4a74921b7b25986176e9",
                AuthorizeConfirmFlag = false
            });

            // リダイレクトURIマスタ
            // Azure SQL Databaseで「DBCC CHECKIDENT」が無効なため、登録されていない場合のみ登録としている。
            // 更新、削除したい場合は手動で削除してから行うこと
            RedirectUriMasters redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "3a9996f6bc7e4963949b3f3ccaf95b1a",
                RedirectUri = "https://www.hc-auth.com/ja-JP/web/authenticate/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "3a9996f6bc7e4963949b3f3ccaf95b1a",
                RedirectUri = "https://www.hc-auth.com/ja-JP/web/userLogin/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "3a9996f6bc7e4963949b3f3ccaf95b1a",
                RedirectUri = "https://www.hc-auth.com/ja-JP/web/account/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "3a9996f6bc7e4963949b3f3ccaf95b1a",
                RedirectUri = "https://www.hc-auth.com/ja-JP/web/luna/authenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            // ロール
            context.Roles.AddOrUpdate(new IdentityRole()
            {
                Id = "cbd06013-b835-4f5d-ba80-88c55d41788b", //ロールID。ASP.NET Identityが自動生成するものを固定値にした。
                Name = Properties.Settings.Default.UserRole
            });

            // 秘密の質問マスタ
            AddSecurityQuestionMasters(context);

            // JWT発行者情報
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin01",
                SecretKey = "[BXd=JNB}{LOr4YS7)h^Tmewnf1;i{jfaW2_u.|VTG!49f18lvikRD_%Y3hW3np5",
                Remark = "本番環境用の発行者情報01"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin02",
                SecretKey = ":Ia+8RLj?x@DOSz@_mKv8Kp^*y^nFOnuBeoXh=%vWyrL_!r0f}pkbPS%H^>6EhP0",
                Remark = "本番環境用の発行者情報02"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin03",
                SecretKey = "0]]MjtWlCg#FC{mPY8Qdf7[+{s*7XSF6+i{%_1dy8G!rQ7@/rcOI){uNzRRwUtzj",
                Remark = "本番環境用の発行者情報03"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin04",
                SecretKey = "0=Jf3Zz*dLJf*Tu-W4wqYF%rE3s)=GzG#+exm7:sWa^]$I[Jna{fWk(^yc2QZz(8",
                Remark = "本番環境用の発行者情報04"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdmin05",
                SecretKey = "6rW^:2A)Xh?MlmXARe/guw!pn.8U6YruU3vM+?_zN$&ks8BeEXB0SgZci:buT_e%",
                Remark = "本番環境用の発行者情報05"
            });
        }
    }
}