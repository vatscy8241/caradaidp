﻿using Microsoft.AspNet.Identity.EntityFramework;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using System.Data.Entity.Migrations;

namespace MTI.CaradaIdProvider.Web.Migrations
{
    /// <summary>
    /// 開発環境用
    /// </summary>
    internal sealed class ConfigurationDevelopment : BaseConfiguration
    {
        /// <summary>
        /// コンテキストにデータを追加してシードする
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(CaradaIdPDbContext context)
        {
            // クライアントマスタ
            context.ClientMasters.AddOrUpdate(new ClientMasters()
            {
                ClientId = "fb417e35fe9745cbad8034edd91ee943",
                ClientName = "AuthProvider",
                ClientSecret = "e8f62fbb96d042c5a3317833cceed45b",
                AuthorizeConfirmFlag = false
            });

            // リダイレクトURIマスタ
            // Azure SQL Databaseで「DBCC CHECKIDENT」が無効なため、登録されていない場合のみ登録としている。
            // 更新、削除したい場合は手動で削除してから行うこと
            RedirectUriMasters redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "fb417e35fe9745cbad8034edd91ee943",
                RedirectUri = "https://dev.hc-auth.com/ja-JP/web/authenticate/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "fb417e35fe9745cbad8034edd91ee943",
                RedirectUri = "https://dev.hc-auth.com/ja-JP/web/userLogin/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "fb417e35fe9745cbad8034edd91ee943",
                RedirectUri = "https://dev.hc-auth.com/ja-JP/web/account/connectAuthenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "fb417e35fe9745cbad8034edd91ee943",
                RedirectUri = "https://dev.hc-auth.com/ja-JP/web/luna/authenticated",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            // 新規登録直接遷移用にRPモックに戻れるようにredirect_uriを設定しておく
            redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "fb417e35fe9745cbad8034edd91ee943",
                RedirectUri = "https://devid.carada.jp/RpMock/Token",
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            // ロール
            context.Roles.AddOrUpdate(new IdentityRole()
            {
                Id = "1782a059-0151-4135-85df-e9bcf3a52551", //ロールID。ASP.NET Identityが自動生成するものを固定値にした。
                Name = Properties.Settings.Default.UserRole
            });

            // 秘密の質問マスタ
            AddSecurityQuestionMasters(context);

            // JWT発行者情報
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev01",
                SecretKey = "BLG;*A4a*HZ6kErbe7Hg=)UbxEFUP6]o[@nU}1yHt:REN-qJ1x5s-Zk:Vt=bxFX=",
                Remark = "開発環境用の発行者情報01"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev02",
                SecretKey = "Uc=Ocmk%K&>M=FttpfjDJEOMt&Qm]2!1RUU:A%=KWj:77@whVg&5F(H$$MKgRV)O",
                Remark = "開発環境用の発行者情報02"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev03",
                SecretKey = "}*?Zil|{$Y-TJt5G2M{]#Zc%74q@CLpa{e@lrrN}anbsfDfI$;{(ZJh*qifK.tHY",
                Remark = "開発環境用の発行者情報03"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev04",
                SecretKey = "$SWexLpp5PO&KZk{(u9JDpU>7PHqf;x}.eL(*n$O-w7J-a=-O]Ls5^*b.f+P8l]Z",
                Remark = "開発環境用の発行者情報04"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaAdminDev05",
                SecretKey = "!BnPq0N3(^E#5DoQ[qIc_A3Ywgo2VKSl!lX1&uh?iUkiByUl%;:f032b>VO;k(q8",
                Remark = "開発環境用の発行者情報05"
            });
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaIdPTestDev",
                SecretKey = "QL^RgpPo40|3?UD!#mqcLXUmVF]0#0J0Fv=7(c]Qud9jah>=Mj-%EZ}Kj*mj#vRn",
                Remark = "開発テスト用の発行者情報(開発環境)"
            });
        }
    }
}