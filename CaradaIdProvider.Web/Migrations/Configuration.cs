﻿using Microsoft.AspNet.Identity.EntityFramework;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using System.Data.Entity.Migrations;

namespace MTI.CaradaIdProvider.Web.Migrations
{
    /// <summary>
    /// ローカル・CI環境用
    /// </summary>
    internal sealed class Configuration : BaseConfiguration
    {
        /// <summary>
        /// コンテキストにデータを追加してシードする
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(CaradaIdPDbContext context)
        {
            // クライアントマスタ
            context.ClientMasters.AddOrUpdate(new ClientMasters()
            {
                ClientId = "60ef3a9f183b43f8bfc582c30f79a6f2",
                ClientName = "AuthProvider",
                ClientSecret = "55e868071218493d9cf2119458b69beb",
                AuthorizeConfirmFlag = false
            });

            // リダイレクトURIマスタ
            // Azure SQL Databaseで「DBCC CHECKIDENT」が無効なため、登録されていない場合のみ登録としている。
            // 更新、削除したい場合は手動で削除してから行うこと
            RedirectUriMasters redirectUriMasters = new RedirectUriMasters()
            {
                ClientId = "60ef3a9f183b43f8bfc582c30f79a6f2",
                RedirectUri = "https://localhost/RpMock/Token"
            };
            AddRedirectUriMasters(context, redirectUriMasters);

            // ロール
            context.Roles.AddOrUpdate(new IdentityRole()
            {
                Id = "36e5abde-2d5c-4b9f-8d4b-adc5d9af9fad", //ロールID。ASP.NET Identityが自動生成するものを固定値にした。
                Name = Properties.Settings.Default.UserRole
            });

            // 秘密の質問マスタ
            AddSecurityQuestionMasters(context);

            // JWT発行者情報
            context.Issuers.AddOrUpdate(new Issuers()
            {
                IssuerId = "CaradaIdPTest",
                SecretKey = "FZlXb!U2d[ydXtTuKp1Q7ncM@}9{?Iit}ta}w;n7Mx{fHa+PoYGr=}((B@V*uAt-",
                Remark = "開発テスト用の発行者情報(ローカル・CI)"
            });
        }
    }
}
