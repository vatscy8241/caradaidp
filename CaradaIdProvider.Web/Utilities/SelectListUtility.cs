﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Utilities
{
    /// <summary>
    /// SelectListに関するユーティリティクラス
    /// </summary>
    public static class SelectListUtility
    {
        /// <summary>
        /// 辞書からSelectListへの変換関数.
        /// </summary>
        /// <param name="dic">辞書</param>
        /// <returns>SelectList</returns>
        public static List<SelectListItem> ConvertDicToSelectList(Dictionary<int, string> dic)
        {
            var retList = new List<SelectListItem>();
            if (dic == null)
            {
                return retList;
            }
            foreach (KeyValuePair<int, string> pair in dic)
            {
                retList.Add(new SelectListItem { Text = pair.Value, Value = pair.Key.ToString() });
            }
            return retList;
        }

        /// <summary>
        /// SelectListから辞書への変換関数.
        /// </summary>
        /// <param name="list">SelectList</param>
        /// <returns>辞書</returns>
        public static Dictionary<int, string> ConvertSelectListToDic(List<SelectListItem> list)
        {
            var retDic = new Dictionary<int, string>();
            if (list == null)
            {
                return retDic;
            }
            foreach (SelectListItem item in list)
            {
                retDic.Add(int.Parse(item.Value), item.Text);
            }
            return retDic;
        }
    }
}