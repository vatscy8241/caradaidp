﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Utilities
{
    /// <summary>
    /// DateTime拡張
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Unix時間を取得する
        /// </summary>
        /// <param name="t">対象DateTime</param>
        /// <returns>Unix時間のlong</returns>
        public static long ToUnixTime(this DateTime t)
        {
            return (long)t.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
    }
}