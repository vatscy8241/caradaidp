﻿using System.Text.RegularExpressions;

namespace MTI.CaradaIdProvider.Web.Utilities
{
    /// <summary>
    /// 機器種別判別ユーティリティクラス.
    /// </summary>
    public static class DeviceTypeUtility
    {
        // 表示モード
        public const string DISPLAY_MODE_SP = "pc";
        public const string DISPLAY_MODE_FP = "fp";

        /// <summary>
        /// UserAgentからPC/FPの判断をします。
        /// </summary>
        /// <param name="ua">UserAgent</param>
        /// <returns>pc/fp</returns>
        public static string GetDisplayModeByUserAgent(string ua)
        {
            if (string.IsNullOrEmpty(ua))
            {
                return DISPLAY_MODE_SP;
            }
            if (Regex.IsMatch(ua, "^.*(DoCoMo|UP.Browser|J-PHONE|Vodafone|SoftBank).*$"))
            {
                return DISPLAY_MODE_FP;
            }
            return DISPLAY_MODE_SP;
        }
    }
}
