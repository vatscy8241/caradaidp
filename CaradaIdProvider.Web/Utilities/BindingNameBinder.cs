﻿using MTI.CaradaIdProvider.Web.Models.ViewModels.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;

namespace MTI.CaradaIdProvider.Web.Utilities
{
    /// <summary>
    /// BindingNameAttributeを使用したBinder
    /// </summary>
    public class BindingNameBinder : DefaultModelBinder
    {
        /// <summary>
        /// 指定されたコントローラー コンテキストとバインディング コンテキストおよび指定されたプロパティ記述子を使用して、指定されたプロパティをバインドします。
        /// </summary>
        /// <param name="controllerContext">コントローラーが機能するコンテキスト。このコンテキスト情報には、コントローラー、HTTP コンテンツ、要求コンテキスト、ルート データなどが含まれます。</param>
        /// <param name="bindingContext">モデルをバインドするコンテキスト。このコンテキストには、モデル オブジェクト、モデル名、モデルの型、プロパティ フィルター、値プロバイダーなどの情報が含まれます。</param>
        /// <param name="propertyDescriptor">バインド対象のプロパティについて説明します。記述子は、コンポーネントの型、プロパティの型、プロパティ値などの情報を提供します。また、プロパティ値を取得または設定するメソッドを提供します。</param>
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            var isBind = false;
            foreach (var bindingNameAttribute in propertyDescriptor.ComponentType.GetProperty(propertyDescriptor.Name)
                                                    .GetCustomAttributes(typeof(BindingNameAttribute), true) as BindingNameAttribute[])
            {
                object value = null;
                // set property value.
                if ("GET".Equals(controllerContext.HttpContext.Request.HttpMethod.ToUpper()))
                {
                    isBind = true;
                    value = controllerContext.HttpContext.Request.QueryString[bindingNameAttribute.Name];

                }
                else if ("POST".Equals(controllerContext.HttpContext.Request.HttpMethod.ToUpper()))
                {
                    isBind = true;
                    value = controllerContext.HttpContext.Request.Form[bindingNameAttribute.Name];
                }

                if (value != null)
                {
                    propertyDescriptor.SetValue(bindingContext.Model, value);
                }
            }

            if (!isBind)
            {
                // 属性が付いていないときのみプロパティ名でバインドする
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
            }
        }
    }
}