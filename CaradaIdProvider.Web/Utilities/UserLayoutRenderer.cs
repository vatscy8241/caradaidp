﻿using NLog;
using NLog.LayoutRenderers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Utilities
{
    /// <summary>
    /// ユーザー情報のカスタムLayoutRenderer
    /// </summary>
    [LayoutRenderer("caradaid-user")]
    public class UserLayoutRenderer : LayoutRenderer
    {
        /// <summary>
        /// ユーザー情報を出力する
        /// </summary>
        /// <param name="builder">nlogのbuilder</param>
        /// <param name="logEvent">nlogのlogEvent</param>
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            IPrincipal user = null;
            if (HttpContext.Current != null)
            {
                user = HttpContext.Current.User;
            }
            if (user == null)
            {
                builder.Append("[Not Logged]");
                return;
            }
            // userが存在するのにidentityがnullになるパターンはない筈
            var identity = user.Identity as ClaimsIdentity;
            var claim = identity.FindFirst(ClaimTypes.NameIdentifier);
            if (claim == null)
            {
                builder.Append("[Not Logged]");
                return;
            }
            // ユーザーID + 半角スペース + CARADA ID
            builder.Append(claim.Value + " " + identity.Name);
        }
    }
}