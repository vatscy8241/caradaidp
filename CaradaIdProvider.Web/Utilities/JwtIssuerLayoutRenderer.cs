﻿using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using NLog.LayoutRenderers;
using System.Text;
using System.Web;

namespace MTI.CaradaIdProvider.Web.Utilities
{
    /// <summary>
    /// JWTを受け取るAPI向けカスタムLayoutRenderer
    /// </summary>
    [LayoutRenderer("api-user")]
    public class JwtIssuerLayoutRenderer : LayoutRenderer
    {
        public static string Issuer { get; set; }

        /// <summary>
        /// JWT Issuser 情報を出力する
        /// </summary>
        /// <remarks>
        /// JWTの検証でIssuer取得時などに mdc: item=api-user に登録してある前提。
        /// それが空なら埋め込み文字列を出力する。
        /// </remarks>
        /// <param name="builder">nlogのbuilder</param>
        /// <param name="logEvent">nlogのlogEvent</param>
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var apiUser = (string)HttpContext.Current.Items[Settings.Default.LogLayoutUserNameIss];
            if (string.IsNullOrEmpty(apiUser))
            {
                apiUser = "[IssUnknown]";
            }
            builder.Append(apiUser);
            return;
        }
    }
}