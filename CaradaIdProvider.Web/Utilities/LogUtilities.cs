﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MTI.CaradaIdProvider.Web.Utilities
{
    /// <summary>
    /// ログ出力用共通クラス
    /// </summary>
    public static class LogUtilities
    {
        /// <summary>
        /// KeyValuePairの繰り返しの形式のパラメータをログ出力用文字列にする。
        /// </summary>
        /// <param name="parameters">parameters</param>
        /// <returns>ログ出力用パラメータ文字列</returns>
        public static string GetParameterString(IEnumerable<KeyValuePair<string, string[]>> parameters)
        {
            var s = new StringBuilder();
            foreach (var param in parameters)
            {
                s.Append(param.Key);
                s.Append("=");
                if (param.Value.Length != 1)
                {
                    s.Append("[");
                }
                foreach (var v in param.Value)
                {
                    s.Append("\"");
                    s.Append(MaskProcessing(param.Key, v));
                    s.Append("\"");
                    if (param.Value.Length != 1)
                    {
                        s.Append(", ");
                    }
                }
                if (param.Value.Length != 1)
                {
                    s.Remove(s.Length - 2, 2);
                    s.Append("]");
                }
                s.Append(", ");
            }
            if (s.Length >= 2)
            {
                s.Remove(s.Length - 2, 2);
            }
            return s.ToString();
        }

        /// <summary>
        /// NameValueCollectionをログ出力用文字列にする。
        /// </summary>
        /// <param name="parameters">parameters</param>
        /// <returns>ログ出力用パラメータ文字列</returns>
        public static string GetParameterString(NameValueCollection parameters)
        {
            var s = new StringBuilder();
            foreach (var key in parameters.AllKeys)
            {
                s.Append(key);
                s.Append("=\"");
                s.Append(MaskProcessing(key, parameters[key]));
                s.Append("\", ");
            }
            if (s.Length >= 2)
            {
                s.Remove(s.Length - 2, 2);
            }
            return s.ToString();
        }

        /// <summary>
        /// Json 文字列をログ出力用文字列にする。
        /// </summary>
        /// <remarks>ネストした構造のJSONには非対応</remarks>
        /// <param name="jsonString">Jsonが期待される文字列</param>
        /// <returns>ログ出力用パラメータ文字列</returns>
        public static string GetParameterString(string jsonString)
        {
            var parameters = new List<KeyValuePair<string, string[]>>();
            var result = string.Empty;
            try
            {
                var json = JObject.Parse(jsonString);
                foreach (var s in json)
                {
                    parameters.Add(new KeyValuePair<string, string[]>(s.Key.ToString(), new string[] { s.Value.ToString() }));
                }
                result = GetParameterString(parameters);
            }
            catch (Exception)
            {
                result = jsonString;
            }
            return result;
        }

        /// <summary>
        /// オブジェクトをログ出力用文字列にする。
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>ログ出力用パラメータ文字列</returns>
        public static string GetParameterString(object obj)
        {
            var nvc = new NameValueCollection();
            obj.GetType().GetProperties().ToList().ForEach(pi => nvc.Add(pi.Name, (pi.GetValue(obj, null) != null) ? pi.GetValue(obj, null).ToString() : ""));
            return GetParameterString(nvc);
        }

        /// <summary>
        /// 出力してはいけない情報のマスク処理をする。
        /// 対象のキー名の正規表現に一致するパラメータの値を"*****"に置換する
        /// "^"と"$"はProperties.Settings.Default.MaskKeyNameRegexesに記載しないこと
        /// </summary>
        /// <param name="key">パラメータのキー名</param>
        /// <param name="value">パラメータの値</param>
        /// <returns>マスク対象なら置換されたパラメータ値。対象外は置換しないパラメータ値</returns>
        public static string MaskProcessing(string key, string value)
        {
            var regexes = Properties.Settings.Default.MaskKeyNameRegexes;
            // キー・値がない場合は値をそのまま値を返す
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(value))
            {
                return value;
            }
            // マスク対象はマスクを返す
            foreach (var s in regexes.Split(','))
            {
                var regex = new Regex("^" + s + "$", RegexOptions.IgnoreCase);
                if (regex.IsMatch(key))
                {
                    return Properties.Settings.Default.MaskString;
                }
            }
            // マスク対象外なのでそのまま値を返す
            return value;
        }
    }
}