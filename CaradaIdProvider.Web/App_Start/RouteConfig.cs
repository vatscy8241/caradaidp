﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MTI.CaradaIdProvider.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // ReleaseコンパイルはMockコントローラを全て排除する
#if RELEASE
            routes.IgnoreRoute("RpMock/{*action}");
            routes.IgnoreRoute("SendGridMock/{*action}");
            routes.IgnoreRoute("NewRegistMock/{*action}");
#endif
            // RCコンパイルはユーザー作製Mockコントローラ以外を排除する
#if RELEASE_CANDIDATE
            routes.IgnoreRoute("RpMock/{*action}");
            routes.IgnoreRoute("SendGridMock/{*action}");
#endif
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "User", action = "LoginView" }
                );
        }
    }
}
