﻿using MTI.CaradaIdProvider.Web.Handlers;
using System.Web.Http;

namespace MTI.CaradaIdProvider.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // 通常のAPIルーティング
            config.Routes.MapHttpRoute(
                name: "ApiDefault",
                routeTemplate: "api/{controller}/{action}"
            );

            config.MessageHandlers.Add(new ApiLogRequestHandler());
        }
    }
}
