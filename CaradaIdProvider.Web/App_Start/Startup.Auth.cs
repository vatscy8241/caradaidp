﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.IdentityManagers;
using Owin;
using System;

namespace MTI.CaradaIdProvider.Web
{
    /// <summary>
    /// スタートアップ用クラス。
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// ASP.NET Identity2.0用
        /// 認証設定の詳細については、http://go.microsoft.com/fwlink/?LinkId=301864 を参照してください
        /// </summary>
        /// <param name="app">IAppBuilder</param>
        public void ConfigureAuth(IAppBuilder app)
        {
            // 1 要求につき 1 インスタンスのみを使用するように DB コンテキスト、ユーザー マネージャー、サインイン マネージャーを構成します。
            app.CreatePerOwinContext(new DbContextFactory().NewCaradaIdPDbContext);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // アプリケーションが Cookie を使用して、サインインしたユーザーの情報を格納できるようにします
            // また、サードパーティのログイン プロバイダーを使用してログインするユーザーに関する情報を、Cookie を使用して一時的に保存できるようにします
            // サインイン Cookie の設定
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/User/LoginView"),
                CookieName = Properties.Settings.Default.LoginCookieName, // 認証Cookie名
                ExpireTimeSpan = TimeSpan.FromDays(Properties.Settings.Default.LoginCookieExpireDate), // 認証Cookieの有効期限
                CookieHttpOnly = true,
                CookieSecure = CookieSecureOption.Always,
                Provider = new CookieAuthenticationProvider
                {
                    // ユーザーがログインするときにセキュリティ スタンプを検証するように設定します。
                    // これはセキュリティ機能の 1 つであり、パスワードを変更するときやアカウントに外部ログインを追加するときに使用されます。
                    // ログインしている状態で、SecurityStampを自前で更新する処理を行うと、validateInterval経過後、
                    // SecurityStamp検証でエラーとなりログアウトさせられるため、暫定処置としてvalidateIntervalを60分に設定する。
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, CaradaIdUser>(
                        validateInterval: TimeSpan.FromMinutes(60),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
        }
    }
}