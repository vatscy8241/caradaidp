﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using MTI.CaradaIdProvider.Web.DataBase.Contexts;
using MTI.CaradaIdProvider.Web.Providers;
using Owin;
using System;

namespace MTI.CaradaIdProvider.Web
{
    /// <summary>
    /// スタートアップ用クラス。
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// OAuth用Owinセットアップ。
        /// </summary>
        /// <param name="app">app</param>
        public void ConfigureOAuth(IAppBuilder app)
        {
            var dbContextFactory = new DbContextFactory();

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = false, // HTTPS接続のみ許容
                AuthorizeEndpointPath = new PathString("/oauth2/auth"),
                TokenEndpointPath = new PathString("/oauth2/token"),
                Provider = new CaradaIdOAuthServiceProvider(),
                AuthorizationCodeProvider = new CaradaIdAuthorizationCodeProvider(),
                AuthorizationCodeExpireTimeSpan = TimeSpan.FromMinutes(60),
                AccessTokenExpireTimeSpan = TimeSpan.FromSeconds(3601) // -1秒がexpires_inになる
            });
        }
    }
}