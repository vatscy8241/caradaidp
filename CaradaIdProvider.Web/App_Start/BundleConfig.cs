﻿using System.Web.Optimization;

namespace MTI.CaradaIdProvider.Web
{
    public class BundleConfig
    {
        // バンドルの詳細については、http://go.microsoft.com/fwlink/?LinkId=301862  を参照してください
        public static void RegisterBundles(BundleCollection bundles)
        {
            // バンドル参照のバンドルと縮小が有効か
            var optimize = true;
#if DEBUG
            optimize = false;
#endif
            BundleTable.EnableOptimizations = optimize;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.matchHeight-min.js",
                        "~/Scripts/jquery.blockUI.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // CARADA IdP用
            bundles.Add(new ScriptBundle("~/bundles/caradaidp").Include(
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/carada-common.js",
                        "~/Scripts/caradaidp.js",
                        "~/Scripts/ie-emulation-modes-warning.js",
                        "~/Scripts/ie10-viewport-bug-workaround.js"));

            bundles.Add(new StyleBundle("~/Content/shared/css/style").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/shared/css/carada-common.css"));
        }
    }
}
