﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MTI.CaradaIdProvider.Web.DataBase.Entities
{
    /// <summary>
    /// ASP.NET Identityでライブラリが使用するテーブル。
    /// </summary>
    public class CaradaIdUser : IdentityUser
    {
        // ここに独自プロファイルを記載
        /// <summary>
        /// 企業ユーザーフラグ
        /// </summary>
        [Required]
        public bool EmployeeFlag { get; set; }

        /// <summary>
        /// 登録日時
        /// </summary>
        [Required]
        public DateTime CreateDateUtc { get; set; }

        /// <summary>
        /// 利用開始日時
        /// </summary>
        public DateTime? UseStartDateUtc { get; set; }

        /// <summary>
        /// 更新日時
        /// </summary>
        public DateTime? UpdateDateUtc { get; set; }

        /// <summary>
        /// 認証コード有効期限
        /// </summary>
        public DateTime? AuthCodeExpireDateUtc { get; set; }

        /// <summary>
        /// 認証コード失敗回数
        /// </summary>
        [Required]
        public int AuthCodeFailedCount { get; set; }

        /// <summary>
        /// 認証時に受け渡す情報を設定
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<CaradaIdUser> manager)
        {
            // authenticationType が CookieAuthenticationOptions.AuthenticationType で定義されているものと一致している必要があります
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // ここにカスタム ユーザー クレームを追加します
            return userIdentity;
        }

        /// <summary>
        /// 認証時に受け渡す情報を設定
        /// </summary>
        /// <param name="manager"></param>
        /// <returns></returns>
        public ClaimsIdentity GenerateUserIdentity(UserManager<CaradaIdUser> manager)
        {
            // authenticationType が CookieAuthenticationOptions.AuthenticationType で定義されているものと一致している必要があります
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // ここにカスタム ユーザー クレームを追加します
            return userIdentity;
        }
    }

}