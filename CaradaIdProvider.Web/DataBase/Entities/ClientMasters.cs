﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MTI.CaradaIdProvider.Web.DataBase.Entities
{
    /// <summary>
    /// クライアント(RP)マスタテーブル
    /// </summary>
    public class ClientMasters
    {
        /// <summary>
        /// クライアントID
        /// </summary>
        [Key]
        [StringLength(32)]
        public string ClientId { get; set; }

        /// <summary>
        /// クライアント名
        /// </summary>
        [StringLength(100)]
        [Index(IsUnique = true)]
        [Required]
        public string ClientName { get; set; }

        /// <summary>
        /// クライアントシークレット
        /// </summary>
        [StringLength(256)]
        [Required]
        public string ClientSecret { get; set; }

        /// <summary>
        /// 認可確認フラグ
        /// </summary>
        [Required]
        public bool AuthorizeConfirmFlag { get; set; }
    }
}