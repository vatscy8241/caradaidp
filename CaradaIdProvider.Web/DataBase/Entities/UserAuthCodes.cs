﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MTI.CaradaIdProvider.Web.DataBase.Entities
{
    /// <summary>
    /// 認可コードテーブル
    /// </summary>
    public class UserAuthCodes
    {
        /// <summary>
        /// 認可コード
        /// </summary>
        [Key]
        [StringLength(32)]
        public string AuthorizationCode { get; set; }

        /// <summary>
        /// ユーザーID
        /// </summary>
        [ForeignKey("CaradaIdUser")]
        [StringLength(128)]
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// RedirectUriマスタのSeqNo
        /// </summary>
        [ForeignKey("RedirectUriMasters")]
        [Required]
        public long RedirectUriSeqNo { get; set; }

        /// <summary>
        /// 有効期限
        /// </summary>
        [Required]
        public DateTime ExpireDateUtc { get; set; }

        /// <summary>
        /// CARADA ID
        /// </summary>
        public CaradaIdUser CaradaIdUser { get; set; }

        /// <summary>
        /// RedirectUriマスタ
        /// </summary>
        public RedirectUriMasters RedirectUriMasters { get; set; }
    }
}