﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MTI.CaradaIdProvider.Web.DataBase.Entities
{
    /// <summary>
    /// ユーザー認可情報テーブル
    /// </summary>
    public class AuthorizedUsers
    {
        /// <summary>
        /// ユーザーID
        /// </summary>
        [Key]
        [StringLength(128)]
        [Column(Order = 1)]
        [ForeignKey("CaradaIdUser")]
        public string UserId { get; set; }
        public virtual CaradaIdUser CaradaIdUser { get; set; }

        /// <summary>
        /// クライアントID
        /// </summary>
        [Key]
        [StringLength(32)]
        [Column(Order = 2)]
        [ForeignKey("ClientMasters")]
        public string ClientId { get; set; }
        public virtual ClientMasters ClientMasters { get; set; }

        /// <summary>
        /// サブジェクト
        /// </summary>
        [Required]
        [StringLength(32)]
        [Index(IsUnique = true)]
        public string Sub { get; set; }
    }
}