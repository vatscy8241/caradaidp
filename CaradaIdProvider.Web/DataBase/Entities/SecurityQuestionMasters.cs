﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MTI.CaradaIdProvider.Web.DataBase.Entities
{
    /// <summary>
    /// 秘密の質問マスタテーブル
    /// </summary>
    public class SecurityQuestionMasters
    {
        /// <summary>
        /// 秘密の質問ID
        /// </summary>
        [Key]
        [Column(Order = 1)]
        public int SecurityQuestionId { get; set; }

        /// <summary>
        /// 質問内容
        /// </summary>
        [Required]
        [StringLength(128)]
        [Column(Order = 2)]
        public string Question { get; set; }

        /// <summary>
        /// 表示順
        /// </summary>
        [Required]
        [Column(Order = 3)]
        [Index]
        public int DisplayOrder { get; set; }
    }
}