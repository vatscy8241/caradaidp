﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MTI.CaradaIdProvider.Web.DataBase.Entities
{
    /// <summary>
    /// JWTでのAPI使用の為の発行者情報を保持するテーブル
    /// </summary>
    [Table("Issuers")]
    public class Issuers
    {
        /// <summary>
        /// 発行者ID
        /// </summary>
        [Key]
        [StringLength(32)]
        [Column(Order = 1)]
        public string IssuerId { get; set; }

        /// <summary>
        /// 秘密鍵
        /// </summary>
        [Required]
        [StringLength(64)]
        [Column(Order = 2)]
        public string SecretKey { get; set; }

        /// <summary>
        /// 備考
        /// </summary>
        [Required]
        [StringLength(256)]
        [Column(Order = 3)]
        public string Remark { get; set; }
    }
}