﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MTI.CaradaIdProvider.Web.DataBase.Entities
{
    /// <summary>
    /// クライアントごとのRedirectUriマスタテーブル
    /// </summary>
    public class RedirectUriMasters
    {
        /// <summary>
        /// SeqNo
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SeqNo { get; set; }

        /// <summary>
        /// クライアントID
        /// </summary>
        [ForeignKey("ClientMasters")]
        [StringLength(32)]
        [Required]
        [Index("IX_ClientId_RedirectUri", IsUnique = true, Order = 1)]
        public string ClientId { get; set; }

        /// <summary>
        /// リダイレクトURI
        /// </summary>
        [StringLength(300)]
        [Required]
        [Index("IX_ClientId_RedirectUri", IsUnique = true, Order = 2)]
        public string RedirectUri { get; set; }

        /// <summary>
        /// クライアントマスタ
        /// </summary>
        public ClientMasters ClientMasters { get; set; }
    }
}