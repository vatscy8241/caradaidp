﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTI.CaradaIdProvider.Web.DataBase.Contexts
{
    /// <summary>
    /// メソッド途中でDbContextを生成する際にコンストラクタの代替となるFactoryです。
    /// </summary>
    public class DbContextFactory
    {
        /// <summary>
        /// 新しいCaradaIdPDbContextを返却します。
        /// </summary>
        public virtual CaradaIdPDbContext NewCaradaIdPDbContext() { return new CaradaIdPDbContext(); }
    }
}