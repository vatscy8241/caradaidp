﻿using Microsoft.AspNet.Identity.EntityFramework;
using MTI.CaradaIdProvider.Web.DataBase.Entities;
using MTI.CaradaIdProvider.Web.Properties;
using NLog;
using System.Data.Entity;

namespace MTI.CaradaIdProvider.Web.DataBase.Contexts
{
    /// <summary>
    /// CARADA IdP DBのContext。
    /// </summary>
    [DbConfigurationType(typeof(GlobalDbConfiguration))]
    public class CaradaIdPDbContext : IdentityDbContext<CaradaIdUser>
    {
        /// <summary>
        /// Debugログ用
        /// </summary>
        private static readonly ILogger log = LogManager.GetLogger(Settings.Default.LogTargetWeb);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CaradaIdPDbContext()
            : base("CaradaIdPDbContext", throwIfV1Schema: false)
        {
            this.Database.Log = s => log.Debug(s);
        }

        /// <summary>
        /// 認可コードテーブル
        /// </summary>
        public virtual DbSet<UserAuthCodes> UserAuthCodes { get; set; }

        /// <summary>
        /// クライアントマスタ
        /// </summary>
        public virtual DbSet<ClientMasters> ClientMasters { get; set; }

        /// <summary>
        /// リダイレクトURIマスタ
        /// </summary>
        public virtual DbSet<RedirectUriMasters> RedirectUriMasters { get; set; }

        /// <summary>
        /// ユーザー認可情報
        /// </summary>
        public virtual DbSet<AuthorizedUsers> AuthorizedUsers { get; set; }

        /// <summary>
        /// 秘密の質問マスタ
        /// </summary>
        public virtual DbSet<SecurityQuestionMasters> SecurityQuestionMasters { get; set; }

        /// <summary>
        /// 秘密の質問回答情報
        /// </summary>
        public virtual DbSet<SecurityQuestionAnswers> SecurityQuestionAnswers { get; set; }

        /// <summary>
        /// JWTでのAPI使用の為の発行者情報
        /// </summary>
        public virtual DbSet<Issuers> Issuers { get; set; }
    }
}