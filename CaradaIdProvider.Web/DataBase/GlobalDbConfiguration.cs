﻿using MTI.CaradaIdProvider.Web.Properties;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Runtime.Remoting.Messaging;

namespace MTI.CaradaIdProvider.Web.DataBase
{
    /// <summary>
    /// アプリケーションのDbConfigurationクラス
    /// </summary>
    public class GlobalDbConfiguration : DbConfiguration
    {
        /// <summary>
        /// 設定情報から取得したリトライ数とディレイ秒数を取得してSqlAzureExecutionStrategyにセットするコンストラクタ。
        /// </summary>
        public GlobalDbConfiguration()
        {
            int retryCount = Settings.Default.DbMaxRetryCount;
            TimeSpan delay = TimeSpan.FromSeconds(Settings.Default.DbMaxDelaySeconds);

            SetExecutionStrategy("System.Data.SqlClient",
                () => SuspendExecutionStrategy ? (IDbExecutionStrategy)new DefaultExecutionStrategy()
                    : new SqlAzureExecutionStrategy(retryCount, delay));
        }

        /// <summary>
        /// SqlAzureExecutionStrategyの実行を停止するかどうかを設定する
        /// </summary>
        public static bool SuspendExecutionStrategy
        {
            get
            {
                return (bool?)CallContext.LogicalGetData("SuspendExecutionStrategy") ?? false;
            }
            set
            {
                CallContext.LogicalSetData("SuspendExecutionStrategy", value);
            }
        }
    }
}