﻿using System;
using System.Threading;
using System.Web.Hosting;

namespace MTI.CaradaIdProvider.Web.Tasks
{
    /// <summary>
    /// HostingEnvironmentを使用した非同期処理のラッパーです。
    /// </summary>
    public class WebHostingBackGroundProcessor : IBackGroundProcessor
    {
        /// <summary>
        /// HostingEnvironment.QueueBackgroundWorkItem()を実行します。処理は直ちに起動します。
        /// </summary>
        /// <param name="workItem">非同期処理定義</param>
        public void QueueBackGroundWorkItem(Action<CancellationToken> workItem) => HostingEnvironment.QueueBackgroundWorkItem(workItem);
    }
}