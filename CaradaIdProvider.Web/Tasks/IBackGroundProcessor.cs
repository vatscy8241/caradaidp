﻿using System;
using System.Threading;

namespace MTI.CaradaIdProvider.Web.Tasks
{
    /// <summary>
    /// 完全非同期に行う処理のためのキューイングを提供します。
    /// </summary>
    public interface IBackGroundProcessor
    {
        /// <summary>
        /// 非同期処理を設定します。HttpContextを引き継げない点に注意してください。
        /// </summary>
        /// <param name="workItem">非同期処理の定義</param>
        void QueueBackGroundWorkItem(Action<CancellationToken> workItem);
    }
}