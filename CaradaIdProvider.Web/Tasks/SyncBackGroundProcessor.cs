﻿using System;
using System.Threading;

namespace MTI.CaradaIdProvider.Web.Tasks
{
    /// <summary>
    /// 同期実行を行うBackGroundProcessorです。テスト用途を想定しています。
    /// </summary>
    public class SyncBackGroundProcessor : IBackGroundProcessor
    {
        /// <summary>
        /// そのまま同期的に実行します。CancellationTokenプロパティを実行に使用します。
        /// </summary>
        /// <param name="workItem">処理定義</param>
        public void QueueBackGroundWorkItem(Action<CancellationToken> workItem) => workItem.Invoke(CancellationToken);


        /// <summary>
        /// 実行を中止する場合はコンストラクタのパラメータにtrueを引き渡したCancellationTokenをセットしてください。
        /// </summary>
        public CancellationToken CancellationToken { get; set; } = new CancellationToken();
    }
}