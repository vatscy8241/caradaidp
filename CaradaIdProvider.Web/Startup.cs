﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MTI.CaradaIdProvider.Web.Startup))]
namespace MTI.CaradaIdProvider.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureOAuth(app);
        }
    }
}
